<#
.SYNOPSIS
    Compute MD5 sum.
.DESCRIPTION
    Create a text file which contains the MD5 sum for all input files. Note
    that the input files must be listed as an array, i.e. comma separated.
    File path is not included in the output file.
.PARAMETER DestinationPath
    Destination of the output text file.
.PARAMETER Path
    Specifies a path to one or more locations of files to compute the MD5
    sum of. Wildcards are allowed.
.EXAMPLE
    create-md5sum.ps1 -DestinationPath "Att 2 to 0001234.md5" firmware*.hex, firmware*.bin, firmware*.map
    Computes the MD5 sum for the firmware build artifacts. Note that wildcards
    are used to handle the version number in the filename.
#>

param (
    [Parameter(Mandatory=$True)]
    [string]$DestinationPath, # Output text file with md5 sum of all input files

    [Parameter(Mandatory=$True)]
    [string[]]$Path           # Array of input files (must be separated by ',' (comma))
)

$Path | % {
    Get-FileHash -Algorithm MD5 $_ |
      % {
	  ($_.Hash).ToLower() + " *" + (Get-Item $_.Path).Name
      } 
} | Out-File -FilePath $DestinationPath -Encoding ASCII -Force
