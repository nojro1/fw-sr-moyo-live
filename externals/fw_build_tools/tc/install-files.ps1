<#
.SYNOPSIS
    Install content of zip archive into directory handled by Mercurial.
.DESCRIPTION
    All files in the DestinationPath are removed with the command "hg rm"
    before the zip file is unpacked. After unpacking all files in the
    directory are added to the version control system with the command
    "hg add".
.PARAMETER File
    Destination of the input zip file.
.PARAMETER DestinationPath
    Specifies a path to the directory the content of File shall be installed.
.PARAMETER Largefiles
    Add files as largefiles in Mercurial.
.EXAMPLE
    install-files.ps1 -File objdictgen_.zip -DestinationPath ./objdictgen
#>

param (
    [Parameter(Mandatory=$True)]
    [string]$File,             # Input zip file
    [Parameter(Mandatory=$True)]
    [string]$DestinationPath,   # Directory where $File shall be instlled
    [switch]$Largefiles = $false # Add files as Mercurial largefiles
)

function Unzip([string]$zipfile, [string]$outpath)
{
    Add-Type -AssemblyName System.IO.Compression.FileSystem
    [System.IO.Directory]::SetCurrentDirectory($(pwd))
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

# verify that $File exist
if(! (Test-Path $File)) {
    Write-Error "Error: '$File' does not exist." -Category InvalidArgument
    exit 1
}

# verify that $DestinationPath exist
if(! (Test-Path $DestinationPath)) {
    Write-Error "Error: '$DestinationPath' does not exist." -Category InvalidArgument
    [void](mkdir -Force $DestinationPath)
}

# verify that $DestinationPath is within a hg repository
hg status $DestinationPath
if(! $?) {
    Write-Error "Error: '$DestinationPath' is not a Mercurial repository." -Category InvalidArgument
    exit 1
}

# remove files
hg remove $DestinationPath/*
if(! $?) {
    Write-Error "Error: Failed to removed files in '$DestinationPath'."
}
[void](mkdir -Force $DestinationPath)

# unpack files
Unzip $File $DestinationPath

# add files
if($Largefiles) {
    hg add --large $DestinationPath
} else {
    hg add $DestinationPath
}
if(! $?) {
    Write-Error "Error: Failed to add files in '$DestinationPath'."
}
