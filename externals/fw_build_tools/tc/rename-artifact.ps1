<#
.SYNOPSIS
    Rename build artifact by adding version number to file name.
.DESCRIPTION
    The version number string is added to the file name separated by an
    underscore ('_'). The file name extension is left as is.
    I.e. "FILENAME.EXTENSION" is renamed to "FILENAME_VERSION.EXTENSION".
.PARAMETER VersionNumber
    Version number that shall be added to the file name.
.PARAMETER Path
    Specifies a path to the file that shall be renamed.
.EXAMPLE
    rename-artifact.ps1 -VersionNumber "1.2.3.4" -Path IARLinkLoad\Release\Exe\ShockLink.hex
    Add version number "1.2.3.4" to the ShockLink.hex file, which is then
    called ShockLink_1.2.3.4.hex.
.EXAMPLE
    rename-artifact.ps1 -VersionNumber %build.number% -Path IARLinkLoad\Release\Exe\ShockLink.hex
    Add version number supplied by Teamcity build server to the ShockLink.hex
    file.
.EXAMPLE
    rename-artifact.ps1 %build.number% IARLinkLoad\Release\Exe\ShockLink.hex
    Add version number to the ShockLink.hex file.
    Note that parameter names have been omitted and must therefore be listed
    in the correct order.
#>

param (
    [Parameter(Mandatory=$True)]
    [string]$VersionNumber,   # Version number, e.g. "1.2.3.4"

    [Parameter(Mandatory=$True)]
    [string[]]$Path           # Input file
)

Get-ChildItem $Path | Rename-Item -NewName { $_.Name -replace ($_.Extension),("_"+$VersionNumber+$_.Extension) }
