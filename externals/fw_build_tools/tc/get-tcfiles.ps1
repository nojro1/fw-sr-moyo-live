<#
.SYNOPSIS
    Download build artifacts from Teamcity server.
.DESCRIPTION
    All specified build artifact files are downloaded from the Teamcity
    server. The string "{VERSION}" can be used to access artifacts that
    contain the version number as part of the file name. Note that the
    version number is stripped from the file name before it is stored in
    the destination folder.  Note that the input files must be listed as an
    array, i.e. comma separated.
.PARAMETER DestinationPath
    Specifies a path to the directory the content of File shall be copied.
.PARAMETER Uri
    URI to the build.
.PARAMETER Files
    Specifies which files to download. When the string "{VERSION}" is used in
    the file name it will be replaced with the build version number when
    accessing the Teamcity server, but removed from the output file name.
.PARAMETER NoModifyBuildnr
    If switch is used then TC buildnumber will not be modified
.PARAMETER PreserveVersionInFilename
    If switch is used then version will not be stripped from filename
.EXAMPLE
    get-tcfiles.ps1 -DestinationPath ./ -Uri "http://fwbuild.laerdal.global:8080/guestAuth/app/rest/builds/buildType:CanLib_BuildRelease,status:SUCCESS" -Files "objdictgen_{VERSION}.zip"
#>

param (
    [Parameter(Mandatory=$True)]
    [string]$DestinationPath, # Directory where $File shall be copied

    [Parameter(Mandatory=$True)]
    [string]$Uri,             # URI to Teamcity build

    [Parameter(Mandatory=$True)]
    [string[]]$Files,         # List of artifacts to download

    [Parameter(Mandatory=$False)]
    [switch]$NoModifyBuildnr = $False,

    [Parameter(Mandatory=$False)]
    [switch]$PreserveVersionInFilename = $False
)


# get build version number
$url = $Uri + "/number"
[string]$version_number = (Invoke-WebRequest -UseBasicParsing -Uri $url).Content
if(! $?) {
    Write-Error "Error: Failed to access '$url'"
    exit 1
}

# download files
foreach ($filepath in $Files) {
    $versioned_filepath = $filepath.replace("{VERSION}", $version_number)
    $stripped_filepath = $filepath.replace("{VERSION}", "")
    if (!$PreserveVersionInFilename)
    {
        $outfilepath = $DestinationPath + "/"+ $stripped_filepath
    }
    else {
        $outfilepath = $DestinationPath + "/"+ $versioned_filepath
    }
    $url = $Uri + "/artifacts/files/" + $versioned_filepath
    Invoke-WebRequest -UseBasicParsing -Uri $url -OutFile $outfilepath
    if(! $?) {
        Write-Error "Error: Failed to access '$url'"
    }
}

if (!$NoModifyBuildnr)
{
    # return build number to teamcity
    Write-Host "##teamcity[buildNumber '$version_number']"
}