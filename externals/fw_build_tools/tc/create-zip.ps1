<#
.SYNOPSIS
    Create zip archive.
.DESCRIPTION
    Use a temporary directory to create a zip file with flat directory
    structure (i.e. one level). Note that the input files must be listed
    as an array, i.e. comma separated.
.PARAMETER InternalPath
    Put the files into this directory instead of at top level of zip file.
.PARAMETER DestinationPath
    Destination of the output zip file.
.PARAMETER Path
    Specifies a path to one or more locations of files to include in the
    zip archive. Wildcards are allowed.
.EXAMPLE
    create-zip.ps1 -DestinationPath "Att 1 to 0001234.zip" firmware*.hex, firmware*.bin, firmware*.map
    Create zip archive of all the firmware build artifacts. Note that wildcards
    are used to handle the version number in the filename.
#>

param (
    [string]$InternalPath,    # Internal path in zip file

    [Parameter(Mandatory=$True, Position = 0)]
    [string]$DestinationPath, # Output zip file

    [Parameter(Mandatory=$True, Position = 1)]
    [string[]]$Path           # Array of input files (must be separated by ',' (comma))
)

function ZipFiles( $zipfilename, $sourcedir )
{
    Add-Type -Assembly System.IO.Compression.FileSystem
    $compressionLevel = [System.IO.Compression.CompressionLevel]::Optimal
    [System.IO.Directory]::SetCurrentDirectory($(pwd))
    [System.IO.Compression.ZipFile]::CreateFromDirectory($sourcedir,
      $zipfilename, $compressionLevel, $false)
}

# create temporary directory
$tmpdir = "tmp"  # todo use unique name?
if(Test-Path $tmpdir) {
    Remove-Item -Recurse $tmpdir # Make sure the temp directory is removed
}
[void](mkdir $tmpdir/$InternalPath)
# copy files (wildcards allowed)
foreach ($filepath in $Path) {
    Copy-Item $filepath $tmpdir/$InternalPath
}
# verify that output folder exist
$output_dir = Split-Path -Path $DestinationPath
if($output_dir) {
    if(!(Test-Path $output_dir)) {
        [void](mkdir $output_dir)
    }
}
# create zip file
if(Test-Path $DestinationPath) {
    Remove-Item $DestinationPath
}
ZipFiles $DestinationPath $tmpdir
# delete temporary directory
Remove-Item -Recurse $tmpdir
