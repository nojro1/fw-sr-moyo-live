﻿<#
.SYNOPSIS
    Create manifest file based on hg information for the given checkout folder
.DESCRIPTION
    Create a manifest file with information needed by agile release

.PARAMETER OutFile
    Name of manifest file
.PARAMETER Version
    Software version
#>

param (
    [Parameter(Mandatory=$True)]
    [string]$OutFile,
    [Parameter(Mandatory=$True)]
    [string]$Version,
    [Parameter(Mandatory=$True)]
    [string]$JiraProjectKey,
    [Parameter(Mandatory=$False)]
    [ValidateSet('hg','git')]
    [string]$VcsType="hg"
)
Write-Output "Software Version: $Version" | Out-File -FilePath $OutFile -Encoding UTF8
Write-Output "`n" | Out-File -FilePath $OutFile -Encoding UTF8  -append

Write-Output "Location of Source Code and Data Repositories:" | Out-File -FilePath $OutFile -Encoding UTF8 -append
if($VcsType -eq "hg"){
    hg paths | Out-File -FilePath $OutFile -Encoding UTF8  -append
}
elseif($VcsType -eq "git"){
    git config --get remote.origin.url | Out-File -FilePath $OutFile -Encoding UTF8  -append
}
Write-Output "`n"| Out-File -FilePath $OutFile -Encoding UTF8  -append

Write-Output "Source Code Tag / Revision:" | Out-File -FilePath $OutFile -Encoding UTF8  -append
if($VcsType -eq "hg"){
    $hash = hg parent --template '{node}' 
}
elseif($VcsType -eq "git"){
    $hash = git rev-parse HEAD
}
Write-Output "Hash : $hash" | Out-File -FilePath $OutFile -Encoding UTF8  -append

Write-Output "`n" | Out-File -FilePath $OutFile -Encoding UTF8  -append

Write-Output "Issue Tracking Location" | Out-File -FilePath $OutFile -Encoding UTF8  -append
Write-Output "https://laerdaljira.atlassian.net/secure/RapidBoard.jspa?projectKey=$JiraProjectKey&view=planning" | Out-File -FilePath $OutFile -Encoding UTF8  -append
