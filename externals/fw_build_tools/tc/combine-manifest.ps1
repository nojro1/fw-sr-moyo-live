<#
.SYNOPSIS
    Combine manifest files
.DESCRIPTION
    Combine manifest files

.PARAMETER OutFile
    Name of combined manifest file
.PARAMETER Infile1
    filename for first manifest file
.PARAMETER Infile2
    filename for second manifest file
.PARAMETER Infile3
    filename for third manifest file (optional)
.PARAMETER Infile1Name
    text to describe the first loadmodule
.PARAMETER Infile2Name
    text to describe the second loadmodule
.PARAMETER Infile3Name
    text to describe the third loadmodule (optional)
#>

param (
    [Parameter(Mandatory=$True)]
    [string]$OutFile,
    [Parameter(Mandatory=$True)]
    [string]$Infile1,
    [Parameter(Mandatory=$True)]
    [string]$Infile2,
    [Parameter(Mandatory=$False)]
    [string]$Infile3,
    [Parameter(Mandatory=$True)]
    [string]$Infile1Name,
    [Parameter(Mandatory=$True)]
    [string]$Infile2Name,
    [Parameter(Mandatory=$False)]
    [string]$Infile3Name
)

$file1Content = [string[]](Get-Content $Infile1)
$Infile1Version = $file1Content[0].Substring(18)
$Infile1RepoLocation = $file1Content[4].Substring(10)
$Infile1RepoHash = $file1Content[8].Substring(17)
$infile1IssueTracker = $file1Content[12]

$file2Content = [string[]](Get-Content $Infile2)
$Infile2Version = $file2Content[0].Substring(18)
$Infile2RepoLocation = $file2Content[4].Substring(10)
$Infile2RepoHash = $file2Content[8].Substring(17)
$infile2IssueTracker = $file2Content[12]

if ($Infile3.Length -gt 0)
{
    $file3Content = [string[]](Get-Content $Infile3)
    $Infile3Version = $file3Content[0].Substring(18)
    $Infile3RepoLocation = $file3Content[4].Substring(10)
    $Infile3RepoHash = $file3Content[8].Substring(17)
    $infile3IssueTracker = $file3Content[12]    
}

$softwareString = "Software Version: $Infile1Name : $Infile1Version, $Infile2Name : $Infile2Version"
if ($Infile3.Length -gt 0)
{
    $softwareString = "$softwareString, $Infile3Name : $Infile3Version"
}
Write-Output $softwareString | Out-File -FilePath $OutFile -Encoding UTF8
Write-Output "" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "Location of Source Code and Data Repositories:" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "  $Infile1Name = $Infile1RepoLocation" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "  $Infile2Name = $Infile2RepoLocation" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "  $Infile3Name = $Infile3RepoLocation" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "Source Code Tag / Revision:" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "  $Infile1Name mercuial hash = $Infile1RepoHash" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "  $Infile2Name mercuial hash = $Infile2RepoHash" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "  $Infile3Name mercuial hash = $Infile3RepoHash" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "Source Code Tag / Revision:" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "  $Infile1Name = $infile1IssueTracker" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "  $Infile2Name = $infile2IssueTracker" | Out-File -FilePath $OutFile -Encoding UTF8 -append
Write-Output "  $Infile3Name = $infile3IssueTracker" | Out-File -FilePath $OutFile -Encoding UTF8 -append
