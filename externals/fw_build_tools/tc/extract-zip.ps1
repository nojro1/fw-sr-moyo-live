﻿<#
.SYNOPSIS
    Extract a zip-file to a destination folder
.PARAMETER DestinationPath
    Relative path to where .zip file shall be extracted
.PARAMETER ZipFile
    .zip file to extract
.EXAMPLE
    extract-zip.ps1 -ZipFile .\cpum-delivery.zip -DestinationPath .\fw_can_bin_artifacts
#>


param (
    [Parameter(Mandatory=$True)]
    [string]$DestinationPath, # Relative path to where $ZipFile shall be extracted

    [Parameter(Mandatory=$True)]
    [string]$ZipFile # ZipFile to extract
)

Add-Type -AssemblyName System.IO.Compression.FileSystem

[System.IO.Compression.ZipFile]::ExtractToDirectory("$pwd\$ZipFile","$pwd\$DestinationPath")
