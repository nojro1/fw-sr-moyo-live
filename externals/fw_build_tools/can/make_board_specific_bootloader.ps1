﻿<#
.SYNOPSIS
    Add Configuration memory for specific board and application to generic bootloader hex.
.DESCRIPTION
    Configuration memory stencil is added to the bootom of a generic bootloader hex file.
    Configuration parameter "Board unique id" is filled with 0xFF to allow PCA production
    to fill in unique number in production.

.PARAMETER InFile
    Bootloader .hex file
.PARAMETER Agileno
    PCA Agile part number
.PARAMETER BoardFamilyId
    Type of PCA board
#>

param (
    [Parameter(Mandatory=$True)]
    [string]$InFile,          
    [Parameter(Mandatory=$True)]
    [string]$Agileno,
    [Parameter(Mandatory=$True)]
    [uint16]$BoardfamilyId,
    [Parameter(Mandatory=$True)]
    [uint16]$CANLibId,
    [Parameter(Mandatory=$False)]
    [uint16]$ApplicationID=0,
    [Parameter(Mandatory=$False)]
    [uint16]$filesystemcontentId=0,
    [Parameter(Mandatory=$False)]
    [switch]$notPCAProduction,
    [Parameter(Mandatory=$False)]
    [string]$hwRevision,
    [Parameter(Mandatory=$False)]
    [string]$nodeId=0,
    [Parameter(Mandatory=$False)]
    [string]$BoardName
)

$bootloaderfile = Resolve-Path($InFile) # syntax : " c:\can_boot_1.2.3.4.hex

$outfile = $bootloaderfile.ToString().Replace(".hex"," $Agileno.hex")


if ($BoardName.Length -gt 0)
{
    $outfile = $outfile.Replace(".hex", "_$BoardName.hex")
}


$contentBytes = [IO.File]::ReadAllLines($bootloaderfile)
[String]$content = ""

foreach ($line in $contentBytes)
{
    if (![System.String]::Equals($line,":00000001FF"))
    {
        $content += $line + [System.Environment]::NewLine
    }

}

[uint32]$partnr_numberic = 0

if ([bool]$result = [int]::TryParse($Agileno.Replace("-",""), [ref]$partnr_numberic))
{
    $content += ":020000040800F2" + [System.Environment]::NewLine
    if (!$notPCAProduction)
    {
        $content += ":04300000EEEE0106e9" + [System.Environment]::NewLine
        $content += ":02300A000A02B8" + [System.Environment]::NewLine

        [String]$variableLine = "300E000204"
    }
    else
    {
        [String]$variableLine = "300000EEEE"
        if ($hwRevision.Length -gt 0)
        {
            $variableLine += "0A02";
            $chararray = $hwRevision.ToCharArray();
            foreach ($element in $chararray)
            {
                $variableLine += [string]::Format("{0:x2}",[Convert]::ToByte($element))

            }

            if ($hwRevision.Length -eq 1)
            {
                $variableLine += "00"
            }
        }
        $variableLine += "0204"
    }

    #agile part number
    for ($i = 0; $i -lt 4; $i++)
    {
        $byte = (($partnr_numberic -shr ($i*8)) -band 0xff);
        [String]$hexbyte = [String]::Format("{0:x2}",$byte)
        $variableLine += $hexbyte
    }

    #boardfamilyid 
    $variableLine += "0302"
    for ($i = 0; $i -lt 2; $i++)
    {
        $byte = (($BoardfamilyId -shr ($i*8)) -band 0xff);
        [String]$hexbyte = [String]::Format("{0:x2}",$byte)
        $variableLine += $hexbyte
    }

    #application id
    if ($ApplicationID -gt 0)
    {
        $variableLine += "0402"
        for ($i = 0; $i -lt 2; $i++)
        {
            $byte = (($ApplicationID -shr ($i*8)) -band 0xff);
            [String]$hexbyte = [String]::Format("{0:x2}",$byte)
            $variableLine += $hexbyte
        }
    }

    #node id
    if ($nodeId -gt 0)
    {
        $variableLine += "0702"
        for ($i = 0; $i -lt 2; $i++)
        {
            $byte = (($nodeId -shr ($i*8)) -band 0xff);
            [String]$hexbyte = [String]::Format("{0:x2}",$byte)
            $variableLine += $hexbyte
        }
    }

    #canlib id
    if ($CANLibId -gt 0)
    {
        $variableLine += "0802"
        for ($i = 0; $i -lt 2; $i++)
        {
            $byte = (($CANLibId -shr ($i*8)) -band 0xff);
            [String]$hexbyte = [String]::Format("{0:x2}",$byte)
            $variableLine += $hexbyte
        }
    }

    #filesystemcontentId
    if ($filesystemcontentId -gt 0)
    {
        $variableLine += "060201000502"
        for ($i = 0; $i -lt 2; $i++)
        {
            $byte = (($filesystemcontentId -shr ($i*8)) -band 0xff);
            [String]$hexbyte = [String]::Format("{0:x2}",$byte)
            $variableLine += $hexbyte
        }
    }

    $variabledatalength = ($VariableLine.length/2)-3

    $VariableLine = [String]::Format("{0:x2}", $variabledatalength) + $variableLine

    for ($i = 0; $i -lt $VariableLine.length; $i+=2)
    {
        $byte = [convert]::ToByte($variableLine.Substring($i,2),16)
        $checksum = ($checksum + $byte) -band 0xff
    }


    $checksum = 1 + (0xff -bxor $checksum)
    [String]$hexbyte = [String]::Format("{0:x2}", $checksum -band 0xff )
    
    $content += ":" + $variableLine.ToUpper() + $hexbyte + [System.Environment]::NewLine
    $content += ":00000001FF"

    Out-File -InputObject $content -FilePath $outfile -Encoding ascii
}
