#This script is developed and tested in python 2.7
#Author : Wei Liao
#Data : 06/06/2016
#Ver : 1.0
import ConfigParser
import os
import shutil
import binascii
import array
#Current directory path 
CurrentPath = os.path.abspath('.')

#file allocation allocation list
FileAllocTableList = []

Config = ConfigParser.ConfigParser()

class FileAllocTable:
    def __init__(self,file_name, file_start_addr, file_size, file_type):
        self.file_name = file_name.replace("\\","/")
        self.file_start_addr = file_start_addr
        self.file_size = file_size
        self.file_type = file_type
    
    def display(self):
        return ('========================================================\n'
         +'File Name  :     ' + self.file_name +'\n'
         +'Start Addr :     ' + str(hex(self.file_start_addr)) +'\n'
         +'File Size  :     ' + str(hex(self.file_size)) +'\n'
         +'File Type  :     ' + str(self.file_type) +'\n'
         +'========================================================\n\n'
        )           
    
class FileSystemHeader:
    def __init__(self,crc32,load_module_id,file_sys_id,file_content_id, file_sys_ver, data_blob_size):
        self.crc32 = crc32
        self.load_module_id = load_module_id
        self.file_sys_id = file_sys_id
        self.file_content_id = file_content_id
        self.file_sys_ver = file_sys_ver
        self.data_blob_size = data_blob_size
    def display(self):
        return ('CRC value : ' + hex(self.crc32) +'\n'
         +'Load Module ID :' + str(self.load_module_id) +'\n'
         +'File System ID :' + str(self.file_sys_id) +'\n'
         +'File Content ID :' + str(self.file_content_id) +'\n'
         +'File System Version : ' + self.file_sys_ver +'\n'
         +'File Data Blob Size : ' + str(self.data_blob_size) +'\n'
        )  
        
def CreateHeaderAndFileAllocTable(rootDir):
   
    fileType = Config.getint("Config", "FileType")
    headerSize  = Config.getint("Config", "HeaderSize")
    entrySize  = Config.getint("Config", "EntrySize")  
    filesysid = Config.getint("Header", "FileSystemId")
    filecontentid = Config.getint("Header", "FileContentId")
    filesysver = Config.get("Header", "FileSystemVersion")
    loadmoduleid = Config.getint("Header", "LoadModuleId")
    header = FileSystemHeader(0x00,loadmoduleid,filesysid,filecontentid,filesysver,0x00)
    for root, dirs, files in os.walk(rootDir):
        for name in sorted(files):
            if name.endswith("snd"):
                print root + name
                fileSize = os.path.getsize(os.path.relpath(os.path.join(root, name)))                
                FileAllocTableList.append(FileAllocTable(os.path.relpath(os.path.join(root, name)),0x00,fileSize,fileType))
    
    #calculate how many files, then we know file alloc table size. 
    FileDataStartAddr =    len(FileAllocTableList) * entrySize + headerSize

    #Write file start address to all file allocation table elements
    for table in FileAllocTableList:
        table.file_start_addr = FileDataStartAddr
        FileDataStartAddr=FileDataStartAddr+table.file_size
        #print table.file_start_addr
    FileAllocTableList[-1].file_start_addr += 0x80000000    #set bit 31 of last file to 1 to indicate this is the end.
    print table.file_start_addr

    #calculate Data blob size 
    header.data_blob_size = FileAllocTableList[-1].file_start_addr - 0x80000000 + FileAllocTableList[-1].file_size - headerSize
    
    return header
    
def main():
    
    Config.read(os.getcwd()+'/config.ini')
    
    header = CreateHeaderAndFileAllocTable(CurrentPath);
    temp = open('temp.bin', 'wb')
    #Write Header to binary file

    #load module id
    load_module_id=[format(header.load_module_id >> i & 0xff,'x') for i in (0,8)]
    for i in load_module_id:
        if len(i) == 1:
            i='0'+i
        temp.write(binascii.unhexlify(i))     
       
    #system id
    file_sys_id=[format(header.file_sys_id >> i & 0xff,'x') for i in (0,8)]
    for i in file_sys_id:
        if len(i) == 1:
            i='0'+i
        temp.write(binascii.unhexlify(i))

    #file content id
    file_content_id=[format(header.file_content_id >> i & 0xff,'x') for i in (0,8)]
    for i in file_content_id:
        if len(i) == 1:
            i='0'+i
        temp.write(binascii.unhexlify(i))     
   
    #file system version 24 bytes
    temp.write('{:{}<24}'.format(header.file_sys_ver, b'\00'))

    #Data blob size
    data_blob_size=[format(header.data_blob_size >> i & 0xff,'x') for i in (0,8,16,24)]
    for i in data_blob_size:
        if len(i) == 1:
            i='0'+i
        temp.write(binascii.unhexlify(i))     
    #Write File Allo cation Table Entries to binary file
    for file in FileAllocTableList:

        #write file name, 32 bytes
        temp.write('{:{}<32}'.format(file.file_name, b'\00'))
        print file.file_start_addr
        
        #write file start address , 4 bytes
        start_addr=[format(file.file_start_addr >> i & 0xff,'x') for i in (0,8,16,24)]
        for i in start_addr:
            if len(i) == 1:
                i='0'+i
            temp.write(binascii.unhexlify(i))
        
        #write file size , 4 bytes        
        file_size=[format(file.file_size >> i & 0xff,'x') for i in (0,8,16,24)]
        for i in file_size:
            if len(i) == 1:
                i='0'+i
            temp.write(binascii.unhexlify(i))
        
        #write file type , 2 bytes            
        file_size=[format(file.file_type >> i & 0xff,'x') for i in (0,8)]
        for i in file_size:
            if len(i) == 1:
                i='0'+i
            temp.write(binascii.unhexlify(i))
        
    for file in FileAllocTableList:
        print CurrentPath + '\\' + file.file_name.replace("/","\\")
        src = open((CurrentPath+'\\' + file.file_name.replace("/","\\")),'rb')        
        #shutil.copyfileobj((open(CurrentPath+'/'+ file.file_name),'rb'),temp)
        shutil.copyfileobj(src,temp)
    temp.close()    
    
    #create fs.bin
    fs = open('fs.bin', 'wb')
    temp = open('temp.bin', 'rb')
    crc32_raw = binascii.crc32(temp.read())
    header.crc32 = crc32_raw
    temp.close()
    temp = open('temp.bin', 'rb')    
    
    #write CRC value to fs.bin
    crc32=[format(crc32_raw >> i & 0xff,'x') for i in (0,8,16,24)]
    for i in crc32:
        if len(i) == 1:
            i='0'+i
        fs.write(binascii.unhexlify(i))    
    #print crc32
    #Merge fs.bin and temp.bin
    shutil.copyfileobj(temp,fs)
    temp.close()
    os.remove('temp.bin')
    fs.close()
    
    #generate report file
    report = open('report.log', 'w')
    report.write(header.display())
    #
    report.write('Total File Number :' + str(len(FileAllocTableList))+'\n')
    index = 1
    for file in FileAllocTableList:
        report.write('File Number '+ str(index) + '\n')
        report.write(file.display())
        index = index + 1
    for file in FileAllocTableList:
        report.write(file.file_name + '\n')
    report.close()      
if __name__ == "__main__":
    main()              