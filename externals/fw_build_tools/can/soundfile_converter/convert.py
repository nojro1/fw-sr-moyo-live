#This script is developed and tested in python 2.7
#Author : Wei Liao
#Data : 07/03/2017
#Ver : 1.0

#==============================================================================================================
#                                      READ ME   
#This script is to covert 16 bit sound file to 12 bit sound file.
#
#How to use:
#1.Create a new folder, and put this script in
#2.Drag .raw files you want to covert in the new created folder. RAW file can be in the new folder or in subfolders
#3.Execute this script
#4.Generated 12 bit .SND files will be in "Modified" folder
import ConfigParser
import os
import shutil
import binascii
import array
import struct
#Current directory path 
CurrentPath = os.path.abspath('.')

#file allocation allocation list
FileAllocTableList = []

Config = ConfigParser.ConfigParser()

class FileAllocTable:
    def __init__(self,file_name):
        self.file_name = file_name         
    
def CreateHeaderAndFileAllocTable(rootDir):

    for root, dirs, files in os.walk(rootDir):
        for name in files:
            if name.endswith("raw"):
                relpath = os.path.relpath(root,CurrentPath)
                if not os.path.exists(CurrentPath+'/'+'modified'+'/'+relpath.replace(' ','_')):
                    os.makedirs(CurrentPath+'/'+'modified'+'/'+relpath.replace(' ','_'))
                FileAllocTableList.append(FileAllocTable(os.path.relpath(os.path.join(root, name))))                
    
def main():
    CreateHeaderAndFileAllocTable(CurrentPath);
    #generate 12 bit sound files
    for file in FileAllocTableList:
        #src = open((CurrentPath+'/'+ file.file_name),'rb')        
        #shutil.copyfileobj((open(CurrentPath+'/'+ file.file_name),'rb'),temp)
        #dec = open(('modified'+'/'+CurrentPath+'/'+ file.file_name),'wb')
        with open((CurrentPath+'/'+ file.file_name),'rb') as src:
            rawdata = src.read(2)
            #data = struct.unpack('h', rawdata)[0]>>4
            des = open((CurrentPath+ '/' + 'modified' + '/' + file.file_name.replace(' ','_').replace('.raw','.snd')),'wb')
            print (CurrentPath+ '/' + 'modified' + '/' + file.file_name.replace(' ','_').replace('.raw','.snd'))
            #dec.write(data)
            #dec.write(struct.pack('H', data))
            #print data
            while rawdata != "":
            # Do stuff with byte.
                data = struct.unpack('h', rawdata)[0]>>4
                if data > 0x800:
                    data = 0x800 - (0xfff - data)
                else:
                    data = data + 0x800                
                des.write(struct.pack('h', data)) #convert 16bit to 12bit
                rawdata = src.read(2)
            des.close()                
if __name__ == "__main__":
    main()              