﻿
param (
    [Parameter(Mandatory=$True)]
    [string]$InFile,
    [Parameter(Mandatory=$False)]
    [switch]$Production
)

$currentFolder = $pwd
cd $PSScriptRoot
$hexFile = "$currentFolder\$InFile"

$BoardName_Suffix = "_development"
$extraarg = "-notPCAProduction"
if ($Production)
{
    $BoardName_Suffix = "_production"
    $extraarg = ""
}


#Pulse small
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-11422 -BoardfamilyId 22 -CANLibId 1 -ApplicationID 1 -BoardName PulseSmall$BoardName_Suffix $extraarg

#Pulse 
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-10692 -BoardfamilyId 1 -CANLibId 1 -ApplicationID 1  -BoardName Pulse$BoardName_Suffix $extraarg

#Audio
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-10688 -BoardfamilyId 2 -CANLibId 1 -ApplicationID 2  -BoardName Audio$BoardName_Suffix $extraarg -filesystemcontentId 8
#Audio (Interim board for ECBaby)
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-14218 -BoardfamilyId 2 -CANLibId 1 -ApplicationID 2  -BoardName Audio2$BoardName_Suffix $extraarg -filesystemcontentId 8

#Audio Ausc Board (Intended to replace 20-10688 and 20-14218)
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-16006 -BoardfamilyId 28 -CANLibId 1 -BoardName AudioAuscultation$BoardName_Suffix $extraarg -filesystemcontentId 8

#RA CPR Interface Board
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-12629 -BoardfamilyId 3 -CANLibId 1 -ApplicationID 23  -BoardName RACPRInterface$BoardName_Suffix $extraarg -nodeId 11

#String Pot Board
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-14173 -BoardfamilyId 5 -CANLibId 1 -ApplicationID 29  -BoardName StringPotCPR$BoardName_Suffix $extraarg -nodeId 11

#Laerdal Link
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-10408 -BoardfamilyId 4 -CANLibId 1 -BoardName LaerdalLink$BoardName_Suffix $extraarg -nodeId 10

#ECG Leads
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-11346 -BoardfamilyId 6 -CANLibId 1 -ApplicationID 6  -BoardName ECGLeads$BoardName_Suffix $extraarg -nodeId 100

#CPR
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-10863 -BoardfamilyId 8 -CANLibId 1 -ApplicationID 8  -BoardName CPR$BoardName_Suffix $extraarg -nodeId 11

#Piezo Valve
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-11099 -BoardfamilyId 10 -CANLibId 1 -ApplicationID 10  -hwRevision B -BoardName PiezoValve$BoardName_Suffix $extraarg

#Motor (without connector for motor chestrise)
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-11418 -BoardfamilyId 11 -CANLibId 1 -ApplicationID 11 -BoardName MotorChestrize$BoardName_Suffix $extraarg
#Motor (with connector for movement head a.k.a. head seizure)
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-14801 -BoardfamilyId 11 -CANLibId 1 -ApplicationID 26 -BoardName MotorMovementHead$BoardName_Suffix $extraarg

#SimBaby Bladders
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-11620 -BoardfamilyId 12 -CANLibId 1 -ApplicationID 12  -BoardName SimBabyBladders$BoardName_Suffix $extraarg -nodeId 70

#Servo
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-11618 -BoardfamilyId 13 -CANLibId 1 -ApplicationID 15  -BoardName Servo$BoardName_Suffix $extraarg

#Pressure
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-10694 -BoardfamilyId 14 -CANLibId 1  -BoardName Pressure$BoardName_Suffix $extraarg

#Compressor
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-11832 -BoardfamilyId 15 -CANLibId 1  -ApplicationID 19 -BoardName Compressor$BoardName_Suffix $extraarg -nodeId 110

#Power LiIon
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-12100 -BoardfamilyId 16 -CANLibId 1  -ApplicationID 20 -nodeId 2 -BoardName PowerLiIon$BoardName_Suffix $extraarg

#Power NiMH
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-12101 -BoardfamilyId 17 -CANLibId 1  -ApplicationID 20 -nodeId 2 -BoardName PowerNiMH$BoardName_Suffix $extraarg

#Motor And Pressure
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-12335 -BoardfamilyId 18 -CANLibId 1  -ApplicationID 21 -BoardName MotorAndPressure$BoardName_Suffix $extraarg

#Eye
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-12073 -BoardfamilyId 19 -CANLibId 1  -ApplicationID 22 -BoardName Eye$BoardName_Suffix $extraarg

#DiffPressure
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-11428 -BoardfamilyId 20 -CANLibId 1  -BoardName DiffPressure$BoardName_Suffix $extraarg

#DiffPressure_6kPa
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-15584 -BoardfamilyId 27 -CANLibId 1  -BoardName DiffPressure6kpa$BoardName_Suffix $extraarg

#GPIO Small
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-11979 -BoardfamilyId 21 -CANLibId 1  -BoardName GPIOsmall$BoardName_Suffix $extraarg

#Single lx-valve
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-13059 -BoardfamilyId 23 -CANLibId 1  -ApplicationId 27 -nodeId 101 -BoardName single-lx-valve$BoardName_Suffix $extraarg

#dual x-valve
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-13057 -BoardfamilyId 25 -CANLibId 1  -BoardName dual-x-valve$BoardName_Suffix $extraarg

#Resusci Power Board
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-13055 -BoardfamilyId 24 -CANLibId 1  -ApplicationID 28 -nodeId 2 -BoardName PowerResusci$BoardName_Suffix $extraarg

#Disc pump
powershell -Command .\make_board_specific_bootloader.ps1 -InFile $hexFile -Agileno 20-11426 -BoardfamilyId 26 -CANLibId 1  -BoardName DiscPump$BoardName_Suffix $extraarg

#Rename items to Att 1 for Master Medias and create md5-files if Production switch is set
if ($Production)
{

    #The following boards only use bootloader in production
    $filepath = [System.IO.Path]::GetDirectoryName($hexFile)

    $org_name = $hexFile.ToString().Replace(".hex"," 20-11418_MotorChestrize_production.hex")
    Rename-Item -Path "$org_name" -NewName "Att 1 to 00048487.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\Att 1 to 00048487.hex" -DestinationPath "$filepath\Att 2 to 00048487.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-10694_Pressure_production.hex")
    Rename-Item -Path "$org_name" -NewName "Att 1 to 00048591.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\Att 1 to 00048591.hex" -DestinationPath "$filepath\Att 2 to 00048591.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-11428_DiffPressure_production.hex")
    Rename-Item -Path "$org_name" -NewName "Att 1 to 00048592.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\Att 1 to 00048592.hex" -DestinationPath "$filepath\Att 2 to 00048592.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-15584_DiffPressure6kpa_production.hex")
    Rename-Item -Path "$org_name" -NewName "Att 1 to 00060034.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\Att 1 to 00060034.hex" -DestinationPath "$filepath\Att 2 to 00060034.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-11979_GPIOsmall_production.hex")
    Rename-Item -Path "$org_name" -NewName "Att 1 to 00048871.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\Att 1 to 00048871.hex" -DestinationPath "$filepath\Att 2 to 00048871.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-11426_DiscPump_production.hex")
    Rename-Item -Path "$org_name" -NewName "Att 1 to 00058839.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\Att 1 to 00058839.hex" -DestinationPath "$filepath\Att 2 to 00058839.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-13057_dual-x-valve_production.hex")
    Rename-Item -Path "$org_name" -NewName "Att 1 to 00049576.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\Att 1 to 00049576.hex" -DestinationPath "$filepath\Att 2 to 00049576.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-16006_AudioAuscultation_production.hex")
    Rename-Item -Path "$org_name" -NewName "Att 1 to 21-00003.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\Att 1 to 21-00003.hex" -DestinationPath "$filepath\Att 2 to 21-00003.md5"

    #The following hex files need to be merged with lib and application before being put in agile
    $org_name = $hexFile.ToString().Replace(".hex"," 20-14801_MotorMovementHead_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00057197.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00057197.hex" -DestinationPath "$filepath\boot for Att 2 to 00057197.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-13059_single-lx-valve_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00049574.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00049574.hex" -DestinationPath "$filepath\boot for Att 2 to 00049574.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-11422_PulseSmall_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00048855.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00048855.hex" -DestinationPath "$filepath\boot for Att 2 to 00048855.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-10692_Pulse_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00047864.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00047864.hex" -DestinationPath "$filepath\boot for Att 2 to 00047864.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-10688_Audio_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00047369.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00047369.hex" -DestinationPath "$filepath\boot for Att 2 to 00047369.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-14218_Audio2_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00054039.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00054039.hex" -DestinationPath "$filepath\boot for Att 2 to 00054039.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-12629_RACPRInterface_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00049230.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00049230.hex" -DestinationPath "$filepath\boot for Att 2 to 00049230.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-10408_LaerdalLink_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00047855.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00047855.hex" -DestinationPath "$filepath\boot for Att 2 to 00047855.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-11346_ECGLeads_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00048039.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00048039.hex" -DestinationPath "$filepath\boot for Att 2 to 00048039.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-10863_CPR_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00047257.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00047257.hex" -DestinationPath "$filepath\boot for Att 2 to 00047257.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-11618_Servo_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00048886.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00048886.hex" -DestinationPath "$filepath\boot for Att 2 to 00048886.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-11832_Compressor_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00048620.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00048620.hex" -DestinationPath "$filepath\boot for Att 2 to 00048620.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-12335_MotorAndPressure_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00048831.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00048831.hex" -DestinationPath "$filepath\boot for Att 2 to 00048831.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-12073_Eye_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00048977.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00048977.hex" -DestinationPath "$filepath\boot for Att 2 to 00048977.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-12100_PowerLiIon_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00049575.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00049575.hex" -DestinationPath "$filepath\boot for Att 2 to 00049575.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-13055_PowerResusci_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00050682.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00050682.hex" -DestinationPath "$filepath\boot for Att 2 to 00050682.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-12101_PowerNiMH_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00051319.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00051319.hex" -DestinationPath "$filepath\boot for Att 2 to 00051319.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-11620_SimBabyBladders_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00050971.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00050971.hex" -DestinationPath "$filepath\boot for Att 2 to 00050971.md5"

    $org_name = $hexFile.ToString().Replace(".hex"," 20-14173_StringPotCPR_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to 00054595.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to 00054595.hex" -DestinationPath "$filepath\boot for Att 2 to 00054595.md5"

    #TODO: Create Agile master media for following
    $org_name = $hexFile.ToString().Replace(".hex"," 20-11099_PiezoValve_production.hex")
    Rename-Item -Path "$org_name" -NewName "boot for Att 1 to piezoMM.hex"
    ..\tc\create-md5sum.ps1 -Path "$filepath\boot for Att 1 to piezoMM.hex" -DestinationPath "$filepath\boot for Att 2 to piezoMM.md5"
}
cd $currentFolder
