/**
  ******************************************************************************
  * @file    Src/main.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    4-April-2016
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/** @addtogroup STM32L4xx_IAP_Main
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "memory_card.h"
#include "flash_if.h"
#include "command.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define IAP_UPLOAD_PATTERN 0xCAFEBEEF //IAP upload active

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
pFunction   JumpToApplication;
uint32_t    JumpAddress;
uint32_t  volatile  FlashProtection = 0;
const char DownloadFile[] = "Moyo.bin";
const char UploadFile[] = "Moyopload.bin";

#pragma location = "IAP_UPLOAD_ACTIVE"
__root const __no_init uint32_t __iap_upload_active;


/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void RunApplication(void);
static void Update(void);
//static void Error_Handler(void);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
    /* STM32L5xx HAL library initialization:
       - Configure the Flash prefetch
       - Systick timer is configured by default as source of time base, but user 
         can eventually implement his proper time base source (a general purpose 
         timer for example or other time source), keeping in mind that Time base 
         duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and 
         handled in milliseconds basis.
       - Low Level Initialization
     */
    HAL_Init();
    
    /* Configure LED1 and LED3 */
    BSP_LED_Init(LED4);
    BSP_LED_Init(LED5);

    /* Configure the system clock to 48 MHz */
    SystemClock_Config();
  
    const uint32_t *iap_upload_active = &__iap_upload_active;
    if(*iap_upload_active != IAP_UPLOAD_PATTERN)
    {
        //Start application if valid TODO
        RunApplication();  
    }
  
    /* Initialize Flash */
    FLASH_If_Init();

    /* Initialize CARD */
    if (Card_Init() == FR_OK)
    {
        /* Run update */
        Update();
    }
    else
    {
        Card_Unlink();
    }

    RunApplication();

    /* Infinite loop */
    while (1)
    {}
}


/**
  * @brief  Update Moyo
  * @param  None
  * @retval None
  */
static void Update(void)
{
    const uint32_t *iap_upload_active = &__iap_upload_active;
    
    if (f_open(&MyFile, DownloadFile, FA_READ) == FR_OK)
    {
        if (COMMAND_DOWNLOAD() == DOWNLOAD_OK)
        {
            /* Unlock the Flash to enable the flash control register access */
            HAL_FLASH_Unlock();
            
            /* Clear boot indication pattern (OK to set to 0 without page erase) */
            HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t)iap_upload_active, 0);
            
            /* Lock flash */
            HAL_FLASH_Lock();
    
            /* Run application */
            RunApplication();
        }
    }
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
    LL_FLASH_SetLatency(LL_FLASH_LATENCY_5);

    if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_5)
    {
        //TODO
        //Error_Handler();  
    };

    LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE0);
    LL_RCC_LSI_Enable();

    /* Wait till LSI is ready */
    while(LL_RCC_LSI_IsReady() != 1)
    {
    };

    LL_RCC_MSI_Enable();

    /* Wait till MSI is ready */
    while(LL_RCC_MSI_IsReady() != 1)
    {
    };

    LL_RCC_MSI_EnableRangeSelection();
    LL_RCC_MSI_SetRange(LL_RCC_MSIRANGE_6);
    LL_RCC_MSI_SetCalibTrimming(0);
    LL_PWR_EnableBkUpAccess();
    LL_RCC_ForceBackupDomainReset();
    LL_RCC_ReleaseBackupDomainReset();
    LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_MSI, LL_RCC_PLLM_DIV_1, 55, LL_RCC_PLLR_DIV_2);
    LL_RCC_PLL_EnableDomain_SYS();
    LL_RCC_PLL_Enable();

    /* Wait till PLL is ready */
    while(LL_RCC_PLL_IsReady() != 1)
    {
    };

    /* Intermediate AHB prescaler 2 when target frequency clock is higher than 80 MHz */
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_2);
  
    LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

    /* Wait till System clock is ready */
    while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
    {
    };

    /* Insure 1�s transition state at intermediate medium speed clock based on DWT*/
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
  

    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
    DWT->CYCCNT = 0;
    while(DWT->CYCCNT < 100);
  
    LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
    LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
    LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
    LL_SetSystemCoreClock(110000000);

    /* Update the time base */
    if (HAL_InitTick (TICK_INT_PRIORITY) != HAL_OK)
    {
        //TODO
        //Error_Handler();  
    };
  
    LL_InitTick(HAL_RCC_GetHCLKFreq(), 1000);
    //LL_InitTick(RCC_GetHCLKClockFreq(RCC_GetSystemClockFreq()), 1000);
}

/**
  * @brief  This function runs a previously loaded application
  * @param  None
  * @retval None
  */
void RunApplication(void)
{
    /* Test if user code is programmed starting from address "APPLICATION_ADDRESS" */
    if (((*(__IO uint32_t*)APPLICATION_ADDRESS) & 0x2FFE0000 ) == 0x20000000)
    {
        __disable_irq();
    
        /* Jump to user application */
        JumpAddress = *(__IO uint32_t*) (APPLICATION_ADDRESS + 4);
        JumpToApplication = (pFunction) JumpAddress;
        /* Initialize user application's Stack Pointer */
#if   (defined ( __GNUC__ ))
        /* Compensation as the Stack Pointer is placed at the very end of RAM */
        __set_MSP((*(__IO uint32_t*) APPLICATION_ADDRESS) - 64);
#else  /* (defined  (__GNUC__ )) */
        __set_MSP(*(__IO uint32_t*) APPLICATION_ADDRESS);
#endif /* (defined  (__GNUC__ )) */

        JumpToApplication();
    } 
    else
    {
        //Error TODO
    }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
#if(0)
void Error_Handler(void)
{
  /* Turn LED3 on */
  BSP_LED_On(LED5);
  while (1)
  {}
}
#endif

/**
  * @brief  EXTI line detection callbacks.
  * @param  GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */

#ifdef USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {}
}

#endif


/**
* @}
*/


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
