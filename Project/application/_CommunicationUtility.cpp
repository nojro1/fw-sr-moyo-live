
// self
#include "CommunicationUtility.h"


//! CommunicationUtility
//! @brief Constructor
//! @param none
//! @return none
//! @remark Protected
//! @author LOL
CommunicationUtility::CommunicationUtility()
{

}

//! ~CommunicationUtility
//! @brief Destructor
//! @param none
//! @return none
//! @remark Protected
//! @author LOL
CommunicationUtility::~CommunicationUtility()
{

}

//! split
//! @brief Method to split a 16-bit unsigned value into 2 x 8-bit values
//! @param value 16-bit value to split
//! @return resulting pair - first = MSB, second = LSB
//! @remark Unsigned version
//! @author LOL
pair< uint8_t, uint8_t > CommunicationUtility::split( uint16_t value )
{
    // resulting pair - first = MSB, second = LSB
    pair< uint8_t, uint8_t > result( highByte(value), lowByte(value) );

    // done
    return result;
}

//! split
//! @brief Method to split a 16-bit unsigned value into 2 x 8-bit values
//!        (optimized 16-8 split method, uses return value optimization)
//! @param value 16-bit value to split
//! @param result Reference to result, a pair of bytes (result.First = MSB)
//! @return none
//! @remark Unsigned version
//! @author LOL
void CommunicationUtility::split( uint16_t value, pair< uint8_t, uint8_t >& result )
{
    // set MSB
    result.first  = ( highByte(value) );
    // set LSB
    result.second = ( lowByte(value) );
}

//! split
//! @brief Method to split a 32 bit unsigned value into 2 x 16 bit values
//! @param value 32-bit value to split
//! @return resulting pair - first = MS word, second = LS word
//! @remark Unsigned version
//! @author LOL
pair< uint16_t, uint16_t > CommunicationUtility::split( uint32_t value )
{
    // resulting pair - first = MSW, second = LSW
    pair< uint16_t, uint16_t > result( static_cast<uint16_t>( ( value & 0xFFFF0000 ) >> 16 ),
                                       static_cast<uint16_t>( ( value & 0x0000FFFF ) >>  0 ));

    // done
    return result;
}

//! breakIntoBytes
//! @brief Method to insert a 32 bit unsigned value into a 8 bit vector, LSB first
//! @param value 32-bit unsigned value to insert
//! @param buffer reference to the vector to insert into
//! @return none
//! @remark Unsigned version
//! @author GIT
void CommunicationUtility::insertBytes(uint32_t value, vector< uint8_t > & buffer)
{
    // add the bytes in order
    buffer.push_back( byte_0(value) );
    buffer.push_back( byte_1(value) );
    buffer.push_back( byte_2(value) );
    buffer.push_back( byte_3(value) );
}


//! breakIntoBytes
//! @brief Method to break a 32 bit unsigned value into 4 bytes
//! @param value 32-bit unsigned value to split
//! @return resulting vector - result[0] = MSB, result [3] = LSB
//! @remark Unsigned version. Has side effect of memory defragmentation (auto vector), so use only at startup
//! @author LOL
vector< uint8_t > CommunicationUtility::breakIntoBytes( uint32_t value )
{
    // resulting vector - result[0] = MSB, result [3] = LSB
    vector< uint8_t > result;
    result.reserve(4);

    // add the bytes in order
    result.push_back( byte_3(value) );
    result.push_back( byte_2(value) );
    result.push_back( byte_1(value) );
    result.push_back( byte_0(value) );

    // done
    return result;
}

//! toByte
//! @brief Method to convert bitset<8> --> uint8_t
//! @param bits Reference to a bitset<8>
//! @return uint8_t that represents the 8-bit value of the bitset
//! @remark Unsigned version
//! @author LOL
uint8_t CommunicationUtility::toByte( const bitset<8>& bits )
{
    return( static_cast<uint8_t>( bits.to_ulong() ) );
}

//! combine
//! @brief Method to combine 2 x 8-bit unsigned values into a 16 bit unsigned value
//! @param byteMsb Most significant byte
//! @param byteLsb Least significant byte
//! @remark Unsigned version
//! @return uint16_t that represents the combined value
uint16_t CommunicationUtility::combine( uint8_t byteMsb, uint8_t byteLsb )
{
    uint16_t result = byteLsb, tempOne = byteMsb;
    return ( result | static_cast<uint16_t>( tempOne << 8 ) );
}

//! combine
//! @brief Method to combine 4 x 8 bit values into a 32 bit value
//! @param msbA Most significant byte in the 32 bit value
//! @param B    Second byte
//! @param C    Third byte
//! @param lsbD Least significant byte in the 32 bit value
//! @return uint32_t that represents the combined value
//! @remark Unsigned version
//! @author LOL
uint32_t CommunicationUtility::combine( uint8_t msbA, uint8_t B, uint8_t C, uint8_t lsbD )
{
    uint16_t AB = combine( msbA, B );
    uint16_t CD = combine( C, lsbD );
    uint32_t result = CD, tempAB = AB;
    return( result | ( tempAB << 16 ) );
}

