
#ifndef COMMUNICATION_UTILITY_H
#define COMMUNICATION_UTILITY_H

    //! @class CommunicationUtility
    //! @brief Utility class that contains useful manipulation functions
    //!
    //! @author Lasse Olsen
    //! @date 22.03.2006
    //!

    #include <utility>
    #include <vector>
#ifdef __embedded_cplusplus
    #include <bitset> // need to include our own bitset, since IAR compiler in embedded C++ mode does not have <bitset>
#else
    #include <bitset>
#endif
    #include "common.h"

    #include "defs.h"

    //namespace bls_2010
    //{
        class CommunicationUtility
        {
        public:

            // get MSB of a 32 bit value
            inline static uint8_t byte_3(uint32_t value)
            {
                return ( static_cast<uint8_t>( ( value & 0xFF000000u ) >> 24 ) );
            }

            // get byte#3 (next to MSB) of a 32 bit value
            inline static uint8_t byte_2(uint32_t value)
            {
                return ( static_cast<uint8_t>( ( value & 0x00FF0000u ) >> 16 ) );
            }

            // get byte#2 (next to LSB) of a 32 bit value
            inline static uint8_t byte_1(uint32_t value)
            {
                return ( static_cast<uint8_t>( ( value & 0x0000FF00u ) >>  8 ) );
            }

            // get LSB of a 32 bit value
            inline static uint8_t byte_0(uint32_t value)
            {
                return ( static_cast<uint8_t>( value & 0x000000FFu ) );
            }


            inline static uint8_t highByte(uint16_t value)
            {
                return (static_cast<uint8_t>( ( value & 0xFF00u ) >> 8));
            }

            inline static uint8_t lowByte(uint16_t value)
            {
                return (static_cast<uint8_t>(value & 0x00FFu));
            }


            // unsigned versions

            // method to split a 16-bit value into 2 x 8-bit values
            static std::pair< uint8_t, uint8_t > split( uint16_t value );

            // optimized 16-8 split method, uses return value optimization
            static void split( uint16_t value, std::pair< uint8_t, uint8_t >& result );

            // method to split a 32 bit value into 2 x 16 bit values
            static std::pair< uint16_t, uint16_t > split( uint32_t value );

            // method to break a 32 bit value into 4 bytes
            static std::vector< uint8_t > breakIntoBytes( uint32_t value );

            // Method to insert a 32 bit unsigned value into a 8 bit vector, LSB first
            static void insertBytes(uint32_t value, std::vector< uint8_t > & buffer);

            // method to convert bitset<8> --> uint8_t
            static uint8_t toByte( const std::bitset<8>& bits );

            // method to combine 2 x 8-bit values into a 16 bit value
            static uint16_t combine( uint8_t byteMsb, uint8_t byteLsb );

            // method to combine 4 x 8 bit values into a 32 bit value
            static uint32_t combine( uint8_t msbA, uint8_t B, uint8_t C, uint8_t lsbD );



            ///////// signed versions //////////


            // method to split a 16-bit value into 2 x 8-bit values
            inline static std::pair< uint8_t, uint8_t > split( int16_t value )
            {
                return split( static_cast<uint16_t>(value) );
            }

            // optimized 16-8 split method, uses return value optimization
            inline static void split( int16_t value, std::pair< uint8_t, uint8_t >& result )
            {
                split( static_cast<uint16_t>(value), result );
            }

            // method to break a 32 bit value into 4 bytes
            inline static std::vector< uint8_t > breakIntoBytes( int32_t value )
            {
                return breakIntoBytes( static_cast<uint32_t>(value) );
            }

            // Method to insert a 32 bit value into a 8 bit vector, LSB first
            inline static void insertBytes(int32_t value, std::vector< uint8_t > & buffer)
            {
                insertBytes(static_cast<uint32_t>(value), buffer);
            }


            // method to convert bitset<8> --> int8_t
            inline static int8_t toSignedByte( const std::bitset<8>& bits )
            {
                return static_cast<int8_t>( toByte(bits) );
            }

            // method to combine 2 x 8-bit values into a 16 bit value
            inline static int16_t combineToSigned( uint8_t byteMsb, uint8_t byteLsb )
            {
                return static_cast<int16_t>( combine( byteMsb, byteLsb ) );
            }

            // method to combine 4 x 8 bit values into a 32 bit value
            inline static int32_t combineToSigned( uint8_t msbA, uint8_t B, uint8_t C, uint8_t lsbD )
            {
                return static_cast<int32_t>( combine( msbA, B, C, lsbD ) );
            }

        protected:

            // constructor and destructor are protected
            CommunicationUtility();
            ~CommunicationUtility();

        };

    //} // end namespace

#endif
