//! @class  ADThread
//! @brief
//! @author Jan Arild R�yneberg
//! @date   28.02.2014
//!

#ifndef _AD_THREAD_H_
#define _AD_THREAD_H_

#include "board_cfg.h"
#include "thread.h"
#include "includes.h"
#include "adc.h"
#include "qrsdetector.h"
#include "ringbuffer.h"
#include "ecgbuffer.h"
#include "configuration.h"
#include "iirfilter.h"

//! @brief Handles the Analog in system
class ADThread:public Thread
{
    
public:
	ADThread();
	~ADThread();
	void run(void *);
    static ADThread* get() { return m_pAdTh; }
    uint16_t GetECGFileData(EcgBuffer::ECGrecord_t *buf, uint16_t noSamples);
    uint8_t GetMaternalHR(void);
    
    
    
protected:

private:
    static const uint16_t THREAD_TICK_TIME = 100;
    static const uint16_t AD_DEBUG_TIME = 100/THREAD_TICK_TIME;
    
    static ADThread* m_pAdTh;  //!< static member to keep track of instance.
    
    static const uint8_t ELECTRODES_TOUCHED_CHECK_TIME = 500/THREAD_TICK_TIME;
    static const uint8_t ELECTRODES_NOT_TOUCHED_CHECK_TIME = 1000/THREAD_TICK_TIME;
    
    uint16_t m_dbgCnt;
    bool m_debugADC;
    QrsDetector* m_QrsDetector;
    RingBuffer m_AdcEcgSamplesBuffer;              //!< ADC samples ringbuffer
    EcgBuffer m_AdcEcgFileSamplesBuffer;           //!< ADC samples for file ringbuffer
    RingBuffer mAdcEcgRawBuffer;                   //!< ADC ecg raw samples ringbuffer
    uint16_t m_HRmother;
    bool m_ecgElectrodesTouched;
    uint8_t m_electrodesTimer;
    uint16_t m_maternalHrDispTimer;
    uint16_t m_maternalHrDispTime;
    OperationalMode_t m_operationalMode;
    IIRfilter* m_iirFilter;
    
    //Average impedance value test
    uint32_t m_impAvg; //Average impedance value test
    uint16_t m_impAvgRemaining;
    uint16_t m_impAvgNoSamples;
    static const uint16_t MAX_MEASUREMENT_TIME = 10000; //ms
        
    void maternalHRhandler(void);
    void maternalECGtestHandler(void);
    void impedanceAvgMeasurement(void);
    void initAvgImpedanceMeasurement(uint16_t a_measurementTimeMs);
    void IIRfilterInit( float *inSamp, float *outSamp, uint16_t length );
    static void TMR_ADCsample_ISR(void);
    
};

#endif
