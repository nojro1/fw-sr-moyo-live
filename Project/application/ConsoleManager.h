
#ifndef CONSOLEMANAGER_H
#define CONSOLEMANAGER_H

#include <vector>
#include <string>
#include "EmulatedEEprom.h"
#include "configuration.h"
#include "bq27510.h"
#include <mallocstats.h>
#include "FHRMprobeSerial.h"
#include "th1super.h"
#include "filethread.h"
#include "uithread.h"

class ConsoleThread;

//! @brief Performs parsing of console commands
class ConsoleManager
{
public:

    explicit ConsoleManager(ConsoleThread * pConTh);
    virtual ~ConsoleManager();

    void parseConsoleCommand(std::string &commandString);

protected:

private:

    void setupArguments(const std::string & commandString);

    void parseSysCommand();
    void displayCPUload();
    void parseAudioCommand();
    void parseDisplayCommand();
    void parsePowerCommand();
    void parseBatteryCommand();
    void parseAdcCommand();
    void parseRTCcommand();
    void parseFileTestCommand();
    void parseECGCommand();
    void parseCFcommand();
    void parseTestCommand();
    void enterBootloader();
    void waitForKeypress() const;
    bool keypress() const;

    bool hasNextArgument() const;

    std::string getNextArgument();

    std::string getCurrentArgument() const;

    bool argumentStartsWith( const std::string &expected ) const;

    void getNextOption();

    bool isOption( const std::string &shortName ) const;

    std::string getNextParameter();

    std::string getNextOptionalParameter();

    void fail( const std::string& message ) const;

    int getNextOptionalNumericParameter(int minVal, int maxVal);

#ifdef HEAP_USAGE_SUPPORT
    void printHeapUsage(void);
#endif
    
    ConsoleThread * m_pConsoleThread;

    typedef std::vector<std::string> Arguments;
    Arguments m_arguments;
    std::string m_previousCommands;
    unsigned int m_currentArgument;

    std::string m_option;
    
    OperationalMode_t m_operationalMode;
    
};


#endif // CONSOLEMANAGER_H


