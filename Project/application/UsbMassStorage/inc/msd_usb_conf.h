/******************** (C) COPYRIGHT 2009 STMicroelectronics ********************
* File Name          : msd_usb_conf.h
* Author             : MCD Application Team
* Version            : V2.0.0
* Date               : 04/27/2009
* Description        : Mass Storage Demo configuration  header
********************************************************************************
* THE PRESENT SOFTWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH SOFTWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MSD_USB_CONF_H
#define __MSD_USB_CONF_H

/*-------------------------------------------------------------*/
/* EP_NUM */
/* defines how many endpoints are used by the device */
/*-------------------------------------------------------------*/
#define MASS_EP_NUM              (3)

/*-------------------------------------------------------------*/
/* --------------   Buffer Description Table  -----------------*/
/*-------------------------------------------------------------*/
/* buffer table base address */

#define MASS_BTABLE_ADDRESS      (0x00)

/* EP0  */
/* rx/tx buffer base address */
#define MASS_ENDP0_RXADDR        (0x18)
#define MASS_ENDP0_TXADDR        (0x58)

/* EP1  */
/* tx buffer base address */
#define MASS_ENDP1_TXADDR        (0x98)

/* EP2  */
/* Rx buffer base address */
#define MASS_ENDP2_RXADDR        (0xD8)


/* ISTR events */
/* IMR_MSK */
/* mask defining which events has to be handled */
/* by the device application software */
#define MASS_IMR_MSK (CNTR_CTRM  | CNTR_RESETM | ISTR_SUSP | ISTR_WKUP)

/* CTR service routines */
/* associated to defined endpoints */
//#define  MASS_EP1_IN_Callback   NOP_Process
#define  MASS_EP2_IN_Callback   NOP_Process
#define  MASS_EP3_IN_Callback   NOP_Process
#define  MASS_EP4_IN_Callback   NOP_Process
#define  MASS_EP5_IN_Callback   NOP_Process
#define  MASS_EP6_IN_Callback   NOP_Process
#define  MASS_EP7_IN_Callback   NOP_Process


#define  MASS_EP1_OUT_Callback   NOP_Process
//#define  MASS_EP2_OUT_Callback   NOP_Process
#define  MASS_EP3_OUT_Callback  NOP_Process
#define  MASS_EP4_OUT_Callback   NOP_Process
#define  MASS_EP5_OUT_Callback   NOP_Process
#define  MASS_EP6_OUT_Callback   NOP_Process
#define  MASS_EP7_OUT_Callback   NOP_Process

#endif /* __MSD_USB_CONF_H */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
