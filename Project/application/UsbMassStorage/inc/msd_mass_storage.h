/******************** (C) COPYRIGHT 2009 STMicroelectronics ********************
* File Name          : mass_storage.h
* Author             : MCD Application Team
* Version            : V2.0.0
* Date               : 04/27/2009
* Description        : This file contains all the functions prototypes for the
*                      Mass Storage firmware driver.
********************************************************************************
* THE PRESENT SOFTWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH SOFTWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MSD_MASS_STORAGE_H
#define __MSD_MASS_STORAGE_H

/* Includes ------------------------------------------------------------------*/
#include "stm32l5xx.h"
#include "usb_lib.h"
#include "usb_istr.h"
#include "usb_pwr.h"
#include "hw_config.h"
#include "msd.h"
#include "usb_init.h"
#include "RTOS.h"


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void MASS_USB_LP_isr(void);
void MASS_USB_HP_isr(void);
void MASS_USB_Istr(void);
void MASS_CTR_HP(void);

#endif /* __MASS_STORAGE_H */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
