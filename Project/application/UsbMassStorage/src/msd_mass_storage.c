/******************** (C) COPYRIGHT 2009 STMicroelectronics ********************
* File Name          : mass_storage.c
* Author             : MCD Application Team
* Version            : V2.0.0
* Date               : 04/27/2009
* Description        : This file provides a set of functions needed to manage the
*                      communication between the STM32F10x USB and the MSD.
********************************************************************************
* THE PRESENT SOFTWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH SOFTWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "msd_mass_storage.h"
#include "msd_hw_config.h"
#include "msd_usb_prop.h"
#include "msd_usb_conf.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/*******************************************************************************
* Function Name  : Mass_Storage_Init
* Description    : Initializes the peripherals used by the mass storage driver.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void Mass_Storage_Init(void)
{
    /* Disable the Pull-Up*/
    MASS_USB_Cable_Config(DISABLE);
    
    //User_Standard_Requests is shared with Virtual COM port. These requests differs between
    //a Mass storage device and a Virtual COM port. We need to have this structure external here,
    //and set the functions pointers here. Virtual COM port contains the actual struct, and is initialized at creation
    User_Standard_Requests.User_GetConfiguration = Mass_Storage_GetConfiguration;
    User_Standard_Requests.User_SetConfiguration = Mass_Storage_SetConfiguration;
    User_Standard_Requests.User_GetInterface = Mass_Storage_GetInterface;
    User_Standard_Requests.User_SetInterface = Mass_Storage_SetInterface;
    User_Standard_Requests.User_GetStatus = Mass_Storage_GetStatus;
    User_Standard_Requests.User_ClearFeature = Mass_Storage_ClearFeature;
    User_Standard_Requests.User_SetEndPointFeature = Mass_Storage_SetEndPointFeature;
    User_Standard_Requests.User_SetDeviceFeature = Mass_Storage_SetDeviceFeature;
    User_Standard_Requests.User_SetDeviceAddress = Mass_Storage_SetDeviceAddress;

    Device_Property.Init = MASS_init;
    Device_Property.Reset = MASS_Reset;
    Device_Property.Process_Status_IN = MASS_Status_In;
    Device_Property.Process_Status_OUT = MASS_Status_Out;
    Device_Property.Class_Data_Setup = MASS_Data_Setup;
    Device_Property.Class_NoData_Setup = MASS_NoData_Setup;
    Device_Property.Class_Get_Interface_Setting = MASS_Get_Interface_Setting;
    Device_Property.GetDeviceDescriptor = MASS_GetDeviceDescriptor;
    Device_Property.GetConfigDescriptor = MASS_GetConfigDescriptor;
    Device_Property.GetStringDescriptor = MASS_GetStringDescriptor;
    Device_Property.RxEP_buffer = 0;
    Device_Property.MaxPacketSize = 0x40; /*MAX PACKET SIZE*/

    Device_Table.Total_Endpoint = MASS_EP_NUM;
    Device_Table.Total_Configuration = 1;
    
}

/*******************************************************************************
* Function Name  : Mass_Storage_Start
* Description    : Starts the mass storage demo.
* Input          : None
* Output         : None
* Return         : Success = true, timeout = false
*******************************************************************************/
bool Mass_Storage_Start (void)
{
  
    MASS_Get_Medium_Characteristics();

    
    /* Select USBCLK source */
    RCC_USBCLKConfig(RCC_USBCLKSource_PLLCLK_1Div5);
  
    /* Enable the USB clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, ENABLE);

    NVIC_InitTypeDef NVIC_InitStructure;
      
    /* Enable the USB_LP_CAN_RX0 Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    
    /* Enable the USB_HP_CAN_TX Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USB_HP_CAN1_TX_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    
    OS_ARM_InstallISRHandler(USB_LP_CAN1_RX0_IRQn, (OS_ISR_HANDLER*)&MASS_USB_LP_isr);
    OS_ARM_InstallISRHandler(USB_HP_CAN1_TX_IRQn, (OS_ISR_HANDLER*)&MASS_USB_HP_isr);

    /* Intialize the USB cell */
    USB_Init();
    
    if(bDeviceState != CONFIGURED)
    {
        return false;
    }
    else
    {
        return true;
    }
  
}

/*******************************************************************************
* Function Name  : Mass_Storage_Stop
* Description    : Stops the mass storage
* Input          : None
* Output         : None
* Return         : Success = true, timeout = false
*******************************************************************************/
void Mass_Storage_Stop (void)
{
    //TODO
    
    /* Disable the USB clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, DISABLE);

    NVIC_InitTypeDef NVIC_InitStructure;
      
    /* Disable the USB_LP_CAN_RX0 Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
    NVIC_Init(&NVIC_InitStructure);
    
    /* Disable the USB_HP_CAN_TX Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USB_HP_CAN1_TX_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
    NVIC_Init(&NVIC_InitStructure);
    
  
}

uint32_t GetUsbDeviceState(void)
{
    return bDeviceState;
}
/*******************************************************************************
* Function Name  : USB_MASS_HP_isr
* Description    : This function handles USB High Priority or CAN TX interrupts 
*                  requests.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void MASS_USB_HP_isr(void)
{
  MASS_CTR_HP();
}

/*******************************************************************************
* Function Name  : USB_MASS_LP_isr
* Description    : This function handles USB Low Priority or CAN RX0 interrupts 
*                  requests.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void MASS_USB_LP_isr(void)
{
  MASS_USB_Istr();
}

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
