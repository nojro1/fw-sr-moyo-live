//! @author Jan Arild R�yneberg
//! @date   30.11.2012
//!

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "includes.h"
#include "EmulatedEEprom.h"
#include "Mutex.h"
#include "CrcCCITT.h"
#include "communicationutility.h"
#include <string>

    class Config
    {
        public:
            
            Config();
            ~Config();
            
            static const uint16_t CFG_EPISODE_VALID_TIME_DEFAULT       = 900;
            static const uint16_t CFG_END_OF_EPISODE_TIME_DEFAULT      = 300;
            static const uint16_t CFG_HR_UPDATE_TIME_DEFAULT           = 2;
            static const uint16_t CFG_HR_AVG_BUFFER_LEN_DEFAULT        = 5;
            static const uint16_t CFG_LOST_FHR_SIG_ALARM_TIME_DEFAULT  = 60;
            static const uint16_t CFG_ATYPICAL_FHR_ALARM_L_DEFAULT     = 110;
            static const uint16_t CFG_ATYPICAL_FHR_ALARM_H_DEFAULT     = 160;
            static const uint16_t CFG_ATYPICAL_FHR_ALARM_TIME_DEFAULT  = 600;
            static const uint16_t CFG_ABNORMAL_FHR_ALARM_L_DEFAULT     = 100;
            static const uint16_t CFG_ABNORMAL_FHR_ALARM_H_DEFAULT     = 180;
            static const uint16_t CFG_ABNORMAL_FHR_ALARM_TIME_DEFAULT  = 180;
            static const uint16_t CFG_SHOW_CLOCK_DEFAULT               = 0;
            static const uint16_t CFG_EN_RSM_DEFAULT                   = 1;
            static const uint16_t CFG_KEEP_EPISODE_TIME_DEFAULT        = 300;
            static const float    CFG_ECG_AMP_SENSE_DEFAULT;
            static const uint16_t CFG_SD_CARD_ENABLED_DEFAULT          = 1;
            static const uint16_t CFG_DOPPLER_SPEAKER_GAIN_DEFAULT     = 2; //SW gain
            static const uint16_t CFG_DOPPLER_SOUND_GAIN_DEFAULT       = 0; //Probe HW gain
            static const uint16_t CFG_USE_FHR_FILTER_DEFAULT           = 1;

            static const uint16_t CFG_DATA_CHKSUM_ADDR = 0xfe; //uint16_t
            static const uint16_t CFG_DEV_ID_MAX_SIZE = 20; // \0 included
            static const uint16_t CFG_CHKSUM_PROT_START = 0x00; //Start of checksum protection area
            static const uint16_t CFG_CHKSUM_PROT_END = 0xdf; //End of checksum protection area
            static const uint16_t CFG_NO_CHKSUM_PROT_START = CFG_CHKSUM_PROT_END + 1; //Start of unprotected area
            static Config* getpConfig() {return m_pConfig; }
            
            enum ConfigData_t
            {
                eCfgDeviceIdentifier            = CFG_CHKSUM_PROT_START + 0x00, //char[20]
                eCfgEcgAmpSens                  = CFG_CHKSUM_PROT_START + 0x14, //uint32_t
                eCfgEpisodevalidTime            = CFG_CHKSUM_PROT_START + 0x18, //uint16_t
                eCfgEndOfEpisodeTimeout         = CFG_CHKSUM_PROT_START + 0x1A, //uint32_t
                eCfgHRupdateTime                = CFG_CHKSUM_PROT_START + 0x1E, //uint16_t
                eCfgHRavgBufLen                 = CFG_CHKSUM_PROT_START + 0x20, //uint16_t
                eCfgLostSignalAlarmTime         = CFG_CHKSUM_PROT_START + 0x22, //uint16_t
                eCfgFhrAtypicalAlarmHigh        = CFG_CHKSUM_PROT_START + 0x24, //uint16_t
                eCfgFhrAtypicalAlarmLow         = CFG_CHKSUM_PROT_START + 0x26, //uint16_t
                eCfgResearchStorageMode         = CFG_CHKSUM_PROT_START + 0x28, //uint16_t
                eCfgShowClock                   = CFG_CHKSUM_PROT_START + 0x2a, //uint16_t
                eCfgEpisodeTooShortTime         = CFG_CHKSUM_PROT_START + 0x2c, //uint16_t
                eCfgFhrAbnormalAlarmHigh        = CFG_CHKSUM_PROT_START + 0x2e, //uint16_t
                eCfgFhrAbnormalAlarmLow         = CFG_CHKSUM_PROT_START + 0x30, //uint16_t
                eCfgFhrAtypicalAlarmTime        = CFG_CHKSUM_PROT_START + 0x32, //uint16_t
                eCfgFhrAbnormalAlarmTime        = CFG_CHKSUM_PROT_START + 0x34, //uint16_t
                eCfgUseSDcard                   = CFG_CHKSUM_PROT_START + 0x36, //uint16_t
                eCfgDopplerSpeakerGain          = CFG_CHKSUM_PROT_START + 0x38, //uint16_t
                eCfgDopplerSoundGain            = CFG_CHKSUM_PROT_START + 0x3a, //uint16_t
                eCfgUseFHRfilter                = CFG_CHKSUM_PROT_START + 0x3c, //uint16_t
                
                eCfgShutdownReason              = CFG_NO_CHKSUM_PROT_START + 0x00, //uint16_t
                eCfgLastBattRemCharge           = CFG_NO_CHKSUM_PROT_START + 0x02, //uint16_t
            };
            
            uint32_t GetConfig(ConfigData_t a_CfgType);
            std::string GetConfigStr(ConfigData_t a_CfgType);
            std::string GetConfigStr(ConfigData_t a_CfgType, uint16_t a_numChars);
            float GetConfigFloat(ConfigData_t a_CfgType);
            void SaveConfig(ConfigData_t a_CfgType, uint16_t a_value);
            void SaveConfig(ConfigData_t a_CfgType, uint32_t a_value);
            void SaveConfig(ConfigData_t a_CfgType, std::string a_value);
            void SaveConfig(ConfigData_t a_CfgType, float a_value);
            bool IsValid();
            void EraseAllConfig();
            bool SetConfigDefault(void);
    
        protected:
            
        private:
            
            static Config* m_pConfig;  //!< static member to keep track of instance.
            
            Config(const Config& right);            //!< block the copy constructor
            Config& operator=(const Config& right); //!< block the assignment operator
            
            bool configLock();
            bool configUnlock();
            uint16_t GetDataCRC();
            
            
    };
    
#endif

