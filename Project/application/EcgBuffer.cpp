//! \file    EcgBuffer.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    05.11.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  05Nov2013, JRo, Original.


// include self first
#include "ecgbuffer.h"

// other includes
#include "RTOS.h"
#include <cstring>

using namespace std;

//! \brief Constructor
EcgBuffer::EcgBuffer() : m_pBuffer(0), m_pGet(0), m_pPut(0), m_pEnd(0), m_numItems(0), m_room(0)
{
}


//! \brief Destructor
EcgBuffer::~EcgBuffer()
{
    delete[] m_pBuffer;
}

void EcgBuffer::Create(uint16_t size)
{
    m_pBuffer = new ECGrecord_t[size];
    memset(m_pBuffer, 0, size * sizeof(ECGrecord_t));
    m_pGet = m_pBuffer;
    m_pPut = m_pBuffer;
    m_pEnd = m_pBuffer + size;
    
    m_numItems = 0;
    m_room = size;
    
}

bool EcgBuffer::Put(ECGrecord_t item)
{
    if(m_room)
    {
        if(m_pPut >= m_pEnd)
        {
            m_pPut = m_pBuffer;
        }
        
        *m_pPut = item;
        m_pPut++;
        
        //OS_EnterRegion();
        m_room--;
        m_numItems++;
        //OS_LeaveRegion();
        
    }
    else
    {
        return false;
    }
    
    return true;
}

EcgBuffer::ECGrecord_t EcgBuffer::Get()
{
    ECGrecord_t item;
    
    if(m_numItems)
    {
        if(m_pGet >= m_pEnd)
        {
            m_pGet = m_pBuffer;
        }
        item = *m_pGet++;
        
        //OS_EnterRegion();
        m_room++;
        m_numItems--;
        //OS_LeaveRegion();
    }
    else
    {
        item.TimeStamp = 0;
        item.Sample = 0;
    }
    
    return item;
}
                
uint16_t EcgBuffer::GetNumItems(void)
{
    return m_numItems;
}

uint16_t EcgBuffer::GetRoom(void)
{
    return m_room;
}

