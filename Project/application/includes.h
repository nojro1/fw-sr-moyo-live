#ifndef _INCLUDES_H_
#define _INCLUDES_H_

//
// Master Include File
//

//#include <cstdint>
#include <stdint.h>
//#include <cstdio>
#include <string.h>
#include <stdlib.h>
//#include <cstdbool>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>


#include <intrinsics.h>
#include "signalDefs.h"
#include "RTOS.h"
#if defined USE_LNRM_BOARD
    #include "LNRM_hw_api.h"
#elif defined USE_RA_CHEST_BOARD
    #include "chestBoard_hw_api.h"
#else
    #include "FHRM_hw_api.h"
#endif
#include "sysdefs.h"
//#include "fs.h"
//#include "fs_int.h"
//#include "utilfuncs.h"

#endif // _INCLUDES_H_

