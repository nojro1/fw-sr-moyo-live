//! \file    FhrBuffer.cpp
//! \author  Jan A. R�yneberg
//! \version 1.0
//! \date    02.04.2014

//! Buffer for keeping FHR rates and alarm states. Helper class for trend component

// include self first
#include "fhrbuffer.h"

// other includes
#include "RTOS.h"
#include <cstring>

using namespace std;


//! Constructor
FhrBuffer::FhrBuffer() : m_pBuffer(0), m_pPut(0), m_pLast(0), m_pFirst(0), m_pCurr(0), m_pEnd(0), m_bufferEmpty(true)
{
}


//! Destructor
FhrBuffer::~FhrBuffer()
{
    delete[] m_pBuffer;
}

//! Create buffer
void FhrBuffer::Create(uint16_t sizeInBytes)
{
    //OS_EnterRegion();
    
    m_pBuffer = new HrInfo_t[sizeInBytes];
    memset(m_pBuffer, 0, sizeInBytes*sizeof(HrInfo_t));
    m_pPut = m_pBuffer;
    m_pCurr = m_pBuffer;
    m_pEnd = m_pBuffer + sizeInBytes - 1;
    m_pLast = m_pBuffer;
    m_pFirst = m_pBuffer;
    
    //OS_LeaveRegion();
}

//! Add new item
bool FhrBuffer::Put(HrInfo_t item)
{
    if(m_pPut > m_pEnd)
    {
        m_pPut = m_pBuffer;
    }
    *m_pPut = item;
    m_pPut++;
    
    m_pFirst++;
    if(m_pFirst > m_pEnd)
    {
        m_pFirst = m_pBuffer;
    }
    
    if(m_pFirst == m_pLast)
    {
        m_pLast++;
        if(m_pLast > m_pEnd)
        {
            m_pLast = m_pBuffer;
        }
    }    
    
    m_bufferEmpty = false;
    
    return true;
}

//! Get last (oldest) byte
bool FhrBuffer::GetLast(HrInfo_t *item)
{
    if(m_bufferEmpty)
    {
        //Buffer empty
        return false;
    }
    
    if(m_pLast > m_pEnd)
    {
        m_pLast = m_pBuffer;
    }
    
    m_pCurr = m_pLast;
    *item = *m_pCurr;
    
    return true;
}

//! Get next byte (oldest -> first)
bool FhrBuffer::GetNext(HrInfo_t *item)
{
    if(m_bufferEmpty)
    {
        //Buffer empty
        return false;
    }
    
    m_pCurr++;
    if(m_pCurr > m_pEnd)
    {
        m_pCurr = m_pBuffer;
    }
    
    if(m_pCurr == m_pLast)
    {
        //Last entry
        return false;
    }
    
    *item = *m_pCurr;
    
    return true;
}

