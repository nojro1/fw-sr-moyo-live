//! \file    FileThread.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    02.12.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  02dec2013, JRo, Original.

#include "filethread.h"

//Test
extern "C" { int fatFsTest(void); }
uint32_t m_FileThreadTestCtr = 0;


const char * FileThread::m_VolName = "";
const char * FileThread::m_SoundFileNameExt = ".snd";
const char * FileThread::m_UtilFileNameExt = ".utl";
const char * FileThread::m_FhrFileNameExt = ".fhr";
const char * FileThread::m_EcgFileNameExt = ".ecg";
const char * FileThread::m_EventFileNameExt = ".evn";
const char * FileThread::m_SystemLogFileName = "system.log";

// static member init
FileThread*  FileThread::m_pFileTh = 0;

/*
===============================================================================
    Construction()

===============================================================================
 */
FileThread::FileThread()
{
    m_pAdcTh = 0;
    m_pSuperTh = 0;
    memset(&m_pSoundFile, 0, sizeof(m_pSoundFile));
    memset(&m_pUtilFile, 0, sizeof(m_pUtilFile));
    memset(&m_pEcgFile, 0, sizeof(m_pEcgFile));
    memset(&m_pFhrFile, 0, sizeof(m_pFhrFile));
    memset(&m_pEventFile, 0, sizeof(m_pEventFile));
    m_pFileTh = this;
    m_error = false;
    m_validEpisode = false;
    m_episodeStartTime = 0;
    m_fileWriteErrorCnt = 0;
    m_researchStorageModeEnabled = false;
    m_SDcardPresent = false;
    
    //Create the sound data buffer
    m_pSoundBuffer = new uint8_t[FileThread::SOUND_BUF_SIZE];
    
    //Create the acc data buffer
    m_pUtilBuffer = new Utilrecord_t[FileThread::UTIL_BUF_SIZE];
    
    m_pEcgBuffer = new EcgBuffer::ECGrecord_t[FileThread::ECG_BUF_SIZE];
    
    m_episodeState = eEpStWaitForStart;
    
    m_FhrRecords.reserve(Th1Super::FHR_RECORDS_BUFFER_SIZE);
    
    m_keepEpisode = false;
    
    m_POSTstatus.finished = false;
    m_POSTstatus.post.postWord = 0;
    
    m_fileWriteErrorCnt = 0;
    
    m_operationalMode = eOMconfig;
    
    m_USBstate = 0xffff;
}

/*
===============================================================================
    Destruction

    
===============================================================================
 */
FileThread::~FileThread()
{
}

/*
===============================================================================
    Process Thread

    
===============================================================================
 */
void FileThread::run(void *pmsg)
{
    
    //Startup routines here
    //Test
    /*
    while(1)                //Running with RTOS
    {
        IPC ipc;
        sigWait(FileThread::THREAD_TICK_TIME, &ipc);  //Wait for a signal or OS schedule
        m_FileThreadTestCtr++;
    }
    */
    //

    //waitUntilAllThreadsStarted();
    DEBUG_TRACE("\r\n%s Pri: %d", getThreadName(), getPriority() );

    //
    // Initialize file system
    //
    /*## FatFS: Link the disk I/O driver(s)  ###########################*/
    if (FATFS_LinkDriver(&SD_Driver, SDPath) != 0)
    {
        m_error = true;
    }
    
    
    if(BSP_SD_IsDetected(0) == SD_PRESENT)
    {
        /* Register the file system object to the FatFs module */
        if(f_mount(&m_fs, (TCHAR const*)SDPath, 0) == FR_OK)
        {
            if(!m_error)
            {
                m_SDcardPresent = true;
            }
        }
    }
    
    if(Config::getpConfig()->IsValid())
    {
        m_SDcardEnabled = Config::getpConfig()->GetConfig(Config::eCfgUseSDcard) == CFG_ENABLED ? true : false;
    }
    
    
    if(m_SDcardPresent)
    {
        uint16_t val = Config::getpConfig()->GetConfig(Config::eCfgResearchStorageMode);
        CONSOLE_PRINT("\r\nResearch storage mode: %u\r\n", val);
            
        if(val > 0)
        {
            m_researchStorageModeEnabled = true;
        }
    }
    
    if(m_SDcardPresent)
    {
        FATFS *fs;
        uint32_t totalSpace;
        uint32_t freeSpace;
        uint32_t fre_clust=0;
        uint32_t fre_sect=0;
        uint32_t tot_sect=0;
        bool success = true;
        
        do
        {
            // Print free space in unit of KB (assuming 512 bytes/sector)
            if(f_getfree(FileThread::m_VolName, reinterpret_cast<DWORD *>(&fre_clust), &fs) == FR_OK)
            {
                // Get total sectors and free sectors
                tot_sect = (fs->n_fatent - 2) * fs->csize;
                fre_sect = fre_clust * fs->csize;
            
                //Calculate space (assuming 512 bytes/sector)
                freeSpace  = fre_sect / 2;
                totalSpace = tot_sect / 2;
            
                /* Print the free space */
                CONSOLE_PRINT("\r\n%10lu kbytes total drive space.\n%10lu kbytes available.\n", totalSpace, freeSpace);
                
                if(HwAbsl::getpHw()->getStartupMode() == HwAbsl::eStartupModeNormal)
                {
                    if(freeSpace < MIN_SD_FREE_SPACE_KB)
                    {
                        success = deleteOldestEpisode();
                    }
                }
                else
                {
                    break;
                }
            }
            else
            {
                //File system error occurred
                success = false;
                CONSOLE_PRINT("\r\nFile system error!\r\n");
            }
        } while(freeSpace < MIN_SD_FREE_SPACE_KB && success);         
        
        if(!success)
        {
            CONSOLE_PRINT("\r\nFree space on SD Card is < %lu KBytes, but there are no episodes to delete\r\n", MIN_SD_FREE_SPACE_KB);
        }
        
        
        //m_pLNRM_FS->FS__SetFileWriteMode(FS_WRITEMODE_FAST);
        
    }
    
    uint32_t postResult = post();
    
    
    //Set creation file time on system log and error log file
    if(!FileExists(m_SystemLogFileName))
    {
        FIL f;
      
        if(f_open(&f, m_SystemLogFileName, FA_OPEN_APPEND | FA_WRITE) == FR_OK)
        {
            setFileTime(m_SystemLogFileName);
        }
    }
    
    m_pAdcTh = reinterpret_cast<ADThread*>(getpThread(P_ADC));
    m_pSuperTh = reinterpret_cast<Th1Super*>(getpThread(P_SUPER));


    
    while(1)                //Running with RTOS
    {
        IPC ipc;
        sigWait(FileThread::THREAD_TICK_TIME, &ipc);  //Wait for a signal or OS schedule

        m_FileThreadTestCtr++;
        
        saveToFiles();
        
		switch(ipc.m_sig)
        {
			case S_OSTIMEOUT:
                {
                    /*
                    uint32_t usbState = GetUsbDeviceState();
                    if(usbState != m_USBstate)
                    {
                        sigDispatch(P_SUPER, S_USB_STATE, usbState, 0 ,0);
                        sigDispatch(P_UI, S_USB_STATE, usbState, 0 ,0);
                        m_USBstate = usbState;
                    }
                    */
                }
                break;

            case S_FILE_CLOSE_EPISODE:
                DEBUG_TRACE("\r\nTh6File - S_FILE_CLOSE_EPISODE\r\n"); 
                CloseEpisode(ipc.m_data1, ipc.m_data2);
                m_episodeState = eEpStStopped;
                break;
    		case S_FILE_CREATE_EPISODE:
                DEBUG_TRACE("\r\nTh6File - S_FILE_CREATE_EPISODE\r\n"); 
                CreateEpisode();
                sigDispatch(P_SUPER, S_FILE_EPISODE_CREATED);
                break;
            case S_FILE_DELETE_OLDEST_EPISODE:
                deleteOldestEpisode();
                break;
            case S_FILE_LIST_EPISODES:
                listRootFiles();
                break;
            case S_FILE_READ_FILE:
            {
                FIL file;
                FRESULT fr;
                FILINFO fno;
                //char buf[5000];
                uint32_t br;
                uint32_t tot = 0;
                fr = f_stat("bigepisode_86M.tst", &fno);
                if(fr == FR_OK)
                {
                    uint32_t tstart = OS_GetTime();
                    DEBUG_TRACE("\r\nTh6File - Reading large file - %u bytes\r\n", fno.fsize); 
                    
                    fr = f_open(&file, "bigepisode_86M.tst", FA_READ);
                    
                    if(fr == FR_OK)
                    {
                        DEBUG_TRACE("\r\nTh6File - Reading...\r\n"); 
                        
                        for (;;) 
                        {
                            f_read(&file, m_pSoundBuffer, FileThread::SOUND_BUF_SIZE, &br);  /* Read a chunk of data from the source file */
                            if (br == 0) break; /* error or eof */
                            tot += FileThread::SOUND_BUF_SIZE;
                            if(tot % 1000000 == 0)
                            {
                                DEBUG_TRACE("read %u\r\n", tot); 
                            }
            
                        }

                        f_close(&file);
                        
                        DEBUG_TRACE("\r\nTh6File - Reading large file - finished\r\n"); 
                        
                        uint32_t tend = OS_GetTime();
                        DEBUG_TRACE("File size: %u bytes\r\n", fno.fsize); 
                        DEBUG_TRACE("Time to read: %u sec\r\n", (tend - tstart) / 1000); 
                        DEBUG_TRACE("Read speed: %u bytes/sec\r\n", fno.fsize/((tend - tstart)/1000)); 
                        
                    }
                    
                        
                        
                }
            }
                
                break;
            case S_FILE_START_EPISODE:
                DEBUG_TRACE("\r\nTh6File - S_FILE_START_EPISODE\r\n"); 
                if(m_episodeState != eEpStRunning)
                {
                    StartEpisode();
                }
                break;
            case S_SYSTEM_SHUTDOWN:
                CloseEpisode(m_validEpisode, m_keepEpisode);
                m_episodeState = eEpStStopped;
                DEBUG_TRACE("\r\nTh6File - S_SYSTEM_SHUTDOWN\r\n");   
                sigDispatch(P_UI, S_FADE_BACKLIGHT, 0, 1);
                
                break;
            case S_FILE_VALIDATE_EPISODE:
                DEBUG_TRACE("\r\nTh6File - Episode valid\r\n");    
                m_validEpisode = true;
                break;
                
            case S_FILE_KEEP_EPISODE:
                DEBUG_TRACE("\r\nTh6File - Keep episode\r\n");
                m_keepEpisode = true;
                break;
                
            case S_FILE_DATA_TRANSFER_MODE:
                {
                    /*
                    
                    HwAbsl::getpHw()->getpWatchdog()->kick();
                    
                    startMassStorage();
                    
                    HwAbsl::getpHw()->getpWatchdog()->kick();
                    
                    */
                }
                break;
            
            case S_ENABLE_RSM_UPDATE:
                if(ipc.m_data1 == CFG_DISABLED)
                {
                    m_researchStorageModeEnabled = false;
                }
                else
                {
                    m_researchStorageModeEnabled = true;
                }
                break;
            
            case S_USB_POWERED:
                //A cable connection is detected while in operational mode.
                //Close episode and try to start mass storage
                
                CloseEpisode(m_validEpisode, m_keepEpisode);
                m_episodeState = eEpStStopped;
                DEBUG_TRACE("\r\nTh6File - S_USB_POWERED dispatched. Close episode\r\n");
                
                //startMassStorage();
                
                break;
                
            case S_ENABLE_SD_CARD_UPDATE:
                if(ipc.m_data1 == 0)
                {
                    m_SDcardEnabled = false;
                }
                else
                {
                    m_SDcardEnabled = true;
                }
                break;
            case S_SET_OPERATIONAL_MODE:
                switch((OperationalMode_t)ipc.m_data1)
                {
                    case eOMnormal:
                    case eOMDataTransfer:
                        break;
                    case eOMconfig:
                        /*
                        if(HwAbsl::getpHw()->getpSerialConsole())
                        {
                            if(HwAbsl::getpHw()->getpSerialConsole()->isUSBHostConnectionOK())
                            {
                                post();
                            }
                        }
                        */
                        break;
                    case eOMbatteryCharge:
                    case eOMbatteryEmpty:
                    case eOMcriticalError:
                    case eOMshutdown:
                        break;
                    case eOMtestMode:
                        m_operationalMode = (OperationalMode_t)ipc.m_data1;
                        /*
                        if(m_pLNRM_FS)
                        {
                            m_pLNRM_FS->Mass__Storage_Stop();
                        }
                        */
                        break;
                    default:
                        break;
                }
                break;
            case S_TEST_COMMAND_POST:
                if(m_operationalMode == eOMtestMode) //Only allow POST from TEST_POST command if in test mode
                {
                    post();
                }
                break;
            case S_TEST_COMMAND_EMPTY_SD_CARD:
                {
                    eraseSDcard();
                }
                break;
            default:
				//Handle unrecognized signals
				sigUnknown(&ipc);
                break;
        }
    }
}

bool FileThread::createEpisodeFile(uint8_t a_fileId, FIL* a_fp, const TCHAR* a_path, LL_RTC_TimeTypeDef a_time, LL_RTC_DateTypeDef a_date)
{
    char fn[30];
    char header[80];
    uint8_t headerLen = 0;
    uint16_t bytesWritten;
    
    switch(a_fileId)
    {
        case SOUND_FILE_ID:
            sprintf(fn, "%s%s", a_path, FileThread::m_SoundFileNameExt);
            break;
        case UTIL_FILE_ID:
            sprintf(fn, "%s%s", a_path, FileThread::m_UtilFileNameExt);
            break;
        case ECG_FILE_ID:
            sprintf(fn, "%s%s", a_path, FileThread::m_EcgFileNameExt);
            break;
        case FHR_FILE_ID:
            sprintf(fn, "%s%s", a_path, FileThread::m_FhrFileNameExt);
            break;
        case EVN_FILE_ID:
            sprintf(fn, "%s%s", a_path, FileThread::m_EventFileNameExt);
            break;
        default:
            return false;
    }
    
    if(f_open(a_fp, fn, FA_CREATE_NEW | FA_WRITE) == FR_OK)
    {
        DEBUG_TRACE("\r\nCreated file: %s\r\n", fn);
        setFileTime(fn);
        if(a_fileId != EVN_FILE_ID)
        {
            headerLen = MakeHeader(a_fileId, a_time, a_date, header);
            f_write(a_fp, &header, headerLen, reinterpret_cast<UINT *>(&bytesWritten));
        }
    }
    else
    {
        memset(a_fp, 0, sizeof(FIL));
        DEBUG_TRACE("\r\nFailed to create %s.\r\n", fn);
        return false;
    }
    
    return true;
}

/*
===============================================================================
    CreateEpisode()

    Create a new episode with files and folder
    Write header for each file
    
===============================================================================
 */
bool FileThread::CreateEpisode()
{
    
    if(m_episodeState != eEpStRunning)
    {
        char pn[30];
        m_validEpisode = false;

        LL_RTC_TimeTypeDef time = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
        LL_RTC_DateTypeDef date = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
        
        CONSOLE_PRINT("\r\nCurrent time:%0.2u:%0.2u:%0.2u\r\n", time.Hours, time.Minutes, time.Seconds);
        CONSOLE_PRINT("\r\nCurrent date:%0.2u/%0.2u-20%0.2u\r\n", date.Day, date.Month, date.Year);
        
        //Create directory (yyyymmddhhmmss) and invalidate ('_') episode (Episode is validated later)
        sprintf(pn, "%s_M20%0.2u%0.2u%0.2u%0.2u%0.2u%0.2u", 
                      FileThread::m_VolName,
                      date.Year, 
                      date.Month,
                      date.Day,
                      time.Hours, 
                      time.Minutes,
                      time.Seconds);
        
        if(f_chdir("/") != FR_OK) 
        {
            DEBUG_TRACE("\r\nFailed changing directory\r\n");
            return false; //Error
        }
        
        if(f_mkdir(pn) != FR_OK)
        {
            DEBUG_TRACE("\r\nFailed creating directory\r\n");
            return false; //Error
        }
        else
        {
            CONSOLE_PRINT("\r\nCreated directory: %s\r\n", pn);
            setFileTime(pn);
        }
        
        //Copy episode name
        std::string epn(pn);
        m_episodeName = epn;
        
        
        //Change dirctory to new episode (episodes are directory with files of same name inside)
        f_chdir(pn);
        
        //Set all file pointers to 0
        memset(&m_pSoundFile, 0, sizeof(FIL));
        memset(&m_pUtilFile, 0, sizeof(FIL));
        memset(&m_pEcgFile, 0, sizeof(FIL));
        memset(&m_pFhrFile, 0, sizeof(FIL));
        memset(&m_pEventFile, 0, sizeof(FIL));
        
        //Create sound samples file (if research storage mode is enabled) 
        if(m_researchStorageModeEnabled)
        {
            if(createEpisodeFile(SOUND_FILE_ID, &m_pSoundFile, pn, time, date) == false)
            {
                return false;
            }
            
        }
        
        //Create utility file (if research storage mode is enabled) 
        if(m_researchStorageModeEnabled)
        {
            if(createEpisodeFile(UTIL_FILE_ID, &m_pUtilFile, pn, time, date) == false)
            {
                return false;
            }   
        }
        
        //Create ECG samples file (if research storage mode is enabled) 
        if(m_researchStorageModeEnabled)
        {
            if(createEpisodeFile(ECG_FILE_ID, &m_pEcgFile, pn, time, date) == false)
            {
                return false;
            }   
        }
        
        //Create FHR samples file (if research storage mode is enabled) 
        if(m_researchStorageModeEnabled)
        {
            if(createEpisodeFile(FHR_FILE_ID, &m_pFhrFile, pn, time, date) == false)
            {
                return false;
            }
        }        
        //Create event file
        if(createEpisodeFile(EVN_FILE_ID, &m_pEventFile, pn, time, date) == false)
        {
            return false;
        }
        
        if(m_pEventFile.obj.fs)
        {
            
            //Create startup events
            Events::getpEvents()->AddEvent(Events::evtEpisodeDateAndTime, 
                                           Events::TO_EPISODE_LOG,
                                           pn);
            Events::getpEvents()->AddEvent(Events::evtSoftwareVersion, 
                                           Events::TO_EPISODE_LOG,
                                           (float)VER_MAJ,
                                           (float)VER_MIN,
                                           (float)VER_MAINTENANCE,
                                           (float)VER_BUILD);
            
            std::string devId = Config::getpConfig()->GetConfigStr(Config::eCfgDeviceIdentifier, MAX_DEV_ID_SIZE);    
            Events::getpEvents()->AddEvent(Events::evtDeviceSerialNumber, 
                                           Events::TO_EPISODE_LOG,
                                           devId.c_str());
            
            Events::getpEvents()->AddEvent(Events::evtDeviceConfiguration, 
                                           Events::TO_EPISODE_LOG,
                                           (float)Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmTime),
                                           (float)Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmLow),
                                           (float)Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmHigh),
                                           (float)Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmTime),
                                           (float)Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmLow),
                                           (float)Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmHigh),
                                           (float)Config::getpConfig()->GetConfig(Config::eCfgLostSignalAlarmTime));
            
            
            std::string moduleVersionInfo = HwAbsl::getpHw()->getProbeSerial()->GetModuleVersionInfo();
            Events::getpEvents()->AddEvent(Events::evtUltrasoundModuleVersion, 
                                           Events::TO_EPISODE_LOG,
                                           moduleVersionInfo.c_str());
            
        }
            
            
        
        return true;
    }
    else if(m_episodeState == eEpStRunning)
    {
        DEBUG_TRACE("\r\nFileThread::CreateEpisode - Episode is already running.\r\n");
        return false;
    }
    else
    {
        DEBUG_TRACE("\r\nFileThread::CreateEpisode - No SD card.\r\n");
        return false;
    }
    
}
    
/*
===============================================================================
    MakeHeader()

    Make header for a file
    
===============================================================================
 */
uint8_t FileThread::MakeHeader(uint8_t fileId, LL_RTC_TimeTypeDef a_time, LL_RTC_DateTypeDef a_date, char * a_buffer)
{
    std::string devId = Config::getpConfig()->GetConfigStr(Config::eCfgDeviceIdentifier, MAX_DEV_ID_SIZE);    
    
    uint8_t len = sprintf(a_buffer, "MOYO SN:%s SW:%0.2u.%0.2u.%0.2u.%0.2u FID:%0.2u DT:20%0.2u%0.2u%0.2u%0.2u%0.2u%0.2u",
                                    devId.c_str(),
                                    VER_MAJ, 
                                    VER_MIN, 
                                    VER_MAINTENANCE, 
                                    VER_BUILD,
                                    fileId,
                                    a_date.Year,
                                    a_date.Month,
                                    a_date.Day,
                                    a_time.Hours,
                                    a_time.Minutes,
                                    a_time.Seconds
                                    );
    
    return len;

}

/*
===============================================================================
    CloseEpisode()

    Finish and close all files related to current episode
    
    Remove "_" if parameter set
    Delete episode (very short episode) if flag set
===============================================================================
 */
void FileThread::CloseEpisode(bool a_validateEpisode, bool a_keepEpisode)
{
    if(m_pSoundFile.obj.fs)
    {
        f_close(&m_pSoundFile);
        //m_pLNRM_FS->FS__FClose(m_pSoundFile);
        m_pSoundFile.obj.fs = 0;
        DEBUG_TRACE("\r\nSound file closed\r\n");
    }
    
    if(m_pUtilFile.obj.fs)
    {
        f_close(&m_pUtilFile);
        m_pUtilFile.obj.fs = 0;
        DEBUG_TRACE("\r\nUtil file closed\r\n");
    }
    
    if(m_pEcgFile.obj.fs)
    {
        f_close(&m_pEcgFile);
        m_pEcgFile.obj.fs = 0;
        DEBUG_TRACE("\r\nECG file closed\r\n");
    }
    
    if(m_pFhrFile.obj.fs)
    {
        f_close(&m_pFhrFile);
        m_pFhrFile.obj.fs = 0;
        DEBUG_TRACE("\r\nFHR file closed\r\n");
    }
    
    if(m_pEventFile.obj.fs)
    {
        UINT bw; 
        while(Events::getpEvents()->Count() > 0)
        {
#ifdef DEBUG_EVENTS                    
            DEBUG_TRACE("\r\nWriting events: %u to go\r\n", Events::getpEvents()->Count());
#endif
            std::string ev = Events::getpEvents()->EventStringPullFront();
            f_write(&m_pEventFile, ev.c_str(), ev.length(), &bw);
              
            //Should event be written to system log?
            if(Events::getpEvents()->isSystemLogEvent())
            {
                writeSystemLog(ev.c_str(), ev.length());
            }
        }
        
        f_close(&m_pEventFile);
        m_pEventFile.obj.fs = 0;
        DEBUG_TRACE("\r\nEvent file closed\r\n");
    }
    
    if(!a_keepEpisode)
    {
        deleteEpisode(m_episodeName);
    }
    else if(a_validateEpisode)
    {
        validateEpisode(m_episodeName);
    }
    
    m_episodeName.clear();
}

/*
===============================================================================
    validateEpisode()
    
    Validate episode by removing the preceding '_' from episode folder and episode files
    Must be done after closing file and before a new episode is created
===============================================================================
 */
bool FileThread::validateEpisode(string a_episodeName)
{
    DIR dpr;        /* Root directory object */
    DIR dp;         /* Directory object */
    FILINFO fno;    /* File information */
    
    if(a_episodeName.length() > 0 && a_episodeName.at(0) == '_')
    {
        //Change dir to root
        f_chdir("/");
        
        if(f_findfirst(&dpr, &fno, "/", a_episodeName.c_str()) == FR_OK)
        {
            //Test if this is a dirctory
            if((fno.fattrib & AM_DIR) == AM_DIR && fno.fname[0])
            {
                //Search the directory 
                std::string f = a_episodeName + ".*";
                std::string d = "/" + a_episodeName;
                if(f_findfirst(&dp, &fno, d.c_str(), f.c_str()) == FR_OK)
                {
                    while(fno.fname[0])
                    {
                        std::string oldName(fno.fname);
                        std::string newName = oldName;
                        newName.erase(0, 1); //Remove the '_'
                        
                        //Make full path
                        newName.insert(0, a_episodeName + "/");
                        oldName.insert(0, a_episodeName + "/");
                        
                        //Rename file
                        if(f_rename(oldName.c_str(), newName.c_str()) == FR_OK)
                        {
                            DEBUG_TRACE("\r\nRenamed %s to %s\r\n", oldName.c_str(), newName.c_str());
                        }
                        else
                        {
                            DEBUG_TRACE("\r\nRename of %s failed!\r\n", oldName.c_str());
                        }
                        
                        f_findnext(&dp, &fno);
                    }

                }
                
                f_closedir(&dp);
                
                //Rename directory
                std::string newName = a_episodeName;
                newName.erase(0, 1); //Remove the '_'
                if(f_rename(a_episodeName.c_str(), newName.c_str()) == FR_OK)
                {
                    DEBUG_TRACE("\r\nRenamed %s to %s\r\n", a_episodeName.c_str(), newName.c_str());
                }
                
                f_closedir(&dpr);
                
                return true;
            }
            
            return false;
        }
        
    }
    return false;
}

/*
===============================================================================
    deleteEpisode()
    
    Delete episode directory and files
===============================================================================
 */
bool FileThread::deleteEpisode(string a_episodeName)
{
    DIR dpr;        /* Directory object (episode name) */
    DIR dp;         /* Directory object (episode file) */
    FILINFO fno;    /* File information */
    
    if(a_episodeName.length() > 0)
    {
        //Change dir to root
        f_chdir("/");
        
        if(f_findfirst(&dpr, &fno, "/", a_episodeName.c_str()) == FR_OK)
        {
            //Test if this is a dirctory
            if((fno.fattrib & AM_DIR) == AM_DIR && fno.fname[0])
            {
                //Search the directory and delete all files
                std::string f = "*";
                std::string d = "/" + a_episodeName;
                if(f_findfirst(&dp, &fno, d.c_str(), f.c_str()) == FR_OK && fno.fname[0])
                {
                    do
                    {
                        std::string name(fno.fname);
                        
                        //Make full path
                        name.insert(0, a_episodeName + "/");
                        
                        //Rename file
                        if(f_unlink(name.c_str()) == FR_OK)
                        {
                            DEBUG_TRACE("\r\nDeleted %s\r\n", name.c_str());
                        }
                        else
                        {
                            DEBUG_TRACE("\r\nDelete of %s failed!\r\n", name.c_str());
                        }
                    } while(f_findnext(&dp, &fno) == FR_OK && fno.fname[0]);

                    f_closedir(&dp);
                }
                
                //Delete directory itselves
                if(f_unlink(a_episodeName.c_str()) == FR_OK)
                {
                    DEBUG_TRACE("\r\nDeleted %s\r\n", a_episodeName.c_str());
                }
                else
                {
                    DEBUG_TRACE("\r\nDelete of %s failed!\r\n", a_episodeName.c_str());
                }
                
                f_closedir(&dpr);
                return true;
            }
            
            
        }                
         
    }
    
    DEBUG_TRACE("\r\nEpisode name len = 0 or error\r\n");
    return false;
    
}

/*
===============================================================================
    writeSystemLog()

    Write an entry to system log
===============================================================================
 */
bool FileThread::writeSystemLog(const char * a_data, uint16_t a_length)
{  
    FIL f;
    uint16_t bw;
    
    //Change dir to root
    f_chdir("/");
    
    if(f_open(&f, m_SystemLogFileName, FA_OPEN_APPEND | FA_WRITE) != FR_OK)
    {
        CONSOLE_PRINT("\r\nSystem log File Open FAILED\r\n");
        return false;
    }
    
    LL_RTC_TimeTypeDef time = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
    LL_RTC_DateTypeDef date = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
    char buf[30];
    uint8_t len;
    
    len = sprintf(buf, "20%0.2u-%0.2u-%0.2u %0.2u:%0.2u:%0.2u,", 
                  date.Year, 
                  date.Month,
                  date.Day,
                  time.Hours, 
                  time.Minutes,
                  time.Seconds);
    
    f_write(&f, buf, len, reinterpret_cast<UINT *>(&bw));
    f_write(&f, a_data, a_length, reinterpret_cast<UINT *>(&bw));   //Event
    f_close(&f);
    setFileTime(m_SystemLogFileName);
    
    return true;

}

/*
===============================================================================
    getPostResult()

    Get the post result for file system
===============================================================================
*/
POST_ctrl_t FileThread::getPostResult(void)
{
    return m_POSTstatus;
}

/*
===============================================================================
    clearPostResult()

    Clear the post result for file system

===============================================================================
*/
void FileThread::clearPostResult(void)
{
    m_POSTstatus.finished = false;
}

/*
===============================================================================
    post()

    Run post for file system

===============================================================================
*/
bool FileThread::post()
{
    FIL fil;        /* File object */
    //FRESULT fr;     /* FatFs return code */
    bool err = false;
    const char txt[] = "Laerdal Fetal Heart Rate Monitor Power On Self Test\r\n";
    const char fn[] = "post.tst";
    char buf[100];
    uint16_t len;
    uint16_t bw;
    
    m_POSTstatus.post.postBits.sdCard = false;
    m_POSTstatus.finished = false;
    
    if(m_SDcardPresent)
    {
        if(f_open(&fil, fn, FA_WRITE | FA_CREATE_ALWAYS) == FR_OK)
        {
            len = sprintf(buf, "%s", txt);
            f_write(&fil, txt, len, reinterpret_cast<UINT *>(&bw));
            f_close(&fil);
            if(f_open(&fil, fn, FA_READ) == FR_OK)
            {
                f_read(&fil, buf, sizeof buf, reinterpret_cast<UINT *>(&len));  /* Read a chunk of data from the source file */
                std::string rTxt(buf);
                std::string wTxt(txt);
                if(rTxt != wTxt)
                {
                    err = true;
                }
                f_close(&fil);
                if(f_unlink(fn) != FR_OK)
                {
                    err = true;
                }
            }
            else
            {
                err = true;
            }
        }
        else
        {
            err = true;
        }
            
    }
    else
    {
        err = true;
    }       
    
    if(err)
    {
        CONSOLE_PRINT("\r\nPOST - POST_STATUS_BITS_SDCARD_ERR\r\n");
        m_POSTstatus.post.postBits.sdCard = true;
        
    }
    
    m_POSTstatus.finished = true;
    
    return err;
}


/*
===============================================================================
    deleteOldestEpisode()

    Find the oldest episode directory and delete all files in it
    Begin with invalid episodes ("_")
===============================================================================
*/
bool FileThread::deleteOldestEpisode()
{
    DIR dp;         /* Directory object */
    FILINFO fno;    /* File information */
    std::string fnOldest;
    
    //Change dir to root
    f_chdir("/");
    
    //Try find invalid episodes ("_M")
    if(f_findfirst(&dp, &fno, "/", "_M20*") == FR_OK)
    {
        fnOldest = std::string(fno.fname);
            
        while(fno.fname[0])
        {
            std::string sfn(fno.fname);
                        
            //Check if next is older
            if(sfn.compare(fnOldest) < 0)
            {
                //Latest fond is older than current, remember latest found
                fnOldest = sfn;
                //DEBUG_TRACE("Current oldest invalid:%s\r\n", fnOldestInvalid.c_str()); 
            }
            
            f_findnext(&dp, &fno);
        }    
    }    
    
    f_closedir(&dp);
    
    if(fnOldest != "")
    {
        return deleteEpisode(fnOldest);
        
    }
    
    fnOldest = "";
    //Try find invalid episodes ("_M")
    if(f_findfirst(&dp, &fno, "/", "M20*") == FR_OK)
    {
        fnOldest = std::string(fno.fname);
            
        while(fno.fname[0])
        {
            std::string sfn(fno.fname);
                        
            //Check if next is older
            if(sfn.compare(fnOldest) < 0)
            {
                //Latest fond is older than current, remember latest found
                fnOldest = sfn;
                //DEBUG_TRACE("Current oldest invalid:%s\r\n", fnOldestInvalid.c_str()); 
            }
            
            f_findnext(&dp, &fno);
        }    
    }    
    
    f_closedir(&dp);
    
    if(fnOldest != "")
    {
        return deleteEpisode(fnOldest);
        
    }
    
    DEBUG_TRACE("\r\nDid not find any episodes to delete\r\n");
    return false;
}

/*
===============================================================================
    FileExists()

    Check if a file exists

===============================================================================
*/
bool FileThread::FileExists(std::string a_filename)
{
    FRESULT fr;
    FILINFO fno;
    
    fr = f_stat(a_filename.c_str(), &fno);
    
    return fr == FR_OK ? true : false;
}


/*
===============================================================================
    saveToFiles()

    Get data from storage buffers in the system and save them to their respective files

===============================================================================
*/
void FileThread::saveToFiles()
{
    uint16_t size;
    uint16_t bw;
    FRESULT res;
    
    uint32_t start = OS_GetTime();    
    
    if(m_episodeState == eEpStRunning && m_SDcardPresent)
    {
        size = HwAbsl::getpHw()->getProbeSerial()->GetFileSoundData(m_pSoundBuffer, SOUND_BUF_SIZE);
        if(m_pSoundFile.obj.fs && size)
        {
            res = f_write(&m_pSoundFile, &m_pSoundBuffer[0], size, reinterpret_cast<UINT *>(&bw));
            if(bw != size || res != FR_OK)
            {
                logFileWriteError(&m_pSoundFile, bw != size);
            }
        }
        
        size = HwAbsl::getpHw()->getProbeSerial()->GetFileUtilData(m_pUtilBuffer, UTIL_BUF_SIZE) * sizeof(Utilrecord_t);
        if(m_pUtilFile.obj.fs && size)
        {
            res = f_write(&m_pUtilFile, &m_pUtilBuffer[0], size, reinterpret_cast<UINT *>(&bw));
            if(bw != size || res != FR_OK)
            {
                logFileWriteError(&m_pUtilFile, bw != size);
            }
        }
        
        size = m_pAdcTh->GetECGFileData(m_pEcgBuffer, ECG_BUF_SIZE) * sizeof(EcgBuffer::ECGrecord_t);
        if(m_pEcgFile.obj.fs && size)
        {
            res = f_write(&m_pEcgFile, &m_pEcgBuffer[0], size, reinterpret_cast<UINT *>(&bw));
            if(bw != size || res != FR_OK)
            {
                logFileWriteError(&m_pEcgFile, bw != size);
            }
            
        }
        
        if(Events::getpEvents()->Count() > 0 && m_pEventFile.obj.fs)
        {
            std::string ev = Events::getpEvents()->EventStringPullFront();
#ifdef DEBUG_EVENTS            
            DEBUG_TRACE("\r\n%s", ev.c_str());
#endif
            res = f_write(&m_pEventFile, ev.c_str(), ev.length(), reinterpret_cast<UINT *>(&bw));
            if(bw != ev.length() || res != FR_OK)
            {
                logFileWriteError(&m_pEcgFile, bw != ev.length());
            }
            
            if(Events::getpEvents()->isSystemLogEvent())
            {
                writeSystemLog(ev.c_str(), ev.length());
            }
            
        }
        
        m_pSuperTh->GetFhrRecords(m_FhrRecords);
        size = sizeof(FHRrecord_t) * m_FhrRecords.size();
        if(m_pFhrFile.obj.fs && size)
        {
            res = f_write(&m_pFhrFile, &m_FhrRecords[0], size, reinterpret_cast<UINT *>(&bw));
            if(bw != size || res != FR_OK)
            {
                logFileWriteError(&m_pFhrFile, bw != size);
            }
            
        }
        
        
    }
    
#ifdef REPORT_BATTERY_STATE
    else if(m_pLNRM_FS && m_pLNRM_FS->Mass__StorageIsActive() == false)
    {
        if(Events::getpEvents()->Count() > 0)
        {
            std::string ev = Events::getpEvents()->EventStringPullFront();
#ifdef DEBUG_EVENTS
            DEBUG_TRACE("\r\n%s", ev.c_str());
#endif
            m_pLNRM_FS->FS__Write(m_pEventFile, ev.c_str(), ev.length());
            
            if(Events::getpEvents()->isSystemLogEvent())
            {
                writeSystemLog(ev.c_str(), ev.length());
            }
            
        }
    }
#endif
    
    uint32_t elapsed = OS_GetTime() - start;
    if(elapsed > 1000)
    {
        CONSOLE_PRINT("\r\nFile write time long: %u ms", elapsed);
    }
    
}

/*
===============================================================================
    setFileTime()

    Set filetime on a file to current time
===============================================================================
*/
bool FileThread::setFileTime(const char * a_pFileName)
{
    //U32 timeStamp;
    //FS_FILETIME fileTime;
    LL_RTC_TimeTypeDef time = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
    LL_RTC_DateTypeDef date = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
    
    //fileTime.Year   = date.Year;
    //fileTime.Month  = date.Month;
    //fileTime.Day    = date.Day;
    //fileTime.Hour   = time.Hours;
    //fileTime.Minute = time.Minutes;
    //fileTime.Second = time.Seconds;
    FILINFO fno;

    fno.fdate = (WORD)((((2000 - 1980) - date.Year) * 512U) | date.Month * 32U | date.Day);
    fno.ftime = (WORD)(time.Hours * 2048U | time.Minutes * 32U | time.Seconds / 2U);
    
    if(f_utime(a_pFileName, &fno) == FR_OK)
    {
        return true;
    }
    
    return false;
}

/*
===============================================================================
    StartEpisode()

    Start an episode
    
===============================================================================
*/
void FileThread::StartEpisode()
{
    m_episodeStartTime = OS_GetTime();
    
    DEBUG_TRACE("\r\nTh6File - Start sample\r\n");
    m_episodeState = eEpStRunning;
    DEBUG_TRACE("\r\nEpisode started on %u\r\n", OS_GetTime());
}

/*
===============================================================================
    logFileWriteError()

    Log a file write error to episode and/or system log

===============================================================================
*/
void FileThread::logFileWriteError(FIL *a_pFile, bool a_diskFull)
{
    int16_t err = f_error(a_pFile);
    
    if(a_diskFull)
    {
        CloseEpisode(m_validEpisode, m_keepEpisode);
    }
    else
    {
        if((OS_GetTime() - m_episodeStartTime) > FILE_WRITE_ERROR_MASK_TIME)
        {
            
            if(m_fileWriteErrorCnt < MAX_FILE_WRITE_ERROR_REPORTS)
            {
                m_fileWriteErrorCnt++;
                Events::getpEvents()->AddEvent(Events::evtFileWritingError, 
                                               Events::TO_EPISODE_LOG | Events::TO_SYSTEM_LOG,
                                               (float)err);
            }
        }
    }    
    
}

/*
===============================================================================
    eraseSDcard()

    Erase all files and directories on the SD card

===============================================================================
*/
void FileThread::eraseSDcard()
{
    DIR dp;         /* Directory object */
    FILINFO fno;    /* File information */
    //bool succ;
    bool err = true;
    
    //Test
    m_operationalMode = eOMtestMode;
    //Test
    
    if(m_SDcardPresent && m_operationalMode == eOMtestMode)
    {
        CloseEpisode(false, false); //Just in case
        m_episodeState = eEpStStopped;
        //succ = true;
        err = false;
    
        //Change dir to root
        f_chdir("/");
    
        if(f_findfirst(&dp, &fno, "/", "*") == FR_OK)
        {
            while(fno.fname[0])
            {
                //Do not delete system files
                if((fno.fattrib & AM_RDO) != AM_RDO &&
                   (fno.fattrib & AM_HID) != AM_HID &&
                   (fno.fattrib & AM_SYS) != AM_SYS)
                {
                    if(deleteEpisode(string(fno.fname)) == false)
                    {
                        err = true;
                    }
                }
                
                f_findnext(&dp, &fno);
            }
        }    
    }    
    
    f_closedir(&dp);
    
    if(err)
    {
        CONSOLE_PRINT("R102 0\r\n"); //Failed
    }
    else
    {
        CONSOLE_PRINT("R102 1\r\n"); //Succeeded
    }   
    
}

void FileThread::listRootFiles()
{
    DIR dpr;        /* Root directory object */
    DIR dp;         /* Episode directory object */
    FILINFO fno;    /* Episode File information */
    string d;       /* Current directory */
    
    d = "/"; //Root
    DEBUG_TRACE("\r\nFiles on SD card:\r\n");
    
    if(f_findfirst(&dpr, &fno, d.c_str(), "*") == FR_OK)
    {
        while(fno.fname[0])
        {
            DEBUG_TRACE("%s\r\n", fno.fname); //Episode
            d = "/" + string(fno.fname);
            if(f_findfirst(&dp, &fno, d.c_str(), "*") == FR_OK)
            {
                while(fno.fname[0])
                {
                    DEBUG_TRACE("\t%s\r\n", fno.fname); //Episode file
                    
                    f_findnext(&dp, &fno);
                }
                
                f_closedir(&dp);
                d = "/"; //Root
            }
            
            f_findnext(&dpr, &fno);
        }
        
    }
       
    f_closedir(&dpr);
}
/*
===============================================================================
    startMassStorage()

    Enable USB and start mass storage driver

===============================================================================
*/
#if(0)
void FileThread::startMassStorage(void)
{
    if(m_pLNRM_FS)
    {

        m_pLNRM_FS->Mass__Storage_Init();
        if(!m_pLNRM_FS->Mass__Storage_Start())
        {
            DEBUG_TRACE("\r\nFileThread - Mass Storage timeout\r\n");
            sigDispatch(P_SUPER, S_MASS_STORAGE_ENABLED, 0, 0);
            sigDispatch(P_UI, S_USB_STATE, UNCONNECTED, 0, 0);
            //sigDispatch(P_SUPER, S_USB_STATE, UNCONNECTED, 0, 0);
            m_pLNRM_FS->Mass__Storage_Stop();
        }
        else
        {
            sigDispatch(P_SUPER, S_MASS_STORAGE_ENABLED, 1, 0);
        }
    }
    else
    {
        DEBUG_TRACE("\r\nFileThread - No SD card\r\n");
        
        //Use mass storage driver to check whether we have connection with a PC or A USB wall charger
        //In case of timeout, send notification.
        //If we have a PC connection, the notification will be handled in S_OSTIMEOUT
        Mass_Storage_Init();
        if(!Mass_Storage_Start())
        {
            DEBUG_TRACE("\r\nFileThread - Mass Storage timeout\r\n");
            sigDispatch(P_SUPER, S_MASS_STORAGE_ENABLED, 0, 0);
            sigDispatch(P_UI, S_USB_STATE, UNCONNECTED, 0, 0);
            //sigDispatch(P_SUPER, S_USB_STATE, UNCONNECTED, 0, 0);
            Mass_Storage_Stop();
        }
        else
        {
            sigDispatch(P_SUPER, S_MASS_STORAGE_ENABLED, 1, 0);
        }
    }
}
#endif