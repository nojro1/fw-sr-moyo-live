//! \file    UIThread.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    09.12.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  09Dec2013, JRo, Original.

#include "uithread.h"
#include "utilfuncs.h"

//Test
uint32_t m_UIThreadTestCtr = 0;

// static member init
UIThread*  UIThread::m_pUiTh = 0;

#ifdef DEBUG_ALARM_HANDLER
    
    static bool UI_CDA_criticalErrorAlarm = false;
    static bool UI_CDA_abnormalFHRalarm = false;
    static bool UI_CDA_lowBatteryAlarm = false;
    static bool UI_CDA_lostSignalAlarm = false;

    static bool UI_CAA_criticalErrorAlarm = false;
    static bool UI_CAA_abnormalFHRalarm = false;
    static bool UI_CAA_lowBatteryAlarm = false;
    static bool UI_CAA_lostSignalAlarm = false;

    static bool UI_alarmAcknowlegde = false;
#endif


char const* TXT_SYSTEM_SHUTDOWN = "Power off";
char const* TXT_PASSED          = "PASSED";
char const* TXT_FAILED          = "FAILED";
char const* TXT_ENABLED         = "ON";
char const* TXT_DISABLED        = "OFF";
char const* TXT_1X              = "1x";
char const* TXT_4X              = "4x";
char const* TXT_MOYO_FW_VERSION = " Moyo  FW : ";
char const* TXT_PROBE_FW_VERSION= " Probe FW : ";
char const* TXT_DATE            = " Date     : ";
char const* TXT_TIME            = " Time     : ";
char const* TXT_POST_DISPLAY    = " POST - Display   : ";
char const* TXT_POST_PROBE      = " POST - Probe     : ";
char const* TXT_POST_CONFIG     = " POST - Config    : ";
char const* TXT_POST_FUELGAUGE  = " POST - Fuel gauge: ";
char const* TXT_POST_SD_CARD    = " POST - SD card   : ";
char const* TXT_POST_FLASH      = " POST - Flash mem : ";
char const* TXT_POST_5VREG      = " POST - Volt reg  : ";
char const* TXT_POST_RTCXT      = " POST - RTC XTAL  : ";
char const* TXT_CONFIG_MODE     = " CONFIGURATION";
char const* TXT_TEST_MODE       = " TEST";
char const* TXT_CFG_AMP_SENS    = " Amp sense set to       ";
char const* TXT_CFG_DEV_ID      = " Device id set to       ";
char const* TXT_CFG_TIME        = " New time set to        ";
char const* TXT_CFG_DATE        = " New date set to        ";
char const* TXT_CFG_EVT         = " Ep valid time set to   ";
char const* TXT_CFG_EOET        = " End of ep tmout set to ";
char const* TXT_CFG_EPTS        = " Ep too short tm set to ";
char const* TXT_CFG_HRUT        = " HR update time set to  ";
char const* TXT_CFG_AVG_BL      = " HR avg buf len set to  ";
char const* TXT_CFG_LSAT        = " Lost sig al tm set to  ";
char const* TXT_CFG_FHR_AT_LH   = " Fhr al at h lim set to ";
char const* TXT_CFG_FHR_AT_LL   = " Fhr al at l lim set to ";
char const* TXT_CFG_FHR_AB_LH   = " Fhr al ab h lim set to ";
char const* TXT_CFG_FHR_AB_LL   = " Fhr al ab l lim set to ";
char const* TXT_CFG_FHR_AT_T    = " Fhr al at t lim set to ";
char const* TXT_CFG_FHR_AB_T    = " Fhr al ab t lim set to ";
char const* TXT_CFG_SHOW_CLOCK  = " Show clock set to      ";
char const* TXT_CFG_RSM         = " Rsch str mode set to   ";
char const* TXT_CFG_SD_CARD     = " SD card func set to    ";
char const* TXT_CFG_SPKR_GAIN   = " Dplr spkr gain set to  ";
char const* TXT_CFG_PROBE_GAIN  = " Probe snd gain set to  ";
char const* TXT_CFG_FHR_FILTER  = " Mask '-?-' set to      "; 

/*
=============================================================================== 

    Construction

===============================================================================
 */
UIThread::UIThread()
{
    //Init POST
    m_postCtrl.postResult.postWord = 0;
    m_postCtrl.probeFinished = false;
    m_postCtrl.displayFinished = false;
    m_postCtrl.configFinished = false;
    m_postCtrl.fuelGaugeFinished = false;
    m_postCtrl.SDcardFinished = false;
    m_postCtrl.ProgramMemChecksumFinished = false;
    m_postCtrl.RTCcrystalFinished = false;
    m_postCtrl.VoltageRegulatorFinished = false;
    m_POSTstatus.finished = false;
    m_POSTstatus.post.postWord = 0;
    
    m_operationalMode = eOMnormal;
    
    m_alarmAcknowlegde = false;
    m_currentDisplayAlarm.alarmByte = 0;
    m_currentAudioAlarm.alarmByte = 0;
    m_alarmLogStatus.alarmByte = 0;
    
    m_displayInterfaceOK = false;
    
    
    m_FhrAtypicalAlarmLow = 110;
    m_FhrAtypicalAlarmHigh = 160;
    m_fhrAlarm = false;
    
    m_showClock = false;
    m_clockComponent = 0;
    m_displayMode = eDmFhrHr;
    m_soundVolume = eSVhigh;
    m_symbolBlinkTimer = BATT_IND_BLINK_TIME;
    m_symbolBlinkState = eSBSStateOn;
    m_critcalErrorSymbol = 0;
    m_symbolRemBlinks = CRITICAL_ERR_NO_BLINKS;
    m_ecgElectrodesTouchedPrevDisplayMode = m_displayMode;
    m_batteryRemainingCapacity = 255; //To ensure battery capacity is updated at startup
    m_displayDimTmr = CHARGE_MODE_DISPLAY_DIM_TIME;
    m_batteryEmptyDisplayState = eBEinit;
    m_infoSymbolActive = eISnone;
    m_displayIsDimming = false;
    m_rsmModeEnabled = false;
    m_showLogoFinishTime = 0;
}

/*
===============================================================================
    
    Destruction

===============================================================================
 */
UIThread::~UIThread()
{
}

/*
===============================================================================
    Process Thread

===============================================================================
 */
void UIThread::run(void *pmsg)
{
    
	//Startup routines here
    
    //Test
    /*
    while(1)                //Running with RTOS
    {
        IPC ipc;
        sigWait(UIThread::THREAD_TICK_TIME, &ipc);  //Wait for a signal or OS schedule
        
        m_UIThreadTestCtr++;
    }
    */

    //waitUntilAllThreadsStarted();
    DEBUG_TRACE("\r\n%s Pri: %d", getThreadName(), getPriority() );
    
    DEBUG_TRACE("\r\nDisplay reset. OStime=%u\r\n", OS_GetTime() );
    
    //Reset display by activate nReset line
    HwAbsl::getpHw()->enableLCDreset(true);
    sleep(10);
    HwAbsl::getpHw()->enableLCDreset(false);
    
    initDisplay();
    
    DEBUG_TRACE("\r\nDisplay init finished. OStime=%u\r\n", OS_GetTime() );
    
    
    m_pFileTh = reinterpret_cast<FileThread*>(getpThread(P_FILE));
    
    m_volumeSymbol = new Symbol(2, 0, 0);
    m_volumeSymbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::volumeOff));
    m_volumeSymbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::volumeOn));
    
    m_UsbSymbol = new Symbol(1, 0, 0);
    m_UsbSymbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::usb));
    
    m_alarmSymbol = new Symbol(2, 0, 0);
    m_alarmSymbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::alarm));
    m_alarmSymbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::alarmAck));
    
    m_alarmAckBigSymbol = new Symbol(1, 0, 0);
    m_alarmAckBigSymbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::alarmAckBig));
    
    m_batteryEmptyBigSymbol = new Symbol(1, 0, 0);
    m_batteryEmptyBigSymbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryEmptyBig));
    m_batteryEmptyBigSymbol->setPos(46, 3);
    m_batteryEmptyBigSymbol->setActive(0);
    
    m_batteryCriticallyLowBigSymbol = new Symbol(1, 0, 0);
    m_batteryCriticallyLowBigSymbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryCriticalBig));
    m_batteryCriticallyLowBigSymbol->setPos(46, 26);
    m_batteryCriticallyLowBigSymbol->setActive(0);
    
    m_batteryCapacityComponent = new BatteryCapacity(eVPstatus, 191, 9);
    
    m_bigBatteryCapacityComponent = new BatteryCapacityBig(eVPCharging, 46, 14);
    
    m_mother = new Symbol(1, 0, 0);
    m_mother->add( reinterpret_cast<PGSYMBOL>(&SymLib::woman));
    
    m_fetalHeartRateComponent = new Heartrate(eVPfetalHeartRate, FHR_X_POS, FHR_Y_POS,
                                              0, 23, 219, 120,
                                              0, eHRMfetalHr);
    m_motherHeartRateComponent = new Heartrate(eVPmaternalHeartRate, 104, 12, 
                                              0, 121, 219, 175,
                                              m_mother, eHRMmaternalHr);
    
    m_motherHeartRateComponent->setQuestionmarkLimit(1);
    
    m_clockComponent = new Clock(eVPstatus, 11, 19);
    
        
    m_volumeSymbol->setPos( 165, 9 );
    
    m_alarmSymbol->setPos( 142, 9 );
    m_alarmSymbol->setActive(0);
    
    m_UsbSymbol->setPos( 150, 10 );
        
    m_alarmAckBigSymbol->setPos( 72, 28 );
    
    m_currentFetalHR = 0;
    m_currentMotherHR = 0;
    
    m_backlightDimmed = false;
    m_DisplayModeKeyPressed = false;
    m_AudioVolumeKeyPressed = false;
    m_OnKeyPressed = false;
    m_OffKeyPressed = false;
    m_PWMdcBacklight = BACKLIGHT_START;
    
    m_HrBigTrendComponent  = new HRtrend(eVPTrend, eVPstatus, HRtrend::eModeFullScreen);
    
    startupHandler();
    
    while(1)                //Running with RTOS
    {
        IPC ipc;
        sigWait(UIThread::THREAD_TICK_TIME, &ipc);  //Wait for a signal or OS schedule

        m_UIThreadTestCtr++;
            
		switch(ipc.m_sig)
        {
			case S_OSTIMEOUT:
            {
                switch(m_operationalMode)
                {
                    case eOMnormal:
                    {

#ifdef DEBUG_ALARM_HANDLER
                        logAlarmState();
#endif

                        backlightDimCtrl();
                        displayDimHandler();
                        buttonHandler();
                        updateDisplay(eDcUpdateClock, 0);
                        m_fetalHeartRateComponent->tick();
                        m_batteryCapacityComponent->tick();
                        
                        uint8_t fhr = HwAbsl::getpHw()->getProbeSerial()->GetFetalHeartRateFiltered();
                
                        updateDisplay(eDcUpdateFhrTrend, m_currentFetalHR);
                        
                        if(fhr != m_currentFetalHR)
                        {
                            m_currentFetalHR = fhr;
                            updateDisplay(eDcUpdateFetalHR, m_currentFetalHR);
                        }
                        
                        if(m_infoSymbolActive != eISnone)
                        {
                            infoSymbolHandler();
                        }
    
                    }
                        break;
                    case eOMDataTransfer:
                    case eOMconfig:
                    case eOMbatteryCharge:
                        updateDisplay(eDcUpdateClock, 0);
                        backlightDimCtrl();
                        buttonHandler();
                        m_bigBatteryCapacityComponent->tick();
                        displayDimHandler();
                        break;
                    case eOMbatteryEmpty:
                        batteryEmptyHandler();
                        break;
                    case eOMcriticalError:
                        //Critical error. Handle symbol blink
                        //Moyo will turn off power when finished
                        if(!m_currentDisplayAlarm.alarmBits.criticalErrorAlarm)
                        {
                            //Only run blink- and power off handler if detected in POST
                            criticalPostErrorHandler(true);
                        }
                        else
                        {
                            buttonHandler();
                        }
                        
                        break;
                    default:
                        break;
                }
            }
    		    break;   
                
            case S_ELECTRODES_TOUCHED:
#ifndef NO_IMPEDANCE_MEASUREMENT
                    if(m_operationalMode == eOMnormal)
                    {
                        maternalImpedanceHandler(ipc.m_data1 == 0 ? false : true);
                    }
#endif

                break;
                
            case S_HR_MATERNAL_UPDATE:
                updateDisplay(eDcUpdateMaternalHR, ipc.m_data1);
                break;
            
            case S_BATTERY_FULL:
                m_displayDimTmr = CHARGE_MODE_DISPLAY_DIM_TIME; //Will un-dim display
                break;
                
            case S_BATTERY_REMAINING_CAPACITY:
                {
                    uint8_t rc = ipc.m_data1;
                    if(rc <= 100)
                    {
                        if(m_operationalMode == eOMnormal)
                        {
                            //Mask increasing values in operating mode. m_batteryRemainingCapacity was
                            //initially read from eeprom and low battery conditions was handled at startup.
                            //We don't want to indicate higher battery level at a later time.
                            
                            if(ipc.m_data1 < m_batteryRemainingCapacity)
                            {
                                m_batteryRemainingCapacity = ipc.m_data1;
                            }
                        }
                        else
                        {
                            m_batteryRemainingCapacity = ipc.m_data1;
                        }
                        
                        updateDisplay(eDcUpdateBattIndicator, m_batteryRemainingCapacity);
                            
                    }
                    
                    
                }
                break;
            
            case  S_BATTERY_EMPTY:
                if(m_operationalMode == eOMnormal)
                {
                    setDisplayMode(eDmBattEmpty);
                }
                
                break;
                
            
            case S_SYSTEM_SHUTDOWN:
                setDisplaySystemShutdownMode(ipc.m_data1 != 0 ? false : true);
                m_operationalMode = eOMshutdown;
                break;
            
                
            case S_SET_DISPLAY_ALARM:
                {
                    DEBUG_TRACE("\r\nUIThread::run - Display-alarm set : %u\r\n", ipc.m_data1);
                    Alarm_t alarm;
                    alarm.alarmByte = ipc.m_data1;
                    bool isAcknowledged = ipc.m_data2 == 1 ? true : false;
                    setDisplayAlarm(alarm, isAcknowledged); //Alarm should already be prioritized
                }
                break;
            
            case S_SET_AUDIO_ALARM:
                {
                    DEBUG_TRACE("\r\nUIThread::run - Audio-alarm set : %u\r\n", ipc.m_data1);
                    Alarm_t alarm;
                    alarm.alarmByte = ipc.m_data1;
                    setAudioAlarm(alarm);
                }
                break;
                
            case S_CLEAR_DISPLAY_ALARM:
                DEBUG_TRACE("\r\nUIThread::run - Display-alarm cleared : %u\r\n", ipc.m_data1);
                Alarm_t alarm;
                alarm.alarmByte = ipc.m_data1;
                clearDisplayAlarm(alarm); //Alarm should already be prioritized
                break;
                
            case S_CLEAR_AUDIO_ALARM:
                {
                    DEBUG_TRACE("\r\nUIThread::run - Audio-alarm cleared : %u\r\n", ipc.m_data1);
                    Alarm_t alarm;
                    alarm.alarmByte = ipc.m_data1;
                    clearAudioAlarm(alarm);
                }
                break;
                
            case S_USB_STATE:
                /*
                if(m_operationalMode == eOMDataTransfer)
                {
                    if(ipc.m_data1 == (uint32_t)CONFIGURED)
                    {
                        fadeBacklightIn();
                        
                        if(m_rsmModeEnabled)
                        {
                            m_clockComponent->showDate(true);
                            m_displayComponentsActive.clock = true;
                            m_clockComponent->invalidate();
                            m_clockComponent->visible(true);
                        }
                        
                        m_UsbSymbol->setXpos(177);
                        showUSBSymbol();
                    }
                    else if(ipc.m_data1 == (uint32_t)SUSPENDED)
                    {
                        fadeBacklightOut(false);
                    }
                }
                */
                break;
                
            case S_USB_POWERED:
                /*
                //A cable connection is detected while in operational mode.
                m_displayMode = eDmbatteryCharge;
                m_operationalMode = eOMDataTransfer;
                setDisplayBatteryChargeMode();
                */
                break;
            
            case S_WATCHDOG_KICK:
                if(m_operationalMode != eOMshutdown)
                {
                    //Safety for shutdown. If something hangs during
                    //shutdown, watchdog will cause shutdown
                    HwAbsl::getpHw()->getpWatchdog()->kick();
                }
                break;    
            
            case S_ECG_AMP_SENS_UPDATE:
                {
                    uint32_t tmp = ipc.m_data1;
                    float tmpf = *((float*)&tmp);   //convert from uint to float
                    
                    updateDisplayCfgTxt(eDcUpdateCfgEcgAmpSens, 0, tmpf);
    
                }
                break;
                
            case S_DEV_ID_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgDeviceIdentifier, 0, 0.0);
                break;

            case S_EPISODE_VALID_TIME_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgEpisodevalidTime, ipc.m_data1, 0.0);
                break;
            case S_HR_DISPUPDATE_TIME_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgHRupdateTime, ipc.m_data1, 0.0);
                break;
            case S_HR_BUF_LEN_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgHRavgBufLen, ipc.m_data1, 0.0);
                break;
            case S_END_OF_EPISODE_TIME_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgEndOfEpisodeTimeout, ipc.m_data1, 0.0);
                break;
            case S_TIME_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgTime, 0, 0.0);
                break;
            case S_DATE_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgDate, 0, 0.0);
                break;
            case S_ENABLE_CLOCK_UPDATE:
                if(ipc.m_data1 == CFG_DISABLED)
                {
                    m_showClock = false;
                    m_clockComponent->visible(false);
                    m_displayComponentsActive.clock = false;
                }
                else
                {
                    m_showClock = true;
                    if(m_displayComponentsActive.clock)
                    {
                        m_clockComponent->visible(true);
                        m_clockComponent->update();
                    }
                    
                }
                
                updateDisplayCfgTxt(eDcUpdateCfgShowClock, ipc.m_data1, 0.0);
                
                break;
            case S_ENABLE_RSM_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgResearchStorageMode, ipc.m_data1, 0.0);
                break;
            case S_LOST_SIG_AL_TIME_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgLostSignalAlarmTime, ipc.m_data1, 0.0);
                break;
            case S_FHR_ALARM_ABNORMAL_HIGH_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgFhrAbnormalAlarmHigh, ipc.m_data1, 0.0);
                break;
            case S_FHR_ALARM_ABNORMAL_LOW_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgFhrAbnormalAlarmLow, ipc.m_data1, 0.0);
                break;
            case S_FHR_ALARM_ATYPICAL_TIME_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgFhrAtypicalAlarmTime, ipc.m_data1, 0.0);
                break;
            case S_FHR_ALARM_ABNORMAL_TIME_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgFhrAbnormalAlarmTime, ipc.m_data1, 0.0);
                break;
            case S_FHR_ATYPICAL_ALARM_HIGH_UPDATE:
                if(ipc.m_data1 <= 250 && ipc.m_data1 >= 60)
                {
                    m_FhrAtypicalAlarmHigh = ipc.m_data1;
                }
                
                //Ensure gap between low and high
                if(m_FhrAtypicalAlarmLow > m_FhrAtypicalAlarmHigh - 10)
                {
                    m_FhrAtypicalAlarmLow = m_FhrAtypicalAlarmHigh - 10;
                }
                
                m_fetalHeartRateComponent->SetHrLimits(m_FhrAtypicalAlarmHigh, m_FhrAtypicalAlarmLow);
                
                m_HrBigTrendComponent->SetHrLimits(m_FhrAtypicalAlarmHigh, m_FhrAtypicalAlarmLow);
                
                updateDisplayCfgTxt(eDcUpdateCfgFhrAtypicalAlarmHigh, ipc.m_data1, 0.0);

                break;
                
            case S_FHR_ATYPICAL_ALARM_LOW_UPDATE:
                if(ipc.m_data1 >= 50 && ipc.m_data1 <= 240)
                {
                    m_FhrAtypicalAlarmLow = ipc.m_data1;
                }
                
                //Ensure gap between low and high
                if(m_FhrAtypicalAlarmLow > m_FhrAtypicalAlarmHigh - 10)
                {
                    m_FhrAtypicalAlarmHigh = m_FhrAtypicalAlarmLow + 10;
                }
                
                m_fetalHeartRateComponent->SetHrLimits(m_FhrAtypicalAlarmHigh, m_FhrAtypicalAlarmLow);
                
                m_HrBigTrendComponent->SetHrLimits(m_FhrAtypicalAlarmHigh, m_FhrAtypicalAlarmLow);
                
                updateDisplayCfgTxt(eDcUpdateCfgFhrAtypicalAlarmLow, ipc.m_data1, 0.0);

                break;
            
            case S_EP_TOO_SHORT_TIME_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgEpisodeTooShortTime, ipc.m_data1, 0.0);
                break;
            
            case S_ENABLE_SD_CARD_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgSDcardFunctionality, ipc.m_data1, 0.0);
                break;
                
            case S_DOPPLER_SOUND_SPEAKER_GAIN_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgDopplerSoundSpeakerGain, ipc.m_data1, 0.0);
                break;
                
            case S_PROBE_SOUND_GAIN_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgProbeSoundGain, ipc.m_data1, 0.0);
                break;
            
            case S_ENABLE_FHR_FILTER_UPDATE:
                updateDisplayCfgTxt(eDcUpdateCfgFhrFilter, ipc.m_data1, 0.0);
                break;
            
            case S_FADE_BACKLIGHT:
                DEBUG_TRACE("\r\nS_FADE_BACKLIGHT\r\n");
                if(ipc.m_data1 == 0)
                {
                    fadeBacklightOut(ipc.m_data2);
                }
                else
                {
                    fadeBacklightIn();
                }
                break;
            
            case S_SET_OPERATIONAL_MODE:
                switch((OperationalMode_t)ipc.m_data1)
                {
                    case eOMnormal:
                    case eOMDataTransfer:
                    case eOMconfig:
                    case eOMbatteryCharge:
                    case eOMbatteryEmpty:
                    case eOMcriticalError:
                    case eOMshutdown:
                        break;
                    case eOMtestMode:
                        m_operationalMode = (OperationalMode_t)ipc.m_data1;
                        m_displayMode = eDmTest;
                        setDisplayMode(eDmTest);
                        break;
                    default:
                        break;
                }
                break;    
            case S_TEST_COMMAND_POST:
                if(m_operationalMode == eOMtestMode) //Only allow POST from TEST_POST command if in test mode
                {
                    post(false);
                }
                break;
            case S_TEST_COMMAND_DISPLAY:
                if(ipc.m_data1 == 1)
                {
                    displayTest(true);
                }
                else
                {
                    displayTest(false);
                }
                break;
               
#ifdef DEBUG_DISPLAY_ALIGNMENT
            case S_DEBUG_DRAW_HOR_LINE:
                drawHorLine(ipc.m_data1, ipc.m_data2);
                break;
            case S_DEBUG_DRAW_VERT_LINE:
                drawVertLine(ipc.m_data1, ipc.m_data2);
                break;
#endif

            default:
				//Handle unrecognized signals
				sigUnknown(&ipc);
                break;
        }
    }
}

/*
===============================================================================
    updateDisplay()

    Update display (if component is active)

===============================================================================
 */
void UIThread::updateDisplay(DisplayCmds_t a_cmd, uint16_t a_value)
{
    switch(a_cmd)
    {
        case eDcUpdateMaternalHR:
            if(m_displayComponentsActive.maternalHR)
            {
                m_motherHeartRateComponent->setHR(a_value);
            }
            break;
        case eDcUpdateFetalHR:
            if(m_displayComponentsActive.fetalHR && 
               m_infoSymbolActive == eISnone)
            {
                m_fetalHeartRateComponent->setHR(a_value);
                FhrLedUpdate(m_currentFetalHR);
            }
            break;
        case eDcUpdateSoundVolume:
            if(m_displayComponentsActive.loadspeaker)
            {
                showVolumeSymbol(m_soundVolume);
            }
            break;
        case eDcUpdateFhrTrend:
            m_HrBigTrendComponent->Add(a_value);
            break;
        case eDcUpdateClock:
            if(m_displayComponentsActive.clock)
            {
                m_clockComponent->update();
            }
            break;
        case eDcUpdateBattIndicator:
            if(m_displayComponentsActive.bigBattIndicator)
            {
                m_bigBatteryCapacityComponent->setRemCapacity(a_value);
            }
            if(m_displayComponentsActive.smallBattIndicator)
            {
                if(m_displayMode == eDmConfig)
                {
                    m_batteryCapacityComponent->charging(true);
                }
                
                m_batteryCapacityComponent->setRemCapacity(a_value);
            }
            break;
        case eDcUpdateChargingIndicator:
            if(m_displayComponentsActive.bigBattIndicator)
            {
                m_bigBatteryCapacityComponent->setChargingState((bool)a_value);
            }
            break;
        default:
            break;
        
    }
}

/*
===============================================================================
    updateDisplayCfgTxt()

    Update config text on display (in config mode)

===============================================================================
 */
void UIThread::updateDisplayCfgTxt(DisplayCmds_t a_cmd, uint16_t a_uiValue, float a_fvalue)
{
    char txt[50];
                    
    if(m_displayMode == eDmConfig)
    {
     
        switch(a_cmd)
        {
            case eDcUpdateCfgDeviceIdentifier:
                {
                    std::string devId = Config::getpConfig()->GetConfigStr(Config::eCfgDeviceIdentifier);
                    sprintf(txt, "\n%s%s\0", TXT_CFG_DEV_ID, devId.c_str());
                }
                break;
            case eDcUpdateCfgTime:
                {
                    LL_RTC_TimeTypeDef time = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
                    sprintf(txt, "\n%s%0.2u:%0.2u:%0.2u\0", TXT_CFG_TIME, time.Hours, time.Minutes, time.Seconds);
                }
                break;
            case eDcUpdateCfgDate:
                {
                    LL_RTC_DateTypeDef date = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
                    sprintf(txt, "\n%s%0.2u.%0.2u.20%0.2u\0", TXT_CFG_DATE, date.Day, date.Month, date.Year);
                }
                break;
            case eDcUpdateCfgEcgAmpSens:
                sprintf(txt, "\n%s%f\0", TXT_CFG_AMP_SENS, a_fvalue);
                break;
            case eDcUpdateCfgEpisodevalidTime:
                sprintf(txt, "\n%s%u\0", TXT_CFG_EVT, a_uiValue);
                break;
            case eDcUpdateCfgEndOfEpisodeTimeout:
                sprintf(txt, "\n%s%u\0", TXT_CFG_EOET, a_uiValue);
                break;
            case eDcUpdateCfgHRupdateTime:
                sprintf(txt, "\n%s%u\0", TXT_CFG_HRUT, a_uiValue);
                break;
            case eDcUpdateCfgHRavgBufLen:
                sprintf(txt, "\n%s%u\0", TXT_CFG_AVG_BL, a_uiValue);
                break;
            case eDcUpdateCfgLostSignalAlarmTime:
                sprintf(txt, "\n%s%u\0", TXT_CFG_LSAT, a_uiValue);
                break;
            case eDcUpdateCfgFhrAtypicalAlarmHigh:
                sprintf(txt, "\n%s%u\0", TXT_CFG_FHR_AT_LH, a_uiValue);
                break;
            case eDcUpdateCfgFhrAtypicalAlarmLow:
                sprintf(txt, "\n%s%u\0", TXT_CFG_FHR_AT_LL, a_uiValue);
                break;
            case eDcUpdateCfgFhrAbnormalAlarmHigh:
                sprintf(txt, "\n%s%u\0", TXT_CFG_FHR_AB_LH, a_uiValue);
                break;
            case eDcUpdateCfgFhrAbnormalAlarmLow:
                sprintf(txt, "\n%s%u\0", TXT_CFG_FHR_AB_LL, a_uiValue);
                break;
            case eDcUpdateCfgFhrAtypicalAlarmTime:
                sprintf(txt, "\n%s%u\0", TXT_CFG_FHR_AT_T, a_uiValue);
                break;
            case eDcUpdateCfgFhrAbnormalAlarmTime:
                sprintf(txt, "\n%s%u\0", TXT_CFG_FHR_AB_T, a_uiValue);
                break;
            case eDcUpdateCfgResearchStorageMode:
                if(a_uiValue == 0)
                {
                    sprintf(txt, "\n%s%s\0", TXT_CFG_RSM, TXT_DISABLED);
                }
                else if(a_uiValue == 1)
                {
                    sprintf(txt, "\n%s%s\0", TXT_CFG_RSM, TXT_ENABLED);
                }
                break;
            case eDcUpdateCfgShowClock:
                if(a_uiValue == 0)
                {
                    sprintf(txt, "\n%s%s\0", TXT_CFG_SHOW_CLOCK, TXT_DISABLED);
                }
                else if(a_uiValue == 1)
                {
                    sprintf(txt, "\n%s%s\0", TXT_CFG_SHOW_CLOCK, TXT_ENABLED);
                }
                break;
            case eDcUpdateCfgEpisodeTooShortTime:
                sprintf(txt, "\n%s%u\0", TXT_CFG_EPTS, a_uiValue);
                break;
            case eDcUpdateCfgSDcardFunctionality:
                if(a_uiValue == 0)
                {
                    sprintf(txt, "\n%s%s\0", TXT_CFG_SD_CARD, TXT_DISABLED);
                }
                else if(a_uiValue == 1)
                {
                    sprintf(txt, "\n%s%s\0", TXT_CFG_SD_CARD, TXT_ENABLED);
                }
                break;
            case eDcUpdateCfgDopplerSoundSpeakerGain:
                sprintf(txt, "\n%s%u\0", TXT_CFG_SPKR_GAIN, a_uiValue);
                break;
            case eDcUpdateCfgProbeSoundGain:
                if(a_uiValue == 0)
                {
                    sprintf(txt, "\n%s%s\0", TXT_CFG_PROBE_GAIN, TXT_4X);
                }
                else if(a_uiValue == 1)
                {
                    sprintf(txt, "\n%s%s\0", TXT_CFG_PROBE_GAIN, TXT_1X);
                }
                break;
            case eDcUpdateCfgFhrFilter:
                if(a_uiValue == 0)
                {
                    sprintf(txt, "\n%s%s\0", TXT_CFG_FHR_FILTER, TXT_DISABLED);
                }
                else if(a_uiValue == 1)
                {
                    sprintf(txt, "\n%s%s\0", TXT_CFG_FHR_FILTER, TXT_ENABLED);
                }
                break;
            
            default:
                break;
        }                
        
        if(m_displayInterfaceOK)
        {
            gselvp(eVPCharging);
            gsetcolorf(G_WHITE);
            gsetcolorb(G_BLACK);
            gputs(txt);
        }
    }    
}

/*
===============================================================================
    setAllDisplayComponentsInactive()

    Inactivate all display components

===============================================================================
 */
void UIThread::setAllDisplayComponentsInactive(void)
{
    m_displayComponentsActive.loadspeaker = false;
    m_displayComponentsActive.smallBattIndicator = false;
    m_displayComponentsActive.bigBattIndicator = false;
    m_displayComponentsActive.fetalHR = false;
    m_displayComponentsActive.maternalHR = false;
    m_displayComponentsActive.fhrTrend = false;
    m_displayComponentsActive.bigFhrTrend = false;
    m_displayComponentsActive.clock = false;
    m_displayComponentsActive.fhrLed = false;
    m_displayComponentsActive.criticalError = false;
    HwAbsl::getpHw()->setGreenLED(false);
    HwAbsl::getpHw()->setRedLED(false);
    m_fetalHeartRateComponent->visible(false);
    m_motherHeartRateComponent->visible(false);
    m_batteryCapacityComponent->visible(false);
    m_clockComponent->visible(false);
    m_infoSymbolActive = eISnone;
}

/*
===============================================================================
    clearTrendArea()

    Clear trend viewport

===============================================================================
 */
void UIThread::clearTrendArea(void)
{
    m_displayComponentsActive.bigBattIndicator = false;
    m_displayComponentsActive.fetalHR = false;
    m_displayComponentsActive.maternalHR = false;
    m_displayComponentsActive.fhrTrend = false;
    m_displayComponentsActive.bigFhrTrend = false;
    m_displayComponentsActive.criticalError = false;
    m_fetalHeartRateComponent->visible(false);
    m_motherHeartRateComponent->visible(false);
    m_HrBigTrendComponent->visible(false);
    m_infoSymbolActive = eISnone;
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPTrend);
        gsetcolorf(G_WHITE);
        gsetcolorb(G_BLACK);
        gclrvp();
    }
    
}

/*
===============================================================================
    clearMHRArea()

    Clear maternal heart rate viewport
===============================================================================
 */
void UIThread::clearMHRArea(void)
{
    m_displayComponentsActive.bigBattIndicator = false;
    m_displayComponentsActive.maternalHR = false;
    m_displayComponentsActive.fhrTrend = false;
    m_displayComponentsActive.bigFhrTrend = false;
    m_displayComponentsActive.criticalError = false;
    m_motherHeartRateComponent->visible(false);
    m_infoSymbolActive = eISnone;
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPmaternalHeartRate);
        gsetcolorf(G_WHITE);
        gsetcolorb(G_BLACK);
        gclrvp();
    }
}

/*
===============================================================================
    clearFHRArea()

    Clear fetal heart rate viewport
===============================================================================
 */
void UIThread::clearFHRArea(void)
{
    m_displayComponentsActive.bigBattIndicator = false;
    m_displayComponentsActive.fetalHR = false;
    m_displayComponentsActive.fhrTrend = false;
    m_displayComponentsActive.bigFhrTrend = false;
    m_displayComponentsActive.criticalError = false;
    m_fetalHeartRateComponent->visible(false);
    m_infoSymbolActive = eISnone;
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPfetalHeartRate);
        gsetcolorf(G_WHITE);
        gsetcolorb(G_BLACK);
        gclrvp();
    }
}

/*
===============================================================================
    clearStatusArea()

    Clear status viewport
===============================================================================
 */
void UIThread::clearStatusArea(void)
{
    m_displayComponentsActive.loadspeaker = false;
    m_displayComponentsActive.smallBattIndicator = false;
    m_displayComponentsActive.fhrLed = false;
    
    if(m_showClock)
    {
        m_displayComponentsActive.clock = false;
        m_clockComponent->invalidate();
        m_clockComponent->visible(false);
    }
    
    m_batteryCapacityComponent->visible(false);
    
    HwAbsl::getpHw()->setGreenLED(false);
    HwAbsl::getpHw()->setRedLED(false);
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPstatus);
        gsetcolorf(G_WHITE);
        gsetcolorb(G_BLACK);
        gclrvp();
    }
}

/*
===============================================================================
    setStatusArea()

    Show and update all components on status viewport
    Update FHR LED
===============================================================================
 */
void UIThread::setStatusArea(void)
{
    m_displayComponentsActive.loadspeaker = true;
    m_displayComponentsActive.smallBattIndicator = true;
    m_displayComponentsActive.fhrLed = true;
    m_displayComponentsActive.fetalHR = true;
    
    if(m_showClock)
    {
        m_displayComponentsActive.clock = true;
        m_clockComponent->invalidate();
        m_clockComponent->visible(true);
    }
    
    if(m_currentDisplayAlarm.alarmByte)
    {
        showAlarmSymbol(m_alarmAcknowlegde);
    }
    
    m_batteryCapacityComponent->invalidate();
    m_batteryCapacityComponent->visible(true);
    m_batteryCapacityComponent->setRemCapacity(m_batteryRemainingCapacity);

    showVolumeSymbol(m_soundVolume);

    FhrLedUpdate(m_currentFetalHR);
    
}

/*
===============================================================================
    setDisplayFhrTrendMode()

    Clear trend viewport and set display in trend mode
===============================================================================
 */
void UIThread::setDisplayFhrTrendMode(void)
{
    
    m_displayComponentsActive.loadspeaker = true;
    m_displayComponentsActive.smallBattIndicator = true;
    m_displayComponentsActive.bigFhrTrend = true;
    m_displayComponentsActive.fhrLed = true;
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPTrend);
        gsetcolorf(G_GREEN);
        gsetcolorb(G_BLACK);
        gclrvp();
    }
    
    if(m_showClock)
    {
        //Trend use some of the clock area. Hide clock
        m_clockComponent->visible(false);
    }
    
    m_HrBigTrendComponent->visible(true);
    m_HrBigTrendComponent->draw();
    FhrLedUpdate(m_currentFetalHR);
    
}


/*
===============================================================================
    setDisplayFhrHrMode()

    Set display in fetal heart rate mode 
===============================================================================
 */
void UIThread::setDisplayFhrHrMode(void)
{
    m_displayComponentsActive.fetalHR = true;
    
    m_fetalHeartRateComponent->invalidate();
    m_fetalHeartRateComponent->setHR(m_currentFetalHR);
    m_fetalHeartRateComponent->visible(true);
    
}

/*
===============================================================================
    setDisplayMaternalHrMode()

    Clear maternal hr viewport and set display in maternal hr mode
===============================================================================
 */
void UIThread::setDisplayMaternalHrMode(void)
{
    if(m_infoSymbolActive != eISnone)
    {
        //This symbol extends over MHR area. Clear before updating MHR
        clearInfoSymbol();
    }
    
    clearMHRArea();
    
    m_displayComponentsActive.maternalHR = true;
    m_motherHeartRateComponent->invalidate();
    m_motherHeartRateComponent->setHR(0); //-?-
    m_motherHeartRateComponent->visible(true);
    m_motherHeartRateComponent->draw();
    
}

/*
===============================================================================
    setDisplayBatteryEmptyMode()

    Clear display and set display in battery empty mode
===============================================================================
 */
void UIThread::setDisplayBatteryEmptyMode(void)
{
    setAllDisplayComponentsInactive();
    
    clearScreen();
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPCharging);
    }
    
    m_batteryEmptyBigSymbol->invalidate();
    m_batteryEmptyBigSymbol->draw();
    
    
}


/*
===============================================================================
    setDisplayCriticalErrorMode()

    Clear display and set display in critical error mode
    Setup blinking of critical error symbol
    Show post code if enabled
===============================================================================
 */
void UIThread::setDisplayCriticalErrorMode(bool a_showPostCode)
{
    setAllDisplayComponentsInactive();
    
    m_operationalMode = eOMcriticalError;
    
    m_displayComponentsActive.criticalError = true;
    
    clearScreen();
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPCharging);
    }
    
    if(m_critcalErrorSymbol == 0)
    {
        m_critcalErrorSymbol = new Symbol(1, 0, 0);
        m_critcalErrorSymbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::criticalError));
        m_critcalErrorSymbol->setPos( 70, 23 ); 
    }
    
    gsetcolorf(G_BLACK);
    gsetcolorb(G_RED);
    m_critcalErrorSymbol->draw(); 
    m_symbolBlinkTimer = CRITICAL_ERR_BLINK_ON_TIME;
    m_symbolRemBlinks = CRITICAL_ERR_NO_BLINKS;
    
    if(a_showPostCode)
    {
        showPostCode(m_postCtrl.postResult);
    }
    
}

/*
===============================================================================
    setDisplaySystemShutdownMode()

    Set display in shut down mode
    Show logo if enabled
===============================================================================
 */
void UIThread::setDisplaySystemShutdownMode(bool a_showLogo)
{
    setAllDisplayComponentsInactive();
    
    //Turn off alarms
    m_currentDisplayAlarm.alarmByte = 0;
    m_currentAudioAlarm.alarmByte = 0;
    
    if(a_showLogo)
    {
        showLGHlogo();
    }
    
}

/*
===============================================================================
    setDisplayBatteryChargeMode()

    Clear display and set display in battery charge mode
===============================================================================
 */
void UIThread::setDisplayBatteryChargeMode(void)
{
    setAllDisplayComponentsInactive();
    
    clearScreen();
    
    m_displayComponentsActive.bigBattIndicator = true;
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPCharging);
    } 
    
    m_bigBatteryCapacityComponent->invalidate();
    m_bigBatteryCapacityComponent->setChargingState(true);
    m_bigBatteryCapacityComponent->setRemCapacity(m_batteryRemainingCapacity);
    m_bigBatteryCapacityComponent->visible(true);
    
    //Turn off FHR LED
    HwAbsl::getpHw()->updateFhrBlinkRate(0, HwAbsl::eFhrLEdColorOff);
    
    
    
}

/*
===============================================================================
    setDisplayTestMode()

    Clear display and set display in production test mode
===============================================================================
 */
void UIThread::setDisplayTestMode(void)
{
    setAllDisplayComponentsInactive();
   
    if(m_displayInterfaceOK)
    {
        gselvp(eVPstatus);
        gsetcolorf(G_WHITE);
        gsetcolorb(G_BLACK);
        gclrvp();
    
        gselvp(eVPCharging);
        gsetcolorf(G_WHITE);
        gsetcolorb(G_BLACK);
        gclrvp();
        
        gselvp(eVPstatus);
        gsetpos(10,19);
        gputs(TXT_TEST_MODE);
    }
    
    m_displayComponentsActive.smallBattIndicator = true;
    
    
    m_batteryCapacityComponent->invalidate();
    m_batteryCapacityComponent->visible(true);
    m_batteryCapacityComponent->charging(true);
    
    m_UsbSymbol->setXpos(150);
    showUSBSymbol();
    
    //Turn off FHR LED
    HwAbsl::getpHw()->updateFhrBlinkRate(0, HwAbsl::eFhrLEdColorOff);
}

/*
===============================================================================
    setDisplayConfigMode()

    Clear display and set display in config mode
===============================================================================
 */
void UIThread::setDisplayConfigMode(void)
{
    setAllDisplayComponentsInactive();
   
    clearScreen();
    
    m_displayComponentsActive.smallBattIndicator = true;
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPstatus);
        gsetpos(10,19);
        gputs(TXT_CONFIG_MODE);
    }
    
    m_batteryCapacityComponent->invalidate();
    m_batteryCapacityComponent->visible(true);
    m_batteryCapacityComponent->charging(true);
    
    m_UsbSymbol->setXpos(150);
    showUSBSymbol();
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPCharging);
        gclrvp();
        gsetvp(0, 24, 219, 167);
        gsetpos(0, 0);
        gselfont(&mono5_8);
    }
        
    
    //Turn off FHR LED
    HwAbsl::getpHw()->updateFhrBlinkRate(0, HwAbsl::eFhrLEdColorOff);
}

/*
===============================================================================
    setDisplayMode()

    Set display in a mode if allowed (dependent of current display mode).
    
===============================================================================
 */
void UIThread::setDisplayMode(DisplayMode_t a_mode)
{
    //First check if we can change mode
    switch(m_displayMode)
    {
        case eDmFhrTrend:
        case eDmFhrHr:
        case eDmElectrodesTouched:
        {
            switch(a_mode)
            {
                case eDmFhrTrend:
                    setDisplayFhrTrendMode();
                    break;
                
                case eDmElectrodesTouched:
                    setDisplayMaternalHrMode();
                    break;
                    
                case eDmBattEmpty:
                    m_operationalMode = eOMbatteryEmpty;
                    m_displayMode = eDmBattEmpty;
                    m_batteryEmptyDisplayState = eBEinit;
                    break;
                    
                case eDmFhrHr:
                    setDisplayFhrHrMode();
                    break;
                
                case eDmCriticalError:
                    m_operationalMode = eOMcriticalError;
                    setDisplayCriticalErrorMode(false);
                    break;
                    
                case eDmSystemShutdown:
                    m_operationalMode = eOMshutdown;
                    setDisplaySystemShutdownMode(true);
                    break;
                    
                default:
                    break;
            }
            m_displayMode = a_mode;
        }
            break;
            
        case eDmbatteryCharge:
            setDisplayBatteryChargeMode();
            break;
        case eDmConfig:
            setDisplayConfigMode();
            break;
        case eDmTest:
            setDisplayTestMode();
            break;
        case eDmBattEmpty:
            m_operationalMode = eOMbatteryEmpty;
            m_displayMode = eDmBattEmpty;
            m_batteryEmptyDisplayState = eBEinit;
            break;
        default:
            break;
    }
}

/*
===============================================================================
    updateVolume()

    Logic for handle volume symbol if volume button was pressed
    Show high/low volume and alarm acknowlegde  based on current alarm state
    Handle display dimming and log changes in volume
===============================================================================
 */
void UIThread::updateVolume()
{
    if(!m_currentDisplayAlarm.alarmBits.criticalErrorAlarm)
    {
        bool alarmAckUpdate = false;
        
        if(m_currentAudioAlarm.alarmByte > 0 && 
           !m_alarmAcknowlegde &&
           !m_currentAudioAlarm.alarmBits.criticalErrorAlarm)
        {
            //Alarm was acknowlegded. Show symbol
            showAlarmSymbol(true);
            showInfoSymbol(eISAlarmAck);
            sigDispatch(P_SUPER, S_ALARM_ACKNOWLEGDE, 1, 0, 0);
            alarmAckUpdate = true;
            m_alarmAcknowlegde = true;
            Events::getpEvents()->AddEvent(Events::evtAlarmAcknowledged, 
                                           Events::TO_EPISODE_LOG,
                                           1.0);
        }
        
        else if(m_soundVolume == eSVoff)
        {
            m_soundVolume = eSVhigh; 
        }
        else
        {
            m_soundVolume = eSVoff;
        }
        
        if(!alarmAckUpdate)
        {
            HwAbsl::getpHw()->getpAudio()->SetSoundVol(m_soundVolume);
        
            showVolumeSymbol(m_soundVolume);
            
            Events::getpEvents()->AddEvent(Events::evtVolume, 
                                           Events::TO_EPISODE_LOG,
                                           (float)m_soundVolume);
        }
        if(!m_currentAudioAlarm.alarmByte && m_soundVolume == eSVoff)
        {
            //Only dim display if sound off and no alarm active
            displayDim(true);
        }
        else
        {
            displayDim(false);
        }
    }
}

/*
===============================================================================
    setFullVolume()

    Set audio volume to full
    Update volume symbol
===============================================================================
 */
void UIThread::setFullVolume(void)
{
    m_soundVolume = eSVhigh;
    HwAbsl::getpHw()->getpAudio()->SetSoundVol(m_soundVolume);
    showVolumeSymbol(m_soundVolume);
}

/*
===============================================================================
    maternalImpedanceHandler()

    Handle maternal FHR area when someone touches/release electrodes
    based on current display mode
    Handle dimming of display
===============================================================================
 */
void UIThread::maternalImpedanceHandler(bool a_electrodesTouched)
{
    if(a_electrodesTouched && 
       m_displayMode != eDmElectrodesTouched && 
       !m_DisplayModeKeyPressed)
    {
        //Undim display
        displayDim(false);
        
        //Change mode temporarely, save state
        m_ecgElectrodesTouchedPrevDisplayMode = m_displayMode;
        setDisplayMode(eDmElectrodesTouched);
        DEBUG_TRACE("\r\nDisp mode: %u (Elec touched)\r\n", m_displayMode);
    }
    else if(!a_electrodesTouched && 
            !m_DisplayModeKeyPressed)
    {
        //Change back to what it was
        clearMHRArea();
        setDisplayMode(m_ecgElectrodesTouchedPrevDisplayMode);
        DEBUG_TRACE("\r\nDisp mode: %u (Elec not touched)\r\n", m_displayMode);
        
        //Restore dim of display if sound off
        if(m_soundVolume == eSVoff && m_currentDisplayAlarm.alarmByte == 0)
        {
            displayDim(true);
        }
        
    }
    
}

/*
===============================================================================
    FhrLedUpdate()

    Handle status LED based on current FHR and alarm state
===============================================================================
 */
void UIThread::FhrLedUpdate(uint8_t a_hr)
{
    if(m_displayComponentsActive.fhrLed)
    {
        if(m_currentDisplayAlarm.alarmBits.lostSignalAlarm)
        {
            //Lost signal alarm set, Led should be off
            HwAbsl::getpHw()->updateFhrBlinkRate(a_hr, HwAbsl::eFhrLEdColorOff);
        }
        else if(m_currentAudioAlarm.alarmBits.abnormalFHRalarm)
        {
            if(a_hr >= 50)
            {
                //If hr < 50, LED should blink with last set rate
                HwAbsl::getpHw()->updateFhrBlinkRate(a_hr, HwAbsl::eFhrLEdColorRed);
            }
        }
        else if(a_hr < 50)
        {
            HwAbsl::getpHw()->updateFhrBlinkRate(a_hr, HwAbsl::eFhrLEdColorOff);
        }
        else if(a_hr < m_FhrAtypicalAlarmLow || a_hr > m_FhrAtypicalAlarmHigh)
        {
            HwAbsl::getpHw()->updateFhrBlinkRate(a_hr, HwAbsl::eFhrLEdColorYellow);
        }
        else
        {
            HwAbsl::getpHw()->updateFhrBlinkRate(a_hr, HwAbsl::eFhrLEdColorGreen);
        }
    }
}

/*
===============================================================================
    displayDim()

    Initiate dim or undim of backlight
    Dim should be gradually. Undim should be promptly
===============================================================================
 */
bool UIThread::displayDim(bool a_enableDim)
{
    bool wasDimmed = m_backlightDimmed; //Current state
    
    if(a_enableDim)
    {
        m_displayIsDimming = true;
        m_backlightDimmed = true;
    }
    else
    {
        m_PWMdcBacklight = BACKLIGHT_FULL;
        m_displayIsDimming = false;
        m_backlightDimmed = false;
    }
    
    HwAbsl::getpHw()->setDutycycleBackLight(m_PWMdcBacklight);
    
    return wasDimmed;
}

/*
===============================================================================
    displayDimHandler()

    Handle dimming of backlight
    Dim should be gradually.
===============================================================================
 */
void UIThread::displayDimHandler(void)
{
    if(m_displayIsDimming)
    {
        if(m_PWMdcBacklight > BACKLIGHT_DIM)
        {
            m_PWMdcBacklight -= BACKLIGHT_DIM_STEP;
            HwAbsl::getpHw()->setDutycycleBackLight(m_PWMdcBacklight);
        }
        else
        {
            HwAbsl::getpHw()->setDutycycleBackLight(BACKLIGHT_DIM);
            m_displayIsDimming = false;
        }
    }
}

/*
===============================================================================
    fadeBacklightOut()

    Fade backlight from full to off gradually
    Initiate shutdown afterwards if enabled
===============================================================================
 */
void UIThread::fadeBacklightOut(bool a_powerOffAfterwards)
{
    HwAbsl::getpHw()->setDutycycleBackLight(BACKLIGHT_FULL);
        
    for(int16_t pwmVal = BACKLIGHT_FULL; pwmVal > BACKLIGHT_START; pwmVal -= BACKLIGHT_FADE_STEP)
    {
        if(pwmVal < 0)
        {
            pwmVal = 0;
        }
        
        HwAbsl::getpHw()->setDutycycleBackLight((uint16_t)pwmVal);
        sleep(THREAD_TICK_TIME);
    }
    
    HwAbsl::getpHw()->setDutycycleBackLight(0);
    
    if(a_powerOffAfterwards)
    {
        //Shut audio off to avoid click
        HwAbsl::getpHw()->audioOff();
        
        //Power off
        HwAbsl::getpHw()->systemShutDown();
    }
}

/*
===============================================================================
    fadeBacklightIn()

    Increase backlight from off to full
===============================================================================
 */
void UIThread::fadeBacklightIn(void)
{
    HwAbsl::getpHw()->setDutycycleBackLight(BACKLIGHT_START);
        
    for(int16_t pwmVal = BACKLIGHT_START; pwmVal < BACKLIGHT_FULL; pwmVal += BACKLIGHT_FADE_STEP)
    {
        if(pwmVal > DC_100_PERC)
        {
            pwmVal = DC_100_PERC;
        }
        
        HwAbsl::getpHw()->setDutycycleBackLight((uint16_t)pwmVal);
        sleep(THREAD_TICK_TIME);
    }
        
    HwAbsl::getpHw()->setDutycycleBackLight(BACKLIGHT_FULL);
}

/*
===============================================================================
    showLGHlogo()

    Clear screen and show Laerdal Global Health logo
===============================================================================
 */
void UIThread::showLGHlogo(void)
{
    if(m_displayInterfaceOK)
    {
        Symbol* LGHlogo = new Symbol(1, 0, 0);
        LGHlogo->add( reinterpret_cast<PGSYMBOL>(&SymLib::LGHlogo));
            
        //Clear screen
        gselvp(eVPstatus);
        gresetposvp(); //Set vp to use full display area
        gsetcolorb(G_BLACK);
        gclrvp();
        
        LGHlogo->setPos(33,57);
        LGHlogo->setActive(0);
        LGHlogo->draw();
        
        delete LGHlogo;
    }
}



/*
===============================================================================
    showVolumeSymbol()

    Show volume symbol in status viewport
===============================================================================
 */
void UIThread::showVolumeSymbol(uint8_t a_volume)
{
    if(m_displayInterfaceOK)
    {
        gselvp(eVPstatus);
        gsetcolorb(G_BLACK);
        gsetcolorf(G_WHITE);
        
        m_volumeSymbol->invalidate();
        
        if(a_volume)
        {
            m_volumeSymbol->setActive(1);
        }
        else
        {
            m_volumeSymbol->setActive(0);
        }
        
        m_volumeSymbol->draw();
    }
}   

/*
===============================================================================
    showAlarmSymbol()

    Show alarm symbol in status viewport
===============================================================================
 */
void UIThread::showAlarmSymbol(bool a_acked)
{
    
    m_alarmSymbol->invalidate();
    
    if(a_acked)
    {
        m_alarmSymbol->setActive(1); 
    }
    else
    {
        m_alarmSymbol->setActive(0);
    }
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPstatus);
        gsetcolorb(G_BLACK);
        gsetcolorf(G_WHITE);    
        m_alarmSymbol->draw();
    }
}

/*
===============================================================================
    showBatteryLowSymbol()

    Clear display and show battery low symbol
===============================================================================
 */
void UIThread::showBatteryLowSymbol(void)
{
    if(m_displayInterfaceOK)
    {
        gselvp(eVPCharging);
        gsetcolorb(G_BLACK);
        
        gclrvp();
        
        Symbol* bl = new Symbol(1, 0, 0);
        bl->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryLowBig));
        bl->setPos(46,27);
        bl->setActive(0);
        bl->draw();
        sleep(2000);
        gclrvp();
        
        delete bl;
    }
    
    
    
}

/*
===============================================================================
    showUSBSymbol()

    Show USB symbol in status viewport
===============================================================================
 */
void UIThread::showUSBSymbol(void)
{
    if(m_displayInterfaceOK)
    {
        gselvp(eVPstatus);
        gsetcolorb(G_BLACK);
        gsetcolorf(G_WHITE);
        
        m_UsbSymbol->invalidate();
        m_UsbSymbol->setActive(0);
        m_UsbSymbol->draw();
    }
}

/*
===============================================================================
    clearAlarmSymbol()

    Hide alarm symbol in status viewport
===============================================================================
 */
void UIThread::clearAlarmSymbol(void)
{
    if(m_displayInterfaceOK)
    {
        gselvp(eVPstatus);
        gsetcolorb(G_BLACK);
        gsetcolorf(G_WHITE);
        m_alarmSymbol->erase();
    }
}


/*
===============================================================================
    post()

    Post handler for all post's.
    Requests post results from modules that runs post test's
    Display FW versions and results (verbose mode)
    Log post result
    Report result (in test mode)
===============================================================================
 */
bool UIThread::post(bool a_verbose)
{
    uint16_t noRetriesLeft = 50;
    bool stop = false;
    bool err = false;
    char txt[20];
    
    m_postCtrl.configFinished = false;
    m_postCtrl.displayFinished = false;
    m_postCtrl.fuelGaugeFinished = false;
    m_postCtrl.probeFinished = false;
    m_postCtrl.ProgramMemChecksumFinished = false;
    m_postCtrl.RTCcrystalFinished = false;
    m_postCtrl.SDcardFinished = false;
    m_postCtrl.VoltageRegulatorFinished = false;
    
    if(a_verbose)
    {
        clearScreen();
        
        //Turn on backlight in case it is not already done
        displayDim(false);
        
        if(m_displayInterfaceOK)
        {
            gselvp(eVPCharging);
            gsetpos(0, 0);
            gselfont(&moyo_mono8_8);
            gsetcolorf(G_WHITE);
            gsetcolorb(G_BLACK);
            
            //Show Time/date
            LL_RTC_TimeTypeDef time = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
            LL_RTC_DateTypeDef date = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
            
            sprintf(txt, "%0.2u.%0.2u.20%0.2u", date.Day, date.Month, date.Year);
            gputs(TXT_DATE);
            gputs(txt);
            gputs("\n");
            
            sprintf(txt, "%0.2u:%0.2u", time.Hours, time.Minutes);
            gputs(TXT_TIME);
            gputs(txt);
            
            gputs("\n");
            
            
            //Display SW version
            gputs(TXT_MOYO_FW_VERSION);
            uint16_t len = sprintf(txt, "%d.%d.%d.%d\0", VER_MAJ, VER_MIN, VER_MAINTENANCE, VER_BUILD);
            gputs(txt);
            gputs("\n");
            gputs(TXT_PROBE_FW_VERSION);
            
            std::string moduleVersionInfo = HwAbsl::getpHw()->getProbeSerial()->GetModuleVersionInfo();
            uint8_t tmout = 20;
            while(moduleVersionInfo == "" && tmout > 0)
            {
                tmout--;
                sleep(100);
                moduleVersionInfo = HwAbsl::getpHw()->getProbeSerial()->GetModuleVersionInfo();
            }
            if(moduleVersionInfo == "")
            {
                gputs("No probe");
            }
            else
            {
                std::string swver(moduleVersionInfo, 13, 13);
                gputs(swver.c_str());
            }
            
            gputs("\n");
            gputs("\n");
        }
    }
    
    do
    {
        if(!m_postCtrl.displayFinished)
        {
            //Display POST if always finished at this point. Update status
            m_POSTstatus.finished = true;
            m_postCtrl.postResult.postBits.displayComm = m_POSTstatus.post.postBits.displayComm;
            m_postCtrl.displayFinished = true;
            
            if(m_postCtrl.postResult.postBits.displayComm)
            {
                err = true;
            }
        
            if(a_verbose)
            {
                if(!m_postCtrl.postResult.postBits.displayComm)
                {
                    if(m_displayInterfaceOK)
                    {
                        gputs(TXT_POST_DISPLAY);
                        gputs(TXT_PASSED);
                        gputs("\n");
                    }
                }
            }
            
        }
        
        if(!m_postCtrl.probeFinished)
        {
            
#ifdef NO_PROBE
            m_postCtrl.postResult.postBits.ultraSoundTransComm = 1;
            m_postCtrl.probeFinished = true;
#else
            
            POST_ctrl_t probe = HwAbsl::getpHw()->getProbeSerial()->getPostResult();
            if(probe.finished)
            {
                m_postCtrl.postResult.postBits.ultraSoundTransComm = probe.post.postBits.ultraSoundTransComm;
                m_postCtrl.probeFinished = true;
                
                if(m_postCtrl.postResult.postBits.ultraSoundTransComm)
                {
                    DEBUG_TRACE("\r\nProbe POST failed!\r\n");
                    err = true;
                }
                
                if(a_verbose)
                {
                    if(!m_postCtrl.postResult.postBits.ultraSoundTransComm)
                    {
                        gputs(TXT_POST_PROBE);
                        gputs(TXT_PASSED);
                        gputs("\n");
                    }
                }
            }
#endif
        }
        
        if(!m_postCtrl.configFinished)
        {
            if(Config::getpConfig()->IsValid())
            {   
                m_postCtrl.postResult.postBits.calibrationCheckSum = false;
            }
            else
            {
                m_postCtrl.postResult.postBits.calibrationCheckSum = true;
            }
            m_postCtrl.configFinished = true;
            if(m_postCtrl.postResult.postBits.calibrationCheckSum)
            {
                err = true;
            }
            
            if(a_verbose)
            {
                
                if(!m_postCtrl.postResult.postBits.calibrationCheckSum)
                {
                    gputs(TXT_POST_CONFIG);
                    gputs(TXT_PASSED);
                    gputs("\n");
                }
            }
        }
        
        if(!m_postCtrl.fuelGaugeFinished)
        {
            Th1Super *pSuperTh = reinterpret_cast<Th1Super*>(getpThread(P_SUPER));
            POST_ctrl_t fuelGauge = pSuperTh->getPostResult();
            if(fuelGauge.finished)
            {
                m_postCtrl.fuelGaugeFinished = true;
                m_postCtrl.postResult.postBits.fuelGaugeComm = fuelGauge.post.postBits.fuelGaugeComm;
                if(fuelGauge.post.postBits.fuelGaugeComm)
                {
                    err = true;
                }
                
                if(a_verbose)
                {
                    
                    if(!m_postCtrl.postResult.postBits.fuelGaugeComm)
                    {
                        gputs(TXT_POST_FUELGAUGE);
                        gputs(TXT_PASSED);
                        gputs("\n");
                    }
                }
            }
                
        }
        
        if(!m_postCtrl.VoltageRegulatorFinished)
        {
            Th1Super *pSuperTh = reinterpret_cast<Th1Super*>(getpThread(P_SUPER));
            POST_ctrl_t voltageRegRes = pSuperTh->getPostResult();
            if(voltageRegRes.finished)
            {
                m_postCtrl.VoltageRegulatorFinished = true;
                m_postCtrl.postResult.postBits.voltageRegulator = voltageRegRes.post.postBits.voltageRegulator;
                if(voltageRegRes.post.postBits.voltageRegulator)
                {
                    err = true;
                }
                
                if(a_verbose)
                {
                    
                    if(!m_postCtrl.postResult.postBits.voltageRegulator)
                    {
                        gputs(TXT_POST_5VREG);
                        gputs(TXT_PASSED);
                        gputs("\n");
                    }
                }
            }
                
        }
        
        if(!m_postCtrl.RTCcrystalFinished)
        {
            Th1Super *pSuperTh = reinterpret_cast<Th1Super*>(getpThread(P_SUPER));
            POST_ctrl_t rtcXtRes = pSuperTh->getPostResult();
            if(rtcXtRes.finished)
            {
                m_postCtrl.RTCcrystalFinished = true;
                m_postCtrl.postResult.postBits.RTCcrystal = rtcXtRes.post.postBits.RTCcrystal;
                if(rtcXtRes.post.postBits.RTCcrystal)
                {
                    err = true;
                }
                
                if(a_verbose)
                {
                    
                    if(!m_postCtrl.postResult.postBits.RTCcrystal)
                    {
                        gputs(TXT_POST_RTCXT);
                        gputs(TXT_PASSED);
                        gputs("\n");
                    }
                }
            }
                
        }
        
        
        if(!m_postCtrl.SDcardFinished)
        {
            FileThread *pFileTh = reinterpret_cast<FileThread*>(getpThread(P_FILE));
            POST_ctrl_t file = pFileTh->getPostResult();
            if(file.finished)
            {
                m_postCtrl.SDcardFinished = true;
                m_postCtrl.postResult.postBits.sdCard = file.post.postBits.sdCard;
                if(file.post.postBits.sdCard)
                {
                    err = true;
                }
                
                if(a_verbose)
                {
                    
                    if(!m_postCtrl.postResult.postBits.sdCard)
                    {
                        gputs(TXT_POST_SD_CARD);
                        gputs(TXT_PASSED);
                        gputs("\n");
                    }
                }
            }
                
        }
        
        if(!m_postCtrl.ProgramMemChecksumFinished)
        {
            POST_ctrl_t progMemCrc = HwAbsl::getpHw()->getPostResult();
            if(progMemCrc.finished)
            {
                m_postCtrl.ProgramMemChecksumFinished = true;
                m_postCtrl.postResult.postBits.programCheckSum = progMemCrc.post.postBits.programCheckSum;
                if(progMemCrc.post.postBits.programCheckSum)
                {
                    err = true;
                }
                
                if(a_verbose)
                {
                    
                    if(!progMemCrc.post.postBits.programCheckSum)
                    {
                        gputs(TXT_POST_FLASH);
                        gputs(TXT_PASSED);
                        gputs("\n");
                    }
                }
            }
                
        }
        
        if(m_postCtrl.displayFinished &&
           m_postCtrl.probeFinished &&
           m_postCtrl.configFinished &&
           m_postCtrl.fuelGaugeFinished &&
           m_postCtrl.SDcardFinished &&
           m_postCtrl.ProgramMemChecksumFinished)
        {
            stop = true;
        }
        
        sleep(100);
        noRetriesLeft--;
        
    } while(!stop && noRetriesLeft > 0);
    
    
    if(noRetriesLeft == 0)
    {
        //Timeout, find source
        if(!m_postCtrl.probeFinished)
        {
            m_postCtrl.postResult.postBits.ultraSoundTransComm = true;
            err = true;
        }
        
        if(!m_postCtrl.displayFinished)
        {
            m_postCtrl.postResult.postBits.displayComm = true;
            err = true;
        }
        
        if(!m_postCtrl.configFinished)
        {
            m_postCtrl.postResult.postBits.calibrationCheckSum = true;
            err = true;
        }
        
        if(!m_postCtrl.fuelGaugeFinished)
        {
            m_postCtrl.postResult.postBits.fuelGaugeComm = true;
            err = true;
        }
        
        if(!m_postCtrl.SDcardFinished)
        {
            m_postCtrl.postResult.postBits.sdCard = true;
            err = true;
        }
        
        if(!m_postCtrl.ProgramMemChecksumFinished)
        {
            m_postCtrl.postResult.postBits.programCheckSum = true;
            err = true;
        }
        if(!m_postCtrl.VoltageRegulatorFinished)
        {
            m_postCtrl.postResult.postBits.voltageRegulator = true;
            err = true;
        }
        if(!m_postCtrl.RTCcrystalFinished)
        {
            m_postCtrl.postResult.postBits.RTCcrystal = true;
            err = true;
        }
    }
    
    //Report error (if exists)
    if(a_verbose)
    {
        if(m_postCtrl.postResult.postBits.displayComm || !m_postCtrl.displayFinished)
        {
            gputs(TXT_POST_DISPLAY);
            gputs(TXT_FAILED);
            gputs("\n");
        }
        
        if(m_postCtrl.postResult.postBits.ultraSoundTransComm || !m_postCtrl.probeFinished)
        {
            gputs(TXT_POST_PROBE);
            gputs(TXT_FAILED);
            gputs("\n");        
        }
                    
        if(m_postCtrl.postResult.postBits.calibrationCheckSum || !m_postCtrl.configFinished)
        {
            gputs(TXT_POST_CONFIG);
            gputs(TXT_FAILED);
            gputs("\n");
        }
        
        if(m_postCtrl.postResult.postBits.fuelGaugeComm || !m_postCtrl.fuelGaugeFinished)
        {
            gputs(TXT_POST_FUELGAUGE);
            gputs(TXT_FAILED);
            gputs("\n");
        }
        
        if(m_postCtrl.postResult.postBits.sdCard || !m_postCtrl.SDcardFinished)
        {
            gputs(TXT_POST_SD_CARD);
            gputs(TXT_FAILED);
            gputs("\n");
        }
        
        if(m_postCtrl.postResult.postBits.programCheckSum || !m_postCtrl.ProgramMemChecksumFinished)
        {
            gputs(TXT_POST_FLASH);
            gputs(TXT_FAILED);
            gputs("\n");
        }
        if(m_postCtrl.postResult.postBits.voltageRegulator || !m_postCtrl.VoltageRegulatorFinished)
        {
            gputs(TXT_POST_5VREG);
            gputs(TXT_FAILED);
            gputs("\n");
        }
        if(m_postCtrl.postResult.postBits.RTCcrystal || !m_postCtrl.RTCcrystalFinished)
        {
            gputs(TXT_POST_RTCXT);
            gputs(TXT_FAILED);
            gputs("\n");
        }
        
        
        sleep(5000);
    }
    
    Events::getpEvents()->AddEvent(Events::evtPowerOnSelfTestResult, 
                                   Events::TO_EPISODE_LOG | Events::TO_SYSTEM_LOG,
                                   (float)m_postCtrl.postResult.postWord);
    
    if(m_displayMode == eDmTest)
    {
        sprintf(txt, "0x%0.2X", m_postCtrl.postResult.postWord);
        CONSOLE_PRINT("R101 %s\r\n", txt);
    }
        
    
    return err;
}


/*
===============================================================================
    criticalPostErrorHandler()

    Handler for post critical error
    Blinks critical error symbol and initates shutdown when finished
===============================================================================
 */
void UIThread::criticalPostErrorHandler(bool a_systemShutdown)
{
    if(m_symbolBlinkTimer)
    {
        m_symbolBlinkTimer--;
    }
    else if(m_symbolRemBlinks)
    {
        if(m_displayInterfaceOK)
        {
            gselvp(eVPCharging);
            gsetcolorf(G_BLACK);
            gsetcolorb(G_RED);
        }
    
        if(m_symbolBlinkState == eSBSStateOff)
        {
            m_critcalErrorSymbol->invalidate();
            m_critcalErrorSymbol->draw();
            m_symbolBlinkState = eSBSStateOn;
            m_symbolBlinkTimer = CRITICAL_ERR_BLINK_ON_TIME;
        }
        else
        {
            m_critcalErrorSymbol->erase();
            m_symbolRemBlinks--;
            if(m_symbolRemBlinks == 0 && a_systemShutdown)
            {
                sigDispatch(P_SUPER, S_SYSTEM_SHUTDOWN, (uint32_t)eSDRcriticalErr, 0 ,0);
            }
            
            m_symbolBlinkState = eSBSStateOff;
            m_symbolBlinkTimer = CRITICAL_ERR_BLINK_OFF_TIME;
            
        }
            
    }
  
}

/*
===============================================================================
    isCriticalError()

    Report if the error is critical
===============================================================================
 */
bool UIThread::isCriticalError(Post_t a_postResult)
{
    if(a_postResult.postBits.displayComm ||
       a_postResult.postBits.ultraSoundTransComm ||
       a_postResult.postBits.programCheckSum ||
       a_postResult.postBits.calibrationCheckSum || 
       a_postResult.postBits.voltageRegulator)
    {
        return true;
    }
    
    return false;
}

/*
===============================================================================
    showPostCode()

    Show the post code on display
===============================================================================
 */
void UIThread::showPostCode(Post_t a_postResult)
{
    char post[20];
    uint16_t len = sprintf(post, "POST:0x%0.2X", a_postResult.postWord);
    
    gselfont(&moyo_mono8_8);
    
    //Position so text is placed lower right
    gsetpos(GDISPW - (ggetfw() * len) - 10, GDISPH - ggetfh() - 25);
    
    gsetcolorf(G_WHITE);
    gsetcolorb(G_BLACK);
    gputs(post);
    
}

/*
===============================================================================
    logAlarm()

    Log alarm state changes
===============================================================================
 */
void UIThread::logAlarm(Alarm_t a_alarm, bool a_alarmOn)
{
    if(a_alarm.alarmBits.lowBatteryAlarm && (a_alarmOn ^ m_alarmLogStatus.alarmBits.lowBatteryAlarm))
    {
        Events::getpEvents()->AddEvent(Events::evtLowBatteryAlarm, 
                                       Events::TO_EPISODE_LOG | Events::TO_SYSTEM_LOG,
                                       a_alarmOn ? 1.0 : 0.0);
        m_alarmLogStatus.alarmBits.lowBatteryAlarm = a_alarmOn;
    }

    if(a_alarm.alarmBits.criticalErrorAlarm && (a_alarmOn ^ m_alarmLogStatus.alarmBits.criticalErrorAlarm))
    {
        Events::getpEvents()->AddEvent(Events::evtCriticalErrorAlarm, 
                                       Events::TO_EPISODE_LOG,
                                       a_alarmOn ? 1.0 : 0.0);
        m_alarmLogStatus.alarmBits.criticalErrorAlarm = a_alarmOn;
    }
            
    if(a_alarm.alarmBits.lostSignalAlarm && (a_alarmOn ^ m_alarmLogStatus.alarmBits.lostSignalAlarm))
    {
        Events::getpEvents()->AddEvent(Events::evtLostSignalAlarm, 
                                       Events::TO_EPISODE_LOG,
                                       a_alarmOn ? 1.0 : 0.0);
        m_alarmLogStatus.alarmBits.lostSignalAlarm = a_alarmOn;
    }
            
    if(a_alarm.alarmBits.abnormalFHRalarm && (a_alarmOn ^ m_alarmLogStatus.alarmBits.abnormalFHRalarm))
    {
        Events::getpEvents()->AddEvent(Events::evtAbnormalFHRalarm, 
                                       Events::TO_EPISODE_LOG,
                                       a_alarmOn ? 1.0 : 0.0);
        m_alarmLogStatus.alarmBits.abnormalFHRalarm = a_alarmOn;
    }
    
}


/*
===============================================================================
    setDisplayAlarm()

    Handle a alarm on display
    Log the alarm
===============================================================================
 */
void UIThread::setDisplayAlarm(Alarm_t a_alarm, bool a_isAcknowledged)
{
    if(m_operationalMode == eOMnormal)
    {
        //Add alarm to log
        logAlarm(a_alarm, true);
        
        //Display should not be dimmed when alarm active
        displayDim(false);
        
        m_alarmAcknowlegde = a_isAcknowledged;
        
        showAlarmSymbol(a_isAcknowledged);
        
        //Low battery display alarm can be set regardless of other display alarms
        if(a_alarm.alarmBits.lowBatteryAlarm)
        {
            if(m_displayComponentsActive.smallBattIndicator)
            {
                m_batteryCapacityComponent->setAlarm(a_alarm);
                
                showInfoSymbol(eISBattLevelCritical);
            }
            
        }
        else
        {
            if(a_alarm.alarmBits.abnormalFHRalarm)
            {
                m_currentDisplayAlarm.alarmByte = 0;
                m_currentDisplayAlarm.alarmBits.abnormalFHRalarm = true;
                m_fetalHeartRateComponent->setAlarm(m_currentDisplayAlarm);
                m_HrBigTrendComponent->SetAlarm(true);
                if(m_displayComponentsActive.fetalHR)
                {
                    updateDisplay(eDcUpdateFetalHR, m_currentFetalHR);
                }
                
#ifdef DEBUG_ALARM_HANDLER
                DEBUG_TRACE("\r\nAbnormal FHR alarm set\r\n");
#endif
            }
            else if(a_alarm.alarmBits.criticalErrorAlarm)
            {
                m_currentDisplayAlarm.alarmByte = 0;
                setDisplayMode(eDmCriticalError);
                m_currentDisplayAlarm.alarmBits.criticalErrorAlarm = true;
                
                
            }
            else if(a_alarm.alarmBits.lostSignalAlarm)
            {
                m_currentDisplayAlarm.alarmByte = 0;
                m_currentDisplayAlarm.alarmBits.lostSignalAlarm = true;
                m_fetalHeartRateComponent->setAlarm(m_currentDisplayAlarm);
                m_HrBigTrendComponent->SetAlarm(false);
                if(m_displayComponentsActive.fetalHR)
                {
                    m_fetalHeartRateComponent->invalidate();
                    updateDisplay(eDcUpdateFetalHR, m_currentFetalHR);
                }
                
#ifdef DEBUG_ALARM_HANDLER
                DEBUG_TRACE("\r\nLost signal alarm set\r\n");
#endif
            }   
        }
    }
}

/*
===============================================================================
    clearDisplayAlarm()

    Clear a alarm on display
    Undim if display was dimmed before alarm was fired
    Log the alarm cleared
===============================================================================
 */
void UIThread::clearDisplayAlarm(Alarm_t a_alarm)
{
    if(m_operationalMode == eOMnormal)
    {
        //Add alarm to log
        logAlarm(a_alarm, false);
        
        clearAlarmSymbol();
        
        m_alarmAcknowlegde = false;
        
        //Restore dim of display if sound off
        if(m_soundVolume == eSVoff)
        {
            displayDim(true);
        }
        
        if(a_alarm.alarmBits.abnormalFHRalarm)
        {
            m_fetalHeartRateComponent->clearAlarm();
            m_HrBigTrendComponent->SetAlarm(false);
            updateDisplay(eDcUpdateFetalHR, m_currentFetalHR);
            
#ifdef DEBUG_ALARM_HANDLER
            DEBUG_TRACE("\r\nAbnormal FHR alarm cleared\r\n");
#endif
        }
        else if(a_alarm.alarmBits.criticalErrorAlarm)
        {
            //Should not clear this alarm
        }
        else if(a_alarm.alarmBits.lostSignalAlarm)
        {
            m_fetalHeartRateComponent->clearAlarm();
            m_HrBigTrendComponent->SetAlarm(false);
            if(m_displayComponentsActive.fetalHR)
            {
                m_fetalHeartRateComponent->invalidate();
                updateDisplay(eDcUpdateFetalHR, m_currentFetalHR);
            }
            
#ifdef DEBUG_ALARM_HANDLER
            DEBUG_TRACE("\r\nLost signal alarm cleared\r\n");
#endif
        }
        else if(a_alarm.alarmBits.lowBatteryAlarm)
        {
            m_batteryCapacityComponent->clearAlarm();
        }
        
        //Clear current alarms
        m_currentDisplayAlarm.alarmByte = 0;
        
    }
}


/*
===============================================================================
    setAudioAlarm()
    
    Handle audio and status led when an alarm is set
===============================================================================
 */
void UIThread::setAudioAlarm(Alarm_t a_alarm)
{
    if(m_operationalMode == eOMnormal)
    {
        //Reset
        m_currentAudioAlarm.alarmByte = 0;
        
        if(a_alarm.alarmBits.criticalErrorAlarm)
        {
            //Turn off doppler sound
            m_soundVolume = eSVoff;
            HwAbsl::getpHw()->getpAudio()->SetSoundVol(m_soundVolume);
        }
        else
        {
            setFullVolume();
            
        }
        
        if(a_alarm.alarmBits.abnormalFHRalarm)
        {
            m_currentAudioAlarm.alarmBits.abnormalFHRalarm = true;
        }
        else if(a_alarm.alarmBits.criticalErrorAlarm)
        {
            m_currentAudioAlarm.alarmBits.criticalErrorAlarm = true;
        }
        else if(a_alarm.alarmBits.lostSignalAlarm)
        {
            m_currentAudioAlarm.alarmBits.lostSignalAlarm = true;
        }
        else if(a_alarm.alarmBits.lowBatteryAlarm)
        {
            m_currentAudioAlarm.alarmBits.lowBatteryAlarm = true;
        }
        
        FhrLedUpdate(m_currentFetalHR);
        
    }
}

/*
===============================================================================
    clearAudioAlarm()

    Handle audio and status led when an alarm is cleared
===============================================================================
 */
void UIThread::clearAudioAlarm(Alarm_t a_alarm)
{
    if(m_operationalMode == eOMnormal)
    {
        m_currentAudioAlarm.alarmByte = 0;
        
        FhrLedUpdate(m_currentFetalHR);
    }
}

/*
===============================================================================
    batteryEmptyHandler()

    Handle battery empty symbol when the battery is empty
    Initiate shutdown afterwards
===============================================================================
 */
void UIThread::batteryEmptyHandler(void)
{
    switch(m_batteryEmptyDisplayState)
    {
        case eBEinit:
            setDisplayBatteryEmptyMode();
            fadeBacklightIn();
            m_batteryEmptyDisplayState = eBEshowSymbol;
            m_battEmptyDisplayTmr = BATT_EMPTY_SHOW_SYMBOL_TIME;
            break;
        case eBEshowSymbol:
            if(m_battEmptyDisplayTmr)
            {
                m_battEmptyDisplayTmr--;
            }
            else
            {
                m_batteryEmptyDisplayState = eBEpowerOff;
            }
            break;
        case eBEpowerOff:
            sigDispatch(P_SUPER, S_SYSTEM_SHUTDOWN, (uint32_t)eSDRbatteryEmpty, 0 ,0);
            m_batteryEmptyDisplayState = eBEwaitPowerOff;
            break;
        case eBEwaitPowerOff:
            //Do nothing
            break;
        default:
            break;
    }
}


/*
===============================================================================
    backlightDimCtrl()

    Handle the dimming of display when in
    file transfer, config or battery charge mode
===============================================================================
 */
void UIThread::backlightDimCtrl(void)
{
    switch(m_operationalMode)
    {
        case eOMnormal:
            //Do nothing
            break;
        case eOMDataTransfer:
        case eOMconfig:
        case eOMbatteryCharge:
            if(m_displayDimTmr == CHARGE_MODE_DISPLAY_DIM_TIME)
            {
                displayDim(false);
                m_displayDimTmr--;
            }
            else if(m_displayDimTmr == 1)
            {
                m_displayDimTmr--;
                displayDim(true);
            }
            else if(m_displayDimTmr)
            {
                m_displayDimTmr--;
            }

            break;
        default:
            break;
        
    }
    
}

/*
===============================================================================
    buttonHandler()

    Handle key press in the differend display modes
===============================================================================
 */
void UIThread::buttonHandler(void)
{
    if(HwAbsl::getpHw()->isAudioVolumeKeyPressed())
    {
        if(!m_AudioVolumeKeyPressed)
        {
            //DEBUG_TRACE("\r\nVolume down\r\n");
            m_AudioVolumeKeyPressed = true;
            
            switch(m_operationalMode)
            {
                case eOMnormal:
                    updateVolume();
                    break;
                case eOMDataTransfer:
                case eOMconfig:
                case eOMbatteryCharge:
                    m_displayDimTmr = CHARGE_MODE_DISPLAY_DIM_TIME; //Will un-dim display
                    break;
                default:
                    break;
                
            }
        }
    }
    else
    {
        m_AudioVolumeKeyPressed = false;
    }
                
    if(HwAbsl::getpHw()->isDisplayModeKeyPressed())
    {
        if(!m_DisplayModeKeyPressed)
        {
            switch(m_operationalMode)
            {
                case eOMnormal:
                {
                    clearTrendArea();
                    setDisplayMode(eDmFhrTrend);
                    
                    //Temporarely undim display
                    bool prevState = displayDim(false);
                    m_backlightDimmed = prevState;
                    
                    Events::getpEvents()->AddEvent(Events::evtDisplayMode, 
                                                   Events::TO_EPISODE_LOG,
                                                   2.0);
                
                    break;
                }
                
                case eOMconfig:
                case eOMDataTransfer:
                case eOMbatteryCharge:
                    m_displayDimTmr = CHARGE_MODE_DISPLAY_DIM_TIME; //Will un-dim display
                    break;
                default:
                    break;
                
            }
            m_DisplayModeKeyPressed = true;
        }
    }
    else
    { 
        if(m_DisplayModeKeyPressed)
        {
            switch(m_operationalMode)
            {
                case eOMnormal:
                    clearTrendArea();
                    setDisplayMode(eDmFhrHr);
                    
                    setStatusArea();
                    
                    //Clock is destroyed when trend component is visible. We need to redraw if enabled
                    if(m_showClock)
                    {
                        m_clockComponent->visible(true);
                    }
                    
                    //Set dimming back to old setting
                    displayDim(m_backlightDimmed);
                    
                    Events::getpEvents()->AddEvent(Events::evtDisplayMode, 
                                                   Events::TO_EPISODE_LOG,
                                                   1.0);
                    break;
                default:
                    break;
                
            }

            m_DisplayModeKeyPressed = false;
        }
    }
    
    if(HwAbsl::getpHw()->isOnKeyPressed())
    {
        if(!m_OnKeyPressed)
        {
            switch(m_operationalMode)
            {
                case eOMnormal:
                    //Do nothing
                    break;
              
                case eOMconfig:
                case eOMDataTransfer:
                case eOMbatteryCharge:
                    m_displayDimTmr = CHARGE_MODE_DISPLAY_DIM_TIME; //Will un-dim display
                    break;
                default:
                    break;
                
            }
            m_OnKeyPressed = true;
        }
    }
    else
    {
        m_OnKeyPressed = false;
    }
    
    if(HwAbsl::getpHw()->isOffKeyPressed())
    {
        if(!m_OffKeyPressed)
        {
            switch(m_operationalMode)
            {
                case eOMnormal:
                case eOMcriticalError:
                    m_operationalMode = eOMshutdown;
                    sigDispatch(P_SUPER, S_SYSTEM_SHUTDOWN, (uint32_t)eSDRoffBtn, 0 ,0);
                    break;
              
                case eOMconfig:
                case eOMDataTransfer:
                case eOMbatteryCharge:
                    //Do nothing
                    
                    break;
                default:
                    break;
                
            }
            
        }
    }
    else
    {
        m_OffKeyPressed = false;
    }
    
}

/*
===============================================================================
    startupHandler()

    Handle system startup regarding to UI
===============================================================================
 */
void UIThread::startupHandler(void)
{
    bool displayModeBtnPressed = false;
    bool batteryEmpty = false;
    bool batteryLevelLow = false;
    bool batteryLevelCritical = false;
    HwAbsl::StartupMode_t startupMode;
    
    if(!m_displayInterfaceOK)
    {
        DEBUG_TRACE("\r\nDisplay init FAILED\r\n" );
        Events::getpEvents()->AddEvent(Events::evtDisplayError, 
                                       Events::TO_SYSTEM_LOG,
                                       1.0);
    
    }

    //Get reason for startup
    startupMode = HwAbsl::getpHw()->getStartupMode();
    
#ifdef SIM_USB_CONNECTED
    //Test
    startupMode = HwAbsl::eStartupModeUSBpowered;
    //
#endif
    //Is display mode button pressed?
    for(uint8_t i = 0; i < 20; i++)
    {
        displayModeBtnPressed = HwAbsl::getpHw()->isDisplayModeKeyPressed();
        sleep(1);
    }

    //Read last battery state from config
    m_batteryRemainingCapacity = Config::getpConfig()->GetConfig(Config::eCfgLastBattRemCharge);
    if(m_batteryRemainingCapacity > 100)
    {
        m_batteryRemainingCapacity = 100;
    }    
    
    if(startupMode == HwAbsl::eStartupModeNormal)
    {
        HwAbsl::getpHw()->getpAudio()->audioOn();
        
        if(m_batteryRemainingCapacity < BATT_EMPTY_LIMIT)
        {
           batteryEmpty = true;
        }
        else if(m_batteryRemainingCapacity >= BATT_EMPTY_LIMIT && 
                m_batteryRemainingCapacity < BATT_CRITICAL_LIMIT)
        {
             batteryLevelCritical = true;
        }
        else if(m_batteryRemainingCapacity < BATT_LOW_LIMIT)
        {
            batteryLevelLow = true;
        }
        
        if(!batteryLevelCritical && !batteryEmpty)
        {
            //Should not beep if citical battery level. Batt low alarm will handle this
            
            //If not critical batt level, play beep
            HwAbsl::getpHw()->getpAudio()->playNormalStartupSound();
            showLGHlogo();
            fadeBacklightIn();
            m_showLogoFinishTime = OS_GetTime() + LOGO_SHOW_TIME;
        }
        
        

    }
    
    
    //Read config
    if(Config::getpConfig()->IsValid())
    {
        m_showClock = Config::getpConfig()->GetConfig(Config::eCfgShowClock) == CFG_ENABLED ? true : false;
        
        m_FhrAtypicalAlarmHigh = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmHigh);
        m_FhrAtypicalAlarmHigh = MIN(CFG_MAX_ATYPICAL_FHR_ALARM_HIGH, m_FhrAtypicalAlarmHigh); //Limit
        m_FhrAtypicalAlarmHigh = MAX(CFG_MIN_ATYPICAL_FHR_ALARM_HIGH, m_FhrAtypicalAlarmHigh); //Limit
        CONSOLE_PRINT("\r\nFHR alarm high set to: %u from config\r\n", m_FhrAtypicalAlarmHigh); 
        
        m_FhrAtypicalAlarmLow = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmLow);
        m_FhrAtypicalAlarmLow = MIN(CFG_MAX_ATYPICAL_FHR_ALARM_LOW, m_FhrAtypicalAlarmLow); //Limit
        m_FhrAtypicalAlarmLow = MAX(CFG_MIN_ATYPICAL_FHR_ALARM_LOW, m_FhrAtypicalAlarmLow); //Limit
        CONSOLE_PRINT("\r\nFHR alarm low set to: %u from config\r\n", m_FhrAtypicalAlarmLow); 
        
        m_HrBigTrendComponent->SetHrLimits(m_FhrAtypicalAlarmHigh, m_FhrAtypicalAlarmLow);
        m_fetalHeartRateComponent->SetHrLimits(m_FhrAtypicalAlarmHigh, m_FhrAtypicalAlarmLow);
        
        m_rsmModeEnabled = Config::getpConfig()->GetConfig(Config::eCfgResearchStorageMode) == 1 ? true:false;
        
        m_postCtrl.postResult.postBits.calibrationCheckSum = false;
    }
    else
    {
        m_postCtrl.postResult.postBits.calibrationCheckSum = true;
    }
    
    //Set Viewport for charging
    if(m_displayInterfaceOK)
    {
        gselvp(eVPCharging);
        gsetvp(0, 24, 219, 175);
    }
    
    //Check if test pin is active
    if(HwAbsl::getpHw()->isTestmodeEnabled())
    {
        m_operationalMode = eOMtestMode;
        m_displayMode = eDmTest;
        
        CONSOLE_PRINT("R100 1\r\n"); //Always success
    }
    else if(startupMode == HwAbsl::eStartupModeUSBpowered)
    {
        DEBUG_TRACE("\r\nStarting Mass Storage\r\n");
            
        sigDispatch(P_FILE, S_FILE_DATA_TRANSFER_MODE);
        
        while(OS_GetTime() <= m_showLogoFinishTime)
        {
            sleep(50);
        }
        
        m_operationalMode = eOMDataTransfer;
        m_displayMode = eDmbatteryCharge;
    }
    else if(startupMode == HwAbsl::eStartupModeUSBvirtCOM)
    {
        /*
        if(HwAbsl::getpHw()->getpSerialConsole())
        {
            //Host USB driver can be slow to install. Wait a bit for it to start
            uint16_t tmout = 30;
            do
            {
                if(HwAbsl::getpHw()->getpSerialConsole()->hasUSBHostConnection() && 
                HwAbsl::getpHw()->getpSerialConsole()->isUSBHostConnectionOK())
                {
                    break;
                }
                
                sleep(100);
                tmout--;
            } while (tmout);
            
            if(HwAbsl::getpHw()->getpSerialConsole()->hasUSBHostConnection() && 
                HwAbsl::getpHw()->getpSerialConsole()->isUSBHostConnectionOK())
            {
                m_operationalMode = eOMconfig;
                m_displayMode = eDmConfig;
            }
            else
            {
                m_operationalMode = eOMbatteryCharge;
                m_displayMode = eDmbatteryCharge;
            }
        }
        else
        {
            m_operationalMode = eOMbatteryCharge;
            m_displayMode = eDmbatteryCharge;
        }
        */
    }
    else
    {
        if(batteryEmpty)
        {
            m_operationalMode = eOMbatteryEmpty;
            m_displayMode = eDmBattEmpty;
            m_batteryEmptyDisplayState = eBEinit;
        }
        else
        {

#ifndef NO_POST
            post(displayModeBtnPressed);
#endif        
            if(isCriticalError(m_postCtrl.postResult))
            {
                m_operationalMode = eOMcriticalError;
                m_displayMode = eDmCriticalError;
                setDisplayCriticalErrorMode(true);
                m_symbolBlinkTimer = CRITICAL_ERR_BLINK_ON_TIME;
                
            }
            else
            {
                if(!displayModeBtnPressed)
                {
                    while(OS_GetTime() <= m_showLogoFinishTime)
                    {
                        sleep(50);
                    }
                }
                
                if(batteryLevelLow)
                {
                    if(m_displayInterfaceOK)
                    {
                        gselvp(eVPCharging);
                        displayDim(false);
                        showBatteryLowSymbol();
                    }
                    
                }
        
                m_operationalMode = eOMnormal;
                m_displayMode = eDmFhrHr;
                
                //Turn on sound from probe
                HwAbsl::getpHw()->getpDac()->ProbeSoundEnable(true);
                
                
                //Setup FHR blink
                HwAbsl::getpHw()->FHRledBlinkContinous(HwAbsl::eFhrLEdColorOff, 50, 1000);
            }

        }
        
 
        
    }
    
    //Turn off green LED
    HwAbsl::getpHw()->setGreenLED(false);

    if(m_displayInterfaceOK)
    {
        gselvp(eVPstatus);
        gsetvp(0, 0, 219, 23);
    }
    
    if(m_operationalMode != eOMcriticalError)
    {
        clearScreen();
        setAllDisplayComponentsInactive();
        setStatusArea();
        setDisplayMode(m_displayMode);
        
        if(startupMode == HwAbsl::eStartupModeUSBpowered)
        {
            //No logo was shown in this case, and backlight is off
            //Fade in
            fadeBacklightIn();
        }
        
        //Enable alarm sound
        sigDispatch(P_SUPER, S_ENABLE_ALARM_SOUND, 1, 0 ,0);
    }
    
    //Update threads with operational mode
    sigDispatch(P_SUPER, S_SET_OPERATIONAL_MODE, (uint32_t)m_operationalMode, 0 ,0);
    sigDispatch(P_FILE, S_SET_OPERATIONAL_MODE, (uint32_t)m_operationalMode, 0 ,0);
    sigDispatch(P_ADC, S_SET_OPERATIONAL_MODE, (uint32_t)m_operationalMode, 0 ,0);
    
#ifndef NO_POST
    if(m_operationalMode == eOMconfig)
    {
        /*
        if(HwAbsl::getpHw()->getpSerialConsole())
        {
            if(HwAbsl::getpHw()->getpSerialConsole()->isUSBHostConnectionOK())
            {
                //We have permission from PC to use 500mA. Devices can be turned on and post performed
                post(displayModeBtnPressed);
                
                //Turn off power after POST
                HwAbsl::getpHw()->transducerPower(false);
                sleep(500);
                HwAbsl::getpHw()->enable5V(false);
                
                //Set display
                clearScreen();
                setAllDisplayComponentsInactive();
                setStatusArea();
                setDisplayMode(m_displayMode);
            
            }
        }
        */
    }
#endif

    //Turn on backlight in case it is not already done
    if(!batteryEmpty)
    {
        displayDim(false);
    }
    
}

/*
===============================================================================
    showInfoSymbol()

    Show a big info symbol
    Use infosymbol timer to control how long it will show on display
===============================================================================
 */
void UIThread::showInfoSymbol(InfoSymbol_t a_infoSymbol)
{
    m_infoSymbolTimer = INFO_SYMBOL_SHOW_TIME;
    m_infoSymbolActive = a_infoSymbol;
    m_fetalHeartRateComponent->visible(false);
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPTrend); //Extends over FHR area. Use trend area
        gsetcolorf(G_WHITE);
        gsetcolorb(G_BLACK);
        gclrvp();
    }
    
    if(a_infoSymbol == eISAlarmAck)
    {
        m_alarmAckBigSymbol->invalidate();
        m_alarmAckBigSymbol->draw();
    }
    else if(a_infoSymbol == eISBattLevelCritical)
    {
        m_batteryCriticallyLowBigSymbol->invalidate();
        m_batteryCriticallyLowBigSymbol->draw();
    
    }
    
    if(m_displayMode == eDmElectrodesTouched)
    {
        //Since symbol extends over MHR area, temporarely disble MHR update
        m_displayComponentsActive.maternalHR = false;
    }
}

/*
===============================================================================
    infoSymbolHandler()
    
    Handle timing of the big alarm acknowledge or critical battery level symbol
    Restore FHR afterwards
===============================================================================
 */
void UIThread::infoSymbolHandler(void)
{
    if(m_infoSymbolTimer)
    {
        m_infoSymbolTimer--;
    }
    else
    {
        clearInfoSymbol();
        m_fetalHeartRateComponent->visible(true);
        updateDisplay(eDcUpdateFetalHR, m_currentFetalHR);
        
        if(m_displayMode == eDmElectrodesTouched)
        {
            //MHR was inactivated during alarm ack, enable MHR update
            setDisplayMaternalHrMode();
        }
    }
}

/*
===============================================================================
    clearInfoSymbol()

    Clear info symbol
===============================================================================
 */
void UIThread::clearInfoSymbol(void)
{
    m_infoSymbolTimer = 0;
    m_infoSymbolActive = eISnone;
    
    if(m_displayInterfaceOK)
    {
        gselvp(eVPTrend); //Extends over FHR area. Use trend area
        gsetcolorf(G_WHITE);
        gsetcolorb(G_BLACK);
        gclrvp();
        gselvp(eVPfetalHeartRate);
        gclrvp();
    }
}

/*
===============================================================================
    getPostResult()

    Get the post result for UI
===============================================================================
 */
POST_ctrl_t UIThread::getPostResult(void)
{
    return m_POSTstatus;
}

/*
===============================================================================
    clearPostResult()

    Clear the post result for UI
===============================================================================
 */
void UIThread::clearPostResult(void)
{
    m_POSTstatus.finished = false;
}

/*
===============================================================================
    displayTest()

    Set or clear the display when in test mode
    If set, entire display area is set to white to
    see dead pixels
===============================================================================
 */
void UIThread::displayTest(bool a_enDis)
{
    if(m_operationalMode == eOMtestMode)
    {
        if(a_enDis)
        {
            HwAbsl::getpHw()->setDutycycleBackLight(DC_100_PERC); //Full
            if(m_displayInterfaceOK)
            {
                gselvp(eVPCharging);
                gsetcolorf(G_BLACK);
                gsetcolorb(G_WHITE);
                gclrvp();
                
                gselvp(eVPstatus);
                gsetcolorf(G_BLACK);
                gsetcolorb(G_WHITE);
                gclrvp();
            }
        }
        else
        {
            m_PWMdcBacklight = BACKLIGHT_FULL;
            HwAbsl::getpHw()->setDutycycleBackLight(m_PWMdcBacklight); //Back to max
            setDisplayTestMode();
        }
    }
}

/*
===============================================================================
    initDisplay()

    Initialize GCLCD display driver
    Update display post
===============================================================================
 */
void UIThread::initDisplay(void)
{
    DEBUG_TRACE("\r\nDisplay init. OStime=%u\r\n", OS_GetTime() );
    if (ginit() == 0)
    {
        m_displayInterfaceOK = true;
    }
    else
    {   // repeat to be sure that there is really an error on the display interface
        if (ginit() == 0)
        {
            m_displayInterfaceOK = true;
        }
    }
    
    //Set POST status
    if(!m_displayInterfaceOK)
    {
        m_POSTstatus.post.postBits.displayComm = 1;
    }
    
    m_POSTstatus.finished = true;
}

/*
===============================================================================
    clearScreen()

    Clear entire screen area (set to black)
===============================================================================
 */
void UIThread::clearScreen(void)
{
    if(m_displayInterfaceOK)
    {
        //Use status VP
        gselvp(eVPstatus);
        
        //Get current status VP coordinates
        GXT xs; GXT xe; GYT ys; GYT ye;
        ggetvp(&xs, &ys, &xe, &ye);
        
        //Clear screen
        gresetposvp(); //Set vp to use full display area
        gsetcolorb(G_BLACK);
        gclrvp();
        
        //Restore VP
        gsetvp(xs, ys, xe, ye);
    }
}
    
#ifdef DEBUG_DISPLAY_ALIGNMENT
void UIThread::drawVertLine(GXT a_xStart, GXT a_xEnd)
{
    gselvp(eVPCharging);
    gsetcolorf(G_WHITE);
    gsetcolorb(G_BLACK);
    
    gmoveto(a_xStart, 0);
    glineto(a_xEnd, GDISPH-1);
}
void UIThread::drawHorLine(GYT a_yStart, GYT a_yEnd)
{
    gselvp(eVPCharging);
    gsetcolorf(G_WHITE);
    gsetcolorb(G_BLACK);
    
    gmoveto(0, a_yStart);
    glineto(a_yEnd, GDISPW-1);
}
#endif

#ifdef DEBUG_ALARM_HANDLER
void UIThread::logAlarmState()
{
    UI_CDA_criticalErrorAlarm = m_currentDisplayAlarm.alarmBits.criticalErrorAlarm;
    UI_CDA_abnormalFHRalarm = m_currentDisplayAlarm.alarmBits.abnormalFHRalarm;
    UI_CDA_lowBatteryAlarm = m_currentDisplayAlarm.alarmBits.lowBatteryAlarm;
    UI_CDA_lostSignalAlarm = m_currentDisplayAlarm.alarmBits.lostSignalAlarm;

    UI_CAA_criticalErrorAlarm = m_currentAudioAlarm.alarmBits.criticalErrorAlarm;
    UI_CAA_abnormalFHRalarm = m_currentAudioAlarm.alarmBits.abnormalFHRalarm;
    UI_CAA_lowBatteryAlarm = m_currentAudioAlarm.alarmBits.lowBatteryAlarm;
    UI_CAA_lostSignalAlarm = m_currentAudioAlarm.alarmBits.lostSignalAlarm;

    UI_alarmAcknowlegde = m_alarmAcknowlegde;
}
#endif

        