//! \file    Configuration.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    07.11.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  07Nov2013, JRo, Original.


#include "configuration.h"


//Mutex
static Mutex * ConfigMutex = 0;

// static member init
Config*  Config::m_pConfig = 0;

//const float initializers
const float Config::CFG_ECG_AMP_SENSE_DEFAULT  = 1.0000;

//! Event
//! @brief Constructor
//! @param none
//! @return none
//! @author JRO
Config::Config()
{
    if(m_pConfig == 0)
    {
        m_pConfig = this;
    }
}

//! ~Event
//! @brief Destructor
//! @param none
//! @return none
//! @remark Protected
//! @author JRO
Config::~Config()
{
    
}

float Config::GetConfigFloat(ConfigData_t a_CfgType)
{        
    configLock();
    
    switch(a_CfgType)
    {
        case eCfgEcgAmpSens:
        {
            uint32_t tmp;
            HwAbsl::getpHw()->getpEEprom()->eeRead(a_CfgType, tmp);
            float tmpf = *((float*)&tmp);   //convert from uint to float
            configUnlock();
            return tmpf;
        }
        default:
            configUnlock();
            return 0;
    }
}
uint32_t Config::GetConfig(ConfigData_t a_CfgType)
{
    configLock();
    
    uint32_t config32;
    uint16_t config16;
    
    switch(a_CfgType)
    {
        case eCfgEpisodevalidTime:
        case eCfgHRupdateTime:
        case eCfgHRavgBufLen:
        case eCfgLostSignalAlarmTime:
        case eCfgFhrAtypicalAlarmHigh:
        case eCfgFhrAtypicalAlarmLow:
        case eCfgLastBattRemCharge:
        case eCfgShowClock:
        case eCfgShutdownReason:
        case eCfgResearchStorageMode:
        case eCfgEpisodeTooShortTime:
        case eCfgFhrAbnormalAlarmHigh:
        case eCfgFhrAbnormalAlarmLow:
        case eCfgFhrAtypicalAlarmTime:
        case eCfgFhrAbnormalAlarmTime:
        case eCfgUseSDcard:
        case eCfgDopplerSpeakerGain:
        case eCfgDopplerSoundGain:
        case eCfgUseFHRfilter:
            HwAbsl::getpHw()->getpEEprom()->eeRead(a_CfgType, config16);
            configUnlock();
            return config16;
        case eCfgEndOfEpisodeTimeout:
        case eCfgEcgAmpSens:
            HwAbsl::getpHw()->getpEEprom()->eeRead(a_CfgType, config32);
            configUnlock();
            return config32;
        default:
            configUnlock();
            return 0;
    }
}

std::string Config::GetConfigStr(ConfigData_t a_CfgType, uint16_t a_numChars)
{
    configLock();
    
    std::string str;
    
    switch(a_CfgType)
    {
        case eCfgDeviceIdentifier:
            {
                uint16_t idx = 0;
                uint8_t ch = 0xff;
                do
                {
                    HwAbsl::getpHw()->getpEEprom()->eeRead(a_CfgType + idx, ch);
                    if(ch != 0)
                    {
                        str += ch;
                    }
                    idx++;
                } while(idx < CFG_DEV_ID_MAX_SIZE && idx < a_numChars && ch != 0);
                
                while(str.length() < a_numChars)
                {
                    str += " ";
                    idx++;
                }
                
                configUnlock();
            }
            return str;
        default:
            configUnlock();
            return "";
    }
}

std::string Config::GetConfigStr(ConfigData_t a_CfgType)
{
    configLock();
    
    std::string str;
    
    switch(a_CfgType)
    {
        case eCfgDeviceIdentifier:
            {
                uint16_t idx = 0;
                uint8_t ch = 0xff;
                do
                {
                    HwAbsl::getpHw()->getpEEprom()->eeRead(a_CfgType + idx, ch);
                    if(ch != 0)
                    {
                        str += ch;
                    }
                    idx++;
                } while(idx < CFG_DEV_ID_MAX_SIZE && ch != 0);
                
                
                configUnlock();
            }
            return str;
        default:
            configUnlock();
            return "";
    }
}

void Config::SaveConfig(ConfigData_t a_CfgType, uint16_t a_value)
{
    SaveConfig(a_CfgType, static_cast<uint32_t>(a_value));
}

void Config::SaveConfig(ConfigData_t a_CfgType, uint32_t a_value)
{
    configLock();
    
    switch(a_CfgType)
    {
        case eCfgEpisodevalidTime:
        case eCfgHRupdateTime:
        case eCfgHRavgBufLen:
        case eCfgLostSignalAlarmTime:
        case eCfgFhrAtypicalAlarmHigh:
        case eCfgFhrAtypicalAlarmLow:
        case eCfgLastBattRemCharge:
        case eCfgShowClock:
        case eCfgShutdownReason:
        case eCfgResearchStorageMode:
        case eCfgEpisodeTooShortTime:
        case eCfgFhrAbnormalAlarmHigh:
        case eCfgFhrAbnormalAlarmLow:
        case eCfgFhrAtypicalAlarmTime:
        case eCfgFhrAbnormalAlarmTime:
        case eCfgUseSDcard:
        case eCfgDopplerSpeakerGain:
        case eCfgDopplerSoundGain:
        case eCfgUseFHRfilter:
            HwAbsl::getpHw()->getpEEprom()->eeWrite( static_cast<uint16_t>(a_value), static_cast<uint16_t>(a_CfgType));
            break;
        case eCfgEndOfEpisodeTimeout:
        case eCfgEcgAmpSens:
            HwAbsl::getpHw()->getpEEprom()->eeWrite( static_cast<uint32_t>(a_value), static_cast<uint16_t>(a_CfgType));
            break;
        default:
            break;
    }
    
    if(a_CfgType <= CFG_CHKSUM_PROT_END)
    {
        //Only update CRC if changing value inside proteced area
        HwAbsl::getpHw()->getpEEprom()->eeWrite(GetDataCRC(), CFG_DATA_CHKSUM_ADDR);
    }
    
    configUnlock();
}

void Config::SaveConfig(ConfigData_t a_CfgType, std::string a_value)
{
    configLock();
    
    switch(a_CfgType)
    {
        case eCfgDeviceIdentifier:
            if(a_value.size() >= CFG_DEV_ID_MAX_SIZE)
            {
                a_value.erase(CFG_DEV_ID_MAX_SIZE - 1, a_value.size() - CFG_DEV_ID_MAX_SIZE + 1);
            }
            a_value += '\0';
            HwAbsl::getpHw()->getpEEprom()->eeWrite((const uint8_t *)(a_value.c_str()), static_cast<uint16_t>(a_CfgType), a_value.length());
            break;
        default:
            break;
    }
    
    if(a_CfgType <= CFG_CHKSUM_PROT_END)
    {
        //Only update CRC if changing value inside proteced area
        HwAbsl::getpHw()->getpEEprom()->eeWrite(GetDataCRC(), CFG_DATA_CHKSUM_ADDR);
    }
    
    configUnlock();
}

void Config::SaveConfig(ConfigData_t a_CfgType, float a_value)
{
    configLock();
    
    uint32_t *pfv = (uint32_t*)&a_value;        //convert from float to uint

    switch(a_CfgType)
    {
        case eCfgEcgAmpSens:
            HwAbsl::getpHw()->getpEEprom()->eeWrite( static_cast<uint32_t>(*pfv), static_cast<uint16_t>(a_CfgType));
            break;
        default:
            break;
    }
    
    if(a_CfgType <= CFG_CHKSUM_PROT_END)
    {
        //Only update CRC if changing value inside proteced area
        HwAbsl::getpHw()->getpEEprom()->eeWrite(GetDataCRC(), CFG_DATA_CHKSUM_ADDR);
    }
    
    configUnlock();
}
        
uint16_t Config::GetDataCRC()
{
    uint8_t cfg[256];
    
    HwAbsl::getpHw()->getpEEprom()->eeRead(cfg, static_cast<uint16_t>(0), sizeof(cfg));
    
    return CrcCCITT::calcCrc(&cfg[CFG_CHKSUM_PROT_START], CFG_CHKSUM_PROT_END + 1 - CFG_CHKSUM_PROT_START);
}

bool Config::IsValid()
{
    configLock();
    
    bool valid = false;
    uint16_t dataChkSum = GetDataCRC();
    uint16_t eeChkSum = 0;
    HwAbsl::getpHw()->getpEEprom()->eeRead(CFG_DATA_CHKSUM_ADDR, eeChkSum);
    if(eeChkSum == dataChkSum)
    {
        valid = true;
    }
    
    configUnlock();
    
    return valid;
}

void Config::EraseAllConfig()
{
    configLock();
    
    uint16_t crc = CrcCCITT::CRC_INIT;
    
    for(uint16_t i = 0; i < CFG_DATA_CHKSUM_ADDR; i++)
    {
        HwAbsl::getpHw()->getpEEprom()->eeWrite(static_cast<uint8_t>(0), i);
        CrcCCITT::crcUpdate(crc, 0);
    }
    
    HwAbsl::getpHw()->getpEEprom()->eeWrite(crc, CFG_DATA_CHKSUM_ADDR);
    
    configUnlock();
}

// lock config
bool Config::configLock()
{
    if (0==ConfigMutex)
    {
        ConfigMutex = new Mutex(); // create when needed
    }
    if (ConfigMutex)
    {
        ConfigMutex->lock();
        return true;
    }
    return false;
}

// unlock config
bool Config::configUnlock()
{
    if (ConfigMutex)
    {
        ConfigMutex->release();
        return true;
    }
    return false;
}

// Set default config data
bool Config::SetConfigDefault(void)
{
    SaveConfig(eCfgEcgAmpSens, CFG_ECG_AMP_SENSE_DEFAULT);
    SaveConfig(eCfgEpisodevalidTime, CFG_EPISODE_VALID_TIME_DEFAULT);
    SaveConfig(eCfgEndOfEpisodeTimeout, CFG_END_OF_EPISODE_TIME_DEFAULT);
    SaveConfig(eCfgHRupdateTime, CFG_HR_UPDATE_TIME_DEFAULT);
    SaveConfig(eCfgHRavgBufLen, CFG_HR_AVG_BUFFER_LEN_DEFAULT);
    SaveConfig(eCfgLostSignalAlarmTime, CFG_LOST_FHR_SIG_ALARM_TIME_DEFAULT);
    SaveConfig(eCfgFhrAtypicalAlarmHigh, CFG_ATYPICAL_FHR_ALARM_H_DEFAULT);
    SaveConfig(eCfgFhrAtypicalAlarmLow, CFG_ATYPICAL_FHR_ALARM_L_DEFAULT);
    SaveConfig(eCfgResearchStorageMode, CFG_EN_RSM_DEFAULT);
    SaveConfig(eCfgShowClock, CFG_SHOW_CLOCK_DEFAULT);
    SaveConfig(eCfgEpisodeTooShortTime, CFG_KEEP_EPISODE_TIME_DEFAULT);
    SaveConfig(eCfgFhrAbnormalAlarmHigh, CFG_ABNORMAL_FHR_ALARM_H_DEFAULT);
    SaveConfig(eCfgFhrAbnormalAlarmLow, CFG_ABNORMAL_FHR_ALARM_L_DEFAULT);
    SaveConfig(eCfgFhrAtypicalAlarmTime, CFG_ATYPICAL_FHR_ALARM_TIME_DEFAULT);
    SaveConfig(eCfgFhrAbnormalAlarmTime, CFG_ABNORMAL_FHR_ALARM_TIME_DEFAULT);
    SaveConfig(eCfgUseSDcard, CFG_SD_CARD_ENABLED_DEFAULT);
    SaveConfig(eCfgDopplerSpeakerGain, CFG_DOPPLER_SPEAKER_GAIN_DEFAULT);
    SaveConfig(eCfgDopplerSoundGain, CFG_DOPPLER_SOUND_GAIN_DEFAULT);
    SaveConfig(eCfgUseFHRfilter, CFG_USE_FHR_FILTER_DEFAULT);
    
    if( GetConfigFloat(eCfgEcgAmpSens) == CFG_ECG_AMP_SENSE_DEFAULT &&
        GetConfig(eCfgEpisodevalidTime) == CFG_EPISODE_VALID_TIME_DEFAULT &&
        GetConfig(eCfgEndOfEpisodeTimeout) == CFG_END_OF_EPISODE_TIME_DEFAULT &&
        GetConfig(eCfgHRupdateTime) == CFG_HR_UPDATE_TIME_DEFAULT &&
        GetConfig(eCfgHRavgBufLen) == CFG_HR_AVG_BUFFER_LEN_DEFAULT &&
        GetConfig(eCfgLostSignalAlarmTime) == CFG_LOST_FHR_SIG_ALARM_TIME_DEFAULT &&
        GetConfig(eCfgFhrAtypicalAlarmHigh) == CFG_ATYPICAL_FHR_ALARM_H_DEFAULT &&
        GetConfig(eCfgFhrAtypicalAlarmLow) == CFG_ATYPICAL_FHR_ALARM_L_DEFAULT &&
        GetConfig(eCfgResearchStorageMode) == CFG_EN_RSM_DEFAULT &&
        GetConfig(eCfgShowClock) == CFG_SHOW_CLOCK_DEFAULT &&
        GetConfig(eCfgEpisodeTooShortTime) == CFG_KEEP_EPISODE_TIME_DEFAULT &&
        GetConfig(eCfgFhrAbnormalAlarmHigh) == CFG_ABNORMAL_FHR_ALARM_H_DEFAULT &&
        GetConfig(eCfgFhrAbnormalAlarmLow) == CFG_ABNORMAL_FHR_ALARM_L_DEFAULT &&
        GetConfig(eCfgFhrAtypicalAlarmTime) == CFG_ATYPICAL_FHR_ALARM_TIME_DEFAULT &&
        GetConfig(eCfgFhrAbnormalAlarmTime) == CFG_ABNORMAL_FHR_ALARM_TIME_DEFAULT &&
        GetConfig(eCfgUseSDcard) == CFG_SD_CARD_ENABLED_DEFAULT &&
        GetConfig(eCfgDopplerSpeakerGain) == CFG_DOPPLER_SPEAKER_GAIN_DEFAULT &&
        GetConfig(eCfgDopplerSoundGain) == CFG_DOPPLER_SOUND_GAIN_DEFAULT && 
        GetConfig(eCfgUseFHRfilter) == CFG_USE_FHR_FILTER_DEFAULT)
    {
        return true;
    }
    
    return false;
    
        
}