1) Maximize volume on original sound and convert to 8kHz sampling rate
2) Goldwave batch convert for .wav to raw format (.snd): 
raw, PCM unsigned 12 bit, little endian, mono, 8000 Hz

3) Convert generated raw files to C++ header files:
bin2h OK_8kHz_12bit_pcm_mono.snd OK_8kHz_12bit_pcm_mono.h -id=snd_ok -ns=bls_2010
bin2h BLS_ERROR_V2_8kHz_12bit_pcm_mono.snd BLS_ERROR_V2_8kHz_12bit_pcm_mono.h -id=snd_error -ns=bls_2010
bin2h connected_8kHz_12bit_pcm_mono.snd connected_8kHz_12bit_pcm_mono.h -id=snd_connected -ns=bls_2010
bin2h disconnected_8kHz_12bit_pcm_mono.snd disconnected_8kHz_12bit_pcm_mono.h -id=snd_disconnected -ns=bls_2010
bin2h MaxVolume_8kHz_12bit_pcm_mono.snd MaxVolume_8kHz_12bit_pcm_mono.h -id=snd_maxvol -ns=bls_2010
bin2h WiFiReset_8kHz_12bit_pcm_mono.snd WiFiReset_8kHz_12bit_pcm_mono.h -id=snd_wifireset -ns=bls_2010
