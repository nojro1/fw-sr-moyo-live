//! \file    FHRMAudio.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    14.12.2011

#ifndef _FHRM_AUDIO_H
#define _FHRM_AUDIO_H

// header includes
#include <cstdint>
#include "FHRMprobeSerial.h"

#if defined(__cplusplus)
 #ifndef __embedded_cplusplus
   using std::uint8_t;
   using std::uint16_t;
 #endif
#endif


// forward declarations
class Dac1;
class Mutex;

////

//! \brief The FHRMAudio class wraps sound playback for FHRM audio
class FHRMAudio
{
public:

    // constructor/destructor
    explicit FHRMAudio(Dac1 * pDac);
    virtual ~FHRMAudio();

    void audioOn();
    void audioOff();
    bool isAudioOn();

    void setAudioVolume(uint8_t percent);
    uint8_t getAudioVolume() {return m_volume;}

    
	void playTestSound();
	void stopPlayback();

    static const uint8_t AUDIO_VOLUME_STEP = 10;
    static const uint8_t AUDIO_VOLUME_DEFAULT = 50;
    static const uint8_t AUDIO_VOLUME_MAX  = 100;

protected:

    void playSound(const uint8_t* sndBuf, uint16_t buflen, bool overrideVolumeSetting = false);

private:
    FHRMAudio(const FHRMAudio& right);            //!< block the copy constructor
    FHRMAudio& operator=(const FHRMAudio& right); //!< block the assignment operator

    Dac1 *m_pAudioDac;
    
    FHRMps *m_pSoundData;

    uint8_t m_volume;

    uint8_t * m_prevSnd;

    uint16_t * m_audioBuf;
    static const uint16_t AUDIO_BUF_SIZE = 0x2000; //! Audio buffer size in # of bytes

    static const uint8_t m_volume_lookup_table[];

    Mutex * m_mutex;

};


#endif   // _FHRM_AUDIO_H

//======== End of FHRMAudio.h ==========



