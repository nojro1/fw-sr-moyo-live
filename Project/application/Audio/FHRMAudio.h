//! \file    FHRMAudio.h
//! \author  Jan A. R�yneberg
//! \version 1.0
//! \date    02.12.2013

#ifndef _FHRM_AUDIO_H
#define _FHRM_AUDIO_H

// header includes
//#include <cstdint>
#include <stdint.h>
#include "FHRMprobeSerial.h"


// forward declarations
class DacDma;

////

//! \brief The FHRMAudio class wraps sound playback for FHRM audio
class FHRMAudio
{
public:

    // constructor/destructor
    explicit FHRMAudio(DacDma * pDac);
    virtual ~FHRMAudio();

    void audioOn();
    void audioOff();
    bool isAudioOn();

    void SetSoundVol(SoundVolume_t a_vol);

    void ProbeSoundEnable(bool a_on);
    
	void playTestSound();
    void playNormalStartupSound();
    void playLowPriorityAlarmSignal();
    void playMediumPriorityAlarmSignal();
    void playSin1000();
    void playSin200();
    void playNormalStartupSoundContinuous();
    void playMediumPriorityAlarmSignalContinuous();
    void playLowPriorityAlarmSignalContinuous();
	void stopPlayback();

    static const uint16_t AUDIO_SOURCE_SAMPLE_FREQ = 4000;  //Hz

protected:

    
private:
    FHRMAudio(const FHRMAudio& right);            //!< block the copy constructor
    FHRMAudio& operator=(const FHRMAudio& right); //!< block the assignment operator

    DacDma *m_pAudioDac;
    
};


#endif   // _FHRM_AUDIO_H

//======== End of FHRMAudio.h ==========



