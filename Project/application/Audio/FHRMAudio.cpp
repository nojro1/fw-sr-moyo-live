//! \file    FHRMAudio.cpp
//! \author  Jan A. R�yneberg
//! \version 1.0
//! \date    02.12.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  02dec2013, JRo, Original.

#include "FHRMAudio.h"

#include <DacDma.h>
#include <includes.h>
#include <utilfuncs.h>
#include <Mutex.h>
#include <Guard.h>

#include <stddef.h>
#include <assert.h>
//#include <algorithm>

#include "..\Sounds\ding_4000_r4096.h"

#include "..\Sounds\Low_Priority_Alarm_Signal_4000k_R4096.h"
#include "..\Sounds\Medium_priority_alarm_signal_4000k_R4096.h"

#include "..\Sounds\Sinus1000_4000_R4096.h"
#include "..\Sounds\Sinus200_4000_R4096.h"

//! \brief Constructor
FHRMAudio::FHRMAudio(DacDma * pDac) : m_pAudioDac(pDac)
{
    assert(m_pAudioDac!=0);
    m_pAudioDac->startSound(4000);
    
}

//! \brief Destructor
FHRMAudio::~FHRMAudio()
{
    m_pAudioDac = 0;
    
}

//! \brief Turn the audio amplifier on
void FHRMAudio::audioOn()
{
    HwAbsl::getpHw()->audioOn();
}

//! \brief Turn the audio amplifier off
void FHRMAudio::audioOff()
{
    HwAbsl::getpHw()->audioOff();
}

//! \brief Return if the audio amplifier is on
bool FHRMAudio::isAudioOn()
{
    return HwAbsl::getpHw()->isAudioOn();
}

//! \brief Enable or disable sampling of sound data from probe
void FHRMAudio::ProbeSoundEnable(bool a_on)
{
    m_pAudioDac->ProbeSoundEnable(a_on);
}

//! \brief Set the doppler sound volume
void FHRMAudio::SetSoundVol(SoundVolume_t a_vol)
{
    m_pAudioDac->SetSoundVol(a_vol);
}

//! \brief Play the startup sound
void FHRMAudio::playNormalStartupSound()
{

    m_pAudioDac->AlarmSoundInterrupt(DING_4000,
                                     DING_4000_len,
                                     false);
        
}

//! \brief Play low priority alarm signal once
void FHRMAudio::playLowPriorityAlarmSignal()
{
    m_pAudioDac->AlarmSoundInterrupt(Low_Priority_Alarm_Signal_4000k, Low_Priority_Alarm_Signal_4000k_len, false);
}

//! \brief Play medium priority alarm signal once
void FHRMAudio::playMediumPriorityAlarmSignal()
{
    m_pAudioDac->AlarmSoundInterrupt(Medium_Priority_Alarm_Signal_4000k, Medium_Priority_Alarm_Signal_4000k_len, false);
}

//! \brief Play low priority alarm signal continious
void FHRMAudio::playLowPriorityAlarmSignalContinuous()
{
    m_pAudioDac->AlarmSoundInterrupt(Low_Priority_Alarm_Signal_4000k, Low_Priority_Alarm_Signal_4000k_len, true);
}

//! \brief Play medium priority alarm signal continious
void FHRMAudio::playMediumPriorityAlarmSignalContinuous()
{
    m_pAudioDac->AlarmSoundInterrupt(Medium_Priority_Alarm_Signal_4000k, Medium_Priority_Alarm_Signal_4000k_len, true);
}

//! \brief Play the startup sound continious
void FHRMAudio::playNormalStartupSoundContinuous()
{
    m_pAudioDac->AlarmSoundInterrupt(DING_4000,
                                     DING_4000_len,
                                     true);
        
}

//! \brief Play 1000 Hz sin signal
void FHRMAudio::playSin1000()
{
    m_pAudioDac->AlarmSoundInterrupt(Sinus1000_4000, Sinus1000_4000_len, true);
}

//! \brief Play 200 Hz sin signal
void FHRMAudio::playSin200()
{
    m_pAudioDac->AlarmSoundInterrupt(Sinus200_4000, Sinus200_4000_len, true);
}
    
//! \brief Stop current sound
void FHRMAudio::stopPlayback()
{
    m_pAudioDac->StopWaveformPlayback();
}


