//! @class  IIRfilter
//! @brief
//! @author Jan Arild R�yneberg
//! @date   16.10.2014
//!

#ifndef _IIR_FILTER_H_
#define _IIR_FILTER_H_
//#include <cstdint>
#include <stdint.h>
#include <string.h>


class IIRfilter
{
    
public:
	IIRfilter();
	~IIRfilter();
    
    void IIRfilterInit(void);
    int16_t filterSample50HzSF200Hz(int16_t a_sample);
    float filterSample50HzSF200HzF(int16_t a_sample);
	
private:
    IIRfilter(const IIRfilter& right);            //!< block the copy constructor
    IIRfilter& operator=(const IIRfilter& right); //!< block the assignment operator
    
    float iirFloat( const float *a_coeffs, float a_inSample, float *inSampHist, float *outSampHist);
    
    // Array to hold 50Hz IIR filter input samples
    float *m_NotchIIRinsampHist;
    
    // Array to hold 50Hz IIR filter output samples
    float *m_NotchIIRoutsampHist;
    
    // Number of samples we need to keep for history
    static const uint8_t IIR_HIST_BUFFER_LEN = 3;
    // IIR filter length
    static const uint8_t IIR_FILTER_LEN = 3;
    
    static const float m_IIR50HzSF200coeffs[ IIR_FILTER_LEN ];
    
    

};

#endif
