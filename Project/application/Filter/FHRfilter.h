//! @class  FHRfilter
//! @brief
//! @author Jan Arild R�yneberg /Joar Eilevstj�nn
//! @date   16.10.2014
//!

#ifndef _FHR_FILTER_H_
#define _FHR_FILTER_H_
//#include <cstdint>
#include <stdint.h>
#include <string.h>


class FHRfilter
{
    
public:
	FHRfilter();
	~FHRfilter();
    
    void Init(void);
    uint8_t FilterSample(uint8_t a_fhr, int16_t a_accEnergy, uint8_t a_fhr_qual, uint32_t a_timeStamp);
    
private:
    FHRfilter(const FHRfilter& right);            //!< block the copy constructor
    FHRfilter& operator=(const FHRfilter& right); //!< block the assignment operator
    
    // Constants:
    static const uint16_t RATE_TIMEOUT = 5*1000;        //FS_FHR;
    static const int64_t TH_T_FETUS_FOUND = 20*1000;   // [ms]
    static const int64_t TH_T_FETUS_LOST = 20*1000;    // [ms]
    static const int16_t TH_ACC_ENRG = 100;
    
    uint16_t prev_fhr_proc;
    uint32_t last_rate_update;
    bool fetus_found;
    uint8_t prev_fhr_qual;
    uint8_t cur_fhr_qual;
    uint8_t cur_fhr_proc;
    int64_t first_good;
    int64_t first_bad;


};

#endif
