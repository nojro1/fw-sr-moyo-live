//! \file    FHRfilter.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    03.11.2014

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  03nov2014, JRo/JEv, Original.

//! Smoothen filter for FHR from doppler probe

#include "FHRfilter.h"


//
//! Construction
//
FHRfilter::FHRfilter()
{
    Init();
    
}

//
//! Destruction
//
FHRfilter::~FHRfilter()
{
    
}

//
//! Init the FHR filter
//
void FHRfilter::Init(void)
{
    // Inits:
    prev_fhr_proc = 0;
    cur_fhr_proc = 0;
    last_rate_update = 0;
    fetus_found = false;
    prev_fhr_qual = 0;
    cur_fhr_qual = 0;
    first_good = UINT32_MAX;
    first_bad = UINT32_MAX;
}

//
//! Filter one FHR sample from doppler probe
//
uint8_t FHRfilter::FilterSample(uint8_t a_fhr, int16_t a_accEnergy, uint8_t a_fhr_qual, uint32_t a_timeStamp)
{
   cur_fhr_qual = a_fhr_qual;
   
   // Fetus found status:
   if(prev_fhr_qual<2 && cur_fhr_qual >= 2) // transition to good quality
   {
        first_good = a_timeStamp;
        first_bad = UINT32_MAX;
   }
   else if(prev_fhr_qual>1 && cur_fhr_qual <= 1) // transition to bad quality
   {
        first_bad = a_timeStamp;
        first_good = UINT32_MAX;
   }
   
   if(!fetus_found && ((int64_t)a_timeStamp - first_good >= TH_T_FETUS_FOUND))
   {
        fetus_found = true; // fetus found
   }
   else if(fetus_found && (((int64_t)a_timeStamp - first_bad >= TH_T_FETUS_LOST) || (a_accEnergy > TH_ACC_ENRG)))
   {
        fetus_found = false; // fetus lost
        first_good = UINT32_MAX;
   }
   
   prev_fhr_qual = cur_fhr_qual;
   
   // Keep showing previous rate up to RATE_TIMEOUT
   if (a_fhr <= 1) // no rate?
   {
      if(fetus_found && ((a_timeStamp - last_rate_update) < RATE_TIMEOUT))
      {
            cur_fhr_proc = prev_fhr_proc; // keep showing previous rate
      }
      else
      {
            cur_fhr_proc = a_fhr;         
      }
   }
   else
   {
      cur_fhr_proc = a_fhr;
      last_rate_update = a_timeStamp;
   }
   
   prev_fhr_proc = cur_fhr_proc;
   
   return cur_fhr_proc;

}
