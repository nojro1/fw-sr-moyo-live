//! \file    IIRfilter.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    16.10.2014

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  16oct2014, JRo, Original.

#include "IIRfilter.h"

//
//! Filter constants for 200Hz sampling frequency
//
const float IIRfilter::m_IIR50HzSF200coeffs[ IIRfilter::IIR_FILTER_LEN ] = 
{
    0.854080685463467, 0.927040342731733, 0
};

//
// Construction
//
IIRfilter::IIRfilter()
{
    m_NotchIIRinsampHist = new float[ IIRfilter::IIR_FILTER_LEN ];
    m_NotchIIRoutsampHist = new float[ IIRfilter::IIR_FILTER_LEN ];
    
    IIRfilterInit();
    
    
}

//
// Destruction
//
IIRfilter::~IIRfilter()
{
    
}

// IIR filter init
void IIRfilter::IIRfilterInit(void)
{
    memset( m_NotchIIRinsampHist, 0, IIRfilter::IIR_FILTER_LEN * sizeof(float) );
    memset( m_NotchIIRoutsampHist, 0, IIRfilter::IIR_FILTER_LEN * sizeof(float) );
}

//
// Return int value of IIR filter one sample
//
int16_t IIRfilter::filterSample50HzSF200Hz(int16_t a_sample)
{
    return (int16_t)iirFloat(IIRfilter::m_IIR50HzSF200coeffs, (float)a_sample, m_NotchIIRinsampHist, m_NotchIIRoutsampHist);
}

//
// Return float value of IIR filter one sample
//
float IIRfilter::filterSample50HzSF200HzF(int16_t a_sample)
{
    return iirFloat(IIRfilter::m_IIR50HzSF200coeffs, (float)a_sample, m_NotchIIRinsampHist, m_NotchIIRoutsampHist);
}

// The IIR filter
float IIRfilter::iirFloat( const float *a_coeffs, float a_inSample, float *inSampHist, float *outSampHist)
{
    //Shift back in time and insert new sample in history in buffer
    inSampHist[2] = inSampHist[1];
    inSampHist[1] = inSampHist[0];
    inSampHist[0] = a_inSample;
    
    //Shift back in time and calculate new sample in history out buffer
    outSampHist[2] = outSampHist[1];
    outSampHist[1] = outSampHist[0];
    
    //200Hz sample freq
    outSampHist[0] = a_coeffs[1] * (inSampHist[0] + inSampHist[2]) - a_coeffs[0] * outSampHist[2];
    
    //Return new calculated value
    return outSampHist[0];
}
