
#include "ConsoleThread.h"
#include "ConsoleManager.h"

#include <string>
//#include <algorithm>

//Test
uint32_t m_CONThreadTestCtr = 0;

ConsoleThread::ConsoleThread()
{
    m_pManager = new ConsoleManager(this);
}

ConsoleThread::~ConsoleThread()
{
    delete m_pManager;
}

void ConsoleThread::sendIPC(THREAD_ID id, THREAD_SIGNAL s,uint32_t d1, uint32_t d2, void * pD)
{
    sigDispatch(id, s, d1, d2, pD, false);
}

void ConsoleThread::run(void *)
{
    //waitUntilAllThreadsStarted();
    DEBUG_TRACE("\r\n%s Pri: %d", getThreadName(), getPriority() );
    
    //Test
    /*
    while (1)
    {
        sleep(50); // we don't need to run often to keep up with user typing on a terminal
        m_CONThreadTestCtr++;
    }
    */
    //

    int ch;
    std::string s;
    s.reserve(32);


    uint32_t return_count = 0;
    while (1)
    {
        sleep(50); // we don't need to run often to keep up with user typing on a terminal
        
        m_CONThreadTestCtr++;

        ch = getchar();

        if (ch != EOF)
        {
            if (ch == '\r')
            {
                return_count++;
                if (return_count == 3)
                {
                    DEBUG_TRACE("\r\n\nPlease type 'help' or '?' for available options\r\n\n");
                }
                if (return_count >= 3)
                {
                    CONSOLE_PRINT("\r\n");
                    if (!s.empty())
                    {
                        transform(s.begin(), s.end(), s.begin(), tolower);
                        s += ' ';
                        m_pManager->parseConsoleCommand(s);
                        s.resize(0);
                    }
                    CONSOLE_PRINT(">");
                }

            }
            else
            {
                if (return_count >= 3)
                {
                    if (ch != '\b') // <> backspace
                    {
                        CONSOLE_PRINT("%c", ch);

                        if (s.length() < s.capacity())
                        {
                            s += ch;
                        }
                        else
                        {
                            s.resize(0);
                        }
                    }
                    else // backspace
                    {
                        if (!s.empty())
                        {
                            s.resize(s.length()-1);
                            CONSOLE_PRINT("\r\n>%s", s.c_str());
                        }
                    }
                }
            }
        }

    }

}



