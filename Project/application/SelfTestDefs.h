#ifndef _SELFTEST_DEFS_H_
#define _SELFTEST_DEFS_H_

static const uint32_t POST_STATUS_BITS_FLOW_SENSOR_NOT_CONNECTED  = 1 << 0;
static const uint32_t POST_STATUS_BITS_COMM_FAIL_ACU              = 1 << 1;
static const uint32_t POST_STATUS_BITS_HOTWIRE_A_DEFECT           = 1 << 2;
static const uint32_t POST_STATUS_BITS_HOTWIRE_B_DEFECT           = 1 << 3;
static const uint32_t POST_STATUS_BITS_ECG_SENSOR_CABLE_ERR       = 1 << 4;
static const uint32_t POST_STATUS_BITS_SDCARD_ERR                 = 1 << 5;
static const uint32_t POST_STATUS_BITS_ACCELEROMETER_ERR          = 1 << 6;
static const uint32_t POST_STATUS_BITS_COMM_FAIL_CO2              = 1 << 7;
static const uint32_t POST_STATUS_BITS_COMM_FAIL_DISPLAY          = 1 << 8;
static const uint32_t POST_STATUS_BITS_COMM_FAIL_POWER            = 1 << 9;
static const uint32_t POST_STATUS_BITS_ECG_ADC_ERR                = 1 << 10;
static const uint32_t POST_STATUS_BITS_SETUP_DATA_ERR             = 1 << 11;
static const uint32_t POST_STATUS_BITS_MEM_ERR                    = 1 << 12;

#endif