//
// Common system defines
//

#ifndef _COMMONDEFS_H_
#define _COMMONDEFS_H_

#include "board_cfg.h"
#include <stdint.h>
#include <stdbool.h>


enum SoundVolume_t
{
    eSVoff = 0,
    eSVhigh        
};

typedef struct
{
	uint8_t programCheckSum	        :1;
	uint8_t calibrationCheckSum     :1;
    uint8_t voltageRegulator        :1;
    uint8_t RTCcrystal	            :1;
	uint8_t displayComm	            :1;
	uint8_t ultraSoundTransComm     :1;
	uint8_t fuelGaugeComm           :1;
	uint8_t sdCard	                :1;
	uint8_t spare9	                :1;
	uint8_t spare10	                :1;
	uint8_t spare11	                :1;
	uint8_t spare12	                :1;
	uint8_t spare13	                :1;
	uint8_t spare14	                :1;
	uint8_t spare15	                :1;
    uint8_t spare16	                :1;
	uint8_t spare17	                :1;
	uint8_t spare18	                :1;
	uint8_t spare19	                :1;
	uint8_t spare20	                :1;
	uint8_t spare21	                :1;
	uint8_t spare22	                :1;
	uint8_t spare23	                :1;
	uint8_t spare24	                :1;
	uint8_t spare25	                :1;
    uint8_t spare26	                :1;
	uint8_t spare27	                :1;
	uint8_t spare28	                :1;
	uint8_t spare29	                :1;
	uint8_t spare30	                :1;
	uint8_t spare31	                :1;
	
} PostBits_t;


typedef union
{
	uint32_t		postWord;
	PostBits_t		postBits;
} Post_t;

typedef struct
{
    Post_t post;
    bool finished;
} POST_ctrl_t;


typedef struct
{
	uint8_t criticalErrorAlarm	:1;
	uint8_t abnormalFHRalarm	:1;
	uint8_t lowBatteryAlarm	    :1;
	uint8_t lostSignalAlarm     :1;
	uint8_t spare4	            :1;
	uint8_t spare5	            :1;
	uint8_t spare6	            :1;
	uint8_t spare7	            :1;
	
} AlarmBits_t;

typedef union
{
	uint8_t		alarmByte;
	AlarmBits_t	alarmBits;
} Alarm_t;

enum OperationalMode_t
{
    eOMnormal,
    eOMDataTransfer,
    eOMconfig,
    eOMbatteryCharge,
    eOMbatteryEmpty,
    eOMcriticalError,
    eOMshutdown,
    eOMtestMode
};

enum ShutdownReason_t 
{
    eSDRoffBtn, 
    eSDRusbDisConn, 
    eSDRcriticalErr, 
    eSDRusbSuspensionPowerloss,
    eSDRbatteryEmpty,
    eSDRepTimeout,
    eWatchdogTimeout
};

enum MassStorageInitResult_t
{
    eMSRUsbTimeout,
    eMSRUsbInitSuccess,
    eMSRNoSDcard
};

enum SoundTestCommand_t
{
    eSTCStop = 0,
    eSTCPlaysine200,
    eSTCPlaysine1000,
    eSTCPlayMedPriAlarm,
    eSTCPlayLowPriAlarm,
    eSTCPlayStartupSound
};

enum StatusLEDtestCommand_t
{
    eSLToff = 0,
    eSLTredLED,
    eSLTgreenLED,
    eSLTyellowLED
};

typedef struct
{
    uint32_t TimeStamp;
    int16_t ProbeAccEnergy;
    int16_t ProbeAccX;
    int16_t ProbeAccY;
    int16_t ProbeAccZ;
} Utilrecord_t;

typedef struct
{
    int16_t AccX;
    int16_t AccY;
    int16_t AccZ;
} Acceleration_t;

typedef struct
{
    uint32_t TimeStamp;
    uint8_t FHR;
    uint8_t FHRquality;
    uint8_t MHR;
    uint8_t ProbeTemp;
} FHRrecord_t;


//Config
const uint32_t CFG_MIN_END_OF_EPISODE_TIME          = 10;       //Sec
const uint32_t CFG_MAX_END_OF_EPISODE_TIME          = 86400;    //Sec
const uint32_t CFG_MIN_VALID_EP_TIME                = 10;       //Sec
const uint32_t CFG_MAX_VALID_EP_TIME                = 32000;    //Sec
const uint32_t CFG_MIN_EP_TOO_SHORT_TIME            = 10;       //Sec
const uint32_t CFG_MAX_EP_TOO_SHORT_TIME            = 32000;    //Sec
const uint16_t CFG_MIN_MHR_UPDATE_TIME              = 1;        //Sec
const uint16_t CFG_MAX_MHR_UPDATE_TIME              = 20;       //Sec
const uint16_t CFG_MIN_MHR_AVG_BUF_LEN              = 3;        //Detections
const uint16_t CFG_MAX_MHR_AVG_BUF_LEN              = 30;       //Detections
const uint16_t CFG_MIN_LOST_SIGNAL_ALARM_TIME       = 1;        //Sec
const uint16_t CFG_MAX_LOST_SIGNAL_ALARM_TIME       = 255;      //Sec
const uint16_t CFG_MIN_ATYPICAL_FHR_ALARM_TIME      = 1;        //Sec
const uint16_t CFG_MAX_ATYPICAL_FHR_ALARM_TIME      = 3600;     //Sec
const uint16_t CFG_MIN_ABNORMAL_FHR_ALARM_TIME      = 1;        //Sec
const uint16_t CFG_MAX_ABNORMAL_FHR_ALARM_TIME      = 3600;     //Sec
const uint16_t CFG_MIN_ATYPICAL_FHR_ALARM_HIGH      = 1;        //BPM
const uint16_t CFG_MAX_ATYPICAL_FHR_ALARM_HIGH      = 250;      //BPM
const uint16_t CFG_MIN_ATYPICAL_FHR_ALARM_LOW       = 1;        //BPM
const uint16_t CFG_MAX_ATYPICAL_FHR_ALARM_LOW       = 250;      //BPM        
const uint16_t CFG_MIN_ABNORMAL_FHR_ALARM_HIGH      = 1;        //BPM
const uint16_t CFG_MAX_ABNORMAL_FHR_ALARM_HIGH      = 250;      //BPM
const uint16_t CFG_MIN_ABNORMAL_FHR_ALARM_LOW       = 1;        //BPM
const uint16_t CFG_MAX_ABNORMAL_FHR_ALARM_LOW       = 250;      //BPM        
const uint16_t CFG_DISABLED                         = 0;        //Off
const uint16_t CFG_ENABLED                          = 1;        //On        
const float    CFG_MIN_AMPSENSE                     = 0.0;
const float    CFG_MAX_AMPSENSE                     = 10.0;
const uint16_t CFG_MIN_DOPPLER_SOUND_SPEAKER_GAIN   = 1;
const uint16_t CFG_MAX_DOPPLER_SOUND_SPEAKER_GAIN   = 10;
const uint16_t CFG_MIN_DOPPLER_SOUND_GAIN           = 0;        //x4
const uint16_t CFG_MAX_DOPPLER_SOUND_GAIN           = 1;        //x1
    


//Battery
const uint8_t BATT_TWO_THIRD_FULL   = 66; //%
const uint8_t BATT_ONE_THIRD_FULL   = 33; //%
const uint8_t BATT_LOW_LIMIT        = BATT_ONE_THIRD_FULL;
const uint8_t BATT_CRITICAL_LIMIT   = 8; //%
const uint8_t BATT_EMPTY_LIMIT      = 5; //%

//Backlight
const uint16_t BACKLIGHT_START      = 0;//DC_100_PERC/100;
const uint16_t BACKLIGHT_DIM        = DC_100_PERC/6;
const uint16_t BACKLIGHT_FULL       = DC_100_PERC/2;

//LEDs
const uint16_t RED_LED_REDUCED_PWM      = 170;           //When FHR LED color is yellow
const uint16_t GREEN_LED_REDUCED_PWM    = 800;

const uint16_t RED_LED_FULL_PWM         = DC_100_PERC;
const uint16_t GREEN_LED_FULL_PWM       = DC_100_PERC;




#endif // _COMMONDEFS_H_

