//! \file    ADThread.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    28.02.2014

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  28Feb2014, JRo, Original.

#include "adthread.h"
#include "utilfuncs.h"


// static member init
ADThread*  ADThread::m_pAdTh = 0;

//Test
uint32_t m_ADThreadTestCtr = 0;

/*
===============================================================================
    
   Construction

===============================================================================
*/
ADThread::ADThread()
{
    if(m_pAdTh == 0)
    {
        m_AdcEcgSamplesBuffer.Create(QrsDetector::MAX_ECG_BUFFER_SIZE);
        m_AdcEcgFileSamplesBuffer.Create(QrsDetector::MAX_ECG_BUFFER_SIZE);
        mAdcEcgRawBuffer.Create(Adc::ADC_ECG_SAMPLES_BUF_SIZE*2);
        m_QrsDetector = new QrsDetector();
        m_pAdTh = this;
        m_HRmother = 1000; //To ensure report first fime
        m_dbgCnt = AD_DEBUG_TIME;
        m_debugADC = false;
        m_ecgElectrodesTouched = false;
        m_electrodesTimer = 0;
        m_maternalHrDispTimer = 0;
        m_maternalHrDispTime = 2000/THREAD_TICK_TIME; //Default 2 sec
        m_operationalMode = eOMnormal;
        m_impAvg = 0;
        m_impAvgRemaining = 0;
        m_impAvgNoSamples = 0;
        m_iirFilter = new IIRfilter();
    }
}

/*
===============================================================================
    
   Destruction

===============================================================================
*/
ADThread::~ADThread()
{
    
}

/*
===============================================================================
    
   Process Thread

===============================================================================
*/
void ADThread::run(void *pmsg)
{
    
	//Startup routines here

    //Test
    /*
    while(1)
    {
        IPC ipc;
        
        sigWait(ADThread::THREAD_TICK_TIME, &ipc);  //Wait for a signal or OS schedule
    
        m_ADThreadTestCtr++;
    }
    */
    //waitUntilAllThreadsStarted();
    DEBUG_TRACE("\r\n%s Pri: %d", getThreadName(), getPriority() );
    
    if(Config::getpConfig()->IsValid() == true)
    {
        float ampSens = Config::getpConfig()->GetConfigFloat(Config::eCfgEcgAmpSens);
        m_QrsDetector->SetAmpSens(ampSens);
        CONSOLE_PRINT("\r\nAMP_SENS set to %f from config\r\n", ampSens);
        
        m_maternalHrDispTime = Config::getpConfig()->GetConfig(Config::eCfgHRupdateTime);
        m_maternalHrDispTime = MIN(CFG_MAX_MHR_UPDATE_TIME, m_maternalHrDispTime); //Limit
        m_maternalHrDispTime = MAX(CFG_MIN_MHR_UPDATE_TIME, m_maternalHrDispTime); //Limit
        m_maternalHrDispTime = (uint16_t)(((uint32_t)m_maternalHrDispTime * 1000) / THREAD_TICK_TIME);
        
        CONSOLE_PRINT("\r\nMaternal HR update time set to %u from config\r\n", m_maternalHrDispTime);
        
        uint16_t len = Config::getpConfig()->GetConfig(Config::eCfgHRavgBufLen);
        len = MIN(CFG_MAX_MHR_AVG_BUF_LEN, len); //Limit
        len = MAX(CFG_MIN_MHR_AVG_BUF_LEN, len); //Limit
        
        m_QrsDetector->SetAvgBufLen((uint8_t)len);
        
        CONSOLE_PRINT("\r\nHR avg buffer len set to %u from config\r\n", len);
    }
    
    while(1)                //Running with RTOS
    {
        IPC ipc;
        int16_t ecg;
        EcgBuffer::ECGrecord_t ecgRecord;
        
        sigWait(ADThread::THREAD_TICK_TIME, &ipc);  //Wait for a signal or OS schedule

        m_ADThreadTestCtr++;
        
        //Get Adc ecg samples
        while(mAdcEcgRawBuffer.GetNumItems() > 0)
        {
            //Get value and subtract baseline so we get int16
            mAdcEcgRawBuffer.Get(&ecg, -2048);

            m_AdcEcgSamplesBuffer.Put(ecg);
            ecgRecord.TimeStamp = OS_GetTime();
            ecgRecord.Sample = ecg;
            ecgRecord.Sample50HzFiltered = ADThread::get()->m_iirFilter->filterSample50HzSF200Hz(ecg);       
            m_AdcEcgFileSamplesBuffer.Put(ecgRecord);
        }
                
        if(m_operationalMode == eOMnormal)
        {
            maternalHRhandler();
        }
        else if(m_operationalMode == eOMtestMode)
        {
            maternalECGtestHandler();
            impedanceAvgMeasurement();
        }
        
        switch(ipc.m_sig)
        {
			case S_OSTIMEOUT:
            {
                if(m_debugADC)
                {
                    if(m_dbgCnt)
                    {
                        m_dbgCnt--;
                    }
                    else
                    {
                        uint16_t v5 = HwAbsl::getpHw()->getpADC()->read(AD_5V);
                        uint16_t amb = HwAbsl::getpHw()->getpADC()->read(AD_AMB_LIGHT);
                        uint16_t imp = HwAbsl::getpHw()->getpADC()->read(AD_IMPEDANCE);
                        uint16_t ecg = HwAbsl::getpHw()->getpADC()->read(AD_ECG);
                        DEBUG_TRACE("AD: 5V: %4u AMB: %4u IMP: %4u ECG: %4u\r\n", v5, amb, imp, ecg);
                        m_dbgCnt = AD_DEBUG_TIME;
                    }
                }
                
            }
    		    break;                
            
            case S_ECG_AMP_SENS_UPDATE:
                {
                    uint32_t tmp = ipc.m_data1;
                    float tmpf = *((float*)&tmp);   //convert from uint to float
                    m_QrsDetector->SetAmpSens(tmpf);
                }
                break;
            case S_ADC_DEBUG:
                if(ipc.m_data1 == 1)
                {
                    m_debugADC = true;
                    m_dbgCnt = AD_DEBUG_TIME;
                    HwAbsl::getpHw()->getpADC()->start();
                }
                else
                {
                    m_debugADC = false;
                }
                break;
            case S_HR_DISPUPDATE_TIME_UPDATE:
                {
                    uint32_t hrUdtime = ipc.m_data1;
                    hrUdtime = MIN(CFG_MAX_MHR_UPDATE_TIME, hrUdtime); //Limit
                    hrUdtime = MAX(CFG_MIN_MHR_UPDATE_TIME, hrUdtime); //Limit
                    m_maternalHrDispTime = (hrUdtime * 1000)/THREAD_TICK_TIME;
                }
                break;
            case S_HR_BUF_LEN_UPDATE:
                {
                    uint32_t len = ipc.m_data1;
                    len = MIN(CFG_MAX_MHR_AVG_BUF_LEN, len); //Limit
                    len = MAX(CFG_MIN_MHR_AVG_BUF_LEN, len); //Limit

                    m_QrsDetector->SetAvgBufLen((uint8_t) len);
                }
                break;
            case S_SET_OPERATIONAL_MODE:
                switch((OperationalMode_t)ipc.m_data1)
                {
                    case eOMnormal:
                    case eOMDataTransfer:
                    case eOMconfig:
                    case eOMbatteryCharge:
                    case eOMbatteryEmpty:
                    case eOMcriticalError:
                    case eOMshutdown:
                        m_operationalMode = (OperationalMode_t)ipc.m_data1;
                        break;
                    case eOMtestMode:
                        m_operationalMode = (OperationalMode_t)ipc.m_data1;
                        
                        //Turn on 100KHz
                        HwAbsl::getpHw()->setImpedanceFreq(true);
                        
                        // Enable ADC timer
                        //TIM_Cmd(ADC_ECG_SAMPLE_TIMx,ENABLE); TODO
                        break;
                    default:
                        break;
                }
                break;
            case S_TEST_COMMAND_MEASURE_MATERNAL_ECG:
                if(m_operationalMode == eOMtestMode)
                {
                    m_QrsDetector->initEcgMinMaxMeasurement(&m_AdcEcgSamplesBuffer, 3000);
                    
                }
                break;
            case S_TEST_COMMAND_MEASURE_IMPEDANCE:
                if(m_operationalMode == eOMtestMode)
                {
                    initAvgImpedanceMeasurement(500);
                }
                break;
            default:
				//Handle unrecognized signals
				sigUnknown(&ipc);
                break;
        }
    }
}

/*
===============================================================================
    
    GetECGFileData()

    Get ECG record(s) from maternal ECG file buffer

===============================================================================
*/
uint16_t ADThread::GetECGFileData(EcgBuffer::ECGrecord_t *buf, uint16_t noSamples)
{
    uint16_t noAvailableSamples = m_AdcEcgFileSamplesBuffer.GetNumItems();
    uint16_t i;
    EcgBuffer::ECGrecord_t* buffer = buf;
    
    //TODO 50Hz filter dummy signal here. Rename dummy signal 
    if(noAvailableSamples < noSamples)
    {
        for(i = 0; i < noAvailableSamples; i++)
        {
            *buffer++ = m_AdcEcgFileSamplesBuffer.Get();
        }
        return noAvailableSamples;
    }
    else
    {
        for(i = 0; i < noSamples; i++)
        {
            *buffer++ = m_AdcEcgFileSamplesBuffer.Get();
        }
    }
      
    return noSamples;
    
}

/*
===============================================================================
    
    TMR_ADCsample_ISR()

    ISR handler for getting ADC samples from ECG channel

===============================================================================
*/
//TODO: Move to thread (timeout handler)
#if(0)
void ADThread::TMR_ADCsample_ISR(void)
{
    
    int16_t ecg;
    EcgBuffer::ECGrecord_t ecgRecord;
    /* Test on TIM10 interrupt */
    if (TIM_GetITStatus(TIM10, TIM_IT_Update))
    {
        /* TIM10 interrupt interrupt pending bit */
        TIM_ClearFlag(TIM10, TIM_FLAG_Update);
            
        ecg = HwAbsl::getpHw()->getpADC()->read(AD_ECG);
        ecg -= 2048; //Subtract baseline so we get int16
        
        ADThread::get()->m_AdcEcgSamplesBuffer.Put(ecg);
        
        ecgRecord.TimeStamp = OS_GetTime();
        ecgRecord.Sample = ecg;
        ecgRecord.Sample50HzFiltered = ADThread::get()->m_iirFilter->filterSample50HzSF200Hz(ecg);
        
        
        ADThread::get()->m_AdcEcgFileSamplesBuffer.Put(ecgRecord);
        
    }
    
    
}

#endif
/*
===============================================================================
    
    GetMaternalHR()

    Get current maternal heart rate

===============================================================================
*/
uint8_t ADThread::GetMaternalHR(void)
{
    return m_HRmother;
}

/*
===============================================================================
    
    maternalECGtestHandler()

    Handler for production test of ECG circuit

===============================================================================
*/
void ADThread::maternalECGtestHandler(void)
{
    m_QrsDetector->ECGminMax(&m_AdcEcgSamplesBuffer);
}

/*
===============================================================================
    
    maternalECGtestHandler()

    Detect electrodes touched and 
    run the QRS detector based on detection

    Update UI with changes in status

===============================================================================
*/
void ADThread::maternalHRhandler(void)
{
    bool qrsDetectorActive = m_QrsDetector->IsActive();
    
    m_QrsDetector->QRSdetectionSimplified(&m_AdcEcgSamplesBuffer);
        
    uint16_t imp = HwAbsl::getpHw()->getpADC()->read(AD_IMPEDANCE);
    
    if(imp < 150 && m_ecgElectrodesTouched) //No impdance -> Nobody is touching electrodes. Show question mark
    {
        m_ecgElectrodesTouched = false;
        m_electrodesTimer = ELECTRODES_NOT_TOUCHED_CHECK_TIME;
        

    }
    else if(imp > 200 && !m_ecgElectrodesTouched) //We have impdance -> Somebody is touching electrodes.
    {
        m_ecgElectrodesTouched = true;
        m_electrodesTimer = ELECTRODES_TOUCHED_CHECK_TIME;
        
        //Since QRS detector is not running when nobody touches electroded, we need to update now
        //to get correct timestamp on events
        m_QrsDetector->updateTime(OS_GetTime());
    }
    
    
    if(!qrsDetectorActive)
    {
        //Reset display timer if nobody is touching electrodes
        m_maternalHrDispTimer = 0;
        m_HRmother = 0;
    }
    else if(m_maternalHrDispTimer > 0)
    {
        //Update it if someone is touching electrodes and timer not elapsed
        m_maternalHrDispTimer--;
    }
    else if(m_maternalHrDispTimer == 0)
    {
        //Timer elapsed, and someone is touching electrodes. Update display
        uint16_t hrMother = m_QrsDetector->GetHR();
        
        if(hrMother != m_HRmother)
        {
            if(m_maternalHrDispTimer == 0)
            {
                //Timer elapsed. Do update
                m_maternalHrDispTimer = m_maternalHrDispTime;
                m_HRmother = hrMother;
                DEBUG_TRACE("\r\nHR mother:%u\r\n", hrMother);
                sigDispatch(P_UI, S_HR_MATERNAL_UPDATE, m_HRmother, 0 ,0);    //Update display
            
#ifndef NO_IMPEDANCE_MEASUREMENT
                sigDispatch(P_SUPER, S_HR_MATERNAL_UPDATE, m_HRmother, 0 ,0); //Update super
                
#else
                sigDispatch(P_SUPER, S_HR_MATERNAL_UPDATE, 0, 0 ,0); 
#endif
            }
        }
    
    }
        

    if(m_electrodesTimer > 1)
    {
        m_electrodesTimer--;
    }
    else if(m_electrodesTimer == 1)
    {
        m_electrodesTimer = 0;
        if(!m_ecgElectrodesTouched && qrsDetectorActive)
        {
            DEBUG_TRACE("\r\nS_ELECTRODES_TOUCHED = 0\r\n");
            sigDispatch(P_UI, S_ELECTRODES_TOUCHED,0,0,0);          //Update display
            sigDispatch(P_SUPER, S_HR_MATERNAL_UPDATE, 0, 0 ,0);    //Update super
            
            m_QrsDetector->SetActive(false);
            
            // Disable ADC timer
            //TIM_Cmd(ADC_ECG_SAMPLE_TIMx,DISABLE); //TODO
        }
        else if(m_ecgElectrodesTouched && !qrsDetectorActive)
        {
            DEBUG_TRACE("\r\nS_ELECTRODES_TOUCHED = 1\r\n");
            sigDispatch(P_UI, S_ELECTRODES_TOUCHED,1,0,0); //Update
            
            m_QrsDetector->SetActive(true);
            
            // Enable ADC timer
            //TIM_Cmd(ADC_ECG_SAMPLE_TIMx,ENABLE); //TODO
        }
    }
    

}

/*
===============================================================================
    
    initAvgImpedanceMeasurement()

    Init the production test for impedance measurement

===============================================================================
*/
void ADThread::initAvgImpedanceMeasurement(uint16_t a_measurementTimeMs)
{
    m_impAvg = 0;
    m_impAvgNoSamples = 0;
    
    if(a_measurementTimeMs > MAX_MEASUREMENT_TIME)
    {
        m_impAvgRemaining = MAX_MEASUREMENT_TIME/THREAD_TICK_TIME;
    }
    else
    {
        m_impAvgRemaining = a_measurementTimeMs/THREAD_TICK_TIME;
    }
}

/*
===============================================================================
    
    impedanceAvgMeasurement()

    Production test for impedance measurement

===============================================================================
*/
void ADThread::impedanceAvgMeasurement(void)
{
    m_impAvg += HwAbsl::getpHw()->getpADC()->read(AD_IMPEDANCE);
    m_impAvgNoSamples++;
    
    if(m_impAvgRemaining == 1) //Once
    {
        CONSOLE_PRINT("R111 %u\r\n", m_impAvg/m_impAvgNoSamples);
        m_impAvgRemaining--;
    }
    else if(m_impAvgRemaining)
    {
        m_impAvgRemaining--;
    }
}
    
