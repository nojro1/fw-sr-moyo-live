//! @class  QRSdetector
//! @brief
//! @author Jan Arild R�yneberg
//! @date   17.03.2014
//!

#ifndef _QRS_DETECTOR_H_
#define _QRS_DETECTOR_H_

#include "thread.h"
#include "includes.h"
#include <vector>
#include <list>
#include <cmath>
#include "records.h"
#include "ringbuffer.h"
#include "iirfilter.h"
#include "events.h"


class QrsDetector
{
    
public:
	QrsDetector();
	~QrsDetector();
    
    void QRSdetectionSimplified(RingBuffer * a_EcgSamples);
    uint16_t GetHR(void);
    void SetAmpSens(float a_ampSens);
    void SetAvgBufLen(uint8_t a_avgBufLen);
    void SetActive(bool a_active);
    bool IsActive(void);
    void updateTime(uint32_t a_OStime);
    void ECGminMax(RingBuffer * a_EcgSamples);
    void initEcgMinMaxMeasurement(RingBuffer * a_EcgSamples, uint16_t a_measurementTimeMs);
    
    static const uint16_t MAX_ECG_BUFFER_SIZE = 100;
    
    
	
protected:

private:
    QrsDetector(const QrsDetector& right);            //!< block the copy constructor
    QrsDetector& operator=(const QrsDetector& right); //!< block the assignment operator
    
    static const uint16_t THREAD_TICK_TIME = 100;
    
    static const uint16_t ECG_SAMPLE_FREQ = 200; //Hz
    
    //FIR filter
    // maximum length of filter than can be handled
    static const uint8_t MAX_FIR_FLT_LEN = 35;
    
    // buffer to hold all of the input samples
    static const uint16_t FIR_BUFFER_LEN = MAX_FIR_FLT_LEN - 1 + MAX_ECG_BUFFER_SIZE;
    static const uint8_t LP_FIR_FILTER_LEN = 27;
    static const uint8_t HP_FIR_FILTER_LEN = 27;
    
    //IIR filter
    // Number of samples we need to keep for history
    static const uint8_t IIR_HIST_BUFFER_LEN = 3;
    // IIR filter length
    static const uint8_t IIR_FILTER_LEN = 3;
    

    //QRS detection algorithm
    //---------------------------------------------------------------------------------------------------
    // CONSTANTS AND INITS
    //---------------------------------------------------------------------------------------------------
    // DEFAULT VALUES:
    static const uint16_t fs = ECG_SAMPLE_FREQ; // Default sampling rate [Hz]
    static const float max_x_val;                       // Default scaled to x in millivolt
    static const float CLIP_VOLTAGE;                    // Approximate hardware clipping voltage [mV]
    static const uint16_t FS_ACC = 100;                 // Acceleration sampling rate [Hz]
    static const uint16_t FS_RATIO = fs/FS_ACC;         // Ratio between acc and ecg sampling rate 
    
    // CONSTANTS K�HLER ALGORITHM:
    static const float LAMBDA_K;			// FLOAT forgetting factor K (amplitude of high-frequency sequence)
    static const float GAIN_K;	            // FLOAT constant gain used in K calculation
    static const float LAMBDA_D;			// FLOAT forgetting factor D (number of zero crossings per segment)
    static const float LAMBDA_ATH;			// FLOAT forgetting adaptive detection threshold
    static const int16_t REFRAC_PERIOD;     // INT refractory period after last detection [samples]
    static const int16_t DELAY;             // INT Delay between x and y [samples]
    
    // ADD'L CONSTANTS:
    static const float MAX_ADAP_TH;			// FLOAT Maximum value of adap_th (to help specificity)
    
    // ECG ENERGY (signal disable) LOGIC:
    static const float ENRG_FF;             // FLOAT Energy calculation forgetting factor
    static const float ENRG_TH;             // FLOAT Energy threshold (relative to max x value)
    static const float ABSX_TH;             // FLOAT Signal absolute amplitude threshold [mv]
    static const float DISABLE_TIMEOUT;     // INT Time to keep algorithm disabled after energy below threshold again

    //SANITY CHECK CONSTANTS:
    static const int16_t MAX_RATE_CHANGE_1_BPM; // INT [bpm]
    static const int16_t MAX_RATE_CHANGE_2_BPM; // INT [bpm]
    static const float TH_MAX_GOODNESS_DROP;	// FLOAT [0-1]
    static const int16_t MIN_RATE;				// INT [bpm]
    static const int16_t RATE_TIMEOUT;          // INT Time to keep last heart rate dsiplayed before "-?-" [samples]
    static const int16_t INIT_TOO_HIGH_BPM;     // INT Threshold to indicate second detection potentially being wrong [bpm]
    static const int16_t EXTREME_RATE_BPM;      // INT Threshold indicating very high rate which should always be sanity checked
    
    static const float ACC_FILT_FF;             // Acceration smoothing filter forgetting factor 
    static const float ACC_AVE_FF;              // Acceleration average value forgetting factor
    static const float ACC_ENRG_FF;             // Acceleration energy forgetting factor
    static const float ACC_ENRG_FF_RAPID;       // Rapid acceleration energy forgetting factor
    static const int16_t ACC_TH_RAPID;          // Acceleration energy where rapid forgetting factor kicks in
    static const int16_t ACC_TH_ENRG_H;         // High acceleration energy threshold
    static const int16_t ACC_TH_ENRG_M;         // Medium acceleration energy threshold
    static const int16_t ACC_TH_ENRG_L;         // Low acceleration energy threshold
    static const int16_t ACC_ENRG_HYSTERESIS;   // Hysteresis around medium acceleration threshold
    static const int16_t TH_ACC_X_REST;         // Acc threshold to use to determine if sensor is on table [mg]
    static const int16_t TH_ACC_Y_REST;         // Acc threshold to use to determine if sensor is on table [mg]
    static const int16_t TH_ACC_Z_REST;         // Acc threshold to use to determine if sensor is on table [mg]

    
    float	    K;					// Amplitude of high-frequency sequence
    int16_t	    signK;				// Sign of K (representing factor (-1)^n)
    float	    z;					// Signal after high-frequency sequence added
    float	    z_prev;				// Previous value of z
    float	    D;					// Zero crossing count signal [0-1]
    float	    D_min;				// Minimum value of D
    float	    adap_th;			// Adaptive detection threshold. [0-1]
    uint32_t    n_D_below_th;		// Time of last zero crossing below threshold
    int16_t	    instrate;			// Instantaneous heart rate [bpm] (NOTE: rounded to make integer)
    int16_t	    averate;			// "Average" heart rate [bpm] - THIS IS THE DISPLAYED HEART RATE
    bool	    search_mode;		// Search for r-wave mode flag
    bool	    update_rate;		// Update output rate flag
    float       r_wave_amp;         // FLOAT Current detection amplitude
    float       ave_r_wave_amp;     // FLOAT Average R wave amplitude

    uint32_t    r_wave;				// 
    uint32_t    prev_r_wave;		// 
    int16_t	    prev_instrate;		// 
    int16_t	    prev_prev_instrate; // 
    uint32_t    prev_prev_r_wave;	// 
    int16_t	    instrate_alt;		// Alternative instantaneous heart rate [bpm]
    uint32_t    prev_rate_update;	// Time of last rate update (used in rate timeout logic)
    int16_t	    rate_mean;			// Average rate [bpm]
    int16_t	    rate_median;		// Median rate [bpm]
    float	    goodness;			// Detection "goodness" measure (higher number means more certain detection) [0-1]
    float	    prev_goodness;		// Previous goodness value
    float	    prev_prev_goodness; // Previous previous goodness value
    int32_t	    ri;					// Detected compl7exes counter
    uint32_t    n;					// Time tick/sample counter
    float       y_cur;              // FLOAT Current filtered and enhanced signal sample 
    float       x_cur;              // FLOAT Current unfiltered ("original") signal sample 
    float       energy;             // FLOAT Signal energy
    bool        disabled;           // BOOLEAN If true, algorithm will not detect QRSs
    uint32_t     disable_time;       // INT Time of last disable=true
    int16_t     prev_deleted_instrate; //Last deleted instantaneous heart rate [bpm] (02.01.13: to avoid deadlock)
    float       TH_MIN_R_AMP;       // FLOAT Minimum r_amp value to detect a QRS (Const)
    float       th_min_r_amp;       // FLOAT Minimum r_amp value to detect a QRS (Var)
    float       AMP_CUTOFF;         // FLOAT Amplitudes below this value will be set to zero (to help specificity)
    float       AMP_SENS;           // Factor to adjust threshold for noise rejection
    float       x_no50Hz;
    bool        adjust_detect;      // BOOLEAN Flag indicating current detection has been adjusted (deleted/ignored)
    bool        prev_adjust_detect; // BOOLEAN Flag with previous value of adjust_detect
    int16_t     AVG_BUF_LEN;		// INT Number of QRS complexes in rate mean and median calculation
    float       AVE_R_AMP_FF;       // FLOAT forgetting factor for calculation of average r_amp value.
    float       AVE_R_AMP_PORTION;  // FLOAT Default value of portion [0-1] of ave_r_amp to use in amplitude threshold.

    bool        table_angle;        // BOOLEAN HR sensor angle likely lying on table flag
    bool        acc_noisy;          // BOOLEAN Flag indicating noise detected from acceleration
    float       acc_ave;            // FLOAT Average acceleration energy
    float       acc_energy;         // FLOAT Acceleration energy (using all axes)
    float       acc_x_filt_mg;      // FLOAT Filtered x axis acceleration sample (could likely be INT)
    float       acc_y_filt_mg;      // FLOAT Filtered y axis acceleration sample (could likely be INT)
    float       acc_z_filt_mg;      // FLOAT Filtered z axis acceleration sample (could likely be INT)
    uint16_t    sigqual;            // INT Signal quality flag
    uint16_t    oldSigQual;
    float       ave_r_amp_p;        // FLOAT Portion [0-1] of ave_r_amp to use in amplitude threshold
    
    
    std::list<int16_t> instrates;

    //Display
    int16_t m_HR;
    int16_t m_prevHR;
    uint16_t m_dispUpdateTimer;
    uint16_t m_displayUpdateTime;
    
    // Filter coeffs
    static const float m_LpFIRcoeffs[ LP_FIR_FILTER_LEN ];
    static const float m_HpFIRcoeffs[ HP_FIR_FILTER_LEN ];
    
    // Array to hold FIR filter input samples
    float *m_LpFIRinsamp;
    float *m_HpFIRinsamp;
    
    // Array to hold FIR high pass filter output samples
    float *m_HpFIRoutput;
    
    // Array to hold FIR low pass filter output samples
    float *m_LpFIRoutput;
    
    float *m_ecgSamples;
    
    std::vector<records::EcgQRSdetRecord_t> m_ecgRecords;
    
    //Amp sens from config
    float m_ampSens;
    
    bool m_active;
    
    int16_t m_ecgMin;
    int16_t m_ecgMax;
    uint16_t m_ecgMinMaxRemaining;
    static const uint16_t MAX_MEASUREMENT_TIME = 10000; //ms
    
    IIRfilter* m_testIirFilter;
    
    void initQRSdetection();
    void addToList(std::list<int16_t> * a_list, int16_t a_value, uint16_t a_maxItems);
    void removeFromList(std::list<int16_t> * a_list);
    int32_t median(std::list<int16_t> a_input);
    int32_t mean(std::list<int16_t> * a_input);
    void firFloat( const float *coeffs, float *input, float *inSamp, float *output, int length, int filterLength );
    float iirFloat( const float *a_coeffs, float a_inSample, float *inSampHist, float *outSampHist);
    void initFilters();
    void FIRfilterInit( float *inSamp, uint16_t length );
    void IIRfilterInit( float *inSamp, float *outSamp, uint16_t length );
    float sign(float x);
    
    
    
};

#endif
