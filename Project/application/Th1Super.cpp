//! \file    th1super.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    02.12.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  02dec2013, JRo, Original.

#include "includes.h"
#include "th1super.h"
#include "Moyo_0510_0123_GOLDEN.h" //Image for fuelgauge (Golden image)
#include "utilfuncs.h"

//Test
uint32_t m_SUPThreadTestCtr = 0;

static Mutex * fhrBufferMutex = 0;

#ifdef DEBUG_EPISODE_HANDLER
    static uint16_t accSignalTimer = 0;
    static uint16_t motionTimer = 0;
    static uint16_t endOfEpisodeTimer = 0;
    uint32_t validEpisodeTimer = 0;
#endif

#ifdef DEBUG_ALARM_HANDLER
    
    static uint8_t fhrquality = 0;
    static Th1Super::FhrAlarmState_t fhrAlarmState = Th1Super::eFASInit;
    static uint16_t fhrAlarmRecoverTimer = 0;
    static uint16_t fhrAtypicalAlarmTimer = 0;
    static uint16_t fhrAbnormalAlarmTimer = 0;
    
    static bool disp_criticalErrorAlarm = false;
	static bool disp_abnormalFHRalarm = false;
	static bool disp_lowBatteryAlarm = false;
	static bool disp_lostSignalAlarm = false;
    
    static bool ack_criticalErrorAlarm = false;
	static bool ack_abnormalFHRalarm = false;
	static bool ack_lowBatteryAlarm = false;
	static bool ack_lostSignalAlarm = false;
    
    static bool aud_criticalErrorAlarm = false;
	static bool aud_abnormalFHRalarm = false;
	static bool aud_lowBatteryAlarm = false;
	static bool aud_lostSignalAlarm = false;
    
    static Th1Super::FhrAlarmState_t lostSignalAlarmState = Th1Super::eFASInit;
    
    static uint16_t fhrLostSignalAlarmTimer = 0;
    static uint16_t fhrLostSignalFilterTimer = 0;
#endif


/*
===============================================================================
    Construction
===============================================================================
 */
Th1Super::Th1Super()
{
    //m_pConfig = 0;
    m_pAdcTh = 0;
    m_resyncSound = true;
    m_pPitchCtrl = &pitchCtrl;
    m_pPitchCtrl->setpoint = 50;
    m_pPitchCtrl->measured = 0;
    m_pPitchCtrl->PrevErr = 0;
    m_pPitchCtrl->Ierror = 0;
    m_pPitchCtrl->Kp = 20;	// P
	m_pPitchCtrl->Ki = 0;	// I
	m_pPitchCtrl->Kd = 10;	// D
	m_pPitchCtrl->Ko = 10; // output scaler
    m_pPitchCtrl->calcBaseLine = HwAbsl::getpHw()->getpDac()->calcDacTimerPeriodFromSampleFreq(FHRMAudio::AUDIO_SOURCE_SAMPLE_FREQ);
    m_pPitchCtrl->nomBaseLine = m_pPitchCtrl->calcBaseLine;
    m_pPitchCtrl->minOutput = m_pPitchCtrl->nomBaseLine - DAC_TIM_VAL_MAX_DEV;
    m_pPitchCtrl->maxOutput = m_pPitchCtrl->nomBaseLine + DAC_TIM_VAL_MAX_DEV;
    m_soundPitchCtrl = eSPCInit;
    m_sndSampleRate = 0;
    m_sndSampleRateChangedCtr = 0;
    m_ulsndTransComm = eUSTCcommunicating;
    m_fhrLostSignalAlarmState = eFASInit;
    m_fhrAlarmState = eFASInit;
    m_fhrAtypicalAlarmLimitLow = 110;
    m_fhrAtypicalAlarmLimitHigh = 160;
    m_fhrAbnormalAlarmLimitLow = 100;
    m_fhrAbnormalAlarmLimitHigh = 180;
    m_fhrAtypicalAlarmTime = FHR_ATYPICAL_ALARM_ON_TIME;
    m_fhrAbnormalAlarmTime = FHR_ABNORMAL_ALARM_ON_TIME;
    
    m_displayAlarm.alarmByte = 0;
    m_audioAlarm.alarmByte = 0;
    m_alarmAcknowlegde.alarmByte = 0;
    m_alarmSoundTimer = 0;
    m_alarmType = eATnoAlarm;
    m_alarmSoundEnabled = false; //Do not play alarm sounds upon startup
    m_batteryRemainingCapacity = UINT8_MAX; //To ensure battery capacity is updated at startup
    m_batteryFull = false;
    
    
#ifdef USE_VOLTAGE_AS_SHUTDOWN_CRITERIA
    m_batteryVoltagemv = INT32_MAX;
#endif
    
    m_batteryEmpty = false;
    m_operationalMode = eOMconfig;
    m_probeTemperature = 0;
    m_probeTemperatureTimer = 0; //Fire immediately
    m_fuelGaugeTmr = FUEL_GAUGE_READ_TIME;
    m_batteryDebugOut = false;
    m_probeSoundDebugOut = false;
    
    //m_pEvents = 0;
    
    m_FhrRecords.reserve(FHR_RECORDS_BUFFER_SIZE);
    m_FhrFileNextSampleTime = 0;
    m_fetalHR = 255;
    m_fetalHRSigQual = FHRMps::eSQnotSet;
    
    
    m_endOfEpisodeTime = 300 * (1000/THREAD_TICK_TIME); //Default 5 minutes
    m_episodeTimers.endOfEpisodeTimer = m_endOfEpisodeTime;
    
    m_validEpisodeTime = 900 * (1000/THREAD_TICK_TIME); //Default 15 minutes
    m_episodeTimers.validEpisodeTimer = m_validEpisodeTime;
    
    m_episodeTooShortTime = 300 * (1000/THREAD_TICK_TIME); //Default 5 minutes
    m_episodeTimers.episodeTooShortTimer = m_episodeTooShortTime;
    
    m_FhrLostSignalAlarmTime = (60000/*5000*//THREAD_TICK_TIME); //Default 60 sec
    
    m_wdKickTmr = 0; //We want to kick as soom as possible
    
    //Init POST
    m_POSTstatus.finished = false;
    m_POSTstatus.post.postWord = 0;
    
    m_maternalHR = 0;
    m_episodeTimers.FHRvalidTimer = END_OF_EP_TMOUT_FILTER_TIME;
    m_episodeTimers.MHRvalidTimer = END_OF_EP_TMOUT_FILTER_TIME;
    m_episodeTimers.accSignalTimer = END_OF_EP_TMOUT_FILTER_TIME;
    m_episodeTimers.FHRinvalidTimer = END_OF_EP_TMOUT_FILTER_TIME;
    m_episodeTimers.motionTimer = MOTION_TIMEOUT;
    
    m_powerGoodTimer = POWER_GOOD_FILTER_TIME;
    
    m_massStorageTimeout = false;
    
    
    m_keyPressTestTimer = 0;
    m_keyTestState = eKTSidle;
    m_currentKeyTested = eKeyNone;
    m_UItestType = eUITTnone;
    m_testpinActive = false;
    
}

/*
===============================================================================
    Destruction
===============================================================================
 */
Th1Super::~Th1Super()
{
}

/*
===============================================================================
    DoPid()

    Helper methode for syncronisation of sound data rate between 
    doppler sensor module and Moyo sound system.
===============================================================================
 */
uint32_t Th1Super::DoPid(ctrlInfo_t *p) 
{ 
    int32_t Perror;
	int32_t output; 

	
    // Error
	Perror = (p->setpoint - p->measured); 
  	
    // Derivative error is the delta Perror over Td
    output = (p->Kp*Perror + p->Kd*(Perror - p->PrevErr) + p->Ki*p->Ierror)/p->Ko;
                       
    p->PrevErr = Perror; 
    output = m_pPitchCtrl->nomBaseLine + output;
        
    // Accumulate integral error or limit output,
	// stop accumulating when output saturates
    if (output >= p->maxOutput) 
	{
        output = p->maxOutput; 
	}   
	else if (output <= p->minOutput) 
    {
	    output = p->minOutput; 
	}
    else
	{ 
        p->Ierror += Perror; 
		if(p->Ierror > 500)
		{
			p->Ierror = 500;
		}
        else if(p->Ierror < -500)
		{
			p->Ierror = -500;
		}
    }

	if(m_probeSoundDebugOut)
    {
#ifndef NDEBUG 
        int16_t pPid = p->Kp*Perror;
        int16_t iPid = p->Ki*p->Ierror;
        int16_t dPid = p->Kd*(Perror - p->PrevErr);
        int32_t oPid = output;
        DEBUG_TRACE("\r\nS:%d, M:%d, P:%d, I:%d, D:%d, O:%d, Out:%d", p->setpoint, p->measured, pPid, iPid, dPid, oPid, output);
#endif

    }
  
	return output; 
}

/*
===============================================================================
    mean()

    Calculate mean value of a uint8_t list
===============================================================================
 */
uint8_t Th1Super::mean(list<uint8_t> * a_input)
{
	uint32_t res = 0;
	list<uint8_t>::iterator it;
	
	for ( it = a_input->begin() ; it != a_input->end(); it++ )
	{
		res += *it;
	}
    res += a_input->size()/2; // to make integer division rounded
	res /= a_input->size();

	return (uint8_t)res;
}

/*
===============================================================================
    mean()

    Calculate mean value of a uint16_t list
===============================================================================
 */
uint16_t Th1Super::mean(list<uint16_t> * a_input)
{
	uint32_t res = 0;
	list<uint16_t>::iterator it;
	
	for ( it = a_input->begin() ; it != a_input->end(); it++ )
	{
		res += *it;
	}
    res += a_input->size()/2; // to make integer division rounded
	res /= a_input->size();

	return (uint16_t)res;
}

/*
===============================================================================
    min()

    Get minimum value of a uint16_t list
===============================================================================
 */
uint16_t Th1Super::min(list<uint16_t> a_input)
{
    uint16_t ret = 0;
    
	//Copy the list
    list<uint16_t> input = a_input;
    
    //Sort
    input.sort();
    
    //Get first element
    ret = input.front();

	return ret;
}

/*
===============================================================================
    min()

    Get minimum value of a uint8_t list
===============================================================================
 */
uint8_t Th1Super::min(list<uint8_t> a_input)
{
    uint8_t ret = 0;
    
	//Copy the list
    list<uint8_t> input = a_input;
    
    //Sort
    input.sort();
    
    //Get first element
    ret = input.front();

	return ret;
}


/*
===============================================================================
    max()

    Get maximum value of a uint16_t list
===============================================================================
 */
uint16_t Th1Super::max(list<uint16_t> a_input)
{
    uint16_t ret = 0;
    
	//Copy the list
    list<uint16_t> input = a_input;
    
    //Sort
    input.sort();
    
    //Get last element
    ret = input.back();

	return ret;
}

/*
===============================================================================
    max()

    Get maximum value of a uint8_t list
===============================================================================
 */
uint8_t Th1Super::max(list<uint8_t> a_input)
{
    uint8_t ret = 0;
    
	//Copy the list
    list<uint8_t> input = a_input;
    
    //Sort
    input.sort();
    
    //Get last element
    ret = input.back();

	return ret;
}

/*
===============================================================================
    addToList()

    Add item to a uint8_t list
===============================================================================
 */
void Th1Super::addToList(list<uint8_t> * a_list, uint8_t a_value, uint8_t a_maxItems)
{
	if(a_list->size() >= a_maxItems)
	{
		a_list->pop_front();    //Remove the oldest
	}
	a_list->push_back(a_value);
}

/*
===============================================================================
    addToList()

    Add item to a uint16_t list
===============================================================================
 */
void Th1Super::addToList(list<uint16_t> * a_list, uint16_t a_value, uint8_t a_maxItems)
{
	if(a_list->size() >= a_maxItems)
	{
		a_list->pop_front();    //Remove the oldest
	}
	a_list->push_back(a_value);
}

/*
===============================================================================
    soundPitchControl()

    Do syncronisation of sound data rate between doppler sensor module and Moyo
    sound system.

    Clock for doppler sensor module and Moyo will by nature always drift 
    a little apart from each other. If nothing is done, we will get buffer
    over- or under run after a while. We will adjust DAC clock speed to
    adjust the "eating" of the sound buffer, so the filling of this is
    around 50%. This must be done carefully, else a pitch shift will be heard
    on the doppler sound.
===============================================================================
 */
void Th1Super::soundPitchControl(void)
{
    uint8_t fill;
    uint16_t rate;
    
#ifndef NDEBUG
    uint32_t probeErrors;
#endif
    
    if(m_PitchTimer)
    {
        m_PitchTimer--;
    }
    else
    {
        m_PitchTimer = PITCH_TIME;
        
        fill = HwAbsl::getpHw()->getProbeSerial()->GetSoundDataFill();
        rate = HwAbsl::getpHw()->getProbeSerial()->GetSoundRate();
        
#ifndef NDEBUG
        probeErrors = HwAbsl::getpHw()->getProbeSerial()->GetTransducerErrCountTot();
#endif
        
        addToList(&sndSampleRates, rate, SND_SMP_RATE_AVG_BUF_LEN);
        addToList(&sndBufferFillLevels, fill, SND_BUF_FILL_AVG_BUF_LEN);
        
        if(m_PitchRegulateTimer)
        {
            m_PitchRegulateTimer--;
        }
        else
        {
            //Only regulate on interval
            
            m_PitchRegulateTimer = PITCH_REG_TIME;
                
            //Smooth rate
            rate = FHRMAudio::AUDIO_SOURCE_SAMPLE_FREQ;//mean(&sndSampleRates);
            //Smooth fill
            fill = mean(&sndBufferFillLevels);

            if(m_probeSoundDebugOut)
            {
                DEBUG_TRACE(", Rate: %u, Buffill: %u, Err: %u\r\n", rate, fill, probeErrors);
            }
            //DEBUG_TRACE("\r\nSoundRate:%u", rate);
            
            switch(m_soundPitchCtrl)
            {
                case eSPCInit:
                    //Startup
                    m_soundPitchCtrl = eSPCcalibrate;
                    break;
                case eSPCcalibrate:
                    //Wait for probe to send stable data 
                    if((max(sndSampleRates) - min(sndSampleRates)) < 100 && rate > 1000)
                    {
                        m_soundPitchCtrl = eSPCRun;
                        m_sndSampleRate = rate;
                    }
                    break;
                case eSPCRun:
                    {
                        //Running state
                        
                        //Do regulation of pitch to try to keep sound buffer filling at 50%
                        m_pPitchCtrl->measured = fill;
                        uint32_t p = DoPid(m_pPitchCtrl);
                        
                        
                        HwAbsl::getpHw()->getpDac()->setDacTimerPeriod(p);
                        
                    }
                    break;
                default:
                    m_soundPitchCtrl = eSPCInit;
                    break;
            }
            
            
        }
    }
}

//////////////////////////////////////////////////////////////////////////
// Thread1 handler thread
//
// When used with an RTOS, this function will not return, but wait for
// a signal, timeschedule, or will be preempted when a process with
// higher priority is ready to run. Without RTOS this function returns
// and an IPC signal struct is passed as the argument when called.
// NULL indicates time schedule.
//

/*
===============================================================================
    Supervisor handler thread

    When used with an RTOS, this function will not return, but wait for
    a signal, timeschedule, or will be preempted when a process with
    higher priority is ready to run. Without RTOS this function returns
    and an IPC signal struct is passed as the argument when called.
    NULL indicates time schedule.
===============================================================================
 */
void Th1Super::run(void *pmsg)
{
    
	//Startup routines here
  
    //Test
    /*
    while(1)                //Running with RTOS
    {
        IPC ipc;
        sigWait(THREAD_TICK_TIME, &ipc);  //Wait for a signal or OS schedule
        m_SUPThreadTestCtr++;
    }
    //
    */
    
    //waitUntilAllThreadsStarted();
    DEBUG_TRACE("\r\n%s Pri: %d", getThreadName(), getPriority() );
    

    /*
        TODO: STM32L5 can't detect this?
    if(HwAbsl::getpHw()->getpWatchdog()->watchdogResetDetected())
    {
        CONSOLE_PRINT("\r\nWATCHDOG RESET DETECTED !\r\n");
        systemShutdown(eWatchdogTimeout);
    }
    */
    
        
    m_pAdcTh = reinterpret_cast<ADThread*>(getpThread(P_ADC));
    
    //The config data in Emulated EEprom
    //Used in several classes, but created here
    //m_pConfig = new Config();
    
    //Create the event handler here. Used by several classes
    //m_pEvents = new Events();
    
    m_PitchRegulateTimer = PITCH_REG_TIME;
    m_PitchTimer = PITCH_TIME;
    
    if(HwAbsl::getpHw()->getStartupMode() == HwAbsl::eStartupModeNormal)
    {
    
        HwAbsl::getpHw()->enableSDpower(true);
            
        HwAbsl::getpHw()->transducerPower(true);
    
        sleep(500);
        
        HwAbsl::getpHw()->enable5V(true);
    
        sleep(500);
    
        //Power on self test for fuel gauge, 5V and RTC
        post();
    
        HwAbsl::getpHw()->getProbeSerial()->SendGetModuleVersionCmd();
    
        sleep(1000);  //Give module time to respond
        
        HwAbsl::getpHw()->getpWatchdog()->kick();
    
        std::string moduleVersionInfo = HwAbsl::getpHw()->getProbeSerial()->GetModuleVersionInfo();
    
        DEBUG_TRACE("\r\nModule version info: %s\r\n", moduleVersionInfo.c_str());
        
        HwAbsl::getpHw()->audioOn();
    
        HwAbsl::getpHw()->setImpedanceFreq(true);
        
        //Check if clock is running
        if(HwAbsl::getpHw()->getpRtc()->RTCgetError())
        {
            LL_RTC_DateTypeDef d;
            LL_RTC_TimeTypeDef t;
            t = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
            d = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
            
            if(d.Day == 1 && d.Month == 1 && t.Hours == 0 && t.Minutes == 0 && t.Seconds == 0)
            {
                //Seems like there have been a dead battery and clock is lost. Start clock.
                HwAbsl::getpHw()->getpRtc()->RTCsetDate(d);
                HwAbsl::getpHw()->getpRtc()->RTCsetTime(t);
            }        
        }
    
        m_operationalMode = eOMnormal;
    
    
    }
    else if(HwAbsl::getpHw()->getStartupMode() == HwAbsl::eStartupModeUSBpowered)
    {
        HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PL_500mA_noCharge);
        
        //Turn on power to SD card
        HwAbsl::getpHw()->enableSDpower(true);
                    
        m_operationalMode = eOMDataTransfer;
    }
    else if(HwAbsl::getpHw()->getStartupMode() == HwAbsl::eStartupModeUSBvirtCOM)
    {
        HwAbsl::getpHw()->initVirtualCom();
        
        HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PM_100mA_noCharge);
        
        m_operationalMode = eOMconfig;
    }
    
    //Test
    HwAbsl::getpHw()->getpDac()->ProbeSoundEnable(true);
    //
    
    if(Config::getpConfig()->IsValid())
    {
        uint16_t val = 0;
        
        //Get last battery remaining capacity from config and log it
        val = Config::getpConfig()->GetConfig(Config::eCfgLastBattRemCharge);
        Events::getpEvents()->AddEvent(Events::evtPowerOnBatteryState, 
                                       Events::TO_EPISODE_LOG,
                                       (float)val);
        if(val <= 100)
        {
            m_batteryRemainingCapacity = val;
        }
        
        //Get end of episode timeout value from config
        m_endOfEpisodeTime = Config::getpConfig()->GetConfig(Config::eCfgEndOfEpisodeTimeout);
        m_endOfEpisodeTime = MIN(CFG_MAX_END_OF_EPISODE_TIME, m_endOfEpisodeTime); //Limit
        m_endOfEpisodeTime = MAX(CFG_MIN_END_OF_EPISODE_TIME, m_endOfEpisodeTime); //Limit
        m_endOfEpisodeTime = m_endOfEpisodeTime * (1000/THREAD_TICK_TIME);
        m_episodeTimers.endOfEpisodeTimer = m_endOfEpisodeTime;
        
        //Get episode valid time value from config
        m_validEpisodeTime = Config::getpConfig()->GetConfig(Config::eCfgEpisodevalidTime);
        m_validEpisodeTime = MIN(CFG_MAX_VALID_EP_TIME, m_validEpisodeTime); //Limit
        m_validEpisodeTime = MAX(CFG_MIN_VALID_EP_TIME, m_validEpisodeTime); //Limit
        m_validEpisodeTime = m_validEpisodeTime * (1000/THREAD_TICK_TIME);
        m_episodeTimers.validEpisodeTimer = m_validEpisodeTime;
        
        //Get episode too short value from config
        m_episodeTooShortTime = Config::getpConfig()->GetConfig(Config::eCfgEpisodeTooShortTime);
        m_episodeTooShortTime = MIN(CFG_MAX_EP_TOO_SHORT_TIME, m_episodeTooShortTime); //Limit
        m_episodeTooShortTime = MAX(CFG_MIN_EP_TOO_SHORT_TIME, m_episodeTooShortTime); //Limit
        m_episodeTooShortTime = m_episodeTooShortTime * (1000/THREAD_TICK_TIME);
        m_episodeTimers.episodeTooShortTimer = m_episodeTooShortTime;
        
        //Get lost signal alarm time value from config
        m_FhrLostSignalAlarmTime = Config::getpConfig()->GetConfig(Config::eCfgLostSignalAlarmTime);
        m_FhrLostSignalAlarmTime = MIN(CFG_MAX_EP_TOO_SHORT_TIME, m_FhrLostSignalAlarmTime); //Limit
        m_FhrLostSignalAlarmTime = MAX(CFG_MIN_EP_TOO_SHORT_TIME, m_FhrLostSignalAlarmTime); //Limit
        m_FhrLostSignalAlarmTime = m_FhrLostSignalAlarmTime * (1000/THREAD_TICK_TIME);
        
        //Get FHR abnormal alarm limit low value from config
        m_fhrAbnormalAlarmLimitLow = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmLow);
        m_fhrAbnormalAlarmLimitLow = MIN(CFG_MAX_ABNORMAL_FHR_ALARM_LOW, m_fhrAbnormalAlarmLimitLow); //Limit
        m_fhrAbnormalAlarmLimitLow = MAX(CFG_MIN_ABNORMAL_FHR_ALARM_LOW, m_fhrAbnormalAlarmLimitLow); //Limit
        
        //Get FHR abnormal alarm limit high value from config
        m_fhrAbnormalAlarmLimitHigh = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmHigh);
        m_fhrAbnormalAlarmLimitHigh = MIN(CFG_MAX_ABNORMAL_FHR_ALARM_HIGH, m_fhrAbnormalAlarmLimitHigh); //Limit
        m_fhrAbnormalAlarmLimitHigh = MAX(CFG_MIN_ABNORMAL_FHR_ALARM_HIGH, m_fhrAbnormalAlarmLimitHigh); //Limit
        
        //Get FHR atypical alarm limit low value from config
        m_fhrAtypicalAlarmLimitLow = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmLow);
        m_fhrAtypicalAlarmLimitLow = MIN(CFG_MAX_ATYPICAL_FHR_ALARM_LOW, m_fhrAtypicalAlarmLimitLow); //Limit
        m_fhrAtypicalAlarmLimitLow = MAX(CFG_MIN_ATYPICAL_FHR_ALARM_LOW, m_fhrAtypicalAlarmLimitLow); //Limit
        
        //Get FHR atypical alarm limit high value from config
        m_fhrAtypicalAlarmLimitHigh = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmHigh);
        m_fhrAtypicalAlarmLimitHigh = MIN(CFG_MAX_ATYPICAL_FHR_ALARM_HIGH, m_fhrAtypicalAlarmLimitHigh); //Limit
        m_fhrAtypicalAlarmLimitHigh = MAX(CFG_MIN_ATYPICAL_FHR_ALARM_HIGH, m_fhrAtypicalAlarmLimitHigh); //Limit
        
        //Get FHR atypical alarm time from config
        m_fhrAtypicalAlarmTime = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmTime);
        m_fhrAtypicalAlarmTime = MIN(CFG_MAX_ATYPICAL_FHR_ALARM_TIME, m_fhrAtypicalAlarmTime); //Limit
        m_fhrAtypicalAlarmTime = MAX(CFG_MIN_ATYPICAL_FHR_ALARM_TIME, m_fhrAtypicalAlarmTime); //Limit
        m_fhrAtypicalAlarmTime = m_fhrAtypicalAlarmTime * (1000/THREAD_TICK_TIME);
        
        //Get FHR abnormal alarm time from config
        m_fhrAbnormalAlarmTime = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmTime);
        m_fhrAbnormalAlarmTime = MIN(CFG_MAX_ABNORMAL_FHR_ALARM_TIME, m_fhrAbnormalAlarmTime); //Limit
        m_fhrAbnormalAlarmTime = MAX(CFG_MIN_ABNORMAL_FHR_ALARM_TIME, m_fhrAbnormalAlarmTime); //Limit
        m_fhrAbnormalAlarmTime = m_fhrAbnormalAlarmTime * (1000/THREAD_TICK_TIME);
        
        //Get doppler speaker gain value from config
        val = Config::getpConfig()->GetConfig(Config::eCfgDopplerSpeakerGain);
        val = MIN(CFG_MAX_DOPPLER_SOUND_SPEAKER_GAIN, val); //Limit
        val = MAX(CFG_MIN_DOPPLER_SOUND_SPEAKER_GAIN, val); //Limit
        HwAbsl::getpHw()->getProbeSerial()->SetDopplerSoundSpeakerGain((uint8_t)val);
        
        //Get doppler sound gain value from config
        val = Config::getpConfig()->GetConfig(Config::eCfgDopplerSoundGain);
        val = MIN(CFG_MAX_DOPPLER_SOUND_GAIN, val); //Limit
        
#if (CFG_MIN_DOPPLER_SOUND_GAIN > 0)
        val = MAX(CFG_MIN_DOPPLER_SOUND_GAIN, val); //Limit
#endif
        
        HwAbsl::getpHw()->getProbeSerial()->SendSetDopplerSoundSignalGainCmd((FHRMps::DopplerSoundSignalGain_t)val);
        
        //Get whether enable of disable FHR filter value from config
        val = Config::getpConfig()->GetConfig(Config::eCfgUseFHRfilter) == CFG_DISABLED ? false : true;
        
        HwAbsl::getpHw()->getProbeSerial()->EnableFHRfilter(val);
        
        
    }
    
    sigDispatch(P_UI, S_BATTERY_REMAINING_CAPACITY, (uint32_t)m_batteryRemainingCapacity, 0, 0);
    
    //Check if test pin is active
    if(HwAbsl::getpHw()->isTestmodeEnabled())
    {
        m_testpinActive = true;
        m_operationalMode = eOMtestMode;
        HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PL_500mA_noCharge);
    }
    
    if(m_operationalMode == eOMnormal)
    {
        //Create episode files
        sigDispatch(P_FILE, S_FILE_CREATE_EPISODE);
    }
    
    m_FhrFileNextSampleTime = OS_GetTime();
    
    HwAbsl::getpHw()->getpWatchdog()->kick();
    
    while(1)                //Running with RTOS
    {
        IPC ipc;
        sigWait(THREAD_TICK_TIME, &ipc);  //Wait for a signal or OS schedule

        m_SUPThreadTestCtr++;
        
		switch(ipc.m_sig)
        {
			case S_OSTIMEOUT:
                {
                    //Normal time schedule
               
                    watchdogKickHandler();
                    
#ifndef NO_FUEL_GAUGE
                    handleFuelGauge();
#endif
                    
                    if(m_operationalMode == eOMnormal)
                    {
                        uint8_t fetalHR = HwAbsl::getpHw()->getProbeSerial()->GetFetalHeartRate();
                        uint8_t FHRquality = HwAbsl::getpHw()->getProbeSerial()->GetSignalQuality();
                        int16_t accEnergy = HwAbsl::getpHw()->getProbeSerial()->GetAccEnergy();
                        
                        EpisodeHandler(fetalHR, m_maternalHR, accEnergy);
                        soundPitchControl();
                        
#ifndef NO_PROBE
                        ultraSndProbeCommHandler();
#endif
                        
                        audioAlarmHandler();
                        
                        
                        FhrLostSignalAlarmHandler(fetalHR, FHRquality);
                        FhrAlarmHandler(fetalHR, FHRquality);
                        batteryAlarmHandler(m_batteryRemainingCapacity);
                        
#ifdef DEBUG_ALARM_HANDLER
                        fhrquality = FHRquality;
#endif
                        
                        
                        uint32_t time = OS_GetTime();
                        if(time >= m_FhrFileNextSampleTime)
                        {
                            sampleFhrData(time);
                            m_FhrFileNextSampleTime += FHR_SAMPLE_TIME;
                        }
                        
                        if(HwAbsl::getpHw()->isUSBpowered())
                        {
                            //Someone connected cable. Wait and see if this situation remains
                            sleep(1000);
                            if(HwAbsl::getpHw()->isUSBpowered())
                            {
                                //Yes. We have to change mode
                                DEBUG_TRACE("\r\nUSB connected while in normal operation\r\n");
                                
                                m_operationalMode = eOMDataTransfer;
                                
                                //Update HW abst layer
                                HwAbsl::getpHw()->setStartupMode(HwAbsl::eStartupModeUSBpowered);
                                
                                //Turn off HW
                                HwAbsl::getpHw()->getProbeSerial()->ProbeEnable(false);
                                HwAbsl::getpHw()->audioOff();
                                HwAbsl::getpHw()->setImpedanceFreq(false);
                                HwAbsl::getpHw()->getpDac()->ProbeSoundEnable(false);
    
                                //Send message to update threads
                                sigDispatch(P_UI, S_USB_POWERED, 1, 0 ,0);
                                sigDispatch(P_FILE, S_USB_POWERED, 1, 0 ,0);
                            }
                        }
                    }
                    else if(m_operationalMode == eOMtestMode)
                    {
                        UItestHandler();
                    }
                    else if(m_operationalMode == eOMcriticalError)
                    {
                        audioAlarmHandler();
                    }
                    
                    
                    
                    if(HwAbsl::getpHw()->getStartupMode() == HwAbsl::eStartupModeUSBpowered ||
                       HwAbsl::getpHw()->getStartupMode() == HwAbsl::eStartupModeUSBvirtCOM)
                    {
                        //Power good sometimes has short spikes. Filter out so
                        //we don't get false detections
                            
                        if(HwAbsl::getpHw()->isUSBpowered())
                        {
                            
                            //Power good. Reset timer
                            m_powerGoodTimer = POWER_GOOD_FILTER_TIME;
                            
                        }
                        else
                        {
                            if(m_powerGoodTimer == 1) //Once
                            {
                                
                                //USB disconnected. Turn off power
                                sigDispatch(P_UI, S_SYSTEM_SHUTDOWN, 1, 0 ,0);
                                systemShutdown(eSDRusbDisConn);
                                m_powerGoodTimer = 0;
                            }
                            else if(m_powerGoodTimer)
                            {
                                m_powerGoodTimer--;
                            }
                            
                        }
                    }
                }
                break;
        
            case S_ALARM_ACKNOWLEGDE:
                if(ipc.m_data1 == 1)
                {
                    //Get the alarm acknowlegded
                    
                    Alarm_t alarmAcknowlegde = getPrioritisedAlarm(m_audioAlarm);
                    m_alarmAcknowlegde.alarmByte |= alarmAcknowlegde.alarmByte;
                }
                
                //Reset lost signal alarm handler if in abnormal FHR alarm (SRS-1873/SRS1874)
                if(m_displayAlarm.alarmBits.abnormalFHRalarm && 
                   m_alarmAcknowlegde.alarmBits.abnormalFHRalarm)
                {
                    m_fhrLostSignalAlarmState = eFASnoAlarm;
                }
                break;
            
            case S_BATTERY_REMAINING_CAPACITY:
                if(m_operationalMode == eOMnormal)
                {
                    //Mask increasing values in operating mode. m_batteryRemainingCapacity was
                    //initially read from eeprom and low battery conditions was handled at startup.
                    //We don't want to indicate higher battery level at a later time.
                    
                    if(ipc.m_data1 < m_batteryRemainingCapacity)
                    {
                        m_batteryRemainingCapacity = ipc.m_data1;
                    }
                }
                else
                {
                    m_batteryRemainingCapacity = ipc.m_data1;
                }
                
                //Limit
                if(m_batteryRemainingCapacity > 100)
                {
                    m_batteryRemainingCapacity = 100;
                }
                break;
    
            case S_BATTERY_DEBUG_OUT:
                m_batteryDebugOut = ipc.m_data1 == 1 ? true : false;
                break;
               
            case S_SYSTEM_SHUTDOWN:
                systemShutdown((ShutdownReason_t)ipc.m_data1);
                break;
            
            case S_PROBE_SOUND_DEBUG:
                if(ipc.m_data1 == 1)
                {
                    m_probeSoundDebugOut = true;
                }
                else
                {
                    m_probeSoundDebugOut = false;
                }
                break;
            
            case S_FILE_EPISODE_CREATED:
                if(m_operationalMode == eOMnormal)
                {
                    //Episode files are created. Now we are ready to start
                    sigDispatch(P_FILE, S_FILE_START_EPISODE, OS_GetTime(), 0, 0);
                }
                break;
            
            case S_HR_MATERNAL_UPDATE:
                if(ipc.m_data1 < 255)
                {
                    m_maternalHR = (uint8_t)ipc.m_data1;
                }
                break;
                
            case S_END_OF_EPISODE_TIME_UPDATE:
                m_endOfEpisodeTime = ipc.m_data1;
                m_endOfEpisodeTime = MIN(CFG_MAX_END_OF_EPISODE_TIME, m_endOfEpisodeTime); //Limit
                m_endOfEpisodeTime = MAX(CFG_MIN_END_OF_EPISODE_TIME, m_endOfEpisodeTime); //Limit
                m_endOfEpisodeTime = m_endOfEpisodeTime * (1000/THREAD_TICK_TIME);
                m_episodeTimers.endOfEpisodeTimer = m_endOfEpisodeTime;
                break;
                
            case S_EPISODE_VALID_TIME_UPDATE:
                m_validEpisodeTime = ipc.m_data1;
                m_validEpisodeTime = MIN(CFG_MAX_VALID_EP_TIME, m_validEpisodeTime); //Limit
                m_validEpisodeTime = MAX(CFG_MIN_VALID_EP_TIME, m_validEpisodeTime); //Limit
                m_validEpisodeTime = m_validEpisodeTime * (1000/THREAD_TICK_TIME);
                m_episodeTimers.validEpisodeTimer = m_validEpisodeTime;
                break;
            
            case S_EP_TOO_SHORT_TIME_UPDATE:
                m_episodeTooShortTime = ipc.m_data1;
                m_episodeTooShortTime = MIN(CFG_MAX_EP_TOO_SHORT_TIME, m_episodeTooShortTime); //Limit
                m_episodeTooShortTime = MAX(CFG_MIN_EP_TOO_SHORT_TIME, m_episodeTooShortTime); //Limit
                m_episodeTooShortTime = m_episodeTooShortTime * (1000/THREAD_TICK_TIME);
                m_episodeTimers.episodeTooShortTimer = m_episodeTooShortTime;
                break;
            case S_LOST_SIG_AL_TIME_UPDATE:
                m_FhrLostSignalAlarmTime = ipc.m_data1;
                m_FhrLostSignalAlarmTime = MIN(CFG_MAX_LOST_SIGNAL_ALARM_TIME, m_FhrLostSignalAlarmTime); //Limit
                m_FhrLostSignalAlarmTime = MAX(CFG_MIN_LOST_SIGNAL_ALARM_TIME, m_FhrLostSignalAlarmTime); //Limit
                break;
            case S_FHR_ALARM_ABNORMAL_HIGH_UPDATE:
                m_fhrAbnormalAlarmLimitHigh = ipc.m_data1;
                m_fhrAbnormalAlarmLimitHigh = MIN(CFG_MAX_ABNORMAL_FHR_ALARM_HIGH, m_fhrAbnormalAlarmLimitHigh); //Limit
                m_fhrAbnormalAlarmLimitHigh = MAX(CFG_MIN_ABNORMAL_FHR_ALARM_HIGH, m_fhrAbnormalAlarmLimitHigh); //Limit
                break;
            case S_FHR_ALARM_ABNORMAL_LOW_UPDATE:
                m_fhrAbnormalAlarmLimitLow = ipc.m_data1;
                m_fhrAbnormalAlarmLimitLow = MIN(CFG_MAX_ABNORMAL_FHR_ALARM_LOW, m_fhrAbnormalAlarmLimitLow); //Limit
                m_fhrAbnormalAlarmLimitLow = MAX(CFG_MIN_ABNORMAL_FHR_ALARM_LOW, m_fhrAbnormalAlarmLimitLow); //Limit
                break;
            case S_FHR_ALARM_ATYPICAL_TIME_UPDATE:
                m_fhrAtypicalAlarmTime = ipc.m_data1;
                m_fhrAtypicalAlarmTime = MIN(CFG_MAX_ATYPICAL_FHR_ALARM_TIME, m_fhrAtypicalAlarmTime); //Limit
                m_fhrAtypicalAlarmTime = MAX(CFG_MIN_ATYPICAL_FHR_ALARM_TIME, m_fhrAtypicalAlarmTime); //Limit
                m_fhrAtypicalAlarmTime = m_fhrAtypicalAlarmTime * (1000/THREAD_TICK_TIME);
                break;
            case S_FHR_ALARM_ABNORMAL_TIME_UPDATE:
                m_fhrAbnormalAlarmTime = ipc.m_data1;
                m_fhrAbnormalAlarmTime = MIN(CFG_MAX_ABNORMAL_FHR_ALARM_TIME, m_fhrAbnormalAlarmTime); //Limit
                m_fhrAbnormalAlarmTime = MAX(CFG_MIN_ABNORMAL_FHR_ALARM_TIME, m_fhrAbnormalAlarmTime); //Limit
                m_fhrAbnormalAlarmTime = m_fhrAtypicalAlarmTime * (1000/THREAD_TICK_TIME);
                break;
            case S_FHR_ATYPICAL_ALARM_HIGH_UPDATE:
                m_fhrAtypicalAlarmLimitHigh = ipc.m_data1;
                m_fhrAtypicalAlarmLimitHigh = MIN(CFG_MAX_ATYPICAL_FHR_ALARM_HIGH, m_fhrAtypicalAlarmLimitHigh); //Limit
                m_fhrAtypicalAlarmLimitHigh = MAX(CFG_MIN_ATYPICAL_FHR_ALARM_HIGH, m_fhrAtypicalAlarmLimitHigh); //Limit
                break;
            case S_FHR_ATYPICAL_ALARM_LOW_UPDATE:
                m_fhrAtypicalAlarmLimitLow = ipc.m_data1;
                m_fhrAtypicalAlarmLimitLow = MIN(CFG_MAX_ATYPICAL_FHR_ALARM_LOW, m_fhrAtypicalAlarmLimitLow); //Limit
                m_fhrAtypicalAlarmLimitLow = MAX(CFG_MIN_ATYPICAL_FHR_ALARM_LOW, m_fhrAtypicalAlarmLimitLow); //Limit
                break;
            case S_ENABLE_ALARM_SOUND:
                m_alarmSoundEnabled = ipc.m_data1 == 1 ? true : false;
                break;
            case S_SET_OPERATIONAL_MODE:
                switch((OperationalMode_t)ipc.m_data1)
                {
                    case eOMnormal:
                    case eOMDataTransfer:
                        break;
                    case eOMbatteryCharge:
                        //Could be a charger connected
                        //Lets try enable 1A charging and see if USB power disappears
                        //Dirty method, but it is the only one we have
                        
                            enableMaxBatteryCharge();
                        break;
                    case eOMbatteryEmpty:
                    case eOMcriticalError:
                        break;
                    case eOMconfig:
                        /*
                        if(HwAbsl::getpHw()->getpSerialConsole())
                        {
                            if(HwAbsl::getpHw()->getpSerialConsole()->isUSBHostConnectionOK())
                            {
                                //We have permission from PC to use 500mA. Turn on power to devices before POST
                                HwAbsl::getpHw()->enableSDpower(true);
                                HwAbsl::getpHw()->transducerPower(true);
                                sleep(500);
                                HwAbsl::getpHw()->enable5V(true);
                                
                                post();
                                
                                //Ultrasound probe POST
                                HwAbsl::getpHw()->getProbeSerial()->Post();
                                
                                //Turn on charger
                                HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PL_500mA_Charge);
                            }
                            
                        }
                        */
                        break;
                    case eOMshutdown:
                        m_operationalMode = (OperationalMode_t)ipc.m_data1;
                        break;
                    case eOMtestMode:
                        {
                            m_operationalMode = (OperationalMode_t)ipc.m_data1;
                            HwAbsl::getpHw()->enableSDpower(true);
                            HwAbsl::getpHw()->transducerPower(true);
                            sleep(500);
                            HwAbsl::getpHw()->enable5V(true);
                            HwAbsl::getpHw()->FHRledBlinkStop();
                            
                            //Get doppler sound gain value from config
                            uint16_t val;
                            val = Config::getpConfig()->GetConfig(Config::eCfgDopplerSoundGain);
                            val = MIN(CFG_MAX_DOPPLER_SOUND_GAIN, val); //Limit
        
#if (CFG_MIN_DOPPLER_SOUND_GAIN > 0)
                            val = MAX(CFG_MIN_DOPPLER_SOUND_GAIN, val); //Limit
#endif
        
                            HwAbsl::getpHw()->getProbeSerial()->SendSetDopplerSoundSignalGainCmd((FHRMps::DopplerSoundSignalGain_t)val);
                        }
                        break;
                    default:
                        break;
                }
                break;
            case S_TEST_COMMAND_POST:
                if(m_operationalMode == eOMtestMode) //Only allow POST from TEST_POST command if in test mode
                {
                    post();
                }
                break;
            case S_TEST_COMMAND_STATUS_LED:    
                if(m_operationalMode == eOMtestMode) //Only allow POST from TEST_POST command if in test mode
                {
                    m_UItestType = eUITTled;
                    initStatusLEDtest((StatusLEDtestCommand_t) ipc.m_data1);
                }
                break;
            case S_TEST_COMMAND_KEY_TEST:
                if(m_operationalMode == eOMtestMode) //Only allow POST from TEST_POST command if in test mode
                {
                    m_keyPressTestTimer = KEY_PRESS_TEST_TIMEOUT;
                    
                    m_UItestType = eUITTkey;
                    
                    initKeyTest();
                }
                break;
            case S_TEST_COMMAND_DISPLAY:
                if(m_operationalMode == eOMtestMode) //Only allow POST from TEST_POST command if in test mode
                {
                    m_UItestType = eUITTdisplay;
                    m_keyPressTestTimer = KEY_PRESS_TEST_TIMEOUT;
                    initKeyTest();
                    sigDispatch(P_UI, S_TEST_COMMAND_DISPLAY, 1, 0 ,0);
                }
                break;
                
            case S_TEST_COMMAND_SOUND:
                if(m_operationalMode == eOMtestMode) //Only allow POST from TEST_POST command if in test mode
                {
                    if((SoundTestCommand_t)ipc.m_data1 != eSTCStop)
                    {
                        m_UItestType = eUITTsound;
                        m_keyPressTestTimer = KEY_PRESS_TEST_TIMEOUT;
                        initKeyTest();
                    }
                    
                    setTestSound((SoundTestCommand_t)ipc.m_data1);
                }
                
                break;
            
            case S_INIT_FUELGAUGE:
                flashFuelgauge();
                break;
                
            /*    
            case S_USB_STATE:
                if(ipc.m_data1 == (uint32_t)UNCONNECTED)
                {
                    //Could be a charger connected
                    //Lets try enable 1A charging and see if USB power disappears
                    //Dirty method, but it is the only one we have
                    
                    enableMaxBatteryCharge();
                }
                else if(ipc.m_data1 == (uint32_t)CONFIGURED)
                {
                    //We got permission from PC to use 500mA. Enable it.
                    if(!m_testpinActive)
                    {
                        DEBUG_TRACE("\r\nUSB current set to 500mA. Battery charge enabled\r\n");
                        HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PL_500mA_Charge);
                    }
                    else
                    {
                        DEBUG_TRACE("\r\nUSB current set to 500mA. Battery charge disabled (in test mode)\r\n");
                        HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PL_500mA_noCharge);
                    }
                    
                    //Wait and see if power is still good
                    sleep(500);
                    if(!HwAbsl::getpHw()->isUSBpowered())
                    {
                        //USB power disappeard. Go back to 100mA
                        DEBUG_TRACE("\r\nUSB overcurrent when pulling 500mA! USB current set to 100mA\r\n");
                        HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PM_100mA_noCharge);
                        sleep(2000);
                    }
                }
                else if(ipc.m_data1 == (uint32_t)SUSPENDED)
                {
                    //Check if mass storage timed out before setting charge current to 100mA
                    //USB driver will issue SUSPENDED if a wall charger is connected.
                    //If timed out before SUSPENDED is issued, se assume 
                    //a wall charger is connected and take no action (charge current should already be 1A)
                    if(m_massStorageTimeout)
                    {
                        enableMaxBatteryCharge();
                        
                    }
                    else
                    {
                        DEBUG_TRACE("\r\nUSB current set to 100mA. (USB suspended)\r\n");
                        HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PM_100mA_noCharge);
                    }
                }
                DEBUG_TRACE("\r\nUSB state: %u\r\n", ipc.m_data1);
                break;
                
            case S_MASS_STORAGE_ENABLED:
                if(ipc.m_data1 == 0)
                {
                    m_massStorageTimeout = true;
                    enableMaxBatteryCharge();
                        
                }
                else
                {
                    m_massStorageTimeout = false;
                }
                break;
            */  
            default:
				//Handle unrecognized signals
				sigUnknown(&ipc);
                break;
        }
    }
}

/*
===============================================================================
    ultraSndProbeCommHandler()

    Monitor communication with doppler sensor. Try to power cycle
    if lost communication. If communication not regained, initate alarm
    and change of mode
===============================================================================
 */
void Th1Super::ultraSndProbeCommHandler(void)
{
    if(HwAbsl::getpHw()->getStartupMode() == HwAbsl::eStartupModeNormal)
    {
        switch(m_ulsndTransComm)
        {
            case eUSTCcommunicating:
                if(HwAbsl::getpHw()->getProbeSerial()->IsTransducerTimedOut())
                {
                    DEBUG_TRACE("\r\nCritical error! Transducer timeout - trying reboot\r\n");
                    m_ulsndTransCommRetryTmr = ULSND_TRANS_POWER_OFF_WAIT_TIME;
                    HwAbsl::getpHw()->getProbeSerial()->ProbeEnable(false);
                    m_ulsndTransComm = eUSTCrebootPowerOff;
                    HwAbsl::getpHw()->getProbeSerial()->ResetTransducerTimeOut();
                }
                else if(HwAbsl::getpHw()->getProbeSerial()->GetTransducerErrCount() >= TRANSDUCER_MAX_ERR_CNT)
                {
                    DEBUG_TRACE("\r\nCritical error! Transducer err count exeeded - trying reboot\r\n");
                    m_ulsndTransCommRetryTmr = ULSND_TRANS_POWER_OFF_WAIT_TIME;
                    HwAbsl::getpHw()->getProbeSerial()->ProbeEnable(false);
                    m_ulsndTransComm = eUSTCrebootPowerOff;
                }
                break;
            case eUSTCrebootPowerOff:
                if(m_ulsndTransCommRetryTmr)
                {
                    m_ulsndTransCommRetryTmr--;
                }
                else
                {
                    HwAbsl::getpHw()->getProbeSerial()->ProbeEnable(true);
                    m_ulsndTransCommRetryTmr = ULSND_TRANS_RETRY_TIME;
                    m_ulsndTransComm = eUSTCrebootPowerOn;
                }
                break;
            case eUSTCrebootPowerOn:
                if(m_ulsndTransCommRetryTmr)
                {
                    m_ulsndTransCommRetryTmr--;
                }
                else
                {
                    if(HwAbsl::getpHw()->getProbeSerial()->IsTransducerTimedOut())
                    {
                        DEBUG_TRACE("\r\nCritical error! Transducer timeout\r\n");
                        
                        //Did not help to recycle power. Report alarm
                        Alarm_t alarm;
                        alarm.alarmByte = 0; //Reset
                        alarm.alarmBits.criticalErrorAlarm = true;
                        setDisplayAlarmPrioHandler(alarm);
                        setAudioAlarmPrioHandler(alarm);
                        
                        m_ulsndTransComm = eUSTClostComm;
                        
                        //If comm is lost and it does not help with reset, we assume cable is broken
                        Events::getpEvents()->AddEvent(Events::evtUltrasoundTransducerCableBroken, 
                                                       Events::TO_EPISODE_LOG | Events::TO_SYSTEM_LOG,
                                                       1.0);
                        
                        //Enable sound DMA, so we can play alarm
                        HwAbsl::getpHw()->getpDac()->ProbeSoundEnable(true);
                    }
                    else
                    {
                        //Regained communication
                        m_ulsndTransComm = eUSTCcommunicating;
                    }
                }
                break;
            
            case eUSTClostComm:
                //Do nothing
                break;
                
            default:
                break;
            
        }
        
    }
}

/*
===============================================================================
    FhrLostSignalAlarmHandler()

    Monitor fetal heart rate and fetal heart rate quality from doppler sensor
    for lost signal.

    Report alarm if condition(s) met

===============================================================================
*/
void Th1Super::FhrLostSignalAlarmHandler(uint16_t a_fhr, uint8_t a_fhrQuality)
{

#ifdef DEBUG_ALARM_HANDLER
    lostSignalAlarmState = m_fhrLostSignalAlarmState;
    fhrLostSignalAlarmTimer = m_fhrLostSignalAlarmTimer;
    fhrLostSignalFilterTimer = m_fhrLostSignalFilterTimer;
#endif

    switch(m_fhrLostSignalAlarmState)
    {
        case eFASInit:
            if(a_fhrQuality > 0x02) //Good
            {
                //Probe most probably attached. Check if it is noise
                m_fhrLostSignalAlarmState = eFASArm;
                m_fhrLostSignalAlarmTimer = FHR_ALARM_INIT_TIME;
            }
            
            break;
        case eFASArm:
            if(a_fhrQuality <= 0x02) //Not good
            {
                //Most probably noise. Wait enable alarm
                m_fhrLostSignalAlarmTimer = FHR_ALARM_INIT_TIME;
            }
            else
            {
                if(m_fhrLostSignalAlarmTimer)
                {
                    m_fhrLostSignalAlarmTimer--;
                }
                else
                {
                    //We have had a decent FHR for a while. Enable alarm
                    m_fhrLostSignalAlarmState = eFASnoAlarm;
                }
            }
            
            break;
        case eFASnoAlarm:
            if(a_fhr < FHR_LOST_SIGNAL_ALARM_LIMIT)
            {
                m_fhrLostSignalAlarmTimer = m_FhrLostSignalAlarmTime;
                m_fhrLostSignalAlarmState = eFASAlBelowThreshold;
                m_fhrLostSignalFilterTimer = FHR_LOST_SIGNAL_FILTER_TIME;
    
            }
            break;
        case eFASAlBelowThreshold:
            
            if(a_fhr >= FHR_LOST_SIGNAL_ALARM_LIMIT)
            {
                if(m_fhrLostSignalFilterTimer)
                {
                    //Ignore short spikes above threshold
                    m_fhrLostSignalFilterTimer--;
                }
                else
                {
                    //FHR was recovered before alarm was fired. Go back to normal state
                    m_fhrLostSignalAlarmState = eFASnoAlarm;
                }
                
            }
            else
            {
                m_fhrLostSignalFilterTimer = FHR_LOST_SIGNAL_FILTER_TIME;
                
                if(m_fhrLostSignalAlarmTimer)
                {
                    m_fhrLostSignalAlarmTimer--;
                }
                else
                {
                    //FHR below threshold for long time. Fire alarm
                    Alarm_t alarm;
                    alarm.alarmByte = 0;
                    alarm.alarmBits.lostSignalAlarm = true;
                    setDisplayAlarmPrioHandler(alarm);
                    setAudioAlarmPrioHandler(alarm);
                    m_fhrLostSignalAlarmState = eFASAlarm;
                }
            }
            break;
        case eFASAlarm:
            if(a_fhr >= FHR_LOST_SIGNAL_ALARM_LIMIT)
            {
                //FHR was recovered. Hold alarm for a while and see if it remains.
                m_fhrLostSignalAlarmTimer = FHR_ALARM_RECOVER_TIME;
                m_fhrLostSignalAlarmState = eFASFhrRecovered;
            }
            break;
        case eFASFhrRecovered:
            if(a_fhr >= FHR_LOST_SIGNAL_ALARM_LIMIT)
            {
                if(m_fhrLostSignalAlarmTimer)
                {
                    m_fhrLostSignalAlarmTimer--;
                }
                else
                {
                    //FHR recovered for a while. Turn off audio alarm
                    Alarm_t alarm;
                    alarm.alarmByte = 0;
                    alarm.alarmBits.lostSignalAlarm = true;
                    clearAudioAlarmPrioHandler(alarm);
                    clearDisplayAlarmPrioHandler(alarm);
                
                    m_fhrLostSignalAlarmState = eFASnoAlarm;
                    
                }
            }
            else
            {
                //Below threshold. Go back to alarm state
                m_fhrLostSignalAlarmState = eFASAlarm;
            }
            break;
        default:
            break;
    }
}

/*
===============================================================================
    FhrAlarmHandler()

    Monitor fetal heart rate and fetal heart rate quality from doppler sensor
    for abnormal and atypical heart rate.

    Report alarm if condition(s) met

===============================================================================
*/
void Th1Super::FhrAlarmHandler(uint16_t a_fhr, uint8_t a_fhrQuality)
{

#ifdef DEBUG_ALARM_HANDLER
    debugAlarmHandler();
#endif

    switch(m_fhrAlarmState)
    {
        case eFASInit:
            if(a_fhrQuality > 0x02) //Good
            {
                //Probe most probably attached. Check if it is noise
                m_fhrAlarmState = eFASArm;
                DEBUG_TRACE("m_fhrAlarmState = eFASArm\r\n");
            }
            break;
        case eFASArm:
            if(a_fhrQuality <= 0x02) //Not good
            {
                //Most probably noise. Wait enable alarm
                m_fhrAtypicalAlarmTimer = 0;
                m_fhrAbnormalAlarmTimer = 0;
                m_fhrAlarmRecoverTimer = 0;
            }
            else
            {
                if(m_fhrAlarmRecoverTimer < FHR_ALARM_INIT_TIME)
                {
                    m_fhrAlarmRecoverTimer++;
                }
                else
                {
                    //We have had good quality FHR for a while. Enable alarm
                    m_fhrAlarmState = eFASnoAlarm;
                    DEBUG_TRACE("m_fhrAlarmState = eFASnoAlarm\r\n");
                }
            }
            break;
        case eFASnoAlarm:
            if((a_fhr < m_fhrAtypicalAlarmLimitLow || 
                a_fhr > m_fhrAtypicalAlarmLimitHigh) &&
                a_fhr >= MIN_VALID_HR)
            {
                //Limit crossed
                m_fhrAtypicalAlarmTimer = 0;
                m_fhrAbnormalAlarmTimer = 0;
                m_fhrAlarmRecoverTimer = 0;
                m_fhrAlarmState = eFASAlBelowThreshold;
                DEBUG_TRACE("m_fhrAlarmState = eFASAlBelowThreshold\r\n");
            }
            break;
        case eFASAlBelowThreshold:
            if(a_fhr >= m_fhrAtypicalAlarmLimitLow && 
                a_fhr <= m_fhrAtypicalAlarmLimitHigh)
            {
                if(m_fhrAlarmRecoverTimer < FHR_ATYPICAL_ALARM_RECOVER_TIME)
                {
                    m_fhrAlarmRecoverTimer++;
                }
                else
                {
                    //FHR was recovered before alarm was fired. Go back to normal state
                    m_fhrAlarmState = eFASnoAlarm;
                    DEBUG_TRACE("m_fhrAlarmState = eFASnoAlarm\r\n");
                }
            }
            else if(a_fhr < MIN_VALID_HR)
            {
                if(m_fhrAlarmRecoverTimer < FHR_LOW_ALARM_RECOVER_TIME)
                {
                    m_fhrAlarmRecoverTimer++;
                }
                else
                {
                    //FHR was recovered before alarm was fired. Go back to normal state
                    m_fhrAlarmState = eFASnoAlarm;
                    m_fhrAtypicalAlarmTimer = 0;
                    m_fhrAbnormalAlarmTimer = 0;
                    m_fhrAlarmRecoverTimer = 0;
                    DEBUG_TRACE("m_fhrAlarmState = eFASnoAlarm\r\n");
                }
            }
            else if((a_fhr < m_fhrAbnormalAlarmLimitLow || 
                     a_fhr > m_fhrAbnormalAlarmLimitHigh) &&
                     a_fhr >= MIN_VALID_HR)
            {
                if(m_fhrAbnormalAlarmTimer < m_fhrAbnormalAlarmTime)
                {
                    m_fhrAbnormalAlarmTimer++;
                    m_fhrAtypicalAlarmTimer++;
                }
                else
                {
                    //FHR outside limits for long time. Fire alarm
                    Alarm_t alarm;
                    alarm.alarmByte = 0;
                    alarm.alarmBits.abnormalFHRalarm = true;
                    setDisplayAlarmPrioHandler(alarm);
                    setAudioAlarmPrioHandler(alarm);
                  
                    m_fhrAlarmState = eFASAlarm;
                    DEBUG_TRACE("m_fhrAlarmState = eFASAlarm (abnormal)\r\n");
                }
                
                m_fhrAlarmRecoverTimer = 0;
            }
            else if((a_fhr < m_fhrAtypicalAlarmLimitLow || 
                     a_fhr > m_fhrAtypicalAlarmLimitHigh) &&
                     a_fhr >= MIN_VALID_HR)
            {
                if(m_fhrAtypicalAlarmTimer < m_fhrAtypicalAlarmTime)
                {
                    m_fhrAtypicalAlarmTimer++;
                }
                else
                {
                    //FHR outside limits for long time. Fire alarm
                    Alarm_t alarm;
                    alarm.alarmByte = 0;
                    alarm.alarmBits.abnormalFHRalarm = true;
                    setDisplayAlarmPrioHandler(alarm);
                    setAudioAlarmPrioHandler(alarm);
                  
                    m_fhrAlarmState = eFASAlarm;
                    DEBUG_TRACE("m_fhrAlarmState = eFASAlarm (atypical)\r\n");
                }
                
                m_fhrAlarmRecoverTimer = 0;
            }
            break;
        case eFASAlarm:
            if(a_fhr >= m_fhrAtypicalAlarmLimitLow && a_fhr <= m_fhrAtypicalAlarmLimitHigh)
            {
                //FHR was recovered. Check to see if it remains
                
                //Hold audio alarm for a while
                m_fhrAlarmRecoverTimer = 0;
                m_fhrAlarmState = eFASFhrRecovered;
                DEBUG_TRACE("m_fhrAlarmState = eFASFhrRecovered\r\n");
            }
            break;
        case eFASFhrRecovered:
            if(a_fhr >= m_fhrAtypicalAlarmLimitLow && a_fhr <= m_fhrAtypicalAlarmLimitHigh)
            {
                if(m_fhrAlarmRecoverTimer < FHR_ALARM_RECOVER_TIME)
                {
                    m_fhrAlarmRecoverTimer++;
                }
                else
                {
                    //FHR recovered for a while. Turn off audio alarm
                    Alarm_t alarm;
                    alarm.alarmByte = 0;
                    alarm.alarmBits.abnormalFHRalarm = true;
                    clearAudioAlarmPrioHandler(alarm);
                    clearDisplayAlarmPrioHandler(alarm);
                
                    m_fhrAlarmState = eFASnoAlarm;
                    DEBUG_TRACE("m_fhrAlarmState = eFASnoAlarm\r\n");
                }
            }
            else
            {
                //Below threshold. Go back to alarm state
                m_fhrAlarmState = eFASAlarm;
                DEBUG_TRACE("m_fhrAlarmState = eFASAlarm\r\n");
            }
            break;
        default:
            break;
    }
}

/*
===============================================================================
    batteryAlarmHandler()

    Monitor battery capacity and report alarm if condition met.
    
    Initiate power off if condition met


===============================================================================
*/
void Th1Super::batteryAlarmHandler(uint8_t a_remainingCapacity)
{
    if(!HwAbsl::getpHw()->isUSBpowered())
    {
        //Running from battery
        
#ifdef USE_VOLTAGE_AS_SHUTDOWN_CRITERIA
        if(m_batteryVoltagemv < BAT_EMPTY_MV)
        {
            if(!m_batteryEmpty)
            {
                //Send once
                sigDispatch(P_UI, S_BATTERY_EMPTY, 0, 0, 0);
                m_batteryEmpty = true;
            }
        }
#else
        if(a_remainingCapacity < BATT_EMPTY_LIMIT)
        {
            if(!m_batteryEmpty)
            {
                //Send once
                sigDispatch(P_UI, S_BATTERY_EMPTY, 0, 0, 0);
                m_batteryEmpty = true;
            }
        }
#endif
        else if(a_remainingCapacity < BATT_CRITICAL_LIMIT)
        {
            if(!m_displayAlarm.alarmBits.lowBatteryAlarm)
            {
                Alarm_t alarm;
                alarm.alarmByte = 0;
                alarm.alarmBits.lowBatteryAlarm = true;
                setDisplayAlarmPrioHandler(alarm);
                setAudioAlarmPrioHandler(alarm);
                
                
     
            }
        }
        else
        {
            if(m_displayAlarm.alarmBits.lowBatteryAlarm)
            {
                //Clear alarm
                Alarm_t alarm;
                alarm.alarmByte = 0;
                alarm.alarmBits.lowBatteryAlarm = true;
                clearDisplayAlarmPrioHandler(alarm);
                clearAudioAlarmPrioHandler(alarm);
            }
        }
    }    
}

/*
===============================================================================
    getAlarmPriority()

    Get alarm priority. Higher number -> higher priority

===============================================================================
*/
uint8_t Th1Super::getAlarmPriority(Alarm_t a_alarm)
{
    if(a_alarm.alarmBits.criticalErrorAlarm)
    {
        return 5;
    }
    else if(a_alarm.alarmBits.abnormalFHRalarm)
    {
        return 4;
    }
    else if(a_alarm.alarmBits.lostSignalAlarm)
    {
        return 3;
    }
    else if(a_alarm.alarmBits.lowBatteryAlarm)
    {
        return 2;
    }
    
    return 1;
}

/*
===============================================================================
    getPrioritisedAlarm()

    Get the highest priority alarm 

===============================================================================
*/
Alarm_t Th1Super::getPrioritisedAlarm(Alarm_t a_alarm)
{
    Alarm_t alarm;
    alarm.alarmByte = 0;
    
    if(a_alarm.alarmBits.criticalErrorAlarm)
    {
        alarm.alarmBits.criticalErrorAlarm = true;
    }
    else if(a_alarm.alarmBits.abnormalFHRalarm)
    {
        alarm.alarmBits.abnormalFHRalarm = true;
    }
    else if(a_alarm.alarmBits.lostSignalAlarm)
    {
        alarm.alarmBits.lostSignalAlarm = true;
    }
    else if(a_alarm.alarmBits.lowBatteryAlarm)
    {
        alarm.alarmBits.lowBatteryAlarm = true;
    }
    
    return alarm;
}

/*
===============================================================================
    setDisplayAlarmPrioHandler()

    Set display alarm.
    Ensure display alarms are prioritized according to SRS-1859, and that alarm update is only
    send when there is a change

===============================================================================
*/
void Th1Super::setDisplayAlarmPrioHandler(Alarm_t a_alarm)
{
    if(a_alarm.alarmBits.lowBatteryAlarm && !m_displayAlarm.alarmBits.lowBatteryAlarm)
    {
        //Low battery alarm for display can always be set
        sigDispatch(P_UI, S_SET_DISPLAY_ALARM, (uint32_t)a_alarm.alarmByte, m_alarmAcknowlegde.alarmBits.lowBatteryAlarm, 0);
        DEBUG_TRACE("\r\nTh1Super::setDisplayAlarmPrioHandler - Low battery display-alarm set!\r\n");
    }
    
    if(a_alarm.alarmBits.lostSignalAlarm && 
       m_displayAlarm.alarmBits.abnormalFHRalarm &&
       m_alarmAcknowlegde.alarmBits.abnormalFHRalarm)
    {
        //Reset abnormal FHR alarm if lost signal alarm
        m_displayAlarm.alarmBits.abnormalFHRalarm = false;
        sigDispatch(P_UI, S_SET_DISPLAY_ALARM, (uint32_t)a_alarm.alarmByte, m_alarmAcknowlegde.alarmBits.lostSignalAlarm, 0);
        
        //And FHR alarmhandler
        m_fhrAlarmState = eFASnoAlarm;
        
    }
    
    uint8_t currAlarmPri = getAlarmPriority(m_displayAlarm);
    uint8_t alarmPri = getAlarmPriority(a_alarm);
    
    if(currAlarmPri < alarmPri)
    {
        if(a_alarm.alarmBits.criticalErrorAlarm && !m_displayAlarm.alarmBits.criticalErrorAlarm)
        {
            sigDispatch(P_UI, S_SET_DISPLAY_ALARM, (uint32_t)a_alarm.alarmByte, m_alarmAcknowlegde.alarmBits.criticalErrorAlarm, 0);
            m_operationalMode = eOMcriticalError;  //Prevent more normal operations
        }
        else if(a_alarm.alarmBits.abnormalFHRalarm && !m_displayAlarm.alarmBits.abnormalFHRalarm)
        {
            sigDispatch(P_UI, S_SET_DISPLAY_ALARM, (uint32_t)a_alarm.alarmByte, m_alarmAcknowlegde.alarmBits.abnormalFHRalarm, 0);
        }
        else if(a_alarm.alarmBits.lostSignalAlarm && !m_displayAlarm.alarmBits.lostSignalAlarm)
        {
            sigDispatch(P_UI, S_SET_DISPLAY_ALARM, (uint32_t)a_alarm.alarmByte, m_alarmAcknowlegde.alarmBits.lostSignalAlarm, 0);
        }
    }
    
    m_displayAlarm.alarmByte |= a_alarm.alarmByte;

}

/*
===============================================================================
    clearDisplayAlarmPrioHandler()

    Clear display alarm.
    Ensure display alarms are prioritized according to SRS-1859, and that 
    alarm update is only sent when there is a change

===============================================================================
*/
void Th1Super::clearDisplayAlarmPrioHandler(Alarm_t a_alarm)
{
    if(a_alarm.alarmBits.lowBatteryAlarm && !m_displayAlarm.alarmBits.lowBatteryAlarm)
    {
        //Low battery alarm for display can always be cleared
        sigDispatch(P_UI, S_CLEAR_DISPLAY_ALARM, (uint32_t)a_alarm.alarmByte, 0, 0);
        DEBUG_TRACE("\r\nTh1Super::setDisplayAlarmPrioHandler - Low battery display-alarm set!\r\n");
    }
    
    Alarm_t alarm = getPrioritisedAlarm(a_alarm);
    
    sigDispatch(P_UI, S_CLEAR_DISPLAY_ALARM, (uint32_t)alarm.alarmByte, 0, 0);
        
    m_displayAlarm.alarmByte &= ~a_alarm.alarmByte;
    
    //Check if any lower priority alarms are set
    if(m_displayAlarm.alarmByte)
    {
        alarm = getPrioritisedAlarm(m_displayAlarm);
        Alarm_t alarmAcknowledge = getPrioritisedAlarm(m_alarmAcknowlegde);
        sigDispatch(P_UI, S_SET_DISPLAY_ALARM, (uint32_t)alarm.alarmByte, (alarmAcknowledge.alarmByte > 0 ? true : false), 0);
    }
}

/*
===============================================================================
    setAudioAlarmPrioHandler()

    Ensure audible alarms are prioritized according to SRS-1859, and that 
    alarm update is only sent when there is a change

===============================================================================
*/
void Th1Super::setAudioAlarmPrioHandler(Alarm_t a_alarm)
{
    if(a_alarm.alarmBits.lostSignalAlarm && 
       m_audioAlarm.alarmBits.abnormalFHRalarm &&
       m_alarmAcknowlegde.alarmBits.abnormalFHRalarm)
    {
        //Reset abnormal FHR alarm if lost signal alarm and abnormal alarm is acknowlegded
        m_audioAlarm.alarmBits.abnormalFHRalarm = false;
        m_alarmAcknowlegde.alarmBits.abnormalFHRalarm = false;
        sigDispatch(P_UI, S_SET_DISPLAY_ALARM, (uint32_t)a_alarm.alarmByte, m_alarmAcknowlegde.alarmBits.lostSignalAlarm, 0);
        
    }
    
    uint8_t currAlarmPri = getAlarmPriority(m_audioAlarm);
    uint8_t alarmPri = getAlarmPriority(a_alarm);
    
    if(currAlarmPri < alarmPri)
    {
        if(a_alarm.alarmBits.criticalErrorAlarm && 
           !m_audioAlarm.alarmBits.criticalErrorAlarm &&
           !m_alarmAcknowlegde.alarmBits.criticalErrorAlarm)
        {
            setAudioAlarm(getAudioAlarmType(a_alarm));
            sigDispatch(P_UI, S_SET_AUDIO_ALARM, (uint32_t)a_alarm.alarmByte, 0, 0);
        }
        else if(a_alarm.alarmBits.abnormalFHRalarm && 
                !m_audioAlarm.alarmBits.abnormalFHRalarm &&
                !m_alarmAcknowlegde.alarmBits.abnormalFHRalarm)
        {
            setAudioAlarm(getAudioAlarmType(a_alarm));
            sigDispatch(P_UI, S_SET_AUDIO_ALARM, (uint32_t)a_alarm.alarmByte, 0, 0);
        }
        else if(a_alarm.alarmBits.lostSignalAlarm && 
                !m_audioAlarm.alarmBits.lostSignalAlarm &&
                !m_alarmAcknowlegde.alarmBits.lostSignalAlarm)
        {
            setAudioAlarm(getAudioAlarmType(a_alarm));
            sigDispatch(P_UI, S_SET_AUDIO_ALARM, (uint32_t)a_alarm.alarmByte, 0, 0);
        }
        else if(a_alarm.alarmBits.lowBatteryAlarm && 
                !m_audioAlarm.alarmBits.lowBatteryAlarm &&
                !m_alarmAcknowlegde.alarmBits.lowBatteryAlarm)
        {
            setAudioAlarm(getAudioAlarmType(a_alarm));
            sigDispatch(P_UI, S_SET_AUDIO_ALARM, (uint32_t)a_alarm.alarmByte, 0, 0);
        }
    }
    
    m_audioAlarm.alarmByte |= a_alarm.alarmByte;
}

/*
===============================================================================
    getAudioAlarmType()

    Get audio alarm to play (alarms are in priorized order: highest priority first

===============================================================================
*/
Th1Super::AlarmType_t Th1Super::getAudioAlarmType(Alarm_t a_alarm)
{
    AlarmType_t audioAlarmType = eATnoAlarm;
    
    if(a_alarm.alarmBits.criticalErrorAlarm)
    {
        audioAlarmType = eATlowPri;
    }
    else if(a_alarm.alarmBits.abnormalFHRalarm)
    {
        audioAlarmType = eATmedPri;
    }
    else if(a_alarm.alarmBits.lostSignalAlarm)
    {
        audioAlarmType = eATlowPri;
    }
    else if(a_alarm.alarmBits.lowBatteryAlarm)
    {
        audioAlarmType = eATlowPri;
    }
    
    return audioAlarmType;
}

/*
===============================================================================
    clearAudioAlarmPrioHandler()

    Clear audible alarm, send update, and if more alarms present, set next alarm based on priority

===============================================================================
*/
void Th1Super::clearAudioAlarmPrioHandler(Alarm_t a_alarm)
{
    uint8_t currAlarmPri = getAlarmPriority(m_audioAlarm);
    uint8_t alarmPri = getAlarmPriority(a_alarm);
    
    if(currAlarmPri >= alarmPri)
    {
        sigDispatch(P_UI, S_CLEAR_AUDIO_ALARM, (uint32_t)a_alarm.alarmByte, 0, 0);
    }
    
    //Clear alarm
    m_audioAlarm.alarmByte &= ~a_alarm.alarmByte;
    m_alarmAcknowlegde.alarmByte &= ~a_alarm.alarmByte;
    
    //Set lower prority (or clear if none) audio alarm
    setAudioAlarm(getAudioAlarmType(m_audioAlarm));
    
    //Update UI if lower prority alarms remaining
    if(m_audioAlarm.alarmByte)
    {
        Alarm_t alarm = getPrioritisedAlarm(m_audioAlarm);
        sigDispatch(P_UI, S_SET_AUDIO_ALARM, (uint32_t)alarm.alarmByte, 0, 0);
    }
}

/*
===============================================================================
    setAudioAlarm()

    Initiate an alarm

===============================================================================
*/
void Th1Super::setAudioAlarm(AlarmType_t a_alarmType)
{
    switch(a_alarmType)
    {
        case eATlowPri:
            m_alarmSoundTimer = 0; //Fire immediate
            break;
        case eATmedPri:
            m_alarmSoundTimer = 0; //Fire immediate
            break;
        default:
            break;
    }
    
    m_alarmType = a_alarmType;
            
}

/*
===============================================================================
    setTestSound()

    Initiate a test sound (production test)

===============================================================================
*/
void Th1Super::setTestSound(SoundTestCommand_t a_command)
{
    if(m_operationalMode == eOMtestMode)
    {
        if(!HwAbsl::getpHw()->getpAudio()->isAudioOn())
        {
            HwAbsl::getpHw()->getpAudio()->audioOn();
        }
        
        HwAbsl::getpHw()->getpAudio()->stopPlayback();
        
        switch(a_command)
        {
            case eSTCStop:
                setAudioAlarm(eATnoAlarm);
                break;
            case eSTCPlaysine200:
                setAudioAlarm(eATnoAlarm);
                HwAbsl::getpHw()->getpAudio()->playSin200();
                break;
            case eSTCPlaysine1000:
                setAudioAlarm(eATnoAlarm);
                HwAbsl::getpHw()->getpAudio()->playSin1000();
                break;
            case eSTCPlayMedPriAlarm:
                setAudioAlarm(eATnoAlarm);
                HwAbsl::getpHw()->getpAudio()->playMediumPriorityAlarmSignalContinuous();
                break;
            case eSTCPlayLowPriAlarm:
                setAudioAlarm(eATnoAlarm);
                HwAbsl::getpHw()->getpAudio()->playLowPriorityAlarmSignalContinuous();
                break;
            case eSTCPlayStartupSound:
                setAudioAlarm(eATnoAlarm);
                HwAbsl::getpHw()->getpAudio()->playNormalStartupSoundContinuous();
                break;
            default:
                break;
        }
    }
}

/*
===============================================================================
    initStatusLEDtest()

    Initiate status LED (production test)

===============================================================================
*/
void Th1Super::initStatusLEDtest(StatusLEDtestCommand_t a_command)
{
    if(m_operationalMode == eOMtestMode)
    {
        m_keyPressTestTimer = KEY_PRESS_TEST_TIMEOUT; //Start keypress timer
    
        initKeyTest();
        
        switch(a_command)
        {
            case eSLToff:
                HwAbsl::getpHw()->setGreenLED(false);
                HwAbsl::getpHw()->setRedLED(false);
                m_keyPressTestTimer = 0; //This command should not create a responce
                m_UItestType = eUITTnone;
                break;
            case eSLTredLED:
                HwAbsl::getpHw()->setGreenLED(false);
                HwAbsl::getpHw()->setRedLED(true);
                break;
            case eSLTgreenLED:
                HwAbsl::getpHw()->setGreenLED(true);
                HwAbsl::getpHw()->setRedLED(false);
                break;
            case eSLTyellowLED:
                HwAbsl::getpHw()->setGreenLED(GREEN_LED_REDUCED_PWM);
                HwAbsl::getpHw()->setRedLED(RED_LED_REDUCED_PWM);
                break;
            default:
                HwAbsl::getpHw()->setGreenLED(false);
                HwAbsl::getpHw()->setRedLED(false);
                m_keyPressTestTimer = 0;
                m_UItestType = eUITTnone;
                break;
        }
    }
}

/*
===============================================================================
    UItestHandler()

    Handle buttons, display and status LED in production test

===============================================================================
*/
void Th1Super::UItestHandler(void)
{
    if(m_operationalMode == eOMtestMode)
    {
        if(m_keyPressTestTimer)
        {
            bool onKeyPressed = HwAbsl::getpHw()->isOnKeyPressed();
            bool displayModeKeyPressed = HwAbsl::getpHw()->isDisplayModeKeyPressed();
            bool audioVolumeKeyPressed = HwAbsl::getpHw()->isAudioVolumeKeyPressed();
            
            //Find if and what key is pressed
            if(!onKeyPressed && !displayModeKeyPressed && !audioVolumeKeyPressed)
            {
                m_currentKeyTested = eKeyNone;
            }
            else if(onKeyPressed && !displayModeKeyPressed && !audioVolumeKeyPressed)
            {
                m_currentKeyTested = eKeyPower;
                
                HwAbsl::getpHw()->setGreenLED(false);
                HwAbsl::getpHw()->setRedLED(false);
            }
            else if(!onKeyPressed && displayModeKeyPressed && !audioVolumeKeyPressed)
            {
                m_currentKeyTested = eKeyDisplayMode;
            }
            else if(!onKeyPressed && !displayModeKeyPressed && audioVolumeKeyPressed)
            {
                m_currentKeyTested = eKeyMute;
            }
            else
            {
                m_currentKeyTested = eKeyError;
            }
        
            if(m_keyPressTestTimer == 1)
            {
                //Timeout
                m_keyPressTestTimer = 0;
                
                HwAbsl::getpHw()->setGreenLED(false);
                HwAbsl::getpHw()->setRedLED(false);
                
                sigDispatch(P_UI, S_TEST_COMMAND_DISPLAY, 0, 0 ,0);
                
                setTestSound(eSTCStop);
                
                m_UItestType = eUITTnone;
                
                CONSOLE_PRINT("R124 0\r\n");
            }
            else if(m_keyPressTestTimer)
            {
                m_keyPressTestTimer--;
                
                switch(m_keyTestState)
                {
                    case eKTSidle:
                        m_keyTestState = eKTSwaitForPress;
                        break;
                    case eKTSwaitForPress:
                        if(m_currentKeyTested != eKeyNone && m_currentKeyTested != eKeyError)
                        {
                            //One key pressed
                            m_keyTestState = eKTSwaitForRelease;
                            m_keyTested = m_currentKeyTested;
                        }
                        break;
                    case eKTSwaitForRelease:
                        if(m_currentKeyTested == eKeyNone && m_currentKeyTested != eKeyError)
                        {
                            if(m_UItestType == eUITTled)
                            {
                                if(m_keyTested == eKeyPower)
                                {
                                    m_keyTestState = eKTSidle;
                                    m_keyPressTestTimer = 0;
                                    m_UItestType = eUITTnone;
                                    CONSOLE_PRINT("R124 %u\r\n", m_keyTested);
                                }
                            }
                            else if(m_UItestType == eUITTdisplay)
                            {
                                if(m_keyTested == eKeyDisplayMode)
                                {
                                    sigDispatch(P_UI, S_TEST_COMMAND_DISPLAY, 0, 0 ,0);
                                    m_keyTestState = eKTSidle;
                                    m_keyPressTestTimer = 0;
                                    m_UItestType = eUITTnone;
                                    CONSOLE_PRINT("R124 %u\r\n", m_keyTested);
                                }
                            }
                            else if(m_UItestType == eUITTsound)
                            {
                                if(m_keyTested == eKeyMute)
                                {
                                    setTestSound(eSTCStop);
                                    m_keyTestState = eKTSidle;
                                    m_keyPressTestTimer = 0;
                                    m_UItestType = eUITTnone;
                                    CONSOLE_PRINT("R124 %u\r\n", m_keyTested);
                                }
                            }
                            else
                            {
                                //One key released
                                m_keyTestState = eKTSidle;
                                m_keyPressTestTimer = 0;
                                m_UItestType = eUITTnone;
                                CONSOLE_PRINT("R124 %u\r\n", m_keyTested);
                            }
                            
                            initKeyTest(); //Reset so we don't retrigger (if wrong button was pressed)
                        }
                        break;
                    default:
                        m_keyTestState = eKTSidle;
                }
            
            
            }
        }
    }
}

/*
===============================================================================
    initKeyTest()

    Init buttons test in production test

===============================================================================
*/
void Th1Super::initKeyTest(void)
{
    if(m_operationalMode == eOMtestMode)
    {
        m_keyTestState = eKTSidle;
        m_currentKeyTested = eKeyNone;
        m_keyTested = eKeyNone;
        
        //Reset debouncers
        for(uint8_t i = 0; i < 100; i++)
        {
            HwAbsl::getpHw()->isOnKeyPressed();
            HwAbsl::getpHw()->isDisplayModeKeyPressed();
            HwAbsl::getpHw()->isAudioVolumeKeyPressed();
        }
    }
}

/*
===============================================================================
    audioAlarmHandler()

    Handler for play alarm sounds at intervals

===============================================================================
*/
void Th1Super::audioAlarmHandler(void)
{
    if(m_alarmSoundTimer)
    {
        m_alarmSoundTimer--;
    }
    else if(m_alarmSoundEnabled)
    {
        //Get whether the alarm was acknowlegded
        Alarm_t alarmAcknowlegde = getPrioritisedAlarm(m_alarmAcknowlegde);
        Alarm_t audioAlarm = getPrioritisedAlarm(m_audioAlarm);
        
        if(audioAlarm.alarmByte != alarmAcknowlegde.alarmByte)
        {
            //Alarm is not acknowledged. Play alarm sound
            
            switch(m_alarmType)
            {
                case eATnoAlarm:
                    break;
                case eATlowPri:
                    HwAbsl::getpHw()->getpAudio()->playLowPriorityAlarmSignal();
                    m_alarmSoundTimer = LOW_PRI_ALARM_CYCLE_TIME;
                    break;
                case eATmedPri:
                    HwAbsl::getpHw()->getpAudio()->playMediumPriorityAlarmSignal();
                    m_alarmSoundTimer = MED_PRI_ALARM_CYCLE_TIME;
                    break;
                default:
                    break;
            }
        }
    }
}

/*
===============================================================================
    handleFuelGauge()

    Handler for reading fuel gauge via I2C at intervals

===============================================================================
*/
void Th1Super::handleFuelGauge()
{
    if(m_fuelGaugeTmr)
    {
        m_fuelGaugeTmr--;
    }
    else
    {
        m_fuelGaugeTmr = FUEL_GAUGE_READ_TIME;
        Bq27510::ProbeResult result;
        //DEBUG_TRACE("\r\nHwAbsl::getpHw()->getpBq27510()->probe(result);\r\n");
        bool success = HwAbsl::getpHw()->getpBq27510()->probe(result);
        
        //DEBUG_TRACE("\r\nHwAbsl::getpHw()->getpBq27510()->probe(result) - Finished;\r\n");
        
        HwAbsl::ChargerStatus_t chState = HwAbsl::getpHw()->getChargeStatus();
        
        if(result.remainingCapacityPercent != m_batteryRemainingCapacity && success)
        {
            if(m_operationalMode == eOMnormal)
            {
                //Mask increasing values in operating mode. m_batteryRemainingCapacity was
                //initially read from eeprom and low battery conditions was handled at startup.
                //We don't want to indicate higher battery level at a later time.
                
                if(result.remainingCapacityPercent < m_batteryRemainingCapacity)
                {
                    m_batteryRemainingCapacity = result.remainingCapacityPercent;
                }
            }
            else
            {
                m_batteryRemainingCapacity = result.remainingCapacityPercent;
                
                //We are not in normal operating mode, i.e. battery is charged
                //If remaining capacity = 100%, keep it at 99% until fully charged, so
                //charge indicator is not showing fully charged before battery actually is fully charged
                if(m_batteryRemainingCapacity > 99 && chState != HwAbsl::eCSTchargeComplete)
                {
                    m_batteryRemainingCapacity = 99;
                }
            }
            
            
            sigDispatch(P_UI, S_BATTERY_REMAINING_CAPACITY, (uint32_t)m_batteryRemainingCapacity, 0, 0);
            
        
        }

        if(chState == HwAbsl::eCSTchargeComplete && !m_batteryFull)
        {
            m_batteryFull = true;
            sigDispatch(P_UI, S_BATTERY_FULL, 1, 0, 0);
        }
                        
#ifdef USE_VOLTAGE_AS_SHUTDOWN_CRITERIA
        if(result.voltage_mV != m_batteryVoltagemv && success)
        {
            m_batteryVoltagemv = result.voltage_mV;
        }
#endif
        
        
        if(m_batteryDebugOut)
        {
            CONSOLE_PRINT("\r\nTh1Super::handleFuelGauge() - \r\n");
            CONSOLE_PRINT("Voltage [mV]: %d\r\n", result.voltage_mV);
            CONSOLE_PRINT("Current [uA]: %d\r\n", result.current_uA);
            CONSOLE_PRINT("Avg current [uA]: %d\r\n", result.averageCurrent_uA);
            CONSOLE_PRINT("Temperature [mC]: %d\r\n", result.temperature_mC);
            CONSOLE_PRINT("Rem capacity [%%]: %d\r\n", result.remainingCapacityPercent);
            if(result.charging)
            {
                CONSOLE_PRINT("Battery is charged\r\n");
            }
            else
            {
                CONSOLE_PRINT("Battery is not charged\r\n");
            }
            if(!success)
            {
                CONSOLE_PRINT("\r\nTransmission fail!\r\n");
            }
        }
        
#ifdef REPORT_BATTERY_STATE
        uint32_t time = OS_GetTime();
        static uint32_t bsNextLogTime = OS_GetTime();
        
        if(time >= bsNextLogTime)
        {
            bsNextLogTime+=60000;
        
            Events::getpEvents()->AddEvent(Events::evtBatteryState, 
                                   Events::TO_SYSTEM_LOG,
                                   (float)m_batteryRemainingCapacity,
                                   (float)result.voltage_mV,
                                   (float)result.averageCurrent_uA,
                                   (float)chState);
        }
#endif

        
    }
}
  
/*
===============================================================================
    systemShutdown()

    Initiate a system shutdown
    
    Save battery capacity for fast update of battery symbol at next startup

===============================================================================
*/
void Th1Super::systemShutdown(ShutdownReason_t a_reason)
{
    
    Events::getpEvents()->AddEvent(Events::evtPowerOffBatteryState, 
                                   Events::TO_EPISODE_LOG,
                                   (float)m_batteryRemainingCapacity);
    //Close open episodes
    sigDispatch(P_FILE, S_SYSTEM_SHUTDOWN, 0, 0 ,0);
    
    HwAbsl::getpHw()->setGreenLED(false);
    DEBUG_TRACE("\r\nPower off\r\n");
    Config::getpConfig()->SaveConfig(Config::eCfgShutdownReason, (uint16_t)a_reason);
    Config::getpConfig()->SaveConfig(Config::eCfgLastBattRemCharge, (uint16_t)m_batteryRemainingCapacity);
    
    //File thread will do shutoff
    
}

/*
===============================================================================
    sampleFhrData()

    Add a record with fhr data to buffer for file

    Add changes in temperature and heart rate to event queue


===============================================================================
*/
void Th1Super::sampleFhrData(uint32_t time)
{
    FHRrecord_t fhrRecord;
    fhrRecord.TimeStamp = time;
    fhrRecord.FHR = HwAbsl::getpHw()->getProbeSerial()->GetFetalHeartRate();
    fhrRecord.FHRquality = HwAbsl::getpHw()->getProbeSerial()->GetSignalQuality();
    fhrRecord.MHR = m_pAdcTh->GetMaternalHR();
    fhrRecord.ProbeTemp = HwAbsl::getpHw()->getProbeSerial()->GetProbeTempAsRaw();
    
    //Only at defined interval. Probe sends too often
    if(m_probeTemperatureTimer)
    {
        m_probeTemperatureTimer--;
    }
    else
    {
        m_probeTemperatureTimer = PROBE_TEMP_EVENT_TIME - 1;
        m_probeTemperature = fhrRecord.ProbeTemp;
        
        Events::getpEvents()->AddEvent(Events::evtProbeTemperature, 
                                       Events::TO_EPISODE_LOG,
                                       (float)m_probeTemperature/4.0);
        
    }

    if(m_fetalHR != fhrRecord.FHR || m_fetalHRSigQual != fhrRecord.FHRquality)
    {
        //FHR changed. Log it
        Events::getpEvents()->AddEvent(Events::evtFHR, 
                                       Events::TO_EPISODE_LOG,
                                       (float)fhrRecord.FHR,
                                       (float)fhrRecord.FHRquality);
        
        m_fetalHR = fhrRecord.FHR;
        m_fetalHRSigQual = static_cast<FHRMps::SignalQuality_t>(fhrRecord.FHRquality);
    }
    
    FhrRecordsLock();
    if(m_FhrRecords.size() >= FHR_RECORDS_BUFFER_SIZE)
    {
        //Delete oldest record
        m_FhrRecords.erase(m_FhrRecords.begin());
    }
    m_FhrRecords.push_back(fhrRecord);
    FhrRecordsUnlock();
}

/*
===============================================================================
    GetFhrRecords()

    Get record(s) with fhr data from buffer for file


===============================================================================
*/
void Th1Super::GetFhrRecords(std::vector<FHRrecord_t> & a_Records)
{
    FhrRecordsLock();
    
    a_Records = m_FhrRecords;
        
    if(m_FhrRecords.size())
    {
        m_FhrRecords.resize(0);
    }
    
    FhrRecordsUnlock();
}

/*
===============================================================================
    FhrRecordsLock()

    Lock FhrRecords buffer

===============================================================================
*/
bool Th1Super::FhrRecordsLock()
{
    if (0==fhrBufferMutex)
    {
        fhrBufferMutex = new Mutex(); // create when needed
    }
    if (fhrBufferMutex)
    {
        fhrBufferMutex->lock();
        return true;
    }
    return false;
}

/*
===============================================================================
    FhrRecordsUnlock()

    Unlock FhrRecords buffer

===============================================================================
*/
bool Th1Super::FhrRecordsUnlock()
{
    if (fhrBufferMutex)
    {
        fhrBufferMutex->release();
        return true;
    }
    return false;
}

/*
===============================================================================
    EpisodeHandler()

    Monitor episode regarding to end of episode and if valid, based on time, 
    heartrate from doppler probe, heartrate from electrodes (maternal hr)
    and calculated acceleration from g-force sensor in doppler sensor

===============================================================================
*/
void Th1Super::EpisodeHandler(uint8_t a_fetal_hr, uint8_t a_maternalHR, int16_t a_accEnergy)
{
    if(a_fetal_hr >= FHR_LOST_SIGNAL_ALARM_LIMIT)
    {
        if(m_episodeTimers.FHRvalidTimer)
        {
            m_episodeTimers.FHRvalidTimer--;
        }
        
        m_episodeTimers.FHRinvalidTimer = END_OF_EP_TMOUT_FILTER_TIME;
    }
    else
    {
        m_episodeTimers.FHRvalidTimer = END_OF_EP_TMOUT_FILTER_TIME;
        
        if(m_episodeTimers.FHRinvalidTimer)
        {
            m_episodeTimers.FHRinvalidTimer--;
        }
    }
    
    if(a_maternalHR >= MIN_VALID_HR)
    {
        if(m_episodeTimers.MHRvalidTimer)
        {
            m_episodeTimers.MHRvalidTimer--;
        }
    }
    else
    {
        m_episodeTimers.MHRvalidTimer = END_OF_EP_TMOUT_FILTER_TIME;
    }
     
    if(abs(a_accEnergy) > MIN_ACCELERATION)
    {
        //Acceleration detected. Set timer so short stops in acceleration are ignored
        m_episodeTimers.motionTimer = MOTION_TIMEOUT;

#ifdef DEBUG_EPISODE_HANDLER
        motionTimer = m_episodeTimers.motionTimer;
#endif
        
        if(m_episodeTimers.accSignalTimer)
        {
            m_episodeTimers.accSignalTimer--;

#ifdef DEBUG_EPISODE_HANDLER
            accSignalTimer = m_episodeTimers.accSignalTimer;
#endif
            
        }
        //DEBUG_TRACE("Acc\r\n");
        //DEBUG_TRACE("Set motionTimer\r\n");
        
    }
    else
    {
        if(m_episodeTimers.motionTimer)
        {
            m_episodeTimers.motionTimer--;

#ifdef DEBUG_EPISODE_HANDLER
            motionTimer = m_episodeTimers.motionTimer;
#endif
            
            if(m_episodeTimers.accSignalTimer)
            {
                m_episodeTimers.accSignalTimer--;

#ifdef DEBUG_EPISODE_HANDLER
                accSignalTimer = m_episodeTimers.accSignalTimer;
#endif
                
            }
        }
        else
        {
            //No motion detected. Reset acc timer
            m_episodeTimers.accSignalTimer = END_OF_EP_TMOUT_FILTER_TIME;

#ifdef DEBUG_EPISODE_HANDLER
            accSignalTimer = m_episodeTimers.accSignalTimer;
#endif
            
            //DEBUG_TRACE("Reset accSignalTimer\r\n");
        }
        
    }
    
    if(m_episodeTimers.accSignalTimer == 0 ||
       m_episodeTimers.FHRvalidTimer == 0 ||
       m_episodeTimers.MHRvalidTimer == 0)
    {
        m_episodeTimers.endOfEpisodeTimer = m_endOfEpisodeTime;

#ifdef DEBUG_EPISODE_HANDLER
        endOfEpisodeTimer = m_episodeTimers.endOfEpisodeTimer;
#endif
        
    }
    else
    {
        if(m_episodeTimers.endOfEpisodeTimer > 1)
        {
            m_episodeTimers.endOfEpisodeTimer--;
            
#ifdef DEBUG_EPISODE_HANDLER
            endOfEpisodeTimer = m_episodeTimers.endOfEpisodeTimer;
#endif
            
        }
        else if(m_episodeTimers.endOfEpisodeTimer == 1) //Only once
        {
            m_episodeTimers.endOfEpisodeTimer--;

            //No acc signals and no maternal HR and no fetal HR for long time. Close episode and shut down
            Events::getpEvents()->AddEvent(Events::evtEndOfEpisodeTimeout, 
                                           Events::TO_EPISODE_LOG,
                                           1.0);
            sigDispatch(P_UI, S_SYSTEM_SHUTDOWN, 0, 0 ,0);
            systemShutdown(eSDRepTimeout);
        }
    }

#ifdef DEBUG_EPISODE_HANDLER
    endOfEpisodeTimer = m_episodeTimers.endOfEpisodeTimer;
    validEpisodeTimer = m_episodeTimers.validEpisodeTimer;
#endif
    
    //Timer for deleting short episodes
    if(m_episodeTimers.episodeTooShortTimer > 1)
    {
        m_episodeTimers.episodeTooShortTimer--;
    }
    else if(m_episodeTimers.episodeTooShortTimer == 1) //Only once
    {
        m_episodeTimers.episodeTooShortTimer--;
        
        //Episode last for for defined time
        sigDispatch(P_FILE, S_FILE_KEEP_EPISODE, 0, 0 ,0);
        
    }
    
    if(m_episodeTimers.FHRinvalidTimer == 0)
    {
        //Invalid FHR for some time. Reset valid episode timer
        m_episodeTimers.validEpisodeTimer = m_validEpisodeTime;
    }
    else if(m_episodeTimers.validEpisodeTimer > 1)
    {
        m_episodeTimers.validEpisodeTimer--;
    }
    else if(m_episodeTimers.validEpisodeTimer == 1) //Only once
    {
        m_episodeTimers.validEpisodeTimer--;
        
        //Valid fetal HR from sensor recieved for defined time
        sigDispatch(P_FILE, S_FILE_VALIDATE_EPISODE, 0, 0 ,0);
        
    }
}

/*
===============================================================================
    watchdogKickHandler()

    Initiate a watchdog kick.

    Watchdog kicks are relayed through critical threads in such a way that
    if one critical thread stop, the watchdog will reset processor

===============================================================================
*/
void Th1Super::watchdogKickHandler(void)
{
    if(m_wdKickTmr)
    {
        m_wdKickTmr--;
    }
    else
    {
        m_wdKickTmr = WD_KICK_TIME;
        
        //Send to UI thread which then will kick the watchdog
        //In this manner, the watchdog will cover lockup in super and UI thread.
        sigDispatch(P_UI, S_WATCHDOG_KICK);
    }
}

/*
===============================================================================
    getPostResult()

    Get POST result for fuel gauge, 5V and RTC

===============================================================================
*/
POST_ctrl_t Th1Super::getPostResult(void)
{
    return m_POSTstatus;
}

/*
===============================================================================
    clearPostResult()

    Clear POST result for fuel gauge, 5V and RTC

===============================================================================
*/
void Th1Super::clearPostResult(void)
{
    m_POSTstatus.finished = false;
}

/*
===============================================================================
    post()

    POST for fuel gauge, 5V and RTC

===============================================================================
*/
bool Th1Super::post(void)
{
    uint16_t result;
    uint16_t noRetriesLeft = 1000;    
    bool success;
    
    m_POSTstatus.post.postWord = 0;
    m_POSTstatus.finished = false;
    
#ifndef NO_FUEL_GAUGE
    do
    {
        //success = HwAbsl::getpHw()->getpBq27510()->getDesignCapacity(result);
        success = HwAbsl::getpHw()->getpBq27510()->getDeviceType(result);
        noRetriesLeft--;
        sleep(100);
    } while (!success && noRetriesLeft > 0);
#else
    success = false;
#endif
    
    if(!success)
    {
        m_POSTstatus.post.postBits.fuelGaugeComm = true;
        DEBUG_TRACE("\r\nPOST FAIL - Fuel gauge bus fail!\r\n");
    }
    else if(result == FUEL_GAUGE_DEFAULT_DESIGN_CAPACITY)
    {
        //Fuel gauge probably not initiated
        m_POSTstatus.post.postBits.fuelGaugeComm = true;
        DEBUG_TRACE("\r\nPOST FAIL - Fuel Gauge not inited!\r\n");
    }
    else
    {
        m_POSTstatus.post.postBits.fuelGaugeComm = false;
    }
    
    //ADC 5V selftest
    uint32_t adcval = 0;
    
    for(uint16_t i = 0; i < ADC5V_POST_NO_SAMPLES; i++)
    {
        adcval += HwAbsl::getpHw()->getpADC()->read(AD_5V);
        sleep(5);
    }
    
    adcval /= ADC5V_POST_NO_SAMPLES;
    
    if(adcval >= ADC5V_MIN && adcval <= ADC5V_MAX)
    {
        //Succedded
        m_POSTstatus.post.postBits.voltageRegulator = false;
    }
    else
    {
        //Failed
        m_POSTstatus.post.postBits.voltageRegulator = true;
    }
    
    //RTC
    m_POSTstatus.post.postBits.RTCcrystal = HwAbsl::getpHw()->getpRtc()->RTCgetError();
    
    m_POSTstatus.finished = true;
    
    return (m_POSTstatus.post.postWord != 0);
    
}

void Th1Super::enableMaxBatteryCharge()
{
    //Could be a charger connected
    //Lets try enable 1A charging and see if USB power disappears
    //Dirty method, but it is the only one we have
    
    //First, see if a reboot is done previously due to power loss (1A was enabled with a USB none-charger and bad battery).
    
    uint16_t lastShutdownReason = Config::getpConfig()->GetConfig(Config::eCfgShutdownReason);
    if((ShutdownReason_t)lastShutdownReason != eSDRusbSuspensionPowerloss)
    {
        //Power good
        if(HwAbsl::getpHw()->isUSBpowered())
        {
            if(!m_testpinActive)
            {
                DEBUG_TRACE("\r\nUSB current set to 1A\r\n");
                HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_1000mA_Charge);
            }
            else
            {
                DEBUG_TRACE("\r\nUSB current set to 500mA. Battery charge disabled (in test mode)\r\n");
                HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PL_500mA_noCharge);
            }
            
            //Wait and see if power is still good
            sleep(2000);
            if(!HwAbsl::getpHw()->isUSBpowered())
            {
                //USB power disappeard. Go back to 500mA
                if(!m_testpinActive)
                {
                    DEBUG_TRACE("\r\nUSB overcurrent when pulling 1A! USB current set back to 500mA\r\n");
                    HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PL_500mA_Charge);
                    sleep(2000);
                }
                else
                {
                    DEBUG_TRACE("\r\nUSB current set to 500mA. Battery charge disabled (in test mode)\r\n");
                    HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PL_500mA_noCharge);
                }
            }
        }
    }
}

/*
===============================================================================
    flashFuelgauge()

    Flash fuelgauge with Golden Image (in production test)
    
    The Golden image contains calibration parameters for current, voltage,
    offsets, load resistor, battery capacitance, battery capacity and temperature.

    Note: Only to be used with a new battery. Fuelgauge will be initiated to
    think a brand new battery is inserted. Aging parameters will be deleted.

    Note: The Golden image is to use only on TI BQ27510-G2 (firmware version 1.23).
    If TI BQ27510-G3 firmware version 4.00) is to be used, a new Golden file have
    to be made.
===============================================================================
*/
void Th1Super::flashFuelgauge(void)
{
    bool success = false;
        
    if(HwAbsl::getpHw()->getpI2C() != NULL && 
       (HwAbsl::getpHw()->getStartupMode() == HwAbsl::eStartupModeUSBpowered ||
        HwAbsl::getpHw()->getStartupMode() == HwAbsl::eStartupModeUSBvirtCOM))
    {
        //Do not try to flash fuelgauge if powered from battery. Will sometimes fail for some reason.
        
        uint8_t cmd[2];
        
        //Disable normal communication with fuelgauge
        HwAbsl::getpHw()->getpBq27510()->enableCommunication(false);
        sleep(200);
        
        //Unseal device
        cmd[0] = 0x14;
        cmd[1] = 0x04;
        HwAbsl::getpHw()->getpI2C()->sendFrame(0xAA, (uint8_t)0, cmd, ARR_ELEMS(cmd));
        sleep(200);
        
        cmd[0] = 0x72;
        cmd[1] = 0x36;
        HwAbsl::getpHw()->getpI2C()->sendFrame(0xAA, (uint8_t)0, cmd, ARR_ELEMS(cmd));
        sleep(200);
        
        cmd[0] = 0xff;
        cmd[1] = 0xff;
        HwAbsl::getpHw()->getpI2C()->sendFrame(0xAA, (uint8_t)0, cmd, ARR_ELEMS(cmd));
        sleep(200);
        HwAbsl::getpHw()->getpI2C()->sendFrame(0xAA, (uint8_t)0, cmd, ARR_ELEMS(cmd));
        sleep(200);
        
            
        //Enter ROM mode
        cmd[0] = 0x00;
        cmd[1] = 0x0F;
        HwAbsl::getpHw()->getpI2C()->sendFrame(0xAA, (uint8_t)0, cmd, ARR_ELEMS(cmd));
        //DEBUG_TRACE("\r\nSend 0xAA, 0x00, 0x%X, 0x%X", cmd[0], cmd[1]);
        sleep(200);
        
        //Setup mass erase
        cmd[0] = 0x00;
        cmd[1] = 0x0C;
        HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, cmd, ARR_ELEMS(cmd));
        //DEBUG_TRACE("\r\nSend 0x16, 0x%X, 0x%X", cmd[0], cmd[1]);
        
        cmd[0] = 0x04;
        cmd[1] = 0x83;
        HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, cmd, ARR_ELEMS(cmd));
        //DEBUG_TRACE("\r\nSend 0x16, 0x%X, 0x%X", cmd[0], cmd[1]);
        
        cmd[0] = 0x05;
        cmd[1] = 0xDE;
        HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, cmd, ARR_ELEMS(cmd));
        //DEBUG_TRACE("\r\nSend 0x16, 0x%X, 0x%X", cmd[0], cmd[1]);
        
        //Mass erase checksum
        cmd[0] = 0x64;
        cmd[1] = 0x6D;
        HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, cmd, ARR_ELEMS(cmd));
        //DEBUG_TRACE("\r\nSend 0x16, 0x%X, 0x%X", cmd[0], cmd[1]);
        
        cmd[0] = 0x65;
        cmd[1] = 0x01;
        HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, cmd, ARR_ELEMS(cmd));
        //DEBUG_TRACE("\r\nSend 0x16, 0x%X, 0x%X", cmd[0], cmd[1]);
        sleep(500);
        
        //Check if succeeded
        uint8_t val[1];
        //success = true; //Test
        //val[0] = 0; //Test
        success = HwAbsl::getpHw()->getpI2C()->receiveFrame(0x16, (uint8_t)0x66, val, ARR_ELEMS(val));
        //DEBUG_TRACE("\r\nRecieve 0x16, 0x66, Resp=%X", val[0]);
        if(val[0] != 0)
        {
            DEBUG_TRACE("\r\nError when erasing flash on fuelgauge!\r\n");
            success = false;
        }
        
        
        if(success)
        {
            //Succeeded, proceed
            for(uint8_t row = 0; row < ARR_ELEMS(dfidata); row++)
            {
                //Set address command
                cmd[0] = 0x00;
                cmd[1] = 0x0A;
                HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, cmd, ARR_ELEMS(cmd));
                //DEBUG_TRACE("\r\nSend 0x16, 0x%X, 0x%X", cmd[0], cmd[1]);
                sleep(10);
                
                //Set address
                cmd[0] = 0x01;
                cmd[1] = row;
                HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, cmd, ARR_ELEMS(cmd));
                //DEBUG_TRACE("\r\nSend 0x16, 0x%X, 0x%X", cmd[0], cmd[1]);
                sleep(10);
                
                //Program row
                HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, (uint8_t)0x04, dfidata[row], sizeof(dfidata[0]));
                sleep(100);
                /*
                DEBUG_TRACE("\r\nSend 0x16, 0x04, ");
                for(uint8_t i=0; i < sizeof(dfidata[0]); i++)
                {
                    DEBUG_TRACE("0x%X ", dfidata[row][i]);
                }
                */
                //Calculate checksum
                uint16_t sum = 0;
                for(uint8_t col = 0; col < sizeof(dfidata[0]); col++)
                {
                    sum += dfidata[row][col];
                }
                uint16_t chkSum = 0x0A + row + sum;
                
                //Send checksum
                cmd[0] = (uint8_t)chkSum;   //LSB
                cmd[1] = chkSum >> 8;       //MSB
                HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, (uint8_t)0x64, &cmd[0], 1);
                sleep(10);
                HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, (uint8_t)0x65, &cmd[1], 1);
                sleep(100);
                //DEBUG_TRACE("\r\nSend 0x16, 0x64, 0x%X", cmd[0]);
                //DEBUG_TRACE("\r\nSend 0x16, 0x65, 0x%X", cmd[1]);
        
                //Check result
                success = HwAbsl::getpHw()->getpI2C()->receiveFrame(0x16, (uint8_t)0x66, val, ARR_ELEMS(val));
                    //DEBUG_TRACE("\r\nRecieve 0x16, 0x66, Resp=%X", val[0]);
                    
                if(!success || val[0] != 0)
                {
                    DEBUG_TRACE("\r\nError when flashing fuelgauge!\r\n");
                    success = false;
                    break;
                }
                
                HwAbsl::getpHw()->getpWatchdog()->kick();    
                
            }
            
            sleep(10);
            
            //Exit ROM mode
            cmd[0] = 0x00;
            cmd[1] = 0x0F;
            HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, cmd, ARR_ELEMS(cmd));
            //DEBUG_TRACE("\r\nSend 0x16, 0x%X, 0x%X", cmd[0], cmd[1]);
            sleep(10);
            
            //Checksum for exit ROM mode
            cmd[0] = 0x64;
            cmd[1] = 0x0F;
            HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, cmd, ARR_ELEMS(cmd));
            //DEBUG_TRACE("\r\nSend 0x16, 0x%X, 0x%X", cmd[0], cmd[1]);
            sleep(10);
            
            cmd[0] = 0x65;
            cmd[1] = 0x00;
            HwAbsl::getpHw()->getpI2C()->sendFrame(0x16, cmd, ARR_ELEMS(cmd));
            //DEBUG_TRACE("\r\nSend 0x16, 0x%X, 0x%X", cmd[0], cmd[1]);
            sleep(500);
            
            //Send reset command
            cmd[0] = 0x41;
            cmd[1] = 0x00;
            HwAbsl::getpHw()->getpI2C()->sendFrame(0xAA, (uint8_t)0, cmd, ARR_ELEMS(cmd));
            sleep(1000);
        
            //Send it enable command
            cmd[0] = 0x21;
            cmd[1] = 0x00;
            HwAbsl::getpHw()->getpI2C()->sendFrame(0xAA, (uint8_t)0, cmd, ARR_ELEMS(cmd));
            sleep(1000);
        
            //Seal device
            cmd[0] = 0x20;
            cmd[1] = 0x00;
            HwAbsl::getpHw()->getpI2C()->sendFrame(0xAA, (uint8_t)0, cmd, ARR_ELEMS(cmd));
            sleep(200);
            
            //Enable normal communication with fuelgauge
            HwAbsl::getpHw()->getpBq27510()->enableCommunication(true);
        
        }
        
    }
    
    CONSOLE_PRINT("R090 %u\r\n", success); 
}

#ifdef DEBUG_ALARM_HANDLER
    void Th1Super::debugAlarmHandler()
    {
        fhrAlarmState = m_fhrAlarmState;
        fhrAtypicalAlarmTimer = m_fhrAtypicalAlarmTimer;
        fhrAbnormalAlarmTimer = m_fhrAbnormalAlarmTimer;
        fhrAlarmRecoverTimer = m_fhrAlarmRecoverTimer;
    
        disp_criticalErrorAlarm = m_displayAlarm.alarmBits.criticalErrorAlarm;
        disp_abnormalFHRalarm = m_displayAlarm.alarmBits.abnormalFHRalarm;
        disp_lowBatteryAlarm = m_displayAlarm.alarmBits.lowBatteryAlarm;
        disp_lostSignalAlarm = m_displayAlarm.alarmBits.lostSignalAlarm;
        
        ack_criticalErrorAlarm = m_alarmAcknowlegde.alarmBits.criticalErrorAlarm;
        ack_abnormalFHRalarm = m_alarmAcknowlegde.alarmBits.abnormalFHRalarm;
        ack_lowBatteryAlarm = m_alarmAcknowlegde.alarmBits.lowBatteryAlarm;
        ack_lostSignalAlarm = m_alarmAcknowlegde.alarmBits.lostSignalAlarm;
        
        aud_criticalErrorAlarm = m_audioAlarm.alarmBits.criticalErrorAlarm;
        aud_abnormalFHRalarm = m_audioAlarm.alarmBits.abnormalFHRalarm;
        aud_lowBatteryAlarm = m_audioAlarm.alarmBits.lowBatteryAlarm;
        aud_lostSignalAlarm = m_audioAlarm.alarmBits.lostSignalAlarm;
    }

#endif
