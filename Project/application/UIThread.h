//! @class  UIThread
//! @brief
//! @author Jan Arild R�yneberg
//! @date   09.12.2013
//!

#ifndef _UI_THREAD_H_
#define _UI_THREAD_H_

#include "thread.h"
#include "includes.h"
#include "rtc.h"
#include "versioninfo.h"
#include "communicationutility.h"
#include <string>
#include "FHRMprobeSerial.h"
#include <gdisp.h>
//#include "UI/Framework/SymbolContainer.h"
#include "UI/Framework/Symbol.h"
#include "UI/Symlib/SymLib.h"
#include "UI/DisplayComponents\batterycapacity.h"
#include "UI/DisplayComponents\batterycapacityBig.h"
#include "UI/DisplayComponents\heartrate.h"
#include "UI/DisplayComponents\hrtrend.h"
#include "board_cfg.h"
#include "filethread.h"
#include "FHRMAudio.h"
#include "clock.h"
#include "ostimer.h"
#include "commondefs.h"


//class PaletteSymbol;

//! @brief Handles the file system
class UIThread:public Thread
{
    
public:
	UIThread();
	~UIThread();
    
    void run(void *);
    static UIThread* get() { return m_pUiTh; }
    POST_ctrl_t getPostResult(void);
    void clearPostResult(void);
    
    typedef struct
    {
        bool loadspeaker;
        bool smallBattIndicator;
        bool bigBattIndicator;
        bool fetalHR;
        bool maternalHR;
        bool fhrTrend;
        bool bigFhrTrend;
        bool clock;
        bool fhrLed;
        bool criticalError;
    } DisplayComponentsActive_t;
    
    
    enum DisplayMode_t
    {
        eDmFhrTrend = 0,
        eDmFhrHr,
        eDmElectrodesTouched,
        eDmBattEmpty,
        eDmCriticalError,
        eDmSystemShutdown,
        eDmbatteryCharge,
        eDmConfig,
        eDmTest
    };
    
    enum DisplayCmds_t
    {
        eDcUpdateMaternalHR = 0,
        eDcUpdateFetalHR,
        eDcUpdateFhrTrend,
        eDcUpdateSoundVolume,
        eDcUpdateClock,
        eDcUpdateBattIndicator,
        eDcUpdateChargingIndicator,
        eDcUpdateCfgDeviceIdentifier,
        eDcUpdateCfgTime,
        eDcUpdateCfgDate,
        eDcUpdateCfgEcgAmpSens,
        eDcUpdateCfgEpisodevalidTime,
        eDcUpdateCfgEndOfEpisodeTimeout,
        eDcUpdateCfgHRupdateTime,
        eDcUpdateCfgHRavgBufLen,
        eDcUpdateCfgLostSignalAlarmTime,
        eDcUpdateCfgFhrAtypicalAlarmHigh,
        eDcUpdateCfgFhrAtypicalAlarmLow,
        eDcUpdateCfgFhrAbnormalAlarmHigh,
        eDcUpdateCfgFhrAbnormalAlarmLow,
        eDcUpdateCfgFhrAtypicalAlarmTime,
        eDcUpdateCfgFhrAbnormalAlarmTime,
        eDcUpdateCfgResearchStorageMode,
        eDcUpdateCfgShowClock,
        eDcUpdateCfgEpisodeTooShortTime,
        eDcUpdateCfgSDcardFunctionality,
        eDcUpdateCfgDopplerSoundSpeakerGain,
        eDcUpdateCfgProbeSoundGain,
        eDcUpdateCfgFhrFilter               
    };
    
    enum SymBlinkState_t
    {
        eSBSStateOff = 0,
        eSBSStateOn
    };
    
    enum ViewPort_t
    {
        eVPstatus = 0,
        eVPfetalHeartRate = 1,
        eVPmaternalHeartRate = 2,
        eVPTrend = 3,
        eVPCharging = 4
            
    };
    
    typedef struct
    {
        Post_t postResult;
        bool displayFinished;
        bool probeFinished;
        bool configFinished;
        bool fuelGaugeFinished;
        bool SDcardFinished;
        bool ProgramMemChecksumFinished;
        bool VoltageRegulatorFinished;
        bool RTCcrystalFinished;
    } PostTestStatus_t;
    
    enum Shutdown_t
    {
        eSDunknown,
        eSDpowerOffBtn,
        eSDusbPowerRemoval,
        eSDusbSupplyOCProtection
    };
    
    enum BatteryEmpty_t
    {
        eBEinit,
        eBEshowSymbol,
        eBEfadeSymbol,
        eBEpowerOff,
        eBEwaitPowerOff
    };
    
    enum InfoSymbol_t
    {
        eISnone,
        eISAlarmAck,
        eISBattLevelCritical
    };
    
    
    
protected:
    static void fadeinCallback( void* pCallbackArgs );

private:
    static const uint16_t THREAD_TICK_TIME = 10;
    
    static UIThread* m_pUiTh;  //!< static member to keep track of instance. TODO: Could possibly be removed
    
    bool m_displayInterfaceOK;
    
    DisplayComponentsActive_t m_displayComponentsActive;
    //Symbol* m_LGHlogo;
    Symbol* m_volumeSymbol;
    //Symbol* m_fetus;
    Symbol* m_mother;
    Symbol* m_critcalErrorSymbol;
    Symbol* m_alarmSymbol;
    Symbol* m_UsbSymbol;
    Symbol* m_batteryEmptyBigSymbol;
    Symbol* m_batteryCriticallyLowBigSymbol;
    Symbol* m_alarmAckBigSymbol;
    BatteryCapacity* m_batteryCapacityComponent;
    BatteryCapacityBig* m_bigBatteryCapacityComponent;
    Heartrate* m_fetalHeartRateComponent;
    static const GXT FHR_TR_X_POS = 39;
    static const GYT FHR_TR_Y_POS = 22;
    static const GXT FHR_X_POS = 39;
    static const GYT FHR_Y_POS = 30;
    Heartrate* m_motherHeartRateComponent;
    //HRtrend* m_HrSmallTrendComponent;
    HRtrend* m_HrBigTrendComponent;
    Clock* m_clockComponent;
    uint8_t m_currentFetalHR;
    uint16_t m_currentMotherHR;
    uint8_t m_FhrAtypicalAlarmLow;
    uint8_t m_FhrAtypicalAlarmHigh;
    DisplayMode_t m_displayMode;
    DisplayMode_t m_ecgElectrodesTouchedPrevDisplayMode;
    SoundVolume_t m_soundVolume;
    FileThread *m_pFileTh;
    uint8_t m_batteryRemainingCapacity;
    static const uint8_t BATT_IND_BLINK_TIME = 500/THREAD_TICK_TIME;
    uint8_t m_symbolBlinkTimer;
    SymBlinkState_t m_symbolBlinkState;
    uint8_t m_symbolRemBlinks;
    static const uint8_t EMPTY_BATT_NO_BLINKS = 3;
    bool m_fhrAlarm;
    bool m_showClock;
    bool m_rsmModeEnabled; //Research storage mode
    static const uint8_t CRITICAL_ERR_BLINK_ON_TIME = 1000/THREAD_TICK_TIME;
    static const uint8_t CRITICAL_ERR_BLINK_OFF_TIME = 750/THREAD_TICK_TIME;
    static const uint8_t CRITICAL_ERR_NO_BLINKS = 3;
    PostTestStatus_t m_postCtrl; //Overall POST
    POST_ctrl_t m_POSTstatus;    //Local POST (for UI)
    bool m_criticalError;
    
    bool m_DisplayModeKeyPressed;
    bool m_AudioVolumeKeyPressed;
    bool m_OnKeyPressed;
    bool m_OffKeyPressed;
    uint16_t m_PWMdcBacklight;
    bool m_backlightDimmed;
    Alarm_t m_currentDisplayAlarm;
    Alarm_t m_currentAudioAlarm;
    Alarm_t m_alarmLogStatus;
    bool m_alarmAcknowlegde;
    OperationalMode_t m_operationalMode;
    uint16_t m_displayDimTmr;   //For automatic dimming after e.g button pushed
    static const uint16_t CHARGE_MODE_DISPLAY_DIM_TIME = 5000/THREAD_TICK_TIME;
    bool m_displayIsDimming;    //For dimming down over time
    
    static const uint16_t INFO_SYMBOL_SHOW_TIME = 2000/THREAD_TICK_TIME; //ms
    uint16_t m_infoSymbolTimer;
    InfoSymbol_t m_infoSymbolActive;
    BatteryEmpty_t m_batteryEmptyDisplayState;
    uint16_t m_battEmptyDisplayTmr;
    static const uint16_t BATT_EMPTY_SHOW_SYMBOL_TIME = 3000/THREAD_TICK_TIME; //ms
    static const uint16_t BACKLIGHT_FADE_TIME = 500; //ms
    static const uint16_t BACKLIGHT_FADE_STEP = (uint16_t)((float)(BACKLIGHT_FULL - BACKLIGHT_START) / (float)BACKLIGHT_FADE_TIME * (float)THREAD_TICK_TIME);
    static const uint16_t BACKLIGHT_DIM_TIME = 500; //ms
    static const uint16_t BACKLIGHT_DIM_STEP = (uint16_t)((float)(BACKLIGHT_FULL - BACKLIGHT_DIM) / (float)BACKLIGHT_DIM_TIME * (float)THREAD_TICK_TIME);
    uint32_t m_showLogoFinishTime;
    static const uint16_t LOGO_SHOW_TIME = 2000; //ms
        
    void initDisplay(void);
    void startupHandler(void);
    void setDisplayMode(DisplayMode_t a_mode);
    void updateDisplay(DisplayCmds_t a_cmd, uint16_t a_value);
    void updateDisplayCfgTxt(DisplayCmds_t a_cmd, uint16_t a_uiValue, float a_fvalue);
    void updateVolume();
    //void batterySymbolHandler();
    void setAllDisplayComponentsInactive(void);
    void clearFHRArea(void);
    void clearMHRArea(void);
    void clearTrendArea(void);
    void clearStatusArea(void);
    void setStatusArea(void);
    void setDisplayBatteryEmptyMode(void);
    void setDisplayMaternalHrMode(void);
    void setDisplayFhrTrendMode(void);
    void setDisplayFhrHrMode(void);
    void setDisplayCriticalErrorMode(bool a_showPostCode);
    void setDisplaySystemShutdownMode(bool a_showLogo);
    void setDisplayBatteryChargeMode(void);
    void setDisplayConfigMode(void);
    void setDisplayTestMode(void);
    void maternalImpedanceHandler(bool a_electrodesTouched);
    void FhrLedUpdate(uint8_t a_hr);
    bool displayDim(bool a_enableDim);
    void criticalPostErrorHandler(bool a_systemShutdown);
    bool isCriticalError(Post_t a_postResult);
    void setDisplayAlarm(Alarm_t a_alarm, bool a_isAcknowledged);
    void clearDisplayAlarm(Alarm_t a_alarm);
    void setAudioAlarm(Alarm_t a_alarm);
    void clearAudioAlarm(Alarm_t a_alarm);
    void showPostCode(Post_t a_postResult);
    bool post(bool a_verbose);
    void batteryEmptyHandler(void);
    void backlightDimCtrl(void);
    void buttonHandler(void);
    void showAlarmSymbol(bool a_acked);
    void clearAlarmSymbol(void);
    void showVolumeSymbol(uint8_t a_volume);
    void showUSBSymbol(void);
    void showBatteryLowSymbol(void);
    void showInfoSymbol(InfoSymbol_t a_infoSymbol);
    void clearInfoSymbol(void);
    void infoSymbolHandler(void);
    void setFullVolume(void);
    void displayDimHandler(void);
    void fadeBacklightOut(bool a_powerOffAfterwards);
    void fadeBacklightIn(void);
    void showLGHlogo(void);
    void displayTest(bool a_enDis);
    void clearScreen(void);
    void logAlarm(Alarm_t a_alarm, bool a_alarmOn);
    
#ifdef DEBUG_DISPLAY_ALIGNMENT
    void drawHorLine(GXT a_xStart, GXT a_xEnd);
    void drawVertLine(GYT a_yStart, GYT a_yEnd);
#endif
    
#ifdef DEBUG_ALARM_HANDLER
    void logAlarmState();
#endif

};

#endif
