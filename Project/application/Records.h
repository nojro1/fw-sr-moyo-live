//! @class  Records
//! @brief
//! @author Jan Arild R�yneberg
//! @date   09.12.2013
//!

#ifndef _RECORDS_H_
#define _RECORDS_H_

#include "includes.h"


class records
{
    
public:
	records();
	~records();
    
    typedef struct
    {
        uint32_t timestamp;
        int16_t  ecgSampleNoneFilt;
        int16_t  ecgSample50HzFilt;
    } EcgRecord_t;
    
    typedef struct
    {
        uint32_t timestamp;
        int16_t  ecgSampleNoneFilt;
        int16_t  ecgSample50HzFilt;
        int16_t  accX;
        int16_t  accY;
        int16_t  accZ;
    } EcgQRSdetRecord_t;
	
protected:

private:
    records(const records& right);            //!< block the copy constructor
    records& operator=(const records& right); //!< block the assignment operator
};

#endif
