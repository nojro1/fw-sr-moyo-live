//! \file    SampleBuffer.h
//! \author  Jan A. R�yneberg
//! \version 1.0
//! \date    02.04.2014

#ifndef _FHRBUFFER_H
#define _FHRBUFFER_H

// header includes
//#include <cstdint>
#include <stdint.h>

class FhrBuffer
{
public:

    // constructor/destructor
    explicit FhrBuffer();
    virtual ~FhrBuffer();
    
    typedef struct
    {
        uint8_t hr;
        bool alarm;
    } HrInfo_t;
    
    void Create(uint16_t size);
    bool Put(HrInfo_t item);
    bool GetLast(HrInfo_t *item);
    bool GetNext(HrInfo_t *item);
    
protected:
    
private:
    FhrBuffer(const FhrBuffer& right);            //!< block the copy constructor
    FhrBuffer& operator=(const FhrBuffer& right); //!< block the assignment operator
    
    HrInfo_t *m_pLast;
    HrInfo_t *m_pFirst;
    HrInfo_t *m_pPut;
    HrInfo_t *m_pCurr;
    HrInfo_t *m_pEnd;
    HrInfo_t *m_pBuffer;
    bool    m_bufferEmpty;
        
};


#endif   // _FHRBUFFER_H

//======== End of FhrBuffer.h ==========

