//! \file    SampleBuffer.h
//! \author  Jan A. R�yneberg
//! \version 1.0
//! \date    05.11.2013

#ifndef _RINGBUFFER_H
#define _RINGBUFFER_H

// header includes
//#include <cstdint>
#include <stdint.h>
class RingBuffer
{
public:

    // constructor/destructor
    explicit RingBuffer();
    virtual ~RingBuffer();
    
    void Create(uint16_t size);
    bool Put(uint8_t item);
    bool Put(int16_t item);
    bool Put(uint8_t *items, uint16_t noItems);
    bool Get(uint8_t *item);
    bool Get(int16_t *item);
    bool Get(int16_t *item, int16_t offset);
    bool Get(uint8_t *items, uint16_t noItems);
    uint16_t Peek(void);
    uint16_t GetNumItems(void);
    uint16_t GetRoom(void);
    
protected:
    
private:
    RingBuffer(const RingBuffer& right);            //!< block the copy constructor
    RingBuffer& operator=(const RingBuffer& right); //!< block the assignment operator
    
    uint8_t *m_pGet;
    uint8_t *m_pPut;
    uint8_t *m_pEnd;
    uint16_t m_numItems;
    uint16_t m_room;
    uint8_t *m_pBuffer;
        
};


#endif   // _RINGBUFFER_H

//======== End of RingBuffer.h ==========

