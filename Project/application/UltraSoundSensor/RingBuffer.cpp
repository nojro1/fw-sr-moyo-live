//! \file    RingBuffer.cpp
//! \author  Jan A. R�yneberg
//! \version 1.0
//! \date    05.11.2013


// include self first
#include "ringbuffer.h"

// other includes
#include "RTOS.h"
#include <string.h>  /* memcpy */

//! \brief Constructor
RingBuffer::RingBuffer() : m_pBuffer(0), m_pGet(0), m_pPut(0), m_pEnd(0), m_numItems(0), m_room(0)
{
}


//! \brief Destructor
RingBuffer::~RingBuffer()
{
    delete[] m_pBuffer;
}

//! \brief Create the ringbuffer
void RingBuffer::Create(uint16_t sizeInBytes)
{
    //OS_EnterRegion();
    
    m_pBuffer = new uint8_t[sizeInBytes];
    memset(m_pBuffer, 0, sizeInBytes);
    m_pGet = m_pBuffer;
    m_pPut = m_pBuffer;
    m_pEnd = m_pBuffer + sizeInBytes;
    m_numItems = 0;
    m_room = sizeInBytes;
    
    //OS_LeaveRegion();
}

//! \brief Put an uint8 in ringbuffer
bool RingBuffer::Put(uint8_t item)
{
    if(m_room)
    {
        if(m_pPut >= m_pEnd)
        {
            m_pPut = m_pBuffer;
        }
        
        *m_pPut = item;
        m_pPut++;
        
        //OS_EnterRegion();
        m_room--;
        m_numItems++;
        //OS_LeaveRegion();
        
    }
    else
    {
        return false;
    }
    
    return true;
}

//! \brief Put an int16 in ringbuffer
bool RingBuffer::Put(int16_t item)
{
    if(m_room >= 2)
    {
        if(m_pPut >= m_pEnd)
        {
            m_pPut = m_pBuffer;
        }
        
        *m_pPut = (uint8_t)item;
        m_pPut++;
        
        if(m_pPut >= m_pEnd)
        {
            m_pPut = m_pBuffer;
        }
        
        *m_pPut = item >> 8;
        m_pPut++;
        
        //OS_EnterRegion();
        m_room-=2;
        m_numItems+=2;
        //OS_LeaveRegion();
    }
    else
    {
        return false;
    }
    
    return true;
}


//! \brief Put uint8(s) in ringbuffer
bool RingBuffer::Put(uint8_t *items, uint16_t noItems)
{
    if(m_room >= noItems)
    {
        if((m_pPut + noItems) > m_pEnd)
        {
            uint8_t len = m_pEnd - m_pPut;
            uint8_t *smp = items;
    
            memcpy(m_pPut, smp, len);
            smp += len;
            
            len =  noItems - len;
            memcpy(m_pBuffer, smp, len);
            
            m_pPut = m_pBuffer + len;
        }
        else
        {
            memcpy(m_pPut, items, noItems);
            m_pPut += noItems;
        }
        
        //OS_EnterRegion();
        m_room -= noItems;
        m_numItems += noItems;
        //OS_LeaveRegion();
    }
    else
    {
        return false;
    }
    
    return true;
}

//! \brief Get an uint8 from ringbuffer
bool RingBuffer::Get(uint8_t *item)
{
    if(m_numItems)
    {
        if(m_pGet >= m_pEnd)
        {
            m_pGet = m_pBuffer;
        }
        
        *item = *m_pGet;
        
        //OS_EnterRegion();
        m_numItems--;
        m_room++;
        m_pGet++;
        //OS_LeaveRegion();
    }
    else 
    {
        return false;
    }
    
    return true;
}

//! \brief Get an int16 from ringbuffer
bool RingBuffer::Get(int16_t *item)
{
    uint16_t smp = 0;
    
    if(m_numItems > 1)
    {
        if(m_pGet >= m_pEnd)
        {
            m_pGet = m_pBuffer;
        }
        
        smp = (*m_pGet++);
        
        if(m_pGet >= m_pEnd)
        {
            m_pGet = m_pBuffer;
        }
        
        smp |= ((*m_pGet++) << 8);
        
        //OS_EnterRegion();
        m_numItems-=2;
        m_room+=2;
        //OS_LeaveRegion();
        
    }
    else 
    {
        *item = 0;
        return false;
    }
    
    *item = (int16_t)smp;
    return true;
}

//! \brief Get an int16 from ringbuffer. Offset value before return it
bool RingBuffer::Get(int16_t *item, int16_t offset)
{
    uint16_t smp = 0;
    
    if(m_numItems > 1)
    {
        if(m_pGet >= m_pEnd)
        {
            m_pGet = m_pBuffer;
        }
        
        smp = *m_pGet++;
        smp = smp << 8;
        
        if(m_pGet >= m_pEnd)
        {
            m_pGet = m_pBuffer;
        }
        
        smp += *m_pGet++;
        
        //OS_EnterRegion();
        m_numItems-=2;
        m_room+=2;
        //OS_LeaveRegion();
    }
    else 
    {
        *item = 0;
        return false;
    }
    
    *item = (int16_t)smp + offset;
    return true;
}

//! \brief Get uint8(s) from ringbuffer
bool RingBuffer::Get(uint8_t *items, uint16_t noItems)
{
    if(m_numItems >= noItems)
    {
        if((m_pGet + noItems) >= m_pEnd)
        {
            uint16_t len = m_pEnd - m_pGet;
            uint8_t *smp = items;
            memcpy(smp, m_pGet, len);
            
            smp += len;
            len =  noItems - len;
            memcpy(smp, m_pBuffer, len);
            
            m_pGet = m_pBuffer + len;
        }
        else
        {
            memcpy(items, m_pGet, noItems);
            m_pGet += noItems;
            
        }
        
        //OS_EnterRegion();
        m_room += noItems;
        m_numItems -= noItems;
        //OS_LeaveRegion();
    }
    else
    {
        return false;
    }
    
    return true;
}

//! \brief Get latest value without incrementing pointers
uint16_t RingBuffer::Peek(void)
{
    uint16_t item = 0;
    
    if(m_numItems >= sizeof(uint16_t))
    {
        if(m_pGet >= m_pEnd)
        {
            m_pGet = m_pBuffer;
        }
        
        if(m_pGet == m_pBuffer)
        {
            //Byte values are across boundaries
            item = *m_pGet;
            item |= (*m_pEnd) << 8;
        }
        else
        {
            item = *m_pGet;
            item |= *(m_pGet-1) << 8;
           
        }
    }
    
    return item;
}

//! \brief Get num bytes (uint8) in ringbuffer
uint16_t RingBuffer::GetNumItems(void)
{
    return m_numItems;
}

//! \brief space in bytes (uint8) in ringbuffer
uint16_t RingBuffer::GetRoom(void)
{
    return m_room;
}

