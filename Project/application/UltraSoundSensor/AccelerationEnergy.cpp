//! \file    AccelerationEnergy.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    09.10.2014

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  09oct2014, JRo, JEv Original.

//!   Calculate acceleration energy from x,y and z axis on an accelerometer
#include "accelerationenergy.h"

// Acceleration energy constants:
const float AccelerationEnergy::ACC_AVE_FF          = 0.98;  // Acceleration average value forgetting factor
const float AccelerationEnergy::ACC_ENRG_FF         = 0.99;  // Acceleration energy forgetting factor
const float AccelerationEnergy::ACC_ENRG_FF_RAPID   = 0.95;  // Rapid acceleration energy forgetting factor
const uint16_t AccelerationEnergy::ACC_TH_RAPID     = 50;    // Acceleration energy where rapid forgetting factor kicks in

//
//! Constructor
//
AccelerationEnergy::AccelerationEnergy()
{
    Init();
}

//
//! Destructor
//
AccelerationEnergy::~AccelerationEnergy()
{
    
}

//
//! Init energy calculation
//
void AccelerationEnergy::Init(void)
{
   // Acceleration energy inits:
   acc_ave = 1000.0;      // FLOAT Average acceleration energy
   acc_energy = 0.0;      // FLOAT Acceleration energy (using all axes)
   initialised = false;

}

//
//! Calculate energy from one sample set (x,y,z acceleration data)
//
int16_t AccelerationEnergy::CalcAccEnergy(int16_t a_accX, int16_t a_accY, int16_t a_accZ)
{

    float cur_acc_x_mg;
    float cur_acc_y_mg;
    float cur_acc_z_mg;
    float acc_abs;
    float ff;
    
    if(initialised)
    {
        // Acceleration energy processing
        cur_acc_x_mg = float(a_accX);
        cur_acc_y_mg = float(a_accY);
        cur_acc_z_mg = float(a_accZ);
          
        // Average and energy of acc. signal:
        acc_abs = sqrt((cur_acc_x_mg*cur_acc_x_mg)+(cur_acc_y_mg*cur_acc_y_mg)+(cur_acc_z_mg*cur_acc_z_mg));
        acc_ave = ACC_AVE_FF*acc_ave + (1.0-ACC_AVE_FF)*acc_abs;
        if(acc_energy > ACC_TH_RAPID)
        {
           ff = ACC_ENRG_FF_RAPID;
        }
        else
        {
         ff = ACC_ENRG_FF;
        }
        acc_energy = ff*acc_energy + (1.0-ff)*abs(acc_abs-acc_ave);
    }
    else
    {
        if(cur_acc_x_mg == 0 && cur_acc_y_mg == 0 && cur_acc_z_mg == 0)
        {
            initialised = false;
        }
        else
        {
            initialised = true;
        }
           
        acc_energy = 0.0;
    }
    return (int16_t)round(acc_energy);

}
