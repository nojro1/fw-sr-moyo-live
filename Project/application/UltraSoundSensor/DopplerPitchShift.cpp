#include "dopplerpitchshift.h"

const float DopplerPitchShift::m_IIR_DCfilterNumCoeffs[ DopplerPitchShift::IIR_DC_FILTER_LEN ] = 
{
    0.80047023156387842000f, -3.96379532752399430000f, 7.88903482475259120000f,
    -7.88903482475259120000f, 3.96379532752399430000f, -0.80047023156387842000f
};
const float DopplerPitchShift::m_IIR_DCfilterDenomCoeffs[ DopplerPitchShift::IIR_DC_FILTER_LEN ] = 
{
    1.00000000000000000000f, -4.50858220497269270000f, 8.19659027197570640000f, 
    -7.50376232167766940000f, 3.45691763637674350000f, -0.64074833267812092000f
};
const float DopplerPitchShift::m_IIR_MixedFilterNumCoeffs[ DopplerPitchShift::IIR_MIXED_FILTER_LEN ] = 
{
    0.99890164982769003000f, -0.99890164982769003000f
};
const float DopplerPitchShift::m_IIR_MixedFilterDenomCoeffs[ DopplerPitchShift::IIR_MIXED_FILTER_LEN ] = 
{
    1.00000000000000000000f, -0.99780329965538006000f
};

const float DopplerPitchShift::m_sinConsts[ DopplerPitchShift::SIN_CONSTS_LEN ] =
{
    0.218143, 
    0.425779, 
    0.612907,
    0.770513,
    0.891007,
    0.968583,
    0.999507,
    0.982287,
    0.917755,
    0.809017,
    0.661312,
    0.481754,
    0.278991,
    0.062791,
    -0.156434,
    -0.368125,
    -0.562083,
    -0.728969,
    -0.860742,
    -0.951057,
    -0.995562,
    -0.992115,
    -0.940881,
    -0.844328,
    -0.707107,
    -0.535827,
    -0.338738,
    -0.125333,
    0.094108,
    0.309017,
    0.509041,
    0.684547,
    0.827081,
    0.929776,
    0.987688,
    0.998027,
    0.960294,
    0.876307,
    0.750111,
    0.587785,
    0.397148,
    0.187381,
    -0.031411,
    -0.248690,
    -0.453990,
    -0.637424,
    -0.790155,
    -0.904827,
    -0.975917,
    -1.000000,
    -0.975917,
    -0.904827,
    -0.790155,
    -0.637424,
    -0.453990,
    -0.248690,
    -0.031411,
    0.187381,
    0.397148,
    0.587785,
    0.750111,
    0.876307,
    0.960294,
    0.998027,
    0.987688,
    0.929776,
    0.827081,
    0.684547,
    0.509041,
    0.309017,
    0.094108,
    -0.125333,
    -0.338738,
    -0.535827,
    -0.707107,
    -0.844328,
    -0.940881,
    -0.992115,
    -0.995562,
    -0.951057,
    -0.860742,
    -0.728969,
    -0.562083,
    -0.368125,
    -0.156434,
    0.062791,
    0.278991,
    0.481754,
    0.661312,
    0.809017,
    0.917755,
    0.982287,
    0.999507,
    0.968583,
    0.891007,
    0.770513,
    0.612907,
    0.425779,
    0.218143,
    0.000000,
    -0.218143,
    -0.425779,
    -0.612907,
    -0.770513,
    -0.891007,
    -0.968583,
    -0.999507,
    -0.982287,
    -0.917755,
    -0.809017,
    -0.661312,
    -0.481754,
    -0.278991,
    -0.062791,
    0.156434,
    0.368125,
    0.562083,
    0.728969,
    0.860742,
    0.951057,
    0.995562,
    0.992115,
    0.940881,
    0.844328,
    0.707107,
    0.535827,
    0.338738,
    0.125333,
    -0.094108,
    -0.309017,
    -0.509041,
    -0.684547,
    -0.827081,
    -0.929776,
    -0.987688,
    -0.998027,
    -0.960294,
    -0.876307,
    -0.750111,
    -0.587785,
    -0.397148,
    -0.187381,
    0.031411,
    0.248690,
    0.453990,
    0.637424,
    0.790155,
    0.904827,
    0.975917,
    1.000000,
    0.975917,
    0.904827,
    0.790155,
    0.637424,
    0.453990,
    0.248690,
    0.031411,
    -0.187381,
    -0.397148,
    -0.587785,
    -0.750111,
    -0.876307,
    -0.960294,
    -0.998027,
    -0.987688,
    -0.929776,
    -0.827081,
    -0.684547,
    -0.509041,
    -0.309017,
    -0.094108,
    0.125333,
    0.338738,
    0.535827,
    0.707107,
    0.844328,
    0.940881,
    0.992115,
    0.995562,
    0.951057,
    0.860742,
    0.728969,
    0.562083,
    0.368125,
    0.156434,
    -0.062791,
    -0.278991,
    -0.481754,
    -0.661312,
    -0.809017,
    -0.917755,
    -0.982287,
    -0.999507,
    -0.968583,
    -0.891007,
    -0.770513,
    -0.612907,
    -0.425779,
    -0.218143,
    -0.000000

};

const float DopplerPitchShift::pi = 3.1415926536f; 
const float DopplerPitchShift::sinconst = 2.0f * pi * DopplerPitchShift::CARRIER / DopplerPitchShift::FS; // "constant" factor in sinus calculation (float)
const float DopplerPitchShift::FF = 0.98f;                    // Forgetting factor for energy calculation
const float DopplerPitchShift::ENRG_TH = 0.008f * 2048.0f;//0.012f * 2048.0f;              // Energy threshold before setting amplitude to zero [normalized units (-1 to 1)]


//Constructor
DopplerPitchShift::DopplerPitchShift()
{
    m_IIRDCfilterHistory = new float[DopplerPitchShift::IIR_DC_FILTER_LEN];
    m_IIRMFfilterHistory = new float[DopplerPitchShift::IIR_MIXED_FILTER_LEN];
    memset(m_IIRDCfilterHistory, 0, DopplerPitchShift::IIR_DC_FILTER_LEN * sizeof(float));
    memset(m_IIRMFfilterHistory, 0, DopplerPitchShift::IIR_MIXED_FILTER_LEN * sizeof(float));
    
    n = 0;
}

//Destruction
DopplerPitchShift::~DopplerPitchShift()
{
    delete[] m_IIRDCfilterHistory;
    delete[] m_IIRMFfilterHistory;
}

void DopplerPitchShift::PitchShift(uint8_t *inputBuffer, uint8_t *outputBuffer, uint16_t noSamples)
{
    uint16_t i;
    //uint16_t smp;
    uint16_t *pInpBuf = (uint16_t *)inputBuffer;
    uint16_t *pOutpBuf = (uint16_t *)outputBuffer;
    float smp_f;
    //float s;
    float y_cur;
    float x_noDC;
    static float enrg = 0.0;
    
    for(i = 0; i < noSamples; i +=2)
    {
        smp_f = (float)(*pInpBuf++);
        
        
        RunIIRPoly( &smp_f, &x_noDC, 1, m_IIRDCfilterHistory, IIR_DC_FILTER_LEN, 
                    m_IIR_DCfilterDenomCoeffs, m_IIR_DCfilterNumCoeffs); //remove DC
        
        
        if(n >= SIN_CONSTS_LEN)
        {
            n = 0;
        }
        
        //s = m_sinConsts[n];
        
        //s = sin(sinconst*n);    // sinus signal
        
        //y_cur = GAIN*s*x_noDC;  // mixed signal
        y_cur = x_noDC * 2;  // mixed signal
   
        // Filter mixed signal:
        /*
        RunIIRPoly( &y_cur, &y_cur, 1, m_IIRMFfilterHistory, IIR_MIXED_FILTER_LEN,
                    m_IIR_MixedFilterDenomCoeffs, m_IIR_MixedFilterNumCoeffs); // remove carrier and low-freq. components
        */
   
        //limit to resolution max amplitude
        if(y_cur < -2048.0f) y_cur = -2048.0f;
        else if(y_cur > 2047.0f) y_cur = 2047.0f;
   
        // Noise gate logic:
        enrg = FF*enrg + (1-FF)*abs(x_noDC); // energy
        if(enrg < ENRG_TH)
        {
            y_cur = 0.0f;
        }
        
        y_cur += 2048.0f;
        *pOutpBuf++ = (uint16_t)y_cur;
        
        n++;
        
    }
}

// IIR filter one sample
void DopplerPitchShift::RunIIRPoly( float *Signal, float *FilteredSignal, uint8_t NumSigPts, 
                                    float *History, uint8_t HistoryLen, 
                                    const float *DenomCoeff, const float *NumCoeff )
{
    uint8_t j, k;
    uint8_t N = HistoryLen - 1;
    float y;//, Reg[100];
    

    //for(j=0; j<100; j++)Reg[j] = 0.0; // Init the delay registers.

    for(j=0; j<NumSigPts; j++)
    {
        // Shift the delay register values.
        for(k=N; k>0; k--)
        {
            History[k] = History[k-1];
        }

        // The denominator
        History[0] = Signal[j];
        for(k=1; k<=N; k++)
        {
            History[0] -= (float)DenomCoeff[k] * (float)History[k];
        }

        // The numerator
        y = 0;
        for(k=0; k<=N; k++)
        {
            y += (float)NumCoeff[k] * (float)History[k];
        }
        
        FilteredSignal[j] = y;
    }

}

