//! @class
//! @brief
//! @author Joar Eilevstj�nn / Jan A. R�yneberg
//! @date   29.09.2014
//!

#ifndef _ACC_ENERGY_H_
#define _ACC_ENERGY_H_

#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include "includes.h"

    class AccelerationEnergy
    {

        public:
            static const float ACC_AVE_FF;          // Acceleration average value forgetting factor
            static const float ACC_ENRG_FF;         // Acceleration energy forgetting factor
            static const float ACC_ENRG_FF_RAPID;   // Rapid acceleration energy forgetting factor
            static const uint16_t ACC_TH_RAPID;     // Acceleration energy where rapid forgetting factor kicks in
            
            AccelerationEnergy();
            ~AccelerationEnergy();
            
            int16_t CalcAccEnergy(int16_t a_accX, int16_t a_accY, int16_t a_accZ);
            
        private:
            AccelerationEnergy(const AccelerationEnergy& right);            //!< block the copy constructor
            AccelerationEnergy& operator=(const AccelerationEnergy& right); //!< block the assignment operator
            
            void Init(void);
            
            float acc_ave;      // FLOAT Average acceleration energy
            float acc_energy;   // FLOAT Acceleration energy (using all axes)
            bool initialised;
   
            
    };

#endif //_ACC_ENERGY_H_

