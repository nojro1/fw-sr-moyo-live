

#define _COBS_C

#include "COBS.h"
#include "fhrmprobeserial.h"


//! cobsEncode
//! @brief  COBS-encode a message
//! @param  src     Source buffer
//! @param  dst     Destination buffer
//! @return true if encoding was possible, else false
//! @author Geir I. Tellnes
//! @date   16.05.2006
//! @remark
//! @pre
//! @post
bool StuffData(const std::vector<uint8_t>& src, std::vector<uint8_t>& dst)
{
    if (!src.empty() && !dst.empty())
    {
       uint8_t code = 0x01;
       dst.push_back(code); // make space for the first COBS code
       size_t code_idx = dst.size() - 1;
        for (size_t i = 0; i < src.size(); i++)
       {
           if (0 == src[i])
          {
               // Insert COBS code
               dst[code_idx] = code;
               code = 0x01;
               dst.push_back(code); // make space for the next COBS code
               code_idx = dst.size() - 1;
           }
           else
           {
               dst.push_back(src[i]);
               code++;
               if (0xFF==code)
               {
                   // Insert COBS code
                   dst[code_idx] = code;
                   code = 0x01;
                   dst.push_back(code); // make space for the next COBS code
                   code_idx = dst.size() - 1;
               }
           }
       }
       // Insert final COBS code
       dst[code_idx] = code;
        return true;
    }
    return false;
}

 /******************************************************************* 
  * Name:	 StuffData
  *
  * Purpose: COBS encode a message
  * 		 Performs byte stuffing of "length" bytes of
  * 		 data at the location pointed to by "src",
  * 		 writing the output to the location pointed
  * 		 to by "dst".
  *
  * Input Parameters: see purpose
  * Permitted range: it is up to the caller to allocate sufficient
  *                  memory for the buffers.
  *					 length: 1..MAX_DATA_PACKET_SIZE
  *
  * Return Parameter: none
  *
  */
 bool StuffData(uint8_t *src, uint16_t length, uint8_t *dst)
 {
	#define FinishBlock(X)	\
		  (*code_ptr = (X),  \
		  code_ptr = dst++,  \
		  code = 0x01)

	uint8_t *end = src + length;
	uint8_t *code_ptr = dst++;
	uint8_t code = 0x01;

	if (length > 0 && length <= MAX_DATA_PACKET_SIZE)
	{
		while (src < end)
		{
			if (0 == *src) FinishBlock(code);
			else
			{
				*dst++ = *src;
				code++;
				if (0xFF==code) FinishBlock(code);
			}
			src++;
		}
		FinishBlock(code);
		return true;
	}
	return true;
 }


 /**************************************************************** UnStuffData
  * Name:	 UnStuffData
  *
  * Purpose: COBS decoding
  *    		 Decodes "length" bytes of data at the location pointed to by "src",
  *    		 writing the output to the location pointed to by "dst".
  *
  * Input Parameters: see purpose
  * Permitted range: it is up to the caller to allocate sufficient
  *                  memory for the buffers.
  *					 length: (MSG_OVERHEAD+1)..MAX_DATA_PACKET_SIZE
  *					 dstLen: (length-MSG_OVERHEAD)..MAX_DATA_PACKET_SIZE
  *
  * Return Parameter: TRUE if successful decoding, FALSE if overflow condition
  *
  */

bool UnStuffData(uint8_t *src, uint16_t length, uint8_t *dst, uint16_t dstLen)
{
	uint8_t cnt = 0;
	uint8_t *end = src + length;

	if (length > COBS_OVERHEAD && length <= MAX_DATA_PACKET_SIZE &&
		dstLen >= (length-COBS_OVERHEAD) && dstLen <= MAX_DATA_PACKET_SIZE)
	{
		while (src < end)
		{
			uint16_t i;
			uint16_t code = *src++;
			for (i=1; i<code; i++)
			{
				if (++cnt > dstLen) return false; // protect destination buffer
				*dst++ = *src++;
			}
			if (code < 0xFF)
			{
				if (++cnt <= dstLen)  		// we do not need the phantom zero
					*dst++ = 0;
			}
		}
		return true;
	}
	return false;
}

