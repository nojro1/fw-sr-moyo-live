//! @class
//! @brief
//! @author Joar Eilevstj�nn / Jan A. R�yneberg
//! @date   25.06.2014
//!

#ifndef _DOPPLER_PITCH_SHIFT_H_
#define _DOPPLER_PITCH_SHIFT_H_

#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include "includes.h"

    class DopplerPitchShift
    {

        public:
            static const float pi;
            static const uint8_t CARRIER = 140; // Carrier frequency [Hz]
            static const uint16_t FS = 4000;    // Sample rate [Hz] 
            static const float sinconst; //"constant" factor in sinus calculation (float)
            static const uint8_t GAIN = 5;     // Make-up gain (=1.8 would be more appropriate to not clip)
            static const float FF;       // Forgetting factor for energy calculation
            static const float ENRG_TH;    // Energy threshold before setting amplitude to zero [normalized units (-1 to 1)]
            
            
            DopplerPitchShift();
            ~DopplerPitchShift();
            
            void PitchShift(uint8_t *inputBuffer, uint8_t *outputBuffer, uint16_t noSamples);
            
        private:
            DopplerPitchShift(const DopplerPitchShift& right);            //!< block the copy constructor
            DopplerPitchShift& operator=(const DopplerPitchShift& right); //!< block the assignment operator
            
            void RunIIRPoly( float *Signal, float *FilteredSignal, uint8_t NumSigPts, 
                                    float *History, uint8_t HistoryLen, 
                                    const float *DenomCoeff, const float *NumCoeff );
            
            // Filter coeffs
            static const uint8_t IIR_DC_FILTER_LEN = 6;
            static const uint8_t IIR_MIXED_FILTER_LEN = 2;
            static const float m_IIR_DCfilterNumCoeffs[ IIR_DC_FILTER_LEN ];
            static const float m_IIR_DCfilterDenomCoeffs[ IIR_DC_FILTER_LEN ];
            static const float m_IIR_MixedFilterNumCoeffs[ IIR_MIXED_FILTER_LEN ];
            static const float m_IIR_MixedFilterDenomCoeffs[ IIR_MIXED_FILTER_LEN ];
            static const uint8_t SIN_CONSTS_LEN = 200;
            static const float m_sinConsts[ SIN_CONSTS_LEN ];
            
            // Array to hold IIR filter history samples
            float *m_IIRDCfilterHistory;
            float *m_IIRMFfilterHistory;
            
            uint8_t n;                  // Iterator. //TODO: Check what happen when rollover
            
    };

#endif //_DOPPLER_PITCH_SHIFT_H_

