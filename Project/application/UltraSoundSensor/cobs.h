
#ifndef _COBS_H
#define _COBS_H

#include <cstdint>
#include <vector>

bool StuffData(const std::vector<uint8_t>& src, std::vector<uint8_t>& dst);
bool StuffData(uint8_t *src, uint16_t length, uint8_t *dst);
bool UnStuffData(uint8_t *src, uint16_t length, uint8_t *dst, uint16_t dstLen);

#endif

