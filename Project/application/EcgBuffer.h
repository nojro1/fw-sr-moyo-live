//! \file    EcgBuffer.h
//! \author  Jan A. R�yneberg
//! \version 1.0
//! \date    21.03.2014

#ifndef _ECG_BUFFER_H
#define _ECG_BUFFER_H

// header includes
//#include <cstdint>
#include <stdint.h>

class EcgBuffer
{
public:

    // constructor/destructor
    explicit EcgBuffer();
    virtual ~EcgBuffer();
    
    typedef struct
    {
        uint32_t TimeStamp;
        int16_t Sample;
        int16_t Sample50HzFiltered;
        
    } ECGrecord_t;
    
    void Create(uint16_t size);
    bool Put(EcgBuffer::ECGrecord_t item);
    EcgBuffer::ECGrecord_t Get(void);
    uint16_t GetNumItems(void);
    uint16_t GetRoom(void);
    
protected:
    
private:
    EcgBuffer(const EcgBuffer& right);            //!< block the copy constructor
    EcgBuffer& operator=(const EcgBuffer& right); //!< block the assignment operator
    
    EcgBuffer::ECGrecord_t *m_pGet;
    EcgBuffer::ECGrecord_t *m_pPut;
    EcgBuffer::ECGrecord_t *m_pEnd;
    uint16_t m_numItems;
    uint16_t m_room;
    EcgBuffer::ECGrecord_t *m_pBuffer;
        
};


#endif   // _ECG_BUFFER_H

//======== End of EcgBuffer.h ==========

