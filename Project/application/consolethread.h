//! @class  ConsoleThread
//! @brief
//! @author Geir Inge Tellnes
//! @date   11.02.2010
//!
#ifndef _CONSOLETHREAD_H_
#define _CONSOLETHREAD_H_

#include "thread.h"

class ConsoleManager;

//! @brief Receives commands from the console UART and forwards them
//! to the ConsoleManager for parsing
class ConsoleThread : public Thread
{

public:
    ConsoleThread();                            // ctor
    ~ConsoleThread();                           // dtor
    void sendIPC(THREAD_ID,THREAD_SIGNAL,uint32_t,uint32_t,void *);

protected:
    void run(void *);

private:
    ConsoleThread(const ConsoleThread& right);            // block the copy constructor
    ConsoleThread& operator=(const ConsoleThread& right); // block the assignment operator

    ConsoleManager *m_pManager;

};


#endif //_CONSOLETHREAD_H_

