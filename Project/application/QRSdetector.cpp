//! \file    QRSdetector.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    17.03.2014

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  17Mar2014, JRo, Original.
//    28.01.2015, JRo, Updated due to Minor adjustments to improve performance

#include "QRSdetector.h"

#include "utilfuncs.h"

const float QrsDetector::m_LpFIRcoeffs[ QrsDetector::LP_FIR_FILTER_LEN ] = 
{
     0.00193043840112783800,  0.00145251000469485410, -0.00173933167948064580, -0.00621508463291966990, -0.00446493730208132910,
     0.00879474384594456160,  0.02171464201926346900,  0.00974010231496434500, -0.03158150000696256100, -0.06051125578044372300,
    -0.01464561243690019400,  0.12170517706868898000,  0.27920681313465512000,  0.34922659009889800000,  0.27920681313465512000,
     0.12170517706868898000, -0.01464561243690019400, -0.06051125578044372300, -0.03158150000696256100,  0.00974010231496434500,
     0.02171464201926346900,  0.00879474384594456160, -0.00446493730208132910, -0.00621508463291966990, -0.00173933167948064580,
     0.00145251000469485410,  0.00193043840112783800
};

const float QrsDetector::m_HpFIRcoeffs[ QrsDetector::HP_FIR_FILTER_LEN ] = 
{
    -0.00171327874759356990, -0.00119086180705004280,  0.00024063877215863276,  0.00365427827951552920,  0.00914709190935163000,
     0.01470208404555718500,  0.01603162218525047400,  0.00784106699335682810, -0.01380594057717991100, -0.04903947723680512200,
    -0.09291222606851104500, -0.13616104281416000000, -0.16795983500481143000,  0.81844405001650056000, -0.16795983500481143000,
    -0.13616104281416000000, -0.09291222606851104500, -0.04903947723680512200, -0.01380594057717991100,  0.00784106699335682810,
     0.01603162218525047400,  0.01470208404555718500,  0.00914709190935163000,  0.00365427827951552920,  0.00024063877215863276,
    -0.00119086180705004280, -0.00171327874759356990
};


//DEFAULT VALUES:
const float QrsDetector::max_x_val       = 2047.0;          // ADC value corresponding to ca. 9mV
const float QrsDetector::CLIP_VOLTAGE    = 9.0;             // Approximate hardware clipping voltage [mV]

// CONSTANTS K�HLER ALGORITHM:
const float QrsDetector::LAMBDA_K        = 0.98;		    // FLOAT forgetting factor K (amplitude of high-frequency sequence)
const float QrsDetector::GAIN_K          = 6.0;	            // FLOAT constant gain used in K calculation
const float QrsDetector::LAMBDA_D        = 0.9;			    // FLOAT forgetting factor D (number of zero crossings per segment)
const float QrsDetector::LAMBDA_ATH      = 0.99;			// FLOAT forgetting factor adaptive detection threshold
const int16_t QrsDetector::REFRAC_PERIOD = 0.1*fs;          // INT refractory period after last detection [samples]
const int16_t QrsDetector::DELAY         = 26;              // INT Delay between x and y [samples]

// ADD'L CONSTANTS:
const float QrsDetector::MAX_ADAP_TH     = 0.9;			    // FLOAT Maximum value of adap_th (to help specificity)

// Signal disable LOGIC:
const float QrsDetector::ABSX_TH         = 0.67*max_x_val;  // FLOAT Signal absolute amplitude threshold [mv]
const float QrsDetector::DISABLE_TIMEOUT = 1.00*fs;         // INT Time to keep algorithm disabled after amplitude below threshold again


//SANITY CHECK CONSTANTS:
const int16_t QrsDetector::MAX_RATE_CHANGE_1_BPM = 40;                  // INT [bpm]
const int16_t QrsDetector::MAX_RATE_CHANGE_2_BPM = 25;                  // INT [bpm]
const float QrsDetector::TH_MAX_GOODNESS_DROP    = 0.1;	                // FLOAT [0-1]
const int16_t QrsDetector::MIN_RATE              = 30;	                // INT [bpm]
const int16_t QrsDetector::RATE_TIMEOUT          = 3*fs;                // INT Time to keep last heart rate dsiplayed before "-?-" [samples]
const int16_t QrsDetector::INIT_TOO_HIGH_BPM     = 120;                 // INT Threshold to indicate second detection potentially being wrong [bpm]
const int16_t QrsDetector::EXTREME_RATE_BPM      = 180;                 // INT Threshold indicating very high rate which should always be sanity checked


//
// Construction
//
QrsDetector::QrsDetector()
{
    m_LpFIRinsamp = new float[ FIR_BUFFER_LEN ];
    m_HpFIRinsamp = new float[ FIR_BUFFER_LEN ];
    m_HpFIRoutput = new float[ MAX_ECG_BUFFER_SIZE ];
    m_LpFIRoutput = new float[ MAX_ECG_BUFFER_SIZE ];
    m_ecgSamples  = new float[ MAX_ECG_BUFFER_SIZE ];
    
    m_testIirFilter = new IIRfilter();
    
    m_prevHR = 0;
    m_HR = 0;
    m_displayUpdateTime = 2000/THREAD_TICK_TIME; //2 sec 
    m_ampSens = 1.0;
    
    // Initialize the filters
    initQRSdetection();
	initFilters();
    
    m_ecgMin = INT16_MAX;
    m_ecgMax = INT16_MIN;
    m_ecgMinMaxRemaining = 0;
    
}

//
// Destruction
//
QrsDetector::~QrsDetector()
{
}


float QrsDetector::sign(float x)
{
	return (x > 0) ? 1 : ((x < 0) ? -1 : 0);
}

// FIR filter init
void QrsDetector::FIRfilterInit( float *inSamp, uint16_t length )
{
    memset( inSamp, 0, length * sizeof(float));
}


void QrsDetector::initFilters()
{
	FIRfilterInit(m_LpFIRinsamp, FIR_BUFFER_LEN);
    FIRfilterInit(m_HpFIRinsamp, FIR_BUFFER_LEN);
    m_testIirFilter->IIRfilterInit();
}

// the FIR filter function
void QrsDetector::firFloat( const float *coeffs, float *input, float *inSamp, float *output, int length, int filterLength )
{
    float acc; // accumulator for MACs
    float *coeffp; // pointer to coefficients
    float *inputp; // pointer to input samples
    // put the new samples at the high end of the buffer
    memcpy( &inSamp[filterLength - 1], input, length * sizeof(float) );
    // apply the filter to each input sample
    for ( int n = 0; n < length; n++ ) 
    {
        // calculate output n
        coeffp = const_cast<float*>(coeffs);
        inputp = &inSamp[filterLength - 1 + n];
        acc = 0;
        for ( int k = 0; k < filterLength; k++ ) 
        {
            acc += (*coeffp++) * (*inputp--);
        }
        output[n] = acc;
    }
    // shift input samples back in time for next time
    memmove( &inSamp[0], &inSamp[length], (filterLength - 1) * sizeof(float) );
}

int32_t QrsDetector::mean(std::list<int16_t> * a_input)
{
	int32_t res = 0;
	std::list<int16_t>::iterator it;
	
	for ( it = a_input->begin() ; it != a_input->end(); it++ )
	{
		res += *it;
	}
    res += a_input->size()/2; // to make integer division rounded
	res /= a_input->size();

	return res;
}

int32_t QrsDetector::median(std::list<int16_t> a_input)
{
	int32_t res = 0;
	std::list<int16_t>::iterator it;
    
    //Copy the list
    std::list<int16_t> input = a_input;
    
	//Sort the list
	input.sort();

	//Get the value in middle
	uint16_t i = 0;
	uint16_t target = input.size() / 2;

	for ( it = input.begin() ; it != input.end(); it++ )
	{
		res = *it;
		if(i++ == target)
		{
			break;
		}
	}

	return res;

}

void QrsDetector::addToList(std::list<int16_t> * a_list, int16_t a_value, uint16_t a_maxItems)
{
	if(a_list->size() >= a_maxItems)
	{
		a_list->pop_front();    //Remove the oldest HR
	}
	a_list->push_back(a_value);
}

void QrsDetector::removeFromList(std::list<int16_t> * a_list)
{
	if(a_list->size() > 0)
	{
		a_list->pop_back(); //Remove the newest HR
	}
}

void QrsDetector::initQRSdetection()
{
	// INITS:
	K					= 0;
	signK				= -1;
	z					= 0;
	z_prev				= 0;
	D					= 1;
	D_min				= 1;
	adap_th				= 0.4;
	n_D_below_th		= 0;
	instrate			= 0;
	averate				= 0;
	search_mode			= false;
	update_rate			= false;
    r_wave_amp          = 0.0;
    ave_r_wave_amp      = 0.0;

	prev_r_wave			= INT32_MAX;
	prev_instrate		= INT16_MAX;
	prev_prev_instrate	= INT16_MAX;
	prev_prev_r_wave	= INT32_MAX;
	instrate_alt		= 0;
	prev_rate_update	= 0;
	rate_mean			= 0;
	rate_median			= 0;
	goodness			= 0.0;
	prev_goodness		= 0.0;
	prev_prev_goodness	= 0.0;
	ri					= 0;
	n					= 0;
	r_wave_amp			= 0.0;
    y_cur               = 0.0;
    x_cur               = 0.0;
    disabled            = true;
    disable_time        = 0;
    prev_deleted_instrate = 0;
    adjust_detect       = false;
    prev_adjust_detect  = false;
    AVG_BUF_LEN         = 13;
    ave_r_amp_p         = AVE_R_AMP_PORTION;
    AMP_SENS            = m_ampSens;
    
    
    TH_MIN_R_AMP        = (0.005*max_x_val/(CLIP_VOLTAGE*AMP_SENS))*(0.005*max_x_val/(CLIP_VOLTAGE*AMP_SENS));
    th_min_r_amp        = TH_MIN_R_AMP;
    AVE_R_AMP_FF        = 0.9;
    AVE_R_AMP_PORTION   = 0.1;
    
    m_active = false;

}

void QrsDetector::QRSdetectionSimplified(RingBuffer * a_EcgSamples)
{
    // QRSdetectionZC Detect QRS complexes in an ECG signal using zero crossing counts.
    //    Implementation and addons to the method published in the paper
    //"QRS Detection Using Zero Crossing Counts" by K�hler, Hennig & Orglmeister (Progress in
    //    Biomedical Research, Vol 8, No 3, Sep 2003, p 138-145)
    //    
    //   Author : Joar Eilevstj�nn
    //   Updated: 28.03.2014

    //
    // Copyright: All software, documentation, and related files in this distribution
    //            are copyright (c) 2012-2015 Laerdal Medical AS
    //
    // HISTORY:
    // 28.03.2014  J.E.  Initial make adapted from QRSdetectionZC.m
    // 16.10.2014  J.E.  Minor changes to make more suited for Moyo and adults.
    // 26.01.2015  J.E.  Minor adjustments to improve performance:
    //                   DISABLE_TIMEOUT = 1*fs; MAX_RATE_CHANGE_1_BPM = 25; doublerate tolerance changed to 0.1
    // 28.01.2015  J.E.  Further tweaks to improve performance

    if(m_active)
    {
        uint16_t i;
        uint16_t size = a_EcgSamples->GetNumItems() / sizeof(uint16_t); //Size is in no of bytes. We are reading int16.
        if(size > MAX_ECG_BUFFER_SIZE)
        {
            size = MAX_ECG_BUFFER_SIZE;
        }



        
        //---------------------------------------------------------------------------------------------------
        // PRE-PROCESSING
        //---------------------------------------------------------------------------------------------------
        
        for(i = 0; i < size; i++)
        {
            int16_t smp;
            a_EcgSamples->Get(&smp);
            m_ecgSamples[i] = (float)smp;
        }
        
        firFloat( m_HpFIRcoeffs, m_ecgSamples, m_HpFIRinsamp, m_HpFIRoutput, size, HP_FIR_FILTER_LEN );	//High-pass filter
        firFloat( m_LpFIRcoeffs, m_HpFIRoutput, m_LpFIRinsamp, m_LpFIRoutput, size, LP_FIR_FILTER_LEN ); //Low-pass filter      
        
        //---------------------------------------------------------------------------------------------------
        // PROCESSING
        //---------------------------------------------------------------------------------------------------
        for(i=0; i<size; i++) // For all samples
        {
            //------------------------------------------------------------------------------------------------
            // DISABLE AND ADAPTIVE AMPLITUDE THRESHOLD LOGIC
            //------------------------------------------------------------------------------------------------   
            x_cur = (float)m_ecgSamples[i];
            
            //TODO: Do we need 50Hz filter?
            //x_cur = iirFloat(m_IIRcoeffs, x_cur, m_NotchIIRinsampHist, m_NotchIIRoutsampHist);
            
            // Disable large gradient values:
            if((abs(x_cur) > ABSX_TH))
            {
                disabled = true;
                disable_time = n; // time 
                ave_r_amp_p = 0.5; // 13.09.13: In effect sets a higher amplitude threshold to first QRS after a poor period, to remove false detections just after a poor period
                search_mode = false; // do not detect QRS complexes now
            }
            else if((n-disable_time > DISABLE_TIMEOUT))
            {
                disabled = false;      
            }
       
            
            //------------------------------------------------------------------------------------------------
            // K�HLER'S ALGORITHM
            //------------------------------------------------------------------------------------------------
            // Signal enhancement:
            float y_cur = sign(m_LpFIRoutput[i])*m_LpFIRoutput[i]*m_LpFIRoutput[i]; // current sample nonlinearly transformed to increase signal quality (enhance spikes)
            if(disabled)
            {
                y_cur = 0.0;
            }
       
            // Amplitude estimation and addition of a high-frequency sequence:
            K =  LAMBDA_K*K + (1-LAMBDA_K)*abs(y_cur)*GAIN_K; // amplitude of high-frequency sequence
            signK = -signK;      // swap sign every sample
            z_prev = z;
            z = y_cur + K*signK;  // signal plus high-frequency sequence
       
            // Detection and counting of zero crossings:
            float d = abs((sign(z) - sign(z_prev))/2.0);
            if (!disabled) // 28.01.15 only adjust thresholds if enabled
            {
                D = LAMBDA_D*D + (1-LAMBDA_D)*d; 
       
                // QRS detection and search for temporal r-wave localization:
                adap_th = LAMBDA_ATH*adap_th + (1-LAMBDA_ATH)*D; // adaptive detection threshold
                adap_th = MIN(adap_th,MAX_ADAP_TH);
            }
            
            bool timeout = (n - n_D_below_th >= REFRAC_PERIOD);   // detection timeout (time since last sample below threshold)
            if (D < adap_th) // below the threshold?
            {
                n_D_below_th = n; // last zero crossing below threshold time
          
                if(timeout) // ready to search again? (if past last refractory period)
                {
                    search_mode = true;
                    r_wave_amp = 0.0; // reset         
                    D_min = 1; // reset
                    ave_r_wave_amp = ave_r_wave_amp*AVE_R_AMP_FF; // fade average amplitude to allow detection on smaller amplitudes after a while
                }
            }
        
            //------------------------------------------------------------------------------------------------
            // QRS DETECTION AND SANITY CHECKS
            //------------------------------------------------------------------------------------------------
            if(search_mode && !disabled)
            {
                if(!timeout) // search for r-wave?
                {
                    if((abs(y_cur) > r_wave_amp))
                    {
                        r_wave_amp = abs(y_cur); // R-wave amplitude
                        r_wave = n;              // R-wave time (position)
                    }
                    if (D < D_min)
                    {
                        D_min = D;
                    }
                }
                else if (r_wave_amp > MAX(TH_MIN_R_AMP,ave_r_amp_p*ave_r_wave_amp)) // high enough R amplitude?
                {
                    ave_r_amp_p = AVE_R_AMP_PORTION; // reset to default value
                    
                    // Average R wave calculation:
                    if(r_wave_amp < (0.55*max_x_val/CLIP_VOLTAGE)*(0.55*max_x_val/CLIP_VOLTAGE)) // i.e. omit for very large waves
                    {
                        if((r_wave_amp/ave_r_wave_amp > 20) && (ave_r_wave_amp>TH_MIN_R_AMP)) // update slowly if sudden rapid increase
                        {
                            ave_r_wave_amp = AVE_R_AMP_FF*ave_r_wave_amp + 0.1*(1-AVE_R_AMP_FF)*r_wave_amp; // average r_wave amplitude            
                        }
                        else
                        {
                            ave_r_wave_amp = AVE_R_AMP_FF*ave_r_wave_amp + 2*(1-AVE_R_AMP_FF)*r_wave_amp; // average r_wave amplitude
                        }
                    }

                    // QRS LIKELY FOUND
                    search_mode = false;
                    update_rate = true;
                    prev_adjust_detect = adjust_detect;
                    adjust_detect = false; // 0 = normal detection, 1 = adjusted (note: still 0 even if inserted a beat)
             
                    instrate = (int16_t)(60/((r_wave - prev_r_wave)/(float)fs)+0.5); // instantaneous rate
                    instrate_alt = (int16_t)(60/((r_wave - prev_prev_r_wave)/(float)fs)+0.5); // alternative instantaneous rate
                    prev_prev_goodness = prev_goodness;
                    prev_goodness = goodness;
                    goodness = 1 - D_min;
             
                    //Prevent div/0
                    if(instrate == 0.0)
                    {
                        instrate = 1.0; 
                    }
                    if(instrate_alt == 0.0)
                    {
                        instrate_alt = 1.0;
                    }
                    if(prev_instrate == 0.0)
                    {
                        prev_instrate = 1.0;
                    }

                    // ABNORMAL OR MISSING BEAT DETECTORS:
                    if((((float)prev_prev_instrate/(float)instrate_alt < 1.1) && ((float)prev_prev_instrate/(float)instrate_alt > 0.9) &&
                      (((float)instrate/(float)prev_instrate > 1.5) || (goodness>prev_goodness+0.2))&&
                      (abs(instrate-rate_median) > abs(instrate_alt-rate_median)) ) || 
                      (((float)instrate/(float)rate_median > 1.8) && (goodness>prev_goodness-0.2) && 
                      ((float)instrate_alt/(float)rate_median > 0.9) && ((float)instrate_alt/(float)rate_median < 1.1)))
                        // PREVIOUS BEAT FALSE
                    {
                        // last detected beat is likely false
                        if((abs((float)prev_deleted_instrate/(float)instrate - 1.0) > 0.1) && prev_deleted_instrate > 0.0)
                        {
                            // 02.01.13: Only delete beat if large enough difference between by instrate and
                            // previous delelted instrate. I.e. if they are too similar we are likely deleting 
                            // every other beat ("deadlock protection").
                            prev_deleted_instrate = instrate;
                            prev_instrate = prev_prev_instrate;
                            prev_goodness = prev_prev_goodness;
                            instrate = instrate_alt;
                            ri = ri - 1;                     // delete previous recorded qrs
                            removeFromList(&instrates);
                            prev_r_wave = prev_prev_r_wave;
                            adjust_detect = true;
                            //TODO
                            
                            Events::getpEvents()->AddEvent(1000*(prev_r_wave - DELAY)/fs, 
                                                            Events::evtMaternalQRS, 
                                                            Events::TO_EPISODE_LOG,
                                                            -1.0, 
                                                            -1.0,
                                                            -1.0);
                            
                        }
                    }
                    else if (((float)prev_instrate/(float)instrate > 1.9) && ((float)prev_instrate/(float)instrate < 2.1) && (prev_prev_goodness > 0)) // SINGLE MISSING BEAT
                    {
                        // we have likely missed a beat
                        prev_prev_instrate = prev_instrate;
                        prev_instrate = instrate;      // use current instrate as next last instrate in case there was no missing beats
                        instrate = prev_prev_instrate; // report rate as it was (~doubles instrate)
                        prev_goodness = 0;
                
                        // Add missing beat:
                        ri = ri + 1;
                        averate = (rate_mean + rate_median)/2; // "Displayed" rate
                        
                        Events::getpEvents()->AddEvent((uint32_t)(1000*(float)(prev_r_wave + (r_wave - prev_r_wave)/2 - DELAY)/(float)fs), 
                                                       Events::evtMaternalQRS,
                                                       Events::TO_EPISODE_LOG,
                                                       (float)averate, 
                                                       (float)instrate,
                                                       0.0);
                        
                        
                        //Keep last AVG_BUF_LEN instrates
                        addToList(&instrates, instrate, AVG_BUF_LEN);
                        
                    }
                    
                    // SANITY CHECK CURRENT BEAT:
                    if((prev_adjust_detect==false) || (instrate > EXTREME_RATE_BPM)) // only adjust if not previous detection was adjusted

                    {
                        if(((rate_median>0) && (instrate - rate_median > MAX_RATE_CHANGE_1_BPM)) ||
                          ((ri <= 2) && (instrate > INIT_TOO_HIGH_BPM)))
                        {
                            // too rapid rate change -> likely a false detection, ignore it unless this detection has
                            // higher goodness than previous detection which then will be overwritten
                            // 18.12.12: Last condition added to reduce number of initial false detections during
                            // algorithm threshold settling.
                            if((abs((float)instrate/(float)prev_instrate-2) < 0.10))
                            {
                                // 02.09.2013 Doublerate present -> do not remove beat to achieve proper double rate
                                // 26.01.2015 Changed limit from 0.05 to 0.1 => same as for 24.01.2012 ri==2 protection - therefore 
                                // it is simplified to apply to all ri.
                            }
                            else if((goodness > prev_goodness + 0.15) || ((instrate > EXTREME_RATE_BPM) && (goodness > prev_goodness)))
                            {
                                ri = ri - 1; // in effect, previous detection will be overwritten

                                Events::getpEvents()->AddEvent(1000*(prev_r_wave - DELAY)/fs, 
                                                        Events::evtMaternalQRS, 
                                                        Events::TO_EPISODE_LOG,
                                                        -1.0, 
                                                        -1.0,
                                                        -1.0);
                                
                                removeFromList(&instrates);
                                instrate = instrate_alt;
                                prev_instrate = prev_prev_instrate;
                                prev_goodness = prev_prev_goodness;
                                adjust_detect = true;
                            }            
                            else
                            {
                                update_rate = false;
                                adjust_detect = true;
                            }
                        }  
                        else if((rate_median>0) && (instrate - rate_median > MAX_RATE_CHANGE_2_BPM) &&
                                (prev_goodness - goodness > TH_MAX_GOODNESS_DROP))
                        {
                            // rapid rate change and decreased goodness -> likely a false detection, ignore it
                            update_rate = false;
                            adjust_detect = true;
                        }
                    }
              
                    if(update_rate) // Only add QRS complex if change in instrate is reasonable
                    {
                        // Add info to detected qrs structure:
                        ri = ri + 1;
                        
                        // Update mean and median:
                        if(instrate >= MIN_RATE)
                        {
                            //Keep last AVG_BUF_LEN instrates
                            addToList(&instrates, instrate, AVG_BUF_LEN);
                            
                            rate_mean = mean(&instrates); // last AVG_BUF_LEN instantaneous rates
                            // NOTE: The instantaneous rate of the first detected QRS are never used in the calculations (non-valid rate).
                            rate_median = median(instrates);
                        }
                        
                        //Log
                        averate = (rate_mean + rate_median)/2; // "Displayed" rate
                        
                        Events::getpEvents()->AddEvent(1000*(r_wave - DELAY)/fs, 
                                                       Events::evtMaternalQRS, 
                                                       Events::TO_EPISODE_LOG,
                                                       (float)averate, 
                                                       (float)instrate,
                                                        goodness);
                        
                    
                        // Update prev_ variables:
                        prev_prev_r_wave = prev_r_wave;
                        prev_r_wave = r_wave;
                        prev_prev_instrate = prev_instrate;
                        prev_instrate = instrate;
                    }
                }          
            }
          
            // Rate output logic:
            if ((update_rate) && (rate_median>0) && (instrate >= MIN_RATE))
            {
                if((prev_rate_update== 0) || (n - prev_rate_update <= RATE_TIMEOUT))
                {
                   // 22.01.13: In effect, this if statement makes rate appear first after three QRS complexes
                   // after a period of "-?-" (but not at startup where only two complexes are needed!).
                   averate = (rate_mean + rate_median)/2; // "Displayed" rate
                }
                                
                m_HR = averate;
                
                prev_rate_update = r_wave; //  use time of last r wave to make timeout calc. from this
                update_rate = false;  // reset
            }
            else if(n - prev_rate_update > RATE_TIMEOUT) // too long time since last rate update (timeout)?
            {
                averate = 0; // C++: Display "-?-"
                m_HR = averate;
                update_rate = false;  // reset
            }


            n++; //Increment time tick
       }
    }
    
}
uint16_t QrsDetector::GetHR(void)
{
    return m_HR;
}

void QrsDetector::SetAmpSens(float a_ampSens)
{
    if(a_ampSens > CFG_MIN_AMPSENSE && a_ampSens <= CFG_MAX_AMPSENSE)
    {
        m_ampSens = a_ampSens;
    }
    initQRSdetection();
    initFilters(); 
}

void QrsDetector::SetAvgBufLen(uint8_t a_avgBufLen)
{
    a_avgBufLen = MIN(CFG_MAX_MHR_AVG_BUF_LEN, a_avgBufLen); //Limit
    a_avgBufLen = MAX(CFG_MIN_MHR_AVG_BUF_LEN, a_avgBufLen); //Limit

    AVG_BUF_LEN = a_avgBufLen;
}

void QrsDetector::SetActive(bool a_active)
{
    m_active = a_active;
}

bool QrsDetector::IsActive(void)
{
    return m_active;
}

void QrsDetector::updateTime(uint32_t a_OStime)
{
    n = fs * a_OStime / 1000;
}

void QrsDetector::ECGminMax(RingBuffer * a_EcgSamples)
{
    uint16_t i;
    uint16_t size = a_EcgSamples->GetNumItems() / sizeof(uint16_t); //Size is in no of bytes. We are reading int16.
    if(size > MAX_ECG_BUFFER_SIZE)
    {
        size = MAX_ECG_BUFFER_SIZE;
    }
    
    int16_t smp;
    for(i = 0; i < size; i++)
    {
        a_EcgSamples->Get(&smp);
        x_no50Hz = m_testIirFilter->filterSample50HzSF200HzF(smp);
        
        smp = (int16_t)x_no50Hz;
        if(smp < m_ecgMin)
        {
            m_ecgMin = smp;
        }
        else if(smp > m_ecgMax)
        {
            m_ecgMax = smp;
        }
        
        if(m_ecgMinMaxRemaining == 1) //Once
        {
            CONSOLE_PRINT("R110 %d %d\r\n", m_ecgMin, m_ecgMax);
            m_ecgMinMaxRemaining--;
        }
        else if(m_ecgMinMaxRemaining)
        {
            m_ecgMinMaxRemaining--;
        }
    }
    
}

void QrsDetector::initEcgMinMaxMeasurement(RingBuffer * a_EcgSamples, uint16_t a_measurementTimeMs)
{
    m_ecgMin = INT16_MAX;
    m_ecgMax = INT16_MIN;
    initFilters();
    
    //Clear buffer
    uint16_t size = a_EcgSamples->GetNumItems() / sizeof(uint16_t); //Size is in no of bytes. We are reading int16.
    int16_t smp;
    for(uint16_t i = 0; i < size; i++)
    {
        a_EcgSamples->Get(&smp);
    }
    
    if(a_measurementTimeMs > MAX_MEASUREMENT_TIME)
    {
        m_ecgMinMaxRemaining = (uint16_t)((uint32_t)ECG_SAMPLE_FREQ * (uint32_t)MAX_MEASUREMENT_TIME / 1000);
    }
    else
    {
        m_ecgMinMaxRemaining = (uint16_t)((uint32_t)ECG_SAMPLE_FREQ * (uint32_t)a_measurementTimeMs / 1000);
    }
}