//! @class  FileThread
//! @brief
//! @author Jan Arild R�yneberg
//! @date   22.06.2012
//!

#ifndef _FILE_THREAD_H_
#define _FILE_THREAD_H_

#include "thread.h"
#include "includes.h"
//#include "fileinterface.h"
//#include "fs.h"
//#include "fs_int.h"
#include "ff.h"
#include "sd_diskio.h" /* defines SD_Driver as external */
#include "rtc.h"
#include "versioninfo.h"
#include "communicationutility.h"
#include <string>
#include "FHRMprobeSerial.h"
#include "adthread.h"
#include "ecgbuffer.h"
#include "commondefs.h"
#include "th1super.h"
#include "events.h"
//#include "msd_mass_storage.h"

//! @brief Handles the file system
class FileThread:public Thread
{
    
public:
	FileThread();
	~FileThread();
	void run(void *);
    static FileThread* get() { return m_pFileTh; }
    POST_ctrl_t getPostResult(void);
    void clearPostResult(void);

        enum ErrorLogEntry_t
    {
        eErrorLogEntryFirst,
        eErrorLogEntryNext,
        eErrorLogEntryAll
    };
    
    enum EpisodeState_t
    {
        eEpStWaitForStart,
        eEpStRunning,
        eEpStStopped
    };
    
    
    
protected:

private:
    static const uint16_t THREAD_TICK_TIME = 50;
    static const char * m_VolName;
    static const char * m_SoundFileNameExt;
    static const char * m_UtilFileNameExt;
    static const char * m_FhrFileNameExt;
    static const char * m_EcgFileNameExt;
    static const char * m_EventFileNameExt;
    static const char * m_SystemLogFileName;
    static const uint8_t SOUND_FILE_ID = 13;
    static const uint8_t UTIL_FILE_ID = 12;
    static const uint8_t ECG_FILE_ID = 11;
    static const uint8_t FHR_FILE_ID = 14;
    static const uint8_t EVN_FILE_ID = 15;
    static const uint32_t MIN_SD_FREE_SPACE_KB = 307200; //300MB (1024*300 = 307200)
    char SDPath[4];   /* SD logical drive path */
    FATFS  m_fs;
    FIL    m_pSoundFile;
    FIL    m_pUtilFile;
    FIL    m_pEcgFile;
    FIL    m_pFhrFile;
    FIL    m_pEventFile;
    std::string m_episodeName;
    bool m_validEpisode;
    bool m_error;
    uint32_t m_episodeStartTime;
    static const uint16_t GET_ERROR_LOG_ENTRY_MAX_CHARS = 80;
    static const uint16_t MAX_ERROR_LOG_INDEXES = 25;
    
    static FileThread* m_pFileTh;  //!< static member to keep track of instance. Needed for return data to ectern C functions
    
    //LNRM_FS * m_pLNRM_FS;
    uint8_t m_fileWriteErrorCnt;
    static const uint8_t MAX_FILE_WRITE_ERROR_REPORTS = 5;
    static const uint32_t FILE_WRITE_ERROR_MASK_TIME = 10000; //ms since episode start
    EpisodeState_t m_episodeState;
    
    ADThread *m_pAdcTh;
    Th1Super *m_pSuperTh;
    
    static const uint16_t SOUND_BUF_SIZE = 512;
    uint8_t *m_pSoundBuffer;
    
    static const uint16_t UTIL_BUF_SIZE = 128;
    Utilrecord_t *m_pUtilBuffer;
    
    static const uint16_t ECG_BUF_SIZE = 128;
    EcgBuffer::ECGrecord_t *m_pEcgBuffer;
    
    std::vector<FHRrecord_t> m_FhrRecords;
    
    bool m_researchStorageModeEnabled;
    bool m_keepEpisode;
    
    POST_ctrl_t m_POSTstatus;

    bool m_SDcardEnabled;
    bool m_SDcardPresent;
    
    
    OperationalMode_t m_operationalMode;
    
    uint32_t m_USBstate;
    
    bool CreateEpisode();
    void StartEpisode();
    void CloseEpisode(bool a_validateEpisode, bool a_deleteEpisode);
    uint8_t MakeHeader(uint8_t fileId, LL_RTC_TimeTypeDef a_time, LL_RTC_DateTypeDef a_date, char * a_buffer);
    bool createEpisodeFile(uint8_t a_fileId, FIL* a_fp, const TCHAR* a_path, LL_RTC_TimeTypeDef a_time, LL_RTC_DateTypeDef a_date);
    bool validateEpisode(string a_episodeName);
    bool deleteEpisode(string a_episodeName);
    bool writeSystemLog(const char *a_data, uint16_t a_length);
    std::vector<int32_t> getNewestErrorLogEntryIndexes(uint16_t a_noEntries);
    std::string getErrorLogEntry(int32_t a_filePos);
    void printErrorLogEntry(ErrorLogEntry_t a_entry);
    std::string getErrorLogEntry(ErrorLogEntry_t a_entry);
    bool deleteSysLog();
    bool deleteErrorLog();
    void logFileWriteError(FIL *a_pFile, bool a_diskFull);
    bool post();
    bool deleteOldestEpisode();
    bool FileExists(std::string a_filename);
    bool CheckPlayback();
    void saveToFiles();
    bool setFileTime(const char * a_pFileName);
    bool FhrFileLock();
    bool FhrFileUnlock();
    void eraseSDcard();
    void listRootFiles();
    //void startMassStorage(void);
    
    
};

#endif
