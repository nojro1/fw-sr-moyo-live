//! @class  Th1Super
//! @brief
//! \author  Jan Arild R�yneberg
//! \date    13.06.2012
//!


#ifndef _TH1_H_
#define _TH1_H_

#include "thread.h"
#include "versioninfo.h"
#include "watchdog.h"
#include "fhrmprobeserial.h"
#include "dacdma.h"
#include "FHRMAudio.h"
#include "bq27510.h"
#include "configuration.h"
#include "board_cfg.h"
#include "commondefs.h"
#include "events.h"
#include "UI/DisplayComponents\batterycapacity.h"
#include <list>
#include <assert.h>
#include "adthread.h"
#include "rtc.h"
//#include "usb_core.h"
//#include "usb_pwr.h"

//! @brief TBD
class Th1Super:public Thread
{
public:
	Th1Super();
	~Th1Super();
	void run(void *);
    POST_ctrl_t getPostResult(void);
    void clearPostResult(void);
    //Events* getpEvents() {return m_pEvents; }
    void GetFhrRecords(std::vector<FHRrecord_t> & a_Records);

    enum SndPitchCtrl_t {eSPCInit, eSPCcalibrate, eSPCRun, eSPCReCalibrate};
    
    enum UlSndTransCommTmoutState_t {eUSTCcommunicating, eUSTCrebootPowerOff, eUSTCrebootPowerOn, eUSTClostComm};
    
    enum FhrAlarmState_t {eFASInit, eFASArm, eFASnoAlarm, eFASAlBelowThreshold, eFASAlarm, eFASFhrRecovered};
    
    enum AlarmType_t {eATnoAlarm, eATlowPri, eATmedPri};
    
    enum KeyTestState_t {eKTSidle, eKTSwaitForPress, eKTSwaitForRelease};
    
    enum UItestType_t {eUITTnone, eUITTled, eUITTkey, eUITTdisplay, eUITTsound};
    
    enum Key_t {eKeyNone = 0, eKeyPower, eKeyDisplayMode, eKeyMute, eKeyError};
    
    typedef struct
    {
        uint32_t endOfEpisodeTimer;
        uint32_t validEpisodeTimer;
        uint32_t episodeTooShortTimer;
        uint16_t accSignalTimer;
        uint16_t motionTimer;
        uint16_t MHRvalidTimer;
        uint16_t FHRvalidTimer;
        uint16_t FHRinvalidTimer;
    } EpisodeTimers_t;
    
    typedef struct
    {
        int16_t setpoint;       // Target buffer fill
        int16_t measured;       // Current buffer fill
        uint16_t nomBaseLine;   // Dac timer value nominal (I.E. 18000)
        uint16_t calcBaseLine;  // Dac timer value calculated (I.E. 17500)
        uint16_t minOutput;
        uint16_t maxOutput;
        uint16_t Kp;            
        uint16_t Kd;            
        uint16_t Td;            
        uint16_t Ki;
        uint16_t Ko;         // Output Scale
        int16_t PrevErr;
        int16_t Ierror;
    } ctrlInfo_t, *pCtrlInfo;
    
    static const uint16_t FHR_RECORDS_BUFFER_SIZE = 10;
    static const uint16_t FHR_SAMPLE_RATE = 2; //Hz
    
protected:

    

private:
    
    uint32_t DoPid(ctrlInfo_t *p);
    uint8_t mean(std::list<uint8_t> * a_input);
    uint16_t mean(std::list<uint16_t> * a_input);
    uint8_t min(std::list<uint8_t> a_input);
    uint16_t min(std::list<uint16_t> a_input);
    uint8_t max(std::list<uint8_t> a_input);
    uint16_t max(std::list<uint16_t> a_input);
    
    void addToList(std::list<uint8_t> * a_list, uint8_t a_value, uint8_t a_maxItems);
    void addToList(std::list<uint16_t> * a_list, uint16_t a_value, uint8_t a_maxItems);
    void soundPitchControl(void);
    void ultraSndProbeCommHandler(void);
    void FhrLostSignalAlarmHandler(uint16_t a_fhr, uint8_t a_fhrQuality);
    void FhrAlarmHandler(uint16_t a_fhr, uint8_t a_fhrQuality);
    void batteryAlarmHandler(uint8_t a_remainingCapacity);
    void setDisplayAlarmPrioHandler(Alarm_t a_alarm);
    void clearDisplayAlarmPrioHandler(Alarm_t a_alarm);
    void setAudioAlarmPrioHandler(Alarm_t a_alarm);
    void clearAudioAlarmPrioHandler(Alarm_t a_alarm);
    void setAudioAlarm(AlarmType_t a_alarmType);
    void audioAlarmHandler(void);
    void handleFuelGauge();
    void systemShutdown(ShutdownReason_t a_reason);
    void sampleFhrData(uint32_t time);
    bool FhrRecordsLock();
    bool FhrRecordsUnlock();
    void EpisodeHandler(uint8_t a_fetal_hr, uint8_t a_maternalHR, int16_t a_accEnergy);
    //void EpisodeValidHandler(void);
    //void EpisodeTooShortHandler(void);
    //void ResetEpisodeValidTime(void);
    void watchdogKickHandler(void);
    bool post(void);
    uint8_t getAlarmPriority(Alarm_t a_alarm);
    AlarmType_t getAudioAlarmType(Alarm_t a_alarm);
    Alarm_t getPrioritisedAlarm(Alarm_t a_alarm);
    void setTestSound(SoundTestCommand_t a_command);
    void initStatusLEDtest(StatusLEDtestCommand_t a_command);
    void statusLEDTestHandler(void);
    void UItestHandler(void);
    void initKeyTest(void);
    void setConfigDefault(void);
    void enableMaxBatteryCharge();
    void flashFuelgauge(void);
    
#ifdef DEBUG_ALARM_HANDLER
    void debugAlarmHandler();
#endif
    
    static const uint16_t THREAD_TICK_TIME = 20;
    uint16_t m_PitchRegulateTimer;
    uint16_t m_PitchTimer;
    SndPitchCtrl_t m_soundPitchCtrl;
    static const uint16_t PITCH_TIME = (200/THREAD_TICK_TIME);
    static const uint16_t PITCH_REG_TIME = (1000/PITCH_TIME/THREAD_TICK_TIME);
    bool m_resyncSound;
    ctrlInfo_t pitchCtrl;
    pCtrlInfo m_pPitchCtrl;
    static const uint8_t SND_BUF_FILL_AVG_BUF_LEN = 10;
    static const uint8_t SND_SMP_RATE_AVG_BUF_LEN = 10;
    static const uint16_t DAC_TIM_VAL_MAX_DEV = 3000;   //From nominal
    std::list<uint8_t> sndBufferFillLevels;
    std::list<uint16_t> sndSampleRates;
    uint16_t m_sndSampleRate;
    uint8_t m_sndSampleRateChangedCtr;
    //Config * m_pConfig;
    UlSndTransCommTmoutState_t m_ulsndTransComm;
    uint16_t m_ulsndTransCommRetryTmr;
    static const uint16_t ULSND_TRANS_POWER_OFF_WAIT_TIME = (1000/THREAD_TICK_TIME);
    static const uint16_t ULSND_TRANS_RETRY_TIME = (6000/THREAD_TICK_TIME);
    POST_ctrl_t m_POSTstatus;
    uint16_t m_fhrLostSignalAlarmTimer;
    FhrAlarmState_t m_fhrLostSignalAlarmState;
    uint16_t m_FhrLostSignalAlarmTime;
    uint16_t m_fhrLostSignalFilterTimer;
    static const uint16_t FHR_LOST_SIGNAL_FILTER_TIME = (2000/THREAD_TICK_TIME); //ms
    static const uint8_t MIN_VALID_HR = 50; //BPM
    static const uint8_t MIN_ACCELERATION = 5; 
    static const uint16_t FHR_LOST_SIGNAL_ALARM_LIMIT = MIN_VALID_HR;
    static const uint16_t FHR_ALARM_INIT_TIME = (10000/THREAD_TICK_TIME); //ms
    uint16_t m_fhrAtypicalAlarmTimer;
    uint16_t m_fhrAbnormalAlarmTimer;
    uint16_t m_fhrAlarmRecoverTimer;
    FhrAlarmState_t m_fhrAlarmState;
    static const uint16_t FHR_ABNORMAL_ALARM_ON_TIME = (/*10000*/180000/THREAD_TICK_TIME); //ms
    static const uint16_t FHR_ALARM_RECOVER_TIME = (5000/THREAD_TICK_TIME); //ms
    static const uint16_t FHR_ATYPICAL_ALARM_RECOVER_TIME = (5000/THREAD_TICK_TIME); //ms
    static const uint16_t FHR_LOW_ALARM_RECOVER_TIME = (30000/THREAD_TICK_TIME); //ms
    static const uint16_t FHR_ATYPICAL_ALARM_ON_TIME = (600000/THREAD_TICK_TIME); //ms
    uint8_t m_fhrAtypicalAlarmLimitLow;
    uint8_t m_fhrAtypicalAlarmLimitHigh;
    uint8_t m_fhrAbnormalAlarmLimitLow;
    uint8_t m_fhrAbnormalAlarmLimitHigh;
    uint16_t m_fhrAtypicalAlarmTime;
    uint16_t m_fhrAbnormalAlarmTime;
    Alarm_t m_displayAlarm;
    Alarm_t m_audioAlarm;
    Alarm_t m_alarmAcknowlegde;
    uint16_t m_alarmSoundTimer;
    AlarmType_t m_alarmType;
    bool m_alarmSoundEnabled; //Needed for not playing alarms upon startup
    static const uint16_t LOW_PRI_ALARM_CYCLE_TIME = (30000/THREAD_TICK_TIME); //ms
    static const uint16_t MED_PRI_ALARM_CYCLE_TIME = (5000/THREAD_TICK_TIME); //ms
    uint8_t m_batteryRemainingCapacity;
    bool m_batteryFull;
    static const uint16_t FUEL_GAUGE_DEFAULT_DESIGN_CAPACITY = 1000;
    
#ifdef USE_VOLTAGE_AS_SHUTDOWN_CRITERIA
    int32_t m_batteryVoltagemv;
    static const uint16_t BAT_EMPTY_MV = 3000; //mv
#endif
    
    bool m_batteryEmpty;
    OperationalMode_t m_operationalMode;
    uint16_t m_fuelGaugeTmr;
    static const uint16_t FUEL_GAUGE_READ_TIME = (1000/THREAD_TICK_TIME);
    bool m_batteryDebugOut;
    bool m_probeSoundDebugOut;
    //Events * m_pEvents;
    std::vector<FHRrecord_t> m_FhrRecords;
    uint32_t m_FhrFileNextSampleTime;
    static const uint16_t FHR_SAMPLE_TIME = 1000/FHR_SAMPLE_RATE;
    ADThread *m_pAdcTh;
    EpisodeTimers_t m_episodeTimers;
    static const uint16_t END_OF_EP_TMOUT_FILTER_TIME = (5000/THREAD_TICK_TIME);
    static const uint16_t MOTION_TIMEOUT = (500/THREAD_TICK_TIME); //Time for signal low from acceleration to be detected as no motion 
    //uint32_t m_endOfEpisodeTimer;
    uint32_t m_endOfEpisodeTime;
    //uint32_t m_validEpisodeTimer;
    uint32_t m_validEpisodeTime;
    //uint32_t m_episodeTooShortTimer;
    uint32_t m_episodeTooShortTime;
    uint16_t m_wdKickTmr;
    static const uint16_t WD_KICK_TIME = (500/THREAD_TICK_TIME);
    uint8_t m_probeTemperature;
    uint16_t m_probeTemperatureTimer;
    static const uint16_t PROBE_TEMP_EVENT_TIME = 10000/FHR_SAMPLE_TIME;
    static const uint8_t    TRANSDUCER_MAX_ERR_CNT = 3;
    uint8_t m_maternalHR;
    uint8_t m_fetalHR;
    FHRMps::SignalQuality_t m_fetalHRSigQual;
    uint16_t m_powerGoodTimer;
    static const uint16_t POWER_GOOD_FILTER_TIME = (2000/THREAD_TICK_TIME);
    bool m_massStorageTimeout;
    //uint16_t m_statusLEDtestTimer;
    //static const uint16_t STATUS_LED_TEST_TIMEOUT = (10000/THREAD_TICK_TIME);
    uint16_t m_keyPressTestTimer;
    static const uint16_t KEY_PRESS_TEST_TIMEOUT = (10000/THREAD_TICK_TIME);
    KeyTestState_t m_keyTestState;
    Key_t m_currentKeyTested;
    Key_t m_keyTested;
    UItestType_t m_UItestType;
    bool m_testpinActive;
    
    static const uint8_t ADC5V_POST_NO_SAMPLES = 20;
    static const uint16_t ADC5V_NOM = 3103; //With 5V = 5.0V, R215 and R216 = 10K, ADC ref = 3.3V
    static const uint16_t ADC5V_MIN = ADC5V_NOM - 100; //TODO set limits
    static const uint16_t ADC5V_MAX = ADC5V_NOM + 100; //TODO set limits

    
 

};

#endif
