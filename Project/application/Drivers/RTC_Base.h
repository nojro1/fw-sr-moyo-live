#ifndef _RTC_BASE_H
#define _RTC_BASE_H

// header includes
#include <ctime>



// forward declarations
////

//! \brief Abstract base class for RTC devices
class RTC_Base
{
public:
  
    virtual ~RTC_Base() {;}

    //! \brief Writes a new time/date to the RTC
    //! \param t: time_t, i.e. the number of seconds elapsed since 00:00 hours, Jan 1, 1970 UTC
    virtual void setTimeDate(std::time_t t) const = 0;

    //! \brief Reads time/date from the RTC
    //! \return time_t, i.e. the number of seconds elapsed since 00:00 hours, Jan 1, 1970 UTC
    virtual std::time_t getTimeDate() const = 0;

protected:

private:

};


#endif   // _RTC_BASE_H


