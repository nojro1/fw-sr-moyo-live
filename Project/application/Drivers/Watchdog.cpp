//! \file    Watchdog.cpp
//! \author  Geir Inge Tellnes
//! \version 1.0
//! \date    12.05.2010


// include self first
#include "Watchdog.h"

// other includes
#include "board_cfg.h"
#include "RTOS.h"
#include <algorithm>

#if !defined(STM32L1XX_MD)
  // assume F family
  #define CFG_WD_TIMEOUT_MAX 26214
#else
  //L family
  #define CFG_WD_TIMEOUT_MAX 28339
#endif

#if (CFG_WATCHDOG_TIMEOUT > CFG_WD_TIMEOUT_MAX)
    #error "CFG_WATCHDOG_TIMEOUT higher than supported by HW"
#endif

// static member init
Watchdog* Watchdog::m_pWdObj = 0;


//! \brief Constructor
//! \remark The independent watchdog timing is accurate to approx. -25%/+50%\n
//! based on the LSI RC oscillator nominal 40 kHz frequency\n
//! so a specified watchdog timeout (in board_cfg.h) of 1 second can become 0.75..1.5 seconds
Watchdog::Watchdog()
{
    //OS_EnterRegion();
    if (0 == m_pWdObj)
    {
        // IWDG timeout varies due to LSI frequency dispersion
        m_wdt.Instance = IWDG;
        
        // IWDG counter clock: 40KHz(LSI) / prescaler = xx KHz
        #if (CFG_WATCHDOG_TIMEOUT <= 409)
            m_wdt.Init.Prescaler = IWDG_PRESCALER_4;   // 10 kHz
            m_wdt.Init.Reload = (CFG_WATCHDOG_TIMEOUT * 10000uL)/1000;
            
        #elif (CFG_WATCHDOG_TIMEOUT <= 819)
            m_wdt.Init.Prescaler = IWDG_PRESCALER_8;   // 5 kHz
            m_wdt.Init.Reload = (CFG_WATCHDOG_TIMEOUT * 5000uL)/1000;
        #elif (CFG_WATCHDOG_TIMEOUT <= 1638)
            m_wdt.Init.Prescaler = IWDG_PRESCALER_16;  // 2.5 kHz
            m_wdt.Init.Reload = (CFG_WATCHDOG_TIMEOUT * 2500uL)/1000;
        #elif (CFG_WATCHDOG_TIMEOUT <= 3276)
            m_wdt.Init.Prescaler = IWDG_PRESCALER_32;  // 1.25 kHz
            m_wdt.Init.Reload = (CFG_WATCHDOG_TIMEOUT * 1250uL)/1000;
        #elif (CFG_WATCHDOG_TIMEOUT <= 6553)
            m_wdt.Init.Prescaler = IWDG_PRESCALER_64;  // 0.625 kHz
            m_wdt.Init.Reload = (CFG_WATCHDOG_TIMEOUT * 625uL)/1000;
        #elif (CFG_WATCHDOG_TIMEOUT <= 13108)
            m_wdt.Init.Prescaler = IWDG_PRESCALER_128; // 0.3125 kHz
            m_wdt.Init.Reload = (CFG_WATCHDOG_TIMEOUT * 312uL)/1000;
        #else
            m_wdt.Init.Prescaler = IWDG_PRESCALER_256; // 0.15625 kHz
            m_wdt.Init.Reload = (CFG_WATCHDOG_TIMEOUT * 156uL)/1000;
        #endif

        //Set window same as reload value
        m_wdt.Init.Window = m_wdt.Init.Reload;
        
        
        

    }
    //OS_LeaveRegion();
}

//! \brief Destructor
Watchdog::~Watchdog()
{
}

//! \brief Start watchdog
void Watchdog::start()
{
    // Enable IWDG (the LSI oscillator will be enabled by hardware)
    //Init (and perforn initial kick)
    if (HAL_IWDG_Init(&m_wdt) != HAL_OK)
    {
         //Error_Handler(); //TODO
    }
}

//! \brief Kick the watchdog to keep it happy
//! \remark Will assert in debug builds if called from interrupt
void Watchdog::kick()
{
    //OS_ASSERT_NOINT(); // not allowed to call from isr
    // Reload IWDG counter
    HAL_IWDG_Refresh(&m_wdt);
}

//! \brief Check if reset cause was watchdog reset
//! \return true if reset caused by the independent watchdog, else false
//! \remark Clears the reset cause after being called
/*
bool Watchdog::watchdogResetDetected() const
{
    bool wdtres = RCC_GetFlagStatus(RCC_FLAG_IWDGRST) != RESET;
    RCC_ClearFlag(); // clear the reset cause
    return wdtres;
}
*/

