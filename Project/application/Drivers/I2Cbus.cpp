//! \file    I2Cbus.cpp
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    24.05.2011

//Prototypes for interrupt service routines
extern "C"
{
    void I2C1_EV_IRQHandler(void);
    void I2C1_ER_IRQHandler(void);
}


// include self first
#ifndef __STDC_LIMIT_MACROS
  //Have to use this compiler private macro bs the code uses C macro limits. Cannot use std C++
   //<limits> in case this driver is compiled in embedded C++ mode
  #define __STDC_LIMIT_MACROS
  #include <cstdint>
#endif
#include "I2Cbus.h"
#include "I2Ccfg.h"

// other includes
#include "rtos.h"
#include <Mutex.h>
#include <utilfuncs.h>

#include <memory>
#include <cstdio>

USING_NAMESPACE_STD;

// static member init

I2Cbus*  I2Cbus::m_pI2Cobj = 0;


//! \brief Constructor
//! \param portNum Which I2C port to use
//! \param bitRate The I2C data rate/clock frequency (500-400000 Hz)
//! \param ownAddress Our I2C slave address (if slave mode used)
I2Cbus::I2Cbus(I2C_Portnum_t portNum, uint32_t bitRate, uint8_t ownAddress)
: m_I2CtimeoutNAK(0)
  ,
  m_TxBufIdx(0), m_RxBufIdx(0), m_TxNumBytes(0), m_RxNumBytes(0),
  m_isMasterReceiver(false), m_receiverFollowsTransmitter(false),
  m_busResetFromIsr(false)
{
    assert_param(bitRate == 100000);
    assert_param(portNum == I2Cbus::I2CPORT_1);
    
    //OS_EnterRegion();

    if (0 == m_pI2Cobj)
    {
        m_pI2Cobj = this;
        m_pMutex = new Mutex;

        m_i2c.Instance = I2C1;
        m_i2c.Init.Timing = 0x40505681; //Note: If bitrate is changed, this value must be changed as well
        m_i2c.Init.OwnAddress1 = ownAddress;
        m_i2c.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
        m_i2c.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
        m_i2c.Init.OwnAddress2 = 0;
        m_i2c.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
        m_i2c.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
        m_i2c.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;

        m_i2c_max_err_cnt = (bitRate/8) / 100; // 10 ms timeout on I2C errors / device unresponsive
        
        softwareReset();
        
        HAL_I2C_DeInit(&m_i2c); // for debug purposes
   
        if (HAL_I2C_Init(&m_i2c) != HAL_OK)
        {
            //Error_Handler(); //TODO
        }
        
        //Configure Analogue filter 
        if (HAL_I2CEx_ConfigAnalogFilter(&m_i2c, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
        {
            //Error_Handler();
        }
        
        //Configure Digital filter 
        if (HAL_I2CEx_ConfigDigitalFilter(&m_i2c, 0) != HAL_OK)
        {
            //Error_Handler();
        }
        

        DWT_Delay_Init(); // for bus time measurements before OS has started
    }

    //OS_LeaveRegion();

}

//! \brief Destructor
I2Cbus::~I2Cbus()
{
    //TODO
}

void I2Cbus::softwareReset()
{
    uint16_t tmout = 100;
    
    CLEAR_BIT(m_i2c.Instance->CR1, I2C_CR1_PE);
    while(tmout-- > 0 && READ_BIT(m_i2c.Instance->CR1, I2C_CR1_PE) > 0);
    SET_BIT(m_i2c.Instance->CR1, I2C_CR1_PE);
}

void I2Cbus::reset()
{
    select();

    softwareReset();
    m_isMasterReceiver = false;
    m_receiverFollowsTransmitter = false; // abort pending reception
    m_busResetFromIsr = false;
    HAL_I2C_DeInit(&m_i2c);
    HAL_I2C_Init(&m_i2c);       // re-init

    deselect();
}

void I2Cbus::cfg_gpio_as_i2c()
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    
    GPIO_InitStruct.Pin = CFG_I2C1_GPIO_pin_SCL | CFG_I2C1_GPIO_pin_SDA;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    
}

void I2Cbus::cfg_gpio_as_gpio()
{
    
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    
    GPIO_InitStruct.Pin = CFG_I2C1_GPIO_pin_SCL | CFG_I2C1_GPIO_pin_SDA;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
    
}


//////////////////////
/// Master methods ///
//////////////////////

//! \brief Receives one frame over I2C
//! \param i2cAddr The I2C bus address of the device
//! \param [out] dstBuf Pointer to buffer with data to receive
//! \param bufSize Number of bytes to be received
//! \return true for success, else false
bool I2Cbus::receiveFrame(I2C_Address_t i2cAddr, uint8_t *dstBuf, uint8_t bufSize)
{
    select();

    bool success = HAL_I2C_Master_Receive(&m_i2c, i2cAddr, dstBuf, bufSize, CFG_I2C_BUS_ERR_TIMEOUT) == HAL_OK ? true : false;
    
    deselect();

    return success;
}

//! \brief Receives one frame over I2C
//! \param i2cAddr The I2C bus address of the device
//! \param deviceInternalAddress 8-bit Address of device internal source register.
//! \param [out] dstBuf Pointer to buffer with data to receive
//! \param bufSize Number of bytes to be received
//! \return true for success, else false
bool I2Cbus::receiveFrame(I2C_Address_t i2cAddr, uint8_t deviceInternalAddress, uint8_t *dstBuf, uint8_t bufSize)
{
    select();

    bool success = HAL_I2C_Mem_Read(&m_i2c, i2cAddr, deviceInternalAddress, 1, dstBuf, bufSize, CFG_I2C_BUS_ERR_TIMEOUT) == HAL_OK ? true : false;
    
    deselect();

    return success;
}

//! \brief Receives one frame over I2C
//! \param i2cAddr The I2C bus address of the device
//! \param deviceInternalAddress 16-bit Address of device internal source register.
//! \param [out] dstBuf Pointer to buffer with data to receive
//! \param bufSize Number of bytes to be received
//! \return true for success, else false
bool I2Cbus::receiveFrame(I2C_Address_t i2cAddr, uint16_t deviceInternalAddress, uint8_t *dstBuf, uint8_t bufSize)
{
    select();

    bool success = HAL_I2C_Mem_Read(&m_i2c, i2cAddr, deviceInternalAddress, 2, dstBuf, bufSize, CFG_I2C_BUS_ERR_TIMEOUT) == HAL_OK ? true : false;

    deselect();

    return success;
}


//! \brief Sends one frame over I2C
//! \param i2cAddr The I2C bus address of the device
//! \param srcBuf Pointer to buffer with data to send
//! \param bufSize Number of bytes to be sent, max 15
//! \return true for success, else false
bool I2Cbus::sendFrame(I2C_Address_t i2cAddr, const uint8_t *srcBuf, uint8_t bufSize)
{

    select();

    bool success = HAL_I2C_Master_Transmit(&m_i2c, i2cAddr, const_cast<uint8_t*>(srcBuf), bufSize, CFG_I2C_BUS_ERR_TIMEOUT) == HAL_OK ? true : false;

    deselect();
    
    return success;
    
}

//! \brief Sends one frame over I2C
//! \param i2cAddr The I2C bus address of the device
//! \param deviceInternalAddress 8-bit Address of device internal destination register.
//! \param srcBuf Pointer to buffer with data to send
//! \param bufSize Number of bytes to be sent, max 15
//! \return true for success, else false
bool I2Cbus::sendFrame(I2C_Address_t i2cAddr, uint8_t deviceInternalAddress, const uint8_t *srcBuf, uint8_t bufSize)
{

    select();

    bool success = HAL_I2C_Mem_Write(&m_i2c, i2cAddr, deviceInternalAddress, 1, const_cast<uint8_t*>(srcBuf), bufSize, CFG_I2C_BUS_ERR_TIMEOUT) == HAL_OK ? true : false;

    deselect();
    
    return success;

}

//! \brief Sends one frame over I2C
//! \param i2cAddr The I2C bus address of the device
//! \param deviceInternalAddress 16-bit Address of device internal destination register.
//! \param srcBuf Pointer to buffer with data to send
//! \param bufSize Number of bytes to be sent, max 14
//! \return true for success, else false
bool I2Cbus::sendFrame(I2C_Address_t i2cAddr, uint16_t deviceInternalAddress, const uint8_t *srcBuf, uint8_t bufSize)
{

    select();

    bool success = HAL_I2C_Mem_Write(&m_i2c, i2cAddr, deviceInternalAddress, 2, const_cast<uint8_t*>(srcBuf), bufSize, CFG_I2C_BUS_ERR_TIMEOUT) == HAL_OK ? true : false;
        
    deselect();
    
    return success;

}



//////////////////////////
/// End Master methods ///
//////////////////////////



//! \brief Selects the registed device
void I2Cbus::select()
{
    m_pMutex->lock();
}

//! \brief Deselects the registed device
void I2Cbus::deselect()
{
    m_pMutex->release();
}


int32_t I2Cbus::getBusTimeMs()
{
    return OS_GetTime();
}

// Private define ------------------------------------------------------------*/
#define  I2C_EVENT_MASTER_BYTE_RECEIVED_NO_BUSY            ((uint32_t)0x00000040)  // RxNE
#define  I2C_EVENT_MASTER_BYTE_RECEIVED_BTF                ((uint32_t)0x00030044)  // BUSY, MSL, RXNE, BTF

//If you are in debugging (something else), then there seems to be a 'bug' which
//causes i2c to get stuck in the intinite ISR loop.  This is to clear it up.
//Note: the problem does not appear (or at least easily) without debugger.
//But this is tested to help
volatile static int dummy_;

#pragma optimize=speed
void I2Cbus::isr_I2Cx()
{

    //TODO: Implement interrupt handling

}


#pragma optimize=speed
void I2Cbus::isr_err_I2Cx()
{
    
    //TODO: Implement proper error and recovery handling
}


/**
* @brief I2C MSP Initialization
* This function configures the hardware resources used in this example
* @param hi2c: I2C handle pointer
* @retval None
*/
void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
    if(hi2c->Instance==I2C1)
    {
        //Initializes the peripherals clock 
        PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
        PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
        if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
        {
            //Error_Handler(); //TODO
        }

        __HAL_RCC_GPIOB_CLK_ENABLE();
        /**I2C1 GPIO Configuration    
        PB6     ------> I2C1_SCL
        PB7     ------> I2C1_SDA 
        */
        GPIO_InitStruct.Pin = CFG_I2C1_GPIO_pin_SCL | CFG_I2C1_GPIO_pin_SDA;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
        GPIO_InitStruct.Pull = GPIO_PULLUP;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

        /* Peripheral clock enable */
        __HAL_RCC_I2C1_CLK_ENABLE();
        /* I2C1 interrupt Init */
        HAL_NVIC_SetPriority(I2C1_EV_IRQn, 5, 0);
        HAL_NVIC_EnableIRQ(I2C1_EV_IRQn);
        HAL_NVIC_SetPriority(I2C1_ER_IRQn, 5, 0);
        HAL_NVIC_EnableIRQ(I2C1_ER_IRQn);
    }  
}

/**
  * @brief This function handles I2C1 event interrupt / I2C1 wake-up interrupt through EXTI line 23.
  */
void I2C1_EV_IRQHandler(void)
{
    I2Cbus::get()->isr_I2Cx();
}

/**
  * @brief This function handles I2C1 error interrupt.
  */
void I2C1_ER_IRQHandler(void)
{
    I2Cbus::get()->isr_err_I2Cx();
}

//======== End of I2Cbus.cpp ==========

