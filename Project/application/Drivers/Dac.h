//! \file    Dac.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    02.05.2011

#ifndef _DAC_H
#define _DAC_H

// header includes
#include <cstdint>

// forward declarations

////

//! \brief The Dac class wraps the DAC on the STM32 family\n
//! For STM32F devices: Uses DMA2 channels 3 and 4 when generating waveforms on the 2 available DAC channels\n
//! For STM32L devices: Uses DMA1 channels 2 and 3 when generating waveforms on the 2 available DAC channels
class Dac
{
public:

    enum DacChannel_t {DAC_CH1=DAC_Channel_1, DAC_CH2=DAC_Channel_2};
    
    struct DacInitStruct_t
    {
        TIM_TypeDef * wavePlaybackTimer;
    };

    // constructor/destructor
    explicit Dac(uint8_t channelMask, const DacInitStruct_t& periphInit);
    virtual ~Dac();

    bool write(DacChannel_t channel, uint16_t data);
    bool writeAsMillivolts(DacChannel_t channel, uint16_t data);

    bool playWaveform(DacChannel_t channel, const uint16_t * pData, uint32_t numsamples, uint32_t sampleFreq, uint16_t repeatCount);
    bool playWaveform(DacChannel_t channel, const uint8_t * pData, uint32_t numsamples, uint32_t sampleFreq, uint16_t repeatCount);
    bool stopWaveformPlaybacks();

    //! \brief Returns the DAC's range, e.g. 4096 steps for a 12 bit DAC (2�^12)
    uint32_t getRange() const { return DAC_RANGE; }

    //! \brief This DACs is internal, i.e. always connected
    bool isConnected() const { return true; }

    static const uint16_t MAX_NUM_DAC_CHANNELS = 2;

protected:
    enum DacMode_t {DACMODE_INIT, DACMODE_WRITE, DACMODE_PLAYBACK};

private:
    Dac(const Dac& right);            //!< block the copy constructor
    Dac& operator=(const Dac& right); //!< block the assignment operator
    enum SampleSize_t {SAMPLE_12BIT, SAMPLE_8BIT};
    bool playWaveformCommon(DacChannel_t channel, const void * pData, uint32_t numsamples,
                       uint32_t sampleFreq, uint16_t repeatCount, SampleSize_t sampleSize);
    TIM_TypeDef * m_playbackTimer;

    DacMode_t m_mode[2];

    static Dac* m_pDac;  //!< static member to keep track of instance. Needed to prevent multiple instances

    static const uint32_t DAC_RANGE = 4096;                   //!< 12 bits DAC range

};


#endif   // _DAC_H

//======== End of Dac.h ==========

