//! \file    EmulatedEEprom.cpp
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    26.10.2010

// include self first
#include "EmulatedEEprom.h"

// other includes
#include "utilfuncs.h"

#if (NB_OF_VAR != 16) && (NB_OF_VAR != 32) && (NB_OF_VAR != 64) && (NB_OF_VAR != 128)
  #error "Invalid emulated eeprom memory size"
#endif

extern const uint16_t VirtAddVarTab[NB_OF_VAR]; // for external linkage, since C++ defaults to internal linkage

// Virtual address defined by the user: 0xFFFF value is prohibited
const uint16_t VirtAddVarTab[NB_OF_VAR] =
{
 0x0000, 0x0101, 0x0202, 0x0303,
 0x0404, 0x0505, 0x0606, 0x0707,
 0x0808, 0x0909, 0x0A0A, 0x0B0B,
 0x0C0C, 0x0D0D, 0x0E0E, 0x0F0F

#if (NB_OF_VAR > 16)    // 32 locations
 ,
 0x1010, 0x1111, 0x1212, 0x1313,
 0x1414, 0x1515, 0x1616, 0x1717,
 0x1818, 0x1919, 0x1A1A, 0x1B1B,
 0x1C1C, 0x1D1D, 0x1E1E, 0x1F1F
#endif

#if (NB_OF_VAR > 32)    // 64 locations
 ,
 0x2020, 0x2121, 0x2222, 0x2323,
 0x2424, 0x2525, 0x2626, 0x2727,
 0x2828, 0x2929, 0x2A2A, 0x2B2B,
 0x2C2C, 0x2D2D, 0x2E2E, 0x2F2F,
 0x3030, 0x3131, 0x3232, 0x3333,
 0x3434, 0x3535, 0x3636, 0x3737,
 0x3838, 0x3939, 0x3A3A, 0x3B3B,
 0x3C3C, 0x3D3D, 0x3E3E, 0x3F3F
#endif

#if (NB_OF_VAR > 64)    // 128 locations
 ,
 0x4040, 0x4141, 0x4242, 0x4343,
 0x4444, 0x4545, 0x4646, 0x4747,
 0x4848, 0x4949, 0x4A4A, 0x4B4B,
 0x4C4C, 0x4D4D, 0x4E4E, 0x4F4F,
 0x5050, 0x5151, 0x5252, 0x5353,
 0x5454, 0x5555, 0x5656, 0x5757,
 0x5858, 0x5959, 0x5A5A, 0x5B5B,
 0x5C5C, 0x5D5D, 0x5E5E, 0x5F5F,

 0x6060, 0x6161, 0x6262, 0x6363,
 0x6464, 0x6565, 0x6666, 0x6767,
 0x6868, 0x6969, 0x6A6A, 0x6B6B,
 0x6C6C, 0x6D6D, 0x6E6E, 0x6F6F,
 0x7070, 0x7171, 0x7272, 0x7373,
 0x7474, 0x7575, 0x7676, 0x7777,
 0x7878, 0x7979, 0x7A7A, 0x7B7B,
 0x7C7C, 0x7D7D, 0x7E7E, 0x7F7F
#endif

};


static inline uint16_t byteAddr2WordAddr(uint16_t byteAddr)
{
  return byteAddr/2;
}

//! \brief Constructor
EmulatedEEprom::EmulatedEEprom()
{
  // Unlock the Flash Program Erase controller
  HAL_FLASH_Unlock();

  // EEPROM Init
  EE_Init();
}

//! destructor
EmulatedEEprom::~EmulatedEEprom()
{
}

//! \brief  Writes one byte to the emulated EEPROM.
//! \param  data : The data to be written to the EEPROM.
//! \param  writeAddr : EEPROM's internal address to write to.
//! \return true for success, else false
bool EmulatedEEprom::eeWrite(uint8_t data, uint16_t writeAddr) const
{
    bool success = false;
    if (writeAddr < sEE_MEMSIZE)
    {
        uint16_t writeData = 0xFFFF;
        if ( eeRead(writeAddr, writeData) )
        {
            writeData = data | (writeData & 0xFF00);
            success = eeWrite(writeData, writeAddr);
        }
    }
    return success;
}

//! \brief  Writes one 16 bit word to the emulated EEPROM.
//! \param  data : The data to be written to the EEPROM.
//! \param  writeAddr : EEPROM's internal address to write to.
//! \return true for success, else false
bool EmulatedEEprom::eeWrite(uint16_t data, uint16_t writeAddr) const
{
    bool success = false;
    if ((writeAddr+1) < sEE_MEMSIZE)
    {
        if (writeAddr % 2 == 0)
        {
            success = EE_WriteVariable(VirtAddVarTab[byteAddr2WordAddr(writeAddr)], data) == HAL_OK ? true : false;
        }
        else
        {
            uint16_t currData1 = 0xFFFF;
            uint16_t currData2 = 0xFFFF;
            uint16_t status1 = EE_ReadVariable(VirtAddVarTab[byteAddr2WordAddr(writeAddr)], &currData1);
            uint16_t status2 = EE_ReadVariable(VirtAddVarTab[byteAddr2WordAddr(writeAddr+1)], &currData2);

            if ( (0 == status1 || 1 == status1) && (0 == status2 || 1 == status2) )
            {
              if ( EE_WriteVariable(VirtAddVarTab[byteAddr2WordAddr(writeAddr)], (currData1 & 0x00FF) | (data << 8)) == HAL_OK )
              {
                  success = EE_WriteVariable(VirtAddVarTab[byteAddr2WordAddr(writeAddr+1)], (currData2 & 0xFF00) | (data >> 8)) == HAL_OK ? true : false;
              }
            }
        }
    }
    return success;
}

//! \brief  Writes one 32 bit word to the emulated EEPROM.
//! \param  data : The data to be written to the EEPROM.
//! \param  writeAddr : EEPROM's internal address to write to.
//! \return true for success, else false
bool EmulatedEEprom::eeWrite(uint32_t data, uint16_t writeAddr) const
{
    if ( eeWrite(static_cast<uint16_t>(data), writeAddr) )
    {
        writeAddr += 2;
        if ( eeWrite(static_cast<uint16_t>(data >> 16), writeAddr) )
        {
            return true;
        }
    }
    return false;
}

//! \brief  Writes buffer of data to the emulated EEPROM.
//! \param  pBuffer : pointer to the buffer  containing the data to be written to the EEPROM.
//! \param  writeAddr : EEPROM's internal address to write to.
//! \param  numByteToWrite : number of bytes to write to the EEPROM.
//! \return true for success, else false
bool EmulatedEEprom::eeWrite(const uint8_t * pBuffer, uint16_t writeAddr, uint16_t numByteToWrite) const
{
    if (numByteToWrite != 0 && (writeAddr+numByteToWrite) <= sEE_MEMSIZE)
    {
        uint16_t adr = writeAddr;

        while (adr < (writeAddr + numByteToWrite))
        {
            if ( adr % 2 == 0 && (adr+1) < (writeAddr + numByteToWrite) )
            {
                uint16_t writeData = *pBuffer++;
                uint16_t tmp = *pBuffer++;
                writeData |= (tmp << 8);
                if ( ! eeWrite(writeData, adr) )
                {
                    return false;
                }
                adr += 2;
            }
            else
            {
              if ( ! eeWrite(*pBuffer++, adr++) )
              {
                  return false;
              }
            }
        }
        return true;
    }
    return false;
}

//! \brief  Reads one byte from the emulated EEPROM.
//! \param  readAddr : EEPROM's internal address to read from.
//! \param  [out] data : Reference to where to store the read data
//! \return true for success, else false
bool EmulatedEEprom::eeRead(uint16_t readAddr, uint8_t& data) const
{
    if (readAddr < sEE_MEMSIZE)
    {
        uint16_t dataRead = 0xFFFF; // if not exist yet, simulate empty EEPROM
        uint16_t status = EE_ReadVariable(VirtAddVarTab[byteAddr2WordAddr(readAddr)], &dataRead);
        if (0 == status || 1 == status)
        {
            if (readAddr % 2 == 0)
            {
                data = LOBYTE(dataRead);
            }
            else
            {
                data = HIBYTE(dataRead);
            }
            return true;
        }
    }
    return false;
}

//! \brief  Reads one 16 bit word from the emulated EEPROM.
//! \param  readAddr : EEPROM's internal address to read from.
//! \param  [out] data : Reference to where to store the read data
//! \return true for success, else false
bool EmulatedEEprom::eeRead(uint16_t readAddr, uint16_t& data) const
{
    if ((readAddr+1) < sEE_MEMSIZE)
    {
        if (readAddr % 2 == 0)
        {
            uint16_t dataRead = 0xFFFF; // if not exist yet, simulate empty EEPROM
            uint16_t status = EE_ReadVariable(VirtAddVarTab[byteAddr2WordAddr(readAddr)], &dataRead);
            if (0 == status || 1 == status)
            {
                data = dataRead;
                return true;
            }
        }
        else
        {
            uint16_t dataRead1 = 0xFFFF;
            uint16_t dataRead2 = 0xFFFF;
            uint16_t status1 = EE_ReadVariable(VirtAddVarTab[byteAddr2WordAddr(readAddr)], &dataRead1);
            uint16_t status2 = EE_ReadVariable(VirtAddVarTab[byteAddr2WordAddr(readAddr+1)], &dataRead2);

            if ( (0 == status1 || 1 == status1) && (0 == status2 || 1 == status2) )
            {
                data = (dataRead1 >> 8) | (dataRead2 << 8);
                return true;
            }
        }
    }
    return false;
}

//! \brief  Reads one 32 bit word from the emulated EEPROM.
//! \param  readAddr : EEPROM's internal address to read from.
//! \param  [out] data : Reference to where to store the read data
//! \return true for success, else false
bool EmulatedEEprom::eeRead(uint16_t readAddr, uint32_t& data) const
{
    if ((readAddr+3) < sEE_MEMSIZE)
    {
        uint16_t dataRead;
        if ( eeRead(readAddr, dataRead) )
        {
          data = dataRead;
          readAddr += 2;
          if ( eeRead(readAddr, dataRead) )
          {
            data |= static_cast<uint32_t>(dataRead) << 16;
            return true;
          }
        }
    }
    return false;
}

//! \brief  Reads a block of data from the emulated EEPROM.
//! \param  pBuffer : pointer to the buffer that receives the data read from the EEPROM.
//! \param  readAddr : EEPROM's internal address to read from.
//! \param  numByteToRead : number of bytes to read from the EEPROM.
//! \return true for success, else false
bool EmulatedEEprom::eeRead(uint8_t * pBuffer, uint16_t readAddr, uint16_t numByteToRead) const
{
    if (numByteToRead != 0 && (readAddr+numByteToRead) <= sEE_MEMSIZE)
    {
      for (uint16_t adr = readAddr; adr < (readAddr + numByteToRead); ++adr)
      {
        if ( !eeRead(adr, *pBuffer++) )
        {
          return false;
        }
      }
      return true;
    }
    return false;
}
