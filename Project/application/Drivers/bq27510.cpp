//! \file    bq27510.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    19.05.2014


// include self first
#include "bq27510.h"

// other includes
#include "CommunicationUtility.h"
#include "utilfuncs.h"


// static member init
Bq27510*  Bq27510::m_pBq27510 = 0;

USING_NAMESPACE_STD;

using namespace bls_2010;

static Mutex * m_mutex = 0;

//! \brief Constructor
//! \param pI2C Reference to I2C driver\n
//
Bq27510::Bq27510(I2Cbus *pI2C)
{
    //OS_EnterRegion();
    if (0 == m_pBq27510)
    {
        m_pBq27510 = this;
        m_pI2C = pI2C;
        
        m_communicationEnabled = true; //Default enabled
        
    }
    //OS_LeaveRegion();
}

//
//! destructor
//
Bq27510::~Bq27510()
{

    //OS_EnterRegion();
    m_pBq27510 = 0;
    //OS_LeaveRegion();
}

//
//! \brief Get current, remaining capacity, voltage and temperature
//! \param result Reference to a ProbeResult struct\n
//! \return false = I2C communication error, false = I2C communication OK\n
//
bool Bq27510::probe(Bq27510::ProbeResult& result)
{
    bool success = true;
    
    uint16_t ui16Value;
    
    success &= getAverageCurrentuA(result.averageCurrent_uA);
    
    success &= (getFlags(ui16Value));
    result.charging = !(ui16Value & 0x01);
    
    success &= getAtRate(result.current_uA);
    success &= getStateOfCharge(result.remainingCapacityPercent);
    success &= getVoltage(result.voltage_mV);
    success &= getTemperature(result.temperature_mC);
        
    return success;
}

//! \brief Get device type
//! \param value Reference to a 16 bit variable\n
//! \return false = I2C communication error, false = I2C communication OK\n
//
bool Bq27510::getDeviceType(uint16_t &value)
{
    Lock();
    
    bool success = false;
    value = 0;
    
    if(m_communicationEnabled)
    {
        uint8_t subCmd[2];
        uint8_t devType[2];
        
        subCmd[0] = SCMD_DEVICE_TYPE_L;
        subCmd[1] = SCMD_DEVICE_TYPE_H;
        
        success = true;
            
        success &= m_pI2C->sendFrame(BQ27510_ADDR, CMD_CONTROL_L, subCmd, ARR_ELEMS(subCmd));
        success &= m_pI2C->receiveFrame(BQ27510_ADDR, CMD_CONTROL_L, devType, ARR_ELEMS(devType));
        value = CommunicationUtility::combine(devType[1], devType[0]);
    }
    
    Unlock();

    return success;
}

//! \brief Get remaining battery capacity
//! \param value Reference to a 8 bit variable (for battery capacity in %)
//! \return false = I2C communication error, false = I2C communication OK\n
//
bool Bq27510::getStateOfCharge(int8_t &value)
{
    Lock();
    
    bool success = false;
    value = 0;
    
    if(m_communicationEnabled)
    {
        uint8_t val[1];
        
        success = m_pI2C->receiveFrame(BQ27510_ADDR, CMD_STATE_OF_CHARGE_L, val, ARR_ELEMS(val));
        value = val[0];
    }

    Unlock();
    
    return success;
}

//
//! \brief Get voltage
//! \param value Reference to a int32 bit variable (for battery voltage in mv)
//! \return false = I2C communication error, false = I2C communication OK\n
//
bool Bq27510::getVoltage(int32_t &value)
{
    Lock();
    
    bool success = false;
    value = 0;
    
    if(m_communicationEnabled)
    {
        uint8_t val[2];
        
        success = m_pI2C->receiveFrame(BQ27510_ADDR, CMD_VOLTAGE_L, val, ARR_ELEMS(val));
        value = CommunicationUtility::combine(val[1], val[0]);
    }

    Unlock();
    
    return success;
}

//
//! \brief Get atRate
//! \param value Reference to a int32 bit variable (for battery current in mA)
//! \return false = I2C communication error, false = I2C communication OK\n
//
bool Bq27510::getAtRate(int32_t &value)
{
    Lock();
    
    bool success = false;
    value = 0;
    
    if(m_communicationEnabled)
    {
        uint8_t val[2];
        
        success = m_pI2C->receiveFrame(BQ27510_ADDR, CMD_AT_RATE_L, val, ARR_ELEMS(val));
        value = (int16_t)CommunicationUtility::combine(val[1], val[0]);
    }

    Unlock();
    
    return success;
    
}

//
//! \brief Get DesignCapacity
//! \param value Reference to a uint16 bit variable (for battery design capacity in mA)
//! \return false = I2C communication error, false = I2C communication OK\n
//
bool Bq27510::getDesignCapacity(uint16_t &value)
{
    Lock();
    
    bool success = false;
    value = 0;
    
    if(m_communicationEnabled)
    {
        uint8_t val[2];
        
        success = m_pI2C->receiveFrame(BQ27510_ADDR, CMD_DESIGN_CAPACITY_L, val, ARR_ELEMS(val));
        value = CommunicationUtility::combine(val[1], val[0]);
    }

    Unlock();
    
    return success;
    
}

//
//! \brief Get AverageCurrent
//! \param value Reference to a int32 bit variable (for battery current in uA)
//! \return false = I2C communication error, false = I2C communication OK\n
//
bool Bq27510::getAverageCurrentuA(int32_t &value)
{
    Lock();
    
    bool success = false;
    value = 0;
    
    if(m_communicationEnabled)
    {
        uint8_t val[2];
        
        success = m_pI2C->receiveFrame(BQ27510_ADDR, CMD_AVG_CURRENT_L, val, ARR_ELEMS(val));
        value = (int16_t)CommunicationUtility::combine(val[1], val[0]) * 1000;
    }

    Unlock();
    
    return success;
}

//
//! \brief Get Temperature
//! \param value Reference to a int32 bit variable (for battery temperature in mC)
//! \return false = I2C communication error, false = I2C communication OK\n
//
bool Bq27510::getTemperature(int32_t &value)
{
    Lock();
    
    bool success = false;
    value = 0;
    
    if(m_communicationEnabled)
    {
        uint8_t val[2];
        
        success = m_pI2C->receiveFrame(BQ27510_ADDR, CMD_TEMPERATURE_L, val, ARR_ELEMS(val));
        value = CommunicationUtility::combine(val[1], val[0]) * 1000;
    }

    Unlock();
    
    return success;
}

//
//! \brief Get Flags
//! \param value Reference to a uint16 bit variable (for battery flags)
//! \return false = I2C communication error, false = I2C communication OK\n
bool Bq27510::getFlags(uint16_t &value)
{
    Lock();
    
    bool success = false;
    value = 0;
    
    if(m_communicationEnabled)
    {
        uint8_t val[2];
        
        success = m_pI2C->receiveFrame(BQ27510_ADDR, CMD_FLAGS_L, val, ARR_ELEMS(val));
        value = CommunicationUtility::combine(val[1], val[0]);
    }

    Unlock();
    
    return success;
}

//
//! \brief Get NominalAvailableCapacity
//! \param value Reference to a uint16 variable (for battery capacity in mA)
//! \return false = I2C communication error, false = I2C communication OK\n
//
bool Bq27510::getNomAvailableCapacity(uint16_t &value)
{
    Lock();
    
    bool success = false;
    value = 0;
    
    if(m_communicationEnabled)
    {
        uint8_t val[2];
        
        success = m_pI2C->receiveFrame(BQ27510_ADDR, CMD_NOM_AVAILABLE_CAPACITY_L, val, ARR_ELEMS(val));
        value = CommunicationUtility::combine(val[1], val[0]);
    }

    Unlock();
    
    return success;
}

//
//! \brief Get FullAvailableCapacity
//! \param value Reference to a uint16 variable (for battery capacity in mA)
//! \return false = I2C communication error, false = I2C communication OK\n
//
bool Bq27510::getFullAvailableCapacity(uint16_t &value)
{
    Lock();
    
    bool success = false;
    value = 0;
    
    if(m_communicationEnabled)
    {
        uint8_t val[2];
        
        success = m_pI2C->receiveFrame(BQ27510_ADDR, CMD_FULL_AVAILABLE_CAPACITY_L, val, ARR_ELEMS(val));
        value = CommunicationUtility::combine(val[1], val[0]);
    }

    Unlock();
    
    return success;
}

//
//! \brief Enable or disable communication with fuelgauge
//! Normal communication must stop e.g. when flashing fuelgauge with golden image
//! \param enable true = enable, false = disable
//
void Bq27510::enableCommunication(bool enable)
{
    if(enable)
    {
        Unlock();
    }
    else
    {
        Lock();
    }
    
    m_communicationEnabled = enable;
}

//
//! \brief Lock fuelgauge
//
bool Bq27510::Lock(void)
{
    if (0==m_mutex)
    {
        m_mutex = new Mutex(); // create when needed
    }
    if (m_mutex)
    {
        m_mutex->lock();
        return true;
    }
    return false;
}

//
//! \brief Unlock fuelgauge
//
bool Bq27510::Unlock(void)
{
    if (m_mutex)
    {
        m_mutex->release();
        return true;
    }
    return false;
}
