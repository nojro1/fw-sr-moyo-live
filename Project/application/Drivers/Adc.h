//! \file    Adc.h
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    05.05.20

#ifndef _ADC_H
#define _ADC_H

// header includes
#include <cstdint>
#include <bitset>
#include "board_cfg.h"
#include "stm32l5xx_hal.h"
#include "stm32l5xx_ll_adc.h"
#include "stm32l5xx_ll_dma.h"
#include "stm32l5xx_ll_gpio.h"
#include "stm32l5xx_ll_tim.h"
#include "stm32l5xx_ll_rcc.h"
#include "stm32l5xx_ll_bus.h"
#include "ringbuffer.h"

// Declaration for ISR
extern "C" { void DMA1_Channel1_IRQHandler(void); }


//! \brief The Adc class wraps the ADC1 on the STM32 family\n
//! Uses DMA1 channel1 for storing the conversion results in a member array

class Adc
{
public:

    enum AD_CH_IDX_TypeDef
    {
        AD_5V_IDX           = 0,
        AD_AMB_LIGHT_IDX    = 1,
        AD_IMPEDANCE_IDX    = 2,
        AD_ECG_IDX          = 3
    };
    
    // constructor/destructor
    Adc();
    ~Adc();

    void     start();
    uint16_t read(uint32_t channel);
    uint16_t readAsMillivolts(uint8_t channel);
    uint32_t readAsMicrovolts(uint8_t channel);
    uint16_t readExternalRefAsMillivolts();
    int16_t  readInternalTemp_C();
    
    
    //! \brief Returns the ADC's range, e.g. 4096 for a 12 bit ADC (2�^12)
    uint32_t getRange() const { return ADC_RANGE; }
    
    //! \brief This ADCs is internal, i.e. always connected
    bool isConnected() const { return true; }
    
    static const uint16_t NO_ADC_SAMPLES_IN_BUFFER = 100;
    static const uint16_t ADC_ECG_SAMPLES_BUF_SIZE = NO_ADC_SAMPLES_IN_BUFFER;
    
    

    
protected:
    uint16_t* m_conversionResults;                          //!< where the DMA puts the ADC conversion results
    static Adc* m_pAdc;  //!< static member to keep track of instance. Needed by isr and to prevent multiple instances
    static RingBuffer*  m_pEcgBuf;
    static Adc* get() { return m_pAdc; }
    

private:
    Adc(const Adc& right);            //!< block the copy constructor
    Adc& operator=(const Adc& right); //!< block the assignment operator

    void init();
    void deinit();
    void cfg_dma();
    void cfg_tim( );
    void init_convres();
    void cfg_adc_calibrate();
    void conversionComplete(const uint16_t * pData, uint16_t numsamples);
    uint16_t averageChannelX(uint32_t channelIdx, const uint16_t * pData, uint16_t numsamples);
    void isrx();
    
    friend void DMA1_Channel1_IRQHandler(void);
    
    static const uint8_t NUM_ADC_CHANNELS           = 4;
    static const uint16_t ADC_SAMPLE_FREQ           = 500;
    static const uint16_t ADC_DMA_BUF_SIZE          = (NUM_ADC_CHANNELS * NO_ADC_SAMPLES_IN_BUFFER);
    
    static const uint32_t ADC_RANGE                 = 4096;             //!< 12 bits ADC range

    static const uint16_t VREF_INTERNAL             = 1200;             //!< Internal reference voltage in mV

    uint16_t m_adc_ch_1;
    uint16_t m_adc_ch_2;
    uint16_t m_adc_ch_3;
    uint16_t m_adc_ch_4;
    


};

#endif   // _ADC_H

//======== End of Adc.h ==========
