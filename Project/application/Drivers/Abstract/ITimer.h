//! \file    ITimer.h
//! \author  DRY
//! \version 1.0
//! \date    20.04.2012
//! \brief   Timer Iterface

#ifndef ITIMER_H
#define ITIMER_H

#include <cstdint>

//Factored the iface from OsTimer.h, to allow for alternative implementations

class ITimer
{
  public:

    virtual ~ITimer() { }

    //! \brief typedef of the callback routine
    //! \param pCallbackArgs Pointer to callback argument
    typedef void (*ITimerCallback)(void *pCallbackArgs);

    virtual int create(std::uint32_t initialDelay,
                       std::uint32_t periodTime,
                       std::uint32_t numPeriods,
                       ITimerCallback pCallbackFunc,
                       void * pCallbackArgs) = 0;

    virtual int remove() = 0;

    virtual int start() = 0;

    virtual int restart()  = 0;

    virtual int stop() = 0;

    virtual bool isRunning() = 0;
};

class ITimerFactory
{
  public:

    virtual ITimer* newTimer() = 0;

    virtual bool freeTimer(ITimer*) = 0;
};

#endif // ITIMER_H
