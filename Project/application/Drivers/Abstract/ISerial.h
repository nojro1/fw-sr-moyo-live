//! \file    ISerial.h
//! \author  DRY
//! \version 1.0
//! \date    25.08.2010

#ifndef IFACE_SERIAL_PORT_H_
#define IFACE_SERIAL_PORT_H_

#ifdef _MSC_VER
  #if (_MSC_VER < 1600) /* Vs 2010 should have cstdint */
    #include "stdint.h"
  #else
    #include <cstdint>
  #endif
#else
  #include <cstdint>
#endif

#if defined(__cplusplus)
 #ifndef __embedded_cplusplus
using std::uint8_t; using std::uint16_t; using std::int16_t; using std::uint32_t;
 #endif
#endif


class ISerial
{
  public:

    virtual ~ISerial() { }

    virtual bool init() = 0;

    virtual bool deinit() = 0;

    virtual int write(uint8_t _data) = 0;

    virtual int write(const uint8_t* _pData, uint16_t _len) = 0;

    virtual int read() =0;

    virtual int read(uint8_t * _pData, uint16_t _len) = 0;

    virtual int read(int16_t timeout) = 0;

    virtual int read(uint8_t * _pData, uint16_t _len, int16_t timeout) = 0;

    virtual void flush(const int& timeout=0) = 0;
    
    virtual uint32_t getBaud() const { return 0; }
    virtual uint32_t setBaud(uint32_t /*baudRate*/) { return 0; }

    virtual void disable() {}
    virtual void enable() {}
    virtual void reset() {}

    virtual bool hasUSBHostConnection() const { return false; }
    virtual bool isUSBHostConnectionOK() const { return true; }

#ifndef _MSC_VER
    virtual bool hasTxBufferSpaceAvailable() const = 0;
    virtual int  getTxBufferSpaceAvailable() const = 0;
    virtual int  getTxBufferWatermark() = 0;    
    virtual int  getTxBufferSize() const = 0;

    virtual int  getRxBufferWatermark() = 0;
    virtual int  getRxBufferSize() const = 0;

#endif

};


#endif /* IFACE_SERIAL_PORT_H_ */
