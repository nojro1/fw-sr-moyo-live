//! \file    IPwmGen.h
//! \author  GIT/DRY
//! \version 1.1
//! \date    12.06.2012

#ifndef _I_PWMGEN_H
#define _I_PWMGEN_H

// header includes
#ifdef _MSC_VER
  #if (_MSC_VER < 1600) /* Vs 2010 should have cstdint */
    #include "stdint.h"
  #else
    #include <cstdint>
  #endif
#else
  #include <cstdint>
#endif

#if defined(__cplusplus)
 #ifndef __embedded_cplusplus
   using std::uint8_t; using std::uint16_t;
 #endif
#endif

#include "board_cfg.h"

// forward declarations

////

//! \brief Abstract base class for PWM generators
class IPwmGen
{
public:

    // constructor/destructor
    IPwmGen() {;}
    virtual ~IPwmGen() {;}

    virtual void setDuty(uint8_t ch, uint16_t duty) const = 0;
    virtual uint16_t getDuty(uint8_t ch) const = 0;

protected:

private:

};


#endif   // _I_PWMGEN_H

//======== End of IPwmGen.h ==========

