#ifndef _EEPROM_BASE_H
#define _EEPROM_BASE_H

#ifdef _MSC_VER
  #if (_MSC_VER < 1600) /* Vs 2010 should have cstdint */
    #include "stdint.h"
  #else 
    #include <cstdint>
  #endif
#else
  #include <cstdint>
#endif

// forward declarations

//! \brief Abstract base class for EEPROMs.
class EEPROM_Base
{
    public:

        virtual ~EEPROM_Base() {;}

        //! \brief  Switch EEPROM power ON (if supported by HW)
        virtual void powerOn() const {;}
        //! \brief  Switch EEPROM power OFF (if supported by HW)
        virtual void powerOff() {;}

        // user memory commands

        //! \brief  Writes one byte to the EEPROM.
        //! \param  data : The data to be written to the EEPROM.
        //! \param  writeAddr : EEPROM's internal address to write to.
        //! \return true for success, else false
        virtual bool eeWrite(std::uint8_t data, std::uint16_t writeAddr) const = 0;

        //! \brief  Writes one 16 bit word to the EEPROM.
        //! \param  data : The data to be written to the EEPROM.
        //! \param  writeAddr : EEPROM's internal address to write to.
        //! \return true for success, else false
        virtual bool eeWrite(std::uint16_t data, std::uint16_t writeAddr) const = 0;

        //! \brief  Writes one 32 bit word to the EEPROM.
        //! \param  data : The data to be written to the EEPROM.
        //! \param  writeAddr : EEPROM's internal address to write to.
        //! \return true for success, else false
        virtual bool eeWrite(std::uint32_t data, std::uint16_t writeAddr) const = 0;

        //! \brief  Writes buffer of data to the EEPROM.
        //! \param  pBuffer : pointer to the buffer  containing the data to be written to the EEPROM.
        //! \param  writeAddr : EEPROM's internal address to write to.
        //! \param  numByteToWrite : number of bytes to write to the EEPROM.
        //! \return true for success, else false
        virtual bool eeWrite(const std::uint8_t* pBuffer, std::uint16_t writeAddr, std::uint16_t numByteToWrite) const = 0;

        //! \brief  Reads one byte from the EEPROM.
        //! \param  readAddr : EEPROM's internal address to read from.
        //! \param  [out] data : Reference to where to store the read data
        //! \return true for success, else false
        virtual bool eeRead(std::uint16_t readAddr, std::uint8_t& data) const = 0;

        //! \brief  Reads one 16 bit word from the EEPROM.
        //! \param  readAddr : EEPROM's internal address to read from.
        //! \param  [out] data : Reference to where to store the read data
        //! \return true for success, else false
        virtual bool eeRead(std::uint16_t readAddr, std::uint16_t& data) const = 0;

        //! \brief  Reads one 32 bit word from the EEPROM.
        //! \param  readAddr : EEPROM's internal address to read from.
        //! \param  [out] data : Reference to where to store the read data
        //! \return true for success, else false
        virtual bool eeRead(std::uint16_t readAddr, std::uint32_t& data) const = 0;

        //! \brief  Reads a block of data from the EEPROM.
        //! \param  pBuffer : pointer to the buffer that receives the data read from the EEPROM.
        //! \param  readAddr : EEPROM's internal address to read from.
        //! \param  numByteToRead : number of bytes to read from the EEPROM.
        //! \return true for success, else false
        virtual bool eeRead(std::uint8_t* pBuffer, std::uint16_t readAddr, std::uint16_t numByteToRead) const = 0;

        virtual std::uint32_t getMemorySize() const = 0;

    protected:

    private:

};

#endif // _EEPROM_BASE_H

