//! \file    I2Ccfg.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    23.03.2010

#ifndef _I2C_CFG_H
#define _I2C_CFG_H

// header includes
#include "isrpriocfg.h"
#include "board_cfg.h"


// This section is not supposed to be modified.
// --------------------------------------------

//! \brief Definition for I2C1 based I/O ports
#if CFG_I2C1_REMAP == 0
#define CFG_I2C1_SCL_SDA_GPIOx              GPIOB
#define CFG_I2C1_GPIO_pin_SCL               GPIO_PIN_6
#define CFG_I2C1_GPIO_pin_SDA               GPIO_PIN_7
#define CFG_I2C1_GPIO_CLK_EN                __HAL_RCC_GPIOB_CLK_ENABLE();
#define CFG_I2C1_GPIO_CLK_DIS               __HAL_RCC_GPIOB_CLK_DISABLE();
#define CFG_I2C1_I2C_CLK                    RCC_PERIPHCLK_I2C1
#elif CFG_I2C1_REMAP == 1
#define CFG_I2C1_SCL_SDA_GPIOx              GPIOB
#define CFG_I2C1_GPIO_pin_SCL               GPIO_PIN_8
#define CFG_I2C1_GPIO_pin_SDA               GPIO_PIN_9
#define CFG_I2C1_GPIO_CLK_EN                __HAL_RCC_GPIOB_CLK_ENABLE()
#define CFG_I2C1_GPIO_CLK_DIS               __HAL_RCC_GPIOB_CLK_DISABLE()
#define CFG_I2C1_I2C_CLK                    RCC_PERIPHCLK_I2C1
#endif

//! \brief Definition for I2C2 based I/O ports
#if CFG_I2C2_REMAP == 0
#define CFG_I2C2_SCL_SDA_GPIOx              GPIOB
#define CFG_I2C2_GPIO_pin_SCL               GPIO_PIN_10
#define CFG_I2C2_GPIO_pin_SDA               GPIO_PIN_11
#define CFG_I2C2_GPIO_CLK_EN                __HAL_RCC_GPIOB_CLK_ENABLE()
#define CFG_I2C2_GPIO_CLK_DIS               __HAL_RCC_GPIOB_CLK_DISABLE()
#define CFG_I2C2_I2C_CLK                    RCC_PERIPHCLK_I2C2
#endif

#endif   // _I2C_CFG_H

//======== End of I2Ccfg.h ==========

