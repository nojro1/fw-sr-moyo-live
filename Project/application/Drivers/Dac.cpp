//! \file    Dac.cpp
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    02.05.2011

// include self first
#include "Dac.h"

// other includes
#include <board_cfg.h>
#include <adccfg.h>
#include "RTOS.h"
#include <utilfuncs.h>
#include <algorithm>
#include <cassert>


#define DAC_DHR12R1_Address      (DAC_BASE + 0x00000008)
#define DAC_DHR12R2_Address      (DAC_BASE + 0x00000014)

#define DAC_DHR8R1_Address      (DAC_BASE + 0x00000010)
#define DAC_DHR8R2_Address      (DAC_BASE + 0x0000001C)


// static member init
Dac*  Dac::m_pDac = 0;

//! \brief Constructor
//! \param channelMask A 8 bit mask, defining which channels to be used\n
//! Possible values = 0x01 for DAC_Channel_1, 0x02 for DAC_Channel_2 and 0x03 for both channels \n
//! \param periphInit Specified initialisation parameters (Timer used for playback, TIM6/7)
Dac::Dac(uint8_t channelMask, const DacInitStruct_t& periphInit)
{
    OS_EnterRegion();
    if (0 == m_pDac)
    {
        m_playbackTimer = periphInit.wavePlaybackTimer;
        assert(TIM6==m_playbackTimer || TIM7==m_playbackTimer);
        assert(channelMask > 0 && channelMask <= 3);

        m_pDac = this;

        // DMA clock enable
     #if !defined (STM32L1XX_MD)
        RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA2, ENABLE);
     #else
        RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
     #endif

        // GPIOA Periph clock enable
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

        // DAC Periph clock enable
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);

        // Once the DAC channel is enabled, the corresponding GPIO pin is automatically
        // connected to the DAC converter. In order to avoid parasitic consumption,
        // the GPIO pin should be configured in analog
        GPIO_InitTypeDef GPIO_InitStructure;
        GPIO_StructInit(&GPIO_InitStructure);
        GPIO_InitStructure.GPIO_Pin =  0;
        if (channelMask & 0x01)
        {
            GPIO_InitStructure.GPIO_Pin |=  GPIO_Pin_4;
        }
        if (channelMask & 0x02)
        {
            GPIO_InitStructure.GPIO_Pin |=  GPIO_Pin_5;
        }
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
        GPIO_Init(GPIOA, &GPIO_InitStructure);

        m_mode[0] = DACMODE_INIT;
        m_mode[1] = DACMODE_INIT;
    }
    OS_LeaveRegion();
}


//! \brief Destructor
Dac::~Dac()
{
    DAC_DeInit();

    // TIMx disable counter
    TIM_Cmd(m_playbackTimer, DISABLE);
}

//! \brief Writes new output value to the selected DAC channel
//! \param channel DAC channel (DAC_Channel_1, DAC_Channel_2)
//! \param data Data to output (only 12 LSB will be used)
//! \return true if success, else false
//! \remark Calling this method will stop all active playbacks
bool Dac::write(DacChannel_t channel, uint16_t data)
{
    OS_EnterRegion();

    int modeIdx = (channel == DAC_CH1) ? 0 : 1;
    // only (re)configure if needed
    if (m_mode[modeIdx] != DACMODE_WRITE)
    {
        DAC_DeInit();

        // TIMx disable counter
        TIM_Cmd(m_playbackTimer, DISABLE);

      #if !defined (STM32L1XX_MD)
        if (DAC_CH1 == channel)
        {
            DMA_DeInit(DMA2_Channel3);
        }
        else
        {
            DMA_DeInit(DMA2_Channel4);
        }
      #else
        if (DAC_CH1 == channel)
        {
             DMA_DeInit(DMA1_Channel2);
        }
        else
        {
             DMA_DeInit(DMA1_Channel3);
        }
      #endif

        // DAC channels Configuration
        DAC_InitTypeDef  DAC_InitStructure;
        DAC_StructInit(&DAC_InitStructure);
        DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
        DAC_Init(channel, &DAC_InitStructure);

        // Enable DAC Channel
        DAC_Cmd(channel, ENABLE);

        m_mode[modeIdx] = DACMODE_WRITE;

    }

    // Now set the output value on the DAC pin
    if (DAC_CH1 == channel)
    {
        DAC_SetChannel1Data(DAC_Align_12b_R, data);
    }
    else
    {
        DAC_SetChannel2Data(DAC_Align_12b_R, data);
    }

    OS_LeaveRegion();

    return true;
}

//! \brief Writes new output value to the selected DAC channel as mV
//! \param channel DAC channel (DAC_Channel_1, DAC_Channel_2)
//! \param data Data to output, in mV (only 12 LSB will be used)
//! \return true if success, else false
//! \remark calling this method will stop all playbacks
bool Dac::writeAsMillivolts(DacChannel_t channel, uint16_t data)
{
    data = static_cast<uint16_t>( (data*(DAC_RANGE-1)) / ADC_VREF );
    return write(channel, data);
}

bool Dac::playWaveform(DacChannel_t channel, const uint16_t * pData, uint32_t numsamples,
                       uint32_t sampleFreq, uint16_t repeatCount)
{
     //Play 12-bit sound wave
     return playWaveformCommon(channel, (void *)pData, numsamples, sampleFreq, repeatCount,SAMPLE_12BIT);
}

bool Dac::playWaveform(DacChannel_t channel, const uint8_t * pData, uint32_t numsamples,
                       uint32_t sampleFreq, uint16_t repeatCount)
{
     //Play 8-bit sound wave
     return playWaveformCommon(channel, (void *)pData, numsamples, sampleFreq, repeatCount,SAMPLE_8BIT);
}
//! \brief Plays a buffer of samples on the selected DAC channel
//! \param channel DAC channel (DAC_Channel_1, DAC_Channel_2)
//! \param pData Buffer of data to output (only 12 LSB will be used)
//! \param numsamples Number of samples in buffer
//! \param sampleFreq Sample frequency of buffered data
//! \param repeatCount How many times the buffer shall be played. 0=infinite, 1=once (2 or more not supported yet)
//! \return true if success, else false
bool Dac::playWaveformCommon(DacChannel_t channel, const void * pData, uint32_t numsamples,
                       uint32_t sampleFreq, uint16_t repeatCount, SampleSize_t sampleSize)
{
    OS_EnterRegion();

    DAC_DeInit();

    // TIMx Periph clock enable
    if (TIM6 == m_playbackTimer)
    {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
    }
    else
    {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);
    }

    // TIMx Configuration
    // Time base configuration

    RCC_ClocksTypeDef clockInfo;
    RCC_GetClocksFreq(&clockInfo);

    uint32_t timerPeriod = 0;
    uint16_t prescaler = 0;
    const uint32_t TIMxCLK = clockInfo.PCLK1_Frequency*((RCC->CFGR & RCC_CFGR_PPRE1) == RCC_CFGR_PPRE1_DIV1 ? 1 : 2); // the driver clock for the timer
    uint32_t max_count = TIMxCLK / std::max(sampleFreq, 1u) - 1;
    if (max_count > 0xFFFF)
    {
        prescaler = max_count/0xFFFF;
    }
    else
    {
        prescaler = 0;
    }
    timerPeriod = (TIMxCLK / (std::max(sampleFreq, 1u)  * (prescaler + 1))) - 1;

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
    TIM_TimeBaseStructure.TIM_Period = timerPeriod;
    TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0x0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(m_playbackTimer, &TIM_TimeBaseStructure);

    // TIMx TRGO selection
    TIM_SelectOutputTrigger(m_playbackTimer, TIM_TRGOSource_Update);

    // DAC channels Configuration
    DAC_InitTypeDef  DAC_InitStructure;
    DAC_StructInit(&DAC_InitStructure);
    if (TIM6 == m_playbackTimer)
    {
        DAC_InitStructure.DAC_Trigger = DAC_Trigger_T6_TRGO;
    }
    else
    {
        DAC_InitStructure.DAC_Trigger = DAC_Trigger_T7_TRGO;
    }
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable;

    DAC_Init(channel, &DAC_InitStructure);

#if !defined (STM32L1XX_MD)
    if (DAC_CH1 == channel)
    {
        DMA_DeInit(DMA2_Channel3);
    }
    else
    {
        DMA_DeInit(DMA2_Channel4);
    }
#else
    if (DAC_CH1 == channel)
    {
         DMA_DeInit(DMA1_Channel2);
    }
    else
    {
         DMA_DeInit(DMA1_Channel3);
    }
#endif

    DMA_InitTypeDef DMA_InitStructure;
    DMA_StructInit(&DMA_InitStructure);
    if (DAC_CH1 == channel)
    {   
      if (SAMPLE_12BIT == sampleSize)
	    { 
            DMA_InitStructure.DMA_PeripheralBaseAddr = DAC_DHR12R1_Address;
    	}
		else  //8bit
        {
            DMA_InitStructure.DMA_PeripheralBaseAddr = DAC_DHR8R1_Address;
        }
	}
    else // channel2
    {
        if (SAMPLE_12BIT == sampleSize)
	    { 
            DMA_InitStructure.DMA_PeripheralBaseAddr = DAC_DHR12R2_Address;
    	}
		else  //8bit
        {
            DMA_InitStructure.DMA_PeripheralBaseAddr = DAC_DHR8R2_Address;
        }
    }
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) pData;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = numsamples;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    if (SAMPLE_12BIT == sampleSize)
    {
        DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
        DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    }
    else // 8bit
    {
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
        DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    }
    if (0==repeatCount)
    {
        DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    }
    else
    {
        DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    }
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;

#if !defined (STM32L1XX_MD)
    if (DAC_CH1 == channel)
    {
        DMA_Init(DMA2_Channel3, &DMA_InitStructure);
        // Enable DMA2 Channel3
        DMA_Cmd(DMA2_Channel3, ENABLE);
    }
    else
    {
        DMA_Init(DMA2_Channel4, &DMA_InitStructure);
        // Enable DMA2 Channel4
        DMA_Cmd(DMA2_Channel4, ENABLE);
    }
#else
    if (DAC_CH1 == channel)
    {
        DMA_Init(DMA1_Channel2, &DMA_InitStructure);
        // Enable DMA1 Channel2
        DMA_Cmd(DMA1_Channel2, ENABLE);
    }
    else
    {
        DMA_Init(DMA1_Channel3, &DMA_InitStructure);
        // Enable DMA1 Channel3
        DMA_Cmd(DMA1_Channel3, ENABLE);
    }
#endif

    // Enable DAC ChannelX: Once the DAC channel is enabled, PA.04 or PA.05 is
    // automatically connected to the DAC converter.
    DAC_Cmd(channel, ENABLE);

    // Enable DMA for DAC Channel
    DAC_DMACmd(channel, ENABLE);

    // TIMx enable counter
    TIM_Cmd(m_playbackTimer, ENABLE);

    channel == DAC_CH1 ? m_mode[0] = DACMODE_PLAYBACK : m_mode[1] = DACMODE_PLAYBACK;

    OS_LeaveRegion();

    return true;
}

//! \brief Stops playback of waveforms
bool Dac::stopWaveformPlaybacks()
{
    OS_EnterRegion();

    // TIMx disable counter
    TIM_Cmd(m_playbackTimer, DISABLE);

    OS_LeaveRegion();

    return true;
}
