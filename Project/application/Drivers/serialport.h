//! \file    serialport.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    22.02.2010

#ifndef _SERIALPORT_H
#define _SERIALPORT_H

// header includes
#include "RTOS.h"
#include <cstdint>
#include "circularbuffer.h"
#include <ISerial.h>
#include "stm32l5xx_hal.h"
#include "stm32l5xx_ll_usart.h"
#include "stm32l5xx_ll_gpio.h"
#include "stm32l5xx_ll_bus.h"
#include "stm32l5xx_ll_rcc.h"
#include "serialportcfg.h"

// forward declarations
//extern "C" { void UART3_MspInit(UART_HandleTypeDef* huart); }
//extern "C" { void USART3_IRQHandler(void); }
extern "C"
{
    void USART1_IRQHandler(void);
    void USART2_IRQHandler(void);
    void USART3_IRQHandler(void);
    void UART4_IRQHandler(void);
    void UART5_IRQHandler(void);
}

class Mutex;

class SerialPort
{
public:
    enum COM_TypeDef
    {
      COM1 = 0,
      COM2 = 1,
      COM3 = 2,
      COM4 = 3,
      COM5 = 4,
      NUM_COMs = COM5+1
    };
    // constructor/destructor
    // constructor/destructor
    explicit SerialPort(COM_TypeDef portNum, uint32_t baudRate,
                        uint16_t bufsizeRead, uint16_t bufsizeWrite,
                        uint32_t parity = LL_USART_PARITY_NONE, uint32_t hardwareFlowControl = LL_USART_HWCONTROL_NONE);

    virtual ~SerialPort();

    int write(uint8_t _data);
    int write(const uint8_t* _pData, uint16_t _len);
    int read();
    int read(uint8_t * _pData, uint16_t _len);
    int read(int16_t timeout);
    int read(uint8_t * _pData, uint16_t _len, int16_t timeout);

    void flush(const int& timeout=0);

    bool hasTxBufferSpaceAvailable() const;
    int  getTxBufferSpaceAvailable() const;
    int  getTxBufferWatermark();
    int  getTxBufferSize() const;

    int  getRxBufferWatermark();
    int  getRxBufferSize() const;
    
    bool init(){return true;}

    bool deinit();


protected:

    void disableRxInterrupt() { CLEAR_BIT(USART3->CR1, USART_CR1_RXNEIE); }
    void enableRxInterrupt()  { SET_BIT(USART3->CR1, USART_CR1_RXNEIE);  }

    void disableTxInterrupt() { CLEAR_BIT(USART3->CR1, USART_CR1_TCIE);  }
    void enableTxInterrupt()  { SET_BIT(USART3->CR1, USART_CR1_TCIE);  }
    
    bool txInterruptIsEnabled()  { return (LL_USART_IsEnabledIT_TC(USART3));  }

    CircularBuffer<uint8_t> m_outBuffer;    //!< output ringbuffer
    CircularBuffer<uint8_t> m_inBuffer;     //!< input ringbuffer
    USART_TypeDef* m_usart;
    virtual void isrX();
    
    //Estimator/approx time to transmitt single char
    const unsigned CHAR_TRX_DELAY_US_;
    
    

private:
    SerialPort(const SerialPort& right);            //!< block the copy constructor
    SerialPort& operator=(const SerialPort& right); //!< block the assignment operator

    //void USARTx_CLK_SOURCE()       { LL_RCC_SetUSARTClockSource(LL_RCC_USART3_CLKSOURCE_PCLK1) ; }
    //void USARTx_SET_TX_GPIO_AF()   { LL_GPIO_SetAFPin_8_15(GPIOB, USART3_Tx_pin, LL_GPIO_AF_7) ; }
    //void USARTx_SET_RX_GPIO_AF()   { LL_GPIO_SetAFPin_8_15(GPIOB, USART3_Rx_pin, LL_GPIO_AF_7) ; }
    //static void isr1();
    static void isr1();
    static void isr2();
    static void isr3();
    static void isr4();
    static void isr5();

    friend void USART1_IRQHandler(void);
    friend void USART2_IRQHandler(void);
    friend void USART3_IRQHandler(void);
    friend void UART4_IRQHandler(void);
    friend void UART5_IRQHandler(void);

    static SerialPort* get(COM_TypeDef portNum) { return m_pPorts[portNum]; }
    
    
    static SerialPort* m_pPorts[NUM_COMs];  //!< static member to keep track of serial ports. Needed by isr.
    static COM_TypeDef m_defaultPort;

    COM_TypeDef m_portNum;
    
    static const USART_TypeDef* COM_USART[NUM_COMs];    //!< for configuration of USART
    static const GPIO_TypeDef* COM_PORT[NUM_COMs];      //!< for configuration of port location
    static const uint32_t COM_USART_CLK[NUM_COMs];      //!< for configuration of USART clock
    static const uint32_t COM_USART_CLK_SRC[NUM_COMs];  //!< for configuration of USART clock source
    static const uint16_t COM_TX_PIN[NUM_COMs];         //!< for configuration of Tx pin location
    static const uint16_t COM_RX_PIN[NUM_COMs];         //!< for configuration of Rx pin location
    static const IRQn_Type COM_ISR[NUM_COMs];           //!< for configuration of ISR vector
    static const uint8_t  COM_ISR_PRIO[NUM_COMs];       //!< for configuration of ISR priorities

    static const void* COM_ISR_HNDL[NUM_COMs];          

    Mutex * m_mutex;

    
public:

    static int MyLowLevelPutchar(int x);

    static int MyLowLevelGetchar();
};

#endif   // _SERIALPORT_H

//======== End of serialport.h ==========
