//! \file    FHRMprobeSerial.h
//! \author  Jan A. R�yneberg
//! \version 1.0
//! \date    31.10.2013

#ifndef _FHRM_PROBE_SERIAL_H
#define _FHRM_PROBE_SERIAL_H

// header includes
#include <cstdint>
#include "stm32l5xx_hal.h"
#include "stm32l5xx_ll_usart.h"
#include "stm32l5xx_ll_gpio.h"
#include "stm32l5xx_ll_bus.h"
#include "stm32l5xx_ll_dma.h"
#include "cobs.h"
#include "crcccitt.h"
#include "ringbuffer.h"
#include "dacdma.h"
#include "communicationutility.h"
#include "includes.h"
#include "serialport.h"
#include "commondefs.h"
#include "dopplerpitchshift.h"
#include "accelerationenergy.h"
#include "fhrfilter.h"
#include "configuration.h"
#include "fhrmaudio.h"


// forward declarations
extern "C" { void TIM7_IRQHandler(void); }
extern "C" { void DMA1_Channel5_IRQHandler(void); }
extern "C"
{
    void USART1_IRQHandler(void);
    void USART2_IRQHandler(void);
    void USART3_IRQHandler(void);
    void UART4_IRQHandler(void);
    void UART5_IRQHandler(void);
}
//extern "C" { void USART1_IRQHandler(void); }
extern "C" { void HAL_USART_RxCpltCallback(UART_HandleTypeDef *husart); }
extern "C" { void HAL_USART_RxHalfCpltCallback(UART_HandleTypeDef *husart); }
////

#define COBS_OVERHEAD 1
#define RECIEVE_BUFFER_SIZE 512
#define SOUND_BUFFER_SIZE 4096//8192
#define UTIL_BUFFER_SIZE 256
#define FHR_BUFFER_SIZE 16
#define FHR_QUALITY_BUFFER_SIZE 16
#define OUT_BUFFER_SIZE 48
#define MAX_OUT_MESSAGE_SIZE 5
#define ENCODED_MSG_BUF_SIZE 256
#define DECODED_MSG_BUF_SIZE ENCODED_MSG_BUF_SIZE
#define MIN_MSG_LEN 5
#define MAX_DATA_PACKET_SIZE DECODED_MSG_BUF_SIZE
#define FRAMING_CHAR 0

USING_NAMESPACE_STD;

//! \brief 
class FHRMps
{
public:
    
    // constructor/destructor
    explicit FHRMps();
    virtual ~FHRMps();

    enum SignalQuality_t {eSQnoSignal, eSQbad, eSQcritical, eSQgood, eSQnotSet};
    enum TransducerState_t {eTSon, eTSoff};
    enum DopplerSoundSignalGain_t {eDSGx4 = 0, eDSGx1, eDSGnotSet};
    
    // UART send buffer
    static const uint8_t MAX_MSGLEN_OUTGOING = 30;
        
    //! \brief This is internal, i.e. always connected
    bool isConnected() const { return true; }
    
    void GetSoundData(uint8_t *buf, uint16_t noSamples);
    void GetSoundData(uint16_t *buf, uint16_t noSamples);
    uint16_t PeekSoundData(void);
    uint16_t GetSoundDataFill(void);
    uint16_t GetFileSoundData(uint8_t *buf, uint16_t noSamples);
    uint16_t GetFileSoundData(uint16_t *buf, uint16_t noSamples);
    uint16_t GetFileUtilData(Utilrecord_t *buf, uint16_t noSamples);
    uint16_t GetFileFHRdata(FHRrecord_t *buf, uint16_t noSamples);
    uint16_t GetSoundRate(void);
    uint8_t GetFetalHeartRate(void);
    uint8_t GetFetalHeartRateFiltered(void);
    std::string   GetModuleVersionInfo(void);
    SignalQuality_t GetSignalQuality(void);
    TransducerState_t GetTransducerState(void);
    void SendRequireStatusCmd(void);
    void SendGetModuleVersionCmd(void);
    bool IsTransducerTimedOut(void);
    void ResetTransducerTimeOut(void);
    uint8_t GetProbeTempAsRaw(void);
    float GetProbeTemp(void);
    POST_ctrl_t getPostResult(void);
    void clearPostResult(void);
    uint16_t GetTransducerErrCount(void);
    uint32_t GetTransducerErrCountTot(void);
    void ProbeEnable(bool a_on);
    void ProbeSoundEnable(bool a_on);
    bool IsProbeSoundEnabled(void);
    int16_t GetAccEnergy(void);
    void GainSoundSamples(uint8_t *a_pSamples, uint16_t a_noSamples, uint8_t a_gain);
    void Post(void);
    Acceleration_t GetCurrentAccel(void);
    void SetDopplerSoundSpeakerGain(uint8_t a_gain);                            //For SW amplifying of gain
    void SendSetDopplerSoundSignalGainCmd(DopplerSoundSignalGain_t a_gain);     //For setting gain in Doppler probe
    DopplerSoundSignalGain_t GetDopplerSignalGain(void);
    void EnableFHRfilter(bool a_enable);
    void DopplerSoundMinMaxValuesInit(uint16_t a_measurementTimeInmsec);
    uint16_t getDopplerSoundMaxValue(void);
    uint16_t getDopplerSoundMinValue(void);
    bool getDopplerSoundMinMaxValuesActive(void);
    

    
protected:

private:
    FHRMps(const FHRMps& right);            //!< block the copy constructor
    FHRMps& operator=(const FHRMps& right); //!< block the assignment operator

    void initUart();
    
    friend void DMA1_Channel5_IRQHandler(void);
    friend void TIM7_IRQHandler(void);
    friend void USART1_IRQHandler(void);
    void Timer7_ISR(void);
    void USART1_ISR(void);
    void DMA_ISR(void);
    void DMA_Channel5_HT_ISR(void);
    void DMA_Channel5_FT_ISR(void);
    void COM1_TxISR(void);
    
    
    void decodeMessage(uint8_t *pBufStart, uint16_t len);
    void SoundRateTimerInit(void);
    uint16_t writeSerialPort(const uint8_t * pData, uint16_t len);
    bool sendMessage(std::vector<uint8_t>& msg);
    bool sendBufferLock();
    bool sendBufferUnlock();
    void USART_SendData(USART_TypeDef * usart, uint8_t data);
    
    void sampleDopplerSoundMinMaxValues(uint8_t *items, uint16_t noItems);
    
#ifdef TEST_SOUND_TO_FILE
    void AddSoundTestData(uint8_t *buffer, uint16_t noSamples);
#endif
    
    void disableCOM1TxInterrupt() { USART1->CR1 &= static_cast<uint16_t>(~USART_CR1_TCIE);  }
    void enableCOM1TxInterrupt()  { USART1->CR1 |= static_cast<uint16_t>(USART_CR1_TCIE);  }
    
    static FHRMps* m_pFHRMps;  //!< static member to keep track of instance. Needed to prevent multiple instances
    static FHRMps* get() { return m_pFHRMps; }
    
    //TIM_HandleTypeDef   m_soundRateTimer;
    //UART_HandleTypeDef  m_husart;
    //DMA_HandleTypeDef   m_husart_rx_dma;
    uint8_t *m_pRecieveBuffer;
    uint8_t *m_pCurrRecieveBufferPos;
    uint8_t *m_pHTRecieveBufferPos; //Half transfer pointer
    uint8_t *m_pFTRecieveBufferPos; //Full transfer pointer
    uint8_t *m_pDecodedMsg;
    uint8_t *m_pEncodedMsg;
    bool    m_synced;
    bool m_probeSoundEnabled;
    uint32_t m_accDataTimestamp;
    bool m_accDataTimestampSync;
        
    RingBuffer m_sndBuffer;               //!< Sound samples for DAC ringbuffer
    RingBuffer m_fileSndBuffer;           //!< Sound samples for file ringbuffer
    RingBuffer m_accFileBuffer;          //!< Accelerometer samples ringbuffer
    
    AccelerationEnergy *m_AccelerationEnergy;
    
    FHRfilter *m_FhrFilter;
    
    std::vector<uint8_t> m_msgBuffer;          //!< Serial message buffer (for transmit message assembly)
    std::vector<uint8_t> m_EncodeBuffer;       //!< Serial transmit buffer (for transmit message assembly)
    RingBuffer m_transmitBuffer;          //!< Serial transmit ringbuffer (for transmit interrupt handler)
    
    uint32_t m_noMessagesRecieved; //Debug
    
    volatile uint32_t m_noSoundSamplesRecieved;
    uint16_t m_soundRate;
    SoundVolume_t m_soundVol;
    uint8_t m_soundGain;
    bool m_FHRfilterEnabled;
    
    uint8_t                 m_fetalHeartRateFiltered;
    uint8_t                 m_fetalHeartRate;
    std::string             m_moduleVersioninfo;
    SignalQuality_t         m_signalQuality;
    TransducerState_t       m_transducerState;
    Acceleration_t          m_currentAccel;
    uint8_t                 m_probeTempRaw;
    DopplerSoundSignalGain_t m_dopplerSoundSignalGain;
    bool                    m_transducerTimeout;
    uint8_t                 m_transducerTimeoutCnt;
    static const uint8_t    TRANSDUCER_TIMEOUT_MAX_COUNT = 5;
    uint16_t                m_transducerErrCnt;
    uint32_t                m_transducerErrCntTot;
    int16_t                 m_accEnergy;
    
    POST_ctrl_t             m_POSTstatus;
    
    //Doppler sound min/max function
    bool                    m_dopplerSoundMinMaxActive;
    uint16_t                m_dopplerSoundMinMaxRemainingSamples;
    uint16_t                m_dopplerSoundMinMaxMinValue;
    uint16_t                m_dopplerSoundMinMaxMaxValue;
    
    
        
};


#endif   // _FHRM_PROBE_SERIAL_H

//======== End of Dac.h ==========

