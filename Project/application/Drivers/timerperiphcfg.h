//! \file    timerperiphcfg.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    06.05.2010

#ifndef _TIMER_PERIPH_CFG_H
#define _TIMER_PERIPH_CFG_H

// header includes
//#include "misc.h"
#include "board_cfg.h"

//TODO: Adapt to STM32L5

// This section is not supposed to be modified.
// --------------------------------------------

//! \brief Definition for TIM1 based I/O ports\n
//! These relate to PwmGen channels 0..7 and/or CapCom channels 0..3
#if CFG_TIM1_REMAP == 0
#define CFG_TIM1_CH_1_4_GPIOx        GPIOA, GPIOA, GPIOA, GPIOA
#define CFG_TIM1_CH_1_4_GPIO_pins    GPIO_PIN_8, GPIO_PIN_9, GPIO_PIN_10, GPIO_PIN_11
#define CFG_TIM1_CH_1N_3N_GPIOx      GPIOB, GPIOB, GPIOB, GPIOB
#define CFG_TIM1_CH_1N_3N_GPIO_pins  GPIO_PIN_13, GPIO_PIN_14, GPIO_PIN_15, GPIO_PIN_15
#elif CFG_TIM1_REMAP == 1
#define CFG_TIM1_CH_1_4_GPIOx        GPIOA, GPIOA, GPIOA, GPIOA
#define CFG_TIM1_CH_1_4_GPIO_pins    GPIO_PIN_8, GPIO_PIN_9, GPIO_PIN_10, GPIO_PIN_11
#define CFG_TIM1_CH_1N_3N_GPIOx      GPIOA, GPIOB, GPIOB, GPIOB
#define CFG_TIM1_CH_1N_3N_GPIO_pins  GPIO_PIN_7, GPIO_PIN_0, GPIO_PIN_1, GPIO_PIN_1
#elif CFG_TIM1_REMAP == 3
#define CFG_TIM1_CH_1_4_GPIOx        GPIOE, GPIOE, GPIOE, GPIOE
#define CFG_TIM1_CH_1_4_GPIO_pins    GPIO_PIN_9, GPIO_PIN_11, GPIO_PIN_13, GPIO_PIN_14
#define CFG_TIM1_CH_1N_3N_GPIOx      GPIOE, GPIOE, GPIOE, GPIOE
#define CFG_TIM1_CH_1N_3N_GPIO_pins  GPIO_PIN_8, GPIO_PIN_10, GPIO_PIN_12, GPIO_PIN_12
#endif

//! \brief Definition for TIM2 based I/O ports\n
//! These relate to PwmGen channels 8..11 and/or CapCom channels 4..7
#if CFG_TIM2_REMAP == 0
#define CFG_TIM2_CH_1_4_GPIOx      GPIOA, GPIOA, GPIOA, GPIOA
#define CFG_TIM2_CH_1_4_GPIO_pins  GPIO_PIN_0, GPIO_PIN_1, GPIO_PIN_2,  GPIO_PIN_3
#elif CFG_TIM2_REMAP == 1
#define CFG_TIM2_CH_1_4_GPIOx      GPIOA, GPIOB, GPIOA, GPIOA
#define CFG_TIM2_CH_1_4_GPIO_pins  GPIO_PIN_15, GPIO_PIN_3, GPIO_PIN_2,  GPIO_PIN_3
#elif CFG_TIM2_REMAP == 2
#define CFG_TIM2_CH_1_4_GPIOx      GPIOA, GPIOA, GPIOB, GPIOB
#define CFG_TIM2_CH_1_4_GPIO_pins  GPIO_PIN_0, GPIO_PIN_1, GPIO_PIN_10,  GPIO_PIN_11
#elif CFG_TIM2_REMAP == 3
#define CFG_TIM2_CH_1_4_GPIOx      GPIOA, GPIOB, GPIOB, GPIOB
#define CFG_TIM2_CH_1_4_GPIO_pins  GPIO_PIN_15, GPIO_PIN_3, GPIO_PIN_10,  GPIO_PIN_11
#endif

//! \brief Definition for TIM3 based I/O ports\n
//! These relate to PwmGen channels 12..15 and/or CapCom channels 8..11
#if CFG_TIM3_REMAP == 0
  #define CFG_TIM3_CH_1_4_GPIOx      GPIOA, GPIOA, GPIOB, GPIOB
  #define CFG_TIM3_CH_1_4_GPIO_pins  GPIO_PIN_6, GPIO_PIN_7, GPIO_PIN_0, GPIO_PIN_1
#elif CFG_TIM3_REMAP == 2
  #define CFG_TIM3_CH_1_4_GPIOx      GPIOB, GPIOB, GPIOB, GPIOB
  #define CFG_TIM3_CH_1_4_GPIO_pins  GPIO_PIN_4, GPIO_PIN_5, GPIO_PIN_0, GPIO_PIN_1
#elif CFG_TIM3_REMAP == 3
  #define CFG_TIM3_CH_1_4_GPIOx       GPIOC, GPIOC, GPIOC, GPIOC
  #define CFG_TIM3_CH_1_4_GPIO_pins   GPIO_PIN_6, GPIO_PIN_7, GPIO_PIN_8, GPIO_PIN_9
#endif

//! \brief Definition for TIM4 based I/O ports\n
//! These relate to PwmGen channels 16..18 and/or CapCom channels 12..15
#if CFG_TIM4_REMAP == 0
#define CFG_TIM4_CH_1_4_GPIOx      GPIOB, GPIOB, GPIOB, GPIOB
#define CFG_TIM4_CH_1_4_GPIO_pins  GPIO_PIN_6, GPIO_PIN_7, GPIO_PIN_8, GPIO_PIN_9
#elif CFG_TIM4_REMAP == 1
  #define CFG_TIM4_CH_1_4_GPIOx      GPIOD, GPIOD, GPIOD, GPIOD
  #define CFG_TIM4_CH_1_4_GPIO_pins  GPIO_PIN_12, GPIO_PIN_13, GPIO_PIN_14, GPIO_PIN_15
#endif

// TIM5 has no remapping possibilities
#define CFG_TIM5_CH_1_4_GPIOx      GPIOA, GPIOA, GPIOA, GPIOA
#define CFG_TIM5_CH_1_4_GPIO_pins  GPIO_PIN_3, GPIO_PIN_3, GPIO_PIN_3, GPIO_PIN_3

// TIM8 has no remapping possibilities
#define CFG_TIM8_CH_1_4_GPIOx      GPIOC, GPIOC, GPIOC, GPIOC
#define CFG_TIM8_CH_1_4_GPIO_pins  GPIO_PIN_6, GPIO_PIN_7, GPIO_PIN_8, GPIO_PIN_9

#if defined(STM32L1XX_MD) || defined(STM32F10X_XL)
//! \brief Definition for TIM9 based I/O ports\n
#if CFG_TIM9_REMAP == 0
  #define CFG_TIM9_CH_1_4_GPIOx      GPIOA, GPIOA, GPIOA, GPIOA
  #define CFG_TIM9_CH_1_4_GPIO_pins  GPIO_PIN_2, GPIO_PIN_3, GPIO_PIN_2, GPIO_PIN_3
  #if defined (STM32L1XX_MD)
    #define CFG_TIM9_CH_1_4_GPIO_pin_source  GPIO_PinSource2, GPIO_PinSource3, GPIO_PinSource2, GPIO_PinSource3
  #endif
#elif CFG_TIM9_REMAP == 1
  #define CFG_TIM9_CH_1_4_GPIOx      GPIOE, GPIOE, GPIOE, GPIOE
  #define CFG_TIM9_CH_1_4_GPIO_pins  GPIO_PIN_5, GPIO_PIN_6, GPIO_PIN_5, GPIO_PIN_6
  #if defined (STM32L1XX_MD)
    #define CFG_TIM9_CH_1_4_GPIO_pin_source  GPIO_PinSource5, GPIO_PinSource6, GPIO_PinSource5, GPIO_PinSource6
  #endif
#endif

//! \brief Definition for TIM10 based I/O ports\n
#if CFG_TIM10_REMAP == 0
#define CFG_TIM10_CH_1_4_GPIOx      GPIOB, GPIOB, GPIOB, GPIOB
#define CFG_TIM10_CH_1_4_GPIO_pins  GPIO_PIN_8, GPIO_PIN_8, GPIO_PIN_8, GPIO_PIN_8
#elif CFG_TIM10_REMAP == 1
    #define CFG_TIM10_CH_1_4_GPIOx      GPIOF, GPIOF, GPIOF, GPIOF
    #define CFG_TIM10_CH_1_4_GPIO_pins  GPIO_PIN_6, GPIO_PIN_6, GPIO_PIN_6, GPIO_PIN_6
#endif

//! \brief Definition for TIM11 based I/O ports\n
#if CFG_TIM11_REMAP == 0
  #define CFG_TIM11_CH_1_4_GPIOx      GPIOB, GPIOB, GPIOB, GPIOB
  #define CFG_TIM11_CH_1_4_GPIO_pins  GPIO_PIN_9, GPIO_PIN_9, GPIO_PIN_9, GPIO_PIN_9
#elif CFG_TIM11_REMAP == 1
    #define CFG_TIM11_CH_1_4_GPIOx      GPIOF, GPIOF, GPIOF, GPIOF
    #define CFG_TIM11_CH_1_4_GPIO_pins  GPIO_PIN_7, GPIO_PIN_7, GPIO_PIN_7, GPIO_PIN_7
#endif

#endif

#endif   // _TIMER_PERIPH_CFG_H

//======== End of timerperiphcfg.h ==========

