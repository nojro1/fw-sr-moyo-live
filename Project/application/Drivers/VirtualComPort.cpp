//! \file    VirtualComPort.cpp
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    28.08.2010

#if OS_USE_VARINTTABLE==0
//Prototypes for interrupt service routines
extern "C"
{
  #ifdef STM32F10X_CL
    void OTG_FS_IRQHandler(void);
  #else
    void USB_LP_CAN1_RX0_IRQHandler(void);
  #endif // STM32F10X_CL

  #ifdef USB_PWR_PIN        
    void EXTI0_IRQHandler(void);
    void EXTI1_IRQHandler(void);
    void EXTI2_IRQHandler(void);
    void EXTI3_IRQHandler(void);
    void EXTI4_IRQHandler(void);
    void EXTI9_5_IRQHandler(void);
    void EXTI15_10_IRQHandler(void);
  #endif

}
#endif // OS_USE_VARINTTABLE

extern "C"
{
void Handle_USBAsynchXfer (void);
void EP1_IN_Callback (void);
void EP3_OUT_Callback(void);
#ifdef STM32F10X_CL
void INTR_SOFINTR_Callback(void);
#else
void SOF_Callback(void);
#endif /* STM32F10X_CL */
}

// include self first
#include "VirtualComPort.h"

// other includes
#include <isrpriocfg.h>
#include <sysdefs.h>

//#include <Thread.h>

extern "C"
{
#include "usb_lib.h"
#include "VirtualCOM/usb_desc.h"
#include "VirtualCOM/hw_config.h"
#include "VirtualCOM/usb_istr.h"
#include "VirtualCOM/usb_pwr.h"
}


// static member init
VirtualComPort*  VirtualComPort::m_pPort = 0;

static uint8_t USB_Tx_State = 0;

//! constructor
//! \brief Initialise the device
//! \param bufsizeRead The receive buffer size
//! \param bufsizeWrite The transmit buffer size
VirtualComPort::VirtualComPort(uint16_t bufsizeRead, uint16_t bufsizeWrite) : m_enabled(false)
{
    OS_EnterRegion();
    if (0 == m_pPort)
    {
        m_inBuffer.create(bufsizeRead);
        m_outBuffer.create(bufsizeWrite);

        m_pPort = this;

      #ifdef STM32F10X_CL
        OS_ARM_InstallISRHandler(OTG_FS_IRQn, (OS_ISR_HANDLER*)&usb_isr);
      #else
        OS_ARM_InstallISRHandler(USB_LP_CAN1_RX0_IRQn, (OS_ISR_HANDLER*)&usb_isr);
      #endif

      #ifdef USB_PWR_PIN      
        // Connect EXTI Line to GPIO Pins
        GPIO_EXTILineConfig(USB_PWR_EXTI_PORTSOURCE, USB_PWR_EXTI_PINSOURCE);
       //Configure EXTI line
        EXTI_InitTypeDef EXTI_InitStructure;
        EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
        EXTI_InitStructure.EXTI_LineCmd = ENABLE;
        EXTI_InitStructure.EXTI_Line = USB_PWR_EXTI_Line;
        EXTI_InitStructure.EXTI_Trigger = USB_PWR_ACTIVE_STATE ? EXTI_Trigger_Rising : EXTI_Trigger_Falling;
        EXTI_Init(&EXTI_InitStructure);

        //Enable and set EXTI Interrupt
        NVIC_InitTypeDef NVIC_InitStructure;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = ISR_PRIO_USB_PWR_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
        NVIC_InitStructure.NVIC_IRQChannel = USB_PWR_EXTI_IRQn;
        NVIC_Init(&NVIC_InitStructure);
    
        OS_ARM_InstallISRHandler(USB_PWR_EXTI_IRQn, (OS_ISR_HANDLER*) &usb_pwr_isr);
      #endif

        Set_System();
        Set_USBClock();
        USB_Interrupts_Config();
        USB_Init();

        m_enabled = true;
        
    }
    OS_LeaveRegion();
}

//! destructor
VirtualComPort::~VirtualComPort()
{
    deinit();
}

//! \brief Read a byte from the virtual COM port input buffer
//! \return The read byte. -1 if none available
int VirtualComPort::read()
{
    if (!m_enabled)
        return -1;
    
    uint8_t data;
    OS_IncDI();
    bool success = m_inBuffer.get(data);
    OS_DecRI();
    return success ? data : -1;
}

//! \brief Read n bytes from the virtual COM port input buffer
//! \param _pData [out] Pointer to destination array
//! \param _len Number of bytes to read
//! \return Number of bytes retrieved, 0 if none
int VirtualComPort::read(uint8_t * _pData, uint16_t _len)
{
    if (!m_enabled)
        return -1;

    OS_IncDI();
    int numRead = m_inBuffer.get(_pData, _len);
    OS_DecRI();
    return numRead;
}

//! \brief Read a byte from the virtual COM port input buffer and\n
//! suspends calling Thread if none available, with timeout
//! \param timeout Timeout in milliseconds (when to give up)
//! \return The read byte. -1 if none available (i.e. timeout)
int VirtualComPort::read(int16_t timeout)
{
    int dta;
    const int aftertimeout = OS_GetTime() + timeout;
    do
    {
        dta = read();
        if (dta < 0)
        {
            OS_Delay(SYSTICK_TIME);
        }
        else
        {
            break;
        }
    } while (OS_GetTime() < aftertimeout);

    return dta;
}

//! \brief Read n bytes from the virtual COM port input buffer and\n
//! suspends calling Thread if none available, with timeout
//! \param _pData [out] Pointer to destination array
//! \param _len Number of bytes to read
//! \param timeout Timeout in milliseconds (when to give up)
//! \return Number of bytes retrieved, 0 if none (i.e. timeout)
int VirtualComPort::read(uint8_t * _pData, uint16_t _len, int16_t timeout)
{
    int numRead;
    const int aftertimeout = OS_GetTime() + timeout;
    do
    {
        numRead = read(_pData, _len);
        if (numRead == 0)
        {
            OS_Delay(SYSTICK_TIME);
        }
        else
        {
            break;
        }
    } while (OS_GetTime() < aftertimeout);

    return numRead;
}

//! \brief Write a byte to the virtual COM port output buffer
//! \param _data The byte to write
//! \return Number of bytes written, 0 if none
int VirtualComPort::write(uint8_t _data)
{
    int bytes_written = 0;
    OS_IncDI();
    if (m_outBuffer.getRoom() > 0)
    {
        m_outBuffer.put(_data);
        bytes_written = 1;
    }
    OS_DecRI();
    return bytes_written;
}

//! \brief Write n bytes to the virtual COM port output buffer
//! \param _pData Pointer to source array
//! \param _len Number of bytes to write
//! \return Number of bytes written, 0 if none
int VirtualComPort::write(const uint8_t * _pData, uint16_t _len)
{
    int bytes_written = 0;
    OS_IncDI();
    if (m_outBuffer.getRoom() >= _len)
    {
        m_outBuffer.put(_pData, _len);
        bytes_written = _len;
    }
    OS_DecRI();
    return bytes_written;
}

//! \brief Checks for available TX buffer space
//! \return true, if available TX buffer space, else false
//! \remark Mainly to be used for console help output, to avoid allocating huge TX ringbuffers\n
//! when printing out helptext.
bool VirtualComPort::hasTxBufferSpaceAvailable() const
{
    OS_IncDI();
    int space = m_outBuffer.getRoom();
    OS_DecRI();
    return space > 0;
}

//! \brief Returns available TX buffer space
//! \return Available TX buffer space in # of bytes
int VirtualComPort::getTxBufferSpaceAvailable() const
{
    OS_IncDI();
    int space = m_outBuffer.getRoom();
    OS_DecRI();
    return space;
}

//! \brief Returns maximum used TX buffer space
//! \return Maximum used TX buffer space in # of bytes
int VirtualComPort::getTxBufferWatermark() 
{
    OS_IncDI();
    int maxused = m_outBuffer.getAndResetHighWatermark();
    OS_DecRI();
    return maxused;
}

//! \brief Returns TX buffer space
//! \return TX buffer space in # of bytes
int VirtualComPort::getTxBufferSize() const
{
    OS_IncDI();
    int size = m_outBuffer.getMaximumSize();
    OS_DecRI();
    return size;
}

//! \brief Returns maximum used RX buffer space
//! \return Maximum used RX buffer space in # of bytes
int VirtualComPort::getRxBufferWatermark() 
{
    OS_IncDI();
    int maxused = m_inBuffer.getAndResetHighWatermark();
    OS_DecRI();
    return maxused;
}

//! \brief Returns RX buffer space
//! \return RX buffer space in # of bytes
int VirtualComPort::getRxBufferSize() const
{
    OS_IncDI();
    int size = m_inBuffer.getMaximumSize();
    OS_DecRI();
    return size;
}

//! \brief Empties Rx and Tx ring-buffers and the UART Rx data register
void VirtualComPort::flush(const int& timeout)
{
    //The timeout param works only for output stream
    if (0 < timeout)
    {
      int aftertimeout = OS_GetTime() + timeout;
      //Note: reading buffer size should not req disabling interrupts
      while (0 < m_outBuffer.getNumItems() && aftertimeout > OS_GetTime())
      {
        OS_Delay(SYSTICK_TIME);
      }
    }//Else you may loose data as we dump buffers

    OS_IncDI();
    m_inBuffer.reset();
    m_outBuffer.reset();
    OS_DecRI();
}


//! \brief De-Initialise the virtual COM device to the state before initialized (including GPIO port settings)
bool VirtualComPort::deinit()
{
  // TODO: ??
  return true;
}

void VirtualComPort::disable()
{
    m_enabled = false;
    USB_Cable_Config(DISABLE);
    flush();
}

void VirtualComPort::enable()
{
    USB_Cable_Config(ENABLE);
    m_enabled = true;
}

void VirtualComPort::reset()
{
    OS_IncDI();
    USB_Init();
    flush();
    USB_Tx_State = 0;
    OS_DecRI();
}

bool VirtualComPort::hasUSBHostConnection() const
{
    //DEBUG_TRACE("VirtualComPort::hasUSBHostConnection() = %d\r\n", bDeviceState);
    return bDeviceState != UNCONNECTED;
}

bool VirtualComPort::isUSBHostConnectionOK() const
{
    //DEBUG_TRACE("VirtualComPort::hasUSBHostConnection() = %d\r\n", bDeviceState);
    return bDeviceState == CONFIGURED;
}

//! \brief Interrupt handler for the virtual COM port driver
void VirtualComPort::usb_isr()
{
  #ifdef STM32F10X_CL
    STM32_PCD_OTG_ISR_Handler();
  #else
    USB_Istr();
  #endif
}

#ifdef USB_PWR_PIN      
//! \brief Interrupt handler for monitoring the USB power line
void VirtualComPort::usb_pwr_isr()
{
    if ( EXTI_GetITStatus(USB_PWR_EXTI_Line) != RESET )
    {
        USB_Cable_Config(ENABLE);
        EXTI_ClearITPendingBit(USB_PWR_EXTI_Line);
    }
}
#endif

#if OS_USE_VARINTTABLE==0

#ifdef STM32F10X_CL
/*******************************************************************************
* Function Name  : OTG_FS_IRQHandler
* Description    : This function handles USB-On-The-Go FS global interrupt request.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void OTG_FS_IRQHandler(void)
{
  STM32_PCD_OTG_ISR_Handler();
}
#else
/*******************************************************************************
* Function Name  : USB_LP_CAN1_RX0_IRQHandler
* Description    : This function handles USB Low Priority or CAN RX0 interrupts
*                  requests.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void USB_LP_CAN1_RX0_IRQHandler(void)
{
  USB_Istr();
}
#endif // STM32F10X_CL

#ifdef USB_PWR_PIN        

void EXTI0_IRQHandler(void)
{
    VirtualComPort::get()->usb_pwr_isr();
}

void EXTI1_IRQHandler(void)
{
    VirtualComPort::get()->usb_pwr_isr();
}

void EXTI2_IRQHandler(void)
{
    VirtualComPort::get()->usb_pwr_isr();
}

void EXTI3_IRQHandler(void)
{
    VirtualComPort::get()->usb_pwr_isr();
}

void EXTI4_IRQHandler(void)
{
    VirtualComPort::get()->usb_pwr_isr();
}

void EXTI9_5_IRQHandler(void)
{
    VirtualComPort::get()->usb_pwr_isr();
}

void EXTI15_10_IRQHandler(void)
{
    VirtualComPort::get()->usb_pwr_isr();
}
#endif // USB_PWR_PIN        

#endif // OS_USE_VARINTTABLE==0

static uint8_t USB_Tx_Buffer[VIRTUAL_COM_PORT_DATA_SIZE];
static uint8_t USB_Rx_Buffer[VIRTUAL_COM_PORT_DATA_SIZE];

static uint16_t USB_Tx_length;

/*******************************************************************************
* Function Name  : Handle_USBAsynchXfer.
* Description    : send data to USB.
* Input          : None.
* Return         : none.
*******************************************************************************/
void Handle_USBAsynchXfer (void)
{

  VirtualComPort * pObj = VirtualComPort::get();

  if( pObj != 0 )
  {
      if( USB_Tx_State != 1 )
      {
        if (pObj->m_outBuffer.getNumItems() == 0)
        {
          USB_Tx_State = 0;
          // Ref. https://my.st.com/public/STe2ecommunities/mcu/Lists/cortex_mx_stm32/Flat.aspx?RootFolder=https%3a%2f%2fmy%2est%2ecom%2fpublic%2fSTe2ecommunities%2fmcu%2fLists%2fcortex_mx_stm32%2fVirtual_COM_Port%20Sending%200x40%20bytes%20to%20PC%20problem%20on%20STM32F107RCT6&FolderCTID=0x01200200770978C69A1141439FE559EB459D7580009C4E14902C3CDE46A77F0FFD06506F5B&currentviews=312
          // "If You are sending a multiple of your endpoint buffer size You must send a empty pakage after Your last transaction to show the host that Your transaction is complete now"
          if (VIRTUAL_COM_PORT_DATA_SIZE==USB_Tx_length)
          {    
             USB_Tx_length = 0;       
             goto SendZeroPacket;
          }
          return;
        }

        USB_Tx_length = pObj->m_outBuffer.get(USB_Tx_Buffer, VIRTUAL_COM_PORT_DATA_SIZE);
        USB_Tx_State = 1;

    SendZeroPacket:

#ifdef STM32F10X_CL
        USB_SIL_Write(EP1_IN, USB_Tx_Buffer, USB_Tx_length);
#else
        UserToPMABufferCopy(USB_Tx_Buffer, ENDP1_TXADDR, USB_Tx_length);
        SetEPTxCount(ENDP1, USB_Tx_length);
        SetEPTxValid(ENDP1);
#endif
      }
      else // bls2010:#1413 - recover from locked up transmission (EP1_IN_Callback never being called while USB_Tx_State=1)
      {
        uint16_t epTxStatus = GetEPTxStatus(ENDP1) & EPTX_STAT;
        if (epTxStatus != EP_TX_VALID)
        {
          USB_Tx_State = 0;
        }
      }
  }

}

//////////////////////////////////////////////////////////////////////////////////////////////////
// The following endpoint functions originally resided in usb_endp.c (in ST's Virtual COM example)
//////////////////////////////////////////////////////////////////////////////////////////////////

/*******************************************************************************
* Function Name  : EP1_IN_Callback
* Description    :
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP1_IN_Callback (void)
{
    VirtualComPort * pObj = VirtualComPort::get();

    if ( USB_Tx_State == 1 && pObj != 0 )
    {
      if (pObj->m_outBuffer.getNumItems() == 0)
      {
        USB_Tx_State = 0;
        // Ref. https://my.st.com/public/STe2ecommunities/mcu/Lists/cortex_mx_stm32/Flat.aspx?RootFolder=https%3a%2f%2fmy%2est%2ecom%2fpublic%2fSTe2ecommunities%2fmcu%2fLists%2fcortex_mx_stm32%2fVirtual_COM_Port%20Sending%200x40%20bytes%20to%20PC%20problem%20on%20STM32F107RCT6&FolderCTID=0x01200200770978C69A1141439FE559EB459D7580009C4E14902C3CDE46A77F0FFD06506F5B&currentviews=312
        // "If You are sending a multiple of your endpoint buffer size You must send a empty pakage after Your last transaction to show the host that Your transaction is complete now"
        if (VIRTUAL_COM_PORT_DATA_SIZE==USB_Tx_length)
        {    
           USB_Tx_length = 0;       
           goto SendZeroPacket;
        }
        return;
      }

      USB_Tx_length = pObj->m_outBuffer.get(USB_Tx_Buffer, VIRTUAL_COM_PORT_DATA_SIZE);
      USB_Tx_State = 1;

SendZeroPacket:

#ifdef STM32F10X_CL
      USB_SIL_Write(EP1_IN, USB_Tx_Buffer, USB_Tx_length);
#else
      UserToPMABufferCopy(USB_Tx_Buffer, ENDP1_TXADDR, USB_Tx_length);
      SetEPTxCount(ENDP1, USB_Tx_length);
      SetEPTxValid(ENDP1);
#endif
    }
}

/*******************************************************************************
* Function Name  : EP3_OUT_Callback
* Description    :
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP3_OUT_Callback(void)
{
  uint16_t USB_Rx_Cnt;

  /* Get the received data buffer and update the counter */
  USB_Rx_Cnt = USB_SIL_Read(EP3_OUT, USB_Rx_Buffer);

  /* USB data will be immediately processed, this allow next USB traffic beeing
  NAKed till the end of the USART Xfet */
  VirtualComPort * pObj = VirtualComPort::get();

  if ( pObj != 0 )
  {
    pObj->m_inBuffer.put(USB_Rx_Buffer, USB_Rx_Cnt);
  }

#ifndef STM32F10X_CL
  /* Enable the receive of data on EP3 */
  SetEPRxValid(ENDP3);
#endif /* STM32F10X_CL */
}

/* Interval between sending IN packets in frame number (1 frame = 1ms) */
#define VCOMPORT_IN_FRAME_INTERVAL             5

/*******************************************************************************
* Function Name  : SOF_Callback / INTR_SOFINTR_Callback
* Description    :
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
#ifdef STM32F10X_CL
void INTR_SOFINTR_Callback(void)
#else
void SOF_Callback(void)
#endif /* STM32F10X_CL */
{
  static uint32_t FrameCount = 0;

  if(bDeviceState == CONFIGURED)
  {
    if (FrameCount++ == VCOMPORT_IN_FRAME_INTERVAL)
    {
      /* Reset the frame counter */
      FrameCount = 0;

      /* Check the data to be sent through IN pipe */
      Handle_USBAsynchXfer();
    }
  }
}

