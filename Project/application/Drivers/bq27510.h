//! \file    bq27510.h
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    19.05.2014

#ifndef _BQ27510_H
#define _BQ27510_H

// header includes
#include <cstdint>
#include "stm32l5xx.h"
#include "i2cbus.h"
#include "communicationutility.h"
#include "Mutex.h"

// forward declarations

////

//! \brief 

class Bq27510
{
public:

    // constructor/destructor
    explicit Bq27510(I2Cbus *pI2C);
    virtual ~Bq27510();

    static const uint8_t BQ27510_ADDR = 0xaa;

    struct ProbeResult
    {
        int32_t voltage_mV;
        int32_t current_uA;
        int32_t averageCurrent_uA;
        int32_t temperature_mC;
        int8_t remainingCapacityPercent;
        bool    charging;
    };

    
    //Commands
    static const uint8_t CMD_CONTROL_L = 0x00;
    static const uint8_t CMD_CONTROL_H = 0x01;
    static const uint8_t CMD_AT_RATE_L = 0x02;
    static const uint8_t CMD_AT_RATE_H = 0x03;
    static const uint8_t CMD_AT_RATE_TO_EMPTY_L = 0x04;
    static const uint8_t CMD_AT_RATE_TO_EMPTY_H = 0x05;
    static const uint8_t CMD_TEMPERATURE_L = 0x06;
    static const uint8_t CMD_TEMPERATURE_H = 0x07;
    static const uint8_t CMD_VOLTAGE_L = 0x08;
    static const uint8_t CMD_VOLTAGE_H = 0x09;
    static const uint8_t CMD_FLAGS_L = 0x0a;
    static const uint8_t CMD_FLAGS_H = 0x0b;
    static const uint8_t CMD_NOM_AVAILABLE_CAPACITY_L = 0x0c;
    static const uint8_t CMD_NOM_AVAILABLE_CAPACITY_H = 0x0d;
    static const uint8_t CMD_FULL_AVAILABLE_CAPACITY_L = 0x0e;
    static const uint8_t CMD_FULL_AVAILABLE_CAPACITY_H = 0x0f;
    static const uint8_t CMD_REMAINING_CAPACITY_L = 0x10;
    static const uint8_t CMD_REMAINING_CAPACITY_H = 0x11;
    static const uint8_t CMD_FULL_CHARGE_CAPACITY_L = 0x12;
    static const uint8_t CMD_FULL_CHARGE_CAPACITY_H = 0x13;
    static const uint8_t CMD_AVG_CURRENT_L = 0x14;
    static const uint8_t CMD_AVG_CURRENT_H = 0x15;
    static const uint8_t CMD_TIME_TO_EMPTY_L = 0x16;
    static const uint8_t CMD_TIME_TO_EMPTY_H = 0x17;
    static const uint8_t CMD_TIME_TO_FULL_L = 0x18;
    static const uint8_t CMD_TIME_TO_FULL_H = 0x19;
    static const uint8_t CMD_STATE_OF_CHARGE_L = 0x2c;
    static const uint8_t CMD_STATE_OF_CHARGE_H = 0x2d;
    
    //Extended commands
    static const uint8_t CMD_DESIGN_CAPACITY_L = 0x3c;
    static const uint8_t CMD_DESIGN_CAPACITY_H = 0x3d;
    static const uint8_t CMD_DATA_FLASH_CLASS = 0x3e;
    static const uint8_t CMD_DATA_FLASH_BLOCK = 0x3f;
    static const uint8_t CMD_BLOCK_DATA_CHECKSUM = 0x60;
    static const uint8_t CMD_BLOCK_DATA_CONTROL = 0x61;
    
    //Sub commands
    static const uint16_t SCMD_CONTROL_STATUS = 0x0000;
    static const uint8_t SCMD_CONTROL_STATUS_L = 0x00;
    static const uint8_t SCMD_CONTROL_STATUS_H = 0x00;
    
    static const uint8_t SCMD_DEVICE_TYPE = 0x0001;
    static const uint8_t SCMD_DEVICE_TYPE_L = 0x01;
    static const uint8_t SCMD_DEVICE_TYPE_H = 0x00;
    
    static const uint8_t SCMD_SEALED = 0x0020;
    static const uint8_t SCMD_SEALED_L = 0x20;
    static const uint8_t SCMD_SEALED_H = 0x00;
    
    
    bool getDeviceType(uint16_t &value);
    bool getStateOfCharge(int8_t &value);
    bool getVoltage(int32_t &value);
    bool getAtRate(int32_t &value);
    bool getAverageCurrentuA(int32_t &value);
    bool getTemperature(int32_t &value);
    bool getFlags(uint16_t &value);
    bool getNomAvailableCapacity(uint16_t &value);
    bool getFullAvailableCapacity(uint16_t &value);
    bool getDesignCapacity(uint16_t &value);

    bool probe(ProbeResult& result);
    
    void enableCommunication(bool enable);
    
protected:

private:
    Bq27510(const Bq27510& right);            //!< block the copy constructor
    Bq27510& operator=(const Bq27510& right); //!< block the assignment operator

    
    
    static Bq27510* get() { return m_pBq27510; }
    
    bool Lock(void);
    bool Unlock(void);

    static Bq27510* m_pBq27510;  //!< static member to keep track of instance. Needed to prevent multiple instances
    
    I2Cbus * m_pI2C;
    
    bool m_communicationEnabled;
    

};


#endif   // Bq27510

//======== End of Bq27510.h ==========

