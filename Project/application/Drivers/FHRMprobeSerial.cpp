//! \file    FHRMprobeSerial.cpp
//! \author  Jan A. R�yneberg
//! \version 1.0
//! \date    21.10.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  21oct2013, JRo, Original.


// include self first
#include "FHRMprobeSerial.h"

// other includes
#include <board_cfg.h>
#include "RTOS.h"
#include <utilfuncs.h>
#include "isrpriocfg.h"
#include <algorithm>
#include <cassert>
#include <vector>
#include "mutex.h"


#define DAC_DHR12R1_Address      (DAC_BASE + 0x00000008)
#define DAC_DHR12R2_Address      (DAC_BASE + 0x00000014)

using namespace bls_2010;

static Mutex * sendMutex = 0;

// static member init
FHRMps*  FHRMps::m_pFHRMps = 0;

//! \brief Constructor
FHRMps::FHRMps()
{
    //OS_EnterRegion();
    if (0 == m_pFHRMps)
    {
        
        m_pFHRMps                   = this;
        
        m_transducerTimeout         = false;
        m_transducerTimeoutCnt      = 0;
        m_transducerErrCnt          = 0;
        m_transducerErrCntTot       = 0;
     
        m_fetalHeartRate            = 0;
        m_fetalHeartRateFiltered    = 0;
        m_signalQuality             = eSQnotSet;
        m_transducerState           = eTSoff;
        m_probeTempRaw              = 0;
        m_accEnergy                 = 0;
        m_dopplerSoundSignalGain    = eDSGnotSet;
        m_FHRfilterEnabled          = false;
        
        m_soundVol = eSVhigh;
        m_soundGain = Config::CFG_DOPPLER_SPEAKER_GAIN_DEFAULT;
        
        m_dopplerSoundMinMaxActive  = false;

        initUart();
        
        //Create the recieve buffer
        m_pRecieveBuffer = new uint8_t[RECIEVE_BUFFER_SIZE];
        m_pCurrRecieveBufferPos = m_pRecieveBuffer;
        m_pHTRecieveBufferPos = m_pCurrRecieveBufferPos + (RECIEVE_BUFFER_SIZE / 2);
        m_pFTRecieveBufferPos = m_pCurrRecieveBufferPos + RECIEVE_BUFFER_SIZE;
        m_synced = false;
        m_noMessagesRecieved = 0;
        m_pEncodedMsg = new uint8_t[ENCODED_MSG_BUF_SIZE];
        m_pDecodedMsg = new uint8_t[DECODED_MSG_BUF_SIZE];
        
        //Create the sound samples buffer
        m_sndBuffer.Create(SOUND_BUFFER_SIZE);        
        m_fileSndBuffer.Create(SOUND_BUFFER_SIZE);
        
        m_noSoundSamplesRecieved = 0;
        m_soundRate = 0;
        SoundRateTimerInit();
        
        m_AccelerationEnergy = new AccelerationEnergy();
        
        m_FhrFilter = new FHRfilter();
        
        //Create the serial transmit buffer
        m_transmitBuffer.Create(OUT_BUFFER_SIZE);
        
        //Reserve space in assembly buffers
        m_EncodeBuffer.reserve(OUT_BUFFER_SIZE);
        m_msgBuffer.reserve(MAX_OUT_MESSAGE_SIZE);
        
        //Create the probe accelerometer samples buffer
        m_accFileBuffer.Create(UTIL_BUFFER_SIZE);
        m_accDataTimestamp = 0;
        m_accDataTimestampSync = false;
        
        //Default disable probe sound
        m_probeSoundEnabled = false;
        
        m_POSTstatus.finished = false;
        m_POSTstatus.post.postWord = 0;
        
        m_currentAccel.AccX = 0;
        m_currentAccel.AccY = 0;
        m_currentAccel.AccZ = 0;
        
        /* USART interrupt */
        NVIC_SetPriority(USART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 1, 1));
        NVIC_EnableIRQ(USART1_IRQn);
        
        /* Enable HT & TC interrupts */
        LL_DMA_EnableIT_HT(DMA1, LL_DMA_CHANNEL_5);
        LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_5);
    
        //Enable probe
        ProbeEnable(true);
       
    }
    //OS_LeaveRegion();
}

//! \brief Destructor
FHRMps::~FHRMps()
{
    //DMA_DeInit(DMA1_Channel5); //TODO
}

void FHRMps::initUart()
{
    LL_USART_InitTypeDef USART_InitStruct = {0};
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* Peripheral clock enable */
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);
    LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);

    //USART1 GPIO Configuration
    //PA9      ------> USART1_TX
    //PA10     ------> USART1_RX 
     
    GPIO_InitStruct.Pin = LL_GPIO_PIN_9|LL_GPIO_PIN_10;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    GPIO_InitStruct.Alternate = LL_GPIO_AF_7;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    
    /* USART1 DMA init */
    LL_DMA_SetPeriphRequest(DMA1, LL_DMA_CHANNEL_5, LL_DMAMUX_REQ_USART1_RX);
    LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_5, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
    LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_5, LL_DMA_PRIORITY_LOW);
    LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_5, LL_DMA_MODE_CIRCULAR);
    LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_5, LL_DMA_PERIPH_NOINCREMENT);
    LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_5, LL_DMA_MEMORY_INCREMENT);
    LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_5, LL_DMA_PDATAALIGN_BYTE);
    LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_5, LL_DMA_MDATAALIGN_BYTE);

    LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_5, (uint32_t)&USART1->RDR);
    LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_5, (uint32_t)m_pRecieveBuffer);
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_5, RECIEVE_BUFFER_SIZE);

    
    /* DMA interrupt init */
    NVIC_SetPriority(DMA1_Channel5_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 1, 0));
    NVIC_EnableIRQ(DMA1_Channel5_IRQn);

    /* USART configuration */
    USART_InitStruct.BaudRate = 115200;
    USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
    USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
    USART_InitStruct.Parity = LL_USART_PARITY_NONE;
    USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
    USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
    USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
    LL_USART_Init(USART1, &USART_InitStruct);
    LL_USART_ConfigAsyncMode(USART1);
    LL_USART_EnableDMAReq_RX(USART1);
    LL_USART_EnableIT_TC(USART1);
    
}

//
//! Turn on or off power to probe and enable/disable USART and DMA accordingly
//! \param a_on : true = on, false = off
//
void FHRMps::ProbeEnable(bool a_on)
{
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
        
    if(a_on)
    {
        // Configure USART Tx as alternate function push-pull
        GPIO_InitStruct.Pin = LL_GPIO_PIN_9|LL_GPIO_PIN_10;
        GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
        GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
        GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
        GPIO_InitStruct.Alternate = LL_GPIO_AF_7;
        LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
        
        //Turn on power to probe
        HwAbsl::getpHw()->enable5V(true);
    
      
        //Enable USART and DMA
        LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);
        LL_USART_Enable(USART1);
        
        //Start communication timeout handler for doppler probe
        LL_TIM_EnableCounter(TIM7);
        
        //Enable USART and DMA
        LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);
        LL_USART_Enable(USART1);
        
    }
    else
    {
        //Stop communication timeout handler for doppler probe
        LL_TIM_DisableCounter(TIM7);
        
        //Disable USART and DMA
        LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_5);
        LL_USART_Disable(USART1);
        
        //Disable DAC DMA
        HwAbsl::getpHw()->getpDac()->ProbeSoundEnable(false);
        
        // Disable USART
        LL_USART_Disable(USART1);
        
        // Disable DMA1 Channel5
        LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_5);
        
        // Configure USART Tx to input with pull down so we can turn power completly off
        GPIO_InitStruct.Pin = LL_GPIO_PIN_9|LL_GPIO_PIN_10;
        GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
        GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Pull = LL_GPIO_PULL_DOWN;
        LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
        
        //Turn off power to probe
        HwAbsl::getpHw()->enable5V(false);
        
    }
    
}

//
//! Set the SW gain for doppler probe sound samples
//! \param a_gain The factor the probe samples will be multiplied with
//
void FHRMps::SetDopplerSoundSpeakerGain(uint8_t a_gain)
{
    uint8_t val = a_gain;
    val = MIN(CFG_MAX_DOPPLER_SOUND_SPEAKER_GAIN, val); //Limit
    val = MAX(CFG_MIN_DOPPLER_SOUND_SPEAKER_GAIN, val); //Limit
    m_soundGain = val;
}


//
//! COBS decode and parse a package from doppler probe
//! \param pBufStart Reference to start of buffer to be COBS decoded\n
//! \param len Length of buffer to be COBS decoded and parsed
//
void FHRMps::decodeMessage(uint8_t *pBufStart, uint16_t len)
{
    
    UnStuffData(pBufStart, len, m_pDecodedMsg, DECODED_MSG_BUF_SIZE);
        
    uint16_t calcCrc = CrcCCITT::calcCrc(m_pDecodedMsg, len - 2);
    uint16_t crc = CommunicationUtility::combine(m_pDecodedMsg[len - 1], m_pDecodedMsg[len - 2]);
    
    if(crc == calcCrc)
    {
        //okctr++;
        len -= 4; // Header and CRC
        
        switch(pBufStart[1])
        {
            case 'S': //Sound samples: [0x53]
                
#ifdef MEASURE_SOUND_PROC_TIME
                HwAbsl::getpHw()->setRedLED(true);
#endif
    
                if(m_dopplerSoundMinMaxActive)
                {
                    sampleDopplerSoundMinMaxValues(&m_pDecodedMsg[2], len);
                }
                
#ifndef USE_GAINED_SOUND_DATA 
                //Raw data to file
                m_fileSndBuffer.Put(&m_pDecodedMsg[2], len); //Remove header and CRC
#endif                
                if(m_probeSoundEnabled)
                {
                    //Set sound volume for FHR sound
                    GainSoundSamples(&m_pDecodedMsg[2], len, m_soundGain);

#ifdef USE_GAINED_SOUND_DATA 
                    //Gained sound data to file
                    m_fileSndBuffer.Put(&m_pDecodedMsg[2], len); //Remove header and CRC
#endif                    
                    //Put in sound buffer
                    m_sndBuffer.Put(&m_pDecodedMsg[2], len); //Remove header and CRC
                    
                }
                
                
                m_noSoundSamplesRecieved += len;
                
#ifdef MEASURE_SOUND_PROC_TIME
                HwAbsl::getpHw()->setRedLED(false);
#endif

                
                break;
                
            case 'A': //Accelerometer samples: [0x41]
            {

                m_accFileBuffer.Put(&m_pDecodedMsg[2], len); //Remove header and CRC
                
                m_currentAccel.AccX = (int16_t)((m_pDecodedMsg[3] << 8) | m_pDecodedMsg[2]);
                m_currentAccel.AccY = (int16_t)((m_pDecodedMsg[5] << 8) | m_pDecodedMsg[4]);
                m_currentAccel.AccZ = (int16_t)((m_pDecodedMsg[7] << 8) | m_pDecodedMsg[6]);
                
            }    
                break;
            
            case 'F': //Fetal Heart Rate [0x46]
                if(len >= 1)
                {
                    m_fetalHeartRate = pBufStart[3];
                    
                    if(m_FHRfilterEnabled)
                    {
                        //Filter
                        m_fetalHeartRateFiltered = m_FhrFilter->FilterSample(m_fetalHeartRate, m_accEnergy, m_signalQuality, OS_GetTime());
                    }
                    else
                    {
                        m_fetalHeartRateFiltered = m_fetalHeartRate;
                    }
    
                }
                break;
                
            case 'V': //Module Version info [0x56]
                if(len >= 0x1a)
                {
                    std::string s(reinterpret_cast<char const*>(&pBufStart[3]), len);
                    m_moduleVersioninfo = s;
                    if(!m_POSTstatus.finished)
                    {
                        m_POSTstatus.post.postBits.ultraSoundTransComm = false;
                        m_POSTstatus.finished = true;
                    }
                }
                break;
            
            case 'Q': //Signal Quality [0x51]
                if(len >= 1)
                {
                    m_signalQuality = static_cast<SignalQuality_t>(pBufStart[3]);
                }
                break;
            
            case 'P': //Transducer state [0x50]
                if(len >= 1)
                {
                    m_transducerState = static_cast<TransducerState_t>(pBufStart[3]);
                }
                break;
                
            case 'U': //Temperature sample [0x55]
                if(len >= 1)
                {
                    m_probeTempRaw = pBufStart[3];
                }
                break;
            case 'G': //Doppler sound signal gain: [0x47]
                if(len >= 1)
                {
                    m_dopplerSoundSignalGain = (DopplerSoundSignalGain_t )pBufStart[3];
                }
                break;
                    
            default:
                break;
        }
    }
    else
    {
        m_transducerErrCntTot++;
        m_transducerErrCnt++;
    }
        
}   

//
//! Gain sound samples from probe
//! Samples are uint16 (LSB, MSB), baseline is 2048 and range is 0 - 4095
//! \param a_pSamples Reference to beginning of buffer to sound samples\n
//! \param a_noSamples Length of buffer\n
//! \param a_gain Gain factor
//
void FHRMps::GainSoundSamples(uint8_t *a_pSamples, uint16_t a_noSamples, uint8_t a_gain)
{
    uint8_t *end = a_pSamples + a_noSamples;
    int32_t smp;
    while(a_pSamples < end)
    {
        smp = (*a_pSamples) | ((*(a_pSamples+1))<<8);
        smp -= 2048;
        smp *= a_gain;
        smp += 2048;
        
        //Limit signal
        if(smp > 4095)
        {
           smp = 4095;
        }
        else if(smp < 0)
        {
            smp = 0;
        }   
        
        *a_pSamples++ = (uint8_t)smp;
        *a_pSamples++ = (uint8_t)(smp>>8);
                    
    }
}

//
//! Send require status command to doppler probe
//
void FHRMps::SendRequireStatusCmd(void)
{

    sendBufferLock();
    
	m_msgBuffer.push_back('I');
	m_msgBuffer.push_back(0x00);	//SIZE
	
    sendMessage(m_msgBuffer);
	
    sendBufferUnlock();
    
}

//
//! Send get module version command to doppler probe
//
void FHRMps::SendGetModuleVersionCmd(void)
{
 
    sendBufferLock();
    
	m_msgBuffer.push_back('v');
	m_msgBuffer.push_back(0x00);	//SIZE
	
    sendMessage(m_msgBuffer);
	
    sendBufferUnlock();
    
}

//
//! Send set sound samples gain command to doppler probe
//! \param a_gain Gain setting: 0 = 4x gain, 1 = 1x gain (no gain)
//
void FHRMps::SendSetDopplerSoundSignalGainCmd(DopplerSoundSignalGain_t a_gain)
{
 
    sendBufferLock();
    
	m_msgBuffer.push_back('g');
	m_msgBuffer.push_back(0x01);	//SIZE
	m_msgBuffer.push_back(a_gain == 0 ? 0 : 1);
    sendMessage(m_msgBuffer);
	
    sendBufferUnlock();
    
}

//
//! Send a message to the doppler probe
//! \param msg Reference to message to send (uint8_t vector)
//
bool FHRMps::sendMessage(std::vector<uint8_t>& msg)
{
    bool success = false;
    if (!msg.empty() && msg.size() <= MAX_MSGLEN_OUTGOING)
    {
        // add CRC
        uint16_t crc = CrcCCITT::calcCrc(&msg[0], static_cast<uint8_t>(msg.size()));
        msg.push_back(CommunicationUtility::lowByte(crc));
        msg.push_back(CommunicationUtility::highByte(crc));

        m_EncodeBuffer.resize(0);
        m_EncodeBuffer.push_back(FRAMING_CHAR);

        if (StuffData(msg, m_EncodeBuffer))
        {
            m_EncodeBuffer.push_back(FRAMING_CHAR);

            writeSerialPort(&m_EncodeBuffer[0], static_cast<uint8_t>(m_EncodeBuffer.size()));
            success = true;
        }

        
    }
    
    msg.resize(0);
    
    return success;
}

//
//! lock sending message
//
bool FHRMps::sendBufferLock()
{
    if (0==sendMutex)
    {
        sendMutex = new Mutex(); // create when needed
    }
    if (sendMutex)
    {
        sendMutex->lock();
        return true;
    }
    return false;
}

//
//! unlock sending message
//
bool FHRMps::sendBufferUnlock()
{
    if (sendMutex)
    {
        sendMutex->release();
        return true;
    }
    return false;
}

//
//! Write n bytes to the serial port transmit buffer
//! \param pData Reference to buffer to get data from\n
//! \param msg Number of bytes to send from buffer
//
uint16_t FHRMps::writeSerialPort(const uint8_t * pData, uint16_t len)
{
    uint16_t bytes_written = 0;

    OS_IncDI();
    
    if (m_transmitBuffer.GetRoom() >= len)
    {
        m_transmitBuffer.Put(const_cast<uint8_t *>(pData), len);
        bytes_written = len;
    }
    OS_DecRI();

    enableCOM1TxInterrupt(); // (Re)Enable the USART1 Transmit interrupt (was disabled here, or in isr when no more items)
    return bytes_written;
}


//
//! Get current fetal heart rate the doppler probe has detected
//! \return Heart rate in BPM
//
uint8_t FHRMps::GetFetalHeartRate(void)
{ 
    
    if(m_fetalHeartRate == 1)
    {
        //Probe issue (Luckcome)
        m_fetalHeartRate = 0;
    }
    return m_fetalHeartRate;   
}

//
//! Get current fetal heart rate the doppler probe has detected filtered
//! \return Heart rate in BPM
//
uint8_t FHRMps::GetFetalHeartRateFiltered(void)
{ 
    
    if(m_fetalHeartRateFiltered == 1)
    {
        //Probe issue (Luckcome)
        m_fetalHeartRateFiltered = 0;
    }
    return m_fetalHeartRateFiltered;   
}


//
//! Get current probe temperature
//! \return Temperture raw data (0x00 = 0�C, 0xFF=63.75�C, 0.25�C/LSB
//
uint8_t FHRMps::GetProbeTempAsRaw(void)
{ 
    return m_probeTempRaw;   
}

//
//! Get current probe temperature float version
//! \return Temperture in deg.C
//
float FHRMps::GetProbeTemp(void)
{ 
    return (float)m_probeTempRaw/4.0; //0x00 = 0�C, 0xFF=63.75�C, 0.25�C/LSB, 0x00
}

//
//! Get current sound signal gain for probe 
//! Note: SendSetDopplerSoundSignalGainCmd() must me sent before calling this methode
//! \return Gain (0 = 4x, 1 = 1x)
//
FHRMps::DopplerSoundSignalGain_t FHRMps::GetDopplerSignalGain(void)
{
    return m_dopplerSoundSignalGain;
}

//
//! Get current sound signal gain for probe 
//! Note: SendGetModuleVersionCmd() must me sent before calling this methode
//! \return HW version and SW version
//
std::string FHRMps::GetModuleVersionInfo(void)
{ 
    OS_IncDI();
    std::string s = m_moduleVersioninfo;
    OS_DecRI();
    return s;   
}

//
//! Get current signal quality  probe has detected
//! \return Signal quality (noSignal, bad, critical, good)
//
FHRMps::SignalQuality_t FHRMps::GetSignalQuality(void)
{ 
    return m_signalQuality;   
}

//
//! Get current transducer state
//! \return Transducer state (on or off)
//
FHRMps::TransducerState_t FHRMps::GetTransducerState(void)
{ 
    return m_transducerState;   
}

//
//! Get whether the probe communication has timed out
//! \return false = no timout, true = timeout
//
bool FHRMps::IsTransducerTimedOut(void)
{ 
    return m_transducerTimeout;   
}

//
//! Get current message error count (since timer ISR was fired)
//! For debug purposes
//! \return Error count since timer ISR was fired
//
uint16_t FHRMps::GetTransducerErrCount(void)
{
    return m_transducerErrCnt;
}

//
//! Get current total doppler probe message errors
//! \return Error count since power on
//
uint32_t FHRMps::GetTransducerErrCountTot(void)
{
    return m_transducerErrCntTot;
}

//
//! Reset the timeout detection for doppler probe
//
void FHRMps::ResetTransducerTimeOut(void)
{ 
    m_transducerTimeout = false;
    m_transducerTimeoutCnt = 0;
}


//
//! Get doppler probe sample(s) from buffer for file
//! \param buf Reference to buffer to put data to\n
//! \param noSamples Number of bytes to put to buffer\n
//! \return Number of bytes that was put to buffer
//
uint16_t FHRMps::GetFileSoundData(uint8_t *buf, uint16_t noSamples)
{
    uint16_t noAvailableSamples = m_fileSndBuffer.GetNumItems();
    
    //DEBUG_TRACE("\r\n%u", noAvailableSamples);
    
    if(noAvailableSamples < noSamples)
    {
        m_fileSndBuffer.Get(buf, noAvailableSamples);
        return noAvailableSamples;
    }
    else
    {
        m_fileSndBuffer.Get(buf, noSamples);
    }
    
    return noSamples;
    
}

//
//! Get current acceleration energy
//! \return Current acceleration energy
//
int16_t FHRMps::GetAccEnergy(void)
{
    return m_accEnergy;
}

//
//! Get utility record(s) from buffer
//! \param buf Reference to buffer to put data to\n
//! \param noSamples Number of bytes to put to buffer\n
//! \return Number of bytes that was put to buffer
//
uint16_t FHRMps::GetFileUtilData(Utilrecord_t *buf, uint16_t noSamples)
{
    uint16_t noAvailableSamples = m_accFileBuffer.GetNumItems() / 6; //3 x uint16
    Utilrecord_t *pBuf = buf;
    uint16_t remSamples;
    int16_t accEnergyPeak = 0;
    
    if(!m_accDataTimestampSync)
    {
        //Get time stamp for first sample
        //50Hz sample rate = 20mS between each sample
        m_accDataTimestamp = OS_GetTime() - (noAvailableSamples*20);
        
        m_accDataTimestampSync = true;
    }
    
    uint32_t ts = OS_GetTime();
    
        
    int16_t acc;
    
    if(noAvailableSamples < noSamples)
    {
        noSamples = noAvailableSamples;
    }
        
    remSamples = noSamples;
    while(remSamples)
    {
        pBuf->TimeStamp = m_accDataTimestamp;
        
        if(m_accFileBuffer.Get(&acc))
        {
            pBuf->ProbeAccX = acc;
        }
        if(m_accFileBuffer.Get(&acc))
        {
            pBuf->ProbeAccY = acc;
        }
        if(m_accFileBuffer.Get(&acc))
        {
            pBuf->ProbeAccZ = acc;
        }
        
        pBuf->ProbeAccEnergy = m_AccelerationEnergy->CalcAccEnergy(pBuf->ProbeAccX, pBuf->ProbeAccY, pBuf->ProbeAccZ);
            
        if(abs(pBuf->ProbeAccEnergy) > accEnergyPeak)
        {
            accEnergyPeak = pBuf->ProbeAccEnergy;
        }
        
        m_accDataTimestamp += 20; //20 ms between each sample
        
        remSamples--;
        pBuf++;

    }
      
    m_accEnergy = accEnergyPeak;
      
    return noSamples;
    
}

//
//! Init timer for sound rate. Used to sync doppler probe sound rate to moyo sound rate
//
void FHRMps::SoundRateTimerInit(void)
{
    // Timer7 Init
    uint32_t timer_clock_frequency = 0;             /* Timer clock frequency */
    uint32_t timer_prescaler = 0;                   /* Time base prescaler to have timebase aligned on minimum frequency possible */
    uint32_t timer_reload = 0;                      /* Timer reload value in function of timer prescaler to achieve time base period */

    LL_TIM_InitTypeDef TIM_InitStruct = {0};

    TIM_InitStruct.Prescaler = 0;
    TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
    TIM_InitStruct.Autoreload = 0;
    LL_TIM_Init(TIM7, &TIM_InitStruct);
    LL_TIM_DisableARRPreload(TIM7);
    LL_TIM_SetTriggerOutput(TIM7, LL_TIM_TRGO_RESET);
    LL_TIM_DisableMasterSlaveMode(TIM7);

    /* Compute TIM7 clock */
    if (LL_RCC_GetAPB1Prescaler() == LL_RCC_APB1_DIV_1)
    {
        timer_clock_frequency = __LL_RCC_CALC_PCLK1_FREQ(SystemCoreClock, LL_RCC_GetAPB1Prescaler());
    }
    else
    {
        timer_clock_frequency = (__LL_RCC_CALC_PCLK1_FREQ(SystemCoreClock, LL_RCC_GetAPB1Prescaler()) * 2);
    }

    /* Timer prescaler calculation */
    /* (computation for timer 16 bits, additional + 1 to round the prescaler up) */
    timer_prescaler = ((timer_clock_frequency / (TIMER_PRESCALER_MAX_VALUE * TIMER_FREQUENCY_RANGE_MIN)) +1);
    /* Timer reload calculation */
    timer_reload = (timer_clock_frequency / (timer_prescaler * 5)); //5Hz
    /* Enable the timer peripheral clock */
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM7);

    /* Set timer pre-scaler value */
    LL_TIM_SetPrescaler(TIM7, (timer_prescaler - 1));

    /* Set timer auto-reload value */
    LL_TIM_SetAutoReload(TIM7, (timer_reload - 1));

    /* Counter mode: select up-counting mode */
    LL_TIM_SetCounterMode(TIM7, LL_TIM_COUNTERMODE_UP); 

    /* Set the repetition counter */
    LL_TIM_SetRepetitionCounter(TIM7, 0);
  
    /*Configure the TIM7 IRQ priority */
    NVIC_SetPriority(TIM7_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),1, 2));
  
    /* Enable the TIM7 global Interrupt and update interrupt */
    NVIC_EnableIRQ(TIM7_IRQn);
    LL_TIM_EnableIT_UPDATE(TIM7);
}

//
//! Get doppler sound sample(s) from buffer byte version
//! \param buf Reference to buffer to put data to\n
//! \param noSamples Number of bytes to put to buffer\n
//
void FHRMps::GetSoundData(uint8_t *buf, uint16_t noSamples)
{
    m_sndBuffer.Get(buf, noSamples);
}

//
//! Get doppler sound sample(s) from buffer uint version
//! \param buf Reference to buffer to put data to\n
//! \param noSamples Number of bytes to put to buffer\n
//
void FHRMps::GetSoundData(uint16_t *buf, uint16_t noSamples)
{
    uint16_t noAvailableSamples = m_sndBuffer.GetNumItems();
    
    if(noAvailableSamples < (noSamples * sizeof(uint16_t)))
    {
        
#ifdef USE_LEDS_AS_SOUND_BUFFER_IND
        GPIO_WriteBit(GPIOC, GPIO_Pin_1, Bit_RESET);
#endif
        
    }
    else
    {
        
#ifdef USE_LEDS_AS_SOUND_BUFFER_IND
        GPIO_WriteBit(GPIOC, GPIO_Pin_1, Bit_SET);
#endif
        
        m_sndBuffer.Get((uint8_t *)buf, noSamples * sizeof(uint16_t));
        
    }
    
}

//
//! Get next soundsample without increment buffer pointers
//! \return Sound sample

//
uint16_t FHRMps::PeekSoundData(void)
{
    return m_sndBuffer.Peek();
}


//
//! Get current doppler sound samples rate. Used to sync doppler probe sound rate to moyo sound rate
//! \return Number of sound samples over one second
//
uint16_t FHRMps::GetSoundRate(void)
{
    return m_soundRate/2; //We are measuring over 1 second, and samples are 12 bits (2 bytes)
}

//
//! Get current doppler sound samples buffer fill. Used to sync doppler probe sound rate to moyo sound rate
//! \return Current fill of buffer in %
//
uint16_t FHRMps::GetSoundDataFill(void)
{
    uint16_t noAvailableSamples = m_sndBuffer.GetNumItems();
    float fill = ((float)noAvailableSamples/(float)SOUND_BUFFER_SIZE) * 100 ;
    
    return (uint16_t) fill;
}


//
//! Enable or disable doppler sound samples to buffer
//! \param a_on false = disabled, true = enabled
//
void FHRMps::ProbeSoundEnable(bool a_on)
{
    m_probeSoundEnabled = a_on;
}

//
//! Get whether doppler sound samples to buffer is enabled or disabled 
//! \return false = disabled, true = enabled
//
bool FHRMps::IsProbeSoundEnabled(void)
{
    return m_probeSoundEnabled;
}

//
//! Enable or disable FHR filter used to filter doppler probe FHR to display
//! \param a_on false = disabled, true = enabled
//
void FHRMps::EnableFHRfilter(bool a_enable)
{
    m_FHRfilterEnabled = a_enable;
}



//
//! ISR handler for communication timeout handler for doppler probe
//
void FHRMps::Timer7_ISR(void)
{
    // Clear update interrupt bit
    if(LL_TIM_IsActiveFlag_UPDATE(TIM7))
    {
        LL_TIM_ClearFlag_UPDATE(TIM7);
    }
  
    m_soundRate = m_noSoundSamplesRecieved;
    
    if(m_noSoundSamplesRecieved == 0)
    {
        if(m_transducerTimeoutCnt++ >= TRANSDUCER_TIMEOUT_MAX_COUNT)
        {
            m_transducerTimeout = true;
            m_transducerTimeoutCnt = TRANSDUCER_TIMEOUT_MAX_COUNT; //Avoid rollover
        }
    }
    
    m_noSoundSamplesRecieved = 0;
    
    m_transducerErrCnt = 0;
  
}

//
//! ISR handler for UART DMA buffer (first half filled)
//
void FHRMps::DMA_Channel5_HT_ISR(void)
{
    //First half of recievebuffer is filled
    
#ifdef MEASURE_PROBE_ISR_TIME
      GPIO_WriteBit(GPIOB, GPIO_Pin_10, Bit_SET);
#endif
      
    uint8_t *pBufEnd = m_pRecieveBuffer; //Reset to start
    uint16_t len;
    uint16_t idx;
        
    //Search for end of message. Stop if half transfer buffer position reached
    while(pBufEnd < m_pHTRecieveBufferPos)
    {
        if(*pBufEnd == 0)
        {
            if(m_synced)
            {
                //Check if we hit a frame end and start (two 0's)
                if(pBufEnd < m_pCurrRecieveBufferPos)
                {
                    //Message begins in last part and extends to first part
                    len = (m_pFTRecieveBufferPos - m_pCurrRecieveBufferPos) + (pBufEnd - m_pRecieveBuffer);
                }
                else
                {
                    len = pBufEnd - m_pCurrRecieveBufferPos;
                }
                
                if(len >= MIN_MSG_LEN && len < ENCODED_MSG_BUF_SIZE)
                {
                    idx = 0;
                    len--; //Remove trailing 0

                    //Copy message to a straight buffer for COBS decode
                    while(idx <= len)
                    {
                        
                        
                        if(m_pCurrRecieveBufferPos >= m_pFTRecieveBufferPos)
                        {
                            //Message is starting in last part of buffer and ending in first part
                            m_pCurrRecieveBufferPos = m_pRecieveBuffer;
                        }
                        m_pEncodedMsg[idx++] = *m_pCurrRecieveBufferPos++;
                    }
                    
                    //Debug
                    // Disable DMA1 Channel5 for debug
                    //DMA_Cmd(DMA1_Channel5, DISABLE);
                    //
    
                    //Remove leading 0 and decode.
                    decodeMessage(&m_pEncodedMsg[1], len - 1);
                    
                    //Debug
                    // Enable DMA1 Channel5 for debug
                    //DMA_Cmd(DMA1_Channel5, ENABLE);
                    //
                    
                    m_noMessagesRecieved++;
                    
                }
                else
                {
                    m_pCurrRecieveBufferPos = pBufEnd;
                  
                }
            }
            else
            {
                m_pCurrRecieveBufferPos = pBufEnd;
                
                m_synced = true;
            }
        }
        
        pBufEnd++;
    }
    
#ifdef MEASURE_PROBE_ISR_TIME
      GPIO_WriteBit(GPIOB, GPIO_Pin_10, Bit_RESET);
#endif
    
}

//
//! ISR handler for UART DMA buffer (last half filled)
//
void FHRMps::DMA_Channel5_FT_ISR(void)
{

#ifdef MEASURE_PROBE_ISR_TIME
      GPIO_WriteBit(GPIOB, GPIO_Pin_10, Bit_SET);
#endif
      
    //Last half of recievebuffer is filled
    uint8_t *pBufEnd = m_pHTRecieveBufferPos; //Reset to half transfer pos
    uint16_t len;
    uint16_t idx;
    
    
    //Search for end of message. Stop if full transfer buffer position reached
    while(pBufEnd < m_pFTRecieveBufferPos)
    {
        if(*pBufEnd == 0)
        {
            if(m_synced)
            {
                //Check if we hit a frame end and start (two 0's)
                len = abs(pBufEnd - m_pCurrRecieveBufferPos);
                if(len >= MIN_MSG_LEN && len < ENCODED_MSG_BUF_SIZE)
                {
                    idx = 0;
                    len--; //Remove trailing 0
                    
                    while(idx <= len)
                    {
                        m_pEncodedMsg[idx++] = *m_pCurrRecieveBufferPos++;
                    }
                    
                    //Debug
                    // Disable DMA1 Channel5 for debug
                    //DMA_Cmd(DMA1_Channel5, DISABLE);
                    //
                    
                    //Remove leading 0 and decode.
                    decodeMessage(&m_pEncodedMsg[1], len - 1);
                    
                    //Debug
                    // Enable DMA1 Channel5 for debug
                    //DMA_Cmd(DMA1_Channel5, ENABLE);
                    //

                    m_noMessagesRecieved++;
                
                }
                else
                {
                    m_pCurrRecieveBufferPos = pBufEnd;
                }
            }
            else
            {
                m_pCurrRecieveBufferPos = pBufEnd;
                m_synced = true;
            }
        }
        
        pBufEnd++;
    }
    
#ifdef MEASURE_PROBE_ISR_TIME
      GPIO_WriteBit(GPIOB, GPIO_Pin_10, Bit_RESET);
#endif
    
}

void FHRMps::USART_SendData(USART_TypeDef * usart, uint8_t data)
{
    usart->TDR = (uint8_t)(data & 0xFF);
}

//
//! ISR handler for UART TX
//
void FHRMps::COM1_TxISR(void)
{
    uint8_t outByte;
    
    if(m_transmitBuffer.Get(&outByte))
    {
        // Write one byte to the transmit data register
        USART_SendData(USART1, outByte);
    }
    else
    {
        // Disable the USART Transmit interrupt
        disableCOM1TxInterrupt();
    }
}


//
//! ISR handler for UART TX
//
void FHRMps::USART1_ISR(void)
{
    if (LL_USART_IsEnabledIT_TC(USART1) && LL_USART_IsActiveFlag_TC(USART1))
    {
        FHRMps::get()->COM1_TxISR();
    }
}

//
//! ISR handler for UART DMA
//
void FHRMps::DMA_ISR(void)
{
    /* Test on DMA channel Half Transfer interrupt */
    if (LL_DMA_IsEnabledIT_HT(DMA1, LL_DMA_CHANNEL_5) && LL_DMA_IsActiveFlag_HT5(DMA1)) {
        LL_DMA_ClearFlag_HT5(DMA1);             /* Clear half-transfer complete flag */
        DMA_Channel5_HT_ISR();
    }

    /* Test on DMA channel Transfer Complete interrupt */
    if (LL_DMA_IsEnabledIT_TC(DMA1, LL_DMA_CHANNEL_5) && LL_DMA_IsActiveFlag_TC5(DMA1)) {
        LL_DMA_ClearFlag_TC5(DMA1);             /* Clear transfer complete flag */
        DMA_Channel5_FT_ISR();
    }
    
}

//
//! Get post result for doppler probe
//! \return POST bit patteren

//
POST_ctrl_t FHRMps::getPostResult(void)
{
    return m_POSTstatus;
}

//
//! Clear post result for doppler probe
//
void FHRMps::clearPostResult(void)
{
    m_POSTstatus.finished = false;
}

//
//! Post for doppler probe
//
void FHRMps::Post(void)
{
    m_POSTstatus.finished = false;
    m_POSTstatus.post.postWord = 0;
    m_POSTstatus.post.postBits.ultraSoundTransComm = true; //Set initially to failed
    
    ProbeEnable(true);
    SendGetModuleVersionCmd();
    
}

//
//! Get current acceleration energy
//! \return struct (x, y and z value)
//
Acceleration_t FHRMps::GetCurrentAccel(void)
{
    return m_currentAccel;
}

//
//! Get min/max values of a specified length of probe sound data. 
//! Sound data must be organised in LSB1,MSB1,LSB2,MSB2,LSBn,MSBn
//! Note: DopplerSoundMinMaxValuesInit() must be run in advance
//
void FHRMps::sampleDopplerSoundMinMaxValues(uint8_t *items, uint16_t noItems)
{
    for(uint16_t i = 0; i < noItems; i += 2)
    {
        if(m_dopplerSoundMinMaxRemainingSamples)
        {
            uint16_t smp = items[i+1];
            smp = smp << 8;
            smp |= items[i];
            
            m_dopplerSoundMinMaxMaxValue = MAX(smp, m_dopplerSoundMinMaxMaxValue);
            m_dopplerSoundMinMaxMinValue = MIN(smp, m_dopplerSoundMinMaxMinValue);
            
            if(m_dopplerSoundMinMaxRemainingSamples > 1)
            {
                m_dopplerSoundMinMaxRemainingSamples -= 2;
            }
            else
            {
                m_dopplerSoundMinMaxRemainingSamples = 0;
            }
            
        }
        else
        {
            //Finished
            m_dopplerSoundMinMaxActive = false;
        }
    }
}

//
//! Init the probe sound samples min/max function
//
void FHRMps::DopplerSoundMinMaxValuesInit(uint16_t a_measurementTimeInmsec)
{
    m_dopplerSoundMinMaxRemainingSamples = (uint16_t)((uint32_t)FHRMAudio::AUDIO_SOURCE_SAMPLE_FREQ * (uint32_t)a_measurementTimeInmsec / 1000 * sizeof(uint16_t));
    m_dopplerSoundMinMaxMinValue = UINT16_MAX;
    m_dopplerSoundMinMaxMaxValue = 0;
    
    m_dopplerSoundMinMaxActive = true;
}

//
//! Get the max value for the doppler sound samples
//
uint16_t FHRMps::getDopplerSoundMaxValue(void)
{
    return m_dopplerSoundMinMaxMaxValue;
}

//
//! Get the min value for the doppler sound samples
//
uint16_t FHRMps::getDopplerSoundMinValue(void)
{
    return m_dopplerSoundMinMaxMinValue;
}

//
//! Get whether the dopplerSoundMinMax function is active
//
bool FHRMps::getDopplerSoundMinMaxValuesActive(void)
{
    return m_dopplerSoundMinMaxActive;
}

/**
  * @brief This function handles TIM7 global interrupt.
  */
extern "C" void TIM7_IRQHandler(void)
{
    static uint16_t ctr = 0;
    ctr++;
    
    if(LL_TIM_IsActiveFlag_UPDATE(TIM7))
    {
        LL_TIM_ClearFlag_UPDATE(TIM7);
        
        FHRMps::get()->Timer7_ISR();
    }
    
}

/**
  * @brief This function handles USART1 global interrupt.
  */

void USART1_IRQHandler(void)
{
    static uint16_t ctr = 0;
    ctr++;
    
    FHRMps::get()->USART1_ISR();
}

/**
  * @brief This function handles DMA1 channel5 global interrupt.
  */
extern "C" void DMA1_Channel5_IRQHandler(void)
{
    static uint16_t ctr = 0;
    ctr++;
    
    FHRMps::get()->DMA_ISR();
}

/*
extern "C" void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    HwAbsl::getpHw()->getProbeSerial()->DMA_ISR();
}

extern "C" void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
    HwAbsl::getpHw()->getProbeSerial()->DMA_ISR();
}
*/