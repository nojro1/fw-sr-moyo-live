//! \file    serialportcfg.h
//! \author  Jan A. R�yneberg
//! \version 1.1
//! \date    29.04.2021

#ifndef _SERIALPORTCFG_H
#define _SERIALPORTCFG_H

// header includes
#include "isrpriocfg.h"
#include "board_cfg.h"

//! \brief Definition for COM port1, connected to USART1, GPIOA9,10
#if CFG_COM1_REMAP == 0
#define CFG_COM1                   USART1
#define CFG_COM1_GPIO              GPIOA
#define CFG_COM1_CLK               LL_APB2_GRP1_PERIPH_USART1
#define CFG_COM1_CLK_SRC           LL_RCC_USART1_CLKSOURCE_PCLK2
#define CFG_COM1_GPIO_CLK          LL_AHB2_GRP1_PERIPH_GPIOA
#define CFG_COM1_RxPin             LL_GPIO_PIN_10
#define CFG_COM1_TxPin             LL_GPIO_PIN_9
#define CFG_COM1_SET_AF_TX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM1_GPIO, CFG_COM1_TxPin, LL_GPIO_AF_7)
#define CFG_COM1_SET_AF_RX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM1_GPIO, CFG_COM1_RxPin, LL_GPIO_AF_7)
#else
//! \brief Definition for COM port1, connected to USART1 (USART1 pins remapped on GPIOB6,7)
#define CFG_COM1                   USART1
#define CFG_COM1_GPIO              GPIOB
#define CFG_COM1_CLK               LL_APB2_GRP1_PERIPH_USART1 
#define CFG_COM1_CLK_SRC           LL_RCC_USART1_CLKSOURCE_PCLK2
#define CFG_COM1_GPIO_CLK          LL_AHB2_GRP1_PERIPH_GPIOB
#define CFG_COM1_RxPin             LL_GPIO_PIN_7
#define CFG_COM1_TxPin             LL_GPIO_PIN_6
#define CFG_COM1_SET_AF_TX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM1_GPIO, CFG_COM1_TxPin, LL_GPIO_AF_7)
#define CFG_COM1_SET_AF_RX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM1_GPIO, CFG_COM1_RxPin, LL_GPIO_AF_7)
#endif

#if CFG_COM2_REMAP == 0
//! \brief Definition for COM port2, connected to USART2, GPIOA2,3
#define CFG_COM2                   USART2
#define CFG_COM2_GPIO              GPIOA
#define CFG_COM2_CLK               LL_APB1_GRP1_PERIPH_USART2
#define CFG_COM2_CLK_SRC           LL_RCC_USART2_CLKSOURCE_PCLK1
#define CFG_COM2_GPIO_CLK          LL_AHB2_GRP1_PERIPH_GPIOA
#define CFG_COM2_RxPin             LL_GPIO_PIN_3
#define CFG_COM2_TxPin             LL_GPIO_PIN_2
#define CFG_COM2_SET_AF_TX_PIN()   LL_GPIO_SetAFPin_0_7(CFG_COM2_GPIO, CFG_COM2_TxPin, LL_GPIO_AF_7)
#define CFG_COM2_SET_AF_RX_PIN()   LL_GPIO_SetAFPin_0_7(CFG_COM2_GPIO, CFG_COM2_RxPin, LL_GPIO_AF_7)
#else
//! \brief Definition for COM port2, connected to USART2 (USART2 pins remapped on GPIOD5,6)
#define CFG_COM2                   USART2
#define CFG_COM2_GPIO              GPIOD
#define CFG_COM2_CLK               LL_APB1_GRP1_PERIPH_USART2
#define CFG_COM2_CLK_SRC           LL_RCC_USART2_CLKSOURCE_PCLK1
#define CFG_COM2_GPIO_CLK          LL_AHB2_GRP1_PERIPH_GPIOD
#define CFG_COM2_RxPin             LL_GPIO_PIN_6
#define CFG_COM2_TxPin             LL_GPIO_PIN_5
#define CFG_COM2_SET_AF_TX_PIN()   LL_GPIO_SetAFPin_0_7(CFG_COM2_GPIO, CFG_COM2_TxPin, LL_GPIO_AF_7)
#define CFG_COM2_SET_AF_RX_PIN()   LL_GPIO_SetAFPin_0_7(CFG_COM2_GPIO, CFG_COM2_RxPin, LL_GPIO_AF_7)
#endif

#if CFG_COM3_REMAP == 0
//! \brief Definition for COM port3, connected to USART3, GPIOB10,11
#define CFG_COM3                   USART3
#define CFG_COM3_GPIO              GPIOB
#define CFG_COM3_CLK               LL_APB1_GRP1_PERIPH_USART3
#define CFG_COM3_CLK_SRC           LL_RCC_USART3_CLKSOURCE_PCLK1
#define CFG_COM3_GPIO_CLK          LL_AHB2_GRP1_PERIPH_GPIOB
#define CFG_COM3_RxPin             LL_GPIO_PIN_11
#define CFG_COM3_TxPin             LL_GPIO_PIN_10
#define CFG_COM3_SET_AF_TX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM3_GPIO, CFG_COM3_TxPin, LL_GPIO_AF_7)
#define CFG_COM3_SET_AF_RX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM3_GPIO, CFG_COM3_RxPin, LL_GPIO_AF_7)
#elif CFG_COM3_REMAP == 1
//! \brief Definition for COM port3, connected to USART3 (USART3 pins remapped on GPIOC4,5)
#define CFG_COM3                   USART3
#define CFG_COM3_GPIO              GPIOC
#define CFG_COM3_CLK               LL_APB1_GRP1_PERIPH_USART3
#define CFG_COM3_CLK_SRC           LL_RCC_USART3_CLKSOURCE_PCLK1
#define CFG_COM3_GPIO_CLK          LL_AHB2_GRP1_PERIPH_GPIOC
#define CFG_COM3_RxPin             LL_GPIO_PIN_5
#define CFG_COM3_TxPin             LL_GPIO_PIN_4
#define CFG_COM3_SET_AF_TX_PIN()   LL_GPIO_SetAFPin_0_7(CFG_COM3_GPIO, CFG_COM3_TxPin, LL_GPIO_AF_7)
#define CFG_COM3_SET_AF_RX_PIN()   LL_GPIO_SetAFPin_0_7(CFG_COM3_GPIO, CFG_COM3_RxPin, LL_GPIO_AF_7)
#elif CFG_COM3_REMAP == 2
//! \brief Definition for COM port3, connected to USART3 (USART3 pins remapped on GPIOC10,11)
#define CFG_COM3                   USART3
#define CFG_COM3_GPIO              GPIOC
#define CFG_COM3_CLK               LL_APB1_GRP1_PERIPH_USART3
#define CFG_COM3_CLK_SRC           LL_RCC_USART3_CLKSOURCE_PCLK1
#define CFG_COM3_GPIO_CLK          LL_AHB2_GRP1_PERIPH_GPIOC
#define CFG_COM3_RxPin             LL_GPIO_PIN_11
#define CFG_COM3_TxPin             LL_GPIO_PIN_10
#define CFG_COM3_SET_AF_TX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM3_GPIO, CFG_COM3_TxPin, LL_GPIO_AF_7)
#define CFG_COM3_SET_AF_RX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM3_GPIO, CFG_COM3_RxPin, LL_GPIO_AF_7)
#else
//! \brief Definition for COM port3, connected to USART3 (USART3 pins remapped on GPIOD8,9)
#define CFG_COM3                   USART3
#define CFG_COM3_GPIO              GPIOD
#define CFG_COM3_CLK               LL_APB1_GRP1_PERIPH_USART3
#define CFG_COM3_CLK_SRC           LL_RCC_USART3_CLKSOURCE_PCLK1
#define CFG_COM3_GPIO_CLK          LL_AHB2_GRP1_PERIPH_GPIOD
#define CFG_COM3_RxPin             LL_GPIO_PIN_9
#define CFG_COM3_TxPin             LL_GPIO_PIN_8
#define CFG_COM3_SET_AF_TX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM3_GPIO, CFG_COM3_TxPin, LL_GPIO_AF_7)
#define CFG_COM3_SET_AF_RX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM3_GPIO, CFG_COM3_RxPin, LL_GPIO_AF_7)
#endif

//! \brief Definition for COM port4, connected to UART4
#define CFG_COM4                   UART4
#define CFG_COM4_GPIO              GPIOC
#define CFG_COM4_CLK               LL_APB1_GRP1_PERIPH_UART4
#define CFG_COM4_CLK_SRC           LL_RCC_UART4_CLKSOURCE_PCLK1
#define CFG_COM4_GPIO_CLK          LL_AHB2_GRP1_PERIPH_GPIOC
#define CFG_COM4_RxPin             LL_GPIO_PIN_11
#define CFG_COM4_TxPin             LL_GPIO_PIN_10
#define CFG_COM4_SET_AF_TX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM4_GPIO, CFG_COM4_TxPin, LL_GPIO_AF_7)
#define CFG_COM4_SET_AF_RX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM4_GPIO, CFG_COM4_RxPin, LL_GPIO_AF_7)

//! \brief Definition for COM port5, connected to UART5
#define CFG_COM5                   UART5
#define CFG_COM5_GPIO              GPIOC
#define CFG_COM5_CLK               LL_APB1_GRP1_PERIPH_UART5
#define CFG_COM5_CLK_SRC           LL_RCC_UART5_CLKSOURCE_PCLK1
#define CFG_COM5_GPIO_CLK          LL_AHB2_GRP1_PERIPH_GPIOC
#define CFG_COM5_RxPin             LL_GPIO_PIN_2 // on GPIOD !!!!
#define CFG_COM5_TxPin             LL_GPIO_PIN_12
#define CFG_COM5_SET_AF_TX_PIN()   LL_GPIO_SetAFPin_8_15(CFG_COM5_GPIO, CFG_COM5_TxPin, LL_GPIO_AF_7)
#define CFG_COM5_SET_AF_RX_PIN()   LL_GPIO_SetAFPin_0_7(CFG_COM5_GPIO, CFG_COM5_RxPin, LL_GPIO_AF_7)

#endif   // _SERIALPORTCFG_H

//======== End of serialportcfg.h ==========

