//! \file    EmulatedEEprom.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    26.10.2010

#ifndef EMULATED_EEPROM_H
#define EMULATED_EEPROM_H

#ifdef _MSC_VER
  #if (_MSC_VER < 1600) /* Vs 2010 should have cstdint */
    #include "stdint.h"
  #else
    #include <cstdint>
  #endif
#else
  #include <cstdint>
#endif
#include "board_cfg.h"
#include "EEPROM_Base.h"
#include "EmulatedEEprom\eeprom.h"

//! \brief The EmulatedEEprom implemements EEPROM emulation in the STM32's internal flash\n
//! according to application note AN2594. 2KB of flash memory will be used for this, residing at end of memory.
//! \remark If a write operation results in a 1K page erase, program execution halts for approx. 20 ms\n
//! so any interrupts during this time will be delayed and may cause problems
class EmulatedEEprom : public EEPROM_Base
{
    public:
        // constructor/destructor
        explicit EmulatedEEprom();
        virtual ~EmulatedEEprom();

        // user memory commands
        bool eeWrite(uint8_t data, uint16_t writeAddr) const;
        bool eeWrite(uint16_t data, uint16_t writeAddr) const;
        bool eeWrite(uint32_t data, uint16_t writeAddr) const;
        bool eeWrite(const uint8_t* pBuffer, uint16_t writeAddr, uint16_t numByteToWrite) const;

        bool eeRead(uint16_t readAddr, uint8_t& data) const;
        bool eeRead(uint16_t readAddr, uint16_t& data) const;
        bool eeRead(uint16_t readAddr, uint32_t& data) const;
        bool eeRead(uint8_t* pBuffer, uint16_t readAddr, uint16_t numByteToRead) const;
        uint32_t getMemorySize() const {return sEE_MEMSIZE;}

        static const uint16_t sEE_MEMSIZE = NB_OF_VAR*2;  //!< # byte locations of user memory

    protected:

    private:
        EmulatedEEprom(const EmulatedEEprom& right);            //!< block the copy constructor
        EmulatedEEprom& operator=(const EmulatedEEprom& right); //!< block the assignment operator

};

#endif // EMULATED_EEPROM_H

