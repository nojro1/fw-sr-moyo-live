//! \file    RTC.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    05.07.2012

//Prototypes for interrupt service routines
extern "C"
{
    //isr();
}

// include self first
#include "rtc.h"

// other includes
#include "RTOS.h"
#include "isrpriocfg.h"


// static member init
Rtc*  Rtc::m_pRtc = 0;


//! \brief Constructor
//! \param None\n
Rtc::Rtc(void)
{
    
    if (0 == m_pRtc)
    {
        m_pRtc = this;
        
        m_error = false;
        
        // Initialize Date and Time structure
        m_date.Month = 01;
        m_date.Day = 01;
        m_date.Year = 20;
        m_time.Seconds = 0;
        m_time.Minutes = 0;
        m_time.Hours = 0;
        
        LL_RTC_InitTypeDef RTC_InitStruct = {0};
        
        //LL_RCC_SetRTCClockSource(LL_RCC_RTC_CLKSOURCE_LSE); //TODO
        LL_RCC_SetRTCClockSource(LL_RCC_RTC_CLKSOURCE_LSI);

        /* Peripheral clock enable */
        LL_RCC_EnableRTC();
        LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_RTCAPB);
        
        //__PWR_CLK_ENABLE();
        //HAL_PWR_EnableBkUpAccess();
        
        
        RTC_InitStruct.HourFormat = LL_RTC_HOURFORMAT_24HOUR;
        RTC_InitStruct.AsynchPrescaler = 127;
        RTC_InitStruct.SynchPrescaler = 255;

        if((RTC->ICSR & RTC_ICSR_INITS) != RTC_ICSR_INITS)
        {
            /** Initialize RTC and set the Time and Date 
            */

            LL_RTC_Init(RTC, &RTC_InitStruct);
            LL_RTC_SetBackupRegisterPrivilege(RTC, LL_RTC_PRIVILEGE_BKUP_ZONE_NONE);
            LL_RTC_SetBackupRegProtection(RTC, LL_RTC_BKP_DR0, LL_RTC_BKP_DR0);
            if(LL_RTC_BKP_GetRegister(RTC,LL_RTC_BKP_DR0) != 0x32F2)
            {

                if(LL_RTC_TIME_Init(RTC, LL_RTC_FORMAT_BCD, &m_time) == ERROR)
                {
                    LL_RCC_ForceBackupDomainReset();
                    LL_RCC_ReleaseBackupDomainReset();
                    if(LL_RTC_TIME_Init(RTC, LL_RTC_FORMAT_BCD, &m_time) == ERROR)
                    {
                        m_error = true;
                    }

                }
                
                if(LL_RTC_DATE_Init(RTC, LL_RTC_FORMAT_BCD, &m_date) == ERROR)
                {
                    LL_RCC_ForceBackupDomainReset();
                    LL_RCC_ReleaseBackupDomainReset();
                    if(LL_RTC_DATE_Init(RTC, LL_RTC_FORMAT_BCD, &m_date) == ERROR)
                    {
                        m_error = true;
                    }
                }
                LL_RTC_BKP_SetRegister(RTC,LL_RTC_BKP_DR0,0x32F2);
            }
            /** Enable the WakeUp 
            */
            LL_RTC_WAKEUP_Enable(RTC);
            LL_RTC_WAKEUP_SetClock(RTC, LL_RTC_WAKEUPCLOCK_DIV_16);
            LL_RTC_WAKEUP_SetAutoReload(RTC, 0);
            LL_RTC_SetAlarmOutEvent(RTC, LL_RTC_ALARMOUT_DISABLE);
            LL_RTC_DisableAlarmPullUp(RTC);
        }
  
    }

    
}


//! destructor
Rtc::~Rtc()
{

    //OS_EnterRegion();
    m_pRtc = 0;
    //OS_LeaveRegion();
}




//! \brief Set date and time
//! \param a_date (date_t)
//! \param a_time (time_t)
void Rtc::RTCsetDateTime(LL_RTC_DateTypeDef a_date, LL_RTC_TimeTypeDef a_time)
{
    m_date.Month = a_date.Month;
    m_date.Day = a_date.Day;
    m_date.Year = a_date.Year;
    
    m_time.Seconds = a_time.Seconds;
    m_time.Minutes = a_time.Minutes;
    m_time.Hours = a_time.Hours;
    
    //Check ranges
    
    //Year
    if(m_date.Year > 99)
    {
        m_date.Year = 99;
    }
    else if(m_date.Year < 20)
    {
        m_date.Year = 20;
    }
    
    //Month
    if(m_date.Month < 1)
    {
        m_date.Month = 1;
    }
    else if(m_date.Month > 12)
    {
        m_date.Month = 12;
    }
    
    //Day
    
    if(m_date.Day < 1)
    {
        m_date.Day = 1;
    }
    else
    {
        switch(m_date.Month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                if(m_date.Day > 31)
                {
                    m_date.Day = 31;
                }
                break;
            case 2:
                if(IsLeapYear(m_date.Year))
                {
                    if(m_date.Day > 29)
                    {
                        m_date.Day = 29;
                    }
                }
                else
                {
                    if(m_date.Day > 28)
                    {
                        m_date.Day = 28;
                    }
                }
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                if(m_date.Day > 30)
                {
                    m_date.Day = 30;
                }
                break;
        }
    }
    
    //Time
    
    if(m_time.Hours > 23)
    {
        m_time.Hours = 23;
    }
    if(m_time.Minutes > 59)
    {
        m_time.Minutes = 59;
    }
    if(m_time.Seconds > 59)
    {
        m_time.Seconds = 59;
    }
    
    //Not used. Set to 0
    m_date.WeekDay = 0;
    
    if(LL_RTC_TIME_Init(RTC, LL_RTC_FORMAT_BCD, &m_time) == ERROR)
    {
        LL_RCC_ForceBackupDomainReset();
        LL_RCC_ReleaseBackupDomainReset();
        if(LL_RTC_TIME_Init(RTC, LL_RTC_FORMAT_BCD, &m_time) == ERROR)
        {
            m_error = true;
        }

    }
    
    if(LL_RTC_DATE_Init(RTC, LL_RTC_FORMAT_BCD, &m_date) == ERROR)
    {
        LL_RCC_ForceBackupDomainReset();
        LL_RCC_ReleaseBackupDomainReset();
        if(LL_RTC_DATE_Init(RTC, LL_RTC_FORMAT_BCD, &m_date) == ERROR)
        {
            m_error = true;
        }
    }
}

//! \brief Set date
//! \param a_date (date_t)
void Rtc::RTCsetDate(LL_RTC_DateTypeDef a_date)
{
    RTCsetDateTime(a_date, m_time);
}

//! \brief Set time
//! \param a_time (time_t)
void Rtc::RTCsetTime(LL_RTC_TimeTypeDef a_time)
{
    RTCsetDateTime(m_date, a_time);
}


//TODO: There is a risk to get time of one day, and date of another. FixMe

//! \brief Get date
//! \return date (date_t)
LL_RTC_DateTypeDef Rtc::RTCgetDate()
{
    //Need to get first time and then date due to lock mecanism in HAL
    LL_RTC_TIME_Get(RTC);
    uint32_t dt = LL_RTC_DATE_Get(RTC);
    
    m_date.Year  = __LL_RTC_GET_YEAR(dt);
    m_date.Month = __LL_RTC_GET_MONTH(dt);
    m_date.Day   = __LL_RTC_GET_DAY(dt);
    
    return m_date;
}

//! \brief Get time
//! \return time (time_t)
LL_RTC_TimeTypeDef Rtc::RTCgetTime()
{
    //Need to get first time and then date due to lock mecanism in HAL
    uint32_t tm = LL_RTC_TIME_Get(RTC);
    LL_RTC_DATE_Get(RTC);
    
    m_time.Hours   = __LL_RTC_GET_HOUR(tm);
    m_time.Minutes = __LL_RTC_GET_MINUTE(tm);
    m_time.Seconds = __LL_RTC_GET_SECOND(tm);
    
    return m_time;
}

//! \brief Calculate if the year is a leap year
//! \param a_nYear year to check (e.g. 2013)
uint8_t Rtc::IsLeapYear(uint16_t a_nYear)
{
    if(a_nYear % 4 != 0) return 0;
    if(a_nYear % 100 != 0) return 1;
    return (uint8_t)(a_nYear % 400 == 0);
}

bool Rtc::RTCgetError(void)
{
    return m_error;
}

uint32_t Rtc::getFatTime(void)
{
    LL_RTC_TimeTypeDef time = RTCgetTime();
    LL_RTC_DateTypeDef date = RTCgetDate();
    
    return ((uint32_t)(2000 + (uint32_t)(date.Year - 1980)) << 25 | 
            (uint32_t)date.Month << 21 | 
            (uint32_t)date.Day << 16 |
            (uint32_t)time.Hours << 11 |
            (uint32_t)time.Minutes << 5 |
            (uint32_t)time.Seconds);    
    
}
//! \brief RTC ISR wrapper. Called every second
void Rtc::RTC_ISR()
{
    Rtc::get()->Rtc::isr();
}


//! \brief Fatfs get_fattime wrapper.
#if _FS_NORTC == 0
DWORD get_fattime(void)
{
    return (DWORD)(Rtc::get()->Rtc::getFatTime());
}
#endif
