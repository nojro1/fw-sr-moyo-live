//! \file    Watchdog.h
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    17.04.2020

#ifndef _WATCHDOG_H
#define _WATCHDOG_H

// header includes
#include "stm32l5xx_hal.h"

//! \brief The Watchdog class for the independent watchdog\n
//! The watchdog timeout is specified in board_cfg.h
class Watchdog
{
public:
    // constructor/destructor
    explicit Watchdog();
    virtual ~Watchdog();

    void kick();
    void start();

    //bool watchdogResetDetected() const;

    //virtual void expected_period(struct period& prd) const;

protected:

private:
    Watchdog(const Watchdog& right);            //!< block the copy constructor
    Watchdog& operator=(const Watchdog& right); //!< block the assignment operator

    static Watchdog* m_pWdObj;                  //!< keep track of instances, only one allowed

    IWDG_HandleTypeDef m_wdt;                   //!< Pointer to the WDT peripheral itself
};


#endif   // _WATCHDOG_H

//======== End of Watchdog.h ==========


