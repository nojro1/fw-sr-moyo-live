//! \file    DacDma.cpp
//! \author  Jan A. R�yneberg
//! \version 1.0
//! \date    14.10.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  14oct2013, JRo, Original.


// include self first
#include "DacDMA.h"

// other includes
#include <board_cfg.h>
#include <adccfg.h>
#include "RTOS.h"
#include <utilfuncs.h>
#include "isrpriocfg.h"
#include <algorithm>
#include <cassert>


// static member init
DacDma*  DacDma::m_pDac = 0;

//! \brief Constructor
//! \param channelMask A 8 bit mask, defining which channels to be used\n
//! Possible values = 0x01 for DAC_Channel_1, 0x02 for DAC_Channel_2 and 0x03 for both channels \n
//! \param periphInit Specified initialisation parameters (Timer used for playback, TIM6)
DacDma::DacDma()
{
    //OS_EnterRegion();
    if (0 == m_pDac)
    {
        //m_playbackTimer.Instance = periphInit.timerInstance;
        
        //Currently we only support channel 1 with TIM6
        //assert(TIM6==m_playbackTimer.Instance);
        //assert(channelMask ==1);

        m_pDac = this;

        m_mode = DACMODE_INIT;
        
        m_alarmSound = 0;
        m_alarmSoundStart = 0;
        m_alarmSoundEnd = 0;
        m_loopAlarmSound = false;
        
        m_soundState = eSPCstopped;
        m_soundVolume = eSVhigh;
        
        //Create the sound data double buffer
        m_pSoundBuffer1 = new uint16_t[SOUND_BUF_SIZE];
        m_pSoundBuffer2 = new uint16_t[SOUND_BUF_SIZE];
            
        //Init the buffers to 2048 (baseline)
        for(uint16_t i = 0; i < SOUND_BUF_SIZE; i++)
        {
            m_pSoundBuffer1[i] = 2048;
            m_pSoundBuffer2[i] = 2048;
        }
        
        LL_DAC_InitTypeDef DAC_InitStruct = {0};

        LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

        /* Peripheral clock enable */
        LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_DAC1);
        LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);
        LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA2);
        LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMAMUX1);
        
        /**DAC1 GPIO Configuration  
        PA4   ------> DAC1_OUT1 
        */
        GPIO_InitStruct.Pin = Audio_P_Pin;
        GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
        GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
        LL_GPIO_Init(Audio_P_GPIO_Port, &GPIO_InitStruct);
        
        /* DMA interrupt init */
        NVIC_SetPriority(DMA2_Channel3_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 2, 0));
    
        // Enable DMA2 Channel3 Transfer Complete interrupt
        LL_DMA_EnableIT_TC(DMA2, LL_DMA_CHANNEL_3);
                           
        // Enable DMA2 Channel3 Half Transfer interrupt
        //LL_DMA_EnableIT_HT(DMA2, LL_DMA_CHANNEL_3);
        
        // Enable DMA2 Channel3 interrupt
        NVIC_EnableIRQ(DMA2_Channel3_IRQn);
        
        
    
        

        /* DAC1 DMA Init */

        /* DAC1_CH1 Init */
        LL_DMA_SetPeriphRequest(DMA2, LL_DMA_CHANNEL_3, LL_DMAMUX_REQ_DAC1_CH1);

        LL_DMA_SetDataTransferDirection(DMA2, LL_DMA_CHANNEL_3, LL_DMA_DIRECTION_MEMORY_TO_PERIPH);

        LL_DMA_SetChannelPriorityLevel(DMA2, LL_DMA_CHANNEL_3, LL_DMA_PRIORITY_HIGH);

        LL_DMA_SetMode(DMA2, LL_DMA_CHANNEL_3, LL_DMA_MODE_CIRCULAR);

        LL_DMA_SetPeriphIncMode(DMA2, LL_DMA_CHANNEL_3, LL_DMA_PERIPH_NOINCREMENT);

        LL_DMA_SetMemoryIncMode(DMA2, LL_DMA_CHANNEL_3, LL_DMA_MEMORY_INCREMENT);

        LL_DMA_SetPeriphSize(DMA2, LL_DMA_CHANNEL_3, LL_DMA_PDATAALIGN_HALFWORD);

        LL_DMA_SetMemorySize(DMA2, LL_DMA_CHANNEL_3, LL_DMA_MDATAALIGN_HALFWORD);

        LL_DMA_DisableChannelPrivilege(DMA2, LL_DMA_CHANNEL_3);
        
        LL_DMA_EnableDoubleBufferMode(DMA2, LL_DMA_CHANNEL_3);
        
        //Set peripheral and double buffer addresses
        LL_DMA_SetPeriphAddress(DMA2, LL_DMA_CHANNEL_3, LL_DAC_DMA_GetRegAddr(DAC1, LL_DAC_CHANNEL_1, LL_DAC_DMA_REG_DATA_12BITS_RIGHT_ALIGNED));
        
        //Lib does not support doble buffer address 2, so do it directly
        DMA_Channel_TypeDef *tmp;
        tmp = (DMA_Channel_TypeDef *)(__LL_DMA_GET_CHANNEL_INSTANCE(DMA2, LL_DMA_CHANNEL_3));
        WRITE_REG(tmp->CM0AR, (uint32_t)m_pSoundBuffer1);
        WRITE_REG(tmp->CM1AR, (uint32_t)m_pSoundBuffer2);
        
        //Set data length (number of transfers)
        LL_DMA_SetDataLength(DMA2, LL_DMA_CHANNEL_3, SOUND_BUF_SIZE/2);
        
        //LL_DMA_SetCurrentTargetMem(DMA2, LL_DMA_CHANNEL_3, LL_DMA_CURRENTTARGETMEM0);
        

        /** DAC channel OUT1 config 
        */
        DAC_InitStruct.TriggerSource = LL_DAC_TRIG_EXT_TIM6_TRGO;
        DAC_InitStruct.WaveAutoGeneration = LL_DAC_WAVE_AUTO_GENERATION_NONE;
        DAC_InitStruct.OutputBuffer = LL_DAC_OUTPUT_BUFFER_ENABLE;
        DAC_InitStruct.OutputConnection = LL_DAC_OUTPUT_CONNECT_INTERNAL;
        DAC_InitStruct.OutputMode = LL_DAC_OUTPUT_MODE_NORMAL;
        LL_DAC_Init(DAC1, LL_DAC_CHANNEL_1, &DAC_InitStruct);
        LL_DAC_DisableTrigger(DAC1, LL_DAC_CHANNEL_1);

        // Playback timer configuration
        LL_TIM_InitTypeDef timInit;
        LL_TIM_StructInit(&timInit);
        
        /* Peripheral clock enable */
        LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM6);
        
        LL_TIM_DisableARRPreload(TIM6);
        LL_TIM_SetClockSource(TIM6, LL_TIM_CLOCKSOURCE_INTERNAL);
        
        timInit.Prescaler                       = 0;
        timInit.ClockDivision                   = LL_TIM_CLOCKDIVISION_DIV1;
        timInit.CounterMode                     = TIM_COUNTERMODE_UP;
        timInit.Autoreload                      = 0;
        if (LL_TIM_Init(TIM6, &timInit) != SUCCESS)
        {
            //Error_Handler(); //TODO
        }
        
        LL_TIM_SetTriggerOutput(TIM6, LL_TIM_TRGO_UPDATE);
        LL_TIM_DisableMasterSlaveMode(TIM6);
        
        //NVIC_EnableIRQ(TIM6_IRQn);
        LL_TIM_EnableIT_UPDATE(TIM6);
       
    }
    
}


//! \brief Destructor
DacDma::~DacDma()
{
    /*
    if (HAL_DAC_DeInit(&m_hdac) != HAL_OK)
    {
        //Error_Handler();//TODO
    }
    
    // TIMx disable counter
    if (HAL_TIM_Base_Stop(&m_playbackTimer) != HAL_OK)
    {
        //Error_Handler();//TODO
    }
    */
    
}

//! \brief Stop current sound
void DacDma::StopWaveformPlayback(void)
{
    m_loopAlarmSound = false;
    m_soundState = eSPCinit;
}
        
//! \brief Interrupt doppler sound with alarm sound
//! Trim transition so no clicks in sound is heard
bool DacDma::AlarmSoundInterrupt(const uint16_t * pData, uint32_t numsamples, bool loop)
{
    //Set fixed clock for DAC TODO
    LL_TIM_SetAutoReload(TIM6, 18000);
    
    //Keep playing probe sound until we get half transfer intterupt
    //After this we will need to adjust DAC to zero (2048)
    m_alarmSound = const_cast<uint16_t *>(pData);
    m_alarmSoundStart = m_alarmSound;
    m_alarmSoundEnd = m_alarmSound + numsamples;
    m_loopAlarmSound = loop;
    m_soundState = eSPCwaitFinishProbeBuffer;
    
    //Stop probe serial so we don't fill up buffer when we play alarm sound. Keep status
    m_probeSoundEnabled = HwAbsl::getpHw()->getProbeSerial()->IsProbeSoundEnabled();
    HwAbsl::getpHw()->getProbeSerial()->ProbeSoundEnable(false);
    
    return true;
}


//! \brief Setup double buffer system for playing sound on the selected DAC channel
//!        Uses DMA half and full transfer interrupt to handle the double buffer system.
//!        Half transfer interrupt fills the free buffer, full transfer interrupt swap buffers.
//!        Note: Dependent of FHRMprobeSerial for getting sound data.
//! \param channel DAC channel (DAC_CHANNEL_1, DAC_CHANNEL_2)
//! \param sampleFreq Sample frequency of buffered data
//! \return true if success, else false
bool DacDma::startSound(uint32_t sampleFreq)
{
    
    m_soundState = eSPCinit;
    
    
    //Fill buffer1
    HwAbsl::getpHw()->getProbeSerial()->GetSoundData(m_pSoundBuffer1, SOUND_BUF_SIZE);
    //m_soundBufferActive = 1;
    m_probeSoundEnabled = false;
    

    // Playback timer configuration
    uint32_t timerPeriod = 0;
    uint16_t prescaler = 0;
    LL_RCC_ClocksTypeDef RCC_Clocks;
    LL_RCC_GetSystemClocksFreq(&RCC_Clocks);
    
    const uint32_t TIMxCLK = RCC_Clocks.PCLK1_Frequency*((RCC->CFGR & RCC_CFGR_PPRE1) == RCC_CFGR_PPRE1_DIV1 ? 1 : 2); // the driver clock for the timer
    uint32_t max_count = TIMxCLK / std::max(sampleFreq, 1u) - 1;
    if (max_count > 0xFFFF)
    {
        prescaler = max_count/0xFFFF;
    }
    else
    {
        prescaler = 0;
    }
    timerPeriod = (TIMxCLK / (std::max(sampleFreq, 1u)  * (prescaler + 1))) - 1;
    m_timerPeriod = timerPeriod;

    LL_TIM_SetPrescaler(TIM6, prescaler);
    LL_TIM_SetAutoReload(TIM6, timerPeriod);
    
    LL_DAC_SetTriggerSource(DAC1, DAC_CHANNEL_1, LL_DAC_TRIG_EXT_TIM6_TRGO);
    
    // TIMx enable counter
    LL_TIM_EnableCounter(TIM6);
    m_mode = DACMODE_PLAYBACK;
    
    LL_DAC_Disable(DAC1, DAC_CHANNEL_1);
    LL_DMA_SetMemoryAddress(DMA2, LL_DMA_CHANNEL_3, (uint32_t)m_pSoundBuffer1);
    LL_DMA_SetDataLength(DMA2, LL_DMA_CHANNEL_3, SOUND_BUF_SIZE);
        
    // Enable DMA2 Channel3
    LL_DMA_EnableChannel(DMA2, LL_DMA_CHANNEL_3);
    
    LL_DAC_EnableDMAReq(DAC1, DAC_CHANNEL_1);
    LL_DAC_Enable(DAC1, DAC_CHANNEL_1);
    LL_DAC_EnableTrigger(DAC1, DAC_CHANNEL_1);
    
    return true;
}

//
//! \brief Set DAC timer periode, so we are able to sync speed with probe sound data
//! \param period DAC timer reload value
//
void DacDma::setDacTimerPeriod(uint16_t period)
{
    if(m_soundState == eSPCplayProbeSound)
    {
        //Do not change DAC clock when playing alarm
        m_timerPeriod = period;
        LL_TIM_SetAutoReload(TIM6, m_timerPeriod);
    }
}

//
//! \brief Get current DAC timer periode
//! \return period DAC timer reload value
uint16_t DacDma::getDacTimerPeriod(void)
{
    return m_timerPeriod;
}

//
//! \brief Calculate the nominal DAC timer periode from sampling frequency
//! \param period Sound sample freq in Hz
//! \return DAC timer reload value
//
uint32_t DacDma::calcDacTimerPeriodFromSampleFreq(uint32_t sampleFreq)
{
    uint16_t prescaler = 0;
    LL_RCC_ClocksTypeDef RCC_Clocks;
    LL_RCC_GetSystemClocksFreq(&RCC_Clocks);
    uint32_t frequency = RCC_Clocks.PCLK1_Frequency;
    const uint32_t TIMxCLK = frequency*((RCC->CFGR & RCC_CFGR_PPRE1) == RCC_CFGR_PPRE1_DIV1 ? 1 : 2); // the driver clock for the timer
    uint32_t max_count = TIMxCLK / std::max(sampleFreq, 1u) - 1;
    if (max_count > 0xFFFF)
    {
        prescaler = max_count/0xFFFF;
    }
    else
    {
        prescaler = 0;
    }
    return (TIMxCLK / (std::max(sampleFreq, 1u)  * (prescaler + 1))) - 1;
}

//
//! \brief Start or stop sound sampling for doppler probe
//! \param a_on false = disable, true = enable
//
void DacDma::ProbeSoundEnable(bool a_on)
{
    m_probeSoundEnabled = a_on;
    
    //Update probe serial so we don't fill up buffer when we stop buffer fill interrupt
    HwAbsl::getpHw()->getProbeSerial()->ProbeSoundEnable(a_on);
    
    if(a_on)
    {
        //Start buffer fill interrupt
        LL_DMA_EnableIT_TC(DMA2, LL_DMA_CHANNEL_3);
        LL_DMA_EnableIT_HT(DMA2, LL_DMA_CHANNEL_3);
    }
    else
    {
        //Stop buffer fill interrupt
        LL_DMA_DisableIT_TC(DMA2, LL_DMA_CHANNEL_3);
        LL_DMA_DisableIT_HT(DMA2, LL_DMA_CHANNEL_3);
    }
    
}

//
//! \brief Return if we currently are playing an alarm sound
//! \return false = not playing alarm, true = playing alarm sound
//
bool DacDma::IsPlayingAlarmSound(void)
{
    return (m_soundState != eSPCplayProbeSound);
}

//
//! \brief Set sound volume to full or off
//! \param a_vol Sound volume (off or full)
//
void DacDma::SetSoundVol(SoundVolume_t a_vol)
{
    m_soundVolume = a_vol;
    if(a_vol == eSVoff)
    {
        HwAbsl::getpHw()->audioOff();
    }
    else
    {
        HwAbsl::getpHw()->audioOn();
    }
}

//
//! \brief Transfer Complete interrupt for DMA. Will fill the free buffer
//
void DacDma::DMA2_Channel3_TC_ISR()
{
    static uint32_t b1cnt=0;
    static uint32_t b2cnt=0;
    uint16_t *pBufActive;
    uint16_t *pBufToFill;
    
    if(LL_DMA_GetCurrentTargetMem(DMA2, LL_DMA_CHANNEL_3) == LL_DMA_CURRENTTARGETMEM0)
    //if(m_soundBufferActive == 1)
    {
        b1cnt++;
        pBufActive = m_pSoundBuffer1;
        pBufToFill = m_pSoundBuffer2;
    }
    else
    {
        b2cnt++;
        pBufActive = m_pSoundBuffer2;
        pBufToFill = m_pSoundBuffer1;
    }
    
    if(m_soundState == eSPCinit)
    {
        if(HwAbsl::getpHw()->getProbeSerial()->GetSoundDataFill() >= 50 )
        {
            //Fill buffer with probe samples
            HwAbsl::getpHw()->getProbeSerial()->GetSoundData(pBufToFill, SOUND_BUF_SIZE);
            m_soundState = eSPCplayProbeSound;
        }
        else
        {
            //Silence
            uint16_t *pBuf = pBufToFill;
            for(uint16_t i = 0; i < SOUND_BUF_SIZE; i++)
            {
                *pBuf++ = 2048;
            }
        }
            
    }
    else if(m_soundState == eSPCplayProbeSound)
    {
        //Fill buffer with probe samples
        HwAbsl::getpHw()->getProbeSerial()->GetSoundData(pBufToFill, SOUND_BUF_SIZE);
    }
    else if(m_soundState == eSPCwaitFinishProbeBuffer)
    {
        HwAbsl::getpHw()->audioOn();
        
        //Playing last probe sound buffer soon finished. 
        //Ready zeroing buffer. This is necressary to avoid snap in sound when
        //DAC is going from last sample of probe sound to first alarm sound sample
        uint16_t smp;
        uint16_t *pbuf = pBufToFill;
        //Ramp up or down from last probe sound sample until we reach baseline
        smp = *(pBufActive+SOUND_BUF_SIZE-1);
        for(uint16_t i = 0; i < SOUND_BUF_SIZE; i++)
        {
            if(smp < 2048) {smp++;}
            else if(smp > 2048) {smp--;}
            else {smp = 2048;}
            
            *pbuf = smp;
            pbuf++;
        }
        
        m_soundState = eSPCzeroProbeSound;
    }
    else if(m_soundState == eSPCzeroProbeSound)
    {
        //Finished zeroing probe sound. Begin fill buffer with alarm sound
        uint16_t *pBuf = pBufToFill;
        for(uint16_t i = 0; i < SOUND_BUF_SIZE; i++)
        {
            if(m_alarmSound >= m_alarmSoundEnd && m_loopAlarmSound)
            {
                m_alarmSound = m_alarmSoundStart;
                *pBuf++ = *m_alarmSound++;
            }
            else if(m_alarmSound >= m_alarmSoundEnd)
            {
                *pBuf++ = 2048;
            }
            else
            {
                *pBuf++ = *m_alarmSound++;
            }
        }
        
        if(m_alarmSound >= m_alarmSoundEnd && !m_loopAlarmSound)
        {
            m_soundState = eSPCplayAlarmSound;
        }
        
    }
    else if(m_soundState == eSPCplayAlarmSound)
    {
        //Finished playing alarm sound.
        //Now we must ramp up or dowm until we are aligned to first probe sample
        uint16_t smp = 2048;
        uint16_t *pBuf = pBufToFill;
        
        if(HwAbsl::getpHw()->getProbeSerial()->GetSoundDataFill() >= 50)
        {
            smp = HwAbsl::getpHw()->getProbeSerial()->PeekSoundData();
        }
            
        for(uint16_t i = 0; i < SOUND_BUF_SIZE; i++)
        {
            if(smp < 2048) {smp++;}
            else if(smp > 2048) {smp--;}
            else {smp = 2048;}
            
            *pBuf = smp;
            pBuf++;
        }
        
        m_soundState = eSPCzeroAlarmSound;
        
        
    }
    else if(m_soundState == eSPCzeroAlarmSound)
    {
        
        if(HwAbsl::getpHw()->getProbeSerial()->GetSoundDataFill() >= 50)
        {
            //Finished aligning. Fill buffer with probe samples and start sampling from probe
            HwAbsl::getpHw()->getProbeSerial()->GetSoundData(pBufToFill, SOUND_BUF_SIZE);
            m_soundState = eSPCplayProbeSound;
        }
        else
        {
            //silence
            uint16_t *pBuf = pBufToFill;
            for(uint16_t i = 0; i < SOUND_BUF_SIZE; i++)
            {
                *pBuf++ = 2048;
            }
            m_soundState = eSPCinit;
        }
        
        //Set back DAC timer to what it was
        setDacTimerPeriod(m_timerPeriod);
        
        //Start probe sound sampling (if on before alarm interrupt)
        HwAbsl::getpHw()->getProbeSerial()->ProbeSoundEnable(m_probeSoundEnabled);
        
        if(m_soundVolume == eSVoff)
        {
            HwAbsl::getpHw()->audioOff();
        }
        
        
        
    }
    
    
}

//
//! \brief Transfer interrupt for DMA.
//
void DacDma::DMA_ISRx(void)
{
    if (LL_DMA_IsActiveFlag_HT3(DMA2))
    {
        /* Clear DMA Half Transfer interrupt pending bit */
        LL_DMA_ClearFlag_HT3(DMA2);
        
        //DMA2_Channel3_HT_ISR();
        
    }
    
    if (LL_DMA_IsActiveFlag_TC3(DMA2))
    {
        /* Clear DMA Full Transfer interrupt pending bit */
        LL_DMA_ClearFlag_TC3(DMA2);
        
        //DMA2_Channel3_FT_ISR();
        DMA2_Channel3_TC_ISR();
        
    }
    
}

//
//! \brief Full transfer interrupt for DMA (DAC). Swap the used buffer with the free
//
/*
void DacDma::DMA2_Channel3_FT_ISR()
{
    //Swap buffers
    if(LL_DMA_GetCurrentTargetMem(DMA2, LL_DMA_CHANNEL_3) == LL_DMA_CURRENTTARGETMEM0)
    //if(m_soundBufferActive == 1)
    {
        //LL_DMA_SetCurrentTargetMem(DMA2, LL_DMA_CHANNEL_3, LL_DMA_CURRENTTARGETMEM1);
        //m_soundBufferActive = 2;
    }
    else
    {
        //LL_DMA_SetCurrentTargetMem(DMA2, LL_DMA_CHANNEL_3, LL_DMA_CURRENTTARGETMEM0);
        //m_soundBufferActive = 1;
    }
    
}
*/

/**
* @brief DAC MSP Initialization
* This function configures the hardware resources used in this example
* @param hdac: DAC handle pointer
* @retval None
*/
#if(0)
void DacDma::DAC_MspInit()
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(m_hdac.Instance==DAC1)
  {
  /* USER CODE BEGIN DAC1_MspInit 0 */

  /* USER CODE END DAC1_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_DAC1_CLK_ENABLE();
  
    __HAL_RCC_GPIOA_CLK_ENABLE();
    //DAC1 GPIO Configuration    
    GPIO_InitStruct.Pin = Audio_P_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(Audio_P_GPIO_Port, &GPIO_InitStruct);

    /* DAC1 DMA Init */
    /* DAC1_CH1 Init */
    m_hdma_dac.Instance = DMA2_Channel3;
    m_hdma_dac.Init.Request = DMA_REQUEST_DAC1_CH1;
    m_hdma_dac.Init.Direction = DMA_MEMORY_TO_PERIPH;
    m_hdma_dac.Init.PeriphInc = DMA_PINC_DISABLE;
    m_hdma_dac.Init.MemInc = DMA_MINC_ENABLE;
    m_hdma_dac.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
    m_hdma_dac.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
    m_hdma_dac.Init.Mode = DMA_NORMAL;
    m_hdma_dac.Init.Priority = DMA_PRIORITY_HIGH;
    if (HAL_DMA_Init(&m_hdma_dac) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }

    if (HAL_DMA_ConfigChannelAttributes(&m_hdma_dac, DMA_CHANNEL_NPRIV) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }

    __HAL_LINKDMA(&m_hdac, DMA_Handle1, m_hdma_dac);

  }

}
#endif

/**
* @brief DAC MSP Initialization
* This function is called at DAC init (HAL driver) and configures the hardware resources used in this application
* @param hdac: DAC handle pointer
* @retval None
*/
/*
extern "C" void HAL_DAC_MspInit(DAC_HandleTypeDef* hdac)
{
    DacDma::get()->DAC_MspInit();
}
*/
        
/**
  * @brief This function handles DMA2 channel3 global interrupt.
  */
extern "C" void DMA2_Channel3_IRQHandler(void)
{
    DacDma::get()->DMA_ISRx();
}
