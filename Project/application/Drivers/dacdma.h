//! \file    Dac.h
//! \author  Geir Inge Tellnes / Jan A. R�yneberg
//! \version 1.0
//! \date    14.10.2013

#ifndef _DAC1_H
#define _DAC1_H

// header includes
#include <cstdint>
#include "includes.h"
#include "fhrmprobeserial.h"
#include "commondefs.h"
#include "stm32l5xx_hal.h"
#include "stm32l5xx_ll_dac.h"
#include "stm32l5xx_ll_tim.h"

// forward declarations
extern "C" { void DMA2_Channel3_IRQHandler(void); }
////

//! \brief The DacDma class wraps the DAC on the STM32 family\n
//! For STM32F devices: Uses DMA2 channels 3 and 4 when generating waveforms on the 2 available DAC channels\n
//! For STM32L devices: Uses DMA1 channels 2 and 3 when generating waveforms on the 2 available DAC channels
class DacDma
{
public:

    enum SoundPlayCtrl_t {eSPCstopped, eSPCinit, eSPCplayProbeSound, eSPCwaitFinishProbeBuffer, eSPCzeroProbeSound,
                          eSPCplayAlarmSound, eSPCzeroAlarmSound, eSPCzeroAlarmSoundFinished};
    
    enum MuteCtrl_t {eACnoMute, eACwaitFinishProbeBuffer, eACmuteProbeSound, eACprobeSoundMuted, 
                     eWaitAlarmSound, eWaitAlarmSoundFinished, eACunmute, eACunmuted};

    // constructor/destructor
    explicit DacDma();
    virtual ~DacDma();

    bool startSound(uint32_t sampleFreq);
    void setDacTimerPeriod(uint16_t periode);
    uint16_t getDacTimerPeriod(void);
    uint32_t calcDacTimerPeriodFromSampleFreq(uint32_t sampleFreq);
    
    //! \brief Returns the DAC's range, e.g. 4096 steps for a 12 bit DAC (2�^12)
    uint32_t getRange() const { return DAC_RANGE; }

    bool AlarmSoundInterrupt(const uint16_t * pData, uint32_t numsamples, bool loop);
    void StopWaveformPlayback(void);
    void ProbeSoundEnable(bool a_on);
    void SoftMuteProbe(void);
    bool GetProbeMuted(void);
    void SetSoundVol(SoundVolume_t a_vol);
    
    bool PlayWaveform(const uint16_t * pData, uint32_t numsamples, uint16_t repeatCount, bool interruptCurrentSound);
        
    //! \brief This DACs is internal, i.e. always connected
    bool isConnected() const { return true; }
    
    bool IsPlayingAlarmSound(void);

    static const uint16_t MAX_NUM_DAC_CHANNELS = 2;
    
    static const uint32_t DAC_CHANNEL = DAC_CHANNEL_1;
    static const uint16_t ALARM_SAMPLE_CNT = 18000;

protected:
    enum DacMode_t {DACMODE_INIT, DACMODE_WRITE, DACMODE_PLAYBACK};

private:
    DacDma(const DacDma& right);            //!< block the copy constructor
    DacDma& operator=(const DacDma& right); //!< block the assignment operator

    void DMA2_Channel3_TC_ISR();
    //void DMA2_Channel3_FT_ISR();
    //void DMA1_Stream1_FT_ISR();
    //void DMA1_Stream4_FT_ISR();
    void DMA_ISRx();
    //void DAC_MspInit();
    
    //friend void HAL_DAC_MspInit(DAC_HandleTypeDef* hdac);
    friend void DMA2_Channel3_IRQHandler(void);
    
    //TIM_HandleTypeDef m_playbackTimer;

    
    DacMode_t m_mode;
    
    static const uint32_t DAC_CONFIG_WORD = 0x2597; //TODO

    static DacDma* m_pDac;  //!< static member to keep track of instance. Needed to prevent multiple instances
    static DacDma* get() { return m_pDac; }
    
    //DAC_HandleTypeDef m_hdac;
    //DMA_HandleTypeDef m_hdma_dac;
    
    static const uint32_t DAC_RANGE = 4096;                   //!< 12 bits DAC range
    
    static const uint16_t SOUND_BUF_SIZE = 256;
    uint16_t *m_pSoundBuffer1;
    uint16_t *m_pSoundBuffer2;
    //uint8_t m_soundBufferActive;
    uint16_t m_noAvailableSamples;
    uint16_t m_timerPeriod;
    bool m_probeSoundEnabled;
    SoundPlayCtrl_t m_soundState;
    uint16_t *m_alarmSound;
    uint16_t *m_alarmSoundStart;
    uint16_t *m_alarmSoundEnd;
    bool m_loopAlarmSound;
    
    SoundVolume_t m_soundVolume;
    
    

};


#endif   // _DAC_H

//======== End of Dac.h ==========

