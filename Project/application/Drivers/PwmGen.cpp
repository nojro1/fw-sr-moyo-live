//! \file    PwmGen.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    15.04.2020


// include self first
#include "pwmgen.h"

#include "RTOS.h"
#include <bitset>
#include <algorithm>

// static member init
PwmGen*  PwmGen::m_pPwmGen = 0;


//! \brief Constructor
//! \param freq frequency for PWM (default 32000)
//! \param initial_duty Initial duty-cycle in %*10 (default 500)
//! \remark The minimum freq_x_x is 1 Hz
PwmGen::PwmGen(uint32_t freq, uint16_t initial_duty)
{
    //OS_EnterRegion();
    if (0 == m_pPwmGen)   // single instance only
    {
        m_pPwmGen = this;
    }
    else
    {
        //OS_LeaveRegion();
    }
    //OS_LeaveRegion();

    //TODO: Add definitions of timers in config file
    
    
    // Time Base configuration
    
    TIM_MasterConfigTypeDef sMasterConfig = {0};
    TIM_OC_InitTypeDef sConfigOC = {0};

  
    m_gpio_pwm_timer.Instance = TIM4;
    
    // Compute the value to be set in ARR register to generate signal frequency at XX Khz
    uint32_t timerPeriod = 0;
    uint16_t prescaler = 0;
    calculateTimerPeriodAndPrescaler(freq, timerPeriod, prescaler, m_gpio_pwm_timer.Instance);
    
    m_gpio_pwm_timer.Init.Prescaler = prescaler;
    m_gpio_pwm_timer.Init.CounterMode = TIM_COUNTERMODE_UP;
    m_gpio_pwm_timer.Init.Period = timerPeriod;
    m_gpio_pwm_timer.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    m_gpio_pwm_timer.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_PWM_Init(&m_gpio_pwm_timer) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&m_gpio_pwm_timer, &sMasterConfig) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }
    
    // CCRx values to initially generate a duty cycle at 'initial_dc'
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    if (initial_duty < DC_100_PERC)
    {
        sConfigOC.Pulse =  static_cast<uint16_t>( (static_cast<uint32_t>(initial_duty) * m_gpio_pwm_timer.Instance->ARR) / DC_100_PERC );
    }
    else
    {
        sConfigOC.Pulse = timerPeriod + 1; // ensure full on, if 100% dc
    }
            
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    if (HAL_TIM_PWM_ConfigChannel(&m_gpio_pwm_timer, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }
    if (HAL_TIM_PWM_ConfigChannel(&m_gpio_pwm_timer, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }
    if (HAL_TIM_PWM_ConfigChannel(&m_gpio_pwm_timer, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }
    if (HAL_TIM_PWM_ConfigChannel(&m_gpio_pwm_timer, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }
    
    //Start PWM
    if (HAL_TIM_PWM_Start(&m_gpio_pwm_timer, TIM_CHANNEL_1) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }
    if (HAL_TIM_PWM_Start(&m_gpio_pwm_timer, TIM_CHANNEL_2) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }
    if (HAL_TIM_PWM_Start(&m_gpio_pwm_timer, TIM_CHANNEL_3) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }
    if (HAL_TIM_PWM_Start(&m_gpio_pwm_timer, TIM_CHANNEL_4) != HAL_OK)
    {
        //Error_Handler(); //TODO
    }

}

//! destructor
PwmGen::~PwmGen()
{
    //OS_EnterRegion();
    
    //TODO

    //OS_LeaveRegion();
}

//! \brief Calculates the timer period needed for the requested PWM frequency and\n
//! calculates the prescaler value needed to avoid timer overflow
//! \param pwmFreq Requested PWM frequency
//! \param [out] timerPeriod Reference to the calculated timer period
//! \param [out] prescaler Reference to the calculated prescaler
void PwmGen::calculateTimerPeriodAndPrescaler(uint32_t pwmFreq, uint32_t& timerPeriod, uint16_t& prescaler, TIM_TypeDef* TIMx)
{
    uint32_t frequency = HAL_RCC_GetPCLK1Freq();
    const uint32_t TIMxCLK = frequency*((RCC->CFGR & RCC_CFGR_PPRE1) == RCC_CFGR_PPRE1_DIV1 ? 1 : 2); // the driver clock for the timer
    

    uint32_t max_count = TIMxCLK / std::max(pwmFreq, 1u) - 1;
    if (max_count > 0xFFFF)
    {
        prescaler = max_count/0xFFFF;
    }
    else
    {
        prescaler = 0;
    }
    timerPeriod = (TIMxCLK / (std::max(pwmFreq, 1u)  * (prescaler + 1))) - 1;
}

//! \brief Set the duty cycle in %*10 or %*100 (depending on CFG_PWM_RESOLUTION_DECIMALS)
//! \param ch The channel to set the new duty cycle on\n
//! \param duty The duty cycle in %*10 or %*100 (depending on CFG_PWM_RESOLUTION_DECIMALS)
void PwmGen::setDuty(uint8_t ch, uint16_t duty) const
{
    assert_param(ch < MAX_NUM_PWM_CHANNELS);

    uint16_t dcVal;
    if (duty < DC_100_PERC)
    {
        dcVal = static_cast<uint16_t>( (static_cast<uint32_t>(duty) * m_gpio_pwm_timer.Instance->ARR) / DC_100_PERC );
    }
    else
    {
        dcVal = m_gpio_pwm_timer.Instance->ARR+1; // ensure full on, if 100%
    }

    switch (ch)
    {
        case 0:
            m_gpio_pwm_timer.Instance->CCR1 = dcVal;
            break;
        case 1:
            m_gpio_pwm_timer.Instance->CCR2 = dcVal;
            break;
        case 2:
            m_gpio_pwm_timer.Instance->CCR3 = dcVal;
            break;
        case 3:
            m_gpio_pwm_timer.Instance->CCR4 = dcVal;
            break;
        default:
            break;
    }
}

//! \brief Get current duty cycle in %*10 or %*100 (depending on CFG_PWM_RESOLUTION_DECIMALS)
//! \param ch The channel to get the duty cycle from\n
//! \return The duty cycle in %*10 or %*100 (depending on CFG_PWM_RESOLUTION_DECIMALS)
uint16_t PwmGen::getDuty(uint8_t ch) const
{
    assert_param(ch < MAX_NUM_PWM_CHANNELS);

    uint32_t dcVal = 0;

    switch (ch)
    {
        case 0:
            dcVal = m_gpio_pwm_timer.Instance->CCR1;
            break;
        case 1:
            dcVal = m_gpio_pwm_timer.Instance->CCR2;
            break;
        case 2:
            dcVal = m_gpio_pwm_timer.Instance->CCR3;
            break;
        case 3:
            dcVal = m_gpio_pwm_timer.Instance->CCR4;
            break;
        default:
            break;
    }

    return static_cast<uint16_t>( (dcVal * DC_100_PERC) / m_gpio_pwm_timer.Instance->ARR );
}

/**
* @brief TIM_PWM MSP Initialization
* This function configures the hardware resources used in this example
* @param htim_pwm: TIM_PWM handle pointer
* @retval None
*/
void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm)
{
    if(htim_pwm->Instance==TIM4) //TODO: USE m_gpio_pwm_timer
    {
        /* Peripheral clock enable */
        __HAL_RCC_TIM4_CLK_ENABLE();
    }
}

void HAL_TIM_MspPostInit(TIM_HandleTypeDef* htim)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    if(htim->Instance==TIM4) //TODO: USE m_gpio_pwm_timer
    {
        __HAL_RCC_GPIOD_CLK_ENABLE();
        __HAL_RCC_GPIOB_CLK_ENABLE();
        /**TIM4 GPIO Configuration    
        PD12     ------> TIM4_CH1
        PD13     ------> TIM4_CH2
        PB8     ------> TIM4_CH3
        PB9     ------> TIM4_CH4 
        */
        GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;
        HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

        GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    }

}