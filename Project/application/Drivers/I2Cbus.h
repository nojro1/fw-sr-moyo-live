//! \file    I2Cbus.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    24.05.2011

#ifndef _I2C_BUS_H
#define _I2C_BUS_H

// header includes
#include <cstdint>
#include "stm32l5xx_hal.h"
#include "dwt_stm32_delay.h"
#include "rtos.h"
#include <Mutex.h>
#include <vector>
#include <board_cfg.h>
#include <CircularBuffer.h>

#ifdef NOT_USING_I2C_MASTER
#ifndef USE_I2C_SLAVE
  #error "I2Cbus - Either Master and/or Slave functionality must be defined"
#endif
#endif

// forward declarations
class Mutex;
class Signal;
////

//! \brief The I2Cbus class wraps the STM32 I2C bus in master and slave modes\n
//! Supports I2C1 to 2, depending on configuration in I2CCfg.h\n
//! Interrupt driven implementation based on AN2824 "STM32F10xxx devices: advanced I2C examples"
//! \remark Slave functionality is enabled with compiler directive USE_I2C_SLAVE
class I2Cbus
{
public:

    //! \enum I2C_Portnum_t
    //! \brief Available I2C ports
    enum I2C_Portnum_t {I2CPORT_1, I2CPORT_MAX = I2CPORT_1};

    static const uint8_t NUM_I2C_PORTS = I2CPORT_MAX+1;

    typedef uint8_t I2C_Address_t; //!< Issued device id for registered devices(chips) on the I2C bus

    // constructor/destructor
    explicit I2Cbus(I2C_Portnum_t portNum, uint32_t bitRate, uint8_t ownAddress);

    virtual ~I2Cbus();

    bool receiveFrame(I2C_Address_t i2cAddr, uint8_t *dstBuf, uint8_t bufSize);
    bool receiveFrame(I2C_Address_t i2cAddr, uint8_t deviceInternalAddress, uint8_t *dstBuf, uint8_t bufSize);
    bool receiveFrame(I2C_Address_t i2cAddr, uint16_t deviceInternalAddress, uint8_t *dstBuf, uint8_t bufSize);
    bool sendFrame(I2C_Address_t i2cAddr, const uint8_t *srcBuf, uint8_t bufSize);
    bool sendFrame(I2C_Address_t i2cAddr, uint8_t deviceInternalAddress, const uint8_t *srcBuf, uint8_t bufSize);
    bool sendFrame(I2C_Address_t i2cAddr, uint16_t deviceInternalAddress, const uint8_t *srcBuf, uint8_t bufSize);


    void reset();

protected:

    void cfg_gpio_as_i2c();
    void cfg_gpio_as_gpio();

    int32_t getBusTimeMs();

    void select();
    void deselect();

    void isr_I2Cx();
    void isr_err_I2Cx();

    static void isr_I2C1();
    static void isr_err_I2C1();

    static void isr_I2C2();
    static void isr_err_I2C2();

    friend void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c);
    
    friend void I2C1_EV_IRQHandler(void);
    friend void I2C1_ER_IRQHandler(void);
    
private:
    I2Cbus(const I2Cbus& right);            //!< block the copy constructor
    I2Cbus& operator=(const I2Cbus& right); //!< block the assignment operator

    void softwareReset();

    I2C_HandleTypeDef   m_i2c;             //!< Pointer to the I2C bus peripheral itself
    I2C_Portnum_t       m_portNum;         //!< This instance's port number
    
    Mutex * m_pMutex;  //!< Mutex for protecting the I2C bus in multithread systems

    static I2Cbus* m_pI2Cobj;           //!< static member to keep track of instances. Needed by isr.
    static I2Cbus* get() { return m_pI2Cobj; }

    I2C_Address_t m_slaveAddr;      //! Current slave address in use, used by isr

    volatile uint32_t m_I2CtimeoutNAK; //! retry timeout after xxx ACK failures
    uint16_t m_i2c_max_err_cnt;        //! for setting max I2C error counts before timed-out

    // used by i2c interrupts for master operation
    volatile uint8_t m_TxBufIdx;    //! Index to next byte to send, used by isr
    volatile uint8_t m_RxBufIdx;    //! Index to next byte to receive, used by isr
    volatile uint8_t m_TxNumBytes;  //! # of remaining bytes in Tx buffer, used by isr
    volatile uint8_t m_RxNumBytes;  //! # of remaining bytes to be received into Rx buffer, used by isr

    #ifndef CFG_I2C_TX_BUF_SIZE     // from board_cfg.h, if existing
        #define I2C_TX_BUF_SIZE 16
    #else
        #define I2C_TX_BUF_SIZE (CFG_I2C_TX_BUF_SIZE)
    #endif
    uint8_t m_TxBuf[I2C_TX_BUF_SIZE];  //! Tx buffer used by isr
    uint8_t * m_RxBuf;                 //! Pointer to Rx buffer used by isr

    bool m_isMasterReceiver;           //! if true, the current I2C mode is Master Receiver, else Master Transmitter
    bool m_receiverFollowsTransmitter; //! if true, the next I2C mode is Master Receiver
    bool m_busResetFromIsr;            //! To signal that isr error handler had to reset the bus

    // used by i2c interrupts for slave operation
    volatile uint8_t m_SlaveRxCount;        //!< Use by isr to detect the first received byte (device sub-address) to select the active Tx and Rx buffers
    volatile uint8_t m_SlaveTxNumBytes;     //!< # of remaining bytes in active Tx buffer, used by isr
    volatile uint8_t m_SlaveRxNumBytes;     //!< # of remaining bytes to be received into active Rx buffer, used by isr

    //! \enum SlaveBufferType_t
    //! \brief
    //! SLAVE_BUF_FIXED for fixed data (typical device values). Data always available\n
    //! SLAVE_BUF_MESSAGE for message type data. Data availability not guaranteed
    enum SlaveBufferType_t {SLAVE_BUF_FIXED, SLAVE_BUF_MESSAGE};

    struct I2C_Slave_Msg_t
    {
        uint8_t                 m_msgSize;      //!< Size of each I2C message. Fixed
        SlaveBufferType_t       m_bufType;      //!< What kind of buffer this is. See SlaveBufferType_t
        CircularBuffer<uint8_t> m_txBuf;        //!< The Tx buffer
        CircularBuffer<uint8_t> m_rxBuf;        //!< The Rx buffer

        bool receiveEnabled() {return m_rxBuf.getMaximumSize() > 0;}
    };

    I2C_Slave_Msg_t * m_pCurrentSlaveBuf;     //!< Pointer to active Tx/Rx buffer, used by isr
    std::vector<I2C_Slave_Msg_t*> m_pSlaveBuffers; //!< Collection of slave buffers
  
    
};


#endif   // _I2C_BUS_H

//======== End of I2Cbus.h ==========



