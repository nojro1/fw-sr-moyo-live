//! \file    pwmgen.h
//! \author  aed / git
//! \version 1.0
//! \date    26.11.2010

#ifndef _PWMGEN_H
#define _PWMGEN_H

// header includes
#include <cstdint>
#include "board_cfg.h"
#include "stm32l5xx_hal.h"

#include "IPwmGen.h"

// forward declarations

////


//! \brief Configuration parameters for one GPIO PWM channel
//! The GPIO PWM will install an interrupt handler on the first available timer
//! that create a PWM signal by bit banging the GIO pin.
struct gpio_pwm_channel_cfg
{
    // gpio config, must be set before calling PwmGen constructor
    uint16_t m_max_count;
    uint16_t m_initial_duty;  //!< range [DC_0_PERC, DC_100_PERC]
    // pwm runtime variables, only set by PwmGen class
    // Note that m_duty must be accessed by atomic operations. As long as the
    // variable is volatile and the struct is not packed (i.e. variable is aligned)
    // the IAR compiler will assure this. See paragraphs "Alignment" and
    // "Type qualifiers" in chapter "Data representation" in
    // "IAR C/C++ Development Guide".
    volatile uint16_t m_duty; //!< range [0, m_max_count + 1]
    uint16_t m_counter;
};

//TODO: Rewrite
//! \brief The PWM generator
//! Uses one or more of TIM1/TIM2/TIM3/TIM4, depending on number of enabled channels,\n
//! as defined by 'channelMask' in constructor\n
//! The PWM channels mapping can be found in TimerPeriphCfg.h\n
//! The PWM frequency (1/period) is configurable for each channel block of 4\n
//! (20 channels = 4 channel blocks = 4 timers, first 8 channels shares TIM1)
//!
//! Bit banged PWM (or PDM) can be genrated by an interrupt handler on a list of
//! GPIO pins specified by \a a_gpio_pwm_cfg. The GPIO PWM channels will use
//! channel number from #GPIO_PWM_BASE_CHANNEL. At least one hardware PWM
//! channel must be enabled for GPIO PWM to work and the interrupt priority for
//! that timer must be set by defining at least one of the following defines
//!   ISR_PRIO_TIM1_UP_IRQn
//!   ISR_PRIO_TIM2_IRQn
//!   ISR_PRIO_TIM3_IRQn
//!   ISR_PRIO_TIM4_IRQn
//! in the file 'isrpriocfg.h'. Failing to do so will produce no compile errors.
//! Example configuration:
//! \code[.cpp]
//! gpio_pwm_channel_cfg gpio_pwm_cfg[1];
//! PwmGen * pPwmGen;
//! gpio_pwm_cfg[0].m_Port = GPIOA;
//! gpio_pwm_cfg[0].m_PortInitStruct.GPIO_Pin = GPIO_Pin_3;
//! gpio_pwm_cfg[0].m_PortInitStruct.GPIO_Speed = GPIO_Speed_2MHz;
//! gpio_pwm_cfg[0].m_PortInitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
//! gpio_pwm_cfg[0].m_mode = GPIO_PWM_MODE_PWM;
//! gpio_pwm_cfg[0].m_max_count = 50;
//! gpio_pwm_cfg[0].m_initial_duty = DC_100_PERC/2;
//! pPwmGen = new PwmGen(0x00030069, 32000, 32000, 32000, 32000, DC_100_PERC/2,
//!                      gpio_pwm_cfg, 1);
//! \endcode

class PwmGen
{
public:

    // constructor/destructor
    PwmGen(uint32_t freq, uint16_t initial_duty);

    ~PwmGen();

    void     setDuty(uint8_t ch, uint16_t duty) const;
    uint16_t getDuty(uint8_t ch) const;

    static const uint8_t MAX_NUM_PWM_CHANNELS = 4;

protected:

    void calculateTimerPeriodAndPrescaler(uint32_t pwmFreq, uint32_t& timerPeriod, uint16_t& prescaler, TIM_TypeDef* TIMx);
    friend void HAL_TIM_PWM_MspInit(TIM_HandleTypeDef* htim_pwm);
    friend void HAL_TIM_MspPostInit(TIM_HandleTypeDef* htim);

private:
    PwmGen(const PwmGen& right);            //!< block the copy constructor
    PwmGen& operator=(const PwmGen& right); //!< block the assignment operator

    static PwmGen* get() { return m_pPwmGen; }
    static PwmGen* m_pPwmGen;

    TIM_HandleTypeDef m_gpio_pwm_timer;

};


#endif   // _PWMGEN_H

