//! \file    VirtualComPort.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    25.08.2010

#ifndef _VIRTUAL_COM_PORT_H
#define _VIRTUAL_COM_PORT_H

// header includes
#include <board_cfg.h>
#include "RTOS.h"
#include <cstdint>
#include <circularbuffer.h>
#include <ISerial.h>

// forward declarations

////

//! \brief The VirtualComPort wraps the USB peripheral as a COM port on the STM32 family\n
//! Compiler directives: \n
//! 1. VCOM_NO_SERNUM : Do not use unique CPU serial number.\n
//! This makes all devices connected to the same USB connector get the same COM port number\n
//! 2. VCOM_ID_VID_LAERDAL and VCOM_ID_PID_LA_VCOM / VCOM_ID_PID_RB_VCOM / VCOM_ID_PID_RA_VCOM / VCOM_ID_PID_MOYO_VCOM:\n
//! Use Laerdal USB vendor ID (VID) and USB product id (PID).\n
//! CAVEAT: Driver needs to be re-certified at Microsoft WHQL to work on Winddows 64-bit\n
//! Also see isrpriocfg.h for setting interrupt priorities.\n

class VirtualComPort : public ISerial
{
public:
    // constructor/destructor
    explicit VirtualComPort(uint16_t bufsizeRead, uint16_t bufsizeWrite);

    virtual ~VirtualComPort();

    int write(uint8_t _data);
    int write(const uint8_t* _pData, uint16_t _len);
    int read();
    int read(uint8_t * _pData, uint16_t _len);
    int read(int16_t timeout);
    int read(uint8_t * _pData, uint16_t _len, int16_t timeout);

    void flush(const int& timeout=0);

    bool hasTxBufferSpaceAvailable() const;
    int  getTxBufferSpaceAvailable() const;
    int  getTxBufferWatermark();
    int  getTxBufferSize() const;

    int  getRxBufferWatermark();
    int  getRxBufferSize() const;

    bool init() { return true; }

    bool deinit();

    void disable();
    void enable();
    void reset();

    bool hasUSBHostConnection() const;
    bool isUSBHostConnectionOK() const;

    // time to wait until USB device is expected to be configured
    // 1 second seems to be enough, but add extra 1 sec to be sure of how the device is powered
    static const int16_t STARTUP_TIME = 2000; 

protected:

private:
    VirtualComPort(const VirtualComPort& right);            //!< block the copy constructor
    VirtualComPort& operator=(const VirtualComPort& right); //!< block the assignment operator

    static void usb_isr();

  #ifdef USB_PWR_PIN		
	static void usb_pwr_isr();
  #endif

#if OS_USE_VARINTTABLE==0
  #ifdef STM32F10X_CL
    friend void OTG_FS_IRQHandler(void);
  #else
    friend void USB_LP_CAN1_RX0_IRQHandler(void);
  #endif // STM32F10X_CL

  #ifdef USB_PWR_PIN        
    friend void EXTI0_IRQHandler(void);
    friend void EXTI1_IRQHandler(void);
    friend void EXTI2_IRQHandler(void);
    friend void EXTI3_IRQHandler(void);
    friend void EXTI4_IRQHandler(void);
    friend void EXTI9_5_IRQHandler(void);
    friend void EXTI15_10_IRQHandler(void);
  #endif  
#endif // #if OS_USE_VARINTTABLE==0

    friend void Handle_USBAsynchXfer (void);
    friend void EP1_IN_Callback (void);
    friend void EP3_OUT_Callback(void);

    static VirtualComPort* get() { return m_pPort; }

    static VirtualComPort* m_pPort;   //!< static member to keep track of port. Needed by isr.

    CircularBuffer<uint8_t> m_outBuffer;    //!< output ringbuffer
    CircularBuffer<uint8_t> m_inBuffer;     //!< input ringbuffer

    bool m_enabled;

};


#endif   // _VIRTUAL_COM_PORT_H

//======== End of VirtualComPort.h ==========

