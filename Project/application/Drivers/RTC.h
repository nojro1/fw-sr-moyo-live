//! \file    RTC.h
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    05.07.2012

#ifndef _RTC_H
#define _RTC_H

// header includes
//#include <cstdint>
#include "stm32l5xx.h"
#include "stm32l5xx_ll_bus.h"
#include "stm32l5xx_ll_rcc.h"
#include "stm32L5xx_ll_rtc.h"
#include "integer.h"

// forward declarations
extern "C" { DWORD get_fattime(void); }
////

//! \brief The Rtc class wraps the RTC on the STM32 family and format date and time into structs\n

class Rtc
{
public:

    // constructor/destructor
    explicit Rtc();
    virtual ~Rtc();

    
    //! \brief This RTC is internal, i.e. always connected
    bool isConnected() const { return true; }
    
    void RTCsetDateTime(LL_RTC_DateTypeDef a_date, LL_RTC_TimeTypeDef a_time);
    void RTCsetDate(LL_RTC_DateTypeDef a_date);
    void RTCsetTime(LL_RTC_TimeTypeDef a_time);
    LL_RTC_DateTypeDef RTCgetDate();
    LL_RTC_TimeTypeDef RTCgetTime();
    bool RTCgetError(void);
    
    
protected:

private:
    Rtc(const Rtc& right);            //!< block the copy constructor
    Rtc& operator=(const Rtc& right); //!< block the assignment operator

    void init();
    void DateUpdate(void);
    void TimeUpdate(void);
    uint8_t IsLeapYear(uint16_t nYear);
    void RTCConfiguration();
    void TimeAdjust();
    void DateAdjust(void);
    uint32_t GetTimeCounterValue(void);
    bool RTCwaitForLSEStartUp(uint16_t a_timeoutInMilliSec);
    uint32_t getFatTime(void);
    static void RTC_ISR();
        
    static void isr();
    friend DWORD get_fattime(void);
    friend void RTC_IRQHandler(void);
    static Rtc* get() { return m_pRtc; }

    static Rtc* m_pRtc;  //!< static member to keep track of instance. Needed by isr and to prevent multiple instances
    
    RTC_HandleTypeDef m_rtc; //!< Pointer to the RTC peripheral itself
    
    LL_RTC_DateTypeDef m_date;
    LL_RTC_TimeTypeDef m_time;
    

    bool m_error;
    
};


#endif   // _RTC_H

//======== End of Rtc.h ==========

