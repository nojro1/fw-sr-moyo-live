//! \file    SerialPort.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    31.03.2020


// include self first
#include "serialport.h"

// other includes
#include "sysdefs.h"
#include <cassert>
#include "stm32l5xx_hal.h"
#include <Mutex.h>
#include <Guard.h>

// static member init
SerialPort*  SerialPort::m_pPorts[SerialPort::NUM_COMs] = {0};

SerialPort::COM_TypeDef SerialPort::m_defaultPort(SerialPort::COM1);

const USART_TypeDef* SerialPort::COM_USART[SerialPort::NUM_COMs]   = {CFG_COM1, CFG_COM2, CFG_COM3, CFG_COM4, CFG_COM5};
const GPIO_TypeDef*  SerialPort::COM_PORT[SerialPort::NUM_COMs]    = {CFG_COM1_GPIO, CFG_COM2_GPIO, CFG_COM3_GPIO, CFG_COM4_GPIO, CFG_COM5_GPIO};
const uint32_t SerialPort::COM_USART_CLK[SerialPort::NUM_COMs]     = {CFG_COM1_CLK, CFG_COM2_CLK, CFG_COM3_CLK, CFG_COM4_CLK, CFG_COM5_CLK};
const uint32_t SerialPort::COM_USART_CLK_SRC[SerialPort::NUM_COMs] = {CFG_COM1_CLK_SRC, CFG_COM2_CLK_SRC, CFG_COM3_CLK_SRC, CFG_COM4_CLK_SRC, CFG_COM5_CLK_SRC};
const uint16_t SerialPort::COM_TX_PIN[SerialPort::NUM_COMs]        = {CFG_COM1_TxPin, CFG_COM2_TxPin, CFG_COM3_TxPin, CFG_COM4_TxPin, CFG_COM5_TxPin};
const uint16_t SerialPort::COM_RX_PIN[SerialPort::NUM_COMs]        = {CFG_COM1_RxPin, CFG_COM2_RxPin, CFG_COM3_RxPin, CFG_COM4_RxPin, CFG_COM5_RxPin};
const IRQn_Type SerialPort::COM_ISR[SerialPort::NUM_COMs]          = {USART1_IRQn, USART2_IRQn, USART3_IRQn, UART4_IRQn, UART5_IRQn};
const uint8_t  SerialPort::COM_ISR_PRIO[SerialPort::NUM_COMs]      = {ISR_PRIO_USART1_IRQn, ISR_PRIO_USART2_IRQn, ISR_PRIO_USART3_IRQn, ISR_PRIO_UART4_IRQn, ISR_PRIO_UART5_IRQn};

//! constructor
//! \brief Initialise the UART device, including GPIO remapping
//! \param portNum The COM port number, COM1, COM2...COM5
//! \param baudRate The baudrate to operate on
//! \param bufsizeRead The receive buffer size
//! \param bufsizeWrite The transmit buffer size
//! \param parity USART_Parity_No (default), USART_Parity_Even, USART_Parity_Odd
//! \param hardwareFlowControl USART_HardwareFlowControl_None (default), USART_HardwareFlowControl_RTS, USART_HardwareFlowControl_CTS, USART_HardwareFlowControl_RTS_CTS
SerialPort::SerialPort(COM_TypeDef portNum, uint32_t baudRate,
                       uint16_t bufsizeRead, uint16_t bufsizeWrite,
                       uint32_t parity, uint32_t hardwareFlowControl) : m_usart(0), m_portNum(COM1), CHAR_TRX_DELAY_US_( 165 < baudRate ? //used in delayUs, which wants uint16
                       static_cast<unsigned>(1/((baudRate / 8 / 4)*3 / 1000.0f)*1000 + 1 ) :
                       65535 )
{
    //For debug: this is a doddgy-low baudrate, likely your mistake
    assert(300 < baudRate);
    
    //OS_EnterRegion();
    
    if (0 == m_pPorts[portNum])
    {
        m_mutex = new Mutex();
        m_inBuffer.create(bufsizeRead);
        m_outBuffer.create(bufsizeWrite);

        m_portNum = portNum;
        m_pPorts[portNum] = this;
        m_usart = const_cast<USART_TypeDef *>(COM_USART[portNum]);
        
        // Enable the peripheral clock of GPIO Port and set alternate function
        switch (portNum)
        {
            case COM1:
                CFG_COM1_SET_AF_TX_PIN();
                CFG_COM1_SET_AF_RX_PIN();
                LL_AHB2_GRP1_EnableClock(CFG_COM1_GPIO_CLK);
                break;
            case COM2:
                CFG_COM2_SET_AF_TX_PIN();
                CFG_COM2_SET_AF_RX_PIN();
                LL_AHB2_GRP1_EnableClock(CFG_COM2_GPIO_CLK);
                break;
            case COM3:
                CFG_COM3_SET_AF_TX_PIN();
                CFG_COM3_SET_AF_RX_PIN();
                LL_AHB2_GRP1_EnableClock(CFG_COM3_GPIO_CLK);
                break;
            case COM4:
                CFG_COM4_SET_AF_TX_PIN();
                CFG_COM4_SET_AF_RX_PIN();
                LL_AHB2_GRP1_EnableClock(CFG_COM4_GPIO_CLK);
                break;
            case COM5:
                CFG_COM5_SET_AF_TX_PIN();
                CFG_COM5_SET_AF_RX_PIN();
                LL_AHB2_GRP1_EnableClock(CFG_COM5_GPIO_CLK);
                break;
            default:
                break;
        }
                
        
        /* Configure Tx Pin as : Alternate function (done above), High Speed, Push pull, Pull up */
        LL_GPIO_SetPinMode(const_cast<GPIO_TypeDef*>(COM_PORT[portNum]), COM_TX_PIN[portNum], LL_GPIO_MODE_ALTERNATE);
        LL_GPIO_SetPinSpeed(const_cast<GPIO_TypeDef*>(COM_PORT[portNum]), COM_TX_PIN[portNum], LL_GPIO_SPEED_FREQ_HIGH);
        LL_GPIO_SetPinOutputType(const_cast<GPIO_TypeDef*>(COM_PORT[portNum]), COM_TX_PIN[portNum], LL_GPIO_OUTPUT_PUSHPULL);
        LL_GPIO_SetPinPull(const_cast<GPIO_TypeDef*>(COM_PORT[portNum]), COM_TX_PIN[portNum], LL_GPIO_PULL_UP);

        /* Configure Rx Pin as : Alternate function (done above), High Speed, Push pull, Pull up */
        LL_GPIO_SetPinMode(const_cast<GPIO_TypeDef*>(COM_PORT[portNum]), COM_RX_PIN[portNum], LL_GPIO_MODE_ALTERNATE);
        LL_GPIO_SetPinSpeed(const_cast<GPIO_TypeDef*>(COM_PORT[portNum]), COM_RX_PIN[portNum], LL_GPIO_SPEED_FREQ_HIGH);
        LL_GPIO_SetPinOutputType(const_cast<GPIO_TypeDef*>(COM_PORT[portNum]), COM_RX_PIN[portNum], LL_GPIO_OUTPUT_PUSHPULL);
        LL_GPIO_SetPinPull(const_cast<GPIO_TypeDef*>(COM_PORT[portNum]), COM_RX_PIN[portNum], LL_GPIO_PULL_UP);

        /* NVIC Configuration for USART interrupts */
        /*  - Set priority for USARTx_IRQn */
        /*  - Enable USARTx_IRQn */
        NVIC_SetPriority(COM_ISR[portNum], COM_ISR_PRIO[portNum]);  
        NVIC_EnableIRQ(COM_ISR[portNum]);

        /* Enable USART peripheral clock and clock source ***********************/
        LL_APB1_GRP1_EnableClock(COM_USART_CLK[portNum]);

        /* Set clock source */
        LL_RCC_SetUSARTClockSource(COM_USART_CLK_SRC[portNum]);
        
        
        /* Configure USART functional parameters ********************************/
  
        /* TX/RX direction */
        LL_USART_SetTransferDirection(const_cast<USART_TypeDef*>(COM_USART[portNum]), LL_USART_DIRECTION_TX_RX);

        /* 8 data bit, 1 start bit, 1 stop bit, parity (parameter) */
        LL_USART_ConfigCharacter(const_cast<USART_TypeDef*>(COM_USART[portNum]), LL_USART_DATAWIDTH_8B, parity, LL_USART_STOPBITS_1);

        /* Hardware Flow control (parameter) */
        LL_USART_SetHWFlowCtrl(const_cast<USART_TypeDef*>(COM_USART[portNum]), hardwareFlowControl);

        
        /* In this app, Peripheral Clock is expected to be equal to 110000000 Hz => equal to SystemCoreClock */
        LL_USART_SetBaudRate(const_cast<USART_TypeDef*>(COM_USART[portNum]), SystemCoreClock, LL_USART_PRESCALER_DIV1, LL_USART_OVERSAMPLING_16, baudRate); 

        /* Enable USART *********************************************************/
        LL_USART_Enable(const_cast<USART_TypeDef*>(COM_USART[portNum]));

        /* Polling USART initialisation */
        while((!(LL_USART_IsActiveFlag_TEACK(const_cast<USART_TypeDef*>(COM_USART[portNum])))) || (!(LL_USART_IsActiveFlag_REACK(const_cast<USART_TypeDef*>(COM_USART[portNum])))))
        {
        }

        /* Enable RXNE and Error interrupts */
        LL_USART_EnableIT_RXNE(const_cast<USART_TypeDef*>(COM_USART[portNum]));
        LL_USART_EnableIT_ERROR(const_cast<USART_TypeDef*>(COM_USART[portNum]));
        
    }
    
    //OS_LeaveRegion();
}

//! destructor
SerialPort::~SerialPort()
{
    deinit();
    delete m_mutex;
}


//! \brief Read a byte from the serial port input buffer
//! \return The read byte. -1 if none available
int SerialPort::read()
{
    Guard g (*m_mutex); // reentrancy protection

    uint8_t data;
    disableRxInterrupt();
    bool success = m_inBuffer.get(data);
    enableRxInterrupt();
    return success ? data : -1;
}

//! \brief Read n bytes from the serial port input buffer
//! \param _pData [out] Pointer to destination array
//! \param _len Number of bytes to read
//! \return Number of bytes retrieved, 0 if none
int SerialPort::read(uint8_t * _pData, uint16_t _len)
{
    Guard g (*m_mutex); // reentrancy protection

    disableRxInterrupt();
    int numRead = m_inBuffer.get(_pData, _len);
    enableRxInterrupt();
    return numRead;
}

//! \brief Read a byte from the serial port input buffer and\n
//! suspends calling Thread if none available, with timeout
//! \param timeout Timeout in milliseconds (when to give up)
//! \return The read byte. -1 if none available (i.e. timeout)
int SerialPort::read(int16_t timeout)
{
    int dta;
    const int aftertimeout = OS_GetTime() + timeout;
    do
    {
        dta = read();
        if (dta < 0)
        {
            OS_Delay(SYSTICK_TIME);
        }
        else
        {
            break;
        }
    } while (OS_GetTime() < aftertimeout);

    return dta;
}

//! \brief Read n bytes from the serial port input buffer and\n
//! suspends calling Thread if none available, with timeout
//! \param _pData [out] Pointer to destination array
//! \param _len Number of bytes to read
//! \param timeout Timeout in milliseconds (when to give up)
//! \return Number of bytes retrieved, 0 if none (i.e. timeout)
int SerialPort::read(uint8_t * _pData, uint16_t _len, int16_t timeout)
{
    int numRead;
    const int aftertimeout = OS_GetTime() + timeout;
    do
    {
        numRead = read(_pData, _len);
        if (numRead == 0)
        {
            OS_Delay(SYSTICK_TIME);
        }
        else
        {
            break;
        }
    } while (OS_GetTime() < aftertimeout);

    return numRead;
}

//! \brief Write a byte to the serial port output buffer
//! \param _data The byte to write
//! \return Number of bytes written, 0 if none
int SerialPort::write(uint8_t _data)
{
    Guard g (*m_mutex); // reentrancy protection

    int bytes_written = 0;
    OS_IncDI();
    if (m_outBuffer.getRoom() > 0)
    {
        m_outBuffer.put(_data);
        bytes_written = 1;
    }
    OS_DecRI();
    
    enableTxInterrupt(); // (Re)Enable the USARTx Transmit interrupt (was disabled here, or in isr when no more items)
    return bytes_written;
}

//! \brief Write n bytes to the serial port output buffer
//! \param _pData Pointer to source array
//! \param _len Number of bytes to write
//! \return Number of bytes written, 0 if none
int SerialPort::write(const uint8_t * _pData, uint16_t _len)
{
    Guard g (*m_mutex); // reentrancy protection

    int bytes_written = 0;

    OS_IncDI();
    if (m_outBuffer.getRoom() >= _len)
    {
        m_outBuffer.put(_pData, _len);
        bytes_written = _len;
    }
    OS_DecRI();
    
    enableTxInterrupt(); // (Re)Enable the USARTx Transmit interrupt (was disabled here, or in isr when no more items)
    return bytes_written;
}

//! \brief Checks for available TX buffer space
//! \return true, if available TX buffer space, else false
//! \remark Mainly to be used for console help output, to avoid allocating huge TX ringbuffers\n
//! when printing out helptext.
bool SerialPort::hasTxBufferSpaceAvailable() const
{
    OS_IncDI();
    int space = m_outBuffer.getRoom();
    OS_DecRI();
    return space > 0;
}

//! \brief Returns available TX buffer space
//! \return Available TX buffer space in # of bytes
int SerialPort::getTxBufferSpaceAvailable() const
{
    OS_IncDI();
    int space = m_outBuffer.getRoom();
    OS_DecRI();
    return space;
}

//! \brief Returns maximum used TX buffer space
//! \return Maximum used TX buffer space in # of bytes
int SerialPort::getTxBufferWatermark()
{
    OS_IncDI();
    int maxused = m_outBuffer.getAndResetHighWatermark();
    OS_DecRI();
    return maxused;
}

//! \brief Returns TX buffer space
//! \return TX buffer space in # of bytes
int SerialPort::getTxBufferSize() const
{
    OS_IncDI();
    int size = m_outBuffer.getMaximumSize();
    OS_DecRI();
    return size;
}

//! \brief Returns maximum used RX buffer space
//! \return Maximum used RX buffer space in # of bytes
int SerialPort::getRxBufferWatermark()
{
    OS_IncDI();
    int maxused = m_inBuffer.getAndResetHighWatermark();
    OS_DecRI();
    return maxused;
}

//! \brief Returns RX buffer space
//! \return RX buffer space in # of bytes
int SerialPort::getRxBufferSize() const
{
    OS_IncDI();
    int size = m_inBuffer.getMaximumSize();
    OS_DecRI();
    return size;
}


//! \brief De-Initialise the UART device to the state before initialized (including GPIO port settings)
bool SerialPort::deinit()
{
    // Disable the USART Receive/transmit interrupts
    disableRxInterrupt();
    disableTxInterrupt();
    
    /* Disable RXNE and Error interrupts */
    LL_USART_DisableIT_RXNE(m_usart);
    LL_USART_DisableIT_ERROR(m_usart);
    
    /* Disable USART *********************************************************/
    LL_USART_Disable(m_usart);
    LL_USART_DeInit(m_usart);
    
    
    return true;
}

// Entry point for library __write for
// STDOUT and STDERR
// Implemented for using printf()
//
int SerialPort::MyLowLevelPutchar(int x)
{
    if (m_pPorts[m_defaultPort])
    {
        SerialPort* pPort = m_pPorts[m_defaultPort];
        while ( !pPort->hasTxBufferSpaceAvailable() )
        {
            __WFI();
        }
        pPort->write(x);
        return 1;
    }
    return -1;
}
//
// Entry point for library __read for
// STDIN
//
int SerialPort::MyLowLevelGetchar()
{
  if (m_pPorts[m_defaultPort])
  {
    SerialPort* pPort = m_pPorts[m_defaultPort];
    if (pPort)
    {
      return pPort->read();
    }
  }
  return -1;
}


//! \brief Interrupt handler for COMx
void SerialPort::isrX()
{
    if (LL_USART_IsActiveFlag_RXNE(m_usart))
    {
        m_pPorts[m_portNum]->m_inBuffer.put( m_usart->RDR);
    }

    
    if(LL_USART_IsActiveFlag_TXE(USART3))
    {
        uint8_t outByte;
        if ( m_pPorts[m_portNum]->m_outBuffer.get(outByte) )
        {
            // Write one byte to the transmit data register
            LL_USART_TransmitData8(USART3, outByte);
        }
        else
        {
            // Disable the USART Transmit interrupt
            disableTxInterrupt();
        }
    }
}


//! \brief Interrupt handler for COM1
void SerialPort::isr1()
{
    m_pPorts[COM1]->isrX();
}

//! \brief Interrupt handler for COM2
void SerialPort::isr2()
{
    m_pPorts[COM2]->isrX();
}

//! \brief Interrupt handler for COM3
void SerialPort::isr3()
{
    m_pPorts[COM3]->isrX();
}

//! \brief Interrupt handler for COM4
void SerialPort::isr4()
{
    m_pPorts[COM4]->isrX();
}

//! \brief Interrupt handler for COM5
void SerialPort::isr5()
{
    m_pPorts[COM5]->isrX();
}

//TODO Conflicts with fhrmprobeserial
/*
void USART1_IRQHandler(void)
{
    SerialPort::get(SerialPort::COM1)->isrX();
}
*/

void USART2_IRQHandler(void)
{
    SerialPort::get(SerialPort::COM2)->isrX();
}

void USART3_IRQHandler(void)
{
    SerialPort::get(SerialPort::COM3)->isrX();
}

void UART4_IRQHandler(void)
{
    SerialPort::get(SerialPort::COM4)->isrX();
}

void UART5_IRQHandler(void)
{
    SerialPort::get(SerialPort::COM5)->isrX();
}



