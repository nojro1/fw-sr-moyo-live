//! \file    Adc.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    17.03.2020

// include self first
#include "Adc.h"


// other includes
#include "RTOS.h"
#include <utilfuncs.h>

USING_NAMESPACE_STD;

// static member init
Adc*        Adc::m_pAdc     = 0;
RingBuffer* Adc::m_pEcgBuf  = 0;

//Test
static uint32_t smpCnt = 0;
//


//! \brief Constructor
Adc::Adc() : m_conversionResults(0)
{
    if (0 == m_pAdc)
    {
        m_pAdc              = this;
        init();
        start();
    }
}


//! destructor
Adc::~Adc()
{

    deinit();
    
    m_pAdc = 0;
}

//! \brief Read the selected ADC channel result
//! \param channel ADC channel (0..15)
//! \remark If a channel is selected that was originally not defined when instatiation this class\n
//! 0 will be returned
//! \return The selected ADC channel's result
uint16_t Adc::read(uint32_t channel)
{
    assert_param(IS_ADC_CHANNEL(channel));
    
    uint16_t adc_val = 0;
    
    switch(channel)
    {
        case AD_5V:
            adc_val = m_adc_ch_1;
            break;
        case AD_AMB_LIGHT:
            adc_val = m_adc_ch_2;
            break;
        case AD_IMPEDANCE:
            adc_val = m_adc_ch_3;
            break;
        case AD_ECG:
            adc_val = m_adc_ch_4;
            break;
        default:
            adc_val = 0; // not configured, so just return 0
            break;
    }
    
    return adc_val;
}

//! \brief Read the selected ADC channel result as mV
//! \param channel ADC channel (0..15)
//! \remark If a channel is selected that was originally not defined when instantiating this class\n
//! 0 will be returned
//! \return The selected ADC channel's result in mV
uint16_t Adc::readAsMillivolts(uint8_t channel)
{
    //TODO
    return 0;
}

//! \brief Read the selected ADC channel result as uV
//! \param channel ADC channel (0..15)
//! \remark If a channel is selected that was originally not defined when instantiating this class\n
//! 0 will be returned
//! \return The selected ADC channel's result in uV
uint32_t Adc::readAsMicrovolts(uint8_t channel)
{
    //TODO
    return 0;
}

//! \brief Read the external reference voltage as mV (based on the internal reference voltage)\n
//! (i.e. power supply voltage, if used as reference)
//! \remark If the Vref channel was not configured when instantiating this class, 0 will be returned
//! \return The external reference voltage in mV
uint16_t Adc::readExternalRefAsMillivolts()
{

    //TODO
    //return static_cast<uint16_t>( (1200*ADC_RANGE) / read(ADC_Channel_Vrefint) );
    return 0;

}

int16_t Adc::readInternalTemp_C()
{
    //TODO
    return 0;
}


void Adc::init_convres()
{
    if (0==m_conversionResults)
    {
        m_conversionResults = new uint16_t[ ADC_DMA_BUF_SIZE ];
    }
    memset(m_conversionResults, 0, ADC_DMA_BUF_SIZE);
}

//! \brief Initialise the ADC device
//! \param channelMask A 16 bit mask, defining which channels to be used\n
//! e.g. 0x8002 = Channel 15 and channel 1 is used
void Adc::init()
{
    
    init_convres( );
    
    LL_ADC_InitTypeDef ADC_InitStruct = {0};
    LL_ADC_REG_InitTypeDef ADC_REG_InitStruct = {0};
    LL_ADC_CommonInitTypeDef ADC_CommonInitStruct = {0};

    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

    LL_RCC_SetADCClockSource(LL_RCC_ADC_CLKSOURCE_PLLSAI1);

    /* Peripheral clock enable */
    LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_ADC);

    LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOC);
    /**ADC1 GPIO Configuration  
    PC0   ------> ADC1_IN1
    PC1   ------> ADC1_IN2
    PC2   ------> ADC1_IN3
    PC3   ------> ADC1_IN4 
    */
    GPIO_InitStruct.Pin = AN_ambLight_Pin|AN_impedance_Pin|AN_ECG_Pin|AN_5V_Pin;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    //ADC1 DMA Init
    cfg_dma( );

    
    //Common config
    ADC_InitStruct.Resolution = LL_ADC_RESOLUTION_12B;
    ADC_InitStruct.DataAlignment = LL_ADC_DATA_ALIGN_RIGHT;
    ADC_InitStruct.LowPowerMode = LL_ADC_LP_MODE_NONE;
    LL_ADC_Init(ADC1, &ADC_InitStruct);
    ADC_REG_InitStruct.TriggerSource = LL_ADC_REG_TRIG_EXT_TIM2_TRGO;
    ADC_REG_InitStruct.SequencerLength = LL_ADC_REG_SEQ_SCAN_ENABLE_4RANKS;
    ADC_REG_InitStruct.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_DISABLE;
    ADC_REG_InitStruct.ContinuousMode = LL_ADC_REG_CONV_SINGLE;
    ADC_REG_InitStruct.DMATransfer = LL_ADC_REG_DMA_TRANSFER_UNLIMITED;
    ADC_REG_InitStruct.Overrun = LL_ADC_REG_OVR_DATA_OVERWRITTEN;
    LL_ADC_REG_Init(ADC1, &ADC_REG_InitStruct);
    LL_ADC_SetOverSamplingScope(ADC1, LL_ADC_OVS_DISABLE);
    LL_ADC_DisableIT_EOC(ADC1);
    LL_ADC_DisableIT_EOS(ADC1);
    LL_ADC_DisableDeepPowerDown(ADC1);
    LL_ADC_EnableInternalRegulator(ADC1);
    ADC_CommonInitStruct.CommonClock = LL_ADC_CLOCK_SYNC_PCLK_DIV2;
    ADC_CommonInitStruct.Multimode = LL_ADC_MULTI_INDEPENDENT;
    LL_ADC_CommonInit(__LL_ADC_COMMON_INSTANCE(ADC1), &ADC_CommonInitStruct);
    LL_ADC_REG_SetTriggerEdge(ADC1, LL_ADC_REG_TRIG_EXT_RISING);
    /** Configure Regular Channel 
    */
    LL_ADC_REG_SetSequencerRanks(ADC1, LL_ADC_REG_RANK_1, LL_ADC_CHANNEL_1);
    LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_1, LL_ADC_SAMPLINGTIME_6CYCLES_5);
    LL_ADC_SetChannelSingleDiff(ADC1, LL_ADC_CHANNEL_1, LL_ADC_SINGLE_ENDED);
    /** Configure Regular Channel 
    */
    LL_ADC_REG_SetSequencerRanks(ADC1, LL_ADC_REG_RANK_2, LL_ADC_CHANNEL_2);
    LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_2, LL_ADC_SAMPLINGTIME_6CYCLES_5);
    LL_ADC_SetChannelSingleDiff(ADC1, LL_ADC_CHANNEL_2, LL_ADC_SINGLE_ENDED);
    /** Configure Regular Channel 
    */
    LL_ADC_REG_SetSequencerRanks(ADC1, LL_ADC_REG_RANK_3, LL_ADC_CHANNEL_3);
    LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_3, LL_ADC_SAMPLINGTIME_6CYCLES_5);
    LL_ADC_SetChannelSingleDiff(ADC1, LL_ADC_CHANNEL_3, LL_ADC_SINGLE_ENDED);
    /** Configure Regular Channel 
    */
    LL_ADC_REG_SetSequencerRanks(ADC1, LL_ADC_REG_RANK_4, LL_ADC_CHANNEL_4);
    LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_4, LL_ADC_SAMPLINGTIME_6CYCLES_5);
    LL_ADC_SetChannelSingleDiff(ADC1, LL_ADC_CHANNEL_4, LL_ADC_SINGLE_ENDED);
    LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);
    
    //Config timer for ADC samples
    cfg_tim( );
    
    
}

void Adc::cfg_tim()
{
    uint32_t timer_clock_frequency = 0;             /* Timer clock frequency */
    uint32_t timer_prescaler = 0;                   /* Time base prescaler to have timebase aligned on minimum frequency possible */
    uint32_t timer_reload = 0;                      /* Timer reload value in function of timer prescaler to achieve time base period */

    LL_TIM_InitTypeDef TIM_InitStruct = {0};

    /* Peripheral clock enable */
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);

    TIM_InitStruct.Prescaler = 0;
    TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
    TIM_InitStruct.Autoreload = 0;
    TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
    LL_TIM_Init(TIM2, &TIM_InitStruct);
    LL_TIM_DisableARRPreload(TIM2);
    LL_TIM_SetClockSource(TIM2, LL_TIM_CLOCKSOURCE_INTERNAL);
    LL_TIM_SetTriggerOutput(TIM2, LL_TIM_TRGO_RESET);
    LL_TIM_DisableMasterSlaveMode(TIM2);
    if (LL_RCC_GetAPB1Prescaler() == LL_RCC_APB1_DIV_1)
    {
        timer_clock_frequency = __LL_RCC_CALC_PCLK1_FREQ(SystemCoreClock, LL_RCC_GetAPB1Prescaler());
    }
    else
    {
        timer_clock_frequency = (__LL_RCC_CALC_PCLK1_FREQ(SystemCoreClock, LL_RCC_GetAPB1Prescaler()) * 2);
    }

    /* Timer prescaler calculation */
    /* (computation for timer 16 bits, additional + 1 to round the prescaler up) */
    timer_prescaler = ((timer_clock_frequency / (TIMER_PRESCALER_MAX_VALUE * TIMER_FREQUENCY_RANGE_MIN)) +1);
    /* Timer reload calculation */
    timer_reload = (timer_clock_frequency / (timer_prescaler * ADC_SAMPLE_FREQ));
    /* Enable the timer peripheral clock */
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);

    /* Set timer pre-scaler value */
    LL_TIM_SetPrescaler(TIM2, (timer_prescaler - 1));

    /* Set timer auto-reload value */
    LL_TIM_SetAutoReload(TIM2, (timer_reload - 1));

    /* Counter mode: select up-counting mode */
    LL_TIM_SetCounterMode(TIM2, LL_TIM_COUNTERMODE_UP); 

    /* Set the repetition counter */
    LL_TIM_SetRepetitionCounter(TIM2, 0);

    /* Set timer the trigger output (TRGO) */
    LL_TIM_SetTriggerOutput(TIM2, LL_TIM_TRGO_UPDATE);

  
}

void Adc::cfg_dma()
{
    /* DMA controller clock enable */
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMAMUX1);
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);
    
    /* ADC1 Init */
    LL_DMA_SetPeriphRequest(DMA1, LL_DMA_CHANNEL_1, LL_DMAMUX_REQ_ADC1);

    LL_DMA_SetDataTransferDirection(DMA1, LL_DMA_CHANNEL_1, LL_DMA_DIRECTION_PERIPH_TO_MEMORY);

    LL_DMA_SetChannelPriorityLevel(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PRIORITY_LOW);

    LL_DMA_SetMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MODE_CIRCULAR);

    LL_DMA_SetPeriphIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PERIPH_NOINCREMENT);

    LL_DMA_SetMemoryIncMode(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MEMORY_INCREMENT);

    LL_DMA_SetPeriphSize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_PDATAALIGN_HALFWORD);

    LL_DMA_SetMemorySize(DMA1, LL_DMA_CHANNEL_1, LL_DMA_MDATAALIGN_HALFWORD);

    LL_DMA_DisableChannelPrivilege(DMA1, LL_DMA_CHANNEL_1);

    LL_ADC_REG_SetContinuousMode(ADC1, LL_ADC_REG_CONV_CONTINUOUS);
    LL_ADC_REG_SetDMATransfer(ADC1, LL_ADC_REG_DMA_TRANSFER_UNLIMITED);
    LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_1, ADC_DMA_BUF_SIZE);
    LL_DMA_ConfigAddresses(DMA1, 
                           LL_DMA_CHANNEL_1,
                           LL_ADC_DMA_GetRegAddr(ADC1, LL_ADC_DMA_REG_REGULAR_DATA),
                           (uint32_t)m_conversionResults,
                           LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
    

    /* Enable DMA transfer interruption: transfer complete */
    LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_1);

    /* Enable DMA transfer interruption: half transfer */
    LL_DMA_EnableIT_HT(DMA1, LL_DMA_CHANNEL_1);

    /* Enable DMA transfer interruption: transfer error */
    LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_1);
    
    /* DMA1_Channel1_IRQn interrupt configuration */
    NVIC_SetPriority(DMA1_Channel1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),5, 0));
    NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  

}

void Adc::cfg_adc_calibrate()
{

}


//! \brief De-Initialise the ADC device to the state before initialized (including GPIO port settings)
void Adc::deinit()
{
    // Stop ADC1 Software Conversion

    // Disable ADC1

    // Disable ADC1 DMA

    // Disable internal reference

    //DMA_DeInit(DMA1_Channel1);

    // TODO: set back GPIO regs to original values (if destructor is ever invoked)

    delete[] m_conversionResults;
}

void Adc::isrx(void)
{
    if(LL_DMA_IsActiveFlag_TC1(DMA1))
    {
        LL_DMA_ClearFlag_TC1(DMA1);
        LL_DMA_ClearFlag_GI1(DMA1);
        conversionComplete(m_conversionResults + (ADC_DMA_BUF_SIZE/2/sizeof(uint16_t)), ADC_DMA_BUF_SIZE/2);
    }
    else if(LL_DMA_IsActiveFlag_HT1(DMA1))
    {
        LL_DMA_ClearFlag_HT1(DMA1);
        LL_DMA_ClearFlag_GI1(DMA1);
        conversionComplete(m_conversionResults, ADC_DMA_BUF_SIZE/2);
    }
}

void Adc::conversionComplete(const uint16_t * pData, uint16_t numsamples)
{
    
    uint16_t i;
    
    if(m_pEcgBuf != NULL)
    {
        for(i = 0; i < ADC_DMA_BUF_SIZE; i++)
        {
            m_pEcgBuf->Put(static_cast<int16_t>(m_conversionResults[i]));
        }
    }    
    m_adc_ch_1 = averageChannelX(AD_5V_IDX, m_conversionResults, NO_ADC_SAMPLES_IN_BUFFER);
    m_adc_ch_2 = averageChannelX(AD_AMB_LIGHT_IDX, m_conversionResults, NO_ADC_SAMPLES_IN_BUFFER);
    m_adc_ch_3 = averageChannelX(AD_IMPEDANCE_IDX, m_conversionResults, NO_ADC_SAMPLES_IN_BUFFER);
    m_adc_ch_4 = averageChannelX(AD_ECG_IDX, m_conversionResults, NO_ADC_SAMPLES_IN_BUFFER);
  
    smpCnt += NO_ADC_SAMPLES_IN_BUFFER;
}

uint16_t Adc::averageChannelX(uint32_t channelIdx, const uint16_t * pData, uint16_t numsamples)
{
    uint32_t sum = 0;
    uint16_t * pvals = const_cast<uint16_t*>(pData);
    uint16_t * pend  = const_cast<uint16_t*>(pData) + (NO_ADC_SAMPLES_IN_BUFFER * NUM_ADC_CHANNELS);
    
    if(channelIdx < NUM_ADC_CHANNELS)
    {
    
        pvals += channelIdx;
        
        while(pvals < pend)
        {
            sum += *pvals;
            pvals += NUM_ADC_CHANNELS;
        }
        
        sum /= numsamples;
    }
    
    return sum;
}

void Adc::start()
{
    /* Enable ADC conversion */
    LL_ADC_Enable(ADC1);
    LL_TIM_EnableCounter(TIM2);
    LL_ADC_REG_StartConversion(ADC1);
}


extern "C" void DMA1_Channel1_IRQHandler(void)
{
    Adc::get()->isrx();
}