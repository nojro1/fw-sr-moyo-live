//! \file    Events.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    07.11.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  07Nov2013, JRo, Original.


#include "events.h"


//Mutex
static Mutex * bufferMutex = 0;

// static member init
Events*  Events::m_pEvents = 0;

//! Event
//! @brief Constructor
//! @param none
//! @return none
//! @author JRO
Events::Events()
{
    if(m_pEvents == 0)
    {
        m_events.reserve(MAX_EVENTS + 1);
        m_events.resize(0);
        
        m_eventString.reserve(200);
        
        m_systemLogEvent = false;
        m_errorLogEvent = false;
        m_episodeStartTime = 0;
        
        m_pEvents = this;
    }
}

//! ~Event
//! @brief Destructor
//! @param none
//! @return none
//! @remark Protected
//! @author JRO
Events::~Events()
{
    
}

void Events::AddEvent(EventId_t a_eventId,
                      uint8_t a_toLog,
                      float a_dataField_1,
                      float a_dataField_2,
                      float a_dataField_3,
                      float a_dataField_4,
                      float a_dataField_5,
                      float a_dataField_6,
                      float a_dataField_7,
                      float a_dataField_8,
                      float a_dataField_9,
                      float a_dataField_10,
                      float a_dataField_11,
                      float a_dataField_12)
{
    bufferLock();
    
    event_t event;
    
    if(m_events.size() >= MAX_EVENTS - 1)
    {
        m_events.erase(m_events.begin());
        DEBUG_TRACE("\r\nEvents::AddEvent - Too many events, deleted one\r\n");
    }
    
    event.m_valid = true;
    event.m_timeStamp = OS_GetTime() - m_episodeStartTime;
    event.m_eventId = a_eventId;
    
    if((a_toLog & TO_SYSTEM_LOG) == TO_SYSTEM_LOG)
    {
        event.m_toSystemLog = true;
    }
    else
    {
        event.m_toSystemLog = false;
    }
    
    if((a_toLog & TO_ERROR_LOG) == TO_ERROR_LOG)
    {
        event.m_toErrorLog = true;
    }
    else
    {
        event.m_toErrorLog = false;
    }
    
    event.m_text[0] = 0;
    event.m_dataFields[0] = a_dataField_1;
    event.m_dataFields[1] = a_dataField_2;
    event.m_dataFields[2] = a_dataField_3;
    event.m_dataFields[3] = a_dataField_4;
    event.m_dataFields[4] = a_dataField_5;
    event.m_dataFields[5] = a_dataField_6;
    event.m_dataFields[6] = a_dataField_7;
    event.m_dataFields[7] = a_dataField_8;
    event.m_dataFields[8] = a_dataField_9;
    event.m_dataFields[9] = a_dataField_10;
    event.m_dataFields[10] = a_dataField_11;
    event.m_dataFields[11] = a_dataField_12;
    m_events.push_back(event);
    
    bufferUnlock();
}

void Events::AddEvent(uint32_t a_timeStamp,
                      EventId_t a_eventId,
                      uint8_t a_toLog,
                      float a_dataField_1,
                      float a_dataField_2,
                      float a_dataField_3,
                      float a_dataField_4,
                      float a_dataField_5,
                      float a_dataField_6,
                      float a_dataField_7,
                      float a_dataField_8,
                      float a_dataField_9,
                      float a_dataField_10,
                      float a_dataField_11,
                      float a_dataField_12)
{
    bufferLock();
    
    event_t event;
    
    if(m_events.size() >= MAX_EVENTS - 1)
    {
        m_events.erase(m_events.begin());
        DEBUG_TRACE("\r\nEvents::AddEvent - Too many events, deleted one\r\n");
    }
    
    event.m_valid = true;
    
    //Avoid negative time
    if(a_timeStamp > m_episodeStartTime)
    {
        event.m_timeStamp = a_timeStamp - m_episodeStartTime;
    }
    else
    {
        event.m_timeStamp = 0;
    }
    
    event.m_eventId = a_eventId;
    
    if((a_toLog & TO_SYSTEM_LOG) == TO_SYSTEM_LOG)
    {
        event.m_toSystemLog = true;
    }
    else
    {
        event.m_toSystemLog = false;
    }
    
    if((a_toLog & TO_ERROR_LOG) == TO_ERROR_LOG)
    {
        event.m_toErrorLog = true;
    }
    else
    {
        event.m_toErrorLog = false;
    }
    
    event.m_text[0] = 0;
    event.m_dataFields[0] = a_dataField_1;
    event.m_dataFields[1] = a_dataField_2;
    event.m_dataFields[2] = a_dataField_3;
    event.m_dataFields[3] = a_dataField_4;
    event.m_dataFields[4] = a_dataField_5;
    event.m_dataFields[5] = a_dataField_6;
    event.m_dataFields[6] = a_dataField_7;
    event.m_dataFields[7] = a_dataField_8;
    event.m_dataFields[8] = a_dataField_9;
    event.m_dataFields[9] = a_dataField_10;
    event.m_dataFields[10] = a_dataField_11;
    event.m_dataFields[11] = a_dataField_12;
    m_events.push_back(event);
    
    bufferUnlock();
}

void Events::AddEvent(EventId_t a_eventId,
                      uint8_t a_toLog,
                      const char *a_text,
                      float a_dataField_1,
                      float a_dataField_2,
                      float a_dataField_3,
                      float a_dataField_4,
                      float a_dataField_5,
                      float a_dataField_6,
                      float a_dataField_7,
                      float a_dataField_8,
                      float a_dataField_9,
                      float a_dataField_10,
                      float a_dataField_11,
                      float a_dataField_12)
{
    bufferLock();
    
    event_t event;
    char *pText = (char *)a_text;
    uint8_t len = 0;
    
    if(m_events.size() >= MAX_EVENTS - 1)
    {
        m_events.erase(m_events.begin());
        DEBUG_TRACE("\r\nEvents::AddEvent - Too many events, deleted one\r\n");
    }
    
    event.m_valid = true;
    event.m_timeStamp = OS_GetTime() - m_episodeStartTime;
    event.m_eventId = a_eventId;
    
    if((a_toLog & TO_SYSTEM_LOG) == TO_SYSTEM_LOG)
    {
        event.m_toSystemLog = true;
    }
    else
    {
        event.m_toSystemLog = false;
    }
    
    if((a_toLog & TO_ERROR_LOG) == TO_ERROR_LOG)
    {
        event.m_toErrorLog = true;
    }
    else
    {
        event.m_toErrorLog = false;
    }
    
    event.m_text[0] = 0;
    
    while(*pText != 0 && len < TEXT_MAX_LEN - 1)
    {
        event.m_text[len++] = *pText++;
    }
    
    event.m_text[len] = 0;  //Add the 0 terminator in case of missing
    
    event.m_dataFields[0] = a_dataField_1;
    event.m_dataFields[1] = a_dataField_2;
    event.m_dataFields[2] = a_dataField_3;
    event.m_dataFields[3] = a_dataField_4;
    event.m_dataFields[4] = a_dataField_5;
    event.m_dataFields[5] = a_dataField_6;
    event.m_dataFields[6] = a_dataField_7;
    event.m_dataFields[7] = a_dataField_8;
    event.m_dataFields[8] = a_dataField_9;
    event.m_dataFields[9] = a_dataField_10;
    event.m_dataFields[10] = a_dataField_11;
    event.m_dataFields[11] = a_dataField_12;
    m_events.push_back(event);
    
    bufferUnlock();
}

Events::event_t Events::GetEvent()
{
    bufferLock();
    
    if(m_events.size() > 0)
    {
        bufferUnlock();
        
        return m_events.at(0);
    }
    else
    {
        event_t event;
        event.m_valid = false;
        
        bufferUnlock();
        
        return event;
    }    
}

std::string Events::EventStringPullFront()
{
    bufferLock();
    
    m_systemLogEvent = false;
    m_errorLogEvent = false;
    
    if(m_events.size() > 0)
    {
        event_t event = m_events.at(0);
        m_events.erase(m_events.begin());
        
        m_systemLogEvent = event.m_toSystemLog;
        m_errorLogEvent = event.m_toErrorLog;
        
        char buf[30];
        uint16_t len;
       
        len = sprintf(buf, "%05u.%03u", event.m_timeStamp / 1000, event.m_timeStamp % 1000);
        std::string time(buf,len);
    
        len = sprintf(buf, "%04u", event.m_eventId);
        std::string evId(buf,len);
        
        
        //Make event string
        m_eventString = time + ",";
        
        switch(event.m_eventId)
        {
            case evtHeaderText:
                m_eventString += "S,";
                m_eventString += "1000,";
                m_eventString += "Header text,";
                if(event.m_text[0])
                {
                    std::string txt(event.m_text);
                    m_eventString += txt;
                    m_eventString += ",";
                }
                break;
            
            case evtEpisodeDateAndTime:
                m_eventString += "S,";
                m_eventString += "1001,";
                m_eventString += "Episode date and time,";
                if(event.m_text[0])
                {
                    std::string txt(event.m_text);
                    m_eventString += txt;
                }
                break;
            case evtDeviceSerialNumber:
                m_eventString += "S,";
                m_eventString += "1002,";
                m_eventString += "Device serial number,";
                if(event.m_text[0])
                {
                    std::string txt(event.m_text);
                    m_eventString += txt;
                }
                else
                {
                    m_eventString += toStr(event.m_dataFields[0], "%8.0f");
                    m_eventString += ",";
                }
                break;
            case evtSoftwareVersion:
                m_eventString += "S,";
                m_eventString += "1003,";
                m_eventString += "Software version,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                m_eventString += ".";
                m_eventString += toStr(event.m_dataFields[1], "%.0f");
                m_eventString += ".";
                m_eventString += toStr(event.m_dataFields[2], "%.0f");
                m_eventString += ".";
                m_eventString += toStr(event.m_dataFields[3], "%.0f");
                break;
            case evtDeviceConfiguration:
              m_eventString += "S,";
                m_eventString += "1004,";
                m_eventString += "Device configuration,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[1], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[2], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[3], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[4], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[5], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[6], "%.0f");
                break;
            case evtUltrasoundModuleVersion:
                m_eventString += "S,";
                m_eventString += "1005,";
                m_eventString += "Ultrasound module version,";
                if(event.m_text[0])
                {
                    //0  1  2  3  4  5  6  7  8  9  10 11 12  
                    //h  h  h  h  h  h  h  h  .  h  h  h  h 
                    
                    //13 14 15 16 17 18 19 20 21 22 23 24 25
                    //s  s  s  s  s  s  s  s  .  s  s  s  s
                    
                    std::string hwver(event.m_text, 0, 13);
                    std::string swver(event.m_text, 13, 13);
                    m_eventString += hwver;
                    m_eventString += ",";
                    m_eventString += swver;
                }
                break;
            case evtDisplayMode:
                m_eventString += "S,";
                m_eventString += "1010,";
                m_eventString += "Display mode,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                break;
            case evtVolume:
                m_eventString += "S,";
                m_eventString += "1011,";
                m_eventString += "Volume,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                break;
            case evtEndOfEpisodeTimeout:
                m_eventString += "S,";
                m_eventString += "1012,";
                m_eventString += "End of episode timeout,";
                m_eventString += toStrBool10(event.m_dataFields[0]);
                break;
            case evtAlarmAcknowledged:
                m_eventString += "S,";
                m_eventString += "1013,";
                m_eventString += "Alarm acknowledged,";
                m_eventString += toStrBool10(event.m_dataFields[0]);
                break;
            case evtPowerOnSelfTestResult:
                m_eventString += "S,";
                m_eventString += "1020,";
                m_eventString += "Power on self test result,";
                m_eventString += formatSelfTestResult(event.m_dataFields[0]);
                break;
            case evtPowerOnBatteryState:
                m_eventString += "S,";
                m_eventString += "1021,";
                m_eventString += "Power on battery state,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                break;
            case evtPowerOffBatteryState:
                m_eventString += "S,";
                m_eventString += "1022,";
                m_eventString += "Power off battery state,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                break;
            case evtDisplayError:
                m_eventString += "S,";
                m_eventString += "1030,";
                m_eventString += "Display error,";
                m_eventString += toStrBool10(event.m_dataFields[0]);
                break;
            case evtBatteryLow:
                m_eventString += "S,";
                m_eventString += "1031,";
                m_eventString += "Battery low,";
                m_eventString += toStrBool10(event.m_dataFields[0]);
                break;
            case evtFileWritingError:
                m_eventString += "S,";
                m_eventString += "1032,";
                m_eventString += "File writing error,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                break;
            case evtUltrasoundTransducerCableBroken:
                m_eventString += "S,";
                m_eventString += "1033,";
                m_eventString += "Ultrasound transducer cable broken,";
                m_eventString += toStrBool10(event.m_dataFields[0]);
                break;
            case evtLowBatteryAlarm:
                m_eventString += "S,";
                m_eventString += "1040,";
                m_eventString += "Low battery alarm,";
                m_eventString += toStrBool10(event.m_dataFields[0]);
                break;
            case evtLostSignalAlarm:
                m_eventString += "S,";
                m_eventString += "1041,";
                m_eventString += "Lost signal alarm,";
                m_eventString += toStrBool10(event.m_dataFields[0]);
                break;
            case evtCriticalErrorAlarm:
                m_eventString += "S,";
                m_eventString += "1042,";
                m_eventString += "Critical error alarm,";
                m_eventString += toStrBool10(event.m_dataFields[0]);
                break;
            case evtAbnormalFHRalarm:
                m_eventString += "S,";
                m_eventString += "1043,";
                m_eventString += "Abnormal FHR alarm,";
                m_eventString += toStrBool10(event.m_dataFields[0]);
                break;
            case evtMaternalQRS:
                m_eventString += "E,";
                m_eventString += " 100,";
                m_eventString += "Maternal QRS,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[1], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[2], "%.2f");
                break;
            case evtECGSignalQuality:
                m_eventString += "E,";
                m_eventString += " 110,";
                m_eventString += "ECG signal quality,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                break;
            case evtFHR:
                m_eventString += "U,";
                m_eventString += " 500,";
                m_eventString += "FHR,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[1], "%.0f");
                break;
            case evtProbeTemperature:
                m_eventString += "U,";
                m_eventString += " 501,";
                m_eventString += "Probe temperature,";
                m_eventString += toStr(event.m_dataFields[0], "%.2f");
                break;
            case evtTransducerState:
                m_eventString += "U,";
                m_eventString += " 502,";
                m_eventString += "Transducer state,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                break;
            case evtBatteryState:
                m_eventString += "S,";
                m_eventString += "9999,";
                m_eventString += "Battery state,";
                m_eventString += toStr(event.m_dataFields[0], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[1], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[2], "%.0f");
                m_eventString += ",";
                m_eventString += toStr(event.m_dataFields[3], "%.0f");
                break;
            default:
                m_eventString += ",";
                m_eventString += ",";
                m_eventString += ",";
                break;
        }
   
        m_eventString += '\r';
        m_eventString += '\n';
        
        bufferUnlock();
        
        return m_eventString;
    }
    else
    {
        
        bufferUnlock();
        
        return "";
    }    
}

uint8_t Events::Count()
{
    return m_events.size();
}

bool Events::isSystemLogEvent()
{
    return m_systemLogEvent;
}

bool Events::isErrorLogEvent()
{
    return m_errorLogEvent;
}

// lock buffer
bool Events::bufferLock()
{
    if (0==bufferMutex)
    {
        bufferMutex = new Mutex(); // create when needed
    }
    if (bufferMutex)
    {
        bufferMutex->lock();
        return true;
    }
    return false;
}

// unlock buffer
bool Events::bufferUnlock()
{
    if (bufferMutex)
    {
        bufferMutex->release();
        return true;
    }
    return false;
}

std::string Events::toStrBool10(float a_value)
{
    if(a_value > 0.0)
    {
        return "1";
    }
    else
    {
        return "0";
    }
}

std::string Events::toStr(float a_value, const char * a_fmtStr)
{
    char buf[20];
    uint16_t len;
    len = sprintf(buf, a_fmtStr, a_value);
    std::string s(buf,len);
    return s;
}

std::string Events::formatSelfTestResult(float a_value)
{
    char buf[20];
    uint16_t len;
    len = sprintf(buf, "0x%X", (uint32_t)a_value);
    std::string s(buf,len);
    return s;
}

void Events::SetEpisodeStartTime(uint32_t a_epStartTime)
{
    m_episodeStartTime = a_epStartTime;
}