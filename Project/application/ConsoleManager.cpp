
// self
#include "ConsoleManager.h"
#include "ConsoleThread.h"
#include "includes.h"
#include "adc.h"
#include "rtc.h"
#include "utilfuncs.h"
#include <FHRMAudio.h>
#include "th1super.h"

#include <stdlib.h>
#include <utility>
//#include <algorithm>

//Boot settings
#pragma location = "IAP_UPLOAD_ACTIVE"
__root const __no_init uint32_t __iap_upload_active;
#define BOOT_SETTINGS_PAGE              CFG_BOOT_SETTINGS_PAGE
#define BOOT_SETTINGS_BANK_NUMBER       CFG_BOOT_SETTINGS_BANK_NUMBER

//! ctor
ConsoleManager::ConsoleManager(ConsoleThread * pConTh)
    : m_pConsoleThread(pConTh), m_currentArgument(0)
{
    m_arguments.reserve(4);
}

//! dtor
ConsoleManager::~ConsoleManager()
{
}


inline void ConsoleManager::setupArguments(const std::string & commandString)
{
    std::string::size_type i = 0;
    m_currentArgument = 0;
    m_arguments.resize(0);
    m_option = "";
    
    while (i < commandString.length() && i != std::string::npos)
    {
        i = commandString.find_first_not_of(' ', i);
        if (i != std::string::npos)
        {
            std::string::size_type j = commandString.find_first_of(' ', i);
            if (j != std::string::npos)
            {
                std::string s(commandString.substr(i, j-i));
                m_arguments.push_back(s);
            }
            i = j;
        }

    }

}

//! \brief Parse the received command string and delegate actions
//! \param commandString string reference containing the commands
//! \remark Please update console_help.txt and console_commands.txt when you add new commands
void ConsoleManager::parseConsoleCommand(std::string & commandString)
{
    static const char helpString[] =
    #include "Console_help.txt"
    ; // this one is needed


    setupArguments(commandString);

    if (argumentStartsWith("*")) // repeat previous command
    {
        commandString = m_previousCommands;
        setupArguments(commandString);
    }

    if (argumentStartsWith("sys"))
    {
        parseSysCommand();
    }
    else if (argumentStartsWith("cfg"))
    {
        parseCFcommand();
    }
    else if (argumentStartsWith("test"))
    {
        parseTestCommand();
    }
    else if (argumentStartsWith("rtc"))
    {
        parseRTCcommand();
    }
    else if (argumentStartsWith("sound"))
    {
        parseAudioCommand();
    }
    else if (argumentStartsWith("power"))
    {
        parsePowerCommand();
    }
    else if (argumentStartsWith("bat"))
    {
        parseBatteryCommand();
    }
    else if (argumentStartsWith("ft"))
    {
        parseFileTestCommand();
    }
    else if (argumentStartsWith("adc"))
    {
        parseAdcCommand();
    }
    else if (argumentStartsWith("disp"))
    {
        parseDisplayCommand();
    }
    else if (argumentStartsWith("ecg"))
    {
        parseECGCommand();
    }
    else if (argumentStartsWith("fw_upd"))
    {
        enterBootloader();
    }
    else if (argumentStartsWith("help") || argumentStartsWith("?"))
    {
        CONSOLE_PRINT(helpString);
    }
    else
    {
        CONSOLE_PRINT("?\n"); // unknown command
    }

    m_previousCommands = commandString;

}


// ----------------------------
// Commands used for all builds
// ----------------------------



//! \brief Parse the 'sys' command and perform actions according to further argument
//! [osinfo] - displays information about the RTOS build
//! [cpuload] [cnt n] - displays each task's CPU load n times
//! Ends up by displaying each task's stack usage in percent
void ConsoleManager::parseSysCommand()
{
    getNextArgument();
    getNextOption();
    
    if (isOption("cpuload"))
    {
        displayCPUload();
    }

#ifdef HEAP_USAGE_SUPPORT
    else if (isOption("heapusage"))
    {
        printHeapUsage();
    }
#endif
    
#if OS_CHECKSTACK
    CONSOLE_PRINT("\r\nStack usage[%%]:\r\n");
    for (size_t i = P_UNDEF+1; i <= NUMBER_OF_THREADS; ++i)
    {
        OS_TASK* pTask = Thread::getpTcb(static_cast<THREAD_ID>(i));
        if (pTask)
        {
            uint32_t stkUsed = (100*OS_GetStackUsed( pTask )) / OS_GetStackSize( pTask );
            CONSOLE_PRINT("%s=%u\r\n", OS_GetTaskName(pTask), stkUsed);
        }
    }
#endif
    
}


void ConsoleManager::displayCPUload()
{
#if OS_PROFILE >= 1
    int cnt = 1;
    getNextOption();
    if (isOption("cnt"))
    {
        cnt = getNextOptionalNumericParameter(1, 1000000);
    }

    CONSOLE_PRINT("\r\nMeasuring CPU load (%d*5 sec intervals). Press a key to terminate.", cnt);
    CONSOLE_PRINT("\r\nCPU load[%%]:\r\n");

    uint32_t prevExecTimeCycles[NUMBER_OF_THREADS] = {0};
    const uint32_t measurementTime = 5000*(m_pConsoleThread->getHw()->getSysClk()/1000);
    do
    {
        OS_STAT_Sample();
        // wait more than 5 seconds since OS_STAT_Sample measures for 5 seconds
        m_pConsoleThread->sleep(5100);

        float totalCpuLoad = 0;
        for (size_t i = P_UNDEF+1; i <= NUMBER_OF_THREADS; ++i)
        {
            OS_TASK* pTask = Thread::getpTcb(static_cast<THREAD_ID>(i));
            if (OS_GetpCurrentTask() != pTask && pTask != 0) // exclude current task (not reliable calc)
            {
                //OS_EnterRegion();
                uint32_t execTimeCycles = OS_STAT_GetExecTime_Cycles(pTask);
                //OS_LeaveRegion();

                float taskCpuLoad = static_cast<float>(execTimeCycles-prevExecTimeCycles[i]) / measurementTime;
                taskCpuLoad *= 100; // to percent
                totalCpuLoad += taskCpuLoad;
                CONSOLE_PRINT("%s=%.3f\r\n",OS_GetTaskName(pTask), taskCpuLoad);
                prevExecTimeCycles[i] = execTimeCycles;
            }
        }
        CONSOLE_PRINT("Total=%.3f\r\n", totalCpuLoad);
    }
    while ( ! keypress() && --cnt > 0);

#else
    CONSOLE_PRINT("\r\nA build with OS_LIBMODE_DP or OS_LIBMODE_RP is needed");
#endif
}

void ConsoleManager::enterBootloader()
{
    const uint32_t *iap_upload_active_addr = &__iap_upload_active;
    uint32_t PageError = 0;
    FLASH_EraseInitTypeDef pEraseInit = {0};
    HAL_StatusTypeDef status = HAL_OK;

    /* Unlock the Flash to enable the flash control register access *************/
    HAL_FLASH_Unlock();

    //Erase IAP flag first (need to erase whole page)
    pEraseInit.Banks = BOOT_SETTINGS_BANK_NUMBER;
    pEraseInit.NbPages = 1;
    pEraseInit.Page = BOOT_SETTINGS_PAGE;
    pEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
    status = HAL_FLASHEx_Erase(&pEraseInit, &PageError);
  
    if(status == HAL_OK)
    {
        //Set flag
        status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, (uint32_t)iap_upload_active_addr, IAP_UPLOAD_PATTERN);
    }
    
    if(status == HAL_OK)
    {
        //Disable all interrupts
        __disable_irq();
    
        //Reset microcontroller
        NVIC_SystemReset();
    }
    
}

void ConsoleManager::parseECGCommand()
{
    getNextArgument();
    getNextOption();
    if (isOption("imp_off"))
    {
        HwAbsl::getpHw()->setImpedanceFreq(false);
    }
    else if (isOption("imp_on"))
    {
        HwAbsl::getpHw()->setImpedanceFreq(true);
    }
}

void ConsoleManager::parseTestCommand()
{
    getNextArgument();
    getNextOption();
    
    if (isOption("c100")) //Enter test mode and enables sensors
    {
        //Set in test mode
        m_pConsoleThread->sendIPC(P_SUPER, S_SET_OPERATIONAL_MODE, (uint32_t)eOMtestMode, 0, 0);
        m_pConsoleThread->sendIPC(P_ADC, S_SET_OPERATIONAL_MODE, (uint32_t)eOMtestMode, 0, 0);
        m_pConsoleThread->sendIPC(P_UI, S_SET_OPERATIONAL_MODE, (uint32_t)eOMtestMode, 0, 0);
        m_pConsoleThread->sendIPC(P_FILE, S_SET_OPERATIONAL_MODE, (uint32_t)eOMtestMode, 0, 0);
        
        CONSOLE_PRINT("R100 1\r\n"); //Always success
    }
    else if (isOption("c101"))
    {
        //Ready for POST (clear POST finished flag)
        Th1Super *pSuperTh = reinterpret_cast<Th1Super*>(m_pConsoleThread->getpThread(P_SUPER));
        FileThread *pFileTh = reinterpret_cast<FileThread*>(m_pConsoleThread->getpThread(P_FILE));
        UIThread *pUITh = reinterpret_cast<UIThread*>(m_pConsoleThread->getpThread(P_UI));
        
        pSuperTh->clearPostResult();
        pFileTh->clearPostResult();
        pUITh->clearPostResult();
        HwAbsl::getpHw()->getProbeSerial()->clearPostResult();
        
        //Start POST
        m_pConsoleThread->sendIPC(P_SUPER, S_TEST_COMMAND_POST, 0, 0, 0);
        m_pConsoleThread->sendIPC(P_FILE, S_TEST_COMMAND_POST, 0, 0, 0);
        m_pConsoleThread->sendIPC(P_UI, S_TEST_COMMAND_POST, 0, 0, 0);
        HwAbsl::getpHw()->getProbeSerial()->Post();
        
        //UI thread will report POST result
    }
    else if (isOption("c102"))
    {
        //Empty SD card
        m_pConsoleThread->sendIPC(P_FILE, S_TEST_COMMAND_EMPTY_SD_CARD, 0, 0, 0);
    }
    else if (isOption("c110"))
    {
        m_pConsoleThread->sendIPC(P_ADC, S_TEST_COMMAND_MEASURE_MATERNAL_ECG, 0, 0, 0);
    }
    else if (isOption("c111"))
    {
        m_pConsoleThread->sendIPC(P_ADC, S_TEST_COMMAND_MEASURE_IMPEDANCE, 0, 0, 0);
    }
    else if (isOption("c112"))
    {
        uint16_t fhr = HwAbsl::getpHw()->getProbeSerial()->GetFetalHeartRateFiltered();
        CONSOLE_PRINT("R112 %u\r\n", fhr);
    }
    else if (isOption("c113"))
    {
        uint16_t sigqual = HwAbsl::getpHw()->getProbeSerial()->GetSignalQuality();
        CONSOLE_PRINT("R113 %u\r\n", sigqual);
    }
    else if (isOption("c114"))
    {
        Acceleration_t acc = HwAbsl::getpHw()->getProbeSerial()->GetCurrentAccel();
        CONSOLE_PRINT("R114 %d %d %d\r\n", acc.AccX, acc.AccY, acc.AccZ);
    }
    else if (isOption("c115"))
    {
        float temp = HwAbsl::getpHw()->getProbeSerial()->GetProbeTemp();
        CONSOLE_PRINT("R115 %u\r\n", (uint16_t)temp);
    }
    else if (isOption("c116"))
    {
        HwAbsl::ChargerStatus_t chState = HwAbsl::getpHw()->getChargeStatus();
        if(chState != HwAbsl::eCSTcharging)
        {
            //Charger off. Turn it on
            HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PL_500mA_Charge);
            
            uint8_t tmout = 50;
        
            while(chState != HwAbsl::eCSTcharging && tmout > 0)
            {
                m_pConsoleThread->sleep(100);
                chState = HwAbsl::getpHw()->getChargeStatus();
                tmout--;
                
            }
            
            m_pConsoleThread->sleep(3000);
        }
        
#ifndef NO_FUEL_GAUGE
        Bq27510::ProbeResult result;
        bool success = HwAbsl::getpHw()->getpBq27510()->probe(result);
        CONSOLE_PRINT("R116 %d %d %u\r\n", result.voltage_mV, result.averageCurrent_uA/1000, result.remainingCapacityPercent);
#else
        CONSOLE_PRINT("I2Cbus disabled\r\n");
#endif
        
    }
    else if (isOption("c117"))
    {
        HwAbsl::getpHw()->getProbeSerial()->DopplerSoundMinMaxValuesInit(3000);
        
        uint8_t tmout = 100;
        
        while(HwAbsl::getpHw()->getProbeSerial()->getDopplerSoundMinMaxValuesActive() && tmout > 0)
        {
            m_pConsoleThread->sleep(100);
            tmout--;
        }
        
        int16_t max = HwAbsl::getpHw()->getProbeSerial()->getDopplerSoundMaxValue();
        int16_t min = HwAbsl::getpHw()->getProbeSerial()->getDopplerSoundMinValue();
        
        CONSOLE_PRINT("R117 %d %d\r\n", min-2048, max-2048);
    }
    else if (isOption("c120"))
    {
        int16_t command = getNextOptionalNumericParameter(0, 3);
        m_pConsoleThread->sendIPC(P_SUPER, S_TEST_COMMAND_STATUS_LED, (uint32_t)command, 0, 0);
        CONSOLE_PRINT("R120 %u\r\n", command);
    }
    else if (isOption("c121"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_TEST_COMMAND_DISPLAY, 0, 0, 0);
        CONSOLE_PRINT("R121 1\r\n");
    }
    else if (isOption("c122"))
    {
        int16_t command = getNextOptionalNumericParameter(0, 5);
        m_pConsoleThread->sendIPC(P_SUPER, S_TEST_COMMAND_SOUND, (uint32_t)command, 0, 0);
        CONSOLE_PRINT("R122 %u\r\n", command);
    }
    else if (isOption("c123"))
    {
        int16_t perc = getNextOptionalNumericParameter(0, 100);
        float dc = (float)perc / 100.0 * (float)DC_100_PERC;
        HwAbsl::getpHw()->setDutycycleBackLight((uint16_t)dc);
        CONSOLE_PRINT("R123 %u\r\n", perc);
    }  
    else if (isOption("c124"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_TEST_COMMAND_KEY_TEST, 0, 0, 0);
    }
    else if (isOption("ledred"))
    {
        HwAbsl::getpHw()->setGreenLED(false);
        HwAbsl::getpHw()->setRedLED(true);
    }
    else if (isOption("ledyellow"))
    {
        HwAbsl::getpHw()->setGreenLED(GREEN_LED_REDUCED_PWM);
        HwAbsl::getpHw()->setRedLED(RED_LED_REDUCED_PWM);
    }
    else if (isOption("ledgreen"))
    {
        HwAbsl::getpHw()->setGreenLED(true);
        HwAbsl::getpHw()->setRedLED(false);
    }
    else if (isOption("ledoff"))
    {
        HwAbsl::getpHw()->setGreenLED(false);
        HwAbsl::getpHw()->setRedLED(false);
    }
    else if (isOption("soundalarm"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_TEST_COMMAND_SOUND, (uint32_t)eSTCPlayMedPriAlarm, 0, 0);
        
    }
    else if (isOption("soundsoftalarm"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_TEST_COMMAND_SOUND, (uint32_t)eSTCPlayLowPriAlarm, 0, 0);
    }
    else if (isOption("soundsine"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_TEST_COMMAND_SOUND, (uint32_t)eSTCPlaysine1000, 0, 0);
    }
    else if (isOption("soundoff"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_TEST_COMMAND_SOUND, (uint32_t)eSTCStop, 0, 0);
    }
    else if (isOption("display"))
    {
    }
    else if (isOption("backlight"))
    {
    }
    else if (isOption("sdcard"))
    {
    }
    else if (isOption("getpost"))
    {
        Th1Super *pSuperTh = reinterpret_cast<Th1Super*>(m_pConsoleThread->getpThread(P_SUPER));
        FileThread *pFileTh = reinterpret_cast<FileThread*>(m_pConsoleThread->getpThread(P_FILE));
        UIThread *pUITh = reinterpret_cast<UIThread*>(m_pConsoleThread->getpThread(P_UI));
        
        Post_t overAllPostResult;
        overAllPostResult.postWord = 0;
        
        POST_ctrl_t postResult;
        
        //Get supervisor post results
        postResult = pSuperTh->getPostResult();
        if(postResult.finished)
        {
            overAllPostResult.postBits.fuelGaugeComm = postResult.post.postBits.fuelGaugeComm;
        }
        else
        {
            overAllPostResult.postBits.fuelGaugeComm = 1; //Report as failed
        }
        
        //Get File post results
        postResult = pFileTh->getPostResult();
        if(postResult.finished)
        {
            overAllPostResult.postBits.sdCard = postResult.post.postBits.sdCard;
        }
        else
        {
            overAllPostResult.postBits.sdCard = 1; //Report as failed
        }
        
        //Get UI post results
        postResult = pUITh->getPostResult();
        if(postResult.finished)
        {
            overAllPostResult.postBits.displayComm = postResult.post.postBits.displayComm;
            overAllPostResult.postBits.calibrationCheckSum = postResult.post.postBits.calibrationCheckSum;
        }
        else
        {
            overAllPostResult.postBits.displayComm = 1;
            overAllPostResult.postBits.calibrationCheckSum = 1; //Report as failed
        }
        
        //Get probe post result
        postResult = HwAbsl::getpHw()->getProbeSerial()->getPostResult();
        if(postResult.finished)
        {
            overAllPostResult.postBits.ultraSoundTransComm = postResult.post.postBits.ultraSoundTransComm;
        }
        else
        {
            overAllPostResult.postBits.ultraSoundTransComm = 1; //Report as failed
        }
        
        //Get program checksum post
        postResult = HwAbsl::getpHw()->getPostResult();
        if(postResult.finished)
        {
            overAllPostResult.postBits.programCheckSum = postResult.post.postBits.programCheckSum;
        }
        else
        {
            overAllPostResult.postBits.programCheckSum = 1; //Report as failed
        }
        
        CONSOLE_PRINT("\r\nPOST result:%X\r\n", overAllPostResult.postWord);
        
    }
    
            
}

void ConsoleManager::parseRTCcommand()
{
    getNextArgument();
    getNextOption();
    if (isOption("time"))
    {
        LL_RTC_TimeTypeDef m_time = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
        CONSOLE_PRINT("\r\nCurrent time:%0.2u:%0.2u:%0.2u\r\n", m_time.Hours, m_time.Minutes, m_time.Seconds);
    }
    else if (isOption("date"))
    {
        LL_RTC_DateTypeDef m_date = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
        CONSOLE_PRINT("\r\nCurrent date:%0.2u/%0.2u-20%0.2u\r\n", m_date.Day, m_date.Month, m_date.Year);
    }
    else if (isOption("setdate"))
    {
        LL_RTC_DateTypeDef d;
    
        d = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
        CONSOLE_PRINT("\r\nCurrent date:%0.2u/%0.2u-20%0.2u\r\n", d.Day, d.Month, d.Year);
        
        d.Day = getNextOptionalNumericParameter(1, 31);
        d.Month = getNextOptionalNumericParameter(1, 12);
        d.Year = getNextOptionalNumericParameter(20, 99);
        HwAbsl::getpHw()->getpRtc()->RTCsetDate(d);
        
        d = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
        CONSOLE_PRINT("\r\nNew date:%0.2u/%0.2u-20%0.2u\r\n", d.Day, d.Month, d.Year);
    }
    else if (isOption("settime"))
    {
        LL_RTC_TimeTypeDef t;
    
        t = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
        CONSOLE_PRINT("\r\nCurrent time:%0.2u:%0.2u:%0.2u\r\n", t.Hours, t.Minutes, t.Seconds);
        
        t.Hours = getNextOptionalNumericParameter(0, 23);
        t.Minutes = getNextOptionalNumericParameter(0, 59);
        t.Seconds = getNextOptionalNumericParameter(0, 59);
        HwAbsl::getpHw()->getpRtc()->RTCsetTime(t);
        
        t = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
        CONSOLE_PRINT("\r\nNew time:%0.2u:%0.2u:%0.2u\r\n", t.Hours, t.Minutes, t.Seconds);

    }
    
}    

void ConsoleManager::parseAdcCommand()
{
    getNextArgument();
    getNextOption();

    if (isOption("debug_on"))
    {
        m_pConsoleThread->sendIPC(P_ADC, S_ADC_DEBUG, 1,0,0 );
    }
    else if (isOption("debug_off"))
    {
        m_pConsoleThread->sendIPC(P_ADC, S_ADC_DEBUG, 0,0,0 );
    }
}
  
void ConsoleManager::parsePowerCommand()
{
    getNextArgument();
    getNextOption();

    if (isOption("poweroff"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_SYSTEM_SHUTDOWN, (uint32_t)eSDRoffBtn, 0 ,0);
        
    }
    else if (isOption("trans_off"))
    {
        HwAbsl::getpHw()->transducerPower(false);
        CONSOLE_PRINT("\r\nTransducer power off\r\n");
    }
    else if (isOption("trans_on"))
    {
        HwAbsl::getpHw()->transducerPower(true);
        CONSOLE_PRINT("\r\nTransducer power on\r\n");
    }
    else if (isOption("5v_on"))
    {
        HwAbsl::getpHw()->enable5V(true);
        CONSOLE_PRINT("\r\n5V on\r\n");
    }
    else if (isOption("5v_off"))
    {
        HwAbsl::getpHw()->enable5V(false);
        CONSOLE_PRINT("\r\n5V off\r\n");
    }
    else if (isOption("USB_100"))
    {
        HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PM_100mA_noCharge);
        CONSOLE_PRINT("\r\nUSB current limit = 100mA\r\n");
    }
    else if (isOption("USB_500"))
    {
        HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_PL_500mA_Charge);
        CONSOLE_PRINT("\r\nUSB current limit = 100mA\r\n");
    }
    else if (isOption("USB_1000"))
    {
        HwAbsl::getpHw()->setUSBcurrentMode(HwAbsl::eUSB_1000mA_Charge);
        CONSOLE_PRINT("\r\nUSB current limit = 1000mA\r\n");
    }
    else if (isOption("amb_on"))
    {
        HwAbsl::getpHw()->enableAmbLightSensor(true);
        CONSOLE_PRINT("\r\nAmb light sensor on\r\n");
    }
    else if (isOption("amb_off"))
    {
        HwAbsl::getpHw()->enableAmbLightSensor(false);
        CONSOLE_PRINT("\r\nAmb light sensor off\r\n");
    }
    
}

void ConsoleManager::parseFileTestCommand()
{
    getNextArgument();
    getNextOption();

    if (isOption("close"))
    {
        uint16_t ve = getNextOptionalNumericParameter(0, 1); //0 = invalid, 1 = valid
        uint16_t ke = getNextOptionalNumericParameter(0, 1); //0 = delete, 1 = keep
        m_pConsoleThread->sendIPC(P_FILE, S_FILE_CLOSE_EPISODE, ve, ke, 0 );
    }
    else if (isOption("create"))
    {
        m_pConsoleThread->sendIPC(P_FILE, S_FILE_CREATE_EPISODE, 0,0,0 );
    }
    else if (isOption("delold"))
    {
        m_pConsoleThread->sendIPC(P_FILE, S_FILE_DELETE_OLDEST_EPISODE, 0,0,0 );
    }
    else if (isOption("list"))
    {
        m_pConsoleThread->sendIPC(P_FILE, S_FILE_LIST_EPISODES, 0,0,0 );
    }
    else if (isOption("read-file"))
    {
        m_pConsoleThread->sendIPC(P_FILE, S_FILE_READ_FILE, 0,0,0 );
    }
    
}

void ConsoleManager::parseBatteryCommand()
{
    getNextArgument();
    getNextOption();

    if (isOption("debugon"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_BATTERY_DEBUG_OUT, 1,0,0 );
    }
    else if (isOption("debugoff"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_BATTERY_DEBUG_OUT, 0,0,0 );
    }
    else if (isOption("nac"))
    {
       
#ifndef NO_FUEL_GAUGE
        uint16_t val;
        bool succ = HwAbsl::getpHw()->getpBq27510()->getNomAvailableCapacity(val);
        if(succ)
        {
            CONSOLE_PRINT("\r\nBattery nominal available capacity [mAh]: %u\r\n", val); 
        }
        else
        {
            CONSOLE_PRINT("\r\nBattery nominal available capacity [mAh]: %u - Transmission fail!\r\n", val); 
        }
#else
        CONSOLE_PRINT("I2Cbus disabled\r\n");
#endif

    }
    else if (isOption("fac"))
    {
#ifndef NO_FUEL_GAUGE
        uint16_t val;
        bool succ = HwAbsl::getpHw()->getpBq27510()->getFullAvailableCapacity(val);
        if(succ)
        {
            CONSOLE_PRINT("\r\nBattery full available capacity [mAh]: %u\r\n", val); 
        }
        else
        {
            CONSOLE_PRINT("\r\nBattery full available capacity [mAh]: %u - Transmission fail!\r\n", val); 
        }
#else
        CONSOLE_PRINT("I2Cbus disabled\r\n");
#endif

    }
    else if (isOption("setrc"))
    {
        uint16_t rp = getNextOptionalNumericParameter(0, 100); //0 to 100%
        Config::getpConfig()->SaveConfig(Config::eCfgLastBattRemCharge, rp);
        m_pConsoleThread->sendIPC(P_UI, S_BATTERY_REMAINING_CAPACITY,rp,0,0 );
        m_pConsoleThread->sendIPC(P_SUPER, S_BATTERY_REMAINING_CAPACITY,rp,0,0 );
        CONSOLE_PRINT("\r\nLast battery remaining time: %u\r\n", rp); 
    }
    
    
}

void ConsoleManager::parseAudioCommand()
{
    FHRMAudio * pAudio = HwAbsl::getpHw()->getpAudio();
    if (0==pAudio)
    {
        return;
    }

    getNextArgument();
    getNextOption();

    if (isOption("off"))
    {
        HwAbsl::getpHw()->audioOff();
        CONSOLE_PRINT("\r\nAudio off\r\n");
    }
    else if (isOption("on"))
    {
        HwAbsl::getpHw()->audioOn();
        CONSOLE_PRINT("\r\nAudio on\r\n");
    }
    else if (isOption("s1"))
    {
        HwAbsl::getpHw()->getpAudio()->audioOn();
        HwAbsl::getpHw()->getpAudio()->ProbeSoundEnable(false);
        HwAbsl::getpHw()->getpAudio()->playNormalStartupSound();
        CONSOLE_PRINT("\r\nPlaying alarm1\r\n");
        m_pConsoleThread->sleep(3000);
        HwAbsl::getpHw()->getpAudio()->ProbeSoundEnable(true);
        
    }
    else if (isOption("sin200"))
    {
        HwAbsl::getpHw()->getpAudio()->audioOn();
        HwAbsl::getpHw()->enable5V(true);
        HwAbsl::getpHw()->getpAudio()->playSin200();
        CONSOLE_PRINT("\r\nPlaying sine 200Hz\r\n");
        
    }
    else if (isOption("sin1000"))
    {
        HwAbsl::getpHw()->getpAudio()->audioOn();
        HwAbsl::getpHw()->enable5V(true);
        HwAbsl::getpHw()->getpAudio()->playSin1000();
        CONSOLE_PRINT("\r\nPlaying sine 1000Hz\r\n");
        
    }
    else if (isOption("stop"))
    {
        HwAbsl::getpHw()->getpAudio()->stopPlayback();
        CONSOLE_PRINT("\r\nStop playback\r\n");
        
    }
    
    else if (isOption("debugon"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_PROBE_SOUND_DEBUG, 1, 0, 0);  
    }
    else if (isOption("debugoff"))
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_PROBE_SOUND_DEBUG, 0, 0, 0);  
    }

}

void ConsoleManager::parseDisplayCommand()
{
    getNextArgument();
    getNextOption();

    if (isOption("redledoff"))
    {
        HwAbsl::getpHw()->setRedLED(false);
        CONSOLE_PRINT("\r\nRed LED off\r\n");
    }
    else if (isOption("redledon"))
    {
        HwAbsl::getpHw()->setRedLED(true);
        CONSOLE_PRINT("\r\nRed LED on\r\n");
    }
    else if (isOption("greenledoff"))
    {
        CONSOLE_PRINT("\r\nGreen LED off\r\n");
        HwAbsl::getpHw()->setGreenLED(false);
    }
    else if (isOption("greenledon"))
    {
        HwAbsl::getpHw()->setGreenLED(true);
        CONSOLE_PRINT("\r\nGreen LED on\r\n");
    }
    else if (isOption("getgled"))
    {
        CONSOLE_PRINT("\r\nGreen LED value (const)%u\r\n", GREEN_LED_REDUCED_PWM);
    }
    else if (isOption("getrled"))
    {
        CONSOLE_PRINT("\r\nRed LED value (const)%u\r\n", RED_LED_REDUCED_PWM);
    }
    else if (isOption("setgled"))
    {
        uint16_t pwm = getNextOptionalNumericParameter(0, DC_100_PERC);
        HwAbsl::getpHw()->setGreenLED(pwm);
        
        CONSOLE_PRINT("\r\nGreen LED value: %u (min:%u, max:%u)\r\n", pwm, 0, DC_100_PERC);
    }
    else if (isOption("setrled"))
    {
        uint16_t pwm = getNextOptionalNumericParameter(0, DC_100_PERC);
        HwAbsl::getpHw()->setRedLED(pwm);
        
        CONSOLE_PRINT("\r\nRed LED value: %u (min:%u, max:%u)\r\n", pwm, 0, DC_100_PERC);
    }
    else if (isOption("fhralon"))
    {
        m_pConsoleThread->sendIPC(P_UI, S_SET_FHR_ATYPICAL_ALARM,1,0,0 );
    }
    else if (isOption("fhraloff"))
    {
        m_pConsoleThread->sendIPC(P_UI, S_SET_FHR_ATYPICAL_ALARM,0,0,0 );
    }
    else if (isOption("dh"))
    {
        uint8_t xs = getNextOptionalNumericParameter(0, 300);
        uint8_t xe = getNextOptionalNumericParameter(0, 300);
        m_pConsoleThread->sendIPC(P_UI, S_DEBUG_DRAW_HOR_LINE,xs,xe,0);
    }
    else if (isOption("dv"))
    {
        uint8_t ys = getNextOptionalNumericParameter(0, 300);
        uint8_t ye = getNextOptionalNumericParameter(0, 300);
        m_pConsoleThread->sendIPC(P_UI, S_DEBUG_DRAW_VERT_LINE,ys,ye,0);
    }
}


void ConsoleManager::parseCFcommand()
{
    getNextArgument();
    getNextOption();
    
    if (isOption("c001")) //Set default configuration values
    {
        if(Config::getpConfig()->SetConfigDefault())
        {
            CONSOLE_PRINT("R001 1\r\n");
        }
        else
        {
            CONSOLE_PRINT("R001 0\r\n");
        }
        
        uint32_t val;
        
        val = Config::getpConfig()->GetConfig(Config::eCfgEpisodevalidTime);
        m_pConsoleThread->sendIPC(P_UI, S_EPISODE_VALID_TIME_UPDATE, val, 0, 0);
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgEndOfEpisodeTimeout);
        m_pConsoleThread->sendIPC(P_UI, S_END_OF_EPISODE_TIME_UPDATE, val, 0, 0); 
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgHRupdateTime);
        m_pConsoleThread->sendIPC(P_UI, S_HR_DISPUPDATE_TIME_UPDATE, val, 0, 0);  
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgHRavgBufLen);
        m_pConsoleThread->sendIPC(P_UI, S_HR_BUF_LEN_UPDATE, val, 0, 0);   
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgLostSignalAlarmTime);
        m_pConsoleThread->sendIPC(P_UI, S_LOST_SIG_AL_TIME_UPDATE, val, 0, 0);
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmHigh);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ATYPICAL_ALARM_HIGH_UPDATE, val, 0, 0);   
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmLow);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ATYPICAL_ALARM_LOW_UPDATE, val, 0, 0);   
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgShowClock);
        m_pConsoleThread->sendIPC(P_UI, S_ENABLE_CLOCK_UPDATE,val,0,0 );
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgResearchStorageMode);
        m_pConsoleThread->sendIPC(P_UI, S_ENABLE_RSM_UPDATE, val,0,0 );
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgEpisodeTooShortTime);
        m_pConsoleThread->sendIPC(P_UI, S_EP_TOO_SHORT_TIME_UPDATE, val,0,0 );
        m_pConsoleThread->sleep(10);
        
        m_pConsoleThread->sendIPC(P_UI, S_DEV_ID_UPDATE, 0, 0, 0);   
        m_pConsoleThread->sleep(10);
        
        float valf = Config::getpConfig()->GetConfigFloat(Config::eCfgEcgAmpSens);
        uint32_t *pfv = (uint32_t*)&valf;        //convert from float to uint
        m_pConsoleThread->sendIPC(P_UI, S_ECG_AMP_SENS_UPDATE, static_cast<uint32_t>(*pfv), 0, 0);   
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmHigh);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ALARM_ABNORMAL_HIGH_UPDATE, val, 0, 0);
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmLow);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ALARM_ABNORMAL_LOW_UPDATE, val, 0, 0);
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmTime);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ALARM_ATYPICAL_TIME_UPDATE, val, 0, 0);   
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmTime);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ALARM_ABNORMAL_TIME_UPDATE, val, 0, 0);  
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgUseSDcard);
        m_pConsoleThread->sendIPC(P_UI, S_ENABLE_SD_CARD_UPDATE,val,0,0 );
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgDopplerSpeakerGain);
        m_pConsoleThread->sendIPC(P_UI, S_DOPPLER_SOUND_SPEAKER_GAIN_UPDATE,val,0,0 );
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgDopplerSoundGain);
        m_pConsoleThread->sendIPC(P_UI, S_PROBE_SOUND_GAIN_UPDATE,val,0,0 );
        m_pConsoleThread->sleep(10);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgUseFHRfilter);
        m_pConsoleThread->sendIPC(P_UI, S_ENABLE_FHR_FILTER_UPDATE,val,0,0 );
        m_pConsoleThread->sleep(10);
        
    }
    
    else if (isOption("c002")) //time
    {
        LL_RTC_TimeTypeDef m_time = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
        CONSOLE_PRINT("R002 %0.2u:%0.2u\r\n", m_time.Hours, m_time.Minutes);
    }
    
    else if (isOption("c003")) //date
    {
        LL_RTC_DateTypeDef m_date = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
        CONSOLE_PRINT("R003 %0.2u.%0.2u.20%0.2u\r\n", m_date.Day, m_date.Month, m_date.Year);
    }
    else if (isOption("c004")) //setdate
    {
        LL_RTC_DateTypeDef d;
    
        d.Day = getNextOptionalNumericParameter(1, 31);
        d.Month = getNextOptionalNumericParameter(1, 12);
        d.Year = getNextOptionalNumericParameter(20, 99);
        HwAbsl::getpHw()->getpRtc()->RTCsetDate(d);
        
        m_pConsoleThread->sendIPC(P_UI, S_DATE_UPDATE, 0, 0, 0);
        
        LL_RTC_DateTypeDef m_date = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
        CONSOLE_PRINT("R003 %0.2u.%0.2u.20%0.2u\r\n", m_date.Day, m_date.Month, m_date.Year);
    }
    else if (isOption("c005")) //settime
    {
        LL_RTC_TimeTypeDef t;
    
        t.Hours = getNextOptionalNumericParameter(0, 23);
        t.Minutes = getNextOptionalNumericParameter(0, 59);
        t.Seconds = getNextOptionalNumericParameter(0, 59);
        HwAbsl::getpHw()->getpRtc()->RTCsetTime(t);
        
        m_pConsoleThread->sendIPC(P_UI, S_TIME_UPDATE, 0, 0, 0);
        
        t = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
        CONSOLE_PRINT("R002 %0.2u:%0.2u\r\n", t.Hours, t.Minutes);
    }
    else if (isOption("c006")) //setepvtime
    {
        uint16_t time = getNextOptionalNumericParameter(CFG_MIN_VALID_EP_TIME, CFG_MAX_VALID_EP_TIME);
        Config::getpConfig()->SaveConfig(Config::eCfgEpisodevalidTime, time);
        m_pConsoleThread->sendIPC(P_SUPER, S_EPISODE_VALID_TIME_UPDATE, time, 0, 0);
        m_pConsoleThread->sendIPC(P_UI, S_EPISODE_VALID_TIME_UPDATE, time, 0, 0);
        
        time = Config::getpConfig()->GetConfig(Config::eCfgEpisodevalidTime);
        CONSOLE_PRINT("R007 %u\r\n", time); 
    }
    else if (isOption("c007")) //getepvtime
    {
        uint16_t time = Config::getpConfig()->GetConfig(Config::eCfgEpisodevalidTime);
        CONSOLE_PRINT("R007 %u\r\n", time);   
    }
    else if (isOption("c008")) //setendeptime
    {
        uint32_t time = getNextOptionalNumericParameter(CFG_MIN_END_OF_EPISODE_TIME, CFG_MAX_END_OF_EPISODE_TIME);
        Config::getpConfig()->SaveConfig(Config::eCfgEndOfEpisodeTimeout, time);
        m_pConsoleThread->sendIPC(P_SUPER, S_END_OF_EPISODE_TIME_UPDATE, time, 0, 0); 
        m_pConsoleThread->sendIPC(P_UI, S_END_OF_EPISODE_TIME_UPDATE, time, 0, 0); 
        
        time = Config::getpConfig()->GetConfig(Config::eCfgEndOfEpisodeTimeout);
        CONSOLE_PRINT("R009 %u\r\n", time);   
    }
    else if (isOption("c009")) //getendeptime
    {
        uint16_t time = Config::getpConfig()->GetConfig(Config::eCfgEndOfEpisodeTimeout);
        CONSOLE_PRINT("R009 %u\r\n", time);   
    }
    else if (isOption("c010")) //sethrupdtime
    {
        uint16_t time = getNextOptionalNumericParameter(CFG_MIN_MHR_UPDATE_TIME, CFG_MAX_MHR_UPDATE_TIME);
        Config::getpConfig()->SaveConfig(Config::eCfgHRupdateTime, time);
        m_pConsoleThread->sendIPC(P_ADC, S_HR_DISPUPDATE_TIME_UPDATE, time, 0, 0);
        m_pConsoleThread->sendIPC(P_UI, S_HR_DISPUPDATE_TIME_UPDATE, time, 0, 0);  
        
        time = Config::getpConfig()->GetConfig(Config::eCfgHRupdateTime);
        CONSOLE_PRINT("R011 %u\r\n", time);   
    }
    else if (isOption("c011")) //gethrupdtime
    {
        uint16_t time = Config::getpConfig()->GetConfig(Config::eCfgHRupdateTime);
        CONSOLE_PRINT("R011 %u\r\n", time);   
    }
    else if (isOption("c012")) //sethravgbl
    {
        uint16_t len = getNextOptionalNumericParameter(CFG_MIN_MHR_AVG_BUF_LEN, CFG_MAX_MHR_AVG_BUF_LEN);
        Config::getpConfig()->SaveConfig(Config::eCfgHRavgBufLen, len);
        m_pConsoleThread->sendIPC(P_ADC, S_HR_BUF_LEN_UPDATE, len, 0, 0);   
        m_pConsoleThread->sendIPC(P_UI, S_HR_BUF_LEN_UPDATE, len, 0, 0);   
        
        len = Config::getpConfig()->GetConfig(Config::eCfgHRavgBufLen);
        CONSOLE_PRINT("R013 %u\r\n", len);   
    }
    else if (isOption("c013")) //gethravgbl
    {
        uint16_t len = Config::getpConfig()->GetConfig(Config::eCfgHRavgBufLen);
        CONSOLE_PRINT("R013 %u\r\n", len);   
    }
    else if (isOption("c014")) //setlostsigalt
    {
        uint16_t time = getNextOptionalNumericParameter(CFG_MIN_LOST_SIGNAL_ALARM_TIME, CFG_MAX_LOST_SIGNAL_ALARM_TIME);
        Config::getpConfig()->SaveConfig(Config::eCfgLostSignalAlarmTime, time);
        m_pConsoleThread->sendIPC(P_SUPER, S_LOST_SIG_AL_TIME_UPDATE, time, 0, 0);   
        m_pConsoleThread->sendIPC(P_UI, S_LOST_SIG_AL_TIME_UPDATE, time, 0, 0);
        
        time = Config::getpConfig()->GetConfig(Config::eCfgLostSignalAlarmTime);
        CONSOLE_PRINT("R015 %u\r\n", time);
    }
    else if (isOption("c015")) //getlostsigalt
    {
        uint16_t time = Config::getpConfig()->GetConfig(Config::eCfgLostSignalAlarmTime);
        CONSOLE_PRINT("R015 %u\r\n", time);   
    }
    else if (isOption("c016")) //Set FHR atypical alarm high
    {
        uint16_t limit = getNextOptionalNumericParameter(CFG_MIN_ATYPICAL_FHR_ALARM_HIGH, CFG_MAX_ATYPICAL_FHR_ALARM_HIGH);
        Config::getpConfig()->SaveConfig(Config::eCfgFhrAtypicalAlarmHigh, limit);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ATYPICAL_ALARM_HIGH_UPDATE, limit, 0, 0);   
        
        limit = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmHigh);
        CONSOLE_PRINT("R017 %u\r\n", limit);   
    }
    else if (isOption("c017")) //Get FHR atypical alarm high
    {
        uint16_t limit = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmHigh);
        CONSOLE_PRINT("R017 %u\r\n", limit);   
    }
    else if (isOption("c018")) //Set FHR atypical alarm low
    {
        uint16_t limit = getNextOptionalNumericParameter(CFG_MIN_ATYPICAL_FHR_ALARM_LOW, CFG_MAX_ATYPICAL_FHR_ALARM_LOW);
        Config::getpConfig()->SaveConfig(Config::eCfgFhrAtypicalAlarmLow, limit);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ATYPICAL_ALARM_LOW_UPDATE, limit, 0, 0);   
        
        limit = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmLow);
        CONSOLE_PRINT("R019 %u\r\n", limit);  
    }
    else if (isOption("c019")) //Get FHR atypical alarm low
    {
        uint16_t limit = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmLow);
        CONSOLE_PRINT("R019 %u\r\n", limit);   
    }
    else if (isOption("c020")) //setshowclk
    {
        uint16_t show = getNextOptionalNumericParameter(CFG_DISABLED, CFG_ENABLED);
        Config::getpConfig()->SaveConfig(Config::eCfgShowClock, show);
        m_pConsoleThread->sendIPC(P_UI, S_ENABLE_CLOCK_UPDATE,show,0,0 );
        
        show = Config::getpConfig()->GetConfig(Config::eCfgShowClock);
        CONSOLE_PRINT("R021 %u\r\r\n", show);
    }
    else if (isOption("c021")) //getshowclk
    {
        uint16_t show = Config::getpConfig()->GetConfig(Config::eCfgShowClock);
        CONSOLE_PRINT("R021 %u\r\n", show);   
    }
    else if (isOption("c022")) //setrsm
    {
        //0 = research storage mode off, 1 = research storage mode on
        uint16_t val = getNextOptionalNumericParameter(CFG_DISABLED, CFG_ENABLED); 
        Config::getpConfig()->SaveConfig(Config::eCfgResearchStorageMode, val);
        m_pConsoleThread->sendIPC(P_FILE, S_ENABLE_RSM_UPDATE, val,0,0 );
        m_pConsoleThread->sendIPC(P_UI, S_ENABLE_RSM_UPDATE, val,0,0 );
        
        val = Config::getpConfig()->GetConfig(Config::eCfgResearchStorageMode);
        CONSOLE_PRINT("R023 %u\r\n", val);   
    }
    else if (isOption("c023")) //getrsm
    {
        uint16_t val = Config::getpConfig()->GetConfig(Config::eCfgResearchStorageMode);
        CONSOLE_PRINT("R023 %u\r\n", val);   
    }
    else if (isOption("c024")) //geteptstime
    {
        uint16_t val = Config::getpConfig()->GetConfig(Config::eCfgEpisodeTooShortTime);
        CONSOLE_PRINT("R024 %u\r\n", val);   
    }
    else if (isOption("c025")) //seteptstime
    {
        uint16_t val = getNextOptionalNumericParameter(CFG_MIN_EP_TOO_SHORT_TIME, CFG_MAX_EP_TOO_SHORT_TIME); 
        Config::getpConfig()->SaveConfig(Config::eCfgEpisodeTooShortTime, val);
        m_pConsoleThread->sendIPC(P_SUPER, S_EP_TOO_SHORT_TIME_UPDATE, val,0,0 );
        m_pConsoleThread->sendIPC(P_UI, S_EP_TOO_SHORT_TIME_UPDATE, val,0,0 );
        
        val = Config::getpConfig()->GetConfig(Config::eCfgEpisodeTooShortTime);
        CONSOLE_PRINT("R024 %u\r\n", val);
    }
    else if (isOption("c026")) //setdevid
    {
        std::string devId = getNextArgument();
        if(devId != "")
        {
            transform(devId.begin(), devId.end(), devId.begin(), toupper);
            Config::getpConfig()->SaveConfig(Config::eCfgDeviceIdentifier, devId);
            
            devId = Config::getpConfig()->GetConfigStr(Config::eCfgDeviceIdentifier);
            m_pConsoleThread->sendIPC(P_UI, S_DEV_ID_UPDATE, 0, 0, 0);   
            
            CONSOLE_PRINT("R027 %s\r\n",devId.c_str());
        }
    }
    else if (isOption("c027")) //getdevid
    {
        std::string devId = Config::getpConfig()->GetConfigStr(Config::eCfgDeviceIdentifier);
        CONSOLE_PRINT("R027 %s\r\n",devId.c_str());
    }
    else if (isOption("c028")) //setampsens
    {
        std::string val = getNextParameter();
        float valf = strtod(val.c_str(), 0);
        uint32_t *pfv = (uint32_t*)&valf;        //convert from float to uint
        if(valf > CFG_MIN_AMPSENSE && valf < CFG_MAX_AMPSENSE)
        {
            Config::getpConfig()->SaveConfig(Config::eCfgEcgAmpSens, static_cast<uint32_t>(*pfv));
            m_pConsoleThread->sendIPC(P_ADC, S_ECG_AMP_SENS_UPDATE, static_cast<uint32_t>(*pfv), 0, 0);
            m_pConsoleThread->sendIPC(P_UI, S_ECG_AMP_SENS_UPDATE, static_cast<uint32_t>(*pfv), 0, 0);   
            
            float valf = Config::getpConfig()->GetConfigFloat(Config::eCfgEcgAmpSens);
            CONSOLE_PRINT("R029 %f\r\n", valf);   
        }
    }
    else if (isOption("c029")) //getampsens
    {
        float valf = Config::getpConfig()->GetConfigFloat(Config::eCfgEcgAmpSens);
        CONSOLE_PRINT("R029 %f\r\n", valf);   
    }
    else if (isOption("c030")) //set FHR abnormal alarm high
    {
        uint16_t limit = getNextOptionalNumericParameter(CFG_MIN_ABNORMAL_FHR_ALARM_HIGH, CFG_MAX_ABNORMAL_FHR_ALARM_HIGH);
        Config::getpConfig()->SaveConfig(Config::eCfgFhrAbnormalAlarmHigh, limit);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ALARM_ABNORMAL_HIGH_UPDATE, limit, 0, 0);

        limit = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmHigh);
        CONSOLE_PRINT("R031 %u\r\n", limit);   
    }
    else if (isOption("c031")) //get FHR abnormal alarm high
    {
        uint16_t limit = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmHigh);
        CONSOLE_PRINT("R031 %u\r\n", limit);
    }
    else if (isOption("c032")) //set FHR abnormal alarm low
    {
        uint16_t limit = getNextOptionalNumericParameter(CFG_MIN_ABNORMAL_FHR_ALARM_LOW, CFG_MAX_ABNORMAL_FHR_ALARM_LOW);
        Config::getpConfig()->SaveConfig(Config::eCfgFhrAbnormalAlarmLow, limit);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ALARM_ABNORMAL_LOW_UPDATE, limit, 0, 0);
    
        limit = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmLow);
        CONSOLE_PRINT("R033 %u\r\n", limit);
    }
    else if (isOption("c033")) //get FHR abnormal alarm low
    {
        uint16_t limit = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmLow);
        CONSOLE_PRINT("R033 %u\r\n", limit);

    }
    else if (isOption("c034")) //Set FHR atypical alarm time
    {
        uint16_t time = getNextOptionalNumericParameter(CFG_MIN_ATYPICAL_FHR_ALARM_TIME, CFG_MAX_ATYPICAL_FHR_ALARM_TIME);
        Config::getpConfig()->SaveConfig(Config::eCfgFhrAtypicalAlarmTime, time);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ALARM_ATYPICAL_TIME_UPDATE, time, 0, 0);   

        time = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmTime);
        CONSOLE_PRINT("R035 %u\r\n", time);
    }
    else if (isOption("c035")) //Get FHR atypical alarm time
    {
        uint16_t time = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmTime);
        CONSOLE_PRINT("R035 %u\r\n", time);
    }
    else if (isOption("c036")) //Set FHR abnormal alarm time
    {
        uint16_t time = getNextOptionalNumericParameter(CFG_MIN_ABNORMAL_FHR_ALARM_TIME, CFG_MAX_ABNORMAL_FHR_ALARM_TIME);
        Config::getpConfig()->SaveConfig(Config::eCfgFhrAbnormalAlarmTime, time);
        m_pConsoleThread->sendIPC(P_UI, S_FHR_ALARM_ABNORMAL_TIME_UPDATE, time, 0, 0);  
    
        time = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmTime);
        CONSOLE_PRINT("R037 %u\r\n", time);
    }
    else if (isOption("c037")) //Get FHR abnormal alarm time
    {
        uint16_t time = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmTime);
        CONSOLE_PRINT("R037 %u\r\n", time);  
    }
    else if (isOption("c038")) //Get Device type
    {
        CONSOLE_PRINT("R038 MOYO\r\n");  
    }
    else if (isOption("c039")) //Enable/disable SD card functionality
    {
        uint16_t enDis = getNextOptionalNumericParameter(CFG_DISABLED, CFG_ENABLED);
        Config::getpConfig()->SaveConfig(Config::eCfgUseSDcard, enDis);
        m_pConsoleThread->sendIPC(P_FILE, S_ENABLE_SD_CARD_UPDATE,enDis,0,0 );
        m_pConsoleThread->sendIPC(P_UI, S_ENABLE_SD_CARD_UPDATE,enDis,0,0 );
        
        enDis = Config::getpConfig()->GetConfig(Config::eCfgUseSDcard);
        CONSOLE_PRINT("R040 %u\n", enDis); 
    }
    else if (isOption("c040")) //Get enable/disable SD card functionality
    {
        uint16_t enDis = Config::getpConfig()->GetConfig(Config::eCfgUseSDcard);
        CONSOLE_PRINT("R040 %u\r\n", enDis);   
    }
    else if (isOption("c041")) //Set Doppler sound speaker gain
    {
        uint16_t gain = getNextOptionalNumericParameter(CFG_MIN_DOPPLER_SOUND_SPEAKER_GAIN, CFG_MAX_DOPPLER_SOUND_SPEAKER_GAIN);
        Config::getpConfig()->SaveConfig(Config::eCfgDopplerSpeakerGain, gain);
        HwAbsl::getpHw()->getProbeSerial()->SetDopplerSoundSpeakerGain((uint8_t)gain);
        m_pConsoleThread->sendIPC(P_UI, S_DOPPLER_SOUND_SPEAKER_GAIN_UPDATE,gain,0,0 );
        
        gain = Config::getpConfig()->GetConfig(Config::eCfgDopplerSpeakerGain);
        CONSOLE_PRINT("R042 %u\r\n", gain); 
    }
    else if (isOption("c042")) //Get Doppler sound speaker gain
    {
        uint16_t gain = Config::getpConfig()->GetConfig(Config::eCfgDopplerSpeakerGain);
        CONSOLE_PRINT("R042 %u\r\n", gain); 
    }
    else if (isOption("c043")) //Set Doppler sound gain (in probe)
    {
        uint16_t gain = getNextOptionalNumericParameter(CFG_MIN_DOPPLER_SOUND_GAIN, CFG_MAX_DOPPLER_SOUND_GAIN);
        Config::getpConfig()->SaveConfig(Config::eCfgDopplerSoundGain, gain);
        HwAbsl::getpHw()->getProbeSerial()->SendSetDopplerSoundSignalGainCmd((FHRMps::DopplerSoundSignalGain_t)gain);
        m_pConsoleThread->sendIPC(P_UI, S_PROBE_SOUND_GAIN_UPDATE,gain,0,0 );
        
        gain = Config::getpConfig()->GetConfig(Config::eCfgDopplerSoundGain);
        CONSOLE_PRINT("R044 %u\r\n", gain); 
        
        m_pConsoleThread->sleep(500);
        gain = HwAbsl::getpHw()->getProbeSerial()->GetDopplerSignalGain();
        DEBUG_TRACE("\r\nGain reported from probe: %u\r\n", gain);
    }
    else if (isOption("c044")) //Get Doppler sound speaker gain
    {
        uint16_t gain = Config::getpConfig()->GetConfig(Config::eCfgDopplerSoundGain);
        CONSOLE_PRINT("R044 %u\r\n", gain); 
    }
    else if (isOption("c045")) //Get Moyo FW version
    {
        CONSOLE_PRINT("R045 %d.%d.%d.%d\r\n", VER_MAJ, VER_MIN, VER_MAINTENANCE, VER_BUILD); 
    }
    else if (isOption("c046")) //Get Doppler probe FW version
    {
        HwAbsl::getpHw()->transducerPower(true); //Turn on power for probe in case it is off
        m_pConsoleThread->sleep(1000);
        
        HwAbsl::getpHw()->getProbeSerial()->SendGetModuleVersionCmd();
        m_pConsoleThread->sleep(100);
        
        std::string moduleVersionInfo = HwAbsl::getpHw()->getProbeSerial()->GetModuleVersionInfo();
        uint8_t tmout = 20;
        while(moduleVersionInfo == "" && tmout > 0)
        {
            tmout--;
            m_pConsoleThread->sleep(100);
            moduleVersionInfo = HwAbsl::getpHw()->getProbeSerial()->GetModuleVersionInfo();
        }
        if(moduleVersionInfo == "")
        {
            CONSOLE_PRINT("R046 0\r\n"); 
        }
        else
        {
            std::string swver(moduleVersionInfo, 13, 13);
            CONSOLE_PRINT("R046 %s\r\n", swver.c_str()); 
        }
    }
    else if (isOption("c047")) //Set whether short -?- periods is masked or not
    {
        uint16_t enDis = getNextOptionalNumericParameter(CFG_DISABLED, CFG_ENABLED);
        Config::getpConfig()->SaveConfig(Config::eCfgUseFHRfilter, enDis);
        m_pConsoleThread->sendIPC(P_UI, S_ENABLE_FHR_FILTER_UPDATE,enDis,0,0 );
        
        enDis = Config::getpConfig()->GetConfig(Config::eCfgUseFHRfilter);
        CONSOLE_PRINT("R048 %u\n", enDis); 
    }
    else if (isOption("c048")) //Get whether short -?- periods is masked or not
    {
        uint16_t enDis = Config::getpConfig()->GetConfig(Config::eCfgUseFHRfilter);
        CONSOLE_PRINT("R048 %u\r\n", enDis);   
    }
    else if (isOption("c090")) //Init fuel gauge to use 2.6Ah varta battery
    {
        m_pConsoleThread->sendIPC(P_SUPER, S_INIT_FUELGAUGE, 0, 0, 0);
    }
    else if (isOption("listconfig"))
    {
        if(Config::getpConfig()->IsValid())
        {
            CONSOLE_PRINT("\r\nConfig data:                                           : Valid\r\n");
        }
        else
        {
            CONSOLE_PRINT("\r\nConfig data                                            : NOT valid\r\n");
        }
        
        uint32_t val; 
        float valf;
        
        LL_RTC_TimeTypeDef m_time = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
        CONSOLE_PRINT("\r\nRTC time                                               : %0.2u:%0.2u:%0.2u\r\n", m_time.Hours, m_time.Minutes, m_time.Seconds);
        
        LL_RTC_DateTypeDef m_date = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
        CONSOLE_PRINT("\r\nRTC date                                               : %0.2u/%0.2u-20%0.2u\r\n", m_date.Day, m_date.Month, m_date.Year);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgEpisodevalidTime);
        CONSOLE_PRINT("\r\nTime for marking episode files as valid                : %u sec\r\n", val);   
        
        val = Config::getpConfig()->GetConfig(Config::eCfgEndOfEpisodeTimeout);
        CONSOLE_PRINT("\r\nInactivitime before end of episode                     : %u sec\r\n", val);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgHRupdateTime);
        CONSOLE_PRINT("\r\nTime between each HR update on display                 : %u sec\r\n", val); 
        
        val = Config::getpConfig()->GetConfig(Config::eCfgHRavgBufLen);
        CONSOLE_PRINT("\r\nHR average buffer length                               : %u\r\n", val);   
        
        val = Config::getpConfig()->GetConfig(Config::eCfgLostSignalAlarmTime);
        CONSOLE_PRINT("\r\nTime for lost FHR signal alarm                         : %u\r\n", val);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmHigh);
        CONSOLE_PRINT("\r\nFHR alarm 1: High limit / high limit in trend display  : %u\r\n", val);   
        
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmLow);
        CONSOLE_PRINT("\r\nFHR alarm 1: Low limit / low limit in trend display    : %u\r\n", val);

        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAtypicalAlarmTime);
        CONSOLE_PRINT("\r\nFHR alarm 1: Alarm trigger time                        : %u\r\n", val);
    
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmHigh);
        CONSOLE_PRINT("\r\nFHR alarm 2: High limit                                : %u\r\n", val);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmLow);
        CONSOLE_PRINT("\r\nFHR alarm 2: Low limit                                 : %u\r\n", val);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgFhrAbnormalAlarmTime);
        CONSOLE_PRINT("\r\nFHR alarm 2: Alarm trigger time                        : %u\r\n", val);
    
        val = Config::getpConfig()->GetConfig(Config::eCfgShowClock);
        CONSOLE_PRINT("\r\nEnable/disable whether clock should be shown           : %u\r\n", val);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgResearchStorageMode);
        CONSOLE_PRINT("\r\nEnable/disable research storage mode                   : %u\r\n", val);  
        
        val = Config::getpConfig()->GetConfig(Config::eCfgEpisodeTooShortTime);
        CONSOLE_PRINT("\r\nMinimum lenght of episode to keep files                : %u\r\n", val);  
        
        std::string devId = Config::getpConfig()->GetConfigStr(Config::eCfgDeviceIdentifier);
        CONSOLE_PRINT("\r\nDevice identifier                                      : %s\r\n",devId.c_str());
        
        valf = Config::getpConfig()->GetConfigFloat(Config::eCfgEcgAmpSens);
        CONSOLE_PRINT("\r\nMaternal ECG algorithm amplitude sensitivity           : %f\r\n", valf);
        
        val = Config::getpConfig()->GetConfig(Config::eCfgUseSDcard);
        CONSOLE_PRINT("\r\nEnable/disable SD card functionality                   : %u\r\n", val);  
        
        val = Config::getpConfig()->GetConfig(Config::eCfgDopplerSpeakerGain);
        CONSOLE_PRINT("\r\nDoppler sound speaker gain                             : %u\r\n", val);  
        
        val = Config::getpConfig()->GetConfig(Config::eCfgDopplerSoundGain);
        CONSOLE_PRINT("\r\nDoppler probe gain                                     : %u\r\n", val);  
        
        val = Config::getpConfig()->GetConfig(Config::eCfgUseFHRfilter);
        CONSOLE_PRINT("\r\nFilter '-?-'                                           : %u\r\n", val);  
    }
    
    
    
}

    
//-----------------------------------
// Utility methods
//-----------------------------------

bool ConsoleManager::keypress() const
{
    return (getchar() != EOF);
}

void ConsoleManager::waitForKeypress() const
{
    while (! keypress() )
    {
        m_pConsoleThread->sleep(50);
    }
}

int ConsoleManager::getNextOptionalNumericParameter(int minVal, int maxVal)
{
    int val = atoi(getNextOptionalParameter().c_str());
    val = (val > minVal && val <= maxVal) ? val : minVal;
    return val;
}

bool
ConsoleManager::hasNextArgument() const
{
  return m_currentArgument < m_arguments.size();
}

std::string
ConsoleManager::getNextArgument()
{
  if ( hasNextArgument() )
    return m_arguments[ m_currentArgument++ ];
  return "";
}


std::string
ConsoleManager::getCurrentArgument() const
{
  if ( m_currentArgument < m_arguments.size() )
    return m_arguments[ m_currentArgument ];
  return "";
}


bool
ConsoleManager::argumentStartsWith( const std::string &expected ) const
{
  return getCurrentArgument().substr( 0, expected.length() ) == expected;
}


void
ConsoleManager::getNextOption()
{
  if ( argumentStartsWith( "-" ) )
    m_option = getNextArgument();
  else
    m_option = "";
}


bool
ConsoleManager::isOption( const std::string &shortName ) const
{
  return (m_option == "-" + shortName);
}


std::string
ConsoleManager::getNextParameter()
{
  if ( !hasNextArgument() )
    fail( "missing parameter" );
  return getNextArgument();
}


std::string
ConsoleManager::getNextOptionalParameter()
{
  if ( argumentStartsWith( "-" )  ||  argumentStartsWith( ":" ) )
    return "";
  return getNextArgument();
}


void
ConsoleManager::fail( const std::string& message ) const
{
  DEBUG_TRACE("Console parsing failed while parsing option %s,\n%s\n", m_option.c_str(), message.c_str());
  //throw CommandLineParserException( "while parsing option " + m_option+ ",\n" + message );
}

#ifdef HEAP_USAGE_SUPPORT
void ConsoleManager::printHeapUsage(void)
{
    
    // Ref IAR Technical Note 28545
    printf("Heap usage:\r\n");
    struct mallinfo m;
    m = __iar_dlmallinfo();
    printf("total free space = %10lu\r\n", m.fordblks);
    __iar_dlmalloc_stats();
    printf("-------------\r\n");    
    
}
#endif
