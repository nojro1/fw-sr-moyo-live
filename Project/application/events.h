//! @author Jan Arild R�yneberg
//! @date   07.11.2012
//!

#ifndef _EVENTS_H_
#define _EVENTS_H_

#include "thread.h"
#include "includes.h"
#include <vector>
#include <string>
#include "Mutex.h"
#include "selftestdefs.h"

    class Events
    {
        public:
            
            Events();
            ~Events();
            
            static Events* getpEvents() {return m_pEvents; }
            
            //Note: Must be updated in functions that are using this when number is changed
            static const uint8_t MAX_FIELDS = 12;
            static const uint8_t MAX_EVENTS = 20; 
            static const uint8_t TEXT_MAX_LEN = 30 + 1; //+ string terminator
            
            
            struct event_t
            {
                bool     m_valid;
                bool     m_toSystemLog;
                bool     m_toErrorLog;
                uint32_t m_timeStamp;
                uint16_t m_eventId;
                char     m_text[TEXT_MAX_LEN];
                float    m_dataFields[MAX_FIELDS];
            };
            
            enum EventId_t
            {
                evtHeaderText,
                evtEpisodeDateAndTime,
                evtDeviceSerialNumber,
                evtSoftwareVersion,
                evtDeviceConfiguration,
                evtUltrasoundModuleVersion,
                evtDisplayMode,
                evtVolume,
                evtEndOfEpisodeTimeout,
                evtAlarmAcknowledged,
                evtPowerOnSelfTestResult,
                evtPowerOnBatteryState,
                evtPowerOffBatteryState,
                evtDisplayError,
                evtBatteryLow,
                evtFileWritingError,
                evtUltrasoundTransducerCableBroken,
                evtLowBatteryAlarm,
                evtLostSignalAlarm,
                evtCriticalErrorAlarm,
                evtAbnormalFHRalarm,
                evtMaternalQRS,
                evtECGSignalQuality,
                evtFHR,
                evtProbeTemperature,
                evtTransducerState,
                evtBatteryState,
            };
            
            static const uint8_t TO_EPISODE_LOG = 1 << 0;
            static const uint8_t TO_SYSTEM_LOG  = 1 << 1;
            static const uint8_t TO_ERROR_LOG   = 1 << 2;
            
            void AddEvent(EventId_t a_eventId,
                          uint8_t a_toLog,
                          float a_dataField_1 = 0.0,
                          float a_dataField_2 = 0.0,
                          float a_dataField_3 = 0.0,
                          float a_dataField_4 = 0.0,
                          float a_dataField_5 = 0.0,
                          float a_dataField_6 = 0.0,
                          float a_dataField_7 = 0.0,
                          float a_dataField_8 = 0.0,
                          float a_dataField_9 = 0.0,
                          float a_dataField_10 = 0.0,
                          float a_dataField_11 = 0.0,
                          float a_dataField_12 = 0.0);
            
            void AddEvent(uint32_t a_timeStamp,
                          EventId_t a_eventId,
                          uint8_t a_toLog,
                          float a_dataField_1 = 0.0,
                          float a_dataField_2 = 0.0,
                          float a_dataField_3 = 0.0,
                          float a_dataField_4 = 0.0,
                          float a_dataField_5 = 0.0,
                          float a_dataField_6 = 0.0,
                          float a_dataField_7 = 0.0,
                          float a_dataField_8 = 0.0,
                          float a_dataField_9 = 0.0,
                          float a_dataField_10 = 0.0,
                          float a_dataField_11 = 0.0,
                          float a_dataField_12 = 0.0);
            
             void AddEvent(EventId_t a_eventId,
                           uint8_t a_toLog,
                           const char  *a_text,
                           float a_dataField_1 = 0.0,
                           float a_dataField_2 = 0.0,
                           float a_dataField_3 = 0.0,
                           float a_dataField_4 = 0.0,
                           float a_dataField_5 = 0.0,
                           float a_dataField_6 = 0.0,
                           float a_dataField_7 = 0.0,
                           float a_dataField_8 = 0.0,
                           float a_dataField_9 = 0.0,
                           float a_dataField_10 = 0.0,
                           float a_dataField_11 = 0.0,
                           float a_dataField_12 = 0.0);
            
            event_t GetEvent();
            std::string EventStringPullFront();
            uint8_t Count();
            bool isSystemLogEvent();
            bool isErrorLogEvent();
            void SetEpisodeStartTime(uint32_t a_epStartTime);
    
        protected:
            
        private:
            
            static Events* m_pEvents;  //!< static member to keep track of instance.
            
            Events(const Events& right);            //!< block the copy constructor
            Events& operator=(const Events& right); //!< block the assignment operator
            
            bool bufferLock();
            bool bufferUnlock();
            std::string toStrBool10(float a_value);
            std::string toStr(float a_value, const char * a_fmtStr);
            std::string formatSelfTestResult(float a_value);
            
            std::vector<event_t> m_events;
            std::string m_eventString;
            bool m_systemLogEvent; //Also store in system log
            bool m_errorLogEvent;   //Also store in error log
            uint32_t m_episodeStartTime;
    };
    
#endif

