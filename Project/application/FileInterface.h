//! @class  LNRM_FS
//! @brief
//! @author Jan Arild R�yneberg
//! @date   11.02.2013
//!

#ifndef _LNRM_FS_H_
#define _LNRM_FS_H_

#include "ff.h"
#include "ff_gen_drv.h"
#include "Mutex.h"
//#include "msd_mass_storage.h"

extern "C"
{
    void Mass_Storage_Init(void);
    bool Mass_Storage_Start (void);
    void Mass_Storage_Stop(void);
    uint32_t GetUsbDeviceState(void);
}

//! @brief Wrapper class for the file system and mass storage device

class LNRM_FS
{
    
public:
	LNRM_FS();
	~LNRM_FS();
	static LNRM_FS* get() { return m_pLNRM_FS; }
    SHORT FS__GetVolumeStatus(const char  * sVolume);
    DWORD FS__GetVolumeSize(const char * sVolume);
    DWORD FS__GetVolumeFreeSpace(const char * sVolume);
    FIL * FS__FOpen(const char * pFileName, const char * pMode);
    //void FS__SetFileWriteMode(FS_WRITEMODE WriteMode);
    //I32 FS__GetFilePos(FS_FILE * pFile);
    //int FS__SetFilePos(FS_FILE * pFile, I32 Offset, int Origin);
    SHORT FS__FClose(FIL *pFile);
    DWORD FS__Write(FIL *pFile, const void *pData, DWORD NumBytes);
    DWORD FS__Read(FIL *pFile, void *pData, DWORD NumBytes);
    int FS__MkDir(const char * pDirName);
    void FS__ClearErr(FS_FILE * pFile);
    char FS__FindFirstFile(FS_FIND_DATA * pfd, const char * sPath, char * sFilename, int sizeofFilename);
    int FS__Rename(const char * sOldName, const char * sNewName);
    char FS__FindNextFile(FS_FIND_DATA * pfd);
    void FS__FindClose(FS_FIND_DATA * pfd);
    I16 FS__FError(FS_FILE * pFile);
    int FS__Remove(const char *sFileName);
    int FS__RmDir(const char * pDirName);
    void FS__FileTimeToTimeStamp(const FS_FILETIME * pFileTime, DWORD * pTimeStamp);
    int FS__SetFileTimeEx(const char * pName, DWORD TimeStamp, int Index);
    //bool Mass__StorageIsActive();
    //void Mass__Storage_Init();
    //bool Mass__Storage_Start();
    //void Mass__Storage_Stop();
    
    
protected:

private:
    LNRM_FS(const LNRM_FS& right);            //!< block the copy constructor
    LNRM_FS& operator=(const LNRM_FS& right); //!< block the assignment operator
    static LNRM_FS* m_pLNRM_FS;               //!< static member to keep track of instance.
    bool m_massStorageActive;
    
    bool Lock();
    bool Unlock();
    
    
};

#endif
