#ifndef _RTOS_H_
#define _RTOS_H_

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#define OS_EnterRegion() taskENTER_CRITICAL()
#define OS_LeaveRegion() taskEXIT_CRITICAL()

#define OS_IncDI() 
#define OS_DecRI() 
#define OS_InitKern() 
#define OS_INIT_SYS_LOCKS() 
#define OS_IsRunning() true
#define OS_Delay(ms) vTaskDelay( (ms) / portTICK_PERIOD_MS )
#define OS_Delayus(us) nrf_delay_us((us))
#define OS_TIME TickType_t
#define OS_GetTime() (xTaskGetTickCount() / portTICK_PERIOD_MS)

#endif // include guard
