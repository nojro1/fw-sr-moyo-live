//! \file    Signal.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    26.05.2011

#ifndef _SIGNAL_H
#define _SIGNAL_H

// header includes
#include "RTOS.H"
#include <cstdint>

// forward declarations
// ...

////

//! \brief The Signal class wraps OS events
//! \remark Signals Must NOT be waited upon from interrupt routines or timer callbacks !

class Signal
{
public:

    // constructor/destructor
    Signal();
    virtual ~Signal();

    void wait();
    bool wait(std::uint16_t millisec);
    void send();

protected:

private:
    Signal(const Signal& right);            // block the copy constructor
    Signal& operator=(const Signal& right); // block the assignment operator

    OS_CSEMA  m_sem;                        //!< uses a counting semaphore
};


#endif   // _SIGNAL_H

//======== End of Signal.h ==========

