//! \file    Signal.cpp
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    26.05.2011

// include self first
#include "Signal.h"

// other includes

//! constructor
Signal::Signal()
{
    OS_CreateCSema(&m_sem, 0);
}


//! destructor
Signal::~Signal()
{
    OS_DeleteCSema(&m_sem);
}

//! \brief Waits for a signal to be received, no timeout
void Signal::wait()
{
    OS_WaitCSema(&m_sem);
}

//! \brief Waits for a signal to be received, with timeout
//! \param millisec Number of milliseconds to wait for signal
//! \return false if timed out, else true
bool Signal::wait(std::uint16_t millisec)
{
    return OS_WaitCSemaTimed(&m_sem, millisec) != 0;
}

//! \brief Sends a signal
void Signal::send()
{
    OS_SignalCSema(&m_sem);
}


//======== End of Signal.cpp ==========

