//! \file    thread.h
//! \author  Geir Inge Tellnes
//! \version 3.0
//! \date    12.03.2020 for FreeRTOS / STM32L5

#ifndef _THREAD_H_
#define _THREAD_H_

#include <cstdint>
#include <cstdio>
#include <bitset>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "mutex.h"
#include "guard.h"
#include "sysdefs.h"
#include "signaldefs.h"

#if defined(__cplusplus)
 #ifndef __embedded_cplusplus
   using std::uint8_t;
   using std::uint16_t;
   using std::uint32_t;
 #endif
#endif

// forward declarations

//
//! \brief Abstract base class for all threads
//
class Thread
{
public:
    Thread();
    virtual ~Thread();


    //! \brief Returns assigned thread ID number
    THREAD_ID getThreadId() const { return m_threadId; }

    uint8_t getPriority() const;
    uint8_t setPriority(uint8_t priority) const;

    //!NB: The stackSize is NOT in bytes !! It's in blocks of sizeof int! 
    void start(const char *threadName, THREAD_ID threadId, uint8_t priority, uint32_t stackSize, uint8_t maxNumMsg, void *pThreadArgument);

    void join();

    void sleep(uint32_t millisec);
    
    static void sigDispatch(THREAD_ID threadId, THREAD_SIGNAL sig, uint32_t d1=0, uint32_t d2=0, void* pData=0, bool hiPri=false);
    static void sigDispatch(THREAD_SIGNAL sig, uint32_t d1=0, uint32_t d2=0, void* pData=0, bool hiPri=false);
    static bool sigDispatchCond(THREAD_ID threadId, THREAD_SIGNAL sig, uint32_t d1=0, uint32_t d2=0, void* pData=0, bool hiPri=false);

    //! \brief Returns pointer to the console resource protection mutex
    //! If none exist, it will be created
    static Mutex * getpConsoleMutex() { return Thread::m_pConsoleMutex = Mutex::get(Thread::m_pConsoleMutex); }

    static TaskHandle_t getpTcb(THREAD_ID threadId);
    static Thread* getpThread(THREAD_ID);

    //! \brief Will suspend this thread (note: called from another thread OR same thread).
    virtual void suspend();
    //! \brief Will resume this thread, if it was suspended.
    virtual void resume();
    //! \brief The calling thread will give up its remaining time slice / OS
    //! \      will task-switch.
    static void yield();

    //! \brief Is the task suspended?
    bool suspended() const;

    static const char * getThreadName();
protected:

    //! \brief Virtual function to be overridden by the derived
    //! class to implement the thread code.
    virtual void run(void *) = 0;

    void sigWait(uint32_t timeout, IPC* ipc);
    void sigWait(IPC* ipc);

    bool sigPeek(IPC* ipc);

    void sigClear();


    void sigUnknown(const IPC* ipc) const;

    static void entryPoint(void * pTh);

    //! \brief Returns pointer to the thread's user argument
    const void * arg() const { return m_pArg_; }
    void * arg() { return m_pArg_; }

    //! \brief Modify the thread's user argument
    //! \param a void pointer to the new user argument
    void   arg(void* a) { m_pArg_ = a; }

    bool areAllThreadsStarted() const;
    void waitUntilAllThreadsStarted();

    void markThreadAsStarted() const;


private:
    Thread(const Thread& right);            //!< block the copy constructor
    Thread& operator=(const Thread& right); //!< block the assignment operator

    THREAD_ID m_threadId;
    void * m_pArg_;

    TaskHandle_t m_tcb;
    static TaskHandle_t m_pTcbs[NUMBER_OF_THREADS];
    static Thread* m_pThread[NUMBER_OF_THREADS];

    QueueHandle_t m_pMbox;
    uint8_t* m_pMboxData;
    static QueueHandle_t m_pMboxes[NUMBER_OF_THREADS];

    static std::bitset<NUMBER_OF_THREADS> m_startedThreads;
    static Mutex* m_pStartedThreadsMutex;

    static Mutex* m_pConsoleMutex;

    static const std::size_t TREAD_NAME_MAX = 15;
    
    TickType_t m_xLastWakeTime;    
};

#ifndef __embedded_cplusplus
  using std::printf;
#endif

#ifndef NDEBUG
    // --------------------------------------------------------------------------
    //! \brief Threadsafe DEBUG_TRACE macro that works on debug builds
    //! Is a variadic macro (i.e. with "..." parameter)
    // ---------------------------------------------------------------------------
  #ifdef DEBUG_TRACE_IS_TIMESTAMPED                     
    #define DEBUG_TRACE(...)                                        \
            do {                                                    \
                Guard g( *Thread::getpConsoleMutex() );             \
                printf("%08d: ", OS_GetTime());                     \
                printf(__VA_ARGS__);                                \
            } while (0)
  #else
    #define DEBUG_TRACE(...)                                        \
            do {                                                    \
                Guard g( *Thread::getpConsoleMutex() );             \
                printf(__VA_ARGS__);                                \
            } while (0)
  #endif // DEBUG_TRACE_IS_TIMESTAMPED
#else
    #define DEBUG_TRACE(...) //empty
#endif // #ifndef NDEBUG


    // --------------------------------------------------------------------------
    //! \brief Threadsafe CONSOLE_PRINT macro that works on release builds only
    //! Is a variadic macro (i.e. with "..." parameter)
    //! \remark: use CONSOLE_PRINT sparingly
    // ---------------------------------------------------------------------------
    #define CONSOLE_PRINT(...)                                   \
        do {                                                     \
            Guard g( *Thread::getpConsoleMutex() );              \
            printf(__VA_ARGS__);                                 \
        } while (0)


#endif

//======== End of thread.h ==========

