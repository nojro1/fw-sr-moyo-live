/*********************************************************************
*
*   IAR PowerPac
*
*   (c) Copyright IAR Systems 2009.  All rights reserved.
*
**********************************************************************
----------------------------------------------------------------------
File    : RTOSInit.c
Purpose : RTOSInit for STM32F103 (IAR STM32-SK).

          AEd - This is a scaled down version for use with our CPP
          framework. HW init is done by the HW wrapper and the ST
          STM32F10x Standard Peripherals Library is used.

          10Feb2010, GIT, Adapted to Peripheral LIB 3.1.2, added sleep for powersave
          14Apr2010, GIT, Now handles system tick other than 1 ms correctly
          30Apr2010, GIT, Now handles default startup assembly files
          11Aug2014, GIT, OS_GetTime_Cycles() now also accurate at system tick periods other than 1 ms

--------  END-OF-HEADER  ---------------------------------------------
*/
#include "RTOS.H"
#include "misc.h"
#include "sysdefs.h"
#include "utilfuncs.h"

/*********************************************************************
*
*       Settings for embOSView
*/
#ifndef   OS_VIEW_ENABLE
  #define OS_VIEW_ENABLE   (0)      // define this one to 1 in your project file if you want embOSView support
#else
#ifdef NDEBUG
  #undef OS_VIEW_ENABLE
  #define OS_VIEW_ENABLE   (0)      // not in release builds
#endif
#endif

#ifndef   OS_VIEW_USE_UART          // If set, UART will be used for communication
  #define OS_VIEW_USE_UART (0)      // Default: 0 => memory access is used
#endif                              // if OS_VIEW_ENABLE is on

/****** End of configuration settings *******************************/


/*********************************************************************
*
*       JLINKMEM and UART settings for OSView
*
*       Automatically generated from configuration settings
*/
#define OS_USE_JLINKMEM   (OS_VIEW_ENABLE && (OS_VIEW_USE_UART == 0))

#define OS_UART_USED      (OS_VIEW_ENABLE && (OS_VIEW_USE_UART != 0) && (0))

#if OS_USE_JLINKMEM
  #include "JLINKMEM.h"
#endif


/*********************************************************************
*
*       Local defines (sfrs used in RTOSInit.c)
*
**********************************************************************
*/

#define SYS_INT_CTRL_STATE        (*(volatile OS_U32*)(0xE000ED04))
#define SYS_PENDSTSET             26

#if defined(STM32F10X_LD) || defined(STM32F10X_MD)
    #define NUM_INTERRUPTS            (16+43)
#elif defined(STM32F10X_LD_VL) || defined(STM32F10X_MD_VL)
    #define NUM_INTERRUPTS            (16+56)
#elif defined (STM32L1XX_MD)
    #define NUM_INTERRUPTS            (16+45)
#elif defined(STM32F10X_HD) || defined(STM32F10X_XL)
    #define NUM_INTERRUPTS            (16+60)
#elif defined(STM32F10X_HD_VL)
    #define NUM_INTERRUPTS            (16+61)
#elif defined(STM32F10X_CL)
    #define NUM_INTERRUPTS            (16+68)
#else
    #error "NUM_INTERRUPTS unknown"
#endif


#if  (OS_UART_USED || OS_USE_JLINKMEM)
  #define OS_COM_INIT() OS_COM_Init()
#else
  #define OS_COM_INIT()
#endif

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
uint32_t OS_TIMER_RELOAD;
uint32_t OS_PCLK_TIMER_DIV_1000000;

#if OS_USE_JLINKMEM
  // Size of the communication buffer for JLINKMEM
const OS_U32 OS_JLINKMEM_BufferSize = 32;
#else
const OS_U32 OS_JLINKMEM_BufferSize = 0;     // Communication not used
#endif

/*********************************************************************
*
*       Local functions
*
**********************************************************************
*/

/*********************************************************************
*
*       Global functions
*
**********************************************************************
*/

/*********************************************************************
*
*       OS_Systick
*
* Function description
*   This is the code that gets called when the processor receives a
*   _SysTick exception. SysTick is used as OS timer tick.
*
* NOTES:
*   (1) It has to be inserted in the interrupt vector table, if RAM
*       vectors are not used. Therefore is is declared public
*/
#if OS_USE_VARINTTABLE
void OS_Systick(void)
#else
void SysTick_Handler(void)
#endif
{
    OS_EnterNestableInterrupt();

#if SYSTICK_TIME == 1
    OS_TICK_Handle();
#else
    OS_TICK_HandleEx();
#endif

#if OS_USE_JLINKMEM
    JLINKMEM_Process();
#endif
    
    OS_LeaveNestableInterrupt();
}

/*********************************************************************
*
*       OS_InitHW()
*
*       Initialize the hardware (timer) required for the OS to run.
*       May be modified, if an other timer should be used
*/
void OS_InitHW(void)
{
    //AEd - HW is initialized in HW wrapper.
    //The only thing we will do here is to read in current clock values
    //for use by the timing utilities in this module.
    OS_TIMER_RELOAD  = SysTick->LOAD;
    OS_PCLK_TIMER_DIV_1000000 = SystemCoreClock/1000000uL;

    // Need to tell OS if our ticks are <> 1 ms
  #if SYSTICK_TIME != 1
    OS_TICK_Config(SYSTICK_TIME, 1);
  #endif
    
    OS_COM_INIT();
    
}

/*********************************************************************
*
*       Idle loop  (OS_Idle)
*
*       Please note:
*       This is basically the "core" of the idle loop.
*       This core loop can be changed, but:
*       The idle loop does not have a stack of its own, therefore no
*       functionality should be implemented that relies on the stack
*       to be preserved. However, a simple program loop can be programmed
*       (like toggeling an output or incrementing a counter)
*/
void OS_Idle(void)     // Idle loop: No task is ready to execute
{
    while (1)
    {
    #if (OS_USE_JLINKMEM == 0)     // Enter CPU halt mode when J-Link communication not used
        __WFI(); // sleep (powersave)
    #endif
    }
}

/*********************************************************************
*
*       OS interrupt handler and ISR specific functions
*
**********************************************************************
*/

#if OS_USE_VARINTTABLE
//
// The interrupt vector table may be located anywhere in RAM
//
#pragma data_alignment=512
__no_init void (*g_pfnRAMVectors[NUM_INTERRUPTS])(void);
#endif

#ifndef APP_START_OFFSET
  #define APP_START_OFFSET 0
#endif

/*********************************************************************
*
*       OS_ARM_InstallISRHandler
*/
OS_ISR_HANDLER* OS_ARM_InstallISRHandler (int ISRIndex, OS_ISR_HANDLER* pISRHandler) {

#if OS_USE_VARINTTABLE

  OS_ISR_HANDLER*  pOldHandler;
  OS_U32 ulIdx;

  pOldHandler = NULL;
  //
  // Check whether the RAM vector table has been initialized.
  //
  if (SCB->VTOR != (uint32_t)g_pfnRAMVectors)
  {
    //
    // Copy the vector table from the beginning of FLASH to the RAM vector table.
    //
    for(ulIdx = 0; ulIdx < ARR_ELEMS(g_pfnRAMVectors); ulIdx++)
    {
      g_pfnRAMVectors[ulIdx] = (void (*)(void))(*((volatile uint32_t *)(ulIdx * 4 + APP_START_OFFSET))); //lint !e613
    }
    //
    // Program NVIC to point at the RAM vector table.
    //
    //Set the Vector Table base location at 0x20000000
    NVIC_SetVectorTable(NVIC_VectTab_RAM, (uint32_t)g_pfnRAMVectors-NVIC_VectTab_RAM);
  }
  //
  // Save the interrupt handler.
  //
  ISRIndex += 0x10; // offset adjust since indexes can start negative
  pOldHandler = g_pfnRAMVectors[ISRIndex];
  g_pfnRAMVectors[ISRIndex] = pISRHandler;
  return (pOldHandler);
#elif USE_DFU_BOOTLOADER
  // Set the Vector Table base location at offset APP_START_OFFSET for DFU bootloader usage
  NVIC_SetVectorTable(NVIC_VectTab_FLASH, APP_START_OFFSET);
  return (NULL);
#else
  NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x00);
  return (NULL);
#endif
}

/*********************************************************************
*
*       OS_GetTime_Cycles()
*
*       This routine is required for high
*       resolution time measurement functions.
*       It returns the system time in timer clock cycles.
*/
OS_U32 OS_GetTime_Cycles(void)
{
    OS_U32 t_cnt;
    OS_U32 time;

    time  =  OS_GetTime32(); //lint !e732
    t_cnt = (OS_TIMER_RELOAD) - SysTick->VAL;
    if (SYS_INT_CTRL_STATE & (1 << SYS_PENDSTSET))      // Missed a counter interrupt?
    {
        t_cnt = (OS_TIMER_RELOAD) - SysTick->VAL;  // Adjust result
        time++;
    }
    return ((OS_TIMER_RELOAD) * time) + t_cnt;
}

/*********************************************************************
*
*       OS_ConvertCycles2us
*
*       Convert Cycles into micro seconds.
*
*       If your clock frequency is not a multiple of 1 MHz,
*       you may have to modify this routine in order to get proper
*       diagonstics.
*
*       This routine is required for profiling or high resolution time
*       measurement only. It does not affect operation of the OS.
*/
OS_U32 OS_ConvertCycles2us(OS_U32 Cycles)
{
    return Cycles/(OS_PCLK_TIMER_DIV_1000000);
}

/*********************************************************************
*
*       Optional communication with embOSView
*
**********************************************************************
*/

#if OS_USE_JLINKMEM                    // Communication via JTAG / SWD

/*********************************************************************
*
*       _JLINKMEM_OnRx()
*/
static void _JLINKMEM_OnRx(OS_U8 Data) {
  OS_OnRx(Data);
}

/*********************************************************************
*
*       _JLINKMEM_OnTx()
*/
static void _JLINKMEM_OnTx(void) {
  OS_OnTx();
}

/*********************************************************************
*
*       _JLINKMEM_GetNextChar()
*/
static OS_INT _JLINKMEM_GetNextChar(void) {
  return OS_COM_GetNextChar();
}

/*********************************************************************
*
*       OS_COM_Init()
*       Initialize memory access for OSView
*/
void OS_COM_Init(void) {
  JLINKMEM_SetpfOnRx(_JLINKMEM_OnRx);
  JLINKMEM_SetpfOnTx(_JLINKMEM_OnTx);
  JLINKMEM_SetpfGetNextChar(_JLINKMEM_GetNextChar);
}

/*********************************************************************
*
*       OS_COM_Send1()
*       Send 1 character via memory
*/
void OS_COM_Send1(OS_U8 c) {
  JLINKMEM_SendChar(c);
}

#elif OS_UART_USED   // Communication via UART, can not be implemented generic

  #error "OS_UART can not be handled with current generic CMSIS version."

  //
  // Device specific versions of the 3 communication functions
  // OS_COM_IsrHandler(void)
  // OS_COM_Send1(OS_U8 c)
  // OS_COM_Init(void)
  // have to be implemented when a UART shall be used for communication to embOSView.
  // Samples can be found in our device specific projects.

#else  /*  No communication or UART selected, using dummies */

void OS_COM_Send1(OS_U8 c) {
  OS_USEPARA(c);           /* Avoid compiler warning */
  OS_COM_ClearTxActive();  /* Let the OS know that Tx is not busy */
}

#endif /*  OS_UART_USED  */

/****** Final check of configuration ********************************/

#ifndef OS_UART_USED
  #error "OS_UART_USED has to be defined"
#endif

/*****  EOF  ********************************************************/
