//! \file    SpinLock.h
//! \author  DRY
//! \version 1.0
//! \date    11.07.2011
//! \brief   Iface to a spin lock implementation specific to the CortexM3 H/W.\n
//! \        Note that it does not rely on RTOS => can be used without it.

#ifndef CORTEX_M3_FRAMEWORK_SPIN_LOCK_H_
#define CORTEX_M3_FRAMEWORK_SPIN_LOCK_H_

#include <ILock.h>

class SpinLock
: public ILock
{
  public:
    
    //You can specify initial sate - unlocked (=0 default), or locked (=1).
    SpinLock(const int& inistate=0);
    
    virtual void lock();

    virtual void unlock();
    
  private:
    
    volatile int _lock;
};


#endif /* CORTEX_M3_FRAMEWORK_SPIN_LOCK_H_*/
