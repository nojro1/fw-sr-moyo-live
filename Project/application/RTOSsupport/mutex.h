//! \file    mutex.h
//! \author  Geir Inge tellnes
//! \version 3.0
//! \date    12.03.2020 for Free RTOS / STM32L5

#ifndef _MUTEX_H
#define _MUTEX_H

// header includes
#include "FreeRTOS.h"
#include "semphr.h"


// forward declarations
// ...

////

//! \brief Wrapper for OS resource semaphores
//! \remark Must NOT be locked from interrupt routines or timer callbacksStatic!
//! \remark Cannot be unlocked from a different thread than it was locked from
class Mutex
{
public:

    // constructor/destructor
    Mutex();
    virtual ~Mutex();

    virtual void lock();
    virtual void release();
    virtual bool tryLock();

    static Mutex * get(Mutex * pMx);

protected:

private:
    Mutex(const Mutex& right);            //!< block the copy constructor
    Mutex& operator=(const Mutex& right); //!< block the assignment operator

    SemaphoreHandle_t  m_xSem;            //!< uses a binary semaphore

};


#endif   // _MUTEX_H

//======== End of Mutex.h ==========

