//! \file    guard.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    18.02.2010

#ifndef _GUARD_H
#define _GUARD_H

// header includes

// forward declarations
class Mutex;


//! \brief Wrapper for OS single-lock semaphores with scoped locking
//!
//! Example of use:\n
//! \n
//! Mutex * m_pResourceMutex; // in the Device driver's C++ class declaration (.h file)\n
//! \n
//! In the .cpp file:\n
//! \n
//! Devicedriver::Devicedriver() : m_pResourceMutex(0) {} \n
//! \n
//! Devicedriver::someOutputOperationsUsingSharedResource()\n
//! {\n
//!     - Guard g(*(m_pResourceMutex = Mutex::get(m_pResourceMutex))); // will protect this resource until out of scope\n
//!     - perform output operations using the shared resource....
//!     .
//! }\n
//! \n
//! \remark Must NOT be used in interrupt routines or timer callbacks!
//!
class Guard
{
public:

    // constructor/destructor
    explicit Guard(Mutex& mx);
    virtual ~Guard();

protected:

private:
    Guard(const Guard& right);            //!< block the copy constructor
    Guard& operator=(const Guard& right); //!< block the assignment operator

    Mutex& m_mutex;         //!< reference to the Mutex object to use
};


#endif   // _GUARD_H

//======== End of guard.h ==========

