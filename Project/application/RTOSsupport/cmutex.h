//! \file    Cmutex.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    23.03.2015

#ifndef _CMUTEX_H
#define _CMUTEX_H

// header includes
#include "RTOS.H"


// forward declarations
// ...

////

//! \brief Wrapper for OS counting semaphores
//! \remark Must NOT be locked from interrupt routines or timer callbacksStatic!\n
//! \remark Can be released from a different thread the one that locked it ! 
class Cmutex
{
public:

    // constructor/destructor
    Cmutex();
    virtual ~Cmutex();

    virtual void lock();
    virtual void release();
    virtual bool tryLock();

    static Cmutex * get(Cmutex * pMx);

protected:

private:
    Cmutex(const Cmutex& right);            //!< block the copy constructor
    Cmutex& operator=(const Cmutex& right); //!< block the assignment operator

    OS_CSEMA  m_sem;                        //!< uses a counting semaphore

};


#endif   // _CMUTEX_H

//======== End of Cmutex.h ==========

