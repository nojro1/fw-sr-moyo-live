//! \file    ostimer.cpp
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    14.04.2010

#ifndef __STDC_LIMIT_MACROS
  //Have to use this compiler private macro bs the code uses C macro limits. Cannot use std C++
   //<limits> in case this driver is compiled in embedded C++ mode
  #define __STDC_LIMIT_MACROS
  #include <cstdint>
#endif
// include self first
#include "ostimer.h"

// andre includes
#include "sysdefs.h"


//! constructor
OsTimer::OsTimer()
: m_timer(0), m_pCallbackFunc(0), m_pCallbackArgs(0), m_periodicTime(0), m_periodCount(UINT32_MAX)
{

}

//! destructor
OsTimer::~OsTimer()
{
    remove();
    m_pCallbackArgs = 0;
}

//! \brief Creates the actual OS timer
//! \param initialDelay Initial delay (ms) before callback is first called
//! \param periodTime Period time (ms) before subsequent calls to callback function. If 0, the timer will be a 'one shot' timer
//! \param numPeriods Number of periods the timer shall run (NOTE: including the initial period). If 0 and periodTime > 0, the timer will retrigger forever
//! \param pCallbackFunc Pointer to callback function. 0 = no callback function
//! \param pCallbackArgs void pointer, Arguments to callback function
//! \return 0 if OK, else negative
//! \remark The callback function MUST be declared as follows:
//! \remark     void myCallback(void *pCallbackArgs);
//! \remark     IMPORTANT: the callback function should be made as short and as fast as possible.
int OsTimer::create(std::uint32_t initialDelay, std::uint32_t periodTime, std::uint32_t numPeriods,
                    OSTimerCallback pCallbackFunc, void * pCallbackArgs)
{
    if ( 0==m_timer ) // only create once
    {
        m_pCallbackFunc = pCallbackFunc;
        m_pCallbackArgs = pCallbackArgs;

        m_periodicTime = periodTime;
        if (m_periodicTime != 0 && numPeriods != 0)
        {
            m_periodCount = numPeriods-1; // -1 due to initial period
        }
        if (0==initialDelay) initialDelay=1; //else it will never start
        
        const TickType_t xDelay = initialDelay / portTICK_PERIOD_MS;

        m_timer = xTimerCreate( "Timer",       // Just a text name, not used by the kernel.
                                xDelay,        // The timer period in ticks.
                                pdFALSE,        // The timers will not auto-reload themselves when they expire.
                                static_cast<void *>(this),   // Assign each timer a unique id equal to its array index.
                                reinterpret_cast<TimerCallbackFunction_t>(OsTimer::myStaticCallback) // Each timer calls the same callback when it expires.
                              );
        
        return m_timer != 0;
    }
    return -1;
}

//! \brief Removes the previously created OS timer
//! \return 0 if OK, else negative
int OsTimer::remove()
{
    if ( m_timer != 0 )
    {
        if (xTimerDelete(m_timer, 0) != pdFAIL)
        {
            m_timer = 0;
            m_periodCount = UINT32_MAX;
            return 0;
        }
    }
    return -1;
}

//! \brief Starts the previously created OS timer
//! \return 0 if OK, else negative
int OsTimer::start()
{
    if ( m_timer != 0 )
    {
        if (xTimerStart(m_timer, 0) != pdFAIL)
            return 0;
    }
    return -1;
}

//! \brief Restarts the previously created OS timer
//! \return 0 if OK, else negative
int OsTimer::restart()
{
    if ( m_timer != 0 )
    {
        if (xTimerReset(m_timer, 0) != pdFAIL)
            return 0;
    }
    return -1;
}

//! \brief Stops the previously created OS timer
//! \return 0 if OK, else negative
int OsTimer::stop()
{
    if ( m_timer != 0 )
    {
        if (xTimerStop(m_timer, 0) != pdFAIL)
            return 0;
    }
    return -1;
}


//! \brief To check if the timer is running
//! \return true if running, else false
//! \remark If timer was started from a thread with lower priority than the timer task\n
//! a sleep of at least 1 tick time will be needed before calling this method\n
//! ref: https://sourceforge.net/p/freertos/discussion/382005/thread/b75af088/
bool OsTimer::isRunning()
{
    if ( m_timer != 0 )
    {
        if (xTimerIsTimerActive( m_timer ) == pdFALSE)
        {
            return false;
        }
        return true;
    }
    return false;
}

//! \brief Static class method that receives callback from the RTOS timer
//! \param pThis Contains the 'this' pointer for the OsTimer instance that create() was called upon
//! \return 0 if OK, else negative

//lint -e{715}
void OsTimer::myStaticCallback( TimerHandle_t pxTimer )
{
  
    if (pxTimer) // contains the 'this' pointer for the object that create() was called upon
    {
        OsTimer *pTmrObj = static_cast<OsTimer*>(pvTimerGetTimerID( pxTimer ));
        if (pTmrObj->m_pCallbackFunc) // only call if timer created with callback function
        {
            pTmrObj->m_pCallbackFunc(pTmrObj->m_pCallbackArgs);
            if (pTmrObj->m_periodCount != 0 && pTmrObj->m_periodicTime != 0)
            {
                if (pTmrObj->m_periodCount > 0 && pTmrObj->m_periodCount != UINT32_MAX)
                {
                    pTmrObj->m_periodCount--;
                }
                
                const TickType_t xDelay = pTmrObj->m_periodicTime / portTICK_PERIOD_MS;

                if (pTmrObj->m_timer != 0 )
                {
                    xTimerChangePeriod(pTmrObj->m_timer, xDelay, 0);
                    xTimerReset(pTmrObj->m_timer, 0);
                }
            }
        }
    }
  
}



//======== End of $Workfile: ostimer.cpp $ ==========

