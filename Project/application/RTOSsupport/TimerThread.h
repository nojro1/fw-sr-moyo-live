//! \file    TimerThread.h
//! \author  DRY
//! \version 1.0
//! \date    20.04.2012
//! \brief   Manages & Runs low-priority app-level timers

#ifndef TIMER_THREAD_H
#define TIMER_THREAD_H

#include <ITimer.h>
#include <thread.h>
#include <cstdint>

//@Note: The timers implementation is NOT thread-safe in general, they can be only with 2 threads -
//one mods them the other (TimerThread) runs them. This is likely enough for many cases, if not you
//can easily do some thread-safe wrapping.

class TimerThread
  : public Thread,
    public ITimerFactory
{
  public:

    enum { MAX_NUM_TIMERS =
     #ifndef MAX_NUM_APP_TIMERS
           12
     #else
           MAX_NUM_APP_TIMERS
     #endif
         };

    class Timer
      : public ITimer
    {
      public:
        ///Note: in this implementation its the timer thread that managers creation & removal of
        //timers. The timer iface

        virtual int create(std::uint32_t initDelay, std::uint32_t periodTime,
                           std::uint32_t numPeriods, ITimerCallback pCbFunc, void * pCbArgs)
        {
          return _timerThread._initTimer(this,initDelay,periodTime, numPeriods,pCbFunc, pCbArgs);
        }

        //These methods are for compatability with ITimer, which may be used without knowing about
        //the TimerThread
        virtual int remove()
        { //This would actually free timer  / require user to obtain  a new one
          //if (_timerThread.freeTimer(this)) return 0;
          //return -1;
          //Not same remove() semantics as in OSTimer.cpp (same os timer wrapper can create/destory
          //many times actual sys timers)
          return stop();
        }

        virtual int start()
        { if (!_free && _stopped)
          { _stopped=false;
            return 0;
          }
          return -1;
        }

        virtual int restart();

        virtual int stop()
        { _stopped=true;
          return 0;
        }

        virtual bool isRunning()
        { if (!(_free || _expired || _stopped) ) return true;
          return false;
        }

      protected:

        bool _free;
        bool _expired;
        bool _stopped;
        std::uint32_t _ms_tick_count;
        std::uint32_t _curr_period_time;
        std::uint32_t _curr_period_count;
        ITimerCallback _usrCbFunc;
        void* _usrCbArgs;

        std::uint32_t _periodic_time;
        std::uint32_t _period_count;
        std::uint32_t _initial_delay;

        TimerThread& _timerThread;
        friend class TimerThread;

        Timer(TimerThread& tT/*manages us*/): _timerThread(tT),
          _free(true), _expired(false), _stopped(true), _usrCbFunc(0),_usrCbArgs(0),
          _periodic_time(0), _period_count(-1), _ms_tick_count(0), _curr_period_time(0),
          _curr_period_count(0){}

        virtual ~Timer() { }
    };


    TimerThread();

    virtual ~TimerThread();

    //If there is unallocated timer will return ptr to reserved but un-initialized timer.
    //It can then be init'ed with Timer::create().
    //Note: this is for compatability with those who only know ITimer iface, otherwise its
    //better to use the getNewTimer() below.
    virtual ITimer* newTimer();

    //If there is unallocated Timer, will return initialized but stopped timer
    Timer* getNewTimer(std::uint32_t initialDelay,
               std::uint32_t periodTime,
               std::uint32_t numPeriods,
               ITimer::ITimerCallback pCallbackFunc,
               void * pCallbackArgs);

    //Free from use. Pass only Timer* obtained with TimerThread::getNewTimer(...)
    virtual bool freeTimer(ITimer*);

  protected:

    Timer* _timers_array[MAX_NUM_TIMERS];
    unsigned _last_newd_timer_idx;

    void run(void* );

  private:

    TimerThread(const TimerThread& right);
    TimerThread& operator=(const TimerThread& right);

    int _initTimer(Timer* tmr,
                   std::uint32_t initialDelay,
                   std::uint32_t periodTime,
                   std::uint32_t numPeriods,
                   ITimer::ITimerCallback pCallbackFunc,
                   void * pCallbackArgs);
};

#endif /* TIMER_THREAD_H */
