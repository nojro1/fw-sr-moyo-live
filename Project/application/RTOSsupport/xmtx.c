/*
----------------------------------------------------------------------
File    : xmtx.c
Purpose : xmtx system interface -- thread locking and unlocking
          functions, adapted to FreeRTOS
--------- END-OF-HEADER ----------------------------------------------
*/

#include <yvals.h>
#include "FreeRTOS.h"
#include "semphr.h"

#if (_DLIB_THREAD_SUPPORT != 0)  // Used in multi thread supported libraries only

_STD_BEGIN

/*********************************************************************
*
*       Global functions
*
**********************************************************************
*/

/*********************************************************************
*
*       __iar_system_Mtxinit()
*/
void __iar_system_Mtxinit(__iar_Rmtx *m) 
{
    *m = xSemaphoreCreateMutex();
    configASSERT(*m != 0);
}

/*********************************************************************
*
*       __iar_system_Mtxdst()
*/
void __iar_system_Mtxdst(__iar_Rmtx *m) 
{
    vSemaphoreDelete(*m); 
}

/*********************************************************************
*
*       __iar_system_Mtxlock()
*/
void __iar_system_Mtxlock(__iar_Rmtx *m) 
{
    while(pdFALSE == xSemaphoreTake(*m, portMAX_DELAY))
    {
        // Just wait again in the case when INCLUDE_vTaskSuspend is not enabled
    }  
}

/*********************************************************************
*
*       __iar_system_Mtxunlock()
*/
void __iar_system_Mtxunlock(__iar_Rmtx *m) 
{
    BaseType_t result = xSemaphoreGive(*m);  
    configASSERT(result == pdTRUE);
}

_STD_END

/********************************************************************/

#endif // _MULTI_THREAD

/****** End Of File *************************************************/
