//! \file    guard.cpp
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    18.02.2010

// include self first
#include "guard.h"
#include "mutex.h"

// other includes

//! constructor
Guard::Guard(Mutex& mx) : m_mutex(mx)
{
    m_mutex.lock();
}

//! destructor
Guard::~Guard()
{
    m_mutex.release();
}


//======== End of guard.cpp ==========

