//! \file    mutex.cpp
//! \author  Geir Inge tellnes / Jan Arild R�yneberg
//! \version 3
//! \date    12.03.2020 for FreeRTOS / STM32L5

// include self first
#include "mutex.h"
#include "task.h"

// other includes
#include <cstdio>

//! constructor
Mutex::Mutex()
{
    m_xSem = xSemaphoreCreateMutex();
}

//! destructor
Mutex::~Mutex()
{
    vSemaphoreDelete(m_xSem);
}

//! \brief Will lock the semaphore. If another caller locks from this or another thread, the call will block
// until the Mutex has been released.
void Mutex::lock()
{
    while(pdFALSE == xSemaphoreTake(m_xSem, portMAX_DELAY))
    {
        // Just wait again in the case when INCLUDE_vTaskSuspend is not enabled
    }
}

//! \brief Will lock the semaphore. If another caller locks from this or another thread, the call will block
// until the Mutex has been released.
bool Mutex::tryLock()
{
    lock();
    return true;
}

//! \brief Unlocks/releases the semaphore
void Mutex::release()
{
    xSemaphoreGive( m_xSem );
}

//! \brief Static class method to get a Mutex.
//! \param pMx Pointer to Mutex to get. A new Mutex will be created if pMx does not point to a Mutex
//! \return Pointer to the (new or already existing) Mutex
Mutex* Mutex::get(Mutex * pMx)
{
    taskENTER_CRITICAL(); // we have to protect this since the pointer is usually static
    if (0 == pMx)
    {
       pMx = new Mutex;
    }
    taskEXIT_CRITICAL();
    return pMx;
}


//======== End of Mutex.cpp ==========

