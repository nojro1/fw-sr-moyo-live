//! \file    thread.cpp
//! \author  Geir Inge tellnes / Jan Arild R�yneberg
//! \version 3.0
//! \date    12.03.2020 for FreeRTOS / STM32L5

#include "thread.h"
#include "utilfuncs.h"
#include <algorithm>

USING_NAMESPACE_STD;

// static members init
TaskHandle_t Thread::m_pTcbs[NUMBER_OF_THREADS];
QueueHandle_t Thread::m_pMboxes[NUMBER_OF_THREADS];
Thread* Thread::m_pThread[NUMBER_OF_THREADS];
std::bitset<NUMBER_OF_THREADS> Thread::m_startedThreads;
Mutex* Thread::m_pStartedThreadsMutex = 0;
Mutex* Thread::m_pConsoleMutex = 0;


Thread::Thread()
  : m_threadId(P_UNDEF), m_pArg_(0), m_pMbox(0), m_pMboxData(0), m_xLastWakeTime(( TickType_t ) 0)
{
}

Thread::~Thread()
{
    taskENTER_CRITICAL();

    // reclaim resources
    if (m_threadId > 0)
    {
        if (m_threadId <= ARR_ELEMS(Thread::m_pTcbs))
        {
            Thread::m_pTcbs[m_threadId-1] = 0;
        }
        if (m_threadId <= ARR_ELEMS(Thread::m_pMboxes))
        {
            Thread::m_pMboxes[m_threadId-1] = 0;
        }
        if (m_threadId <= ARR_ELEMS(Thread::m_pThread))
        {
            Thread::m_pThread[m_threadId-1] = 0;
        }
    }
    delete[] m_pMboxData;

    taskEXIT_CRITICAL();

    vQueueDelete(m_pMbox);
    vTaskDelete(m_tcb);


}

//! \brief Startup function for the thread object
//! \param threadName Character array containing the thread's name
//! \param threadId Thread identifier enum
//! \param priority Thread priority. Higher numbers = higher priority
//! \param stackSize The thread's stack-size (in platform specific stack depth (i.e. NOT bytes)
//! \param maxNumMsg Maximum number of mailbox messages. 0=no mailbox
//! \param pThreadArgument Pointer to user argument for thread callback
//! \param pHWObj Pointer to hardware abstraction object

void Thread::start( const char *threadName, THREAD_ID threadId, uint8_t priority,
                   uint32_t stackSize, uint8_t maxNumMsg, void *pThreadArgument)
{
    // parameter check + prevent multiple calls
    if (P_UNDEF == m_threadId && threadId != P_UNDEF && threadId <= NUMBER_OF_THREADS &&
        stackSize != 0)
    {
        m_threadId = threadId;                                      // Thread identifier
        m_pArg_ = pThreadArgument;                                  // User arg for use by the thread

        Thread::m_pTcbs[m_threadId-1] = m_tcb;                      // keep track of TCB pointers
        Thread::m_pThread[m_threadId-1] = this;                     // register the address of the object

        if (maxNumMsg != 0) // only start mailbox if the thread need it !
        {
            m_pMboxData = new uint8_t[sizeof(IPC)];
            m_pMbox = xQueueCreate( maxNumMsg, sizeof( IPC ) );
            if( m_pMbox != NULL )
            {
                vQueueAddToRegistry( m_pMbox, threadName );
            }
            Thread::m_pMboxes[m_threadId-1] = m_pMbox;             // keep track of mailbox pointers
        }

        /* Create the task, storing the handle.  Note that the passed parameter 
           ucParameterToPass must exist for the lifetime of the task, so in this 
           case is declared static.  If it was just an an automatic stack variable 
           it might no longer exist, or at least have been corrupted, by the time
           the new task attempts to access it. */
         if (xTaskCreate( Thread::entryPoint, threadName, stackSize, this, priority, &m_tcb ) != pdPASS)
         {
            //APP_ERROR_HANDLER(NRF_ERROR_NO_MEM); //TODO
         }
         configASSERT( m_tcb );
    }


}

//! \brief Waits until the thread has terminated
//! Feasible only in environments where 'main()' can exit
void Thread::join()
{
    while (m_threadId != P_UNDEF)
    {
        sleep(10);
    }
}


//! \brief The physical entry point for the os task sceduler startup sequence.
//! This function is 'static', and is not allowed to use the 'this' pointer.
//! The pointer is passed as the argument from the RTOS call.
//! If the thread exits, its resources will be reclaimed
//! \param pTh Pointer to Thread object
void Thread::entryPoint(void *pTh)
{
    Thread *pp = reinterpret_cast<Thread*>(pTh);
    
    // Initialise the xLastWakeTime variable with the current time.
	pp->m_xLastWakeTime = xTaskGetTickCount();
    
    pp->run(0); // will not return unless thread has terminated

    DEBUG_TRACE( "\r\n%s terminated\r\n", pp->getThreadName() );

    // reclaim resources after thread exit
    taskENTER_CRITICAL();

    if (pp->m_threadId > 0)
    {
        if (pp->m_threadId <= Thread::m_startedThreads.size())
        {
            Thread::m_startedThreads.reset(pp->m_threadId-1);
        }
        if (pp->m_threadId <= ARR_ELEMS(Thread::m_pTcbs))
        {
            Thread::m_pTcbs[pp->m_threadId-1] = 0;
        }
        if (pp->m_threadId <= ARR_ELEMS(Thread::m_pMboxes))
        {
            Thread::m_pMboxes[pp->m_threadId-1] = 0;
        }
        if (pp->m_threadId <= ARR_ELEMS(Thread::m_pThread))
        {
            Thread::m_pThread[pp->m_threadId-1] = 0;
        }
    }

    pp->m_threadId = P_UNDEF;
    pp->m_pArg_ = 0;

    delete[] pp->m_pMboxData;
    pp->m_pMboxData = 0;

    taskEXIT_CRITICAL();

    vQueueDelete(pp->m_pMbox);
    pp->m_pMbox = 0;

    vTaskDelete(pp->m_tcb);
}


//!
//! \brief Dispatch (sends) an IPC signal/message to a specific thread that owns a mailbox
//! \param threadId Thread identifier of receiving thread
//! \param sig Signal identifier enum
//! \param d1 Optional user data 1
//! \param d2 Optional user data 2
//! \param pData Optional pointer to user data (for structs etc..)
//! \param hiPri Indicates if this is a high priority signal
//!
void Thread::sigDispatch(THREAD_ID threadId, THREAD_SIGNAL sig,
                         uint32_t d1, uint32_t d2, void * pData,
                         bool hiPri)
{
    IPC ipc(sig, d1, d2, pData);

    /* Send a pointer to a IPC object.  Block if the queue is already full. */
    if(hiPri)   //put message at the beginning of the mailbox
    {
        xQueueSendToFront( Thread::m_pMboxes[threadId-1], reinterpret_cast<void*>(&ipc), portTICK_PERIOD_MS  );   
    }
    else        //put message in queue
    {
        xQueueSendToBack( Thread::m_pMboxes[threadId-1], reinterpret_cast<void*>(&ipc), portTICK_PERIOD_MS  );   
    }
}

//!
//! \brief Dispatch (sends) an IPC signal/message to ALL active threads that own a mailbox\n
//! i.e. a broadcast
//! \param sig Signal identifier enum
//! \param d1 Optional user data 1
//! \param d2 Optional user data 2
//! \param pData Optional pointer to user data (for structs etc..)
//! \param hiPri Indicates if this is a high priority signal
//!
void Thread::sigDispatch(THREAD_SIGNAL sig,
                         uint32_t d1, uint32_t d2, void * pData,
                         bool hiPri)
{
    for (std::size_t threadId = P_UNDEF+1; threadId <= NUMBER_OF_THREADS; ++threadId)
    {
        if (Thread::m_pMboxes[threadId-1] != 0) // does the thread have a mailbox?
        {
            sigDispatch(static_cast<THREAD_ID>(threadId), sig, d1, d2, pData, hiPri);
        }
    }
}


//! \brief Dispatch (sends) an IPC signal/message to a specific thread that owns
//!        a mailbox if the mailbox is not full.
//! \note This function can be called in timer or interrupt context.
//! \param threadId Thread identifier of receiving thread
//! \param sig Signal identifier enum
//! \param d1 Optional user data 1
//! \param d2 Optional user data 2
//! \param pData Optional pointer to user data (for structs etc..)
//! \param hiPri Indicates if this is a high priority signal
//! \retval true  Signal successfully dispatched
//! \retval false Mailbox full, no signal sent
bool Thread::sigDispatchCond(THREAD_ID threadId, THREAD_SIGNAL sig,
                             uint32_t d1, uint32_t d2, void * pData,
                             bool hiPri)
{
    IPC ipc(sig, d1, d2, pData);
    bool mailbox_full;

    /* Send a pointer to a IPC object.  Don't block if the queue is already full. */
    if(hiPri)   //put message at the beginning of the mailbox
    {
        mailbox_full = xQueueSendToFront( Thread::m_pMboxes[threadId-1], reinterpret_cast<void*>(&ipc), ( TickType_t ) 0  ) != pdTRUE ;   
    }
    else        //put message in queue
    {
        mailbox_full = xQueueSendToBack( Thread::m_pMboxes[threadId-1], reinterpret_cast<void*>(&ipc), ( TickType_t ) 0  ) != pdTRUE ;   
    }
    return mailbox_full;
}


//! \brief Receives (waits for) IPC signal/message, with timeout
//! \param timeout Timeout in milliseconds
//! \param ipc Pointer to the IPC message
void Thread::sigWait(uint32_t timeout, IPC* ipc)
{
    ipc->m_sig = S_OSTIMEOUT; // init to timeout
    if (timeout != 0)
    {
        xQueueReceive( m_pMbox, ipc, timeout);
    }
    else
    {
        xQueueReceive( m_pMbox, ipc, portTICK_PERIOD_MS);
    }
}

//! \brief Receives (waits for) IPC signal/message, without timeout
//! \param ipc Pointer to the IPC message
void Thread::sigWait(IPC* ipc)
{
    sigWait(0, ipc);
}

//! \brief Checks if a IPC signal/message is available
//! \param ipc Pointer to the IPC message
//! \return true if message available, else false
//! \remark Does not remove the message from the mailbox queue !
bool Thread::sigPeek(IPC* ipc)
{
    ipc->m_sig = S_UNDEF; // init to undefined
    return xQueuePeek( m_pMbox, ipc, ( TickType_t ) 0 ) == pdTRUE ;
}

//! \brief Purge all queued signals
void Thread::sigClear()
{
    if (m_pMbox)
        xQueueReset( m_pMbox );
}


//! \brief Helper method for tracing unknown signals
//! \param ipc Pointer to the IPC message
void Thread::sigUnknown(const IPC* ipc) const
{
    DEBUG_TRACE("\r\n%s-->%d\r\n", getThreadName(), ipc->m_sig);
}

//! \brief Suspend thread for 'millisec' mS
//! \param millisec Number of milliseconds to suspend the thread
void Thread::sleep(uint32_t millisec)
{
    const TickType_t xDelay = millisec / portTICK_PERIOD_MS;
    vTaskDelayUntil( &m_xLastWakeTime, xDelay );
}

//! \brief Returns the assigned thread name
const char * Thread::getThreadName()
{
    return pcTaskGetTaskName( xTaskGetCurrentTaskHandle() );
}

//! \brief Returns the current thread priority
uint8_t Thread::getPriority() const
{
    return uxTaskPriorityGet( xTaskGetCurrentTaskHandle() );
}

//! \brief Set new priority to current thread
//! \param priority The new priority
//! \return The previous priority
uint8_t Thread::setPriority(uint8_t priority) const
{
    uint8_t prevPrio = getPriority();
    vTaskPrioritySet( xTaskGetCurrentTaskHandle(), priority );
    return prevPrio;
}

//! \brief To check if all threads have started
//! \return true if all threads have started, else false
bool Thread::areAllThreadsStarted() const
{
    Guard g(*(Thread::m_pStartedThreadsMutex = Mutex::get(Thread::m_pStartedThreadsMutex))); // will protect this resource until out of scope

    if (m_threadId > 0 && m_threadId <= Thread::m_startedThreads.size())
    {
        Thread::m_startedThreads.set(m_threadId-1);
    }

    return Thread::m_startedThreads.count() == Thread::m_startedThreads.size();
}

//! \brief Waits until all threads have started
void Thread::waitUntilAllThreadsStarted() 
{
    while(! areAllThreadsStarted() )       //Wait for last thread to start
    {
        sleep(10);
    }
}

//! \brief Mark this threads as started.
//! \remark For use where some of your threads do not call waitUntilAllThreadsStarted() at entry of run(), while other threads do
void Thread::markThreadAsStarted() const
{
    Guard g(*(Thread::m_pStartedThreadsMutex = Mutex::get(Thread::m_pStartedThreadsMutex))); // will protect this resource until out of scope
    if (m_threadId > 0 && m_threadId <= Thread::m_startedThreads.size())
    {
        Thread::m_startedThreads.set(m_threadId-1);
    }
}

//! \brief Get pointer to a task control block
//! (mainly to be used from a console for system info)
//! \param threadId The thread identified to get the tcb pointer for
//! \return Pointer to the requested task control block
TaskHandle_t Thread::getpTcb(THREAD_ID threadId)
{

    if (threadId > 0 && threadId <= ARR_ELEMS(Thread::m_pTcbs))
    {
        return Thread::m_pTcbs[threadId-1];
    }
    return 0;
}

//! \brief Get pointer to a thread
//! \param threadId The thread identifier
//! \return Pointer to the requested thread object
Thread* Thread::getpThread(THREAD_ID threadId)
{

    if (threadId > 0 && threadId <= ARR_ELEMS(Thread::m_pThread))
    {
        return Thread::m_pThread[threadId-1];
    }
    return 0;
}

void Thread::suspend()
{
    vTaskSuspend(this->m_tcb);
}

void Thread::resume()
{
    vTaskResume(this->m_tcb);
}

void Thread::yield()
{
  taskYIELD();
}

bool Thread::suspended() const
{
  return eSuspended == eTaskGetState(this->m_tcb);
}

//======== End of thread.cpp ==========

