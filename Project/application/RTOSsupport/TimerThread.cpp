//! \file    TimerThread.h
//! \author  DRY
//! \version 1.0
//! \date    24.04.2012
//! \brief   Manages & Runs low-priority app-level timers

#include "TimerThread.h"
//Basic tick ms time info
#include <sysdefs.h>
#include <cassert>

int
TimerThread::Timer::restart()
{
  if (!_free)
  { //can't restart free (un-init'ed) timer
    _stopped=true; //run(): signal not to use
    //Thread::sleep(1); // run(): make sure it finished with current
    _ms_tick_count = 0;
    _curr_period_time = 0;
    _curr_period_count = _period_count;
    _expired = false;
    _stopped = false; //run(): use it
    return 0;
  }
  return -1;
}

TimerThread::TimerThread()
  :_last_newd_timer_idx(MAX_NUM_TIMERS-1)
{
  for (int i=0; i < MAX_NUM_TIMERS; ++i)
  {
    //Note: invalid ref this object before it completely contructed.
    //but we know Timer won't use us until we're done
    _timers_array[i] = new TimerThread::Timer(*this);
  }
}

TimerThread::~TimerThread()
{
  for (int i=0; i < MAX_NUM_TIMERS; ++i)
  {
    //make sure its not running
    freeTimer(_timers_array[i]);
    delete _timers_array[i];
  }
}

ITimer*
TimerThread::newTimer()
{
  unsigned next_timer_idx=-1;
  Timer* tmr(0);

  next_timer_idx = (_last_newd_timer_idx +1) %  MAX_NUM_TIMERS;
  while(next_timer_idx  != _last_newd_timer_idx)
  {
    if (_timers_array[next_timer_idx]->_free ||
        _timers_array[next_timer_idx]->_expired)
    {
      tmr = _timers_array[next_timer_idx];
      _last_newd_timer_idx = next_timer_idx;
      break;
    }
    next_timer_idx = (next_timer_idx +1) %  MAX_NUM_TIMERS;
  }

  return tmr;
}

TimerThread::Timer*
TimerThread::getNewTimer(std::uint32_t init_delay,
           std::uint32_t per_time,
           std::uint32_t num_periods,
           ITimer::ITimerCallback cb,
           void * cb_args)
{
  Timer* tmr = static_cast<Timer*>( newTimer() );

  int res = _initTimer(tmr, init_delay, per_time, num_periods, cb, cb_args);
  assert(0==res);

  return tmr;
}

bool
TimerThread::freeTimer(ITimer* tptr)
{
  //Note: the timers ptr table gets inited only once / no changes
  //Can read it without locking. Check if this is a valid ptr
  Timer* rptr(0);

  for (int i=0; i < MAX_NUM_TIMERS; ++i)
  {
    if (tptr == _timers_array[i])
    {
      void* vp = tptr;
      rptr = static_cast<Timer*>(vp);
      break;
    }
  }

  if (rptr)
  {
    rptr->_usrCbFunc = 0;  //run() : don't call in case about to do it
    rptr->_stopped = true; //run() : don't call next iteration
    rptr->_free = true;    //can new it now
  }

  //dbg: inform that you trying to use illegal timer
  assert(rptr);
  return rptr ? true : false;
}

//Note: this is private method to be called by TimerThread::Timer when someone creates it without
//using the TimerThread::getNewTimer(). Note: no check if the caller is one of our Timers - its
//assumed.
int
TimerThread::_initTimer(Timer* tmr, std::uint32_t init_delay, std::uint32_t period_time,
                        std::uint32_t num_periods, ITimer::ITimerCallback cb, void * cb_args)
{
  if (tmr)
  {
    tmr->_stopped = true;
    tmr->_expired = false;
    tmr->_free = false;

    tmr->_periodic_time = period_time;
    if (period_time != 0 && num_periods != 0)
    {
      tmr->_period_count = num_periods;//-1;
      tmr->_curr_period_count = tmr->_period_count;
    }
    else
    {
      tmr->_period_count = -1;
      //tmr->_curr_period_count = -1;
    }
    tmr->_usrCbFunc = cb;
    tmr->_usrCbArgs = cb_args;
    tmr->_initial_delay = init_delay;
    tmr->_ms_tick_count = 0;
    tmr->_curr_period_time = 0;
    return 0;
  }

  return -1;
}

void
TimerThread::run(void* args)
{
  TimerThread::Timer* t(0);

  markThreadAsStarted(); // mark this thread as started so that if application calls waitUntilAllThreadsStarted() in its thread run entry,
                         // it will not stall on this thread not having been marked as started

  for (;;)
  {
    //Note: Thread::sleep(1) is a real-time sleep, 1 = sys granularity
    rtos_support::Thread::sleep(1);

    //Go through list of timers and if they're non-free (are used) then increment them and
    //call the callback if any.
    //Note: timers are NOT locked while executing (add later if needed), and timer list doens't
    //need to be locked.
    for (int i=0; i < MAX_NUM_TIMERS; ++i)
    {
      t = _timers_array[i];
      if (!t->_stopped) //read-only
      {
        t->_ms_tick_count += SYSTICK_TIME;
        t->_curr_period_time += SYSTICK_TIME;
        if (t->_periodic_time <= t->_curr_period_time)
        {
          if (t->_initial_delay <= t->_ms_tick_count)
          {
            //if (0 != t->_usrCbFunc && t->_curr_period_count >0)
            if (!(t->_expired || 0==t->_usrCbFunc))
            {
              t->_usrCbFunc(t->_usrCbArgs);
              if (t->_period_count != -1) //if not indefinite
              {
                if (0 == --t->_curr_period_count)
                  t->_expired = true;//remove this time from use
              }
              if (0==t->_periodic_time) // if one-shot timer
              {
                  t->_expired = true;
              }
            }
          }
          t->_curr_period_time = 0;
        }//if(timer period up)
      }//if(timer is used)
    }
  }//for(;;)
}
