//! \file    Cmutex.cpp
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    23.03.2015

// include self first
#include "cmutex.h"
#include "thread.h"

// other includes
#include <cstdio>

//! constructor
Cmutex::Cmutex()
{
    OS_CreateCSema(&m_sem, 1);
}

//! destructor
Cmutex::~Cmutex()
{
    OS_DeleteCSema(&m_sem);
}

//! \brief Will lock the semaphore. If another caller locks from this or another thread, the call will block
// until the Mutex has been released.
void Cmutex::lock()
{
    OS_WaitCSema(&m_sem);
}

//! \brief Will lock the semaphore. If another caller locks from this or another thread, the call will block
// until the Mutex has been released.
bool Cmutex::tryLock()
{
    lock();
    return true;
}

//! \brief Unlocks/releases the semaphore
void Cmutex::release()
{
    OS_SignalCSema(&m_sem);
}

//! \brief Static class method to get a Mutex.
//! \param pMx Pointer to Mutex to get. A new Mutex will be created if pMx does not point to a Mutex
//! \return Pointer to the (new or already existing) Mutex
Cmutex* Cmutex::get(Cmutex * pMx)
{
    OS_EnterRegion(); // we have to protect this since the pointer is usually static
    if (0 == pMx)
    {
       pMx = new Cmutex;
    }
    OS_LeaveRegion();
    return pMx;
}


//======== End of Cmutex.cpp ==========

