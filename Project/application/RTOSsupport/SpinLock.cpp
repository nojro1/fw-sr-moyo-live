//! \file    SpinLock.h
//! \author  DRY
//! \version 1.0
//! \date    11.07.2011
//! \brief   Implements a spin-lock for the Cortex-m3

#include <SpinLock.h>
#include <cm3_spinlock.h>
#include <cassert>

SpinLock::SpinLock(const int& inistate)
: _lock(inistate)
{
  assert(_lock == 0 || _lock == 1);
}

void SpinLock::lock() { _cm3_get_lock( &_lock ); }

void SpinLock::unlock() { _cm3_free_lock( &_lock ); }
