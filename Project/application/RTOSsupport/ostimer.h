//! \file    ostimer.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    18.02.2010



#ifndef _OSTIMER_H
#define _OSTIMER_H

// header includes
#include "cstdint"
#include "rtos.h"
#include <ITimer.h>

// forward declarations
// ...

//! \brief Wrapper for RTOS timers

class OsTimer
  : public ITimer
{
public:

    // constructor/destructor
    OsTimer();
    virtual ~OsTimer();

    //! \brief Re-typedef (for convenience)
    typedef ITimerCallback OSTimerCallback;

    int create(std::uint32_t initialDelay, 
               std::uint32_t periodTime, 
               std::uint32_t numPeriods, 
               OSTimerCallback pCallbackFunc, 
               void * pCallbackArgs);

    int remove();

    int start();
    int restart();
    int stop();

    bool isRunning();

protected:

    static void myStaticCallback( TimerHandle_t pxTimer );

private:
    OsTimer(const OsTimer& right);            //!< block the copy constructor
    OsTimer& operator=(const OsTimer& right); //!< block the assignment operator

    TimerHandle_t   m_timer;
    OSTimerCallback m_pCallbackFunc;          //!< user callback routine
    void * m_pCallbackArgs;

    std::uint32_t m_periodicTime;
    std::uint32_t m_periodCount;
};

class OsTimerFactory
: public ITimerFactory
{
public:

  virtual ITimer* newTimer() { return new OsTimer; }

  virtual bool freeTimer(ITimer* tmr) { delete tmr; return true; }

};

#endif   // _OSTIMER_H

//======== End of ostimer.h ==========


