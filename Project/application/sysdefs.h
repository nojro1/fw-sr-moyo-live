//
// Common system defines
//

#ifndef _SYSDEFS_H_
#define _SYSDEFS_H_

//#include "DfuBootloadCommon.h"

#define STATUS_MONITOR 1    //Display status messages at console port
#define USART1_BAUD 115200
#define USART2_BAUD 9600
#define USART3_BAUD 115200

#define USART1_RXBUFSZ 128
#define USART1_TXBUFSZ 128
#define USART2_RXBUFSZ 128
#define USART2_TXBUFSZ 128
#define USART3_RXBUFSZ 128
#define USART3_TXBUFSZ 128

#define CONSOLE_USART 3 //0 = Virtual COM, 2 = UART2 //Currently (Nucleo L552) occupied

#define CONSOLE_BAUD 115200
#define CONSOLE_RXBUFSZ 16
#define CONSOLE_TXBUFSZ 64
#if CONSOLE_USART == 1
    #undef USART1_BAUD
    #undef USART1_RXBUFSZ
    #undef USART1_TXBUFSZ
    #define USART1_BAUD CONSOLE_BAUD
    #define USART1_RXBUFSZ CONSOLE_RXBUFSZ
    #define USART1_TXBUFSZ CONSOLE_TXBUFSZ
#elif CONSOLE_USART == 2
    #undef USART2_BAUD
    #undef USART2_RXBUFSZ
    #undef USART2_TXBUFSZ
    #define USART2_BAUD CONSOLE_BAUD
    #define USART2_RXBUFSZ CONSOLE_RXBUFSZ
    #define USART2_TXBUFSZ CONSOLE_TXBUFSZ
#elif CONSOLE_USART == 3
    #undef USART3_BAUD
    #undef USART3_RXBUFSZ
    #undef USART3_TXBUFSZ
    #define USART3_BAUD CONSOLE_BAUD
    #define USART3_RXBUFSZ CONSOLE_RXBUFSZ
    #define USART3_TXBUFSZ CONSOLE_TXBUFSZ
#endif

#define HOSTCOMM_BAUD 115200
#define HOSTCOMM_RXBUFSZ 0x200
#define HOSTCOMM_TXBUFSZ 0x200

//! \enum THREAD_ID
//! \brief Thread numbering/identifiers
enum THREAD_ID {
  P_UNDEF=0, P_SUPER, P_CONS, P_FILE, P_UI, P_ADC, NUMBER_OF_THREADS = P_ADC
};


#define SYSTICK_TIME 1      //RTOS system tick time in mS

static const uint16_t MAX_DEV_ID_SIZE = 12;

#define IAP_UPLOAD_PATTERN 0xCAFEBEEF //IAP upload active

#endif // _SYSDEFS_H_
