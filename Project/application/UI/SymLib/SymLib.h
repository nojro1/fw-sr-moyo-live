/*****************************************************************************
 *
 * Copyright: (c) Laerdal Medical 2006-2014
 *
 * %name:          SymLib.h %
 * %version:       1 %
 * %created_by:    nogte1 / nojro1 %
 *
 * Note: Adapted from Littlesister. Rewritten to fit MOYO
 *
 */


//! @class SymLib
//! @brief Symbol library wrapper
//!
//! @author LOL/GIT/JRO
//!


#ifndef SYMLIB_H
#define SYMLIB_H

// header includes
#include <gdisp.h>
#include "includes.h"

class SymLib
{
    public:

    // 37x56, 8 bit
    struct csym37x56x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[37*56];
    };
    
    // 37x56, 16 bit
    struct csym37x56x16_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[37*56*2];
    };
    
    // 19x27, 8 bit
    struct csym19x27x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[19*27];
    };
    
    // 55x27, 8 bit
    struct csym55x27x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[55*27];
    };
    
    // 24x7, 16 bit
    struct csym24x7x16_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[24*7*2];
    };
    
    // 109x56, 16 bit
    struct csym109x56x16_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[109*58*2];
    };
    
    // 109x56, 8 bit
    struct csym109x56x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[109*58];
    };
    
    // 21x7, 8 bit
    struct csym21x7x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[21*7];
    };
    
    // 13x28, 8 bit
    struct csym13x28x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[13*28];
    };
    
    // 133x103, 8 bit
    struct csym133x103x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[133*103];
    };
    
    // 32x61, 8 bit
    struct csym32x61x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[32*61];
    };
    
    // 133x125, 8 bit
    struct csym133x125x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[133*125];
    };
    
    // 24x59, 4 bit
    __packed struct csym24x59x4_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[(24*59)/2];
    };
    
    // 46x75, 1 bit
    __packed struct sym46x75x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((46/8)+1)*75];
    };

    // 46x75, 16 bit
    struct csym46x75x16_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[(46*75)*2];
    };
 
    // 153x62, 8 bit
    struct csym153x62x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[153*62];
    };
    
    // 133x79, 8 bit
    struct csym133x79x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[133*79];
    };
    
    // 85x85, 8 bit
    struct csym85x85x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[85*85];
    };
    
    // 76x75, 8 bit
    struct csym76x75x8_t
    {
        GCSYMHEAD sh;
        SGUCHAR b[76*75];
    };
    
    // 4x6, 1 bit
    __packed struct sym4x6x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((4/8)+1)*6];
    };
    
    // 20x10, 1 bit
    __packed struct sym20x10x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((20/8)+1)*10];
    };
    
    // 22x23, 1 bit
    __packed struct sym22x23x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((22/8)+1)*23];
    };
    
    // 15x10, 1 bit
    __packed struct sym15x10x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((15/8)+1)*10];
    };
    
    // 43x21, 1 bit
    __packed struct sym43x21x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((43/8)+1)*21];
    };
    
    // 20x14, 1 bit
    __packed struct sym20x14x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((20/8)+1)*14];
    };
    
    // 15x39, 1 bit
    __packed struct sym15x39x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((15/8)+1)*39];
    };
    
    // 8x37, 1 bit
    __packed struct sym8x37x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((8/8)+1)*37];
    };
    
    // 76x76, 1 bit
    __packed struct sym76x76x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((76/8)+1)*76];
    };
    
    // 30x12, 1 bit
    __packed struct sym30x12x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((30/8)+1)*12];
    };
    
    // 56x34, 1 bit
    __packed struct sym56x34x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((56/8)+1)*34];
    };
    
    // 12x12, 1 bit
    __packed struct sym12x12x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((12/8)+1)*12];
    };
    
    // 20x12, 1 bit
    __packed struct sym20x12x1_t
    {
        GSYMHEAD sh;
        SGUCHAR b[((20/8)+1)*12];
    };
    
    
    
    static const csym37x56x8_t   greenDigits[];
    static const csym37x56x16_t  yellowDigits[];
    static const csym37x56x8_t   redDigits[];
    static const csym19x27x8_t   greyDigits[];
    static const csym55x27x8_t   questionMarkGrey[];
    static const csym153x62x8_t  LGHlogo[];
    static const csym85x85x8_t   criticalError[];
    static const csym76x75x8_t   alarmAckBig[];
    static const csym32x61x8_t   batteryCapacityBlockBig[];
    static const csym133x125x8_t batteryEmptyBig[];
    static const sym8x37x1_t     batteryCapacityEmptyBlockBig[];
    static const sym20x14x1_t    batteries[];
    static const csym133x103x8_t batteryFrameBig[];
    static const csym133x79x8_t  batteryLowBig[];
    static const csym133x79x8_t  batteryCriticalBig[];
    static const csym13x28x8_t   woman[];
    static const csym24x7x16_t   bpm[];
    static const csym21x7x8_t    bpm_grey[];
    static const sym20x12x1_t    volumeOn[];
    static const sym20x12x1_t    volumeOff[];
    static const csym109x56x8_t  questionMarkRed[];
    static const csym109x56x16_t questionMarkYellow[];
    static const sym15x39x1_t    mother[];
    static const sym76x76x1_t    error[];
    static const sym30x12x1_t    usb[];
    static const sym12x12x1_t    alarm[];
    static const sym12x12x1_t    alarmAck[];
    static const sym56x34x1_t    batteryCharge[];
    
    protected:

    private:

    };


#endif   // SYMLIB_H

//======== End of $Workfile: SymLib.h $ ==========


