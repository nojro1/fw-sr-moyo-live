//! \file    SymLib.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    10.07.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  10jul2013, JRo, Original.


// include self first
#include "UI/Symlib/symlib.h"


#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym37x56x8_t SymLib::greenDigits[] =
{
    #include "UI/SymLib/Symbols/green/Green_0.sym"
    ,
    #include "UI/SymLib/Symbols/green/Green_1.sym"
    ,
    #include "UI/SymLib/Symbols/green/Green_2.sym"
    ,
    #include "UI/SymLib/Symbols/green/Green_3.sym"
    ,
    #include "UI/SymLib/Symbols/green/Green_4.sym"
    ,
    #include "UI/SymLib/Symbols/green/Green_5.sym"
    ,
    #include "UI/SymLib/Symbols/green/Green_6.sym"
    ,
    #include "UI/SymLib/Symbols/green/Green_7.sym"
    ,
    #include "UI/SymLib/Symbols/green/Green_8.sym"
    ,
    #include "UI/SymLib/Symbols/green/Green_9.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym37x56x16_t SymLib::yellowDigits[] =
{
    #include "UI/SymLib/Symbols/yellow/Yellow_0.sym"
    ,
    #include "UI/SymLib/Symbols/yellow/Yellow_1.sym"
    ,
    #include "UI/SymLib/Symbols/yellow/Yellow_2.sym"
    ,
    #include "UI/SymLib/Symbols/yellow/Yellow_3.sym"
    ,
    #include "UI/SymLib/Symbols/yellow/Yellow_4.sym"
    ,
    #include "UI/SymLib/Symbols/yellow/Yellow_5.sym"
    ,
    #include "UI/SymLib/Symbols/yellow/Yellow_6.sym"
    ,
    #include "UI/SymLib/Symbols/yellow/Yellow_7.sym"
    ,
    #include "UI/SymLib/Symbols/yellow/Yellow_8.sym"
    ,
    #include "UI/SymLib/Symbols/yellow/Yellow_9.sym"
};
#pragma diag_default=Pa050


#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym37x56x8_t SymLib::redDigits[] =
{
    #include "UI/SymLib/Symbols/red/Red_0.sym"
    ,
    #include "UI/SymLib/Symbols/red/Red_1.sym"
    ,
    #include "UI/SymLib/Symbols/red/Red_2.sym"
    ,
    #include "UI/SymLib/Symbols/red/Red_3.sym"
    ,
    #include "UI/SymLib/Symbols/red/Red_4.sym"
    ,
    #include "UI/SymLib/Symbols/red/Red_5.sym"
    ,
    #include "UI/SymLib/Symbols/red/Red_6.sym"
    ,
    #include "UI/SymLib/Symbols/red/Red_7.sym"
    ,
    #include "UI/SymLib/Symbols/red/Red_8.sym"
    ,
    #include "UI/SymLib/Symbols/red/Red_9.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym19x27x8_t SymLib::greyDigits[] =
{
    #include "UI/SymLib/Symbols/grey/Grey_0.sym"
    ,
    #include "UI/SymLib/Symbols/grey/Grey_1.sym"
    ,
    #include "UI/SymLib/Symbols/grey/Grey_2.sym"
    ,
    #include "UI/SymLib/Symbols/grey/Grey_3.sym"
    ,
    #include "UI/SymLib/Symbols/grey/Grey_4.sym"
    ,
    #include "UI/SymLib/Symbols/grey/Grey_5.sym"
    ,
    #include "UI/SymLib/Symbols/grey/Grey_6.sym"
    ,
    #include "UI/SymLib/Symbols/grey/Grey_7.sym"
    ,
    #include "UI/SymLib/Symbols/grey/Grey_8.sym"
    ,
    #include "UI/SymLib/Symbols/grey/Grey_9.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym55x27x8_t SymLib::questionMarkGrey[] =
{
    #include "UI/SymLib/Symbols/grey/Questionmark_grey.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym153x62x8_t SymLib::LGHlogo[] =
{
    #include "UI/SymLib/Symbols/Logo_color.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym85x85x8_t SymLib::criticalError[] =
{
    #include "UI/SymLib/Symbols/Critical_error.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym133x125x8_t SymLib::batteryEmptyBig[] = 
{
    #include "UI/SymLib/Symbols/empty_battery.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym76x75x8_t  SymLib::alarmAckBig[] =
{
    #include "UI/SymLib/Symbols/alarm_ack_big_icon.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::sym20x14x1_t SymLib::batteries[] = 
{
    #include "UI/SymLib/Symbols/battery_empty.sym"
    ,
    #include "UI/SymLib/Symbols/battery_1.sym"
    ,
    #include "UI/SymLib/Symbols/battery_2.sym"
    ,
    #include "UI/SymLib/Symbols/battery_3.sym"
    ,
    #include "UI/SymLib/Symbols/battery_charging.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::sym8x37x1_t SymLib::batteryCapacityEmptyBlockBig[] =
{
    #include "UI/SymLib/Symbols/BatteryCapacityEmptyBlockBig.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym32x61x8_t SymLib::batteryCapacityBlockBig[] =
{
    #include "UI/SymLib/Symbols/BigBatteryChargingbar1off.sym"
    ,
    #include "UI/SymLib/Symbols/BigBatteryChargingbar1on.sym"
    ,
    #include "UI/SymLib/Symbols/BigBatteryChargingbar2off.sym"
    ,
    #include "UI/SymLib/Symbols/BigBatteryChargingbar2on.sym"
    ,
    #include "UI/SymLib/Symbols/BigBatteryChargingbar3off.sym"
    ,
    #include "UI/SymLib/Symbols/BigBatteryChargingbar3on.sym"
    
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym133x103x8_t SymLib::batteryFrameBig[] =
{
    #include "UI/SymLib/Symbols/BigBatteryChargingFrame.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym133x79x8_t SymLib::batteryLowBig[] =
{
    #include "UI/SymLib/Symbols/Battery_low_big.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym133x79x8_t SymLib::batteryCriticalBig[] =
{
    #include "UI/SymLib/Symbols/BigBatterycriticallylowbattery.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym13x28x8_t SymLib::woman[] =
{
    #include "UI/SymLib/Symbols/grey/woman_icon_grey_background.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::sym20x12x1_t  SymLib::volumeOff[] =
{
    #include "UI/SymLib/Symbols/volume_off.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::sym20x12x1_t  SymLib::volumeOn[] =
{
    #include "UI/SymLib/Symbols/volume_on.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym24x7x16_t  SymLib::bpm[] =
{
    #include "UI/SymLib/Symbols/green/bpm_green.sym"
    ,
    #include "UI/SymLib/Symbols/yellow/bpm_yellow.sym"
    ,
    #include "UI/SymLib/Symbols/red/bpm_red.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym21x7x8_t  SymLib::bpm_grey[] =
{
    #include "UI/SymLib/Symbols/grey/BPM_grey.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym109x56x8_t SymLib::questionMarkRed[] =
{
    #include "UI/SymLib/Symbols/question_mark_red.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::csym109x56x16_t SymLib::questionMarkYellow[] =
{
    #include "UI/SymLib/Symbols/question_mark_yellow.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::sym15x39x1_t SymLib::mother[] =
{
    #include "UI/SymLib/Symbols/mother.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::sym76x76x1_t SymLib::error[] =
{
    #include "UI/SymLib/Symbols/error.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::sym30x12x1_t SymLib::usb[] =
{
    #include "UI/SymLib/Symbols/USB.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::sym12x12x1_t  SymLib::alarm[] =
{
    #include "UI/SymLib/Symbols/moyo_alarm.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::sym12x12x1_t  SymLib::alarmAck[] =
{
    #include "UI/SymLib/Symbols/moyo_alarm_ack.sym"
};
#pragma diag_default=Pa050

#pragma diag_suppress=Pa050 //Non-native end of line sequence
const SymLib::sym56x34x1_t SymLib::batteryCharge[] =
{
    #include "UI/SymLib/Symbols/BatteryCharging.sym"
};
#pragma diag_default=Pa050

//======== End of $Workfile: SymLib.cpp $ ==========


