/*****************************************************************************
 * Project:   LittleSister PUCK software (ARM)
 *
 * Copyright: (c) Laerdal Medical 2006
 *
 * $Workfile: SymbolContainer.h $
 * $Author: Nogte1 $
 * $Revision: 6 $
 *
 * $Log: /LittleSister/src/Puck/UI/Framework/SymbolContainer.h $
 * 
 * 6     28.09.07 23:24 Nogte1
 * Removed some warning level 3 lint warnings
 * 
 * 5     25.04.07 19:35 Nogte1
 * Optimization: made some frequently called methods return void
 *
 * 4     22.02.07 20:34 Nogte1
 * Fixed warnings from PC-Lint
 *
 * 3     26.01.07 16:47 Nolol1
 * Invalidate + SetActiveSymbol
 *
 * 2     25.01.07 16:26 Nolol1
 * Added support for nested symbols
 *
 */

//! @class SymbolContainer
//! @brief Abstract base class containing the methods related to displaying aggregated symbols
//!
//! The Viewport and Widget classes are derived from this class
//!
//! @author LOL
//!


#ifndef SYMBOL_CONTAINER_H
#define SYMBOL_CONTAINER_H

// header includes
#include "includes.h"
#include <gdisp.h>
#include <vector>

//namespace LittleSister
//{
    // forward declarations
    class Symbol;

    // typedefs
    typedef std::vector<Symbol*> SymbolCollection;

    ////

    class SymbolContainer
    {
    public:

        // methods to manipulate symbols
        size_t addSymbol( Symbol* symbol );
        Symbol* getSymbol( size_t index ) const;
        size_t removeSymbol( size_t index );

        // method to modify screen symbol
        void setActiveSymbol( uint8_t newSymbolID );

    protected:

        // constructor/destructor
        SymbolContainer();
        virtual ~SymbolContainer();

        // draw method
        void draw();

        // method to invalidate the collection
        void invalidate();

    private:

        SymbolContainer(const SymbolContainer& right);            // block the copy constructor
        SymbolContainer& operator=(const SymbolContainer& right); // block the assignment operator

        // symbols associated directly with this object
        SymbolCollection m_symbols;

        // invalidate flag (only draw if this is true)
        bool m_invalidate;
    };

//} // end namespace LittleSister

#endif   // SYMBOL_CONTAINER_H

//======== End of $Workfile: SymbolContainer.h $ ==========

