/*****************************************************************************
 * Project:   LittleSister PUCK software (ARM)
 *
 * Copyright: (c) Laerdal Medical 2006
 *
 * $Workfile: Symbol.h $
 * $Author: Nogte1 $
 * $Revision: 11 $
 *
 * $Log: /LittleSister/src/Puck/UI/Framework/Symbol.h $
 * 
 * 11    30.06.08 14:07 Nogte1
 * Ref CR-322
 * Added erase() method
 *
 * 10    9.08.07 17:21 Nogte1
 * Prepared for derived class
 *
 * 9     6.07.07 12:04 Nogte1
 * Reduced RAM usage (this class is instantiated many times!)
 *
 * 8     25.04.07 19:33 Nogte1
 * Optimization: made some frequently called methods return void
 *
 * 7     18.04.07 15:55 Nolol1
 * Implemented the copy constructor
 *
 * 6     30.03.07 21:58 Nogte1
 * Removed virtual from classes that are not supposed to be derived from
 * (to save CODE and RAM space)
 *
 * 5     20.02.07 16:25 Nogte1
 * Added methods to get symbol position
 *
 * 4     12.12.06 9:43 Nogte1
 * Added invalidation
 *
 * 3     5.12.06 12:24 Nogte1
 * Initial implementation
 *
 * 2     23.11.06 17:47 Nolol1
 * Baseline implementation (class stubs)
 *
 */

//! @class Symbol
//! @brief Abstraction of a graphical symbol / bitmap
//!
//! @author LOL
//!


#ifndef SYMBOL_H
#define SYMBOL_H

// header includes
#include <gdisp.h>
#include "includes.h"
#include <vector>

//namespace LittleSister
//{
    // forward declarations
    // ...

    ////

    class Symbol
    {
    public:

        // constructor/destructor
        Symbol(uint8_t numSymbols, GXT x, GYT y);
        virtual ~Symbol();

        // copy constructor
        Symbol( const Symbol& rhs );

        // add a symbol-item to the symbol array
        void add(const PGSYMBOL sym);

        // set active symbol-item to display
        int setActive(uint8_t symIdx);
        int setActiveFirst();
        int setActiveNext();
        int setActiveNext(bool allowWraparound);
        int setActivePrev();
        int setActivePrev(bool allowWraparound);
        int setActiveLast();

        // get active symbol-item displayed at draw()
        inline uint8_t getActive() const {return m_active;}

        // these ones can be used for fading or similar
        inline bool isFirstActive() const {return (0==m_active);}
        inline bool isLastActive() const {return ((m_symbols.size()-1) == m_active);}

        inline bool isEmpty() const {return m_symbols.empty();}

        virtual void draw();
        virtual void erase();
        

        inline void invalidate() {m_invalidate = true;}
        inline bool isInvalidated() {return m_invalidate;}

        // get (x,y) position
        inline GXT getXpos() const {return m_xpos;}
        inline GYT getYpos() const {return m_ypos;}

        // get width/height
        GXT getWidth();
        GYT getHeight();

        // get (x,y) position
        inline void setXpos( GXT xpos )     {   m_xpos = xpos;             }
        inline void setYpos( GYT ypos )     {   m_ypos = ypos;             }
        inline void setPos( GXT x, GYT y )  {   m_xpos = x;  m_ypos = y;   }
        
        //Set background color (for erase)
        void setBackgroundColor(GCOLOR color);

    protected:
        typedef std::vector<PGSYMBOL> symVector;

    private:

        Symbol& operator=(const Symbol& right); // block the assignment operator

        uint8_t m_active;
        bool m_invalidate;
        GXT m_xpos;
        GYT m_ypos;
        symVector m_symbols;
        GCOLOR m_backgroundColor;
    };

//} // end namespace LittleSister

#endif   // SCREEN_H

//======== End of $Workfile: Symbol.h $ ==========

