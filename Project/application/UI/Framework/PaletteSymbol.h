/*****************************************************************************
 * Project:   LittleSister PUCK software (ARM)
 *
 * Copyright: (c) Laerdal Medical 2007
 *
 * $Workfile: PaletteSymbol.h $
 * $Author: Nogte1 $
 * $Revision: 1 $
 *
 * $Log: /LittleSister/src/Puck/UI/Framework/PaletteSymbol.h $
 * 
 * 1     9.08.07 17:22 Nogte1
 * Initial commit of palette specialization symbols
 *
 */

//! @class PaletteSymbol
//! @brief Abstraction of a graphical symbol / bitmap using 4 bit palette
//!
//! @author GIT
//!


#ifndef PALETTESYMBOL_H
#define PALETTESYMBOL_H

// header includes
#include <gdisp.h>
#include "includes.h"
#include <vector>

// base class
#include "UI/Framework/Symbol.h"

//namespace LittleSister
//{
    // forward declarations
    // ...

    ////

    class PaletteSymbol : public Symbol
    {
    public:

        // constructor/destructor
        PaletteSymbol(uint8_t numSymbols, GXT x, GYT y, const GPALETTE_RGB * palette);
        ~PaletteSymbol();

        // copy constructor
        PaletteSymbol( const PaletteSymbol& rhs );

        void draw();


    protected:

    private:

        // note: all symbols in the array must use the same palette
        const GPALETTE_RGB * m_palette;

    };

//} // end namespace LittleSister

#endif   // PALETTESYMBOL_H

//======== End of $Workfile: PaletteSymbol.h $ ==========

