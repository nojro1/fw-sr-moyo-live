/*****************************************************************************
 * Project:   LittleSister PUCK software (ARM)
 *
 * Copyright: (c) Laerdal Medical 2007
 *
 * $Workfile: PaletteSymbol.cpp $
 * $Author: Nogte1 $
 * $Revision: 2 $
 *
 * $Log: /LittleSister/src/Puck/UI/Framework/PaletteSymbol.cpp $
 * 
 * 2     9.08.07 21:00 Nogte1
 * Fixed some lint warnings
 * 
 * 1     9.08.07 17:22 Nogte1
 * Initial commit of palette specialization symbols
 *
 */

// include self first
#include "UI/Framework/PaletteSymbol.h"

// andre includes

//namespace LittleSister
//{
    //! constructor
    //! @brief Constructs a symbol holder for palette symbols
    //! @param numSymbols Number of equally sized symbol items
    //! @param x x-coordinate of symbol
    //! @param y y-coordinate of symbol
    //! @param palette pointer to 4 bit palette
    //! @return none
    //! @author GIT
    //! @remark none
    //! @pre  none
    //! @post The add() method must be invoked afterwards. Active symbol = first symbol
    PaletteSymbol::PaletteSymbol(uint8_t numSymbols, GXT x, GYT y, const GPALETTE_RGB * palette)
                    : Symbol(numSymbols, x, y),
                      m_palette(palette)
    {
    }

    //! destructor
    PaletteSymbol::~PaletteSymbol()
    {
        m_palette = 0;
    }

    // copy constructor
    PaletteSymbol::PaletteSymbol( const PaletteSymbol& rhs ) : Symbol(rhs)
    {
        m_palette = rhs.m_palette; //lint !e1554 
                                   // (it is OK since the pointer is a const pointer)
    }

    //! draw
    //! @brief Draws the active symbol item
    //! @param none
    //! @return none
    //! @author GIT
    //! @remark none
    //! @pre none
    //! @post none
    void PaletteSymbol::draw()
    {
        if (isInvalidated())
        {
            if (m_palette)
            {
               // set the symbol's specific palette first
               ghw_palette_wr(0, 16, m_palette);
            }
            Symbol::draw(); // base class draw
        }
    }

//} // end namespace LittleSister

//======== End of $Workfile: PaletteSymbol.cpp $ ==========

