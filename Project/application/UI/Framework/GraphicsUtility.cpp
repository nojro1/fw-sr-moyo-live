/*****************************************************************************
 * Project:   LittleSister PUCK software (ARM)
 *
 * Copyright: (c) Laerdal Medical 2006
 *
 * $Workfile: GraphicsUtility.cpp $
 * $Author: Nogte1 $
 * $Revision: 5 $
 *
 * $Log: /LittleSister/src/Puck/UI/Framework/GraphicsUtility.cpp $
 * 
 * 5     29.01.07 14:58 Nogte1
 * Reversed previous change (was not needed)
 *
 * 4     29.01.07 13:40 Nogte1
 * fillBox() method now uses current viewport
 *
 * 3     1.12.06 11:31 Nolol1
 * Added fillBox() method
 *
 */

// include self first
#include "UI/Framework/GraphicsUtility.h"

// andre includes


    //! constructor
    GraphicsUtility::GraphicsUtility()
    {

    }

    //! destructor
    GraphicsUtility::~GraphicsUtility()
    {

    }

    void GraphicsUtility::fillBox( GXT xb, GYT yb, GXT xe, GYT ye, GCOLOR color )
    {
        GCOLOR c = gsetcolorf(color);
        gfillvp(xb,yb,xe,ye, 0xffff);
        gsetcolorf(c);
    }

    //! foo
    //! @brief <Brief Description>
    //! @param enParam Dette er en parameter
    //! @param enParamTil Dette er nok en parameter
    //! @return 0 dersom OK, ellers -1
    //! @author LOL
    //! @remark Dette er en kommentar
    //! @pre Eventuelle pre conditions
    //! @post Eventuelle post conditions
    // int Template::foo( int enParam, bool enParamTil )
    // {
    //     return 0;
    // }


//======== End of $Workfile: GraphicsUtility.cpp $ ==========

