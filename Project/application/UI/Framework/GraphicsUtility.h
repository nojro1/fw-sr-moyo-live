/*****************************************************************************
 * Project:   LittleSister PUCK software (ARM)
 *
 * Copyright: (c) Laerdal Medical 2006
 *
 * $Workfile: GraphicsUtility.h $
 * $Author: Nogte1 $
 * $Revision: 4 $
 *
 * $Log: /LittleSister/src/Puck/UI/Framework/GraphicsUtility.h $
 * 
 * 4     30.03.07 21:58 Nogte1
 * Removed virtual from classes that are not supposed to be derived from
 * (to save CODE and RAM space)
 *
 * 3     1.12.06 11:31 Nolol1
 * Added fillBox() method
 *
 * 2     23.11.06 17:47 Nolol1
 * Baseline implementation (class stubs)
 *
 */

//! @class GraphicsUtility
//! @brief Static utility class for graphical library function wrappers
//!
//! @author LOL
//!


#ifndef GRAPHICS_UTILITY_H
#define GRAPHICS_UTILITY_H

// header includes
#include "includes.h"
#include <gdisp.h>


//namespace LittleSister
//{
    // forward declarations
    // ...

    ////

    class GraphicsUtility
    {
    public:

    static void fillBox(GXT xb, GYT yb, GXT xe, GYT ye, GCOLOR color);

    protected:

        // constructor/destructor are protected - no instances should ever be created
        GraphicsUtility();
        ~GraphicsUtility();

    private:
        GraphicsUtility(const GraphicsUtility& right);            // block the copy constructor
        GraphicsUtility& operator=(const GraphicsUtility& right); // block the assignment operator

    };

//} // end namespace LittleSister

#endif   // GRAPHICS_UTILITY_H

//======== End of $Workfile: GraphicsUtility.h $ ==========

