/*****************************************************************************
 *
 * Copyright: (c) Laerdal Medical 2006-2009
 *
 * %name:          GraphicsManager.h %
 * %version:       4 %
 * %instance:      1 %
 * %created_by:    nogte1 %
 *
 * Note: VSS loginfo removed after migrating to Synergy
 *
 */

//! @class GraphicsManager
//! @brief Manager class for the graphical functions (OLED display)
//!
//! @author LOL
//!


#ifndef GRAPHICS_MANAGER_H
#define GRAPHICS_MANAGER_H

// header includes
#include "common/common.h"
#include "puck/common/defs.h"

#include <map>
#include <vector>

// need to include screen.h and uistate.h due to enums
#include "Puck/UI/Framework/Screen.h"
#include "Puck/UI/Framework/UIState.h"

// OS Timer
#include "Puck/BSP/OSTimer.h"

// RamTex
#include <gdisp.h>

namespace LittleSister
{
    // forward declarations
    class Widget;
    class DepthWidget;
    class SpeedometerWidget;
    class CompCountWidget;
    class LogoWidget;
    class StartWidget;
    class VentilationWidget;
    class StatusWidget;
    class MiniEventWidget1;
    class MiniEventWidget2;
    class Viewport;
    class ICoreAdmin;
    class ICommTransmitRPL;
    class Mutex;


    // constants for the viewport IDs. No gaps allowed in enum !!!!
    enum ViewportID {
        eStartViewport           = 0,
        eIntroViewport           = 1,
        eZoomInViewport          = 2,
        eDepthWidgetViewport     = 3,
        eSpeedoWidgetViewport    = 4,
        eCountdownViewport       = 5,
        eVentilationViewport     = 6,
        eStandClearViewport      = 7,
        eInactivityViewportSolo  = 8,
        eInactivityViewportCivic = 9,
        eHereAmIViewport         = 10,
        eMiniEventReviewViewport1= 11,
        eMiniEventReviewViewport2= 12,
        eLogoViewport            = 13,
        eCompCountViewport       = 14,
        eHighestViewport         = eCompCountViewport // to enable compile-time assertion - see cpp file
    };

    // collection typedefs
    typedef std::vector< Widget*            > WidgetCollection;
    typedef std::map< ScreenID, Screen*     > ScreenCollection;
    typedef std::map< ViewportID, Viewport* > ViewportCollection;
    typedef std::map< UIStateID, UIState*   > StateCollection;

    ////

    class GraphicsManager
    {
    public:

        static const uint32_t UI_UPDATE_PERIOD = 20;

        // Depth widget viewport coordinates
        static const GXT VP_DW_X_TOP_LEFT = 0;
        static const GYT VP_DW_Y_TOP_LEFT = 0;
        static const GXT VP_DW_X_BOTTOM_RIGHT = 66;
        static const GYT VP_DW_Y_BOTTOM_RIGHT = 127;

        // Speedometer widget viewport coordinates
        static const GXT VP_SW_X_TOP_LEFT = 68;
        static const GYT VP_SW_Y_TOP_LEFT = 0;
        static const GXT VP_SW_X_BOTTOM_RIGHT = 127;
        static const GYT VP_SW_Y_BOTTOM_RIGHT = 80;

        // Compression counter widget viewport coordinates
        static const GXT VP_CW_X_TOP_LEFT = 80;
        static const GYT VP_CW_Y_TOP_LEFT = 95;
        static const GXT VP_CW_X_BOTTOM_RIGHT = 127;
        static const GYT VP_CW_Y_BOTTOM_RIGHT = 127;

        // constructor/destructor
        GraphicsManager();
        ~GraphicsManager();

        bool isDisplayInterfaceOK() const { return m_displayInterfaceOK; }

        // to signal that OLED display is ON/OFF from Core Component
        void displayOnNotification();
        void displayOffNotification();

        // for OLED display blink requests from widgets
        void requestDisplayBlinkOn();
        void requestDisplayBlinkOff();

        // for dim/undim of OLED display
        void dimDisplayIntensityTo(uint8_t percentIntensity);
        void resetDisplayIntensityToDefault();

        // update method - called by the thread when screen is to be repainted
        void update();

        // methods to get rate, depth and force
        inline uint16_t getRate() const    {   return m_rate;      }
        inline int16_t getDepth() const    {   return m_depth;     }

        // methods to receive depth+force and rate for display purposes (exposed through interface)
        void displayDepth( int16_t depth_in_mm, bool isNewCompStart,  bool isCompressionReleaseStarted );
        void updateCompressionCounter();
        void displayRate(  uint16_t rate );

        void setShallowDepthWarning(int16_t depth_in_mm);
        void clearShallowDepthWarning();

        void setIncompleteReleaseWarning(int16_t depth_in_mm);
        void clearIncompleteReleaseWarning();

        // method to zoom in on the patient (exposed through interface)
        void zoomInOnPatient();

        // method to fade in the rate widget speedometer (exposed through interface)
        bool fadeInSpeedometer();

        // method to start countdown (exposed through interface)
        int startCountdownFrom(uint8_t fromValue, uint32_t periodTime);
        void abortCountdown();
        void countdownCompletedNotification() const;

        void suspend();

        // methods related to inactivity (exposed through interface)
        void inactivityStart( uint16_t initial_value_in_seconds );
        void inactivityStop();
        void setInactivityTime( uint16_t inactivity_time_in_seconds );

        // method to add a widget to the total set of widgets in the UI system
        size_t addWidget( Widget* aWidget );

        // publish/subscribe mechanism between manager and widgets
        int subscribeToInactivity( Widget* subscriber );

        // method to invalidate the manager (i.e. invalidate the active screen)
        void invalidate();

        // method to switch UI state
        int switchToState( UIStateID id );
        UIStateID getState() const;

        // test methods for OLED display
        void oledTestScreen1();
        void oledTestScreen2();
        void oledTestScreen3();

        // method called when the Puck changes mode mid-session (Solo <-> Civic)
        void processOperatingModechange() const;

        // method to display the 'Stand Clear' screen (exposed through interface)
        void displayStandClearScreen();

        // method to remove the 'Stand Clear' screen (exposed through interface)
        void removeStandClearScreen();


        // methods related to ventilation mode and the "here am I" screen
        void displayVentilationScreen();
        void removeVentilationScreen();
        bool startHereAmIDisplay();
        void stopHereAmIDisplay();

        // methods for activiating the Logo, Start and Intro screens
        void showLogoScreen( bool showVersionInfo );
        void displayStartScreen();
        void removeStartScreen();
        void showIntroScreen();

        // method for showing large battery status at shutdown
        void showBatteryShutdownScreen(const CommonDefs::BatteryStatus_t batteryStatus);

        // method for showing large data card status at shutdown
        void showDatacardShutdownScreen(const CommonDefs::DatacardStatus_t cardStatus);

        // method for showing large service symbol at shutdown
        void showServiceShutdownScreen();

        // method to reset the needle position (exposed through interface)
        void resetSpeedometerNeedlePosition();

        // method to redisplay the arrows on the ventilation prompt screen
        bool redisplayVentilationArrows();

        ////

        int configureCompCounterDisplay(const CommonDefs::CompCounterConfig_t & config);

        ////

        // method to notify component regarding a patient mode change
        void notifyOfPatientModeChange( CommonDefs::PatientMode newMode );

        // method to notify all components regarding a change in CPR guidelines
        void notifyOfGuidelineChange();

        // methods related to displaying the Mini Event Review screens
        void displayMiniEventReview1();
        void displayMiniEventReview2();
        void removeMiniEventReview();

        // method for displaying battery status on relevant screens that has this available
        void updateBatteryStatus(const CommonDefs::BatteryStatus_t batteryStatus);

        // method for displaying data-card status on relevant screens that has this available
        void updateDatacardStatus(const CommonDefs::DatacardStatus_t cardStatus);

        // method for displaying a status text on relevant screens that has this available
        void displayStatusText(const std::string& statusText);

        // metod for resuming normal UI
        void resumeNormalDisplayMode();

        // INLINE method for quering the zoom state
        inline bool isZoomed()
        {
            return m_zoomed;
        }

         // test method for setting the rate display interval directly
        void setRateInterval( int interval );

    protected:

        // init
        int init();

        // update methods for subscribers
        void updateInactivitySubscribers( uint16_t inactivity_in_seconds );

        // internal screen management
        size_t addScreen( ScreenID id, Screen* screen );
        Screen* getScreen( ScreenID id ) const;
        int switchScreen( ScreenID newScreenID );
        int switchScreen( Screen* newScreen );
        int modifyScreenSymbol( uint8_t newSymbolID );
        int zoomInScreens();
        int zoomOutScreens();
        int fadeScreen( uint8_t fadeLevel );
        int startWidgetTimer();
        int stopWidgetTimer();

        // internal viewport management
        size_t addViewport( ViewportID id, Viewport* viewport );
        Viewport* getViewport( ViewportID id ) const;

        // init method for screens - one for each Screen object
        void initStartScreen();
        void initLogoScreen( StatusWidget* statusWidget );
        void initIntroScreen( DepthWidget* depthWidget, StatusWidget* statusWidget );
        void initZoomInScreen();
        void initNormalScreen( DepthWidget* depthWidget, SpeedometerWidget* speedoWidget,
                               CompCountWidget* compCountWidget, bool forSolo);
        void initCountdownScreen();
        void initVentilationScreen();
        void initStandClearScreen();
        void initInactivityScreenCivic();
        void initHereAmIScreen();
        void initEventReviewScreen1();
        void initEventReviewScreen2();

        // method to init the UI state machine
        void initUIStates();

        // method to get direct access to a UI state
        UIState* getState( UIStateID id ) const;

        // callback method for the zoom timer
        static void zoomCallback( void* pCallbackArgs );

        // callback method for the fade timer
        static void fadeCallback( void* pCallbackArgs );

        // callback method for the coundown timer
        static void countdownCallback( void* pCallbackArgs );

        // *** LOL 20080113: New action "callback" for eIsZoomedAction
        void depthStackIsZoomed();

    private:
        GraphicsManager(const GraphicsManager& right);              // block the copy constructor
        GraphicsManager& operator=(const GraphicsManager& right);   // block the assignment operator

        void suspendAllWidgets();
        void resumeAllWidgets();

        void switchBackToValidPreviousState();

        // helper method for performing display intensity operations
        void updateDisplayIntensity();

        // methods for resource protection
        void lock();
        void unlock();
        Mutex * m_screenMutex;

        // collection of all Viewport objects
        ViewportCollection m_viewports;

        // collection of all Screen objects
        ScreenCollection m_screens;

        // pointer to the ACTIVE Screen object
        Screen*  m_activeScreen;

        // pointer to the Screen object for the previous screen that was drawn
        Screen * m_previouslyDrawnScreen;

        // collection of all widgets
        WidgetCollection m_allWidgets;

        // Widget subscribers
        WidgetCollection m_inactivitySubscribers;

        // Widgets
        DepthWidget*       m_depthWidget;
        SpeedometerWidget* m_speedoWidget;
        CompCountWidget*   m_compCountWidget;
        LogoWidget*        m_logoWidget;
        VentilationWidget* m_ventWidget;
        StartWidget*       m_startWidget;
        StatusWidget*      m_statusWidget;
        MiniEventWidget1*  m_MERWidget1;
        MiniEventWidget2*  m_MERWidget2;

        // current rate, depth and force
        int16_t  m_depth;
        uint16_t m_rate;

        bool m_widgetsSuspended;

        // pointer to the ACTIVE state in the UI state machine
        UIState* m_uiState;

        // pointer to the PREVIOUS state in the UI state machine
        UIState* m_prevState;

        // collection of all states
        StateCollection m_allStates;

        // flag for clearing the screen on the next pass
        bool m_doClear;

        // flag for allowing display blink operations
        bool m_displayBlinkOperationsAllowed;
        enum DisplayOnOffOperation_t {eDispOpNoChange, eDispOpOn, eDispOpOff};
        DisplayOnOffOperation_t m_displayOp;

        // flag for signalling dim/undim of OLED display
        bool m_performDisplayIntensityOperations;
        uint8_t m_targetDisplayIntensityPercent;
        uint8_t m_currentDisplayIntensityPercent;

        // timer for performing the zoom-in on the patient
        OsTimer m_zoomTimer;

        // counter for zoom timer
        int m_zoomCounter;

        // timer for performing fade-in of the rate widget
        OsTimer m_fadeTimer;

        // counter for fade timer
        int m_fadeCounter;

        // timer for performing countdown
        OsTimer m_countdownTimer;

        // counter for countdown timer
        int m_countdownCounter;

        // status of display connections
        bool m_displayInterfaceOK;

        // zoomed flag
        bool m_zoomed;


        bool m_resetCompCounterAtStandClearScreen;

        // interface to Core
        ICoreAdmin* m_coreAdminInterface;

        // interface to Communication
        ICommTransmitRPL * m_commRPLInterface;

        // special case for UIAction - we will allow these action objects to manipulate the manager directly
        friend class UIAction;
    };

} // end namespace LittleSister

#endif   // GRAPHICS_MANAGER_H

//======== End of $Workfile: GraphicsManager.h $ ==========

