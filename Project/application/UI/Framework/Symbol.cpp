/*****************************************************************************
 * Project:   LittleSister PUCK software (ARM)
 *
 * Copyright: (c) Laerdal Medical 2006
 *
 * $Workfile: Symbol.cpp $
 * $Author: Nogte1 $
 * $Revision: 10 $
 *
 * $Log: /LittleSister/src/Puck/UI/Framework/Symbol.cpp $
 * 
 * 10    30.06.08 14:07 Nogte1
 * Ref CR-322
 * Added erase() method
 *
 * 9     28.09.07 23:17 Nogte1
 * Removed some warning level 3 lint warnings
 *
 * 8     9.08.07 17:21 Nogte1
 * Prepared for derived class
 *
 * 7     6.07.07 12:04 Nogte1
 * Reduced RAM usage (this class is instantiated many times!)
 *
 * 6     25.04.07 19:34 Nogte1
 * Optimization: made some frequently called methods return void
 *
 * 5     18.04.07 15:55 Nolol1
 * Implemented the copy constructor
 *
 * 4     12.12.06 9:43 Nogte1
 * Added invalidation
 *
 * 3     5.12.06 12:24 Nogte1
 * Initial implementation
 *
 */

// include self first
#include "UI/Framework/Symbol.h"

// andre includes
#include "UI/Symlib/SymLib.h"
#include "UI/Framework/GraphicsUtility.h"

//namespace LittleSister
//{
    //! constructor
    //! @brief Constructs a symbol holder
    //! @param numSymbols Number of equally sized symbol items
    //! @param x x-coordinate of symbol
    //! @param y y-coordinate of symbol
    //! @return none
    //! @author GIT
    //! @remark none
    //! @pre  none
    //! @post The add() method must be invoked afterwards. Active symbol = first symbol
    Symbol::Symbol(uint8_t numSymbols, GXT x, GYT y)
                    : m_active(0), m_invalidate(false),
                      m_xpos(x),
                      m_ypos(y),
                      m_backgroundColor(G_BLACK)
    {
        m_symbols.reserve(numSymbols);
    }

    //! destructor
    Symbol::~Symbol()
    {

    }

    // copy constructor
    Symbol::Symbol( const Symbol& rhs )
    {
        // copy constructor is implemented as a deep copy

        // copy static elemements
        m_active     = rhs.m_active;
        m_invalidate = rhs.m_invalidate;
        m_xpos       = rhs.m_xpos;
        m_ypos       = rhs.m_ypos;

        // copy dynamic elements
        for( symVector::const_iterator cit = rhs.m_symbols.begin(); cit != rhs.m_symbols.end(); cit++ )
            m_symbols.push_back( *cit );
    }

    //! add
    //! @brief Adds a symbol item to the symbol holder
    //! @param sym Pointer to symbol structure
    //! @return none
    //! @author GIT
    //! @remark none
    //! @pre none
    //! @post none
    void Symbol::add(const PGSYMBOL sym)
    {
        if (sym != 0)
        {
            m_symbols.push_back(sym);
            m_invalidate = true;
        }
    }

    //! setActive
    //! @brief Sets a symbol item to be the active symbol
    //! @param symIdx index to symbol item
    //! @return 0 at success, else -1
    //! @author GIT
    //! @remark none
    //! @pre none
    //! @post none
    int Symbol::setActive(uint8_t symIdx)
    {
        if (symIdx < m_symbols.size())
        {
            if (m_active != symIdx)
            {
                m_active = symIdx;
                m_invalidate = true;
            }
            return 0;
        }
        return -1;
    }

    //! setActiveFirst
    //! @brief Sets the first symbol item to be the active symbol
    //! @param none
    //! @return 0 at success, else -1
    //! @author GIT
    //! @remark none
    //! @pre none
    //! @post none
    int Symbol::setActiveFirst()
    {
        return setActive(0);
    }

    //! setActiveNext
    //! @brief Sets the next symbol item to be the active symbol
    //! @param none
    //! @return 0 at success, else -1
    //! @author GIT
    //! @remark none
    //! @pre none
    //! @post none
    int Symbol::setActiveNext()
    {
        return setActive(m_active+1);
    }

    //! setActiveNext
    //! @brief Sets the first symbol item to be the active symbol with optional wraparaund to the first symbol
    //! @param allowWraparound If true, we can wraparaund to the first symbol if end was reached
    //! @return 0 at success, else -1
    //! @author GIT
    //! @remark none
    //! @pre none
    //! @post none
    int Symbol::setActiveNext(bool allowWraparound)
    {
        if (allowWraparound && (static_cast<size_t>(m_active)+1) >= m_symbols.size())
        {
            return setActiveFirst();
        }
        else
        {
            return setActiveNext();
        }
    }

    //! setActivePrev
    //! @brief Sets the previous symbol item to be the active symbol
    //! @param none
    //! @return 0 at success, else -1
    //! @author GIT
    //! @remark none
    //! @pre none
    //! @post none
    int Symbol::setActivePrev()
    {
        if (m_active > 0)
        {
            return setActive(m_active-1);
        }
        return -1;
    }

    //! setActivePrev
    //! @brief Sets the previous symbol item to be the active symbol with optional wraparaund to the last symbol
    //! @param allowWraparound If true, we can wraparaund to the last symbol if beginning was reached
    //! @return 0 at success, else -1
    //! @author GIT
    //! @remark none
    //! @pre none
    //! @post none
    int Symbol::setActivePrev(bool allowWraparound)
    {
        if (allowWraparound && 0 == m_active)
        {
            return setActiveLast();
        }
        else
        {
            return setActivePrev();
        }
    }

    //! setActiveLast
    //! @brief Sets the last symbol item to be the active symbol
    //! @param none
    //! @return 0 at success, else -1
    //! @author GIT
    //! @remark none
    //! @pre none
    //! @post none
    int Symbol::setActiveLast()
    {
        if (!m_symbols.empty())
        {
            return setActive(static_cast<uint8_t>(m_symbols.size()-1));
        }
        return -1;
    }

    //! draw
    //! @brief Draws the active symbol item
    //! @param none
    //! @return none
    //! @author GIT
    //! @remark none
    //! @pre none
    //! @post none
    void Symbol::draw()
    {
        if (m_invalidate)
        {
            m_invalidate = false;
            if (m_active < m_symbols.size())
            {
                gputsym(m_xpos, m_ypos, m_symbols[m_active]);
            }
        }
    }

    //! @brief Set background color
    //! @author JRO
    void Symbol::setBackgroundColor(GCOLOR color)
    {
        m_backgroundColor = color;
        gsetcolorb(m_backgroundColor);
    }
    
    //! @brief Erases the active symbol item
    //! @author GIT
    void Symbol::erase()
    {
        if (m_active < m_symbols.size())
        {
            // clear area occupied by battery symbol
            GXT xpos = getXpos();
            GYT ypos = getYpos();

            GXT xwidth  = getWidth();
            GYT yheight = getHeight();

            GraphicsUtility::fillBox(xpos, ypos,
                                     xpos + xwidth-1,
                                     ypos + yheight-1,
                                     m_backgroundColor);
        }
    }

    //! @brief Returns width of active symbol
    //! @return The width, or 0 if no symbol available
    //! @author GIT
    GXT Symbol::getWidth()
    {
        if (m_active < m_symbols.size())
        {
            return (GXT) gsymw(m_symbols[m_active]); //lint !e826
        }
        return 0;
    }

    //! @brief Returns heigth of active symbol
    //! @return The heigth, or 0 if no symbol available
    //! @author GIT
    GYT Symbol::getHeight()
    {
        if (m_active < m_symbols.size())
        {
            return (GYT) gsymh(m_symbols[m_active]); //lint !e826
        }
        return 0;
    }

//} // end namespace LittleSister

//======== End of $Workfile: Symbol.cpp $ ==========

