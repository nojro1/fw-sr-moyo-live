/*****************************************************************************
 *
 * Copyright: (c) Laerdal Medical 2006-2009
 *
 * %name:          GraphicsManager.cpp %
 * %version:       5 %
 * %instance:      1 %
 * %created_by:    nogte1 %
 *
 * Note: VSS loginfo removed after migrating to Synergy
 *
 */

// include self first
#include "Puck/UI/Framework/GraphicsManager.h"

// RamTex
#include <ssd1339.h>  // ssd1339 controller specific definements
#include <bussim.h>

#include "Puck/BSP/HwAbsl.h"
#include "Puck/BSP/OsAbsl.h"
#include "Puck/BSP/Mutex.h"

// Widget base class header
#include "Puck/UI/Widgets/Widget.h"

// Symbol classes
#include "Puck/UI/Framework/Symbol.h"
#include "Puck/UI/Framework/PaletteSymbol.h"

// Symbol library
#include "Puck/UI/Symlib/SymLib.h"

// other includes
#include "Puck/UI/Framework/Screen.h"
#include "Puck/UI/Framework/Viewport.h"
#include "Puck/UI/Widgets/DepthWidget.h"
#include "Puck/UI/Widgets/SpeedometerWidget.h"
#include "Puck/UI/Widgets/InactivityWidgetSolo.h"
#include "Puck/UI/Widgets/InactivityWidgetCivic.h"
#include "Puck/UI/Widgets/HereAmIWidget.h"
#include "Puck/UI/Widgets/LogoWidget.h"
#include "Puck/UI/Widgets/VentilationWidget.h"
#include "Puck/UI/Widgets/StartWidget.h"
#include "Puck/UI/Widgets/CompCountWidget.h"
#include "Puck/UI/Widgets/StatusWidget.h"
#include "Puck/UI/Widgets/MiniEventWidget1.h"
#include "Puck/UI/Widgets/MiniEventWidget2.h"

#include "Puck/Core/corehelper.h"
#include "Puck/Core/Interface/ICoreAdmin.h"
#include "Puck/Config/StoredData.h"

#include "Puck/Communication/CommunicationHelper.h"
#include "Puck/Communication/Interfaces/ICommTransmitRPL.h"

// *** LOL 20080113
#include "Puck/Feedback/FeedbackHelper.h"
#include "Puck/Feedback/Interface/IFeedbackAdmin.h"

// UI States and UI Actions
#include "Puck/UI/Framework/UIState.h"
#include "Puck/UI/Framework/UIAction.h"

#include <algorithm>

USING_NAMESPACE_STD;
using namespace LittleSister;

#pragma diag_suppress=Pe174 // temporarily disable "Warning[Pe174]: expression has no effect"
#pragma diag_suppress=Pe177 // temporarily disable "Warning[Pe177]: function "..." was declared but never referenced"
static void compile_time_check_of_viewport_usage()
{
    // will not compile if we have more viewports than defined in Gdispcfg.h
    CTA(eHighestViewport < GNUMVP ); //lint !e522
}
#pragma diag_default=Pe174 // restore warning level
#pragma diag_default=Pe177 // restore warning level



    //! constructor
    GraphicsManager::GraphicsManager()
        : m_activeScreen( 0 ),
          m_previouslyDrawnScreen( 0 ),
          m_depthWidget(  0 ),
          m_speedoWidget( 0 ),
          m_compCountWidget( 0 ),
          m_logoWidget(   0 ),
          m_ventWidget(   0 ),
          m_startWidget(  0 ),
          m_statusWidget( 0 ),
          m_MERWidget1( 0 ),
          m_MERWidget2( 0 ),
          m_depth( 0 ),
          m_rate(  0 ),
          m_widgetsSuspended( false ),
          m_uiState(   0 ),
          m_prevState( 0 ),
          m_doClear( false ),
          m_displayBlinkOperationsAllowed( false ),
          m_displayOp( eDispOpNoChange ),
          m_performDisplayIntensityOperations( false ),
          m_targetDisplayIntensityPercent( 100 ),
          m_currentDisplayIntensityPercent( 100 ),
          m_zoomCounter( 0 ),
          m_fadeCounter( 0 ),
          m_countdownCounter ( 0 ),
          m_displayInterfaceOK(false),
          m_zoomed( false ),
          m_resetCompCounterAtStandClearScreen(false),
          m_coreAdminInterface( 0 ),
          m_commRPLInterface( 0 )
    {
        // create Mutex
        m_screenMutex = new Mutex("Screen");

        // init self
        this->init();
    }

    //! destructor
    GraphicsManager::~GraphicsManager()
    {
        // TODO: Because widgets and (in some cases) viewports are shared between the logical screens, we
        // can't simply do a hierachical tear-down of the structure. This would lead to memory errors as we would
        // try to delete objects that are already deleted.
        // Instead we should implement a mechanism that deletes only unique objects
        // NIX! delete m_screen;

        gexit();
        delete m_screenMutex;
    }


    // init
    int GraphicsManager::init()
    {
        // init RamTex, and update status for POST
        if (ginit() == 0)
        {
            m_displayInterfaceOK = true;
        }
        else
        {   // repeat to be sure that there is really an error on the display interface
            if (ginit() == 0)
            {
                m_displayInterfaceOK = true;
            }
        }

        // get interfaces
        m_coreAdminInterface = CoreHelper::instance().getAdminInterface();
        if( 0 == m_coreAdminInterface )
        {
            return -1;
        }

        m_commRPLInterface = CommunicationHelper::instance().getRPLInterface();
        if ( 0 == m_commRPLInterface )
        {
            return -1;
        }

        // widgets are shared between screens, so they need to be created at this scope
        m_depthWidget  = new DepthWidget();
        m_speedoWidget = new SpeedometerWidget();
        m_compCountWidget = new CompCountWidget();
        m_statusWidget = new StatusWidget(); // CR-322, for battery status icon etc..

        if( m_coreAdminInterface->isDeviceModeSolo() )
        {
            // init screens for SOLO (note - order is significant)
            initStartScreen();
            initIntroScreen( m_depthWidget, m_statusWidget );
            initZoomInScreen();
            initNormalScreen( m_depthWidget, m_speedoWidget, m_compCountWidget, true );
            initEventReviewScreen1();
            initEventReviewScreen2();
            initLogoScreen( m_statusWidget );
        }
        else
        {
            // init screens for CCS connected to defib. (note - order is significant)
            initStartScreen();
            initIntroScreen( m_depthWidget, 0 );
            initZoomInScreen();
            initNormalScreen( m_depthWidget, m_speedoWidget, m_compCountWidget, false );
            initCountdownScreen();
            initVentilationScreen();
            initStandClearScreen();
            initInactivityScreenCivic();
            initHereAmIScreen();
            initLogoScreen( m_statusWidget );
        }

        // init the UI state machine
        initUIStates();

        // invalidate the screen to force repaint
        invalidate();

        // done
        return 0;
    }


    // to signal that OLED display is ON from Core Component
    void GraphicsManager::displayOnNotification()
    {
        lock();
        m_displayBlinkOperationsAllowed = true;
        m_displayOp = eDispOpOn; // ensure that ON-command is sent to OLED at next update, in case it was OFF
        unlock();
    }

    // to signal that OLED display is OFF from Core Component
    void GraphicsManager::displayOffNotification()
    {
        lock();
        m_displayBlinkOperationsAllowed = false;
        m_displayOp = eDispOpOff;
        unlock();
    }

    // for OLED display blink requests from widgets
    void GraphicsManager::requestDisplayBlinkOn()
    {
        lock();
        if ( m_displayBlinkOperationsAllowed )
        {
            m_displayOp = eDispOpOn;
        }
        unlock();
    }

    void GraphicsManager::requestDisplayBlinkOff()
    {
        lock();
        if ( m_displayBlinkOperationsAllowed )
        {
            m_displayOp = eDispOpOff;
        }
        unlock();
    }

    // for dimming of OLED display
    void GraphicsManager::dimDisplayIntensityTo(uint8_t percentIntensity)
    {
        lock();
        if (percentIntensity != m_targetDisplayIntensityPercent && percentIntensity <= 100)
        {
            m_targetDisplayIntensityPercent = percentIntensity;
            m_performDisplayIntensityOperations = true; // signal to update() method
        }
        unlock();
    }

    // for un-dimming of OLED display
    void GraphicsManager::resetDisplayIntensityToDefault()
    {
        lock();
        if (m_targetDisplayIntensityPercent != 100)
        {
            m_targetDisplayIntensityPercent = 100;
            m_performDisplayIntensityOperations = true; // signal to update() method
        }
        unlock();
    }

    // method to notify component regarding a patient mode change
    void GraphicsManager::notifyOfPatientModeChange( CommonDefs::PatientMode newMode )
    {
        newMode = newMode; // to please lint

        // instruct widgets to pull new guidelines
        if( m_depthWidget )
            m_depthWidget->updateGuidelines();
        if( m_speedoWidget )
            m_speedoWidget->updateGuidelines();
    }

    // method to notify component regarding a change in CPR guidelines
    void GraphicsManager::notifyOfGuidelineChange()
    {
        // instruct widgets to pull new guidelines
        if( m_depthWidget )
            m_depthWidget->updateGuidelines();
        if( m_speedoWidget )
            m_speedoWidget->updateGuidelines();
    }


    //lint -e{429}
    void GraphicsManager::initStartScreen()
    {
        // create screen
        Screen* screen = new Screen( eStartScreen );

        // add to collection
        this->addScreen( eStartScreen, screen );

        // create viewport - for start screen
        Viewport* vp = new Viewport( eStartViewport, 0, 0, 127, 127 );
        screen->addViewport( eStartViewport, vp );

        // add viewport to collection
        this->addViewport( eStartViewport, vp );

        // create specialized widget for the start screen
        m_startWidget = new StartWidget();

        // add this widget to the viewport
        vp->addWidget( m_startWidget );

        // init the widget to set up the data subscriptions
        m_startWidget->init( this );
    }

    //lint -e{429}
    void GraphicsManager::initIntroScreen( DepthWidget* depthWidget, StatusWidget* statusWidget )
    {
        // create screen
        Screen* screen = new Screen( eIntroScreen );

        // add to collection
        this->addScreen( eIntroScreen, screen );

        // create viewport - for minimized depth widget + outline figure
        Viewport* vp = new Viewport( eIntroViewport, 0, 0, 127, 127 );
        screen->addViewport( eIntroViewport, vp );

        // add viewport to collection
        this->addViewport( eIntroViewport, vp );

        // add depth widget to this viewport
        vp->addWidget( depthWidget );

        // make sure depth widget is small (zoomed out)
        depthWidget->zoomOut();

        // Add the symbol for the outline
        PaletteSymbol* symbol = new PaletteSymbol(1, 0, 0, SymLib::introPalette);
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::introScreen[0]) );
        symbol->setActive( 0 );
        vp->addSymbol( symbol );
        symbol->invalidate();

        // add status widget to this viewport
        if ( statusWidget != 0 )
        {
            vp->addWidget( statusWidget );
        }
    }

    //lint -e{429}
    void GraphicsManager::initZoomInScreen()
    {
        // create screen
        Screen* screen = new Screen( eZoomInScreen );

        // add to collection
        this->addScreen( eZoomInScreen, screen );

       // create viewport - for the zoom-in images
        Viewport* vp = new Viewport( eZoomInViewport, 0, 0, 127, 127 );
        screen->addViewport( eZoomInViewport, vp );

        // add viewport to collection
        this->addViewport( eZoomInViewport, vp );

        // add Zoom symbols (4 steps), palette type
        PaletteSymbol* symbol = new PaletteSymbol(4, 0, 0, SymLib::zoomPalette);
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::zoomScreen[0]) );
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::zoomScreen[1]) );
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::zoomScreen[2]) );
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::zoomScreen[3]) );
        symbol->setActive( 0 );
        vp->addSymbol( symbol );
        symbol->invalidate();
    }

    //lint -e{429}
    void GraphicsManager::initNormalScreen( DepthWidget* depthWidget, SpeedometerWidget* speedoWidget,
                                            CompCountWidget* compCountWidget, bool forSolo )
    {
        // create screen
        Screen* screen = new Screen( eNormalScreen );

        // add to collection
        this->addScreen( eNormalScreen, screen );

        // create viewport - for depth widget
        Viewport* vpD = new Viewport( eDepthWidgetViewport,
                                      VP_DW_X_TOP_LEFT, VP_DW_Y_TOP_LEFT,
                                      VP_DW_X_BOTTOM_RIGHT, VP_DW_Y_BOTTOM_RIGHT );

        screen->addViewport( eDepthWidgetViewport, vpD );

        // create viewport - for rate widget
        Viewport* vpR = new Viewport( eSpeedoWidgetViewport,
                                      VP_SW_X_TOP_LEFT, VP_SW_Y_TOP_LEFT,
                                      VP_SW_X_BOTTOM_RIGHT, VP_SW_Y_BOTTOM_RIGHT);

        screen->addViewport( eSpeedoWidgetViewport, vpR );

        // create viewport - for compression counter widget
        Viewport* vpC = new Viewport( eCompCountViewport,
                                      VP_CW_X_TOP_LEFT, VP_CW_Y_TOP_LEFT,
                                      VP_CW_X_BOTTOM_RIGHT, VP_CW_Y_BOTTOM_RIGHT);

        screen->addViewport( eCompCountViewport, vpC );

        // add viewports to collection
        this->addViewport( eDepthWidgetViewport,   vpD );
        this->addViewport( eSpeedoWidgetViewport,  vpR );
        this->addViewport( eCompCountViewport,     vpC );

        // add depth widget to the associated (left) viewport
        vpD->addWidget( depthWidget );

        // add speedometer widget to the associated (right) viewport
        vpR->addWidget( speedoWidget );

        // add compression counter widget to the associated (right) viewport
        vpC->addWidget( compCountWidget );

        // init the widgets to set up the data subscriptions
        depthWidget->init( this );
        speedoWidget->init( this );
        compCountWidget->init( this );

        if ( forSolo )
        {
            // enable and configure comp count widget
            if (m_coreAdminInterface)
            {
                CommonDefs::CompCounterConfig_t cfg =
                {
                    CommonDefs::eCompCountAllFlagsSet,
                    0,
                    25,
                    30,
                    10
                };
                UiSettings_t s;
                if (m_coreAdminInterface->getUiSettings(s))
                {
                    cfg.m_timeout = s.m_displayTimeout * 1000; //lint !e734
                    compCountWidget->configure(cfg, cfg.m_timeout);
                }
            }

            // create viewport - for the solo inactivity widget
            Viewport* vpI = new Viewport( eInactivityViewportSolo, 72, 6, 123, 31 );
            screen->addViewport( eInactivityViewportSolo, vpI );

            // add viewport to collection
            this->addViewport( eInactivityViewportSolo, vpI );

            // create specialized widget for the solo inactivity mode
            InactivityWidgetSolo* inactivityWidgetS = new InactivityWidgetSolo();

            // add this widget to the viewport
            vpI->addWidget( inactivityWidgetS );

            // init the widget to set up the data subscriptions
            inactivityWidgetS->init( this );
        }

    }

    //lint -e{429}
    void GraphicsManager::initCountdownScreen()
    {
        // create screen
        Screen* screen = new Screen( eCountdownScreen );

        // add to collection
        this->addScreen( eCountdownScreen, screen );

        // share/recycle left viewport (for depth widget)
        Viewport* vpD = getViewport( eDepthWidgetViewport );
        screen->addViewport( eDepthWidgetViewport, vpD );

        // create new viewport for countdown numbers (5,4,3,2,1)
        Viewport* vpC = new Viewport( eCountdownViewport, 75, 0, 127, 127);
        screen->addViewport( eCountdownViewport, vpC );

        // add the new viewport to the collection
        this->addViewport( eCountdownViewport, vpC );

        // Add the symbols for the (5) countdown icons, palette type
        PaletteSymbol* symbol = new PaletteSymbol(5, 0, 32, SymLib::countdownPalette);
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::countdownScreen[0]) );
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::countdownScreen[1]) );
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::countdownScreen[2]) );
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::countdownScreen[3]) );
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::countdownScreen[4]) );
        symbol->setActive( 0 );
        vpC->addSymbol( symbol );
        symbol->invalidate();
    }

    //lint -e{429}
    void GraphicsManager::initVentilationScreen()
    {
        // create screen
        Screen* screen = new Screen( eVentilationScreen );

        // add to collection
        this->addScreen( eVentilationScreen, screen );

        // create new viewport for ventilation icons
        Viewport* vp = new Viewport( eVentilationViewport, 0, 0, 127, 127);
        screen->addViewport( eVentilationViewport, vp );

        // add the new viewport to the collection
        this->addViewport( eVentilationViewport, vp );

        // create specialized widget for the ventilation prompt screen
        m_ventWidget = new VentilationWidget();

        // add this widget to the viewport
        vp->addWidget( m_ventWidget );

        // init the widget to set up the data subscriptions
        m_ventWidget->init( this );
    }

    //lint -e{429}
    void GraphicsManager::initStandClearScreen()
    {
        // create screen
        Screen* screen = new Screen( eStandClearScreen );

        // add to collection
        this->addScreen( eStandClearScreen, screen );

        // create viewport - for the stand clear screen
        Viewport* vp = new Viewport( eStandClearViewport, 0, 0, 127, 127 );
        screen->addViewport( eStandClearViewport, vp );

        // add viewport to collection
        this->addViewport( eStandClearViewport, vp );

        // Add the symbols for the (1) stand clear icon
        PaletteSymbol* symbol = new PaletteSymbol(1, 0, 0, SymLib::standClearPalette);
        symbol->add( reinterpret_cast<PGSYMBOL>(&SymLib::standClearScreen[0]) );
        symbol->setActive( 0 );
        vp->addSymbol( symbol );
        symbol->invalidate();
    }


    //lint -e{429}
    void GraphicsManager::initInactivityScreenCivic()
    {
        // create screen
        Screen* screen = new Screen( eInactivityScreenCivic );

        // add to collection
        this->addScreen( eInactivityScreenCivic, screen );

        // create viewport - for the civic inactivity screen
        Viewport* vp = new Viewport( eInactivityViewportCivic, 0, 0, 127, 127 );
        screen->addViewport( eInactivityViewportCivic, vp );

        // add viewport to collection
        this->addViewport( eInactivityViewportCivic, vp );

        // create specialized widget for the civic inactivity mode
        InactivityWidgetCivic* inactivityWidgetC = new InactivityWidgetCivic();

        // add this widget to the viewport
        vp->addWidget( inactivityWidgetC );

        // init the widget to set up the data subscriptions
        inactivityWidgetC->init( this );
    }


    //lint -e{429}
    void GraphicsManager::initHereAmIScreen()
    {
        // create screen
        Screen* screen = new Screen( eHereAmIScreen );

        // add to collection
        this->addScreen( eHereAmIScreen, screen );

        // create viewport - for the 'here am I' screen
        Viewport* vp = new Viewport( eHereAmIViewport, 0, 0, 127, 127 );
        screen->addViewport( eHereAmIViewport, vp );

        // add viewport to collection
        this->addViewport( eHereAmIViewport, vp );

        // create specialized widget for the 'here am I' state
        HereAmIWidget* hereAmIWidget = new HereAmIWidget();

        // add this widget to the viewport
        vp->addWidget( hereAmIWidget );

        // init the widget to set up the data subscriptions
        hereAmIWidget->init( this );
    }

    // init Mini Event Review screen 1
    //lint -e{429}
    void GraphicsManager::initEventReviewScreen1()
    {
        // create screen
        Screen* screen = new Screen( eMiniEventReviewScreen1 );

        // add to collection
        this->addScreen( eMiniEventReviewScreen1, screen );

        // create viewport - for the screen
        Viewport* vp = new Viewport( eMiniEventReviewViewport1, 0, 0, 127, 127 );
        screen->addViewport( eMiniEventReviewViewport1, vp );

        // add viewport to collection
        this->addViewport( eMiniEventReviewViewport1, vp );

        // create specialized widget for the start screen
        m_MERWidget1 = new MiniEventWidget1();

        // add this widget to the viewport
        vp->addWidget( m_MERWidget1 );

        // init the widget to set up the data subscriptions
        m_MERWidget1->init( this );
    }

    // init Mini Event Review screen 2
    //lint -e{429}
    void GraphicsManager::initEventReviewScreen2()
    {
        // create screen
        Screen* screen = new Screen( eMiniEventReviewScreen2 );

        // add to collection
        this->addScreen( eMiniEventReviewScreen2, screen );

        // create viewport - for the screen
        Viewport* vp = new Viewport( eMiniEventReviewViewport2, 0, 0, 127, 127 );
        screen->addViewport( eMiniEventReviewViewport2, vp );

        // add viewport to collection
        this->addViewport( eMiniEventReviewViewport2, vp );

        // create specialized widget for the start screen
        m_MERWidget2 = new MiniEventWidget2;

        // add this widget to the viewport
        vp->addWidget( m_MERWidget2 );

        // init the widget to set up the data subscriptions
        m_MERWidget2->init( this );
    }

    //lint -e{429}
    void GraphicsManager::initLogoScreen(StatusWidget* statusWidget)
    {
        // create screen
        Screen* screen = new Screen( eLogoScreen );

        // add to collection
        this->addScreen( eLogoScreen, screen );

        // create viewport - for logo screen
        Viewport* vp = new Viewport( eLogoViewport, 0, 0, 127, 127 );
        screen->addViewport( eLogoViewport, vp );

        // add viewport to collection
        this->addViewport( eLogoViewport, vp );

        // create specialized widget for the logo screen
        m_logoWidget = new LogoWidget();

        // add this widget to the viewport
        vp->addWidget( m_logoWidget );

        // init the widget to set up the data subscriptions
        m_logoWidget->init( this );

        // add status widget to this viewport
        if ( statusWidget != 0 )
        {
            vp->addWidget( statusWidget );
            // init the widget to set up the data subscriptions
            statusWidget->init( this );
        }

    }

    // method to init the UI state machine
    //lint -e{429}
    void GraphicsManager::initUIStates()
    {
        // create the states
        UIState* initialState         = new UIState( eInitial );
        UIState* startState           = new UIState( eStart );
        UIState* introState           = new UIState( eIntro );
        UIState* zoom1State           = new UIState( eZoom1 );
        UIState* zoom2State           = new UIState( eZoom2 );
        UIState* zoom3State           = new UIState( eZoom3 );
        UIState* zoom4State           = new UIState( eZoom4 );
        UIState* zoom5State           = new UIState( eZoom5 );
        UIState* fadeIn1State         = new UIState( eFadeIn1 );
        UIState* fadeIn2State         = new UIState( eFadeIn2 );
        UIState* fadeIn3State         = new UIState( eFadeIn3 );
        UIState* fadeIn4State         = new UIState( eFadeIn4 );
        UIState* normalState          = new UIState( eNormal );
        UIState* countdown5State      = new UIState( eCountdown5 );
        UIState* countdown4State      = new UIState( eCountdown4 );
        UIState* countdown3State      = new UIState( eCountdown3 );
        UIState* countdown2State      = new UIState( eCountdown2 );
        UIState* countdown1State      = new UIState( eCountdown1 );
        UIState* ventilationState     = new UIState( eVentilation );
        UIState* standClearState      = new UIState( eStandClear );
        UIState* inactivitySoloState  = new UIState( eInactivitySolo );
        UIState* inactivityCivicState = new UIState( eInactivityCivic );
        UIState* hereAmIState         = new UIState( eHereAmI );
        UIState* MER1State            = new UIState( eMiniEventReview1 );
        UIState* MER2State            = new UIState( eMiniEventReview2 );
        UIState* logoState            = new UIState( eLogo );

        ////

        // Add all states to a lookup table for direct access.
        // Note: this table replaces the individual state transition vector<>s, such that it is always
        // possible to go from any state to any other state. This is done to conserve RAM
        m_allStates.insert( pair<UIStateID,UIState*>( eInitial,          initialState ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eStart,            startState ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eIntro,            introState ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eZoom1,            zoom1State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eZoom2,            zoom2State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eZoom3,            zoom3State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eZoom4,            zoom4State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eZoom5,            zoom5State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eFadeIn1,          fadeIn1State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eFadeIn2,          fadeIn2State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eFadeIn3,          fadeIn3State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eFadeIn4,          fadeIn4State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eNormal,           normalState ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eCountdown5,       countdown5State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eCountdown4,       countdown4State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eCountdown3,       countdown3State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eCountdown2,       countdown2State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eCountdown1,       countdown1State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eVentilation,      ventilationState ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eStandClear,       standClearState ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eInactivitySolo,   inactivitySoloState ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eInactivityCivic,  inactivityCivicState ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eHereAmI,          hereAmIState ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eMiniEventReview1, MER1State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eMiniEventReview2, MER2State ) );
        m_allStates.insert( pair<UIStateID,UIState*>( eLogo,             logoState ) );

        ////

        // associate screens with states
        initialState->setScreen(         static_cast<Screen *>(0) );    // no screen
        startState->setScreen(           getScreen( eStartScreen ) );
        introState->setScreen(           getScreen( eIntroScreen ) );
        zoom1State->setScreen(           getScreen( eZoomInScreen ) );
        zoom2State->setScreen(           getScreen( eZoomInScreen ) );
        zoom3State->setScreen(           getScreen( eZoomInScreen ) );
        zoom4State->setScreen(           getScreen( eZoomInScreen ) );
        zoom5State->setScreen(           getScreen( eNormalScreen ) );
        fadeIn1State->setScreen(         getScreen( eNormalScreen ) );
        fadeIn2State->setScreen(         getScreen( eNormalScreen ) );
        fadeIn3State->setScreen(         getScreen( eNormalScreen ) );
        fadeIn4State->setScreen(         getScreen( eNormalScreen ) );
        normalState->setScreen(          getScreen( eNormalScreen ) );
        countdown5State->setScreen(      getScreen( eCountdownScreen ) );
        countdown4State->setScreen(      getScreen( eCountdownScreen ) );
        countdown3State->setScreen(      getScreen( eCountdownScreen ) );
        countdown2State->setScreen(      getScreen( eCountdownScreen ) );
        countdown1State->setScreen(      getScreen( eCountdownScreen ) );
        ventilationState->setScreen(     getScreen( eVentilationScreen ) );
        standClearState->setScreen(      getScreen( eStandClearScreen ) );
        inactivitySoloState->setScreen(  getScreen( eNormalScreen) );
        inactivityCivicState->setScreen( getScreen( eInactivityScreenCivic ) );
        hereAmIState->setScreen(         getScreen( eHereAmIScreen ) );
        MER1State->setScreen(            getScreen( eMiniEventReviewScreen1 ) );
        MER2State->setScreen(            getScreen( eMiniEventReviewScreen2 ) );
        logoState->setScreen(            getScreen( eLogoScreen ) );

        // create generic (reusable) action objects for state transitions
        UIAction* zoomInAction       = new UIAction( eZoomInAction );
        UIAction* zoomOutAction      = new UIAction( eZoomOutAction );
        UIAction* invalidateAction   = new UIAction( eInvalidateAction );

        // add custom actions to the state graph

        // switch screen actions need to be performed first
        UIAction* startSwitchScreenAction = new UIAction( eSwitchScreenAction );
        startState->addEntryAction( startSwitchScreenAction );
        UIAction* introSwitchScreenAction = new UIAction( eSwitchScreenAction );
        introState->addEntryAction( introSwitchScreenAction );
        UIAction* zoom1SwitchScreenAction = new UIAction( eSwitchScreenAction );
        zoom1State->addEntryAction( zoom1SwitchScreenAction );
        UIAction* zoom5SwitchScreenAction = new UIAction( eSwitchScreenAction );
        zoom5State->addEntryAction( zoom5SwitchScreenAction );
        UIAction* fadeIn1SwitchScreenAction = new UIAction( eSwitchScreenAction );
        fadeIn1State->addEntryAction( fadeIn1SwitchScreenAction );
        UIAction* normalSwitchScreenAction = new UIAction( eSwitchScreenAction );
        normalState->addEntryAction( normalSwitchScreenAction );
        UIAction* countdown5SwitchScreenAction = new UIAction( eSwitchScreenAction );
        countdown5State->addEntryAction( countdown5SwitchScreenAction );
        UIAction* countdown4SwitchScreenAction = new UIAction( eSwitchScreenAction );
        countdown4State->addEntryAction( countdown4SwitchScreenAction );
        UIAction* countdown3SwitchScreenAction = new UIAction( eSwitchScreenAction );
        countdown3State->addEntryAction( countdown3SwitchScreenAction );
        UIAction* countdown2SwitchScreenAction = new UIAction( eSwitchScreenAction );
        countdown2State->addEntryAction( countdown2SwitchScreenAction );
        UIAction* ventilationSwitchScreenAction = new UIAction( eSwitchScreenAction );
        ventilationState->addEntryAction( ventilationSwitchScreenAction );
        UIAction* standClearSwitchScreenAction = new UIAction( eSwitchScreenAction );
        standClearState->addEntryAction( standClearSwitchScreenAction );
        UIAction* inactivityCivicSwitchScreenAction = new UIAction( eSwitchScreenAction );
        inactivityCivicState->addEntryAction( inactivityCivicSwitchScreenAction );
        UIAction* hereAmISwitchScreenAction = new UIAction( eSwitchScreenAction );
        hereAmIState->addEntryAction( hereAmISwitchScreenAction );
        UIAction* eventReview1SwitchScreenAction = new UIAction( eSwitchScreenAction );
        MER1State->addEntryAction( eventReview1SwitchScreenAction );
        UIAction* eventReview2SwitchScreenAction = new UIAction( eSwitchScreenAction );
        MER2State->addEntryAction( eventReview2SwitchScreenAction );
        UIAction* logoSwitchScreenAction = new UIAction( eSwitchScreenAction );
        logoState->addEntryAction( logoSwitchScreenAction );

        // ... followed by actions that modify symbols and/or perform fade actions + start timers
        UIAction* zoom1ToggleSymbolAction = new UIAction( eModifySymbolAction, 0 );
        zoom1State->addEntryAction( zoom1ToggleSymbolAction );
        UIAction* zoom2ToggleSymbolAction = new UIAction( eModifySymbolAction, 1 );
        zoom2State->addEntryAction( zoom2ToggleSymbolAction );
        UIAction* zoom3ToggleSymbolAction = new UIAction( eModifySymbolAction, 2 );
        zoom3State->addEntryAction( zoom3ToggleSymbolAction );
        UIAction* zoom4ToggleSymbolAction = new UIAction( eModifySymbolAction, 3 );
        zoom4State->addEntryAction( zoom4ToggleSymbolAction );
        UIAction* countdown1ToggleSymbolAction = new UIAction( eModifySymbolAction, 0 );
        countdown1State->addEntryAction( countdown1ToggleSymbolAction );
        UIAction* countdown2ToggleSymbolAction = new UIAction( eModifySymbolAction, 1 );
        countdown2State->addEntryAction( countdown2ToggleSymbolAction );
        UIAction* countdown3ToggleSymbolAction = new UIAction( eModifySymbolAction, 2 );
        countdown3State->addEntryAction( countdown3ToggleSymbolAction );
        UIAction* countdown4ToggleSymbolAction = new UIAction( eModifySymbolAction, 3 );
        countdown4State->addEntryAction( countdown4ToggleSymbolAction );
        UIAction* countdown5ToggleSymbolAction = new UIAction( eModifySymbolAction, 4 );
        countdown5State->addEntryAction( countdown5ToggleSymbolAction );
        UIAction* fade1Action = new UIAction( eFadeAction, 1 );
        fadeIn1State->addEntryAction( fade1Action );
        UIAction* fade2Action = new UIAction( eFadeAction, 2 );
        fadeIn2State->addEntryAction( fade2Action );
        UIAction* fade3Action = new UIAction( eFadeAction, 3 );
        fadeIn3State->addEntryAction( fade3Action );
        UIAction* fade4Action = new UIAction( eFadeAction, 4 );
        fadeIn4State->addEntryAction( fade4Action );
        UIAction* fadeFinishedAction = new UIAction( eFadeAction, 5 );
        fadeIn4State->addExitAction( fadeFinishedAction );

        // ensure that we are showing the full UI in normal state
        normalState->addEntryAction( fadeFinishedAction );

        // *** LOL 20080113: Apply the new eIsZoomedAction to Zoom5 and Normal
        UIAction* isZoomedAction = new UIAction( eIsZoomedAction );
        zoom5State->addEntryAction(  isZoomedAction );
        normalState->addEntryAction( isZoomedAction );

        UIAction* startTimerAction = new UIAction( eStartTimerAction );
            inactivityCivicState->addEntryAction( startTimerAction );
            hereAmIState->addEntryAction( startTimerAction );
            // ventilationState->addEntryAction( startTimerAction );   -- CR-120
        UIAction* stopTimerAction = new UIAction( eStopTimerAction );
            inactivityCivicState->addExitAction( stopTimerAction );
            hereAmIState->addExitAction( stopTimerAction );
            ventilationState->addExitAction( stopTimerAction );        // CR-120

        // Finally, add generic actions to the state graph (zoom, suspend, resume and invalidate)
        startState          ->addEntryAction( invalidateAction );
        introState          ->addEntryAction( zoomOutAction    );
        introState          ->addEntryAction( invalidateAction );
        zoom1State          ->addEntryAction( zoomOutAction    );
        zoom2State          ->addEntryAction( invalidateAction );
        zoom3State          ->addEntryAction( invalidateAction );
        zoom4State          ->addEntryAction( invalidateAction );
        zoom5State          ->addEntryAction( zoomInAction     );
        zoom5State          ->addEntryAction( invalidateAction );
        fadeIn1State        ->addEntryAction( invalidateAction );
        fadeIn2State        ->addEntryAction( invalidateAction );
        fadeIn3State        ->addEntryAction( invalidateAction );
        fadeIn4State        ->addEntryAction( invalidateAction );
        normalState         ->addEntryAction( zoomInAction     );
        normalState         ->addEntryAction( invalidateAction );

        countdown5State     ->addEntryAction( zoomInAction     ); // to avoid displaying countdown with an unzoomed stack
        countdown4State     ->addEntryAction( zoomInAction     ); // to avoid displaying countdown with an unzoomed stack
        countdown3State     ->addEntryAction( zoomInAction     ); // to avoid displaying countdown with an unzoomed stack
        countdown2State     ->addEntryAction( zoomInAction     ); // to avoid displaying countdown with an unzoomed stack

        countdown4State     ->addEntryAction( invalidateAction );
        countdown3State     ->addEntryAction( invalidateAction );
        countdown2State     ->addEntryAction( invalidateAction );
        countdown1State     ->addEntryAction( invalidateAction );
        inactivitySoloState ->addEntryAction( invalidateAction );
        inactivityCivicState->addEntryAction( invalidateAction );
        hereAmIState        ->addEntryAction( invalidateAction );
        MER1State           ->addEntryAction( invalidateAction );
        MER2State           ->addEntryAction( invalidateAction );
        logoState           ->addEntryAction( invalidateAction );

        // provide common pointer to manager for all action objects
        UIAction::setManager( this );

        // set the startup state
        m_uiState = initialState;

        // ensure that entry actions are executed
        m_uiState->entry();

        // for simplicity and consistency, the previous states are initially equal to the start state
        m_prevState =  m_uiState;

        // the active startup screen is the screen associated with this state
        m_activeScreen = m_uiState->getScreen();
    }

    // TEMP: method to get direct access to a UI state
    UIState* GraphicsManager::getState( UIStateID id ) const
    {
        StateCollection::const_iterator cit = m_allStates.find( id );
        return( cit == m_allStates.end() ? 0 : cit->second );
    }

    // TEMP: method to get direct access to a UI state id
    UIStateID GraphicsManager::getState() const
    {
        if (m_uiState)
        {
            return m_uiState->getID();
        }
        return static_cast<UIStateID>(0);
    }

    // method to switch UI state
    int GraphicsManager::switchToState( UIStateID id )
    {
        // get access to old state and new state
        UIState* oldState = m_uiState;
        UIState* newState = getState( id );

        // verify that these states exist
        if( 0 == oldState || 0 == newState )
            return -1;

        // execute exit actions on the old state
        oldState->exit();

        // keep track of the previous state
        m_prevState = oldState;

        // switch state by moving the pointer
        m_uiState = newState;

        // execute entry actions on the new state
        newState->entry();

        // done
        return 0;
    }

    //lint -e{429}
    size_t GraphicsManager::addScreen( ScreenID id, Screen* screen )
    {
        m_screens.insert( pair<ScreenID,Screen*>( id, screen ) );
        return m_screens.size();
    }

    Screen* GraphicsManager::getScreen( ScreenID id ) const
    {
        ScreenCollection::const_iterator it;
        if( ( it = m_screens.find( id ) ) == m_screens.end() )
            return 0;
        return it->second;
    }


    // switch active screen (uses id)
    int GraphicsManager::switchScreen( ScreenID newScreenID )
    {
        // get pointer to new screen
        Screen* newScreen = getScreen( newScreenID );
        if( ! newScreen )
            return -1;

        // don't switch screen if it is the same as the current screen
        if( *m_activeScreen == *newScreen )
            return 0;

        lock();

        m_doClear = true; // clear the old screen (lazy/delayed clear - will clear on next frame)

        // switch to new screen
        m_activeScreen = getScreen( newScreenID );

        // invalidate the new screen to force repaint
        invalidate();

        unlock();

        // done
        return 0;
    }

    // switch active screen (uses the actual Screen object)
    int GraphicsManager::switchScreen( Screen* newScreen )
    {
        // don't switch screen if it is the same as the current screen
        if( *m_activeScreen == *newScreen )
            return 0;

        lock();

        m_doClear = true; // clear the old screen (lazy/delayed clear - will clear on next frame)

        // switch to new screen
        m_activeScreen = newScreen;

        // invalidate the new screen to force repaint
        invalidate();

        unlock();

        // done
        return 0;
    }


    int GraphicsManager::modifyScreenSymbol( uint8_t newSymbolID )
    {
        // use screen to switch bitmap on the associated symbol
        if (m_activeScreen)
            m_activeScreen->setActiveSymbol( newSymbolID );
        return 0;
    }

    int GraphicsManager::zoomInScreens()
    {
        // use screen to zoom in widgets
        if (m_activeScreen)
            m_activeScreen->zoomIn();
        return 0;
    }

    int GraphicsManager::zoomOutScreens()
    {
        // use screen to zoom out widgets
        if (m_activeScreen)
            m_activeScreen->zoomOut();
        return 0;
    }

    int GraphicsManager::fadeScreen( uint8_t fadeLevel )
    {
        // use screen to fade widgets
        if (m_activeScreen)
            m_activeScreen->fade( fadeLevel );
        return 0;
    }

    int GraphicsManager::startWidgetTimer()
    {
        // use screen to start widget timers
        if (m_activeScreen)
            m_activeScreen->startWidgetTimer();
        return 0;
    }

    int GraphicsManager::stopWidgetTimer()
    {
        // use screen to stop widget timers
        if (m_activeScreen)
            m_activeScreen->stopWidgetTimer();
        return 0;
    }

    // method to invalidate the manager (i.e. invalidate the active screen)
    void GraphicsManager::invalidate()
    {
        if( m_activeScreen )
            m_activeScreen->invalidate();
    }


    //lint -e{429}
    size_t GraphicsManager::addViewport( ViewportID id, Viewport* viewport )
    {
        m_viewports.insert( pair<ViewportID, Viewport*>( id, viewport ) );
        return m_viewports.size();
    }

    Viewport* GraphicsManager::getViewport( ViewportID id ) const
    {
        ViewportCollection::const_iterator it;
        if( ( it = m_viewports.find( id ) ) == m_viewports.end() )
            return 0;
        return it->second;
    }

    // update method - called by the thread when screen is to be repainted
    void GraphicsManager::update()
    {
        // if zoom counter is >= 6, cleanup and terminate
        if( m_zoomCounter >= 6 )
        {
            m_zoomTimer.remove();
            m_zoomCounter = 0;
        }

        // if fade counter is >=6, cleanup and terminate
        if( m_fadeCounter >= 6 )
        {
            m_fadeTimer.remove();
            m_fadeCounter = 0;
        }

        // if countdown counter is < -2, cleanup and terminate
        if( m_countdownCounter < -2 )
        {
            m_countdownTimer.remove();
            m_countdownCounter = 0;
        }

        lock();
        Screen * screenToDraw = m_activeScreen;
        Screen * screenToClear = 0;
        // check if the previous screen is to be cleared before drawing
        if( m_doClear )
        {
            m_doClear = false;
            screenToClear = m_previouslyDrawnScreen;
        }
        unlock();

        // clear previous screen first
        if (screenToClear)
        {
            screenToClear->clear();
        }

        // finally, call draw on the ACTIVE screen object
        if( screenToDraw )
        {
            screenToDraw->draw();
            m_previouslyDrawnScreen = screenToDraw;
        }

        // now check if display intensity operations are to be performed
        if ( m_performDisplayIntensityOperations )
        {
            lock();
            updateDisplayIntensity();
            unlock();
        }

        // blinkmode handling (ref. HereAmI & Inactivity widgets)
        lock();
        DisplayOnOffOperation_t currDispOp = m_displayOp;
        m_displayOp = eDispOpNoChange;
        unlock();

        if ( currDispOp != eDispOpNoChange )
        {
            if ( eDispOpOff == currDispOp )
            {
                ghw_dispoff();
            }
            else
            {
                ghw_dispon();
            }
        }

    }

    // Helper method for performing display intensity operations
    // Is supposed to be called from update()
    void GraphicsManager::updateDisplayIntensity()
    {
        // undimming
        if (m_targetDisplayIntensityPercent >= 100 && m_currentDisplayIntensityPercent < m_targetDisplayIntensityPercent)
        {
            m_currentDisplayIntensityPercent = m_targetDisplayIntensityPercent;
            simwrby(GHWCMD, GCTRL_LIGHTLEVEL);
            simwrby(GHWWR, GCTRL_LIGHTLEVEL_DATA);
            m_performDisplayIntensityOperations = false;
            return;
        }

        // dimming
        if (m_targetDisplayIntensityPercent < m_currentDisplayIntensityPercent)
        {
            SGUCHAR brightnessValue = static_cast<SGUCHAR>((static_cast<uint16_t>(GCTRL_LIGHTLEVEL_DATA+1) * static_cast<uint16_t>(m_currentDisplayIntensityPercent)) / 100);
            brightnessValue = min(brightnessValue, static_cast<SGUCHAR>(GCTRL_LIGHTLEVEL_DATA));
            brightnessValue = max(brightnessValue, static_cast<SGUCHAR>(1));
            simwrby(GHWCMD, GCTRL_LIGHTLEVEL);
            simwrby(GHWWR, --brightnessValue);
            m_currentDisplayIntensityPercent = static_cast<uint8_t>((static_cast<uint16_t>(brightnessValue) * 100) / GCTRL_LIGHTLEVEL_DATA);
        }
        else
        {
            m_performDisplayIntensityOperations = false;
        }
    }

    void GraphicsManager::displayDepth( int16_t depth_in_mm, bool isNewCompStart,  bool isCompressionReleaseStarted )
    {
        // capture depth
        if (depth_in_mm != m_depth || isNewCompStart)
        {
            m_depth = depth_in_mm;
            if( isNewCompStart )
            {
                resumeAllWidgets();
            }
        }

        // update widgets
        if (m_depthWidget)
            m_depthWidget->updateDepth( m_depth, isNewCompStart, isCompressionReleaseStarted);

    }

    void GraphicsManager::updateCompressionCounter()
    {
        if (m_compCountWidget)
        {
            m_compCountWidget->increment();
        }
    }


    void GraphicsManager::displayRate(  uint16_t rate )
    {
        // capture rate
        if( rate != m_rate )
        {
            m_rate = rate;
            resumeAllWidgets();
        }

        // update widget
        if (m_speedoWidget)
            m_speedoWidget->updateRate( m_rate );
    }

    void GraphicsManager::setShallowDepthWarning(int16_t depth_in_mm)
    {
        if (m_depthWidget)
            m_depthWidget->setShallowDepthWarning(depth_in_mm);
    }

    void GraphicsManager::clearShallowDepthWarning()
    {
        if (m_depthWidget)
            m_depthWidget->clearShallowDepthWarning();
    }

    void GraphicsManager::setIncompleteReleaseWarning(int16_t depth_in_mm)
    {
        if (m_depthWidget)
            m_depthWidget->setIncompleteReleaseWarning(depth_in_mm);
    }

    void GraphicsManager::clearIncompleteReleaseWarning()
    {
        if (m_depthWidget)
            m_depthWidget->clearIncompleteReleaseWarning();
    }


    // method to zoom in on the patient (exposed through interface)
    void GraphicsManager::zoomInOnPatient()
    {
        if (!m_uiState)
            return;

        // this operation is only allowed in the Intro state
        if( eIntro == m_uiState->getID() )
        {
            // comments about the zoom-in:
            // - the start state is Intro
            // - the end state is Zoom5
            // - the sequence is (Intro->Zoom1->Zoom2->Zoom3->Zoom4->Zoom5
            // - the interval between frames is 80 msec

            // create the timer
            m_zoomTimer.create( "zoom", 80, 80, zoomCallback, reinterpret_cast<void*>( this ) );

            // reset the zoom counter
            m_zoomCounter = 0;

            // start the timer
            m_zoomTimer.start();
        }
    }

    // static callback method for the zoom timer
    void GraphicsManager::zoomCallback( void* pCallbackArgs )
    {
        // unpack manager from callback argument
        GraphicsManager* manager = reinterpret_cast<GraphicsManager*>( pCallbackArgs );

        // increment the zoom counter
        manager->m_zoomCounter += 1;

        // first timer tick -> switch to zoom1
        if( 1 == manager->m_zoomCounter )
            manager->switchToState( eZoom1 );

        // second timer tick -> switch to zoom2
        else if( 2 == manager->m_zoomCounter )
            manager->switchToState( eZoom2 );

        // third timer tick -> switch to zoom3
        else if( 3 == manager->m_zoomCounter )
            manager->switchToState( eZoom3 );

        // fourth timer tick -> switch to zoom4
        else if( 4 == manager->m_zoomCounter )
            manager->switchToState( eZoom4 );

        // fifth and final timer tick -> switch to zoom5
        else if( 5 == manager->m_zoomCounter )
           manager->switchToState( eZoom5 );

        // for values >= 6, the timer will be terminated in the next update()
    }

    // method to fade in the rate widget speedometer (exposed through interface)
    bool GraphicsManager::fadeInSpeedometer()
    {
        if (m_uiState)
        {
            // this operation is only allowed in the Zoom5 state
            if( eZoom5 == m_uiState->getID() )
            {
                // comments about the fade-in:
                // - the start state is Zoom5
                // - the end state is Normal
                // - the sequence is (Zoom5->Fade1->Fade2->Fade3->Fade4->Normal)
                // - the interval between frames is 100 msec

                // create the timer
                m_fadeTimer.create( "fade", 100, 100, fadeCallback, reinterpret_cast<void*>( this ) );

                // reset the zoom counter
                m_fadeCounter = 0;

                // start the timer
                m_fadeTimer.start();
                return true; // signal that speedo is displayed
            }
            return ( eNormal == m_uiState->getID() ); // speedo is displayed if we are in normal screen
        }
        return false;
    }

    // staticcallback method for the fade timer
    void GraphicsManager::fadeCallback( void* pCallbackArgs )
    {
        // unpack manager from callback argument
        GraphicsManager* manager = reinterpret_cast<GraphicsManager*>( pCallbackArgs );

        // increment the fade counter
        manager->m_fadeCounter += 1;

        // first timer tick -> switch to fade1
        if( 1 == manager->m_fadeCounter )
            manager->switchToState( eFadeIn1 );

        // second timer tick -> switch to fade2
        else if( 2 == manager->m_fadeCounter )
            manager->switchToState( eFadeIn2 );

        // third timer tick -> switch to fade3
        else if( 3 == manager->m_fadeCounter )
            manager->switchToState( eFadeIn3 );

        // fourth timer tick -> switch to fade4
        else if( 4 == manager->m_fadeCounter )
            manager->switchToState( eFadeIn4 );

        // fifth and final timer tick -> switch to normal
        else if( 5 == manager->m_fadeCounter )
           manager->switchToState( eNormal );

        // for values >= 6, the timer will be terminated in the next update()
    }


    // method to start countdown (exposed through interface)
    int GraphicsManager::startCountdownFrom(uint8_t fromValue, uint32_t periodTime)
    {
        if (0==m_uiState)
        {
            return -1;
        }

        // validate
        if (fromValue < 2 || fromValue > 5 || 0 == periodTime || periodTime > 2000)
        {
            return -1;
        }

        // this operation is not allowed in the StandClear state or in the eVentilation state
        if(   m_uiState->getID() != eStandClear &&
              m_uiState->getID() != eVentilation  )
        {
            // create the timer
            m_countdownTimer.create( "countdwn", periodTime, periodTime, countdownCallback, reinterpret_cast<void*>( this ) );

            // reset the countdown counter
            m_countdownCounter = fromValue;

            switch (m_countdownCounter)
            {
                case 5:
                    switchToState( eCountdown5 );
                    break;
                case 4:
                    switchToState( eCountdown4 );
                    break;
                case 3:
                    switchToState( eCountdown3 );
                    break;
                case 2:
                    switchToState( eCountdown2 );
                    break;
                default:
                    break;

            }

            // start the timer
            m_countdownTimer.start();

            // CR-298 notify client has been moved to FeedbackManager

            return 0; // indicate success
        }

        return -1; // indicate failure
    }

    // method to abort a running countdown
    // CR-274
    void GraphicsManager::abortCountdown()
    {

        // sanity check
        if (0 == m_uiState)
            return;

        if ( m_countdownCounter > 0 || eCountdown1 == m_uiState->getID() ) // if countdown is active
        {
            m_countdownTimer.remove();
            m_countdownCounter = 0;
            countdownCompletedNotification();
            switchToState( eNormal );
        }

    }


    void GraphicsManager::countdownCompletedNotification() const
    {
        // inform the Feedback component about this event
        IFeedbackAdmin* fbAdmin = FeedbackHelper::instance().getAdminInterface();
        if( fbAdmin )
        {
            fbAdmin->countdownCompletedNotification();
        }
    }

    void GraphicsManager::countdownCallback(void * pCallbackArgs)
    {
        // unpack manager from callback argument
        GraphicsManager* manager = reinterpret_cast<GraphicsManager*>( pCallbackArgs );

        if (manager != 0) // protect against NULL pointer
        {
            // decrement the countdown counter
            manager->m_countdownCounter--;

            switch (manager->m_countdownCounter)
            {
                case 5:
                    manager->switchToState( eCountdown5 );
                    break;
                case 4:
                    manager->switchToState( eCountdown4 );
                    break;
                case 3:
                    manager->switchToState( eCountdown3 );
                    break;
                case 2:
                    manager->switchToState( eCountdown2 );
                    break;
                case 1:
                    manager->switchToState( eCountdown1 );
                    break;
                case 0:
                    manager->countdownCompletedNotification();  // signal that we are completed
                    break;
                case -2:
                    manager->switchToState( eNormal ); // exception switch to normal (3 seconds timeout) if not stopped by other means
                    break;
                default:
                    // for values < -2, the timer will be terminated in the next update()
                    break;

            }
        }

    }

    // *** LOL 20080113: New action "callback" for eIsZoomedAction
    void GraphicsManager::depthStackIsZoomed()
    {
        // This method is called using UI state machine / action mechanisms; it is called whenever the UI state machine
        // enters Zoom5 or Normal. There are two actions that need to be performed when this happens:
        // a) Set the m_zoomed flag to true, allowing the isZoomed() method to return this state to its callers
        // b) Talk to the Feedback component and inform about the fact that the depth stack was zoomed

        // action a) - set the m_zoomed flag
        m_zoomed = true;

        // action b) - inform the Feedback component about this event
        IFeedbackAdmin* fbAdmin = FeedbackHelper::instance().getAdminInterface();
        if( fbAdmin )
            fbAdmin->depthStackWasZoomed();
    }

    void GraphicsManager::suspend()
    {
        // reset widget graphics after specified amount of time
        suspendAllWidgets();
    }

    // method to start inactivity display
    void GraphicsManager::inactivityStart( uint16_t initial_value_in_seconds )
    {
        // safety valve
        if (!m_uiState || !m_coreAdminInterface)
            return;

        // inactivity is only allowed from the following states:
        // - Start
        // - Intro
        // - Zoom5
        // - Normal
        if( eStart == m_uiState->getID() || eIntro == m_uiState->getID() ||
            eZoom5 == m_uiState->getID() || eNormal == m_uiState->getID() )
        {
            // inform the widgets about the initial inactivity time
            updateInactivitySubscribers( initial_value_in_seconds );

            // check for Solo vs Civic mode + invoke the corresponding inactivity mode
            if( m_coreAdminInterface->isDeviceModeSolo() )
            {
                // switch to the SOLO inactivity state
                switchToState( eInactivitySolo );
            }
            else
            {
                // switch to the CIVIC inactivity state (timer will start as a result of this state transition)
                switchToState( eInactivityCivic );
            }
        }
    }

    // method to stop inactivity display and resume normal UI
    void GraphicsManager::inactivityStop()
    {
        // NOTE: at this time we should go back to whatever state we were in previously
        switchBackToValidPreviousState();

        // reset the inactivity counter
        updateInactivitySubscribers( 0 );
    }

    // method to set the inactivity time (in seconds)
    void GraphicsManager::setInactivityTime( uint16_t inactivity_time_in_seconds )
    {
        // update the widgets with the new inactivity time
        updateInactivitySubscribers( inactivity_time_in_seconds );
    }


    // ACTUAL "Here am I" handler responding to the CMD_SHOW_HERE_AM_I commmand
    bool GraphicsManager::startHereAmIDisplay()
    {
        // sanity check
        if (0 == m_uiState || 0 == m_commRPLInterface)
            return false;

        // Activating the 'Here am I' screen is only allowed from the following states:
        // - Start
        // - Intro
        // - InactivityCivic
        if(    eStart           == m_uiState->getID()
            || eIntro           == m_uiState->getID()
            || eInactivityCivic == m_uiState->getID() )
        {
            // perform UI state change
            switchToState( eHereAmI );

            // send RPL command to confirm state change
            m_commRPLInterface->send_RPL_HERE_AM_I_SHOWN( CommonDefs::eON );
            return true;
        }
        return false;
    }

    // ACTUAL "Here am I" handler responding to the CMD_SHOW_HERE_AM_I
    void GraphicsManager::stopHereAmIDisplay()
    {
        // sanity check
        if (0 == m_uiState)
            return;

        if ( eHereAmI ==  m_uiState->getID() )
        {
            // switch back to the Intro state)
            switchToState( eIntro );
        }
        // CR-335: reply moved to FeedbackManager
    }

    // method to display the 'Stand Clear' screen (exposed through interface)
    void GraphicsManager::displayStandClearScreen()
    {
        // CR-442, avoid re-entry so that Here-Am-I works afterwards
        if ( m_uiState && m_uiState->getID() !=  eStandClear )
        {
            m_countdownCounter = -100; // so that the timer can be removed in update()
            m_zoomCounter = 100;       // CR-541 - so that the timer can be removed in update() - i.e. terminate any active zooming

            // switch to stand clear state
            switchToState( eStandClear );
        }
    }

    // method to remove the 'Stand Clear' screen (exposed through interface)
    void GraphicsManager::removeStandClearScreen()
    {
        if (m_resetCompCounterAtStandClearScreen && m_compCountWidget)
        {
            m_compCountWidget->resetCount();
        }

        // NOTE: at this time we should go back to whatever state we were in previously
        switchBackToValidPreviousState();
    }


    // ACTUAL method to display the ventilation screen - response to the CMD_VENT_SEQUENCE_START command
    void GraphicsManager::displayVentilationScreen()
    {
        // so that the timer can be removed in update()
        m_countdownCounter = -100;
        m_zoomCounter = 100;       // CR-541 - so that the timer can be removed in update() - i.e. terminate any active zooming

        // switch to ventilation state
        switchToState( eVentilation );

        // send RPL to confirm that we are now prompting for ventilations
        m_commRPLInterface->send_RPL_VENT_SEQUENCE_START();
    }

    // ACTUAL method to display the ventilation screen - response to the CMD_VENT_SEQUENCE_STOP command
    void GraphicsManager::removeVentilationScreen()
    {
        // NOTE: at this time we should go back to whatever state we were in previously
        switchBackToValidPreviousState();

        // CR-335: reply moved to FeedbackManager
    }

    // method for activiating the Logo screen
    void GraphicsManager::showLogoScreen( bool showVersionInfo )
    {
        // modify the logo widget to toggle the version info display
        if( m_logoWidget )
            m_logoWidget->toggleVersionDisplay( showVersionInfo );

        // switch to the logo state
        switchToState( eLogo );
    }

    // method for showing large battery status at shutdown
    // SRS-2365/SRS-2366/CR-322
    void GraphicsManager::showBatteryShutdownScreen(const CommonDefs::BatteryStatus_t batteryStatus)
    {
        if (m_logoWidget && m_uiState && m_uiState->getID() != eLogo )
        {
            // since low and empty battery is shown with large centered image, hide any small icons
            updateBatteryStatus(CommonDefs::eBatteryUnknown);

            m_logoWidget->toggleVersionDisplay(false); // no version display
            switch (batteryStatus)
            {
                case CommonDefs::eBatteryLow:
                    m_logoWidget->replaceLogoWithBatteryLowSymbol();
                    break;
                case CommonDefs::eBatteryEmpty:
                    m_logoWidget->replaceLogoWithBatteryEmptySymbol();
                    break;
                default:
                    break;
            }
        }
        // switch to the logo state
        switchToState( eLogo );
    }

    // method for showing large data card status at shutdown
    void GraphicsManager::showDatacardShutdownScreen(const CommonDefs::DatacardStatus_t cardStatus)
    {
        if (m_logoWidget && m_uiState && m_uiState->getID() != eLogo )
        {
            // since full data card status is shown with large centered image, hide any small icons
            updateDatacardStatus(CommonDefs::eCardNotPresent);

            m_logoWidget->toggleVersionDisplay(false); // no version display
            switch (cardStatus)
            {
                case CommonDefs::eCardAlmostFull:
                case CommonDefs::eCardFull:
                    m_logoWidget->replaceLogoWithDatacardFullSymbol();
                    break;
                case CommonDefs::eCardError:
                    m_logoWidget->replaceLogoWithDatacardErrorSymbol();
                    break;
                default:
                    break;
            }
        }
        // switch to the logo state
        switchToState( eLogo );
    }

    // method for showing large service symbol at shutdown
    void GraphicsManager::showServiceShutdownScreen()
    {
        if (m_logoWidget && m_uiState && m_uiState->getID() != eLogo )
        {
            m_logoWidget->toggleVersionDisplay(true); // show version display
            m_logoWidget->replaceLogoWithServiceSymbol();
        }
        // switch to the logo state
        switchToState( eLogo );
    }

    // method for activiating the Start screen
    void GraphicsManager::displayStartScreen()
    {
        // sanity check
        if (0 == m_uiState)
            return;

        // only switch to the start state of no previous state
        if ( eInitial == m_uiState->getID() )
        {
            switchToState( eStart );
        }
    }

    // method for removing the Start screen
    // Intro screen will be the next screen, unless other active screen
    void GraphicsManager::removeStartScreen()
    {
        // sanity check
        if (0 == m_uiState)
            return;

        // no action if we are already out of the start state
        if ( eStart == m_uiState->getID() )
        {
            switchToState( eIntro );
        }
    }

    // method for activiating the Intro screen
    void GraphicsManager::showIntroScreen()
    {
        // switch to the intro state
        switchToState( eIntro );
    }

    // method to reset the needle position (exposed through interface)
    void GraphicsManager::resetSpeedometerNeedlePosition()
    {
        if( m_speedoWidget )
            m_speedoWidget->resetNeedlePosition();
    }

    // method to redisplay the arrows on the ventilation prompt screen
    bool GraphicsManager::redisplayVentilationArrows()
    {
        // sanity check
        if (0 == m_uiState || 0 == m_commRPLInterface)
            return false;

        // we will only accept this message if we are in the ventilation pause state
        if( eVentilation == m_uiState->getID() && m_ventWidget != 0 )
        {
            m_ventWidget->redisplayArrows();

            // CR-121: CMD_VENTILATE_PROMPT does not generate a reply
            m_commRPLInterface->send_RPL_VENTILATE_PROMPT();
            return true;
        }
        return false;
    }


    int GraphicsManager::configureCompCounterDisplay(const CommonDefs::CompCounterConfig_t & config)
    {
        if (m_compCountWidget)
        {
            m_resetCompCounterAtStandClearScreen = config.m_behaviour & CommonDefs::eCompCountResetsAtStandClear;
            if (m_coreAdminInterface)
            {
                UiSettings_t s;
                if (m_coreAdminInterface->getUiSettings(s))
                {
                    return m_compCountWidget->configure(config, s.m_displayTimeout * 1000); //lint !e734
                }
            }
        }
        return -1;
    }

    // method related to displaying the Mini Event Review screens
    void GraphicsManager::displayMiniEventReview1()
    {
        if (m_MERWidget1 && m_coreAdminInterface)
        {
            SessionPerformanceData_t sessionData;
            if (m_coreAdminInterface->getSessionData(sessionData))
            {
                m_MERWidget1->setValues(sessionData.m_numCompsTotal, sessionData.m_numCompsCorrectRelease,
                                        sessionData.m_numCompsAdequateDepth, sessionData.m_numCompsAdequateRate,
                                        sessionData.m_numCompsWithRateInfo);
            }
        }
        // switch to MER1 state
        switchToState( eMiniEventReview1 );
    }

    // method related to displaying the Mini Event Review screens
    void GraphicsManager::displayMiniEventReview2()
    {
        if (m_MERWidget2 && m_coreAdminInterface)
        {
            SessionPerformanceData_t sessionData;
            if (m_coreAdminInterface->getSessionData(sessionData))
            {
                m_MERWidget2->setValues(sessionData.m_numCompsTotal,
                                        sessionData.m_sessionStopTimeSec-sessionData.m_sessionStartTimeSec, sessionData.m_compInactivityTimeSec);
            }
        }
        // switch to MER2 state
        switchToState( eMiniEventReview2 );
    }

    // method related to removing the Mini Event Review screens
    void GraphicsManager::removeMiniEventReview()
    {
        // sanity check
        if (0 == m_uiState)
            return;

        // no action if we are already out of the MER states
        if ( eMiniEventReview1 == m_uiState->getID() || eMiniEventReview2 == m_uiState->getID())
        {
            if ( ! isZoomed() )
            {
                switchToState(eIntro);
            }
            else
            {
                switchToState(eNormal);
            }
        }
    }

    // method for displaying battery status on relevant screens that has this available
    // SRS-2362/2363/2364/CR-322
    void GraphicsManager::updateBatteryStatus(const CommonDefs::BatteryStatus_t batteryStatus)
    {
        if (m_statusWidget)
        {
            m_statusWidget->updateBatteryStatus(batteryStatus);
        }
    }

    // method for displaying data-card status on relevant screens that has this available
    void GraphicsManager::updateDatacardStatus(const CommonDefs::DatacardStatus_t cardStatus)
    {
        if (m_statusWidget)
        {
            m_statusWidget->updateDatacardStatus(cardStatus);
        }
    }

    // method for displaying a status text on relevant screens that has this available
    void GraphicsManager::displayStatusText(const std::string& statusText)
    {
        if (m_statusWidget)
        {
            m_statusWidget->displayStatusText(statusText);
        }
    }

    // metod for resuming normal UI
    void GraphicsManager::resumeNormalDisplayMode()
    {
        // switch to normal state
        switchToState( eNormal );
    }

    // method to add a widget to the total set of widgets in the UI system
    size_t GraphicsManager::addWidget( Widget* aWidget )
    {
        m_allWidgets.push_back( aWidget );
        return m_allWidgets.size();
    }

    int GraphicsManager::subscribeToInactivity( Widget* subscriber )
    {
        m_inactivitySubscribers.push_back( subscriber );
        return 0;
    }

    void GraphicsManager::updateInactivitySubscribers( uint16_t inactivity_in_seconds )
    {
        for_each(m_inactivitySubscribers.begin(), m_inactivitySubscribers.end(),
                 bind2nd(mem_fun(Widget::updateInactivity), inactivity_in_seconds));
    }

    void GraphicsManager::suspendAllWidgets()
    {
        if ( !m_widgetsSuspended )
        {
            for_each(m_allWidgets.begin(), m_allWidgets.end(), mem_fun(Widget::suspend));
            m_widgetsSuspended = true;
        }
    }

    void GraphicsManager::resumeAllWidgets()
    {
        if ( m_widgetsSuspended )
        {
            for_each(m_allWidgets.begin(), m_allWidgets.end(), mem_fun(Widget::resume));
            m_widgetsSuspended = false;
        }
    }


    void GraphicsManager::switchBackToValidPreviousState()
    {
        // switch back to the previous state, unless we are in some strange state (like countdown states etc.)
        if ( m_prevState != 0 && 
            ( eStart == m_prevState->getID() || eIntro == m_prevState->getID() || eZoom5 == m_prevState->getID() ) )
        {
            // never return to start state
            if ( eStart == m_prevState->getID() )
            {
                switchToState( eIntro );
            }
            else if ( eZoom5 == m_prevState->getID() && 
                      (eStandClear == m_uiState->getID() || eVentilation == m_uiState->getID()) ) // CR-463
            {
                switchToState( eNormal );
            }
            else
            {
                switchToState( m_prevState->getID() );
            }
        }
        else
        {
            switchToState( eNormal );
        }
    }

    // lock semaphore
    void GraphicsManager::lock()
    {
        m_screenMutex->lock();
    }

    // unlock semaphore
    void GraphicsManager::unlock()
    {
        m_screenMutex->release();
    }

    // test method for OLED display (static compression screen)
    void GraphicsManager::oledTestScreen1()
    {
        // switch to normal state
        switchToState( eNormal );

        // make sure widgets are zoomed in and faded in
        zoomInScreens();
        fadeScreen(3);

        // set depth and rate values directly
        m_depthWidget->updateDepth( 30, true, false );
        m_speedoWidget->updateRate( 110 );

        // invalidate the screen
        invalidate();
    }

    // test method for OLED display (static green here-am-I screen)
    void GraphicsManager::oledTestScreen2()
    {
        // hack to show the static green background of the here-am-I (i.e. no animation)

        // switch to normal state
        switchToState( eNormal );

        lock();
        m_activeScreen = 0;
        m_doClear = false;  // avoid clear by update()
        m_displayBlinkOperationsAllowed = false;

        gselvp(0);  // switch to default viewport
        gclrvp();

        const GPALETTE_RGB * palette = SymLib::hereAmIPalette;
        ghw_palette_wr(0, 16, palette);

        PGSYMBOL symbol = reinterpret_cast<PGSYMBOL>(&SymLib::hereAmIScreen[0]);
        gputsym( 0, 9, symbol );

        unlock();
    }

    // *** LOL 20071101: requested by John Sigve for the test process (shows yellow inactivity screen)
    void GraphicsManager::oledTestScreen3()
    {
        // hack to show the static yellow background of the inactivity (i.e. no animation)

        // switch to normal state
        switchToState( eNormal );

        lock();

        m_activeScreen = 0;
        m_doClear = false;  // avoid clear by update()
        m_displayBlinkOperationsAllowed = false;

        gselvp(0);  // switch to default viewport
        gclrvp();

        const GPALETTE_RGB * palette = SymLib::inactivityPalette;
        ghw_palette_wr(0, 16, palette);

        PGSYMBOL symbol = reinterpret_cast<PGSYMBOL>(&SymLib::inactivityScreen[0]);
        gputsym( 0, 0, symbol );

        unlock();
    }

     // test method for setting the rate display interval directly
    void GraphicsManager::setRateInterval( int interval )
    {
        if( m_speedoWidget )
            m_speedoWidget->setDisplayInterval( static_cast<uint8_t>(interval) );
    }

    // method called when the Puck changes mode mid-session (Solo <-> Civic)
    void GraphicsManager::processOperatingModechange() const
    {
        // nothing to do
    }


//======== End of $Workfile: GraphicsManager.cpp $ ==========

