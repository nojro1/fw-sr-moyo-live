/*****************************************************************************
 * Project:   LittleSister PUCK software (ARM)
 *
 * Copyright: (c) Laerdal Medical 2006
 *
 * $Workfile: SymbolContainer.cpp $
 * $Author: Nogte1 $
 * $Revision: 8 $
 *
 * $Log: /LittleSister/src/Puck/UI/Framework/SymbolContainer.cpp $
 * 
 * 8     28.09.07 23:25 Nogte1
 * Removed some warning level 3 lint warnings
 * 
 * 7     9.07.07 12:15 Nogte1
 * Use for_each algorithm instead of loops
 *
 * 6     25.04.07 19:35 Nogte1
 * Optimization: made some frequently called methods return void
 *
 * 5     22.02.07 20:35 Nogte1
 * Fixed warnings from PC-Lint
 *
 * 4     29.01.07 16:15 Nolol1
 *
 * 3     26.01.07 16:47 Nolol1
 * Invalidate + SetActiveSymbol
 *
 * 2     25.01.07 16:26 Nolol1
 * Added support for nested symbols
 *
 */

// include self first
#include "UI/Framework/SymbolContainer.h"

// andre includes
#include "UI/Framework/Symbol.h"

#include <algorithm>
#include <functional>

//namespace LittleSister
//{
    //! constructor
    SymbolContainer::SymbolContainer()
        : m_invalidate( true )
    {

    }

    //! destructor
    SymbolContainer::~SymbolContainer()
    {

    }

    // draw method
    void SymbolContainer::draw()
    {
        if( m_invalidate )
        {
            for_each(m_symbols.begin(), m_symbols.end(), mem_fun(Symbol::draw));
            m_invalidate = false;
        }
    }

    // method to invalidate the collection
    void SymbolContainer::invalidate()
    {
        m_invalidate = true;
        for_each(m_symbols.begin(), m_symbols.end(), mem_fun(Symbol::invalidate));
    }

    // method to modify screen symbol
    void SymbolContainer::setActiveSymbol( uint8_t newSymbolID )
    {
        for_each(m_symbols.begin(), m_symbols.end(), bind2nd(mem_fun(Symbol::setActive), newSymbolID));
        //for_each(m_symbols.begin(), m_symbols.end(), mem_fun(Symbol::invalidate));
    }

    // returns the new number of symbols
    size_t SymbolContainer::addSymbol( Symbol* symbol )
    {
        m_symbols.push_back( symbol );
        return m_symbols.size();
    }

    Symbol* SymbolContainer::getSymbol( size_t index ) const
    {
        return ( index >= m_symbols.size() ) ? 0 : m_symbols[index];
    }

    // returns 0 if not able to remove, or removed the last one, else the new number of symbols
    size_t SymbolContainer::removeSymbol( size_t index )
    {
        if( index >= m_symbols.size() )
            return 0;
        SymbolCollection::iterator it = m_symbols.begin() + index;
        m_symbols.erase( it );
        return m_symbols.size();
    }

//} // end namespace LittleSister

//======== End of $Workfile: SymbolContainer.cpp $ ==========

