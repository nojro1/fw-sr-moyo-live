/***********************************************************************
 *
 * This file contains I/O port definition for the target system and is
 * included by sgio.h
 *
 * This file is normally generated automatically by the SGSETUP tool using
 * the sgio_pc.h header file as a template. The definitions here correspond
 * to I/O port definitions in sgio_pc.h but are here using the I/O register
 * definition syntax for the target compiler.
 *
 * The example definitions here assume that the LCD I/O registers are
 * memory mapped at fixed addresses, and that the register SELECT line on
 * the display is connected to address bus bit 0 signal for 8 bit bus and.
 * to address bus bit 1 signal for a 16 bit bus.
 *
 * Modify these definitions so they fit the actual target system and the
 * I/O register definition syntax used by the actual target compiler.
 *
 * Copyright (c) RAMTEX International 2006
 * Version 1.0
 *
 **********************************************************************/
#include "stm32l552xx.h"

 /* Used only in GHW_BUS8 mode */
#define GHWWR  (* (SGUCHAR volatile *) ( 0x60020000 ))
#define GHWRD  (* (SGUCHAR volatile *) ( 0x60020000 ))
#define GHWSTA (* (SGUCHAR volatile *) ( 0x60000000 ))
#define GHWCMD (* (SGUCHAR volatile *) ( 0x60000000 ))

/* Used only in GHW_BUS16 mode */
#define GHWWRW  (* (SGUINT volatile *) ( FMC_BANK1_4 | 0x00800000UL )) //Using FSMC and A22 for SELECT //( 0x60020000 )) //Using FSMC and A16 for SELECT
#define GHWRDW  (* (SGUINT volatile *) ( FMC_BANK1_4 | 0x00800000UL )) //Using FSMC and A22 for SELECT //( 0x60020000 )) //Using FSMC and A16 for SELECT
#define GHWSTAW (* (SGUINT volatile *) ( FMC_BANK1_4 )) //Using FSMC and A22 for SELECT //( 0x60000000 )) //Using FSMC and A16 for SELECT
#define GHWCMDW (* (SGUINT volatile *) ( FMC_BANK1_4 )) //Using FSMC and A22 for SELECT //( 0x60000000 )) //Using FSMC and A16 for SELECT

/* Used only in GHW_BUS32 mode */
#define GHWWRDW  (* (SGULONG volatile *) ( 0x60020000 ))
#define GHWRDDW  (* (SGULONG volatile *) ( 0x60020000 ))
#define GHWSTADW (* (SGULONG volatile *) ( 0x60000000 ))
#define GHWCMDDW (* (SGULONG volatile *) ( 0x60000000 ))


