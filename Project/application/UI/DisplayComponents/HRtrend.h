//! @class  UIThread
//! @brief
//! @author Jan Arild R�yneberg
//! @date   09.12.2013
//!

#ifndef _HR_TREND_H_
#define _HR_TREND_H_

#include "thread.h"
#include "includes.h"
#include <gdisp.h>
#include "fhrbuffer.h"
#include "commondefs.h"


class HRtrend
{
    
public:
	enum HrTrendMode_t
    {
        eModeHalfScreen = 0,
        eModeFullScreen
    };
    
    HRtrend(SGUCHAR a_graphViewPort, SGUCHAR a_statusViewPort, HrTrendMode_t a_mode);

	~HRtrend();
    
    typedef struct
    {
        GXT xStart;
        GYT yStart;
        GXT xEnd;
        GYT yEnd;
    } RectangleCoordinates_t;
    
    static const RectangleCoordinates_t ViewPortFrameSmall;
    static const RectangleCoordinates_t ViewPortFrameBig;
    static const RectangleCoordinates_t HrTrendFrameSmall;
    static const RectangleCoordinates_t HrTrendFrameBig;
    static const uint8_t maxHR;
    static const uint8_t minHR;
    
    void draw(void);
	void Reset(void);
    void Add(uint8_t a_HR);
    void SetHrLimits(uint8_t a_highLimit, uint8_t a_lowLimit);
    void visible(bool a_visible);
    void SetAlarm(bool a_alarm);

    
protected:

private:

    SGUCHAR m_viewPort;         //For graph
    SGUCHAR m_statusViewPort;   //For "BPM". Is located in status viewport.
    uint8_t m_FhrLimitLower;
    uint8_t m_FhrLimitUpper;
    uint8_t m_maxTime;
    uint32_t m_currentTime;
    uint32_t m_startTime;
    uint8_t m_currHR;
    GXT m_currHrTrendXpos; //Used to detect if we should update trend
    GXT m_currAbsHrTrendXpos; //Used to keep track of last (oldest) trend pos
    uint8_t m_noTicks;
    RectangleCoordinates_t m_trendFrameCoord;
    RectangleCoordinates_t m_viewPortCoord;
    static const uint8_t TREND_PIXEL_WITH = 2;
    HrTrendMode_t m_mode;
    FhrBuffer m_fhrBuffer;
    bool m_visible;
    bool m_alarm;
    static const uint8_t BPM_X_POS = 3; 
    static const uint8_t BPM_Y_POS = 19;
    
    GXT GetTrendFrameXposFromTime(uint32_t a_time, uint16_t a_maxTime);
    GYT GetTrendFrameYposFromHR(uint8_t a_HR);
    std::string GetTrendTimeMaxTxt(HrTrendMode_t a_mode);
    std::string GetTrendTimeTxt(HrTrendMode_t a_mode);
    void DrawTrendPixel(GXT a_xPos, FhrBuffer::HrInfo_t a_hrInfo, bool a_clear);
    void DrawStartLine();
    void DrawTicks(void);
    
};

#endif
