//! @class  BatteryCapacity
//! @brief
//! @author Jan Arild R�yneberg
//! @date   14.03.2014
//!

#ifndef _BATTERY_CAPACITY_BIG_H_
#define _BATTERY_CAPACITY_BIG_H_

#include "includes.h"
#include <gdisp.h>
#include "UI/Framework/Symbol.h"
#include "UI/Symlib/SymLib.h"


class BatteryCapacityBig
{
    
public:
	BatteryCapacityBig(SGUCHAR a_viewPort, GXT x_pos, GYT y_pos);
	~BatteryCapacityBig();
    
    enum BlinkState_t {eBSinactive, eBSoff, eBSon};
    
    void draw(void);
    void invalidate(void);
    void setRemCapacity(uint8_t a_remCapacity);
    void visible(bool a_visible);
    void setChargingState(bool a_on);
    void tick(void);
	
protected:

private:
    BatteryCapacityBig(const BatteryCapacityBig& right);            //!< block the copy constructor
    BatteryCapacityBig& operator=(const BatteryCapacityBig& right); //!< block the assignment operator
    
    SGUCHAR m_viewPort;
    
    Symbol* m_batteryFrame;
    Symbol* m_capacityBlockLow;
    Symbol* m_capacityBlock33;
    Symbol* m_capacityBlock67;
    Symbol* m_capacityBlock100;
    
    uint8_t m_remCapacity;
    bool m_forceDraw;
    bool m_charging;
    BlinkState_t m_blinkState;
    bool m_blinkOn;
    static const uint16_t BLINK_PERIOD = 1000;
    uint16_t m_tickTime;
    uint16_t m_tickTimer;
    bool m_visible;
    
};

#endif
