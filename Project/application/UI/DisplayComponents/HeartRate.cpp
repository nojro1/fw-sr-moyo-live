//! \file    Heartrate.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    14.03.2014

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  14Mar2014, JRo, Original.

#include "Heartrate.h"

//
//! Construction
//
Heartrate::Heartrate(SGUCHAR a_viewPort, GXT x_pos, GYT y_pos,
                     GXT a_vpXs, GYT a_vpYs, GXT a_vpXe, GYT a_vpYe,
                     Symbol* a_icon, HRmode_t a_mode)
{
    m_HRmode = a_mode;
    
    if(m_HRmode == eHRMmaternalHr)
    { 
        m_backgroundColor = G_DDARKGREY;
    }
    else
    { 
        m_backgroundColor = G_BLACK;
    }

    gselvp(a_viewPort);
    gsetvp(a_vpXs, a_vpYs, a_vpXe, a_vpYe);
    gsetcolorb(m_backgroundColor);
    gclrvp();
    
    m_tickTime = 10; //TODO: set from parameter
    m_tickTimer = BLINK_PERIOD / m_tickTime;
    
    m_hr = 0;
    m_alarm.alarmByte = 0;
    m_viewPort = a_viewPort;
    
    m_currentColor = eCIinsideLimColor;
    
    m_maxHR = 250;
    m_minHR = 50;
    
    m_icon = a_icon;
    
    // create symbols
    if(a_mode == eHRMmaternalHr)
    {
        m_MHRhundreds = new Symbol( 10, 0, 0);
        m_MHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greyDigits[ 0 ] ) );
        m_MHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greyDigits[ 1 ] ) );
        m_MHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greyDigits[ 2 ] ) );
        m_MHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greyDigits[ 3 ] ) );
        m_MHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greyDigits[ 4 ] ) );
        m_MHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greyDigits[ 5 ] ) );
        m_MHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greyDigits[ 6 ] ) );
        m_MHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greyDigits[ 7 ] ) );
        m_MHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greyDigits[ 8 ] ) );
        m_MHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greyDigits[ 9 ] ) );
        
        // use the copy constructor to create the other digit symbols
        m_MHRtens = new Symbol( *m_MHRhundreds );
        m_MHRones = new Symbol( *m_MHRhundreds );
        
        m_hundreds = m_MHRhundreds;
        m_tens     = m_MHRtens;
        m_ones     = m_MHRones;
        
        m_MHRquestionMark = new Symbol( 1, 0, 0);
        m_MHRquestionMark->add( reinterpret_cast<PGSYMBOL>( &SymLib::questionMarkGrey[0] ) );
        
        m_questionMark = m_MHRquestionMark;
        
        m_MHRbpm = new Symbol( 3, 0, 0);
        m_MHRbpm->add( reinterpret_cast<PGSYMBOL>( &SymLib::bpm_grey[0] ) );
        
        m_bpm = m_MHRbpm;
        
    }
    else
    {
        m_FHRhundreds = new Symbol( 30, 0, 0);
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greenDigits[ 0 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greenDigits[ 1 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greenDigits[ 2 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greenDigits[ 3 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greenDigits[ 4 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greenDigits[ 5 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greenDigits[ 6 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greenDigits[ 7 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greenDigits[ 8 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::greenDigits[ 9 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::yellowDigits[ 0 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::yellowDigits[ 1 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::yellowDigits[ 2 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::yellowDigits[ 3 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::yellowDigits[ 4 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::yellowDigits[ 5 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::yellowDigits[ 6 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::yellowDigits[ 7 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::yellowDigits[ 8 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::yellowDigits[ 9 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::redDigits[ 0 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::redDigits[ 1 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::redDigits[ 2 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::redDigits[ 3 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::redDigits[ 4 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::redDigits[ 5 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::redDigits[ 6 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::redDigits[ 7 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::redDigits[ 8 ] ) );
        m_FHRhundreds->add( reinterpret_cast<PGSYMBOL>( &SymLib::redDigits[ 9 ] ) );
        
        // use the copy constructor to create the other digit symbols
        m_FHRtens = new Symbol( *m_FHRhundreds );
        m_FHRones = new Symbol( *m_FHRhundreds );
        
        m_hundreds = m_FHRhundreds;
        m_tens     = m_FHRtens;
        m_ones     = m_FHRones;
        
        m_FHRquestionMark = new Symbol( 2, 0, 0);
        m_FHRquestionMark->add( reinterpret_cast<PGSYMBOL>( &SymLib::questionMarkYellow[0] ) );
        m_FHRquestionMark->add( reinterpret_cast<PGSYMBOL>( &SymLib::questionMarkRed[0] ) );
        
        m_questionMark = m_FHRquestionMark;
        
        m_FHRbpm = new Symbol( 3, 0, 0);
        m_FHRbpm->add( reinterpret_cast<PGSYMBOL>( &SymLib::bpm[0] ) );
        m_FHRbpm->add( reinterpret_cast<PGSYMBOL>( &SymLib::bpm[1] ) );
        m_FHRbpm->add( reinterpret_cast<PGSYMBOL>( &SymLib::bpm[2] ) );
    
        m_bpm = m_FHRbpm;
        
    }
    
    m_hundreds->    setBackgroundColor(m_backgroundColor);
    m_tens->        setBackgroundColor(m_backgroundColor);
    m_ones->        setBackgroundColor(m_backgroundColor);
    m_bpm->         setBackgroundColor(m_backgroundColor);
    m_questionMark->setBackgroundColor(m_backgroundColor);
    
    if(m_icon)
    {
        m_icon->setBackgroundColor(m_backgroundColor);
        m_icon->setPos(x_pos-72, y_pos);
        m_icon->invalidate();
    }
    
    if(m_HRmode == eHRMfetalHr)
    { 
        m_hundreds->setPos(x_pos, y_pos);
        m_tens->setPos(x_pos+45, y_pos);
        m_ones->setPos(x_pos+91, y_pos);
        m_bpm->setPos(x_pos+136, y_pos+49);
        m_questionMark->setPos(x_pos+18, y_pos);
    }
    else
    {
        m_hundreds->setPos(x_pos, y_pos);
        m_tens->setPos(x_pos+22, y_pos);
        m_ones->setPos(x_pos+44, y_pos);
        m_bpm->setPos(x_pos+71, y_pos+20);
        m_questionMark->setPos(x_pos+7, y_pos);
    }
    
    
    m_bpm->setActive(0);
    m_bpm->invalidate();
    m_questionMark->setActive(0);
    m_questionmarkActive = false;
    m_numbersActive = false;

    m_HrLimitLower = 0;
    m_HrLimitUpper = 250;
    
    m_blinkState = eBSoff;
    m_blinkOn = false;
    m_visible = false;
    
}

//
//! Destruction
//
Heartrate::~Heartrate()
{
    delete m_hundreds;
    delete m_tens;
    delete m_ones;
    delete m_bpm;
    delete m_questionMark;
}

//
//! Set whether the symbol should be visible or not (does not (re)paint
//! during update calls)
//
void Heartrate::visible(bool a_visible)
{
    gselvp(m_viewPort);
    if(a_visible && !m_visible)
    {
        m_visible = true;
        eraseNumbers();
        eraseQuestionMark();
        gsetcolorb(m_backgroundColor);
        gclrvp();
        invalidate();
        draw();
    }
    else if(!a_visible && m_visible)
    {
        eraseNumbers();
        eraseQuestionMark();
        
        gsetcolorb(G_BLACK);
        gclrvp();
        
        m_visible = false;
    }
}

//
//! Tick for animation. 
//! Note: Must be called at interval defined by m_tickTime
//
void Heartrate::tick(void)
{
    if(m_blinkOn)
    {
        if(m_tickTimer)
        {
            m_tickTimer--;
        }
        else
        {
            m_tickTimer = BLINK_PERIOD / m_tickTime;
            
            if(m_blinkState == eBSoff)
            {
                m_blinkState = eBSon;
                
                if(m_visible)
                {
                    eraseNumbers();
                    drawQuestionMark();
                    
                    if(m_HRmode == eHRMfetalHr)
                    { 
                        drawBPM();
                    }
                }
            }
            else if(m_blinkState == eBSon)
            {
                m_blinkState = eBSoff;
                if(m_visible)
                {
                    eraseNumbers();
                    eraseQuestionMark();
                    if(m_HRmode == eHRMfetalHr)
                    { 
                        eraseBPM();
                    }
                }
            }
        }
    }
        
}

//
//! Invaldate all components so they will be repainted on next draw()
//
void Heartrate::invalidate(void)
{
    m_hundreds->invalidate();
    m_tens->invalidate();
    m_ones->invalidate();
    m_bpm->invalidate();
    m_questionMark->invalidate();
    
    if(m_icon)
    {
        m_icon->invalidate();
    }
}

//
//! Draw the symbol
//
void Heartrate::draw(void)
{
    if(m_visible)
    {
        
        gselvp(m_viewPort);
        gsetcolorb(m_backgroundColor);
        
        ColorIdx_t lastColor = m_currentColor;
        
        if(m_HRmode == eHRMfetalHr)
        { 
            if(m_alarm.alarmBits.abnormalFHRalarm)
            {
                m_currentColor = eCIalarmColor;
            }
            else if(m_hr >= m_HrLimitLower && m_hr <= m_HrLimitUpper)
            {
                m_currentColor = eCIinsideLimColor;
            }
            else if(m_hr < m_HrLimitLower || m_hr > m_HrLimitUpper)
            {
                m_currentColor = eCIoutsideLimColor;
            }
        }
        
        
        
        
        
        if(lastColor != m_currentColor)
        {
            m_bpm->invalidate();
            m_hundreds->invalidate();
            m_tens->invalidate();
            m_ones->invalidate();
            
            if(m_icon)
            {
                m_icon->invalidate();
            }
        }
        
        if(m_hr < m_minHR)
        {
            eraseNumbers();
            drawQuestionMark();
        }
        else
        {
            eraseQuestionMark();
            drawNumbers();
        }   
        
        if(m_icon)
        {
            m_icon->draw();
        }
      
        if(m_HRmode == eHRMfetalHr)
        { 
            m_bpm->setActive(m_currentColor);
        }
        m_bpm->invalidate();
        m_bpm->draw();
    }
}

//
//! Draw the numbers
//
void Heartrate::drawNumbers(void)
{
    gselvp(m_viewPort);
    gsetcolorb(m_backgroundColor);
    
    //DEBUG_TRACE("\r\nHeartrate::drawNumbers - Color: %u\r\n", m_currentColor);
    
    m_hundreds->setActive((m_hr / 100)        + (m_currentColor*10));
    m_tens    ->setActive(((m_hr % 100) / 10) + (m_currentColor*10));
    m_ones    ->setActive((m_hr % 10)         + (m_currentColor*10));
    
    if(m_hr < 10)
    {
        m_hundreds->erase();
        m_tens->erase();
        m_ones->draw();
    }
    else if(m_hr < 100)
    {
        m_hundreds->erase();
        m_tens->draw();
        m_ones->draw();
    }
    else
    {
        m_hundreds->draw();
        m_tens->draw();
        m_ones->draw();
    }
    
    m_numbersActive = true;
}

//
//! Erase the numbers
//
void Heartrate::eraseNumbers(void)
{
    if(m_numbersActive)
    {
        gselvp(m_viewPort);
        gsetcolorb(m_backgroundColor);
        
        m_hundreds->invalidate();
        m_tens->invalidate();
        m_ones->invalidate();
        
        m_hundreds->erase();
        m_tens->erase();
        m_ones->erase();
        
        m_numbersActive = false;
    }
}

//
//! Draw the "-?-"
//
void Heartrate::drawQuestionMark(void)
{
    if(!m_questionmarkActive)
    {
        gselvp(m_viewPort);
        gsetcolorb(m_backgroundColor);
        
        //DEBUG_TRACE("\r\nHeartrate::drawQuestionMark - Color: %u\r\n", m_currentColor);
        
        m_questionMark->invalidate();
     
        if(m_HRmode == eHRMfetalHr)
        {
            if(m_alarm.alarmBits.abnormalFHRalarm)
            {
                m_questionMark->setActive(1);
            }
            else
            {
                m_questionMark->setActive(0);
            }
        }
        
        m_questionMark->draw();
        
        m_questionmarkActive = true;
    }
}

//
//! Erase the "-?-"
//
void Heartrate::eraseQuestionMark(void)
{
    if(m_questionmarkActive)
    {
        gselvp(m_viewPort);
        gsetcolorb(m_backgroundColor);
        
        m_questionMark->invalidate();
        m_questionMark->erase();
        m_questionmarkActive = false;
    }
}

//
//! Draw the "BPM"
//
void Heartrate::drawBPM(void)
{
    gselvp(m_viewPort);
    gsetcolorb(m_backgroundColor);
    
    m_bpm->invalidate();
 
    m_bpm->setActive(m_currentColor);
    
    m_bpm->draw();
    
    
}
//
//! Erase the "BPM"
//
void Heartrate::eraseBPM(void)
{
    gselvp(m_viewPort);
    gsetcolorb(m_backgroundColor);
    
    m_bpm->invalidate();
    m_bpm->erase();
}


//
//! Set the limit for when the "-?-" should be drawn
//
void Heartrate::setQuestionmarkLimit(uint8_t a_limit)
{
    m_minHR = a_limit;
}
    
//
//! Set hr value and draw the numbers
//
void Heartrate::setHR(uint16_t a_hr)
{
    if(!m_blinkOn)
    {
        //Do not update if blinking is active. This is controlled from tick()
        if(m_hr != a_hr)
        {
            m_hr = a_hr;
            draw();
        }
    }
    
    m_hr = a_hr;
}

//
//! Set limits for where color changes
//
void Heartrate::SetHrLimits(uint8_t a_highLimit, uint8_t a_lowLimit)
{
    //Check high limit
    if(a_highLimit > m_maxHR)
    {
        m_HrLimitUpper = m_maxHR;
    }
    else if(a_highLimit <= m_minHR)
    {
        m_HrLimitUpper = m_minHR + 1;
    }
    else
    {
        m_HrLimitUpper = a_highLimit;
    }
    
    //Check low limit
    if(a_lowLimit < m_minHR)
    {
        m_HrLimitLower = m_minHR;
    }
    else if(a_lowLimit >= m_HrLimitUpper)
    {
        m_HrLimitLower = m_HrLimitUpper - 1;
    }
    else
    {
        m_HrLimitLower = a_lowLimit;
    }
    
    draw();
    
}

//
//! Set an alarm
//
void Heartrate::setAlarm(Alarm_t a_alarm)
{
    m_alarm = a_alarm;
    
    //Force update numbers and questionmark
    m_questionmarkActive = false;
    m_numbersActive = false;
    
    if(a_alarm.alarmBits.abnormalFHRalarm)
    {
        m_blinkOn = false;
        draw();
    }
    else if(a_alarm.alarmBits.lostSignalAlarm)
    {
        draw();
        m_blinkOn = true;
        
    }
}

//
//! Clear an alarm
//
void Heartrate::clearAlarm(void)
{
    m_alarm.alarmByte = 0;
    m_blinkOn = false;
    
    //Force update numbers and questionmark
    m_questionmarkActive = true;
    m_numbersActive = true;
    
    setHR(m_hr);
    draw();
}


