//! \file    Clock.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    25.04.2014

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  25Apr2014, JRo, Original.

#include "Clock.h"


//
//! Construction
//
Clock::Clock(SGUCHAR a_viewPort, GXT x_pos, GYT y_pos)
{
    m_viewPort = a_viewPort;
    
    m_xPos = x_pos;
    m_yPos = y_pos;
    m_lastTime.Hours = 0;
    m_lastTime.Minutes = 0;
    m_lastTime.Seconds = 0;
    m_lastDate.Month = 0;
    m_lastDate.Day = 0;
    m_lastDate.Year = 0;
    m_visible = false;
    m_showDate = false;
}

//
//! Destruction
//
Clock::~Clock()
{
    
}

//
//! Invaldate clock so it will be repainted on next update
//
void Clock::invalidate(void)
{
    m_lastTime.Hours = 0;
    m_lastTime.Minutes = 0;
    m_lastTime.Seconds = 0;
    m_lastDate.Month = 0;
    m_lastDate.Day = 0;
    m_lastDate.Year = 0;
}

//
//! Set position for clock. Position is relative for current viewport
//
void Clock::setPos(GXT x_pos, GYT y_pos)
{
    char buffer[25];

    sprintf(buffer, "                   \0");
    gselvp(m_viewPort);
    gsetpos(m_xPos, m_yPos);
    gselfont(&moyo_mono8_8);
    gsetcolorf(G_WHITE);
    gsetcolorb(G_BLACK);
    gputs(buffer);
    
    m_xPos = x_pos;
    m_yPos = y_pos;
}

//
//! Draw the clock
//
void Clock::draw(void)
{
    if(m_visible)
    {
        char buffer[25];

        //Time
        sprintf(buffer, "%0.2u:%0.2u\0", m_lastTime.Hours, m_lastTime.Minutes);
        gselvp(m_viewPort);
        gsetpos(m_xPos, m_yPos);
        gselfont(&moyo_mono8_8);
        gsetcolorf(G_WHITE);
        gsetcolorb(G_BLACK);
        gputs(buffer);
        
        //Date
        if(m_showDate)
        {
            sprintf(buffer, "%0.2u.%0.2u.20%0.2u\0", m_lastDate.Day, m_lastDate.Month, m_lastDate.Year);
            gsetpos(m_xPos + 60, m_yPos);
            gputs(buffer);
        }
        
        
    }
}

//
//! (Re)Draw the clock if time changed
//
void Clock::update(void)
{
    LL_RTC_TimeTypeDef time = HwAbsl::getpHw()->getpRtc()->RTCgetTime();
    LL_RTC_DateTypeDef date = HwAbsl::getpHw()->getpRtc()->RTCgetDate();
    
    if(time.Hours != m_lastTime.Hours ||
       time.Minutes != m_lastTime.Minutes)      //Clock resolution is minute, so don't test on seconds
    {
        m_lastTime.Hours = time.Hours;
        m_lastTime.Minutes = time.Minutes;
        m_lastTime.Seconds = time.Seconds;
        
        m_lastDate.Month = date.Month;
        m_lastDate.Day = date.Day;
        m_lastDate.Year = date.Year;
        
        draw();
    }
}

//
//! Set whether the clock should be visible or not (does not (re)paint
//! during update calls)
//
void Clock::visible(bool a_visible)
{
    if(a_visible && !m_visible)
    {
        m_visible = true;
        update();
        draw();
    }
    else if(!a_visible && m_visible)
    {
        gselvp(m_viewPort);
        gsetpos(m_xPos, m_yPos);
        gselfont(&moyo_mono8_8);
        gsetcolorf(G_WHITE);
        gsetcolorb(G_BLACK);
        if(m_showDate)
        {
            gputs("                  ");            
        }
        else
        {
            gputs("      ");
        }
        
    }
    
    m_visible = a_visible;
}

//
//! Enable or disable date (time will always show)
//
void Clock::showDate(bool a_show)
{
    m_showDate = a_show;
    update();
}