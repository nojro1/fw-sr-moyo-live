//! @class  Heartrate
//! @brief
//! @author Jan Arild R�yneberg
//! @date   14.03.2014
//!

#ifndef _HEART_RATE_H_
#define _HEART_RATE_H_

#include "includes.h"
#include "commondefs.h"
#include <gdisp.h>
#include "UI/Framework/Symbol.h"
#include "UI/Symlib/SymLib.h"

enum HRmode_t {eHRMfetalHr, eHRMmaternalHr};

class Heartrate
{
    
public:
    Heartrate(SGUCHAR a_viewPort, GXT x_pos, GYT y_pos,
                     GXT a_vpXs, GYT a_vpYs, GXT a_vpXe, GYT a_vpYe,
                     Symbol* a_icon, HRmode_t a_mode);
	~Heartrate();
    
    enum BlinkState_t {eBSinactive, eBSoff, eBSon};
    
    enum ColorIdx_t {eCIinsideLimColor = 0, eCIoutsideLimColor = 1, eCIalarmColor = 2};
    
    void draw(void);
    void invalidate(void);
    void setHR(uint16_t a_hr);
    void SetHrLimits(uint8_t a_highLimit, uint8_t a_lowLimit);
    void setQuestionmarkLimit(uint8_t a_limit);
    void setAlarm(Alarm_t a_alarm);
    void clearAlarm(void);
    void visible(bool a_visible);
    void tick(void);
	
protected:
    static void blinkCallback(void * pCallbackArgs);
    
private:
    Heartrate(const Heartrate& right);            //!< block the copy constructor
    Heartrate& operator=(const Heartrate& right); //!< block the assignment operator
    
    void drawNumbers(void);
    void eraseNumbers(void);
    void drawQuestionMark(void);
    void eraseQuestionMark(void);
    void drawBPM(void);
    void eraseBPM(void);
    bool Lock();
    bool Unlock();
    
    BlinkState_t m_blinkState;
    bool m_blinkOn;
    static const uint16_t BLINK_PERIOD = 1000;
    uint16_t m_tickTime;
    uint16_t m_tickTimer;
    
    
    // symbols for the digits
    Symbol* m_hundreds;
    Symbol* m_tens;
    Symbol* m_ones;
    Symbol* m_FHRhundreds;
    Symbol* m_FHRtens;
    Symbol* m_FHRones;
    Symbol* m_MHRhundreds;
    Symbol* m_MHRtens;
    Symbol* m_MHRones;
    
    Symbol* m_bpm;
    Symbol* m_FHRbpm;
    Symbol* m_MHRbpm;
    Symbol* m_questionMark;
    Symbol* m_FHRquestionMark;
    Symbol* m_MHRquestionMark;
    Symbol* m_icon;
    
    SGUCHAR m_viewPort;
    
    ColorIdx_t m_currentColor;
    GCOLOR m_backgroundColor;
    
    uint8_t m_HrLimitLower;
    uint8_t m_HrLimitUpper;
    Alarm_t m_alarm;
    bool m_questionmarkActive;
    bool m_numbersActive;
    uint16_t m_hr;
    uint8_t m_maxHR;
    uint8_t m_minHR;
    bool m_visible;
    HRmode_t m_HRmode;
    

};

#endif
