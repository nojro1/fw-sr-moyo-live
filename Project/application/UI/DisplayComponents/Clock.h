//! @class  Clock
//! @brief
//! @author Jan Arild R�yneberg
//! @date   25.04.2014
//!

#ifndef _CLOCK_H_
#define _CLOCK_H_

#include "includes.h"
#include <gdisp.h>
#include "rtc.h"

class Clock
{
    
public:
	Clock(SGUCHAR a_viewPort, GXT x_pos, GYT y_pos);
	~Clock();
    
    void draw(void);
    void invalidate(void);
    void update(void);
    void visible(bool a_visible);
    void setPos(GXT x_pos, GYT y_pos);
    void showDate(bool a_show);
    
protected:

private:
    Clock(const Clock& right);            //!< block the copy constructor
    Clock& operator=(const Clock& right); //!< block the assignment operator
    
    SGUCHAR m_viewPort;
    GXT m_xPos;
    GYT m_yPos;
    LL_RTC_TimeTypeDef m_lastTime;
    LL_RTC_DateTypeDef m_lastDate;
    bool m_visible;
    bool m_showDate;
};

#endif
