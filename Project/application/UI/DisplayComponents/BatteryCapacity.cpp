//! \file    BatteryCapacity.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    14.03.2014

//	  PROJECT:    Fetal Heart Rate Monitor
//
//	  HISTORY (version, date, signature, note):
//	  14Mar2014, JRo, Original.

#include "BatteryCapacity.h"

//
//! Construction
//
BatteryCapacity::BatteryCapacity(SGUCHAR a_viewPort, GXT x_pos, GYT y_pos)
{
    m_viewPort = a_viewPort;
    m_isCharging = false;
    
    m_tickTime = 10; //TODO: set from parameter
    m_tickTimer = BLINK_PERIOD / m_tickTime;
    
    m_battery = new Symbol(5, 0, 0);
    
    m_battery->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteries[0]));
    m_battery->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteries[1]));
    m_battery->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteries[2]));
    m_battery->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteries[3]));
    m_battery->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteries[4]));
        
    m_battery->setPos(x_pos, y_pos);
    
    m_remCapacity = 100;
    
    m_battery->setActive(0);
    
    m_blinkState = eBSinactive;
    m_visible = false;
    
}

//
//! Destruction
//
BatteryCapacity::~BatteryCapacity()
{
    delete m_battery;
}

//
//! Invaldate all components so they will be repainted on next draw()
//
void BatteryCapacity::invalidate(void)
{
    m_battery->invalidate();
}

//
//! Set capacity for the battery symbol.
//! Enable charging symbol if charged
//
void BatteryCapacity::setRemCapacity(uint8_t a_remCapacity)
{
    m_remCapacity = a_remCapacity;
    
    gselvp(m_viewPort);
    
    gsetcolorb(G_BLACK);
	gsetcolorf(G_WHITE);
    
    if(m_isCharging)
    {
        m_battery->setActive(4);
        m_battery->draw();
    }
    else if(a_remCapacity > 67)
    {
        m_battery->setActive(3);
        m_battery->draw();
    }
    else if(a_remCapacity > 33)
    {
        m_battery->setActive(2);
        m_battery->draw();
    }
    else if(a_remCapacity >= BATT_CRITICAL_LIMIT)
    {
        m_battery->setActive(1);
        m_battery->draw();
    }
    else
    {
        gsetcolorf(G_RED);
        m_battery->setActive(0);
        m_battery->draw();
    }
    
}

//
//! Draw the symbol
//
void BatteryCapacity::draw(void)
{
    if(m_visible && m_blinkState != eBSoff)
    {
        setRemCapacity(m_remCapacity);
        
    }
}

//
//! Set battery low indication
//
void BatteryCapacity::setAlarm(Alarm_t a_alarm)
{
    m_alarm = a_alarm;
    
    if(a_alarm.alarmBits.lowBatteryAlarm)
    {
        m_blinkState = eBSoff;
        invalidate();
        draw();
    }
    
}

//
//! Clear battery low indication
//
void BatteryCapacity::clearAlarm(void)
{
    m_alarm.alarmByte = 0;
    m_blinkState = eBSinactive;
    
    invalidate();
    draw();
    
}

//
//! Tick for animation. 
//! Note: Must be called at interval defined by BLINK_PERIOD
//
void BatteryCapacity::tick(void)
{
    if(m_tickTimer)
    {
        m_tickTimer--;
    }
    else
    {
        m_tickTimer = BLINK_PERIOD / m_tickTime;
        
        if(m_blinkState == eBSoff)
        {
            m_blinkState = eBSon;
            
            if(m_visible)
            {
                m_battery->invalidate();
                draw();
            }
        }
        else if(m_blinkState == eBSon)
        {
            m_blinkState = eBSoff;
            if(m_visible)
            {
                gselvp(m_viewPort);
                gsetcolorb(G_BLACK);
                gsetcolorf(G_WHITE);
                m_battery->erase();
            }
        }
    }
}

//
//! Set whether the symbol should be visible or not (does not (re)paint
//! during update calls)
//
void BatteryCapacity::visible(bool a_visible)
{
    if(a_visible && !m_visible)
    {
        m_visible = true;
        draw();
    }
    
    m_visible = a_visible;
}

//
//! Set whether charging indicator should be set at next draw
//
void BatteryCapacity::charging(bool a_isCharging)
{
    m_isCharging = a_isCharging;
}

