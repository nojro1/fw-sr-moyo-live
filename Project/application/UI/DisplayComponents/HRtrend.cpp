//! \file    HRtrend.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    12.03.2014

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  12Mar2014, JRo, Original.

#include "hrtrend.h"

const uint8_t HRtrend::maxHR = 200;
const uint8_t HRtrend::minHR = 50;

char const* TXT_BPM         = "BPM     ";
char const* TXT_BPM_CLEAR   = "        ";
char const* TXT_MINUTES     = " min ";
char const* TXT_MIN_BPM     = "50";
char const* TXT_MAX_BPM     = "200";
char const* TXT_30_MINUTES  = "30";

//
//! Construction
//
HRtrend::HRtrend(SGUCHAR a_graphViewPort, SGUCHAR a_statusViewPort, HrTrendMode_t a_mode)//, Mutex* a_mutex)
{
    m_viewPort = a_graphViewPort;
    m_statusViewPort = a_statusViewPort;
    m_mode = a_mode;
    m_alarm = false;
    
    //Set viewport coordinates and default values related to size
    if(a_mode == eModeHalfScreen)
    {
        m_viewPortCoord.xStart      = HRtrend::ViewPortFrameSmall.xStart;
        m_viewPortCoord.yStart      = HRtrend::ViewPortFrameSmall.yStart;
        m_viewPortCoord.xEnd        = HRtrend::ViewPortFrameSmall.xEnd;
        m_viewPortCoord.yEnd        = HRtrend::ViewPortFrameSmall.yEnd;
        
        m_trendFrameCoord.xStart    = HRtrend::HrTrendFrameSmall.xStart;
        m_trendFrameCoord.yStart    = HRtrend::HrTrendFrameSmall.yStart;
        m_trendFrameCoord.xEnd      = HRtrend::HrTrendFrameSmall.xEnd;
        m_trendFrameCoord.yEnd      = HRtrend::HrTrendFrameSmall.yEnd;

        m_maxTime = 30; //minutes
        m_noTicks = 6;
    }
    else
    {
        m_viewPortCoord.xStart      = HRtrend::ViewPortFrameBig.xStart;
        m_viewPortCoord.yStart      = HRtrend::ViewPortFrameBig.yStart;
        m_viewPortCoord.xEnd        = HRtrend::ViewPortFrameBig.xEnd;
        m_viewPortCoord.yEnd        = HRtrend::ViewPortFrameBig.yEnd;
        
        m_trendFrameCoord.xStart    = HRtrend::HrTrendFrameBig.xStart;
        m_trendFrameCoord.yStart    = HRtrend::HrTrendFrameBig.yStart;
        m_trendFrameCoord.xEnd      = HRtrend::HrTrendFrameBig.xEnd;
        m_trendFrameCoord.yEnd      = HRtrend::HrTrendFrameBig.yEnd;

        m_maxTime = 30; //minutes
        m_noTicks = 6;
        
        //Current x graph len is 181 pixels. 
        //This corresponds to 19.89 seconds per dot (2x2 pixels). There is one update for every two pixels.
        
        //Current Y graph higth is 119 pixels. 
        //This corresponds to 1.26 BPM per pixel (2x2 pixels). There is one update for every one pixel.
    }
    
    m_fhrBuffer.Create(((m_trendFrameCoord.xEnd - m_trendFrameCoord.xStart)/TREND_PIXEL_WITH)+1);
    
    m_FhrLimitLower = 110;
    m_FhrLimitUpper = 160;
    
    
    gselvp(m_viewPort);
    gsetvp(m_viewPortCoord.xStart,
           m_viewPortCoord.yStart,
           m_viewPortCoord.xEnd,
           m_viewPortCoord.yEnd);
    gclrvp();
    
    m_visible = false;
    m_currHrTrendXpos = m_trendFrameCoord.xEnd;
    m_currAbsHrTrendXpos = m_trendFrameCoord.xEnd - TREND_PIXEL_WITH;
    
    Reset();
    
}

//
//! Destruction
//
HRtrend::~HRtrend()
{
}


//
//! Viewport coordinates for half screen version
//
const HRtrend::RectangleCoordinates_t HRtrend::ViewPortFrameSmall =
{
    0, 106, 219, 169
};

//
//! Viewport coordinates for full screen version
//
const HRtrend::RectangleCoordinates_t HRtrend::ViewPortFrameBig =
{
    0, 26, 219, 175
};

//
//! Trend frame coordinates for half screen version
//
const HRtrend::RectangleCoordinates_t HRtrend::HrTrendFrameSmall =
{
    //Coordinates relative to viewport
    (GXT)( 32 - HRtrend::ViewPortFrameSmall.xStart), 
    (GYT)(111 - HRtrend::ViewPortFrameSmall.yStart),
    (GXT)(212 - HRtrend::ViewPortFrameSmall.xStart),
    (GYT)(152 - HRtrend::ViewPortFrameSmall.yStart)
};

//
//! Trend frame coordinates for full screen version
//
const HRtrend::RectangleCoordinates_t HRtrend::HrTrendFrameBig =
{
    //Coordinates relative to viewport
    (GXT)( 31 - HRtrend::ViewPortFrameBig.xStart), 
    (GYT)( 33 - HRtrend::ViewPortFrameBig.yStart),
    (GXT)(212 - HRtrend::ViewPortFrameBig.xStart),
    (GYT)(152 - HRtrend::ViewPortFrameBig.yStart)
};

//
//! (Re)Draw the trend
//
void HRtrend::draw(void)
{
    gselvp(m_viewPort);
	gsetcolorb(G_BLACK);
	
    //Draw trend frame
    gsetcolorf(G_DDARKGREY);
    grectangle(m_trendFrameCoord.xStart,
               m_trendFrameCoord.yStart,
               m_trendFrameCoord.xEnd,
               m_trendFrameCoord.yEnd);
    
    //Thicker lines on upper and lower part of trend frame
    gmoveto(m_trendFrameCoord.xStart, m_trendFrameCoord.yStart+1);
    glineto(m_trendFrameCoord.xEnd, m_trendFrameCoord.yStart+1);
    gmoveto(m_trendFrameCoord.xStart, m_trendFrameCoord.yEnd-1);
    glineto(m_trendFrameCoord.xEnd, m_trendFrameCoord.yEnd-1);
        
    
    //Draw HR alarm frame
    
    GYT yPos;
    
    yPos = GetTrendFrameYposFromHR(m_FhrLimitLower);
    gmoveto(m_trendFrameCoord.xStart+1, yPos);
    glineto(m_trendFrameCoord.xEnd, yPos);
    gmoveto(m_trendFrameCoord.xStart+1, yPos-1);
    glineto(m_trendFrameCoord.xEnd, yPos-1);
    
    yPos = GetTrendFrameYposFromHR(m_FhrLimitUpper);
    gmoveto(m_trendFrameCoord.xStart+1, yPos);
    glineto(m_trendFrameCoord.xEnd, yPos);
    gmoveto(m_trendFrameCoord.xStart+1, yPos-1);
    glineto(m_trendFrameCoord.xEnd, yPos-1);
    
    
    DrawTicks();
    
    //Draw time line
    gsetcolorf(G_GREY);
    gmoveto(m_trendFrameCoord.xStart, m_trendFrameCoord.yEnd + 10);
    glineto(m_trendFrameCoord.xEnd, m_trendFrameCoord.yEnd + 10);
    gmoveto(m_trendFrameCoord.xStart, m_trendFrameCoord.yEnd + 5);
    glineto(m_trendFrameCoord.xStart, m_trendFrameCoord.yEnd + 15);
    gmoveto(m_trendFrameCoord.xEnd, m_trendFrameCoord.yEnd + 5);
    glineto(m_trendFrameCoord.xEnd, m_trendFrameCoord.yEnd + 15);
    
    
    //Draw text and numbers
    gsetcolorf(G_GREY);
    gselfont(&moyo_mono8_8);
    
    std::string txt = " " + GetTrendTimeMaxTxt(m_mode);
    txt += TXT_MINUTES;
    gsetpos(GetTrendFrameXposFromTime((m_maxTime/2)*60000, m_maxTime) - (gpstrlen(txt.c_str())/2), m_trendFrameCoord.yEnd + gpstrheight(txt.c_str()) + 5);
    gputs(txt.c_str());
    
    txt = TXT_MIN_BPM;
    gsetpos(m_trendFrameCoord.xStart - gpstrlen(txt.c_str()) - 9, GetTrendFrameYposFromHR(minHR) + (gpstrheight(txt.c_str()) / 2));
    gputs(txt.c_str());
    
    txt = TXT_MAX_BPM;
    gsetpos(m_trendFrameCoord.xStart - gpstrlen(txt.c_str()) - 3, GetTrendFrameYposFromHR(maxHR) + (gpstrheight(txt.c_str()) / 2));
    gputs(txt.c_str());
    
    //FHR limit low
    char c[5];
    sprintf(c, "%u", m_FhrLimitLower);
    txt = std::string(c);
    gsetpos(m_trendFrameCoord.xStart - gpstrlen(txt.c_str()) - 3, GetTrendFrameYposFromHR(m_FhrLimitLower) + (gpstrheight(txt.c_str()) / 2));
    gputs(txt.c_str());
    
    //FHR limit high
    sprintf(c, "%u", m_FhrLimitUpper);
    txt = std::string(c);
    gsetpos(m_trendFrameCoord.xStart - gpstrlen(txt.c_str()) - 3, GetTrendFrameYposFromHR(m_FhrLimitUpper) + (gpstrheight(txt.c_str()) / 2));
    gputs(txt.c_str());
    
    //BPM
    gselvp(m_statusViewPort);
    gsetcolorf(G_GREY);
    gselfont(&moyo_mono8_8);
    gsetpos(BPM_X_POS, BPM_Y_POS);
    gputs(TXT_BPM);
    
    
    gselvp(m_viewPort);
    
    //Draw graph
    //Begin at oldest trend pixel
    GXT xPos = m_currAbsHrTrendXpos;
    bool stop = true;
    FhrBuffer::HrInfo_t lastHR;
    
    if(m_fhrBuffer.GetLast(&lastHR))
    {
        stop = false;
    }
    
    while(xPos < m_trendFrameCoord.xEnd && !stop)
    {
        
        if(xPos != m_trendFrameCoord.xStart) //On trend frame
        {
            DrawTrendPixel(xPos, lastHR, false);
        }

        xPos+=TREND_PIXEL_WITH;
        
        if(!m_fhrBuffer.GetNext(&lastHR))
        {
            //No more buffer entries
            stop = true;
        }
    }
    
    DrawStartLine();
}
        
//
//! Get the time line max text
//
std::string HRtrend::GetTrendTimeMaxTxt(HrTrendMode_t a_mode)
{
    switch(a_mode)
    {
        case eModeHalfScreen:
            return TXT_30_MINUTES;
        case eModeFullScreen:
            return TXT_30_MINUTES;
        default:
            return "";
    }
}

//
//! Get the time line scale text
//
std::string HRtrend::GetTrendTimeTxt(HrTrendMode_t a_mode)
{
    switch(a_mode)
    {
        case eModeHalfScreen:
            return "min";
        case eModeFullScreen:
            return "min";
        default:
            return "";
            
    }
}

//
//! Reset the trend
//
void HRtrend::Reset(void)
{
    m_startTime = OS_GetTime();
    m_currentTime = 0;
    m_currAbsHrTrendXpos = m_trendFrameCoord.xEnd;
    m_currHR = minHR;
}

//
//! Set whether the trend should be visible or not (does not (re)paint
//! during update calls)
//
void HRtrend::visible(bool a_visible)
{
    if(a_visible && !m_visible)
    {
        draw();
    }
    else if(m_visible)
    {
        //Do not clear if not visible, as this will clear other components
        gselvp(m_viewPort);
        gsetcolorb(G_BLACK);
        gclrvp();
        
        //BPM
        gselvp(m_statusViewPort);
        gsetcolorf(G_BLACK);
        gselfont(&moyo_mono8_8);
        std::string txt = "   ";
        gsetpos(BPM_X_POS, BPM_Y_POS);
        gputs(TXT_BPM);
    
    }
    
    m_visible = a_visible;
}

//
//! Add a point to the trend if calculated position has incremented to next pixel
//! Update start line with correct color
//! Save point and alarm state so we keep track of them between trend is visible
//
void HRtrend::Add(uint8_t a_HR)
{
    uint32_t time = OS_GetTime() - m_startTime;
    
    FhrBuffer::HrInfo_t lastHR;
    FhrBuffer::HrInfo_t currHR;
    currHR.hr = a_HR;
    currHR.alarm = m_alarm;
    
    uint8_t newPos = GetTrendFrameXposFromTime(time, m_maxTime);
    
    m_currHR = a_HR;
    
    //Update startline so we get correct color regardless of trend pixels update
    DrawStartLine();
    
    //Time to update trend pixels?
    if(abs(m_currHrTrendXpos - newPos) >= TREND_PIXEL_WITH)
    {
        //DEBUG_TRACE("\r\nTrend update: currpos:%u, newpos: %u", m_currHrTrendXpos, newPos);
     
        if(m_visible)
        {
            //Begin at oldest trend pixel
            GXT xPos = m_currAbsHrTrendXpos;
       
            gselvp(m_viewPort);
            
            bool stop = true;
            if(m_fhrBuffer.GetLast(&lastHR))
            {
                stop = false;
            }
            
            
            while(xPos < m_trendFrameCoord.xEnd && !stop)
            {
                //Move trend one pixel to left
                
                //Clear old pixel
                
                //Do not draw on frame
                if(GetTrendFrameYposFromHR(lastHR.hr) >= m_trendFrameCoord.yStart+TREND_PIXEL_WITH && GetTrendFrameYposFromHR(lastHR.hr) <= m_trendFrameCoord.yEnd-TREND_PIXEL_WITH)
                {
                    
                    if(xPos >= (m_trendFrameCoord.xStart + TREND_PIXEL_WITH - 1)) //On trend frame
                    {
                        DrawTrendPixel(xPos, lastHR, true);
                    }
                
                    //Move pixel
                    if(xPos >= (m_trendFrameCoord.xStart+TREND_PIXEL_WITH)) //On trend frame
                    {
                        DrawTrendPixel(xPos-TREND_PIXEL_WITH, lastHR, false);
                    }
                }
                
                xPos+=TREND_PIXEL_WITH;
                
                if(!m_fhrBuffer.GetNext(&lastHR))
                {
                    //No more buffer entries
                    stop = true;
                }
            }
            
            
            //Draw new pixel
            DrawTrendPixel(m_trendFrameCoord.xEnd - TREND_PIXEL_WITH, currHR, false);
        
        }
    
        //Add to history buffer
        m_fhrBuffer.Put(currHR);
        
        if(m_currAbsHrTrendXpos > (m_trendFrameCoord.xStart + m_viewPortCoord.xStart) + 1)
        {
            m_currAbsHrTrendXpos-=TREND_PIXEL_WITH;
        }
        
        m_currHrTrendXpos = GetTrendFrameXposFromTime(time, m_maxTime);
        m_currentTime = time;   
    }
}

//
//! Draw or clear a trend pixel. If cleared pixel is on a component (e.g. FHR limit line), restore it 
//
void HRtrend::DrawTrendPixel(GXT a_xPos, FhrBuffer::HrInfo_t a_hrInfo, bool a_clear)
{
    //Draw new pixel
    GYT yPos = GetTrendFrameYposFromHR(a_hrInfo.hr);
    
    //Do not draw on frame
    if(yPos >= m_trendFrameCoord.yStart+TREND_PIXEL_WITH && yPos < m_trendFrameCoord.yEnd-TREND_PIXEL_WITH)
    {
        if(a_clear)
        {
            //Set color according to current background (FHR limits = dark grey, other = black
            GYT yPosUpper = GetTrendFrameYposFromHR(m_FhrLimitUpper);
            GYT yPosLower = GetTrendFrameYposFromHR(m_FhrLimitLower);
            
            if(yPos <= yPosUpper && 
               yPos >= yPosUpper - TREND_PIXEL_WITH )
            {
                //On FHR upper limits, restore line after clear
                gsetcolorf(G_BLACK);
                grectangle(a_xPos, yPos, a_xPos + TREND_PIXEL_WITH - 1, yPos + TREND_PIXEL_WITH - 1);
                //DEBUG_TRACE("\r\nRect BLK: xs:%u ys:%u xe:%u ye:%u", a_xPos, yPos, a_xPos + TREND_PIXEL_WITH - 1, yPos + TREND_PIXEL_WITH - 1);
                gsetcolorf(G_DDARKGREY);
                grectangle(a_xPos, yPosUpper - 1, a_xPos + TREND_PIXEL_WITH - 1, yPosUpper);
                //DEBUG_TRACE("\r\nRect DGR: xs:%u ys:%u xe:%u ye:%u", a_xPos, yPosUpper - 1, a_xPos + TREND_PIXEL_WITH - 1, yPosUpper);
            }
            else if(yPos <= yPosLower && 
                    yPos >= yPosLower - TREND_PIXEL_WITH )
            {
                //On FHR lower limits, restore line after clear
                gsetcolorf(G_BLACK);
                grectangle(a_xPos, yPos, a_xPos + TREND_PIXEL_WITH - 1, yPos + TREND_PIXEL_WITH - 1);
                //DEBUG_TRACE("\r\nRect BLK: xs:%u ys:%u xe:%u ye:%u", a_xPos, yPos, a_xPos + TREND_PIXEL_WITH - 1, yPos + TREND_PIXEL_WITH - 1);
                gsetcolorf(G_DDARKGREY);
                grectangle(a_xPos, yPosLower - 1, a_xPos + TREND_PIXEL_WITH - 1, yPosLower);
                //DEBUG_TRACE("\r\nRect DGR: xs:%u ys:%u xe:%u ye:%u", a_xPos, yPosLower - 1, a_xPos + TREND_PIXEL_WITH - 1, yPosLower);
            }
            else
            {
                //In black area
                gsetcolorf(G_BLACK);
                grectangle(a_xPos, yPos, a_xPos + TREND_PIXEL_WITH - 1, yPos + TREND_PIXEL_WITH - 1);
            }
        }
        else
        {
            if(a_hrInfo.alarm)
            {
                gsetcolorf(G_MOYO_RED);
            }
            else if(a_hrInfo.hr <= m_FhrLimitUpper && a_hrInfo.hr >= m_FhrLimitLower)
            {
                gsetcolorf(G_MOYO_GREEN);
            }
            else
            {
                gsetcolorf(G_MOYO_YELLOW);
            }
            grectangle(a_xPos, yPos, a_xPos + TREND_PIXEL_WITH - 1, yPos + TREND_PIXEL_WITH - 1);
            
            //Refresh ticks if in ticks area
            if(yPos <= m_trendFrameCoord.yStart + 3 + TREND_PIXEL_WITH ||
               yPos >= m_trendFrameCoord.yEnd - 3 - TREND_PIXEL_WITH)
            {
                DrawTicks();
            }
        }
        
    }
    
    
}

//
//! Set limits for atypical alarm limit lines
//
void HRtrend::SetHrLimits(uint8_t a_highLimit, uint8_t a_lowLimit)
{
    //Check high limit
    if(a_highLimit > maxHR)
    {
        m_FhrLimitUpper = maxHR;
    }
    else if(a_highLimit <= minHR)
    {
        m_FhrLimitUpper = minHR + 1;
    }
    else
    {
        m_FhrLimitUpper = a_highLimit;
    }
    
    //Check low limit
    if(a_lowLimit < minHR)
    {
        m_FhrLimitLower = minHR;
    }
    else if(a_lowLimit >= m_FhrLimitUpper)
    {
        m_FhrLimitLower = m_FhrLimitUpper - 1;
    }
    else
    {
        m_FhrLimitLower = a_lowLimit;
    }
    
}

//
//! Get xposition related to time considered trend size
//
GXT HRtrend::GetTrendFrameXposFromTime(uint32_t a_time, uint16_t a_maxTime)
{
    return (GXT)(m_trendFrameCoord.xEnd - (((float)a_time/1000.0/(float)a_maxTime/60.0)*(m_trendFrameCoord.xEnd - m_trendFrameCoord.xStart)));
}

//
//! Get yposition related to HR considered trend size
//
GYT HRtrend::GetTrendFrameYposFromHR(uint8_t a_HR)
{
    float yPos;
    yPos = (a_HR - HRtrend::minHR)/((float)HRtrend::maxHR - (float)HRtrend::minHR);
    yPos *= (m_trendFrameCoord.yEnd - m_trendFrameCoord.yStart);
    yPos = m_trendFrameCoord.yEnd - yPos;
    
    return (GYT)yPos;
}

//
//! Set an alarm
//
void HRtrend::SetAlarm(bool a_alarm)
{
    m_alarm = a_alarm;
}
       
//
//! Draw start line in correct color
//
void HRtrend::DrawStartLine()
{
    if(m_visible)
    {
        gselvp(m_viewPort);
        
        GCOLOR color;
        
        if(m_alarm)
        {
            color = G_MOYO_RED;
        }
        else if(m_currHR <= m_FhrLimitUpper && m_currHR >= m_FhrLimitLower)
        {
            color = G_MOYO_GREEN;
        }
        else
        {
            color = G_MOYO_YELLOW;
        }
        
        gsetcolorf(color);
        grectangle(m_trendFrameCoord.xEnd, 
                   m_trendFrameCoord.yStart,
                   m_trendFrameCoord.xEnd+1,
                   m_trendFrameCoord.yEnd);
    }
}
    
//
//! Draw time ticks
//
void HRtrend::DrawTicks(void)
{
    uint8_t interval = m_maxTime / m_noTicks;
    gsetcolorf(G_DDARKGREY);
    for(uint8_t x = interval; x < m_maxTime; x += interval)
    {
        GXT xPos = GetTrendFrameXposFromTime(x*60000, m_maxTime);
        
        //Upper
        gmoveto(xPos, m_trendFrameCoord.yStart);
        glineto(xPos, m_trendFrameCoord.yStart+5);
        gmoveto(xPos+1, m_trendFrameCoord.yStart);
        glineto(xPos+1, m_trendFrameCoord.yStart+5);
        
        //Lower
        gmoveto(xPos, m_trendFrameCoord.yEnd);
        glineto(xPos, m_trendFrameCoord.yEnd-5);
        gmoveto(xPos+1, m_trendFrameCoord.yEnd);
        glineto(xPos+1, m_trendFrameCoord.yEnd-5);
    }
}