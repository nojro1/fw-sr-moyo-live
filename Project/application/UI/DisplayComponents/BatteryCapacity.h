//! @class  BatteryCapacity
//! @brief
//! @author Jan Arild R�yneberg
//! @date   14.03.2014
//!

#ifndef _BATTERY_CAPACITY_H_
#define _BATTERY_CAPACITY_H_

#include "includes.h"
#include "commondefs.h"
#include <gdisp.h>
#include "UI/Framework/Symbol.h"
#include "UI/Symlib/SymLib.h"


class BatteryCapacity
{
    
public:
	BatteryCapacity(SGUCHAR a_viewPort, GXT x_pos, GYT y_pos);
	~BatteryCapacity();
    
    enum BlinkState_t {eBSinactive, eBSoff, eBSon};
    
    static const uint16_t BLINK_PERIOD = 1000;
    
    void draw(void);
    void invalidate(void);
    void setRemCapacity(uint8_t a_remCapacity);
    void showFrame(bool a_show);
    void visible(bool a_visible);
    void setAlarm(Alarm_t a_alarm);
    void clearAlarm(void);
    void charging(bool a_isCharging);
    void tick(void);
    
	
private:
    BatteryCapacity(const BatteryCapacity& right);            //!< block the copy constructor
    BatteryCapacity& operator=(const BatteryCapacity& right); //!< block the assignment operator
    
    SGUCHAR m_viewPort;
    
    uint16_t m_tickTime;
    uint16_t m_tickTimer;
    BlinkState_t m_blinkState;

    Symbol* m_battery;
    
    uint8_t m_remCapacity;
    Alarm_t m_alarm;
    bool m_visible;
    bool m_isCharging;

    
};

#endif
