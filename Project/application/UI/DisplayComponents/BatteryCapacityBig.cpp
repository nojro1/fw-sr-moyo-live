//! \file    BatteryCapacityBig.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    10.04.2014

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  10Apr2014, JRo, Original.


#include "BatteryCapacityBig.h"


//
//! Construction
//
BatteryCapacityBig::BatteryCapacityBig(SGUCHAR a_viewPort, GXT x_pos, GYT y_pos)
{
    m_viewPort = a_viewPort;
    
    m_tickTime = 10; //TODO: set from parameter
    m_tickTimer = BLINK_PERIOD / m_tickTime;
    
    m_batteryFrame = new Symbol(1, 0, 0);
    m_capacityBlock33 = new Symbol(2, 0, 0);
    m_capacityBlock67 = new Symbol(2, 0, 0);
    m_capacityBlock100 = new Symbol(2, 0, 0);
    m_capacityBlockLow = new Symbol(1, 0, 0);
    
    m_batteryFrame->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryFrameBig));
    m_capacityBlock33->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryCapacityBlockBig[0]));
    m_capacityBlock33->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryCapacityBlockBig[1]));
    m_capacityBlock67->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryCapacityBlockBig[2]));
    m_capacityBlock67->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryCapacityBlockBig[3]));
    m_capacityBlock100->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryCapacityBlockBig[4]));
    m_capacityBlock100->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryCapacityBlockBig[5]));
    m_capacityBlockLow->add( reinterpret_cast<PGSYMBOL>(&SymLib::batteryCapacityBlockBig[0])); //TODO
    
    m_batteryFrame->setPos(x_pos, y_pos);
    m_capacityBlockLow->setPos(x_pos+5, y_pos+5);
    m_capacityBlock33->setPos(x_pos+10, y_pos+21);
    m_capacityBlock67->setPos(x_pos+47, y_pos+21);
    m_capacityBlock100->setPos(x_pos+84, y_pos+21);
    
    m_remCapacity = 100;
    m_forceDraw = true;
    m_charging = false;
    
    m_batteryFrame->setActive(0);
    m_capacityBlock33->setActive(1);
    m_capacityBlock67->setActive(1);
    m_capacityBlock100->setActive(1);
    m_capacityBlockLow->setActive(0);
    
    m_blinkState = eBSoff;
    m_blinkOn = false;
    m_visible = false;
    
}

//
//! Destruction
//
BatteryCapacityBig::~BatteryCapacityBig()
{
    delete m_batteryFrame;
    delete m_capacityBlock33;
    delete m_capacityBlock67;
    delete m_capacityBlock100;
    delete m_capacityBlockLow;
}

//
//! Invaldate all components so they will be repainted on next draw()
//
void BatteryCapacityBig::invalidate(void)
{
    m_batteryFrame->invalidate();
    m_capacityBlock33->invalidate();
    m_capacityBlock67->invalidate();
    m_capacityBlock100->invalidate();
    m_capacityBlockLow->invalidate();
}

//
//! Set capacity for the battery symbol.
//! Enable charging symbol if charged
//
void BatteryCapacityBig::setRemCapacity(uint8_t a_remCapacity)
{
    if((m_remCapacity != a_remCapacity) || m_forceDraw)
    {
        m_forceDraw = false;
        
        if(!m_charging)
        {
        
            gselvp(m_viewPort);
            
            
            invalidate();
            
            if(a_remCapacity > 67)
            {
                m_capacityBlock33->setActive(1);
                m_capacityBlock67->setActive(1);
                m_capacityBlock100->setActive(1);
                m_capacityBlock33->draw();
                m_capacityBlock67->draw();
                m_capacityBlock100->draw();
            }
            else if(a_remCapacity > 33)
            {
                m_capacityBlock33->setActive(1);
                m_capacityBlock67->setActive(1);
                m_capacityBlock100->setActive(0);
                m_capacityBlock33->draw();
                m_capacityBlock67->draw();
                m_capacityBlock100->draw();
            }
            else if(a_remCapacity >= BATT_CRITICAL_LIMIT)
            {
                m_capacityBlock33->setActive(1);
                m_capacityBlock67->setActive(0);
                m_capacityBlock100->setActive(0);
                m_capacityBlock33->draw();
                m_capacityBlock67->draw();
                m_capacityBlock100->draw();
            }
            else
            {
                m_capacityBlock33->setActive(0);
                m_capacityBlock67->setActive(0);
                m_capacityBlock100->setActive(0);
                m_capacityBlock33->draw();
                m_capacityBlock67->draw();
                m_capacityBlock100->draw();
            }
            
        }            
    }
    
    m_remCapacity = a_remCapacity;
}

//
//! Draw the symbol
//
void BatteryCapacityBig::draw(void)
{
    if(m_visible || m_forceDraw)
    {
        gselvp(m_viewPort);
        
        m_batteryFrame->invalidate();
        m_batteryFrame->draw();
        
        setRemCapacity(m_remCapacity);
        
    }        
    
}

//
//! Set whether the symbol should be visible or not (does not (re)paint
//! during update calls)
//
void BatteryCapacityBig::visible(bool a_visible)
{
    gselvp(m_viewPort);
    
    invalidate();
    
    if(a_visible && !m_visible)
    {
        m_visible = true;
        m_forceDraw = true;
        draw();
    }
    else if(!a_visible && m_visible)
    {
        m_batteryFrame->erase();
        m_capacityBlock33->erase();
        m_capacityBlock67->erase();
        m_capacityBlock100->erase();
        m_capacityBlockLow->erase();
    }
    
    m_visible = a_visible;
    
}

//
//! Set whether charging indicator should be set at next draw
//
void BatteryCapacityBig::setChargingState(bool a_on)
{
    m_charging = a_on;
        
}

//
//! Tick for animation. 
//! Note: Must be called at interval defined by BLINK_PERIOD
//
void BatteryCapacityBig::tick(void)
{
    if(m_charging)
    {
        if(m_tickTimer)
        {
            m_tickTimer--;
        }
        else
        {
            m_tickTimer = BLINK_PERIOD / m_tickTime;
            
            if(m_blinkState == eBSoff)
            {
                m_blinkState = eBSon;
                
                if(m_visible)
                {
                    gselvp(m_viewPort);
                    
                    if(m_remCapacity > 99)
                    {
                        m_capacityBlock33->setActive(1);
                        m_capacityBlock67->setActive(1);
                        m_capacityBlock100->setActive(1);
                        m_capacityBlock33->draw();
                        m_capacityBlock67->draw();
                        m_capacityBlock100->draw();
                        
                    }
                    else if(m_remCapacity > 67)
                    {
                        m_capacityBlock33->setActive(1);
                        m_capacityBlock67->setActive(1);
                        m_capacityBlock100->setActive(1);
                        m_capacityBlock33->draw();
                        m_capacityBlock67->draw();
                        m_capacityBlock100->draw();
                    }
                    else if(m_remCapacity > 33)
                    {
                        m_capacityBlock33->setActive(1);
                        m_capacityBlock67->setActive(1);
                        m_capacityBlock100->setActive(0);
                        m_capacityBlock33->draw();
                        m_capacityBlock67->draw();
                        m_capacityBlock100->draw();
                    }
                    else
                    {
                        m_capacityBlock33->setActive(1);
                        m_capacityBlock67->setActive(0);
                        m_capacityBlock100->setActive(0);
                        m_capacityBlock33->draw();
                        m_capacityBlock67->draw();
                        m_capacityBlock100->draw();
                    }
                    
                }
            }
            else if(m_blinkState == eBSon)
            {
                m_blinkState = eBSoff;
                if(m_visible)
                {
                    gselvp(m_viewPort);
                    
                    if(m_remCapacity > 99)
                    {
                        m_capacityBlock33->setActive(1);
                        m_capacityBlock67->setActive(1);
                        m_capacityBlock100->setActive(1);
                        m_capacityBlock33->draw();
                        m_capacityBlock67->draw();
                        m_capacityBlock100->draw();
                        
                    }
                    else if(m_remCapacity > 67)
                    {
                        m_capacityBlock33->setActive(1);
                        m_capacityBlock67->setActive(1);
                        m_capacityBlock100->setActive(0);
                        m_capacityBlock33->draw();
                        m_capacityBlock67->draw();
                        m_capacityBlock100->draw();
                    }
                    else if(m_remCapacity > 33)
                    {
                        m_capacityBlock33->setActive(1);
                        m_capacityBlock67->setActive(0);
                        m_capacityBlock100->setActive(0);
                        m_capacityBlock33->draw();
                        m_capacityBlock67->draw();
                        m_capacityBlock100->draw();
                    }
                    else
                    {
                        m_capacityBlock33->setActive(0);
                        m_capacityBlock67->setActive(0);
                        m_capacityBlock100->setActive(0);
                        m_capacityBlock33->draw();
                        m_capacityBlock67->draw();
                        m_capacityBlock100->draw();
                    }
                }
            }
        }
    }
        
}
