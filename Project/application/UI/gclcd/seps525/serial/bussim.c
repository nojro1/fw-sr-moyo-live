/***************************** bussim.c ************************************

   Generic template file for external bus simulator drivers.
   Implemented for serial (SPI) bus simulation using a parallel port.

   The functions below must be implemented as described in the comments.
   Use the proper I/O port instructions given by the actual processor
   architecture, the actual LCD connection method and the I/O access
   capabilities of the target C compiler.

   The portable sgwrby(..) and sgrdby(..) syntax is used for illustration.
   This syntax can be replaced by the compilers native syntax for I/O access.

   The functions in this module is called by the ghwinit.c module when the
   compiler switch GHW_SINGLE_CHIP is defined and GHW_NOHDW is undefined.

   The LCD controller does not support read back of the video memory in
   serial mode. Serial bus mode will therefore require that GBUFFER is
   defined, so the LCD driver functions still can do read-modify-write
   operations on the display content.
   A simrdby() function is not required for the same reason.

   The driver assumes that the address is either 0 or 1.

   Copyright (c) RAMTEX Engineering Aps 2004-2012

****************************************************************************/
#ifndef GHW_NOHDW
#ifdef  GHW_SINGLE_CHIP

/* Define SPI bus bit locations in the output register
   (modify to fit the actual hardware) */
#define  A0    0x01   /* LCD controller register select bit */
#define  SCLK  0x02   /* SPI clock bit */
#define  SDA   0x04   /* SPI data out bit */
#define  CS    0x08   /* LCD controller chip select bit (negative chip select) */
#define  RST   0x10   /* LCD controller reset (negative pulse) */

#include <bussim.h>

/*#include < Port declaration file for your compiler > */
#include <sgio.h> /* or use the SG function syntax (*/

/*
   Simulate a serial bus write operation for a LCD controller via
   an I/O register interface.

   In this impelementation example the output register is assumed to
   support readback so unused register bits are not modified.

   The parallel port is here called P1 and accesed using the SG syntax.
   (Modify to a compiler specific syntax if nessesary to fit the
    actual hardware and compiler)

*/
void simwrby(SGUCHAR adr, SGUCHAR dat)
   {
   register SGUCHAR tmp,msk;
   /* 1. Read state of unused bits and initiate used bit for A0 (D/C) */
   tmp = (sgrdby(P1) & (~(A0|SCLK|SDA|CS)))  | ((adr & 0x1) ? A0 : 0);

   /* 3. Loop while clocking data out, msb first */
   msk=0x80;
   do
      {
      if (((dat & msk) != 0))
         {
         sgwrby(P1,tmp|SDA|SCLK);       /* 4.a Set data high  (other bits is unchanged) */
         sgwrby(P1,tmp|SDA);           /* 5.a Set clock low  (other bits is unchanged) */
         sgwrby(P1,tmp|SDA|SCLK);      /* 6.a Set clock high (other bits is unchanged) */
         }
      else
         {
         sgwrby(P1,tmp|SCLK);          /* 4.b Set data low   (other bits is unchanged)*/
         sgwrby(P1,tmp);               /* 5.b Set clock low  (other bits is unchanged)*/
         sgwrby(P1,tmp|SCLK);          /* 6.b Set clock high (other bits is unchanged)*/
         }
      msk >>= 1;
      }
   while(msk != 0);
   }

/*
   Initialize and reset LCD display.
   Is called before simwrby() is invoked for the first time

   The LCD reset line is toggled here if it connected to a bus port.
   (it may alternatively be hard-wired to the reset signal to the processors
   in the target system).

   The sim_reset() function is invoked automatically via the ghw_io_init() function.
*/
void sim_reset( void )
   {
   register SGUCHAR tmp;
   /* 1. Init data port setup (if required by port architecture) */
   /* 2. Make A0,CS,SDA,SCLK,RST to outputs (if required by port architecture) */
   /* 3. Set SCLK and CS high */
   tmp = sgrdby(P1) | (A0|SDA|SCLK|CS|RST);
   sgwrby(P1, tmp);
   /* 4. Set LCD reset line /RST active low   (if /RST is connected to a port bit) */
   sgwrby(P1, (tmp & ~RST));
   /* 5. Set LCD reset line /RST passive high (if /RST is connected to a port bit) */
   sgwrby(P1, tmp);
   }

#endif /* GHW_SINGLE_CHIP */
#endif /* GHW_NOHDW */


