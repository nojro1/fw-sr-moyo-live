#ifndef BUSSIM_H
#define BUSSIM_H
/***************************** bussim.c ************************************

   Definitions for the OLED bus simulator access functions.

   The definitions below should be modified to reflect the port layout in
   the target system and the given access function implementation.

   The defintions below assume the following hardware bit functionality of
   the "address" byte parameter to the access functions.

      A0 = RS  (Data/command select)

   RAMTEX Engineering Aps 2004-2012

****************************************************************************/

#define GHWWR  0x1
#define GHWRD  0x1
#define GHWSTA 0x0
#define GHWCMD 0x0

/*************** Do not modify the definitions below ********************/
#include <gdisphw.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef GBUFFER
   #error GBUFFER must be defined in serial mode so the library can do read-modify-write operations
#endif
/* Single chip mode -> access via user supplied access driver functions */

void simwrby( SGUCHAR adr, SGUCHAR dat);
#define simrdby( a ) 0xff /* Read is not used in buffered single chip mode */
void sim_reset( void );

#ifdef __cplusplus
}
#endif

#endif /* BUSSIM_H */
