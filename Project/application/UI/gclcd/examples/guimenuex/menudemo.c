/*
   This demo illustrates how a list menu system could be implemented.

   List memu
   ---------
   The "generic" menu functions are located in modules found in the guilib\ directory.
   These functions can create and manage list menus containing a large number of elemenets
   with very little RAM memory consumption.

   The list menu element data is treated like a linear virtual array of elements,
   with element index 0 as the top most element in the menu.
   Each element is drawn via an element format and draw function 
   (in this example is used mnuelem_strp(..) )

   Soft key & lists
   ----------------
   A list can also be embedded in a softkey to get a "toggle key" where a selection
   between multiple options is done via multiple key presses.

   In this demo the soft key will "toggle" the list menu view mode between "cursor and 
   selection mode" and "list view mode" so the behaviour of each mode can be illustrated.

   RAMTEX Engineering ApS 2010
*/
#include <wnmenu.h>
#include <gevents.h>
#include <stdio.h>

/*
   List menu content.
*/
static const char *menu1[] =
   {
   "Line 1",
   "Line 2",
   "Line 3",
   "Line 4",
   "Line 5 gjy",
   "Line 6",
   "Line 7",
   "Line 8",
   "Line 9",
   NULL
   };

/*
   Toggle key content.
*/
static const char *selectkey[] =
   {
   "View scroll",
   "Cursor scroll",
   NULL
   };

static GSTYLE lstmenu_style;
static GSTYLE infomenu_style;

static void init_menu_styles(void)
   {
   wnset_style(&lstmenu_style,GMFONT,0,GNORMAL,FR_INSERT|FR_BUFFER,FR_NONE,0,1,1,1,1);
   wnset_style(&infomenu_style,&narrow10,0,(GVPCLR|GLINECUT|GALIGN_LEFT|GNOSCROLL),FR_W1B1,FR_NONE,0,1,1,1,1);
   }

void main(void)
   {
   MENUDYN md;
   ginit();

   // Init user event interface
   wm_keyintf_init();

   // Set background color
   gsetcolorb(G_RGB_TO_COLOR(20,108,237)); // Make a custom bacground color
   gclrvp();

   init_menu_styles(); // Prepare the "look" descriptors

   // Select style for list menu
   wnsel_style(&lstmenu_style);

   // Create list menu (viewport 1)
   gm_list_init(1,&md,                       // Viewport and Menu descriptor structure
           5, 5,                             // Location of upper left viewport corner
           mnuelem_strp_maxwidth(&menu1[0]), // Fit width to text array content
           4,                                // Max 4 element lines is visible
           0,                                // 0 -> Fit line height to style font
           mnuelem_strp_size(&menu1[0])-1,   // Fit maxindex to number of text array elements
           0,0,                              // Init menu with view and cursor on top element
           (void *) &menu1[0],               // Menu element array list
           (MENUFUNC)&mnuelem_strp);         // Function for drawing content of one menu element


   // Create selection key (viewport 2)
   gm_key_init(2,               // View port
         wnxl(1),wnyb(1)+4,     // Position, align below list menu (outer coordinates)
         gvpxr(1)-gvpxl(1)+1,0, // Width = list menu inner width, height = auto set according to style font
         &selectkey[0]);        // Toggle key text list

   // Create info window (viewport 0) as a framed viewport below selection key
   wnsel_style(&infomenu_style);
   gsetcolorb(G_WHITE);
   gsetcolorf(G_RGB_TO_COLOR(0,100,0)); // Make a custom foreground color
   wnvpcreate(0, gvpxl(1),  gvpyb(2)+9, (GDISPW-1)-gvpxl(1), gvpyb(2)+8+gfgetfh(GMFONT));

   // Demo event loop
   for (;;)
      {
      EVENT event;
      if ((event = wm_keyintf()) != EVENT_KEY_NOEVENT)
         {
         MENUINDEX selection = -1;
         if (event == EVENT_KEY_ESC)
            return; // User break.

         // Process menu events
         if ((event = gm_list_event(1, event, &selection)) == EVENT_SELECTION)
            {
            if (selection >= 0)
               {
               char str[40];
               sprintf(str,"Item at index %u selected", selection);
               gputs(str);
               }
            continue;
            }

         // Process key events
         if (gm_key_event(2, event,&selection) == EVENT_SELECTION)
            {
            if (selection > 0)
               {
               gputs("Using View scroll mode");
               ggetapp_vp(1)->cursorvisible = 0;
               }
            else
               {
               gputs("Using Cursor scroll mode");
               ggetapp_vp(1)->cursorvisible = 1;
               }
            gm_list_update(1);
            continue;
            }
         }
      }

   gm_list_close(1); // Show background restore from buffer
   gexit();
   }


