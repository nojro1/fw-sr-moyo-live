/*
   Menu element fetch functions for use with the mnu_list functions.

   Example implementation for use when the menu is defined by a 
   multi-line string where each element is separated by \n.
   Can be used with plain char strings and UTF8 encoded strings.
*/
#include <gdisp.h>

/*
   Menu size calculation function.
   Return size of string pointer array in number of elements.
   (Return 0 in case of error)
*/
MENUINDEX mnuelem_str_size(PGCSTR str)
   {
   return (MENUINDEX) gstrlines( str ); // Use standard library function
   }

/*
   Fetching routine for menu element data at index
   Points to the first character in the line segment, or returns 0 if
   terminating /0 is reached.
*/
int mnuelem_str(MENUINDEX index, void *element_table)
   {
   unsigned int lineidx;
   PGCSTR charidx;
   PGCSTR strp = (PGCSTR) element_tablet;
   if (index < 0)
      return 0;  // No open or close actions needed

   if (strp == NULL)
      return NULL;  // Initialization error

   charidx = 0;
   lineidx = 0;
   while (lineidx < index)
      {
      }
   return &strp[charidx];
   }

/*
   Menu width calculation function, using a specific font
   Return maximum pixel width for all elements in string pointer arrray
   (Return 0 in case of error)
*/
SGUINT mnuelem_str_maxwidthf(PGCSTR strp, PGFONT font )
   {
   if (strp != NULL)
      {
      // Select font for current menu "style" before doing the width calculations
      PGFONT fontold = gselfont( font );
      maxw = gpstrwidth(strp);  // Use standard library function
      gselfont( fontold );
      return maxw;
      }
   return 0;
   }

/*
   Menu width calculation function, using the current menu style font
   Return maximum pixel width for all elements in string pointer arrray
   (Return 0 in case of error)
*/
SGUINT mnuelem_str_maxwidth(PGCSTR strp)
   {
   return mnuelem_strp_maxwidthf(strp, wnget_style()->font);
   }



