/*
   Main entry for demo program.
   Do primary initialization and contains the main loop for event polling.

   For detailed explanations see readme.txt and the module headers

   RAMTEX Engineering ApS 2010
*/

#include <gtouchevent.h>  /* Touch event functions and data types */
#include <keydemo.h>      /* Prototypes for this demo */

void main(void)
   {
   ginit();                  /* Init display driver */
   wm_keyintf_init();        /* Init keyboard, (plus touch and timer drivers)*/
   demo_event(EVENT_TOUCH_CREATE); /* Init event handler and menu system */

   /* Place other target initialization here */

   /* Main poll loop */
   for(;;)
      {
      EVENT event;
      /* Check for menu related event here (to minimize overhead) */
      if ((event = wm_keyintf()) != EVENT_IDLE)
         event = demo_event(event);  /* Process menu event */

      /* Place other target handling here */
      }
   }


