/************************* keydemo2.c *********************************

   This module create and manage a configuration view containing the
   following elements:
   - A multi selection touch key: key content is a text array
   - Two touch keys with auto repeat: key content are symbols
   - Two framed viewports for showing paramters and graphic curves.
   - How to dynamically update curves (clear old position, set new position)
     with a minimum of screen draw operations

   This demo illustrate
   - How to handle touch key events in a simple manner
   - How to create an adaptive design so the size and placement of drawing
     element can be automatically adapted to the size of a parent viewport.
   - How to deal with repeat events from a key being held down.

   RAMTEX Enginering ApS 2010

**********************************************************************/
#include <gtouchevent.h> // + include gtouchevent.h, gevents.h gdisp.h
#include <keydemo.h>     // Demo specific defines
#include <gkeylook.h>    // Demo key look defines
#include <wnpopup.h>
#include <stdlib.h>      // abs
#include <stdio.h>       // sprintf


#ifndef GFUNC_VP
   #error Error this demo require that GFUNC_VP is defined in gdispcfg.h
#endif

/* Map to global key type definitions (to reuse RAM) (0,1 is reserved) */
#define KEY_ADJUSTTYPE   &touchkeys[2]
#define KEY_ADJUST_UP    &touchkeys[3]
#define KEY_ADJUST_DOWN  &touchkeys[4]

/* Parameters controlling the regulation curve, incl adjustment limits  */

#define PARAM_NUM 4

/* Setup parameters and configuration limits */
static SGINT para_setpoints[PARAM_NUM] = {0, -27, 20, -20};
static SGINT para_setpoints_old[PARAM_NUM];

/* limitation and scaling constants */
static GCODE PARAM_CFG FCODE para_limits[PARAM_NUM] =
   {
   {50, -50, 1000}, // 1
   {50, -50,   20}, // 0.1
   {100, -100, 1}, // 0.001
   {100, -100, 1}  // 0.001
   };

/* Paramter select key text table (must be NULL terminated) */
static GCODE PGSTR FCODE adjust_key[PARAM_NUM+1] =
   {
   #if (GDISPW < 160)
   {"Offset"},
   {"Slope"},
   {"Curve +"},
   {"Curve -"},
   #else
   {" 1. Offset Point "},
   {" 2. Slope "},
   {" 3. Curve + "},
   {" 4. Curve - "},
   #endif
   NULL
   };

static SGUCHAR curvevp,curvevpold,datavp;

/* Key symbols (up/down) */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR b[9];       /* Symbol data, variable length = (cxpix/8+1)*cypix */
   }
 /* array of GSYMBOLs*/
 GCODE FCODE keyup =
   {{7,7},{
   0x10,   // 0001000
   0x38,   // 0011100
   0x38,   // 0011100
   0x7c,   // 0111110
   0x7c,   // 0111110
   0xfe,   // 1111111
   0xfe,   // 1111111
   }};

static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR b[9];       /* Symbol data, variable length = (cxpix/8+1)*cypix */
   }
 /* array of GSYMBOLs*/
 GCODE FCODE keydown =
   {{7,7},{
   0xfe,   // 1111111
   0xfe,   // 1111111
   0x7c,   // 0111110
   0x7c,   // 0111110
   0x38,   // 0011100
   0x38,   // 0011100
   0x10,   // 0001000
   }};

/*
   Calculate regulation curve based on the parameter set
   Algoritm : level = setpoint + x*slope + (x^2)*(x >= 0) ? curve+ : curve-)

   Calculation use signed integers and fixpoint offsets (no floating point lib is needed)
*/
static long calculate_point(SGINT *param, int x)
   {
   long y;
   y =  (long) param[0] * para_limits[0].scale;  // Offset
   y += ((long) x)*((long) param[1])*para_limits[1].scale; // Slope
   y += (((long)(x*x))*((long) ((x >= 0) ? param[2] : param[3]))/10) * para_limits[2].scale; // Curvature
   y += FIXPOINT/2; // 50% rounding
   y /= FIXPOINT;
   return y * -1; /* Screen has 0 on top */
   }

/*
   Update point in y(t) curve, i.e. delete previus point and draw new point
*/
static void update_yt_point(long ynew, long yold, GXT t)
   {
   GYT h;
   h = ggetvph_vp(curvevp);
   /* Detected changes in calculated point value, and remove old curve fragment */
   if ((yold < h) && (yold >= 0))
      {
      if ((t > 0) && (yold != ynew))
         /* Within the viewport area, Remove to old point */
         glineto_vp(curvevpold,t,(GYT)yold);
      else
         gmoveto_vp(curvevpold,t,(GYT)yold);
      }
   else
      /* Outside viewport area, let nett starting point track */
      gmoveto_vp(curvevpold,t,(yold < 0) ? 0 : (GYT)(h-1));

   /* Draw new curve fragment */
   if ((ynew < h) && (ynew >= 0))
      {
      if (t > 0)
         /* Within the viewport area, Draw to new point */
         glineto_vp(curvevp,t,(GYT)ynew);
      else
         gmoveto_vp(curvevp,t,(GYT)ynew);
      }
   else
      {
      /* Outside viewport area, let start point track */
      gmoveto_vp(curvevp,t,(ynew < 0) ? 0 : (GYT)(h-1));
      }
   }

/*
   Update curve viewport.

   This function illustrates the concept of using a parameter copy to
   detect changes in a curve form. This optimizes update speed as the
   graphic screen is only redrawn when needed.

   It also illustrates how "blink free" curve updates can be made without
   the use of a graphic buffer.

   A dual viewport is used for the graphic.
   They are configured equally, except that one viewport is configured for
   the foreground color (draw curve), a then other is configured to the
   background color (delete color).

   If init = 1 draw unconditionally
   If init = 0 update only changed points. Uses a copy of the old parameters to detect changes.
*/
static void draw_curve(unsigned char init, SGINT *para_old, SGINT *para_new)
   {
   int w,xc,x;
   long ynew,yold,yc,h;
   w = (int) ((unsigned int) ggetvpw_vp(curvevp));
   h = (long)((unsigned long) ggetvph_vp(curvevp));
   xc = w/2;
   yc = h/2;

   /* Update curve points */
   for (x = 0; x < w; x++)
      {
      ynew = calculate_point(para_new, x-xc);
      if (init == 0)
         yold = calculate_point(para_old, x-xc);
      else
         yold = ynew;

      update_yt_point(ynew+yc, yold+yc, (GXT) x);
      }

   gcircle_vp(curvevp,(GXT)xc,(GYT)yc,2,0); /* Draw center point */

   /* Update change detection set */
   for (h=0;h<PARAM_NUM;h++)
      para_old[h] = para_new[h];
   }

/*
   Show setup parameter for the current paramter index.

   Update the data viewport with the formatted parameter value

   The setpoint value is updated with the increment parameter,
   followed by a range limitation so the value is always within the
   setpoint_limits.
*/
static void show_parameter( SGINT increment )
   {
   char str[15];
   SGUCHAR oldvp;
   unsigned int index;
   SGINT value;

   /* Get index for currently selected parameter */
   index = gtouch_get_index( KEY_ADJUSTTYPE );

   /* Update parameter, and assure updated value is within limits */
   value = para_setpoints[index];
   value += increment;
   if (value < para_limits[index].min)
      value = para_limits[index].min;
   else
   if (value > para_limits[index].max)
      value = para_limits[index].max;
   para_setpoints[index] = value;

   /* Format parameter as a fixed point value with two decimals */
   sprintf( str, "%c%u.%02u",
        ((value < 0) ? '-' : ' '), // Sign
        (abs((int)value) / 100),   // Value part before decimal point
        (abs((int)value) % 100));  // Value part after decimal point

   /* Output parameter */
   oldvp = gselvp(datavp);
   gputs(str);
   gselvp(oldvp);

   }

/*
   Create the "look" for demo1.
   It consist of these graphic elements;
   -  A framed viewport for showing graphic curves
   -  Parameter select key
   -  A framed viewport for showing selected parameter
   -  Two keys, up/down, for parameter value change

   The look, size and placement is adapted to the current viewport
*/
static void demo_2_create(void)
   {
   GXT vpw,xb,xe;
   GYT vph,yb,ye,keyh;
   SGUCHAR fw;

   gsetupdate( GUPDATE_OFF );

   submenu_preset(); /* Set submenu viewport area */

   // Get basic dynamic sizes
   fw = wnframe_thicknes(FR_INSERT)*2; // Key frame
   vpw = ggetvpw();
   vph = ggetvph();
   xb  = gvpxl(ggetvpnum()); /* Get absolute coordinate for viewport */

   // Height of keys
   keyh = (gfgetfh(KEYFONT) * 3)/2;

   // Add top text, centered
   gselfont(TOPTEXTFONT);
   gsetmode(GALIGN_HCENTER);
   gsetpos(0,ggetfh());
   gputs("Set Regulation Curve");

   // Place select key aligned left, bottom (coordinates relative to viewport)
   gtouch_create(KEY_ADJUSTTYPE,
         0,                vph-keyh,
         gplistw((PGSTR*)&adjust_key[0],KEYFONT)+4, vph-1, /* Adjust key width to max text length*/
         0, GOBJT_STRARR, &adjust_key, 1);

   // Place down key aligned right, same y level (absolute coordinates)
   yb = gtouch_yt(KEY_ADJUSTTYPE);
   ye = gtouch_yb(KEY_ADJUSTTYPE);
   gtouch_create(KEY_ADJUST_DOWN,
         xb+(vpw-1)-keyh, yb,
         xb+(vpw-1)     , ye,
         GTFL_ABSPOS|GTFL_TICK_REPEAT, GOBJT_SYMBOL, (void *) &keydown, 1);

   // Place up-key aligned left for down-key, same y level (absolute coordinates)
   xe = gtouch_xl(KEY_ADJUST_DOWN)-KEYSPACE;
   gtouch_create(KEY_ADJUST_UP,
         xe-(keyh-1), yb,
         xe,          ye,
         GTFL_ABSPOS|GTFL_TICK_REPEAT, GOBJT_SYMBOL, (void *) &keyup, 1);

   // Place curve window aligned above keys, below top text (absolute coordinates)
   vph = gvpyt(ggetvpnum());
   gselvp(curvevp);
   gframedvp( gtouch_xl(KEY_ADJUSTTYPE),           // Align to leftmost key edge
              vph + gfgetfh(TOPTEXTFONT)+KEYSPACE, // Below toptext
              gtouch_xr(KEY_ADJUST_DOWN),          // Align to rightmost key edge
              gtouch_yt(KEY_ADJUSTTYPE)-KEYSPACE,  // Above keys
              FR_INSERT, G_BLUE, G_WHITE,1,0);

   // Copy all curvevp settings to viewport used for curve delete
   // (hint to make curve update/redraw faster)
   gsetupcpy(curvevpold,curvevp);
   gselvp(curvevpold);
   gsetcolorf(ggetcolorb()); // Set line draw color = background color

   // Place parameter viewport aligned between keys (absolute coordinates)
   gselvp(datavp);
   gframedvp( gtouch_xr(KEY_ADJUSTTYPE)+KEYSPACE, yb,
              gtouch_xl(KEY_ADJUST_UP )-KEYSPACE, ye,
              FR_INSERT, G_BLACK, G_WHITE,1,0);

   gsetmode(GALIGN_RIGHT|GALIGN_VCENTER|GLINECUT|GVPCLR);
   gselfont( PARAMETERFONT );

   /* First time draw of curve based on parameter */
   show_parameter(0);
   draw_curve(1, &para_setpoints_old[0], &para_setpoints[0]);

   gsetupdate( GUPDATE_ON );
   }

/*
   Top event handler for demo 2 "look"
*/
EVENT demo_event_2( EVENT event)
   {
   switch (event)
      {
      case EVENT_TOUCH_CREATE:
         {
         // Allocate viewports
         curvevp = wn_get_vpnum();
         curvevpold = wn_get_vpnum();
         datavp = wn_get_vpnum();
         demo_2_create();
         break;
         }
      case EVENT_TOUCH_CLOSE:
         {
         // Release viewport (in reverse order)
         wn_free_vpnum(datavp);
         wn_free_vpnum(curvevpold);
         wn_free_vpnum(curvevp);
         break;
         }
      default:
         {
         EVENT ev;
         // Check Up key
         ev = gtouch_event(KEY_ADJUST_UP, event, touch_x,touch_y,1);
         if ((ev == EVENT_TOUCH_UP) ||
             (ev == EVENT_TOUCH_DOWN_REPEAT))
            {
            /* Increment (slow or fast), stop at max limit */
            show_parameter((ev == EVENT_TOUCH_UP) ? 1 : 5);
            /* Update curve to reflect the changed setting */
            draw_curve(0, &para_setpoints_old[0], &para_setpoints[0]);
            break;
            }

         // Check down key
         ev = gtouch_event(KEY_ADJUST_DOWN, event, touch_x,touch_y,1);
         if ((ev == EVENT_TOUCH_UP) ||
             (ev == EVENT_TOUCH_DOWN_REPEAT))
            {
            /* Decrement (slow or fast), stop at max limit */
            show_parameter((ev == EVENT_TOUCH_UP) ? -1 : -5);
            /* Update curve to reflect the changed setting */
            draw_curve(0, &para_setpoints_old[0], &para_setpoints[0]);
            break;
            }

         // Check paramter select key
         if (gtouch_event(KEY_ADJUSTTYPE, event, touch_x,touch_y,1) == EVENT_TOUCH_UP)
            {
            unsigned int index;
            /* Increment key content parameter index, with wrap around */
            index = gtouch_get_index(KEY_ADJUSTTYPE) + 1;
            if (index >= gtouch_get_numelem(KEY_ADJUSTTYPE))
               index = 0;
            /* Update key content with new data */
            gtouch_key_draw_index( KEY_ADJUSTTYPE, index, 1);
            show_parameter(0);
            break;
            }
         }
      }
   return EVENT_IDLE;
   }

