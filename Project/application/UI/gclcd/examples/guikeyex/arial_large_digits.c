/**************************** arial_large_digits.c ***********************

   arial_large_digits font table and code page structure definitions.
   This file has been auto-generated with the IconEdit / FontEdit tool.

   Copyright(c) RAMTEX 1998-2009

*****************************************************************/
#include <gdisphw.h>

/* Code page entry (one codepage range element) */
static struct
   {
   GCPHEAD chp;
   GCP_RANGE cpr[7];     /* Adjust this index if more codepage segments are added */
   }
GCODE FCODE arial_large_digitscp =
   {
   #include "arial_large_digits.cp" /* Codepage table */
   };

/* Symbol table entry with fixed sized symbols */
static struct
   {
   GSYMHEAD sh;         /* Symbol header */
   SGUCHAR b[75];       /* Symbol data, "variable length" */
   }
GCODE FCODE arial_large_digitssym[17] =
   {
   #include "arial_large_digits.sym" /* Include symbols */
   };

/* Font structure */
GCODE GFONT FCODE arial_large_digits =
   {
   19,       /* averange width */
   25,       /* height */
   sizeof(arial_large_digitssym[0]) - sizeof(GSYMHEAD), /* number of bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL)arial_large_digitssym, /* pointer to array of SYMBOLS */
   17,      /* num symbols */
   (PGCODEPAGE)&arial_large_digitscp
   };

