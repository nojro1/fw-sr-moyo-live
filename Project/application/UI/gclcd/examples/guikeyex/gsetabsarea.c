/****************************** gsetabsarea.c *******************************************

   Validate absolute area coordinates (force within screen area in case of error)
   Convert area coordinates from viewport relative coordinates to absolute coordnates

   reltoabs == 0  The coordinates pointed to is assumed to be absolute coordinates,
                  no conversion is needed.

   reltoabs != 0  The coordinates pointed to is assumed to be viewport relative
                  coordinates, convert to absolute coordinates

   2010 RAMTEX Engineering ApS

******************************************************************************************/

#include <gdisp.h>
#include <gtouchevent.h>

void gsetabsarea( GXT *xb, GYT *yb, GXT *xe, GYT *ye, SGUCHAR reltoabs)
   {
   SGUINT _xb,_xe,_yb,_ye;
   /* Use temp variables to check touch area in order to prevent
      viewport overflow or GXT,GYT saturation */
   _xb = (SGUINT) ((xb != NULL) ? *xb : 0);
   _yb = (SGUINT) ((yb != NULL) ? *yb : 0);
   _xe = (SGUINT) ((xe != NULL) ? *xe : GDISPW-1);
   _ye = (SGUINT) ((ye != NULL) ? *ye : GDISPH-1);

   /* Force end >= begin */
   if (_xe < _xb) _xb = _xe;
   if (_ye < _yb) _yb = _ye;

   if (reltoabs)
      {
      /* Is viewport relative parameters, convert to abosolute */
      SGUCHAR vp = ggetvpnum();
      _xb += (SGUINT) gvpxl(vp);
      _yb += (SGUINT) gvpyt(vp);
      _xe += (SGUINT) gvpxl(vp);
      _ye += (SGUINT) gvpyt(vp);
      }

   /* Force inside valid screen areas */
   if (_xb >= GDISPW) _xb = GDISPW-1;
   if (_xe >= GDISPW) _xe = GDISPW-1;
   if (_yb >= GDISPW) _yb = GDISPH-1;
   if (_ye >= GDISPW) _ye = GDISPH-1;

   /* Area is valid, write (converted) coordinates back */
   if (xb != NULL) *xb = (GXT) _xb;
   if (yb != NULL) *yb = (GYT) _yb;
   if (xe != NULL) *xe = (GXT) _xe;
   if (ye != NULL) *ye = (GYT) _ye;
   }


