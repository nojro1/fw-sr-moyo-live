/************************* keydemo.h **************************************

   Demo specific definitions and prototypes

   This header file defines parameters controlling the "look" of the
   touch keys in the demo

   2010 RAMTEX Engineering ApS

***************************************************************************/

#ifndef GKEYDEMO_H
#define GKEYDEMO_H

#include <gdisp.h>
#include <gevents.h>

#ifdef __cplusplus
    extern "C" {
#endif

/* Generic content draw function prototype */
typedef void (*PDRAWFUNC)(void *);

/* keyintf.c */
EVENT wm_keyintf(void);     /* Test for key or touch event */
void wm_keyintf_init(void);
extern GXT touch_x;
extern GYT touch_y;

/* A single array of touch screen elements is used (to reuse RAM) */
#define  TOUCH_NUMKEYS 5
/* Global key data storage (reused by different menus) */
extern GTE touchkeys[TOUCH_NUMKEYS];

#define  KEYSPACE  4
#define  KEYWIDTH  (GDISPW-KEYSPACE*5)/4

/* Define fonts used by demo */
#if ((GDISPW < 160) || (GDISPH < 160))
  #define  LARGENUMFONT  &ariel18
  #define  PARAMETERFONT &cp8859_9
  #define  TOPTEXTFONT   &cp8859_9
#else
   extern GCODE GFONT FCODE arial_large_digits;
   #define  LARGENUMFONT  &arial_large_digits
   #define  PARAMETERFONT &cp8859_14
   #define  TOPTEXTFONT   &cp8859_14
#endif
/* KEYFONT is defined in gkeylook.h */

/* Types for parameter configuration data */
typedef struct
   {
   SGINT max;    /* Parameter maximum limit */
   SGINT min;    /* Parameter minimum limit */
   SGLONG scale; /* Parameter scale factor * FIXPOINT */
   } PARAM_CFG;

#define FIXPOINT  1000  /* The fixed point offset is 3 digits Ex 0.001 */


/* Preset area area available for sub menus (so area used for primary keys is reserved) */
void submenu_preset(void);

EVENT demo_event(EVENT event);    /* Top event handler */
EVENT demo_event_1(EVENT event, char adjustview); /* Sub screen event handler */
EVENT demo_event_2(EVENT event);  /* Sub screen event handler */

#ifdef __cplusplus
   }
#endif
#endif


