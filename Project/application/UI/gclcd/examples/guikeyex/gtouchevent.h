/***************************************************************************

   Function prototypes and data types for GTE touch area event handling

   This module defines a set of generic event handling methods which are
   platform and hardware independent.

   The implementation of these methods are done in gtouchevent.c and
   keylook.c

   2010 RAMTEX Engineering ApS

***************************************************************************/
#ifndef GTOUCH_EVENT_H
#define GTOUCH_EVENT_H
#include <gdisp.h>    /* Display driver types and functions */
#include <gkeylook.h> /* Key content object definitions */
#include <gevents.h>  /* Event name definitions */

#ifdef __cplusplus
    extern "C" {
#endif


typedef void (*PGTR_DRAWFUNC)(void *, EVENT);

typedef struct
   {
   GXT xb;              /* Touch area (key or menu inner area) upper left corner (absolute) */
   GYT yb;
   GXT xe;
   GYT ye;
   SGUCHAR flags;                 /* Flags, Has focus, state toggle, repeat count */
   SGUCHAR tickcnt;               /* Tick count for event repeat */
   SGINT state;                   /* Current (configured) state for area */
   GCOLOR color_close;            /* Area delete color (=viewport background color at key creation)*/
   PGTR_DRAWFUNC draweventfunc;   /* Function handling the (key) event "look" */
   GTE_DRAWDESC  drawdesc;        /* Data used by "look" drawing functions */
   } GTE, *PGTE;

/* GTE flag parametes */
#define GTFL_NONE        0x00
#define GTFL_FOCUS       0x01  /* Key has (initial) focus */
#define GTFL_TOGGLE      0x02  /* Key is state toggle key (state 0->1->0 toggling) */
#define GTFL_TICK_REPEAT 0x04  /* Activate key repeat */
#define GTFL_ABSPOS      0x08  /* Setup coordinates is absolute screen positions */

/* GTE tickcnt limits for repeat count */
#define  GTE_TICKS_START    10
#define  GTE_TICKS_REPEART   3

/**************************************************************************
  gtouchevent.c
***************************************************************************/

/* Init a touch area structure */
void gtouch_create(PGTE eventarea,
      GXT xb, GYT yb, GXT xe, GYT ye,  /* Active area */
      SGUCHAR flags,                   /* Config flags */
      GOBJTYPE objtype,                /* Object area content type (used by draw function) */
      void *obj,                       /* Pointer object area content (used by draw function) */
      SGUCHAR dodraw);                 /* Draw object at once */

/* Check / process a touch state change, return the resulting event */
EVENT gtouch_event(PGTE eventarea, EVENT event, GXT xt, GYT yt, SGUCHAR dodraw);
/* Get / set touch state parameter */
SGUINT gtouch_getstate(PGTE eventarea);
void gtouch_setstate(PGTE eventarea, SGUINT state);

/**************************************************************************
   gsetabsarea.c
   Convert area coordinates from viewport relative coordinates to absolute coordnates
   Validate absolute area coordinates (force within screen area in case of error)
***************************************************************************/
void gsetabsarea( GXT *xb, GYT *yb, GXT *xe, GYT *ye, SGUCHAR reltoabs );

/**************************************************************************

   gkeylook.c
   Low-level key draw functions
   These functions implement and controls the key "look"

***************************************************************************/
void gtouch_key_draw( PGTE eventarea, EVENT event);

/* Init the "look" for a key, called from gtouch_create(..) */
void gtouch_look_init( PGTE eventarea, GOBJTYPE objtype, void *pobj, SGUCHAR dodraw );
/* Refresh the key content (after change in content setting  */
void gtouch_key_draw_index( PGTE eventarea, unsigned int index, SGUCHAR dodraw );
/* Return current index for array drawing elements */
unsigned int gtouch_get_index( PGTE eventarea);
/* Return number of elements for array drawing elements */
unsigned int gtouch_get_numelem( PGTE eventarea);

/* Change foreground or background colors for touch event area */
void gtouch_setcolorf( PGTE eventarea, GCOLOR foreground_color, SGUCHAR dodraw );
void gtouch_setcolorb( PGTE eventarea, GCOLOR background_color, SGUCHAR dodraw );

/**********************************************************************

   Size rapport macros (for dynamic position alignment)
   These macros return absolute coordinates

***********************************************************************/

/* Coordinate for touch area right edge */
#define  gtouch_xr(eventarea) (((eventarea) == NULL) ? 0 : (eventarea)->xe)
/* Coordinate for touch area left edge */
#define  gtouch_xl(eventarea) (((eventarea) == NULL) ? 0 : (eventarea)->xb)
/* Coordinate for touch area top edge */
#define  gtouch_yt(eventarea) (((eventarea) == NULL) ? 0 : (eventarea)->yb)
/* Coordinate for touch area bottom edge */
#define  gtouch_yb(eventarea) (((eventarea) == NULL) ? 0 : (eventarea)->ye)
/* Touch area width */
#define  gtouch_w(eventarea)  (((eventarea) == NULL) ? 0 : ((eventarea)->xe - (eventarea)->xb + 1))
/* Touch area height */
#define  gtouch_h(eventarea)  (((eventarea) == NULL) ? 0 : ((eventarea)->ye - (eventarea)->yb + 1))

#ifdef __cplusplus
 }
#endif

#endif

