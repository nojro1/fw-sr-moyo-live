/************************* keydemo1.c *********************************

   This module create and manage two views
   A) Show current setpoint value
   B) Show setpoint value + two touch keys Up and Down

   This demo illustrate
   - How to handle touch key events in a simple manner
   - How to create an adaptive design so the size and placement of drawing
     element can be automatically adapted to the size of a parent viewport.
   - How to deal with repeat events from a key being held down.

   RAMTEX Enginering ApS 2010

**********************************************************************/
#include <gtouchevent.h> // + include gtouchevent.h, gevents.h gdisp.h
#include <keydemo.h>     // Demo specific defines
#include <stdlib.h>      // abs
#include <stdio.h>       // sprintf
#include <wnpopup.h>

/* Map local keys to global GTE instance to reuse RAM (0,1 is reserved) */
#define KEY_ADJUST_UP    &touchkeys[2]
#define KEY_ADJUST_DOWN  &touchkeys[3]

static SGUCHAR datavp; /* viewport use for dynamic output */

/* Setpoint parameter and configuration limits */
static SGINT setpoint = 215; /* 21.5 */
static GCODE PARAM_CFG FCODE setpoint_limits = {600, -100, 1000};
#define DEGREE_SYM "\xb0"

/*
   Show setup parameter.

   Update the data viewport with the formatted parameter value

   The setpoint value is updated with the increment parameter,
   followed by a range limitation so the value is always within the
   setpoint_limits.

   The parameter value is colored in accordance with the level.

*/
static void show_parameter( SGINT increment )
   {
   char str[15];
   SGUCHAR oldvp;
   GCOLOR col;

   setpoint += increment;  /* Update setpoint*/

   if (setpoint < setpoint_limits.min)
      setpoint = setpoint_limits.min;
   else
   if (setpoint > setpoint_limits.max)
      setpoint = setpoint_limits.max;

   /* Format parameter as a fixed point value with one decimal */
   sprintf( str, "%c%u.%u " DEGREE_SYM "C", ((setpoint < 0) ? '-' : ' '),
        (abs(setpoint) / 10), (abs(setpoint) % 10));

   /* Select foreground color based on setpoint value */
   if (setpoint >= 500)
      col = G_RED;
   else
   if (setpoint >= 400)
      col = G_ORANGE;
   else
   if (setpoint > 0)
      col = G_BLACK;
   else
      col = G_BLUE;

   /* Output parameter string to viewport
      The datavp viewport is already preconfigured for auto alignment
      and using auto clear so here we just output data. */
   oldvp = gselvp(datavp);
   gsetcolorf(col);
   gputs(str);
   gselvp(oldvp);
   }

/*
   Create demo 1 "view"
   Two similar views are handled here
      a) Show the setpoint parameter alone
      b) Show the setpont parameter + Up/down adjustment soft-keys
*/
static void demo_1_create( char adjustview)
   {
   GXT vpw,xb,fw;
   GYT vph,yb,fh;
   SGUCHAR vp;
   GCOLOR colorback;

   gsetupdate( GUPDATE_OFF );

   submenu_preset(); /* Set submenu viewport area */

   /* Get basic size info for dynamic auto scaling */
   vpw = ggetvpw();
   vph = ggetvph();

   // Add top text, centered
   gselfont(TOPTEXTFONT);
   gsetpos(0,ggetfh());
   gsetmode(GALIGN_HCENTER);
   if (adjustview)
      {
      gputs("Set Point Adjustment");

      /* Add adjustment keys */
      // Place Up key aligned left, bottom (coordinates relative to viewport)
      gtouch_create(KEY_ADJUST_UP,
            0,                vph-(gfgetfh(KEYFONT) * 3)/2,
            (vpw-KEYSPACE)/2, vph-1,
            GTFL_TICK_REPEAT, GOBJT_STR, "Up", 1);

      // Place Down key aligned right of up key (absolute coordinates)
      gtouch_create(KEY_ADJUST_DOWN,
          gtouch_xr(KEY_ADJUST_UP)+KEYSPACE,   gtouch_yt(KEY_ADJUST_UP),
          gvpxr(ggetvpnum()), gtouch_yb(KEY_ADJUST_UP),
          GTFL_ABSPOS|GTFL_TICK_REPEAT, GOBJT_STR, "Down", 1);
      }
   else
      {
      gputs("Set Point");
      }

   // Place setpoint parameter viewport centered in parent viewport
   // Fit viewport size to match the font and text length
   // Let background color be the same as the parent viewport

   /* Get basic size info for dynamic auto scaling */
   colorback = ggetcolorb();       /* Get current background color */
   vp = ggetvpnum();               /* Get parent viewport */

   /* Configure parameter viewport */
   gselvp(datavp);
   gresetvp();
   gsetcolorb(colorback);          /* Let background color match parent */
   gsetmode(GALIGN_HCENTER|GALIGN_VCENTER|GLINECUT|GVPCLR); /* Auto alignment and clear */
   gselfont(LARGENUMFONT);         /* Use an application specific font */

   /* Place parameter viewport at center of parent viewport */
   fh = ggetfh();                  /* Get font size*/
   fw = ggetfw();
   xb = (vpw-10*fw)/2 + gvpxl(vp); /* Absolute coordinates for parameter viewport */
   yb = (vph-fh  )/2 + gvpxl(vp);
   gsetvp(xb,yb,xb+10*fw,yb+fh);

   gselvp(vp);
   show_parameter(0); /* Format and draw setpoint paramter */

   gsetupdate( GUPDATE_ON );
   }

/*
   Common top event handler for the demo_1a and demo_1b views
*/
EVENT demo_event_1( EVENT event, char adjustview)
   {
   switch (event)
      {
      case EVENT_TOUCH_CREATE:
         {
         datavp = wn_get_vpnum();  // Allocate viewport
         demo_1_create( adjustview );
         break;
         }
      case EVENT_TOUCH_CLOSE:
         {
         wn_free_vpnum(datavp);   // Release viewport
         break;
         }
      default:
         {
         if (adjustview)
            {
            // Check touch areas for an action
            EVENT ev;
            ev = gtouch_event(KEY_ADJUST_UP, event, touch_x,touch_y,1);
            if ((ev == EVENT_TOUCH_UP) ||
                (ev == EVENT_TOUCH_DOWN_REPEAT))
               {
               /* Increment (slow or fast), stop at max limit */
               show_parameter((ev == EVENT_TOUCH_UP) ? 1 : 5);
               break;
               }

            ev = gtouch_event(KEY_ADJUST_DOWN, event, touch_x,touch_y,1);
            if ((ev == EVENT_TOUCH_UP) ||
                (ev == EVENT_TOUCH_DOWN_REPEAT))
               {
               /* Decrement (slow or fast), stop at max limit */
               show_parameter((ev == EVENT_TOUCH_UP) ? -1 : -5);
               break;
               }
            }
         }
      }
   return EVENT_IDLE;
   }




