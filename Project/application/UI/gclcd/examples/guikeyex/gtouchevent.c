/***************************************************************************

  Touch area event handler.
  -------------------------
  These touch screen events are processed in this module:

  Input events used:
     EVENT_TOUCH_DOWN,         Basic Touch down event
     EVENT_TOUCH_UP,           Basic Touch up event

     EVENT_TOUCH_CREATE,       Create (first time draw)
     EVENT_TOUCH_UPDATE,       Explicit update of the content of an area
     EVENT_TOUCH_CLOSE,        Close  (final event)
     EVENT_TICK,

  Return event responce:
     An EVENT_IDLE is returned if the input event is don't care or consumed.
     One of these event is returned if the event should be acted upon by by
     the application level
     EVENT_TOUCH_UP,           Touch up event handled
     EVENT_TOUCH_DOWN,         Touch down event handled
     EVENT_TOUCH_DOWN_REPEAT,  Touch down (timing) repeat (responce on EVENT_TICK)
     EVENT_TOUCH_FOCUSLOST,    Focus lost event: Area had focus and detected touch down
                               outside area or drag to outside.

  This module is independent of the actual touch area "look".
  For touch keys the actual "touch key look" is created in gkeylook.c

  2010 RAMTEX Engineering ApS

***************************************************************************/
#include <gtouchevent.h>

/*
   Init a touch area structure
   The current viewport is assumed to be the "drawing canvas" for any
   touch (key) areas

   The area is updated at once if dodraw != 0
*/
void gtouch_create(PGTE eventarea,     /* Touch area descriptor */
      GXT xb, GYT yb, GXT xe, GYT ye,  /* Active area */
      SGUCHAR flags,                   /* Config flags */
      GOBJTYPE objtype,                /* Object area content type (used by draw function) */
      void *obj,                       /* Pointer object area content (used by draw function) */
      SGUCHAR dodraw)                  /* Draw object at once */
   {
   /* Force valid coordinates, convert viewport relative coordinates to abs coordingates */
   gsetabsarea(&xb,&yb,&xe,&ye,((flags & GTFL_ABSPOS)==0));
   eventarea->xb = xb;
   eventarea->yb = yb;
   eventarea->xe = xe;
   eventarea->ye = ye;
   eventarea->flags = flags & (GTFL_FOCUS | GTFL_TOGGLE | GTFL_TICK_REPEAT);
   eventarea->tickcnt = 0;
   eventarea->state = 0;
   eventarea->color_close = ggetcolorb(); /* Save canvas background color (used as "key close" color) */
   eventarea->draweventfunc = NULL;  /* Force init by gtouch_look_init(..) */
   gtouch_look_init( eventarea, objtype, obj, 0);
   if (dodraw && (eventarea->draweventfunc != NULL))
      eventarea->draweventfunc(eventarea, EVENT_TOUCH_CREATE);
   }

/*
   Filter and process a touch state changes, return the resulting event
   (The operation is independent of the current viewport settings)

   The related touch (key) area is updated at once if dodraw != 0
*/
EVENT gtouch_event(PGTE eventarea, EVENT event, GXT xt, GYT yt, SGUCHAR dodraw)
   {
   if (eventarea == NULL)
      return EVENT_IDLE;   /* Illegal parameter, Not defined */

   if ((event == EVENT_TOUCH_UP) || (event == EVENT_TOUCH_DOWN) || (event == EVENT_TOUCH_DRAG))
      { /* Evaluate touch event for this touch area */
      if ((xt >= eventarea->xb) && (xt <= eventarea->xe) &&
          (yt >= eventarea->yb) && (yt <= eventarea->ye))
         {
         /* Is inside area */
         if ((event == EVENT_TOUCH_DOWN) && !(eventarea->flags & GTFL_FOCUS))
            {
            /* Draw touch down look */
            if (dodraw && (eventarea->draweventfunc != NULL))
               (eventarea->draweventfunc)((void *)eventarea,EVENT_TOUCH_DOWN);
            /* Update for new state */
            eventarea->tickcnt = 0;
            eventarea->flags |= GTFL_FOCUS;
            /* If repeat key we signal at once, else we wait for touch up */
            return ((eventarea->flags & GTFL_TICK_REPEAT) ? EVENT_TOUCH_DOWN : EVENT_IDLE);
            }
         else
         if ((event == EVENT_TOUCH_UP) && (eventarea->flags & GTFL_FOCUS))
            {
            if (eventarea->flags & GTFL_TOGGLE)
               { /* Is a state auto toogle key */
               eventarea->state = (eventarea->state == 0) ? 1 : 0;
               }
            if (dodraw && (eventarea->draweventfunc != NULL))
               /* Reset object drawing to "non selected look"*/
               (eventarea->draweventfunc)(eventarea,EVENT_TOUCH_UP);
            /* Update for new state */
            eventarea->flags &= ~GTFL_FOCUS;
            return EVENT_TOUCH_UP;
            }
         }
      else
         {
         /* Touch up/down event outside area, or drag to outside area */
         if (eventarea->flags & GTFL_FOCUS)
            {
            if (dodraw && (eventarea->draweventfunc != NULL))
               /* Reset object drawing to "non selected look"*/
               (eventarea->draweventfunc)(eventarea,EVENT_TOUCH_FOCUSLOST);
            eventarea->flags &= ~GTFL_FOCUS;
            return EVENT_TOUCH_FOCUSLOST;
            }
         }
      }
   else
   if (event == EVENT_TICK)
      { /* Evaluate tick event for this touch area */
      if ((eventarea->flags & (GTFL_FOCUS|GTFL_TICK_REPEAT))
           == (GTFL_FOCUS|GTFL_TICK_REPEAT)) /* Has focus & Tick is enabled*/
         {
         /* Process event tick */
         eventarea->tickcnt++;
         if ((eventarea->tickcnt >= (GTE_TICKS_START + GTE_TICKS_REPEART)) ||
             (eventarea->tickcnt ==  GTE_TICKS_START))
            {
            eventarea->tickcnt = GTE_TICKS_START; /* restart tick repeat */
            return EVENT_TOUCH_DOWN_REPEAT;
            }
         }
      }
   else
   if (event == EVENT_TOUCH_UPDATE)
      {
      /* Recreate key look */
      if (dodraw && (eventarea->draweventfunc != NULL))
         (eventarea->draweventfunc)(eventarea,EVENT_TOUCH_UPDATE);
      }
   else
   if (event == EVENT_TOUCH_CLOSE)
      {
      /* Clear object area to "removed" look */
      if (dodraw && (eventarea->draweventfunc != NULL))
         (eventarea->draweventfunc)(eventarea,EVENT_TOUCH_CLOSE);
      eventarea->state = 0;
      }
   return EVENT_IDLE; /* No action needed or not inside area */
   }

/*
   Set and get key area state.
   The meaning and purpose of key "state" is application dependent

   The state parameter is just a storage location which is set to 0 upon
   creation and removal of the touch ares.

   If the key is defined as a state "toggle" key via the creation
   flag GTFL_TOGGLE then gtouch_setstate(..) should normally not be used.
*/
/*
   State key area state parameter
*/
void gtouch_setstate(PGTE eventarea, SGUINT state)
   {
   if (eventarea != NULL)
      eventarea->state = state;
   }

/*
   Get key area state parameter
*/
SGUINT gtouch_getstate(PGTE eventarea)
   {
   if (eventarea != NULL)
      return eventarea->state;
   return 0;
   }

