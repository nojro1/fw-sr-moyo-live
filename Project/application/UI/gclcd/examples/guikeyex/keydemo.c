/************************* keydemo.c *********************************

   This module contains the main event scheduler for the key demo.
   Handles the step between the views and the dispatching of events
   to the active subview.

   The demo consist of 4 views:
   -  Start up view and bottom keys with On/Off and Next keys (this module)
   -  A Show setpoint view (keydemo1.c)
   -  A Show and adjust setpoint view (keydemo1.c)
   -  A regulation setup curve view (keydemo2.c)

   RAMTEX Enginering ApS 2010

**********************************************************************/
#include <gtouchevent.h> // + include gtouchevent.h, gevents.h gdisp.h
#include <keydemo.h>     // Demo specific defines
#include <wnpopup.h>

// Top Menu states
typedef enum
   {
   MNU_INIT,
   MNU_DEMO_ON,
   MNU_DEMO_1a,
   MNU_DEMO_1b,
   MNU_DEMO_2,
   MNU_LAST,
   } MNU_STATE;

#define NUM_DEMOS (MNU_LAST-MNU_DEMO_1a)
static MNU_STATE mnu_state;

/* A single array of touch screen elements is used for all demos */
GTE touchkeys[TOUCH_NUMKEYS];

/* Map to global key type definitions (to reuse RAM) */
#define  KEYONOFF    &touchkeys[0]  /* Global ON/OFF key */
#define  KEYNEXT     &touchkeys[1]  /* Global Next demo key */
#define  BACKGROUND_COLOR  G_LLIGHTGREY


const char *inittext = "Touch key demo";

/* Preset area used by sub menus (so area used for primary keys is reserved) */
void submenu_preset(void)
   {
   GXT xe;
   GYT ye;
   xe = GDISPW-1;
   ye = gtouch_yt( KEYONOFF )-1; // Pixel line above primary keys
   gselvp(0);
   gresetvp();
   gsetcolorb( BACKGROUND_COLOR );
   gsetcolorf( G_BLACK );
   gsetvp( 0, 0, xe, ye );
   gclrvp();
   // Add decoration
   wnframe( KEYSPACE,  KEYSPACE, xe-KEYSPACE, ye-KEYSPACE, FR_HILL_LINE);
   // Sub window located inside decoration
   gsetvp( KEYSPACE*2, KEYSPACE*2, xe-KEYSPACE*2, ye-KEYSPACE*2);
   }

/*
   Handle "On" "Off" key events
   Upon state changes the whole screen is wiped out
   (any sub screen handlers only have to clear up )
*/
static EVENT demo_event_0_onoff( EVENT event)
   {
   if (event == EVENT_TOUCH_CREATE)
      {
      // Create On-Off key, delay drawing until text and colors is assigned
      gtouch_create(KEYONOFF,
         (GXT)KEYSPACE,           (GYT)((SGUINT)GDISPH-(KEYSPACE+(3*gfgetfh(KEYFONT))/2)),
         (GXT)(KEYSPACE+KEYWIDTH),(GYT)((SGUINT)GDISPH-KEYSPACE),
         GTFL_ABSPOS|GTFL_TOGGLE, GOBJT_EMPTY, 0, 0);
      event = EVENT_TOUCH_UP;
      }
   else
      // Check for touch event
      event = gtouch_event(KEYONOFF, event, touch_x,touch_y,1);

   if (event == EVENT_TOUCH_UP)
      {
      // Process "look" handling for On-Off (and create) events
      SGUINT state;
      #ifdef GBUFFER
      GUPDATE update;
      #endif
      // Set key color and text in accordance with key state
      state = gtouch_getstate(KEYONOFF);
      // Assign key text and colors in accordance with state
      gtouch_look_init(KEYONOFF, GOBJT_STR, (state != 0) ? "Off" : "On",0);
      gtouch_setcolorf(KEYONOFF, (state != 0) ? G_YELLOW : G_RED, 0);
      // Set full screen background / foreground color
      gsetcolorb((state != 0) ? BACKGROUND_COLOR : G_BLACK);
      gsetcolorf((state != 0) ? G_BLACK : G_WHITE);

      // Fast full redraw on new background (fast clear of any previous "look")
      #ifdef GBUFFER
      update = gsetupdate(GUPDATE_OFF);
      #endif
      gresetvp();
      gclrvp();
      if (state == 0)
         {
         gsetmode(GALIGN_HCENTER|GALIGN_TOP);
         gselfont(TOPTEXTFONT);
         gputs(inittext);
         }
      gtouch_key_draw(KEYONOFF, EVENT_TOUCH_CREATE);
      #ifdef GBUFFER
      gsetupdate(update);
      #endif
      return EVENT_TOUCH_UP;
      }
   return EVENT_IDLE;
   }

/*
   Handle "Next" key events

   Preset current viewport to the area above the primary heys
*/
static EVENT demo_event_0_next( EVENT event)
   {
   if (event == EVENT_TOUCH_CREATE)
      {
      // Create NEXT key on top of a already cleared background
      gtouch_create(KEYNEXT,
         (GXT)(KEYSPACE*2+KEYWIDTH),  (GYT)((SGUINT)GDISPH-(KEYSPACE+(3*gfgetfh(KEYFONT))/2)),
         (GXT)((KEYSPACE+KEYWIDTH)*2),(GYT)((SGUINT)GDISPH-KEYSPACE),
         GTFL_ABSPOS, GOBJT_STR, "Next", 1);
      }
   else
      {
      if (gtouch_event(KEYNEXT, event, touch_x,touch_y,1) == EVENT_TOUCH_UP)
         {
         // Increment next key state with wrap around
         SGUINT state = gtouch_getstate(KEYNEXT);
         gtouch_setstate(KEYNEXT, (state == NUM_DEMOS) ? 0 : state+1);
         return event;
         }
      }
   return EVENT_IDLE;
   }

/*
   Top event handler.
   -  Manages initial demo screen,
   -  Processing state changes related to On / Off key events
   -  Processing state changes related to Next key events
   -  Destibution of events to the menu screen look which is active;
*/
EVENT demo_event(EVENT event)
   {
   if (event == EVENT_TOUCH_CREATE)
      mnu_state = MNU_INIT;

   switch (mnu_state)
      {
      case MNU_INIT:
         {
         /* Do first time initialization. Is only activated once after startup */
         demo_event_0_onoff( EVENT_TOUCH_CREATE );
         mnu_state = MNU_DEMO_ON;
         break;
         }
      case MNU_DEMO_ON:
         {
         // Check if On key is pressed
         if (demo_event_0_onoff(event) == EVENT_TOUCH_UP)
            {
            // On key is pressed
            demo_event_0_next( EVENT_TOUCH_CREATE );
            submenu_preset(); // Protect primary keys
            demo_event_1( EVENT_TOUCH_CREATE, 0 ); /* Create look for demo 1 */
            mnu_state = MNU_DEMO_1a;
            }
         break;
         }
      case MNU_DEMO_1a:
      case MNU_DEMO_1b:
         {
         // First filter for global keys

         // Check if Off key is pressed
         if (demo_event_0_onoff(event) == EVENT_TOUCH_UP)
            {
            // Off key is pressed
            demo_event_1( EVENT_TOUCH_CLOSE, (mnu_state == MNU_DEMO_1b)); // Close existing demo
            mnu_state = MNU_DEMO_ON;     // Change menu state
            break;
            }
         // Check if Next key is pressed
         if (demo_event_0_next(event) == EVENT_TOUCH_UP)
            {
            // Select demo 2
            if (mnu_state == MNU_DEMO_1b)
               {
               demo_event_1( EVENT_TOUCH_CLOSE, 1); // Close existing demo
               demo_event_2( EVENT_TOUCH_CREATE );  // Open new demo
               mnu_state = MNU_DEMO_2;
               }
            else
               {
               demo_event_1( EVENT_TOUCH_CLOSE,  0); // Close existing demo
               demo_event_1( EVENT_TOUCH_CREATE, 1); // Open new demo
               mnu_state = MNU_DEMO_1b;
               }
            break;
            }

         // Filter for demo "look" specific events
         demo_event_1( event, (mnu_state == MNU_DEMO_1b)); /* Process event 1 */
         break;
         }

      case MNU_DEMO_2:
         {
         // First filter for global keys

         // Check if Off key is pressed
         if (demo_event_0_onoff(event) == EVENT_TOUCH_UP)
            {
            // Off key is pressed
            demo_event_2( EVENT_TOUCH_CLOSE ); // Close existing demo
            mnu_state = MNU_DEMO_ON;     // Change menu state
            break;
            }

         // Check if Next key is pressed
         if (demo_event_0_next(event) == EVENT_TOUCH_UP)
            {
            demo_event_2( EVENT_TOUCH_CLOSE );  // Close existing demo
            // Wrap back to demo 1
            demo_event_1( EVENT_TOUCH_CREATE, 0 );
            mnu_state = MNU_DEMO_1a;
            break;
            }
         // Filter for demo "look" specific events
         demo_event_2( event ); /* Process event 2 */
         break;
         }
      default:
         break;
      }

   return EVENT_IDLE; // This is top event so all event types has been processed
   }

