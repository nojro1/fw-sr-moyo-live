Touch Key GUI library demo
--------------------------

The project in this directory demonstrates how to make and use touch sensitive
soft keys (touch keys), and how to create a touch key "look" on top of the
generic GUI driver library features.

The project example has focus on these topics:
-  How to implement the basic "look" and dynamic behavior for
   using soft keys in connection with a touch screen
-  How to create an event system based on touch screen and timer
   tick events.
-  How to implement typical "views" for use of touch screen input
   and how to implement soft-keys content from different data types
   - Single select keys using strings or symbols
   - Multi select keys using text arrays or symbol fonts.

The application code in this demo consist primarily of these modules:

   key_main.c  // Main loop (assuming use of a polled system)
   keydemo.c   // Top-level menu views, soft keys for navigation handling
   keydemo1.c  // Simple views showing setpoint adjustment via soft keys
   keydemo2.c  // A more complex view showing more advanced soft key handling
   keyintf.c   // Soft key interface. Collect events from the (simulated)
               // touch screen and timer hardware.

   gtouchevent.c // Generic touch screen event handler. Controls the
                 // dynamic characteristics of a touch sensitive area, i.e.
                 // the dynamics controlling the "look and feel" of the touch keys
   gkeylook.c    // Generic functions for creating the touch key "look"

For more implementation details on the demo and event system design please
read the module headers

gdispcfg.h configuration
------------------------
Changes from default gdispcfg.h.

  #define GHW_PALETTE_BACKGROUND  G_BLACK
  #define GHW_PALETTE_FOREGROUND  G_WHITE

else just use the library defaults.

The suggested change is just to let the demo start with the screen in
a "turned-off" look.

Moving to target
----------------
As provided the project uses the PC LCD screen simulator.
In order to move this project to a target system the following target
hardware specific drivers must be created:

-  A touch screen driver which return three events
      EVENT_TOUCH_DOWN,   When touch down detected (finger press)
      EVENT_TOUCH_UP,     When touch up detected (finger lift)
      EVENT_TOUCH_DRAG,   When touch drag (finger moved while pressed)
   plus updating a global touch x,y coordinate set with x,y values normalized
   to the screen pixel size {0 to GDISPW-1},{0 to GDISPH-1}.

-  Timer driver which returns a timer tick event (EVENT_TICK)
   ex every 20 or 100 ms.
   This event may be optional depending on the application as it is only
   required by gtouchevent.c to implement soft keys with auto repeat


