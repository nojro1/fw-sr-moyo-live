/*******************************************************************************************

   This module creates and handle the actual "look" for the touch keys.

   The key position and key content look is based on parameters in a
   GTE touch event area description.

   This implementation uses 3D-frames to show touch-down and touch-up states

   This implementation supports multiple key content object types:
      - blank area
      - text string
      - array of text stings (index is selected in advance)
      - single symbol
      - symbol in font (index is selected in advance)
      - custom drawing function

   The content data is located in a GTE_DRAWDESC structure, which are part
   of the GTE (touch area event) structure.
   The first GTE_DRAWDESC structure parameter, the GOBJTYPE type parameter,
   identifies which type of data object the GTE_DRAWDESC structure / union
   contains.

   The GTE_DRAWDESC data types are defined in gkeylook.h

   ----

   If another graphic "look" is wanted then this is the module to update.

   2010 RAMTEX Engineering ApS

******************************************************************************************/
#include <gtouchevent.h>
#include <wnpopup.h> /* Frame types (used for draw of key state "looks" ) */
#include <string.h>  /* memcpy, memset */

/* Set current viewport to inner key area (local function) */
static void gtouch_set_inner_area( PGTE eventarea )
   {
   SGUCHAR fs;
   fs = wnframe_thicknes(FR_PLATE);
   gsetvp(eventarea->xb+fs,eventarea->yb+fs,
          eventarea->xe-fs,eventarea->ye-fs);
   }

/* Draw the key "look" for different states */
void gtouch_key_draw( PGTE eventarea, EVENT event)
   {
   SGUCHAR vp,oldvp;
   if (eventarea == NULL)
      return;
   if ((eventarea->xe-eventarea->xb <= 5) ||
       (eventarea->ye-eventarea->yb <= 5))
      /* Area is too smal for this key design type */
      return;

   if ((event == EVENT_TOUCH_CREATE) || (event == EVENT_TOUCH_UPDATE))
      {
      /*****  Draw the complete key *****/
      /* Get a temporary viewport */
      vp = wn_get_vpnum();
      oldvp = gselvp(vp);

      /* Draw key frame */
      gresetvp();
      if (event == EVENT_TOUCH_CREATE)
         {
         wnframe( eventarea->xb,eventarea->yb,
                  eventarea->xe,eventarea->ye,
                  FR_PLATE);
         }

      /* Draw key content */
      if (eventarea->drawdesc.drawfunc != NULL)
         {
         gtouch_set_inner_area( eventarea );
         gsetcolorf(eventarea->drawdesc.colorf);
         gsetcolorb(eventarea->drawdesc.colorb);
         eventarea->drawdesc.drawfunc( eventarea );
         }

      /* Restore */
      gselvp(oldvp);
      wn_free_vpnum(vp);
      }
   else
   if ((event == EVENT_TOUCH_DOWN) ||
       (event == EVENT_TOUCH_UP)   ||
       (event == EVENT_TOUCH_FOCUSLOST ))
      {
      /*****  Update the key frame "3D-look" in accordance with the key "state" *****/
      vp = wn_get_vpnum();
      oldvp = gselvp(vp);
      gresetvp(); /* Use absolute coordinates */
      wnframe( eventarea->xb,eventarea->yb,
               eventarea->xe,eventarea->ye,
               (event == EVENT_TOUCH_DOWN) ?  FR_INSERT : FR_PLATE);

      gselvp(oldvp);
      wn_free_vpnum(vp);
      }
   else
   if (event == EVENT_TOUCH_CLOSE)
      {
      /*****  Clear the key area to the original background color *****/
      /* Get a temporary viewport */
      vp = wn_get_vpnum();
      oldvp = gselvp(vp);

      /* Clear key area to background color */
      gsetvp(eventarea->xb,eventarea->yb,eventarea->xe,eventarea->ye);
      gsetcolorb( eventarea->color_close );
      gclrvp();

      /* Restore */
      gselvp(oldvp);
      wn_free_vpnum(vp);
      }

   /* Ignore all other events */
   }

/***************************************************************************

   Below is key content functions for drawing the key inner area content in
   accordance with the GTE_DRAWDESC in the GTE structure.
   The content may be a text string, an element in a text string array,
   a symbol or a symbol in a font

****************************************************************************/



/*
   Draw a symbol centeret in the current vieport
   Clear area around symbol to background color
*/
static void gtouch_draw_center_symbol( PGSYMBOL psym )
   {
   GXT x,w,sw;
   GYT y,h,sh;
   if (psym == NULL)
      return;
   sw = gsymw(psym);
   sh = gsymh(psym);
   w=ggetvpw();
   h=ggetvph();
   // Centre symbol
   if (sw >= w)
      x=0;
   else
      x=(w-sw)/2;
   if (sh >= h)
      y=0;
   else
      y=(h-sh)/2;
   gputsym(x,y,psym);
   // Clear area around symbol
   // In this way we avoid update "blinking" in non-buffered mode
   if (y>0)
      gfillvp(0,0,w-1,y-1,0);      // Clear top
   if (x>0)
      gfillvp(0,y,x-1,(y+sh)-1,0);    // Clear left for symbol
   if (x+sw < w)
      gfillvp(x+sw,y,w-1,(y+sh)-1,0); // Clear right for symbol
   if (y+sh < h)
      gfillvp(0,y+sh,w-1,h-1,0);   // Clear bottom
   }

/*
   Draw key content as a blank (colored) area
*/
static void gtouch_draw_blank( PGTE eventarea )
   {
   gclrvp();
   }

/*
   Draw key content for a string
*/
static void gtouch_draw_str( PGTE eventarea )
   {
   gsetmode( GVPCLR| GALIGN_HCENTER | GALIGN_VCENTER | GNOSCROLL | GPARTIAL_LINE | GLINECUT);
   gselfont( eventarea->drawdesc.d.str.font );
   if (eventarea->drawdesc.d.str.str != NULL)
      gputs( eventarea->drawdesc.d.str.str );
   }

/*
   Draw key content for a string array element
*/
static void gtouch_draw_strarr( PGTE eventarea )
   {
   gsetmode( GVPCLR| GALIGN_HCENTER | GALIGN_VCENTER | GNOSCROLL | GPARTIAL_LINE | GLINECUT);
   gselfont( eventarea->drawdesc.d.strarr.font );
   if (eventarea->drawdesc.d.strarr.index < eventarea->drawdesc.d.strarr.numelements)
      {
      PGSTR str = eventarea->drawdesc.d.strarr.pstr[eventarea->drawdesc.d.strarr.index];
      if (str != NULL)
         gputs( str );
      }
   }

/*
   Draw key content from a symbol
*/
static void gtouch_draw_symbol( PGTE eventarea )
   {
   gtouch_draw_center_symbol( eventarea->drawdesc.d.sym.psym);
   }

/*
   Draw key content for a font symbol
*/
static void gtouch_draw_font( PGTE eventarea )
   {
   /* Get pointer to symbol in font */
   if (eventarea->drawdesc.d.symfont.index < eventarea->drawdesc.d.symfont.numelements)
      {
      PGSYMBOL psym;
      psym = ggetfsym( eventarea->drawdesc.d.symfont.index,
                       eventarea->drawdesc.d.symfont.pfont);
      gtouch_draw_center_symbol( psym );
      }
   }

/*
   Init the key area "look" descriptor.
   Configure the GTE_DRAWDESC structure in a GTE eventarea, in accordance
   with the key content object type: GOBJTYPE

   The function will handle insetion of defaults in most cases
*/
void gtouch_look_init( PGTE eventarea, GOBJTYPE objtype, void *pobj, SGUCHAR dodraw )
   {
   if (eventarea == NULL)
      return;

   /* Clear "look" descriptor (= empty area) */
   memset(&eventarea->drawdesc, 0, sizeof(GTE_DRAWDESC));

   if (eventarea->draweventfunc == NULL)
      // First time init, insert default drawing event handling
      eventarea->draweventfunc = (PGTR_DRAWFUNC) &gtouch_key_draw;

   if ((pobj == NULL) && (objtype != GOBJT_BLANK))
      return; /* Assume a touch sense area without graphics */

   /* Init object descriptor */
   switch (objtype)
      {
      /* Types used for default initialization */
      case GOBJT_BLANK    :  /* Blank area (colored field) */
         {
         eventarea->drawdesc.type     = GOBJTS_BLANK;
         eventarea->drawdesc.drawfunc = (PCONTENT_FUNC)&gtouch_draw_blank;
         eventarea->drawdesc.colorf   = KEYCOLORF;
         eventarea->drawdesc.colorb   = KEYCOLORB;
         break;
         }
      case GOBJT_STR      :  /* C String */
         {
         eventarea->drawdesc.type     = GOBJTS_STR;
         eventarea->drawdesc.drawfunc = (PCONTENT_FUNC)&gtouch_draw_str;
         eventarea->drawdesc.d.str.str  = (PGSTR) pobj;
         eventarea->drawdesc.d.str.font = KEYFONT;
         eventarea->drawdesc.colorf   = KEYCOLORF;
         eventarea->drawdesc.colorb   = KEYCOLORB;
         break;
         }
      case GOBJT_STRARR   :  /* NULL terminated C String array */
         {
         eventarea->drawdesc.type   = GOBJTS_STRARR;
         eventarea->drawdesc.drawfunc = (PCONTENT_FUNC)&gtouch_draw_strarr;
         eventarea->drawdesc.d.strarr.pstr    = (PGSTR *) pobj;
         eventarea->drawdesc.d.strarr.numelements = 0;
         eventarea->drawdesc.d.strarr.index  = 0;
         if (eventarea->drawdesc.d.strarr.pstr != NULL)
            {
            // Search the terminating NULL element to find the pointer array size.
            for (;;eventarea->drawdesc.d.strarr.numelements++)
               {
               if (eventarea->drawdesc.d.strarr.pstr[
                    eventarea->drawdesc.d.strarr.numelements] == NULL)
                  break;
               }
            }
         eventarea->drawdesc.d.strarr.font   = KEYFONT;
         eventarea->drawdesc.colorf = KEYCOLORF;
         eventarea->drawdesc.colorb = KEYCOLORB;
         break;
         }
      case GOBJT_SYMBOL   :  /* Symbol */
         {
         eventarea->drawdesc.type     = GOBJTS_SYMBOL;
         eventarea->drawdesc.drawfunc = (PCONTENT_FUNC)&gtouch_draw_symbol;
         eventarea->drawdesc.d.sym.psym = (PGSYMBOL) pobj;
         eventarea->drawdesc.colorf   = KEYCOLORF;
         eventarea->drawdesc.colorb   = KEYCOLORB;
         break;
         }
      case GOBJT_SYMFONT  :  /* Font of symbols */
         {
         eventarea->drawdesc.type   = GOBJTS_SYMFONT;
         eventarea->drawdesc.drawfunc = (PCONTENT_FUNC)&gtouch_draw_font;
         eventarea->drawdesc.d.symfont.index  = 0;
         eventarea->drawdesc.d.symfont.numelements = 0;
         eventarea->drawdesc.d.symfont.pfont  = (PGFONT) pobj;
         if (pobj != NULL)
            eventarea->drawdesc.d.symfont.numelements =
               gfontsize( eventarea->drawdesc.d.symfont.pfont);
         eventarea->drawdesc.colorf = KEYCOLORF;
         eventarea->drawdesc.colorb = KEYCOLORB;
         break;
         }
      case GOBJT_FUNC  :  /* Custom drawing function */
         {
         eventarea->drawdesc.type     = GOBJTS_FUNC;
         eventarea->drawdesc.drawfunc = (PCONTENT_FUNC) pobj;
         eventarea->drawdesc.colorf = KEYCOLORF;
         eventarea->drawdesc.colorb = KEYCOLORB;
         break;
         }

      /* Types used for complete initialization of GTE_DRAWDESC data sets */
      case GOBJTS_BLANK   :  /* Blank area structure */
      case GOBJTS_STR     :  /* C String  structure */
      case GOBJTS_STRARR  :  /* C String array  structure */
      case GOBJTS_SYMBOL  :  /* Symbol  structure */
      case GOBJTS_FUNC    :  /* Custom function structure */
      case GOBJTS_SYMFONT :  /* Font of symbols  structure */
         {
         PGTE_DRAWDESC p;
         p = (PGTE_DRAWDESC) pobj;
         if (objtype == p->type)
            {
            /* The type matches, assume correct data, copy whole setup */
            memcpy(&eventarea->drawdesc, p, sizeof(GTE_DRAWDESC));
            }
         break;
         }
      default:
         return;
      }

   if (dodraw)
      // Draw at once
      gtouch_key_draw( eventarea, EVENT_TOUCH_UPDATE);
   }


/*****************************************************************

   Functions for changing the key look while the key is drawn

******************************************************************/

/*
   The index parameter can be used for selecting one of multiple
   elements if the content is a font or a sting array.
   The parameter is don't care for single element objects.
*/
void gtouch_key_draw_index( PGTE eventarea, unsigned int index, SGUCHAR dodraw )
   {
   unsigned int numelem;
   if (eventarea == NULL)
      return;

   /* Check and set index if it is a multi element object */
   switch (eventarea->drawdesc.type)
      {
      case GOBJTS_STRARR:
         {
         // Do a limit check, optionally force index within range
         numelem = eventarea->drawdesc.d.strarr.numelements;
         if (index >= numelem)
            index = (numelem == 0) ? 0 : numelem-1;
         eventarea->drawdesc.d.strarr.index = index;
         break;
         }
      case GOBJTS_SYMFONT:
         {
         // Do a limit check, optionally force index within range
         numelem = eventarea->drawdesc.d.symfont.numelements;
         if (index >= numelem)
            index = (numelem == 0) ? 0 : numelem-1;
         eventarea->drawdesc.d.symfont.index = index;
         break;
         }
      default:
         break;
      }

   if (dodraw)
      gtouch_key_draw( eventarea, EVENT_TOUCH_UPDATE);
   }

/*
   Return current index for array drawing elements
   Return 0 for all other element types or parameter error
*/
unsigned int gtouch_get_index( PGTE eventarea)
   {
   if (eventarea == NULL)
      return 0;

   switch (eventarea->drawdesc.type)
      {
      case GOBJTS_STRARR:
         return eventarea->drawdesc.d.strarr.index;
      case GOBJTS_SYMFONT:
         return eventarea->drawdesc.d.symfont.index;
      default:
         return 0;
      }
   }

/*
   Return number of elements for array drawing elements
   Return 0 for all other element types or parameter error
*/
unsigned int gtouch_get_numelem( PGTE eventarea)
   {
   if (eventarea == NULL)
      return 0;

   switch (eventarea->drawdesc.type)
      {
      case GOBJTS_STRARR:
         return eventarea->drawdesc.d.strarr.numelements;
      case GOBJTS_SYMFONT:
         return eventarea->drawdesc.d.symfont.numelements;
      default:
         return 0;
      }
   }

/*
   Change foreground color for content of a touch (key) area

   The area is updated at once if dodraw != 0
*/
void gtouch_setcolorf( PGTE eventarea, GCOLOR foreground_color, SGUCHAR dodraw )
   {
   if (eventarea != NULL)
      {
      eventarea->drawdesc.colorf = foreground_color;
      if (dodraw)
         gtouch_key_draw( eventarea, EVENT_TOUCH_UPDATE);
      }
   }

/*
   Change background color for content of a touch (key) area

   The area is updated at once if dodraw != 0
*/
void gtouch_setcolorb( PGTE eventarea, GCOLOR background_color, SGUCHAR dodraw )
   {
   if (eventarea != NULL)
      {
      eventarea->drawdesc.colorb = background_color;
      if (dodraw)
         gtouch_key_draw( eventarea, EVENT_TOUCH_UPDATE);
      }
   }




