/***************************************************************************

   Target touch screen and timer driver interface for demo.

   This module (simulate) interface to a touch screen controller
   and a hardware timer

   Creates the primary events:

   EVENT_TOUCH_DOWN,        Basic Touch down event (finger press)
   EVENT_TOUCH_UP,          Basic Touch up event (finger lift)
   EVENT_TOUCH_DRAG,        Basic Touch drag (during touch down)
   EVENT_TICK,              Timer tick

   The latest touch x,y coordinates are carried in the public values
      GXT touch_x;
      GYT touch_y;

   In PC simulation mode touch events are simulated with mouse right-clicks
   on the LCD simulator screen.
   The demo can be exited by pressing the keyboard Esc key when the
   LCD simulator has focus.

   Adaptions needed to move the demo to target mode:
      A (interrupt based) timestamp counter should be implemented in the timeout.c module
      A touch screen controller handler shou be implemented in the gtouchkp.c module

   2010 RAMTEX Engineering ApS

***************************************************************************/
#include <wnpopup.h>
#include <timer.h>     /* Timestamp, Timeout feature */
#include <gevents.h>

#ifdef GHW_PCSIM
#include <gsimintf.h>  /* Simulator functions */
#include <gkeycode.h>  /* Keys definitions */
#include <windows.h>   /* Sleep() */
#endif

/* Public variables to hold touch event x,y coordinates.
   The x,y values must have been normalized to the screen size by the driver */
GXT touch_x;
GYT touch_y;

static TIMEOUT ticktime;

/*
    Fetch GUI event  ( (Keyboard,) touch screen, time tick)

    Returns EVENT_IDLE if no GUI event is detected.
*/
EVENT wm_keyintf(void)
   {
   /* Check for touch click event */
   unsigned char edge, level;

   /* Use generic touch interface function */
   if (gtouchkp(&edge, &level, &touch_x, &touch_y))
      {
      /* Change in touch state, convert to events */
      if (edge && level)
         return EVENT_TOUCH_DOWN;
      if (edge && !level)
         return EVENT_TOUCH_UP;
      if (!edge && level)
         return EVENT_TOUCH_DRAG;
      }

   /* Check for keyboard event */
   #ifdef GHW_PCSIM
   if (GSimKbHit())
      {
      /* Get PC key and translate to demo events */
      if (GSimKbGet() == GK_ESC)
         {
         exit(1); /* Skip demo */
         }
      }
   #endif

   /* Check for a timing event */
   if (timeout(&ticktime))
      {
      timeout_set(&ticktime,2);
      return EVENT_TICK;
      }

   #ifdef GHW_PCSIM
   Sleep(1); /* Leave some time to other PC applications */
   #endif

   /* else just return no event */
   return EVENT_IDLE;
   }

/*
    Initialize GUI event drivers here
*/
void wm_keyintf_init(void)
   {
   #ifdef GHW_PCSIM
   GSimPuts("----\n"
            "Touch screen simulated with mouse right-click\n"
            "Exit demo with Esc");
   #else
   /* Insert init of keyboard, timer and touch controller here */
   #endif

   timeout_set(&ticktime,0);
   }

