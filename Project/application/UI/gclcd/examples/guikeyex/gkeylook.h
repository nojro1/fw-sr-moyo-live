/*******************************************************************************************

   Key content definitions.

   Defines a number of structures used for storing data used for key drawing
   The structures are combined in GTE_DRAWDESC union which is part of the
   GTE touch area descriptor

   The GOBJT_xxx types are incomplete types defining just the primary
   drawing data parameter. The rest of the GTE_DRAWDESC elements are
   automatically set to defaults (colors, fonts, indexes, etc)

   The GOBJTS_xxx types identify pointers to complete data structure types
   containing all the parameters needed by the touch area "look" drawing

   After a GTE touch screen area event structure is created then
   GTE_DRAWDESC data part is a complete GOBJTS_xxx data description
   structure type.

   ----

   If new graphic key content data types is wanted then this is the module to update.

   2010 RAMTEX Engineering ApS

******************************************************************************************/
#ifndef GKEYLOOK_H
#define GKEYLOOK_H

#include <gdisp.h>  // GCOLOR definition

#ifdef __cplusplus
    extern "C" {
#endif

#if ((GDISPW < 160) || (GDISPH < 160))
   #define  KEYFONT      &cp8859_9
#else
   #define  KEYFONT      &cp8859_14
#endif
#define  KEYCOLORB G_GREY
#define  KEYCOLORF G_BLACK

/* Generic content draw method function prototype,
   the parameter will in most implementations be a pointer to
   the parent GTE structure (defined in gtouchevent.h) */
typedef void (*PCONTENT_FUNC)(void *);

/*
   Define data structures used by graphic area content operations
   A union of these structures is incoorporated in the GTE structure.
   The content of the structures is used by the functions in gkeylook.c
*/
typedef enum
   {
   /* Types used for default initialization except primary pointer data */
   GOBJT_EMPTY = 0, /* Empty area (sense area without "graphic look") */
   GOBJT_BLANK,     /* Blank area (colored field) */
   GOBJT_STR,       /* C String */
   GOBJT_STRARR,    /* Null terminated C string pointer array */
   GOBJT_SYMBOL,    /* Symbol */
   GOBJT_SYMFONT,   /* Font (of symbols )*/
   GOBJT_FUNC,      /* Custom drawing function */

   /* Types used for complete initialization */
   GOBJTS_BLANK,    /* Blank area structure */
   GOBJTS_STR,      /* C String  structure */
   GOBJTS_STRARR,   /* C String array  structure */
   GOBJTS_SYMBOL,   /* Symbol  structure */
   GOBJTS_SYMFONT,  /* Font of symbols  structure */
   GOBJTS_FUNC,     /* Custom drawing function  structure */
   GOBJTS_LAST      /* Last type (for limit check) */
   } GOBJTYPE;

/*
   Structures for data content storages. All the structures are combined by
   the GTE_DRAWDESC union which are part of the GTE structure
   (= graphic touch event handler structure)

   New data content structures for other types of data can be defined below and
   inserted in the GTE_DRAWDESC union.
   The only requirement is that the two first structure parameters are
   the same:
      {
      GOBJTYPE type;       // Object type identifier, defined above
      PCONTENT_FUNC drawfunc;  // Function taking a pointer to a touch area structure
      ..... // other structure members
      }

*/

typedef struct
   {
   PGSTR str;          /* Pointer to C string */
   PGFONT font;         /* Font used for output (NULL = "look" default) */
   } GOBJ_STR,*PGOBJ_STR;

typedef struct
   {
   PGSTR *pstr;             /* Pointer to array of C string pointers */
   PGFONT font;             /* Font used for output (NULL = "look" default) */
   unsigned int numelements;/* Index to string in string array */
   unsigned int index;      /* Index to string in string array */
   } GOBJ_STRARR, *PGOBJ_STRARR;

typedef struct
   {
   PGSYMBOL psym;     /* Pointer to symbol */
   } GOBJ_SYMBOL, *PGOBJ_SYMBOL;

typedef struct
   {
   PGFONT pfont;            /* Pointer to font of symbols */
   unsigned int numelements;/* Max number of elements */
   unsigned int index;      /* Index to string in string array */
   } GOBJ_SYMFONT,*PGOBJ_SYMFONT;

/* Union which create an overlap of the above key data structures */
typedef union
   {
   void *ptr;   /* Dummy */
   GOBJ_STR     str;
   GOBJ_STRARR  strarr;
   GOBJ_SYMBOL  sym;
   GOBJ_SYMFONT symfont;
   } GTE_DRAWDATA, *PGTE_DRAWDATA;

typedef struct
   {
   GOBJTYPE type;      /* Object type, must be assigned to GOBJT_BLANK */
   PCONTENT_FUNC drawfunc;
   GCOLOR colorf;      /* Area foreground color (used during drawing update) */
   GCOLOR colorb;      /* Area foreground color (used during drawing update) */
   GTE_DRAWDATA d;
   } GTE_DRAWDESC, *PGTE_DRAWDESC;

#ifdef __cplusplus
   }
#endif
#endif



