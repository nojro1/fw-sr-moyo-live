/*
   This example contains a minimum system to check the LCD module
   configuration (gdispcfg.h and sgxxxxx.h files)
*/
#include <gdisp.h> /* LCD prototypes */
//---------------------------------------------------------
/*
SSD1963_8-bit
Program for writing to Newhaven Display 5.7" TFT 640x480

(c)2009 Curt Lagerstam - Newhaven Display International, LLC.

        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation; either version 2 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.
*/
//---------------------------------------------------------
#include        <stdio.h>        // define I/O functions
#include        <sgio.h>
#include  <windows.h> //sleep


//unsigned char code pic[9600];
//=======================================
void delayms(int n)
{
Sleep(n);
}
//;******************************************************************************
void Write_Command(unsigned char cmd)
{
sgwrby(GHWCMD,(SGUCHAR)cmd);
}
//;******************************************************************************
void Write_Data(unsigned char dat)
{
sgwrby(GHWWR, (SGUCHAR)dat);
}
//====================================================
void Command_Write(unsigned char REG,unsigned char VALUE)
{
Write_Command(REG);
Write_Data(VALUE);
}
//======================================================
void SendData(unsigned long color)
{
Write_Data((unsigned char)(color>>16)); //red
Write_Data((unsigned char)(color>>8));          //green
Write_Data((unsigned char)color);                       //blue
}
//======================================================
// initialize controller
//======================================================
void Init_SSD1963 (void)
{
delayms(5);
delayms(100);
Write_Command(0x01);     //Software Reset
Write_Command(0x01);
Write_Command(0x01);
delayms(10);
Command_Write(0xe0,0x01);    //START PLL
Command_Write(0xe0,0x03);    //LOCK PLL
Write_Command(0xb0);            //SET LCD MODE  SET TFT 18Bits MODE
Write_Data(0x0c);                       //SET TFT MODE & hsync+Vsync+DEN MODE
Write_Data(0x80);                       //SET TFT MODE & hsync+Vsync+DEN MODE
Write_Data(0x02);                       //SET horizontal size=640-1 HightByte
Write_Data(0x7f);                   //SET horizontal size=640-1 LowByte
Write_Data(0x01);                       //SET vertical size=480-1 HightByte
Write_Data(0xdf);                       //SET vertical size=480-1 LowByte
Write_Data(0x00);                       //SET even/odd line RGB seq.=RGB
Command_Write(0xf0,0x00);       //SET pixel data I/F format=8bit
Command_Write(0x3a,0x60);   // SET R G B format = 6 6 6
Write_Command(0xe6);            //SET PCLK freq=4.94MHz  ; pixel clock frequency
Write_Data(0x02);
Write_Data(0xff);
Write_Data(0xff);
Write_Command(0xb4);            //SET HBP,
Write_Data(0x02);                       //SET HSYNC Total=760
Write_Data(0xf8);
Write_Data(0x00);                       //SET HBP 68
Write_Data(0x44);
Write_Data(0x0f);                       //SET VBP 16=15+1
Write_Data(0x00);                       //SET Hsync pulse start position
Write_Data(0x00);
Write_Data(0x00);                       //SET Hsync pulse subpixel start position
Write_Command(0xb6);            //SET VBP,
Write_Data(0x01);                       //SET Vsync total
Write_Data(0xf8);
Write_Data(0x00);                       //SET VBP=19
Write_Data(0x13);
Write_Data(0x07);                       //SET Vsync pulse 8=7+1
Write_Data(0x00);                       //SET Vsync pulse start position
Write_Data(0x00);
Write_Command(0x2a);            //SET column address
Write_Data(0x00);                       //SET start column address=0
Write_Data(0x00);
Write_Data(0x02);                       //SET end column address=639
Write_Data(0x7f);
Write_Command(0x2b);            //SET page address
Write_Data(0x00);                       //SET start page address=0
Write_Data(0x00);
Write_Data(0x01);                       //SET end page address=479
Write_Data(0xdf);
Write_Command(0x29);            //SET display on
}
//======================================================
void WindowSet(unsigned int s_x,unsigned int e_x,unsigned int s_y,unsigned int e_y)
{
Write_Command(0x2a);            //SET page address
Write_Data((unsigned char)(s_x>>8));                    //SET start page address=0
Write_Data(s_x);
Write_Data((unsigned char)(e_x>>8));                    //SET end page address=639
Write_Data(e_x);

Write_Command(0x2b);            //SET column address
Write_Data((unsigned char)(s_y>>8));                    //SET start column address=0
Write_Data(s_y);
Write_Data((unsigned char)(e_y>>8));                    //SET end column address=479
Write_Data(e_y);
}
//=======================================
void FULL_ON(unsigned long dat)
{
unsigned int x,y;
WindowSet(0x0000,0x027f,0x0000,0x01df);
Write_Command(0x2c);
for(x=0; x<480; x++)
   {
   for(y= 0;y<640;y++)
      {
           SendData(dat);
      }
   }
}
//=======================================
void QUADS(void)
{
unsigned int i,j;
WindowSet(0x0000,0x027f,0x0000,0x01df);
Write_Command(0x2c);
for(j= 0 ;j<240;j++)
  {
   for(i=0;i<320;i++)
   {
    SendData(0x0000FF);             //blue
   }
   for(i=0;i<320;i++)
   {
    SendData(0xFF0000);             //red
   }
 }
for(j= 0 ;j<240;j++)
  {
   for(i=0;i<320;i++)
   {
    SendData(0xFFFF00);             //yellow
   }
   for(i=0;i<320;i++)
   {
    SendData(0x00FF00);             //green
   }
 }
}
//=======================================
void tstmain(void)
{
ghw_io_init();

 Init_SSD1963();
 delayms(5);
 QUADS();
 //delayms(2000);
 FULL_ON(0xff0000);             //red
 //delayms(2000);
 FULL_ON(0x00ff00);             //green
 //delayms(2000);
 FULL_ON(0x0000ff);             //blue
 //delayms(2000);

}
void main(void)
   {
   /*
      Initialize display hardware and print an error message
      on the center of the LCD display module
   */
   const char str[] = "3 LINE\nCENTERED\nMESSAGE";
//   tstmain();
//ghw_io_init();

// Init_SSD1963();
// delayms(5);
// QUADS();

   ghw_puterr( &str[0] );

   for(;;)       /* Wait forever in target mode */
      {
      static int i = 0;
      i++; /* A dummy operation so we can place a breakpoint here */
      }
   }
