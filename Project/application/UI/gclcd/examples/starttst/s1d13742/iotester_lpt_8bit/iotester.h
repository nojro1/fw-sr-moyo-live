#ifndef IOTESTER_H
#define IOTESTER_H
/**********************************************************************/
/*                                                                    */
/*   iotester.h                                                       */
/*                                                                    */
/*   Include  this  prototype  in  every  module  using  the iot_...  */
/*   functions for iotester access.                                   */
/*                                                                    */
/*   Revision date:     09-09-05                                      */
/*   Revision Purpose:  Added support for IOTESTER hardware ver 4.    */
/*                      - 20 bit address bus mode                     */
/*                      - 16 bit databus mode                         */
/*                      - Target interrupt support                    */
/*                      - Message call back function                  */
/*                      - Byte access modes for IOTester registers    */
/*                                                                    */
/*   Version number: 4                                                */
/*   Copyright (c) RAMTEX Engineering Aps 1998-2005                   */
/*                                                                    */
/**********************************************************************/

#ifdef __cplusplus
    extern "C" {
#endif

/* typedef's for dynamic loaded library */
typedef int (*IOT_INT_SH)(unsigned short);
typedef void (*IOT_CHARCHAR)(unsigned char, unsigned char);
typedef void (*IOT_VOID)(void);
typedef unsigned long (*IOT_LONG_LONG)(unsigned long);
typedef unsigned long (*IOT_LONG_LONGLONG)(unsigned long, unsigned long);
typedef void (*IOT_LONGLONG)(unsigned long, unsigned long);
typedef void (*IOT_LONGLONGLONG)(unsigned long, unsigned long, unsigned long);
typedef void (*IOT_INTCHAR)(unsigned short, unsigned char);
typedef void (* IOT_FUNC_PTR)(void);
typedef int (*IOT_INTINSTALL)(unsigned short, IOT_FUNC_PTR, unsigned short);
typedef void (*IOT_SH)(unsigned short);
typedef unsigned char (*IOT_CHAR_CHAR)(unsigned char);
typedef unsigned char (*IOT_CHAR)(void);

/* function pointers for iot dynamic loaded library */
/* could happen non-extern with extern declarations in iotester.h */
extern IOT_INTINSTALL    iot_device_intr_init;
extern IOT_VOID          iot_device_exit;
extern IOT_CHARCHAR      iot_set_buswait;
extern IOT_LONG_LONG     iot_rd;
extern IOT_LONGLONG      iot_wr;
extern IOT_LONGLONG      iot_and;
extern IOT_LONGLONG      iot_or;
extern IOT_LONGLONG      iot_xor;
extern IOT_LONG_LONGLONG iot_rdbuf;
extern IOT_LONGLONGLONG  iot_wrbuf;
extern IOT_LONGLONGLONG  iot_andbuf;
extern IOT_LONGLONGLONG  iot_orbuf;
extern IOT_LONGLONGLONG  iot_xorbuf;
extern IOT_INTCHAR       iot_outportb;
extern IOT_INT_SH        iot_inportb;
extern IOT_CHAR_CHAR     iot_intr_enable;
extern IOT_CHAR          iot_intr_poll;

/* IOT declarations from iotester.h */
/* Define IOTESTER modes */
#define  IOT_DEVICE0  0x00000000
#define  IOT_DEVICE1  0x01000000
#define  IOT_DEVICE2  0x02000000
#define  IOT_DEVICE3  0x03000000
#define  IOT_DEVICE4  0x04000000
#define  IOT_DEVICE5  0x05000000
#define  IOT_DEVICE6  0x06000000
#define  IOT_DEVICE7  0x07000000

#define  IOT_REGSIZE  0x10000000
#define  IOT_REGMSK   0x30000000
#define  IOT_ENDIAN   0x40000000  /* Bus MSB at high address */
#define  IOT_IOREG    0x80ffffe0

#define  IOT_REG8     0x00000000  /* 8 bit register */
#define  IOT_REG16    0x10000000  /* 16 bit register */
#define  IOT_REG16L   0x10000000  /* 16 bit register, MSB at low address */
#define  IOT_REG24L   0x20000000  /* 24 bit register, MSB or MSW  at low address */
#define  IOT_REG32L   0x30000000  /* 32 bit register, MSB or MSW  at low address */
#define  IOT_REG16H   0x50000000  /* 16 bit register, MSB at high address */
#define  IOT_REG24H   0x60000000  /* 24 bit register, MSB or MSW  at high address */
#define  IOT_REG32H   0x70000000  /* 32 bit register, MSB or MSW at high address */

/* Sub indexes of internal IOTESTER registers  */
#define ADR_BUSDAT   28UL
#define ADR_ADR      25UL
#define ADR_MODE     24UL
#define ADR_INPUT    20UL
#define ADR_IODIR    16UL
#define ADR_IOREGS   12UL
#define ADR_IOREGC    8UL

/* Predefined IOTester registers */
#define  IOT_BUS8_REG    (IOT_IOREG|IOT_REG8  |ADR_BUSDAT)
#define  IOT_BUS16_REG   (IOT_IOREG|IOT_REG16H|ADR_BUSDAT)
#define  IOT_BUS32_REG   (IOT_IOREG|IOT_REG32H|ADR_BUSDAT)
#define  IOT_ADR_REG     (IOT_IOREG|IOT_REG24H|ADR_ADR   )
#define  IOT_MODE_REG    (IOT_IOREG|IOT_REG8  |ADR_MODE  )
#define  IOT_INPUT_REG   (IOT_IOREG|IOT_REG32H|ADR_INPUT )
#define  IOT_IODIR_REG   (IOT_IOREG|IOT_REG32H|ADR_IODIR )
#define  IOT_IOSET_REG   (IOT_IOREG|IOT_REG32H|ADR_IOREGS)
#define  IOT_IOCLR_REG   (IOT_IOREG|IOT_REG32H|ADR_IOREGC)

/* Predefined IOTester registers for byte access */
#define  IOT_ADR_REG0    (IOT_IOREG|IOT_REG8|(ADR_ADR   +0))
#define  IOT_ADR_REG1    (IOT_IOREG|IOT_REG8|(ADR_ADR   +1))
#define  IOT_ADR_REG2    (IOT_IOREG|IOT_REG8|(ADR_ADR   +2))
#define  IOT_INPUT_REG0  (IOT_IOREG|IOT_REG8|(ADR_INPUT +0))
#define  IOT_INPUT_REG1  (IOT_IOREG|IOT_REG8|(ADR_INPUT +1))
#define  IOT_INPUT_REG2  (IOT_IOREG|IOT_REG8|(ADR_INPUT +2))
#define  IOT_INPUT_REG3  (IOT_IOREG|IOT_REG8|(ADR_INPUT +3))
#define  IOT_IODIR_REG0  (IOT_IOREG|IOT_REG8|(ADR_IODIR +0))
#define  IOT_IODIR_REG1  (IOT_IOREG|IOT_REG8|(ADR_IODIR +1))
#define  IOT_IODIR_REG2  (IOT_IOREG|IOT_REG8|(ADR_IODIR +2))
#define  IOT_IODIR_REG3  (IOT_IOREG|IOT_REG8|(ADR_IODIR +3))
#define  IOT_IOSET_REG0  (IOT_IOREG|IOT_REG8|(ADR_IOREGS+0))
#define  IOT_IOSET_REG1  (IOT_IOREG|IOT_REG8|(ADR_IOREGS+1))
#define  IOT_IOSET_REG2  (IOT_IOREG|IOT_REG8|(ADR_IOREGS+2))
#define  IOT_IOSET_REG3  (IOT_IOREG|IOT_REG8|(ADR_IOREGS+3))
#define  IOT_IOCLR_REG0  (IOT_IOREG|IOT_REG8|(ADR_IOREGC+0))
#define  IOT_IOCLR_REG1  (IOT_IOREG|IOT_REG8|(ADR_IOREGC+1))
#define  IOT_IOCLR_REG2  (IOT_IOREG|IOT_REG8|(ADR_IOREGC+2))
#define  IOT_IOCLR_REG3  (IOT_IOREG|IOT_REG8|(ADR_IOREGC+3))

/* Predefined IOTESTER mode configurations */
/* I/O only mode */
#define  IOT_MODE_IO             0x00
/* 8 bit data bus modes */
#define  IOT_MODE_RDWR_ADR20     0xF4
#define  IOT_MODE_RDWR_ADR18     0xFC
#define  IOT_MODE_RDWR_ADR16     0xBC
#define  IOT_MODE_RDWR_ADR8      0x9C
#define  IOT_MODE_RDWR_ADR2      0x8C
#define  IOT_MODE_E_RW_ADR20     0xF0
#define  IOT_MODE_E_RW_ADR18     0xF8
#define  IOT_MODE_E_RW_ADR16     0xB8
#define  IOT_MODE_E_RW_ADR8      0x98
#define  IOT_MODE_E_RW_ADR2      0x88
/* 16 bit data bus modes */
#define  IOT_MODE_RDWR_ADR12_D16 0xF5
#define  IOT_MODE_RDWR_ADR10_D16 0xFD
#define  IOT_MODE_RDWR_ADR8_D16  0xBD
#define  IOT_MODE_RDWR_ADR2_D16  0xAD
#define  IOT_MODE_E_RW_ADR12_D16 0xF1
#define  IOT_MODE_E_RW_ADR10_D16 0xF9
#define  IOT_MODE_E_RW_ADR8_D16  0x99
#define  IOT_MODE_E_RW_ADR2_D16  0x89

/* Enable interrupt. Can be OR'ed with the other mode setups (override use of P17 as A17) */
#define  IOT_MODE_IRQ_ENABLE 0x02

/* Reset and interupt bits in IOT_IOSET_REG, IOT_IOCLR_REG, IOT_IODIR_REG, IOT_INPUT_REG */
#define  IOT_RESET_BIT 0x080000
#define  IOT_IRQ_BIT   0x020000

/* LPT port addresses for iotester_init parameters */
#define IOT_LPTSEEK     0
#define IOT_LPT1        0x378
#define IOT_LPT2        0x278
#define IOT_LPT3        0x3BC
#define IOT_IRQ_DEFAULT 0
#define IOT_LPT_IRQ5    5
#define IOT_LPT_IRQ7    7

/* Error reasons */
#define IOT_OK 0
#define IOT_HW_ACCESS_FAULT     0x01 /* Some error (other bits specify the cause) */
#define IOT_DLL_ACCESS_FAULT    0x03 /* Can not load IOTESTER.DLL */
#define IOT_FUNC_ACCESS_FAULT   0x05 /* Could not connect all DLL functions (DLL version error) */
#define IOT_DRIVER_ACCESS_FAULT 0x09 /* Could not open for LPT port access */
#define IOT_LPT_ACCESS_FAULT    0x11 /* No IOTESTER found on LPT1, LPT2, LPT3 */
#define IOT_TESTER_ACCESS_FAULT 0x21 /* No IOTESTER found on specified LPT port */
#define IOT_VERSION_FAULT       0x41 /* IOTESTER.SYS and IOTESTER.DLL versions does not match */

/* initialization: */
void __stdcall iot_print_message(const char *str);
int iot_init_interrupt(unsigned short lptport, IOT_FUNC_PTR intrfunc, unsigned short irq_num);
#define iot_init(lptport) iot_init_interrupt((lptport), NULL, 0)

/* termination: */
void iot_exit(void);

#ifdef __cplusplus
   }
#endif
#endif
