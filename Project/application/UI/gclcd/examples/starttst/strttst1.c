/*
   This example contains a minimum system to check the LCD module
   configuration (gdispcfg.h and sgxxxxx.h files)
*/
#include <gdisp.h> /* LCD prototypes */

void main(void)
   {
   /*
      Initialize display hardware and print an error message
      on the center of the LCD display module
   */
   const char str[] = "3 LINE\nCENTERED\nMESSAGE";

   ghw_puterr( &str[0] );

   for(;;)       /* Wait      forever in target mode */
      {
      static int i = 0;
      i++; /* A dummy operation so we can place a breakpoint here */
      }
   }

