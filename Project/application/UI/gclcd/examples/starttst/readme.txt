STARTTST project
----------------
Create a new (console) project.

Add the application file:
\GCLCD\EXAMPLES\STARTTST\STRTTST1.C   The demo program

Add the LCD controller driver files:
\GCLCD\controller\GHWINIT.C           The LCD controller driver
\GCLCD\controller\GHWBUF.C            Buffer handler (if buffered mode is used)
\GCLCD\controller\CFGIO\GHWIOINI.C    Any LCD controller specific target setup
                                      Read comments in GHWIOINI.C for details

Add these directories to the default include file paths:
   \GCLCD\INC
   \GCLCD\FONTS
   \GCLCD\EXAMPLES\STARTTST\controller\target

PC Simulation
=============
The following extra steps are needed to do PC simulation:

Win32 mode simulation with Borland C++ Builder 5 or MSVC 6 (or later versions)
-----------------------------------------------------------------------------
Add the compiler definitions:
    GHW_PCSIM     Turns on the simulator interface.
    GHW_NOHDW     Turns off direct LCD hardware I/O.
    SGPCMODE      Turns on the PC compiler variant of SG integer types.

Add the simulator file:
  \GCLCD\controller\controllerSIM.C    The LCD controller simulator.

Add graphic Win32 interface:
  \GLCD\controller\GSIMINTF.C          Interface to LCD simulator program.

Add the Win socket library (With MSVC 6. Newer compiler versions may add it automatically).
   WSOCK32.LIB

Connect display hardware to the PC via the IOTester tool
========================================================
A number of ready made IOTester projects are included with the display driver library.

The IOTester tool enable connection of real I/O hardware (display / touch screen) to the PC 
in such a way the I/O hardware can be accessed and operated directly from a GUI program 
application running on the PC. It is the most efficient way to test and use a new display 
module.
For more information about the IOTester tool chain please see
     www.ramtex.dk/iotester/iotester.htm

If you want to create you own IOTester-USB based project from scratch these few 
extra steps are needed:

  Use these compiler definitions instead:
   IOTESTER_USB  Turns on IOTester access functions.
   SGPCMODE      Turns on the PC compiler variant of SG integer types.

  From the IOTester C_CPP source library directory (part of IOTester tool driver lib)
  make a local copy on the project directory of the follow two files:
   GHWIOINI.C    Init IOTester for use with display driver library
                 (any adjustments to "processor bus" types and reset pin polarities
                 are done here). Replaces the default GHWIOINI.C file.
   SGIOPCTA.H    SG definitions of LCD registers for use with IOTester
                 (any adjustments to register access definitions are done here)

  The following IOTester files may either be copied to the project directory as well (as done
  in the ready made project examples), or may be included directly from their original location.
  If the original location is used then update the compilers include file path accordingly.
   SGIO.H        SG portable interface for use with IOTester (and target)
   SGTYPES.H     SG portable fixed sized types.
   IOTESTER.H    Prototypes for IOTester access interface
   IOTREG.H      Definition of internal IOTester registers and basic access types
   IOTESTER.C    I/O access interface for IOTester.
                 Add this file to the compilation project


