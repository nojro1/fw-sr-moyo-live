/*---------------------------------------------------------------------------*
 * Generic template for sgio.h for use with the RAMTEX IOTESTER tool
 *
 * The purpose with the SG headers is to assure that the final code can be
 * compiled with both PC compilers and compilers for the target processor.
 *
 * This header maps the portable SG syntax to the corresponding IOTESTER
 * access functions. This header is specific for the IOTESTER tool when
 * external peripheral I/O hardware is operated directly from an embedded
 * program executing on the PC.
 *
 * NOTE :  A call of the function :  iotester_init(0)  or  sginit()
 *         MUST be done before any of other SG function is called.
 *
 * This header file contains:
 * -    Definitions of the portable SG I/O function syntax.
 * -    Includes I/O register definitions for target in SGIOPCTA.H or SGIO_TA.H
 * -    Includes fixed integer size definitions from SGTYPES.H
 *
 * Any taget compiler using memory mapped I/O can also use this SG header file
 * directly
 *
 * Copyright (c) RAMTEX International 2004
 * Version 1.0
 *
 * A description of the SG syntax can be found at:
 *     www.ramtex.dk/standard/sgsyntax.htm
 *
 *--------------------------------------------------------------------------*/
#ifndef SGIO_H
#define SGIO_H

#include <sgtypes.h>

#ifdef  SGPCMODE
   /* This part are used when compiling with PC compilers.
      Ignore this part if you only use the target C compiler.
      These prototype definitions should never need modifications. */

   /* Define the iotester function */
   #include <iotester.h>

   #ifdef __cplusplus
   extern "C" {
   #endif

   /* Map standard SG functions to the IOTESTER functions */
   /* byte access to target CPU : */
   #define sgwrby( PADDR, PVAL )  iot_wr((PADDR),(SGULONG)(PVAL))
   #define sgrdby( PADDR )        (SGUCHAR) iot_rd((PADDR))
   #define sgorby( PADDR, PVAL )  iot_or((PADDR),(SGULONG)(PVAL))
   #define sgandby( PADDR, PVAL ) iot_and((PADDR),(SGULONG)(PVAL))

   /* word access to target CPU : */
   #define sgwrwo( PADDR, PVAL )  iot_wr((PADDR),(SGULONG)(PVAL))
   #define sgrdwo( PADDR )        (SGUINT) iot_rd((PADDR))
   #define sgorwo( PADDR, PVAL )  iot_or((PADDR),(SGULONG)(PVAL))
   #define sgandwo( PADDR, PVAL ) iot_and((PADDR),(SGULONG)(PVAL))

   /* double word access (long word) to target CPU : */
   #define sgwrdw( PADDR, PVAL )  iot_wr((PADDR), (SGULONG)(PVAL))
   #define sgrddw( PADDR )        (SGULONG) iot_rd((PADDR))
   #define sgordw( PADDR, PVAL )  iot_or( (PADDR),(SGULONG)(PVAL))
   #define sganddw( PADDR, PVAL ) iot_and((PADDR),(SGULONG)(PVAL))

   /* byte access to buffer array in target memory */
   #define sgwrbuf( PADDR, POFF, PVAL )  iot_wrbuf((PADDR),(SGULONG)(POFF),(SGULONG)(PVAL))
   #define sgrdbuf( PADDR, POFF )        (SGUCHAR) iot_rdbuf((PADDR), (SGULONG)(POFF))
   #define sgorbuf( PADDR, POFF, PVAL )  iot_orbuf((PADDR), (SGULONG)(POFF),(SGULONG)(PVAL))
   #define sgandbuf( PADDR, POFF, PVAL ) iot_andbuf((PADDR),(SGULONG)(POFF),(SGULONG)(PVAL))

   #define sginit() iot_init(IOT_LPTSEEK)
   #define sgexit() iot_exit()

   #include <sgiopcta.h>

   #ifdef __cplusplus
      }
   #endif

#else
   /* This part is used with the target C compiler.

      Macro definitions for SG I/O functions are used when compiling with
      the target compiler.

      Modify them to fit the syntax for I/O access used by the target C
      compiler if necessary.

      The purpose of these macro functions is to map between the
      "standardized" SG syntax used by portable libraries and the
      non-standardized syntax used by the target compiler.

      The implementation below just assumes that the target compiler uses a
      simple assignment syntax for access operations on I/O registers.
      (i.e. just like memory mapped I/O).
   */

   /* byte access to target CPU : */
   #define sgwrby( PADDR, PVAL ) ( PADDR = ( PVAL ))
   #define sgrdby( PADDR ) (( PADDR ))
   #define sgorby( PADDR, PVAL ) ( PADDR |= ( PVAL ))
   #define sgandby( PADDR, PVAL ) ( PADDR &= ( PVAL ))

   /* word access to target CPU : */
   #define sgwrwo( PADDR, PVAL ) ( PADDR = ( PVAL ))
   #define sgrdwo( PADDR ) (( PADDR ))
   #define sgorwo( PADDR, PVAL ) ( PADDR |= ( PVAL ))
   #define sgandwo( PADDR, PVAL ) ( PADDR &= ( PVAL ))

   /* double word access (long word) to target CPU : */
   #define sgwrdw( PADDR, PVAL ) ( PADDR = ( PVAL ))
   #define sgrddw( PADDR ) (( PADDR ))
   #define sgordw( PADDR, PVAL ) ( PADDR |= ( PVAL ))
   #define sganddw( PADDR, PVAL ) ( PADDR &= ( PVAL ))

   /* byte access to buffer array in target memory */
   /* access to target CPU buffer: */
   #define sgwrbuf( PADDR, PINDEX, PVAL )  ((* ((SGUCHAR volatile *)( PADDR + (PINDEX)))) =  ( PVAL ))
   #define sgrdbuf( PADDR, PINDEX )         (* ((SGUCHAR volatile *)( PADDR + (PINDEX))))
   #define sgorbuf( PADDR, PINDEX, PVAL )  ((* ((SGUCHAR volatile *)( PADDR + (PINDEX)))) |= ( PVAL ))
   #define sgandbuf( PADDR, PINDEX, PVAL ) ((* ((SGUCHAR volatile *)( PADDR + (PINDEX)))) &= ( PVAL ))

   /* Include definitions of the symbolic names and locations
      for I/O registers in target.
      sgio_ta.h must use the syntax for I/O register definitions
      required by the target compiler.
      (If the target compiler have already defined I/O registers
      for the target processor in a header file, it may be enough just
      to copy this file to sgio_ta.h and then add any I/O register
      definitions used by the portable I/O library.)
   */
   #include <sgio_ta.h>

   #define sginit() { }
   #define sgexit() { }
#endif

#endif /* SGIO_H */
/* eof sgio.h */
