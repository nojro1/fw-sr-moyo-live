#include <wnpopup.h>
/*
   Scroll area horizontally by use of a temporary graphic buffer.
   Delta > 1 move existing screen picture to the left.
   Delta < 1 move existing screen picture to the right
*/
void scroll_hor(GXT xb, GYT yb, GXT xe, GYT ye, SGINT delta, SGUCHAR *buf)
   {
   GXT x0,xf;
   GYT y0,yf;
   if ((delta != 0) && (buf != NULL))
      {
      #ifdef GBUFFER
      GUPDATE oldupd;
      oldupd = gsetupdate(GUPDATE_OFF);
      #endif
      /* Assure absolute coordinates */
      ggetvp(&x0,&y0,&xf,&yf);
      gresetposvp();  /* reset vp so gfillvp can be used here */
      xb += x0;
      yb += y0;
      xe += x0;
      ye += y0;
      /* scroll or just fill ? */
      if ( (xe-xb) < abs(delta) )
         /* Clear whole area */
         gfillvp(xb,yb,xe,ye,0);
      else
         {
         /* Copy area to be preserved to buffer (assume it is large enough)*/
         ghw_rdblk(((delta > 0) ? xb+abs(delta) : xb), yb,
            ((delta > 0) ? xe : xe-abs(delta)), ye, buf, ghw_blksize(xb,yb,xe,ye));
         /* Write back preserved area to new position */
         ghw_wrblk(((delta > 0) ? xb : xb + abs(delta)), yb,
                   ((delta > 0) ? xe-abs(delta) : xe), ye, buf);
         /* Clear difference area */
         gfillvp(((delta > 0) ? xe-abs(delta)+1 : xb),yb,
                 ((delta > 0) ? xe : xb+abs(delta)-1),ye,0);
         }
      /* Restore vp coordinates */
      gsetvp(x0,y0,xf,yf);
      gsetupdate(oldupd);
      }
   }


/*
   Scroll viewport out horizontally (simulate slide changing effect)
   yb,ye defines the vertical position of the current viewport to be scrolled
   out horizontally.
   Delta > 1 scroll existing screen picture out to the left.
   Delta < 1 scroll existing screen picture to the right
   If *delay() is != NULL the delay function is called between each step
*/
void scroll_out(GYT yb, GYT ye, SGINT delta, void (*delay)(void))
   {
   GXT xb,xe,x;
   GYT y0,yf;
   SGUCHAR *buf;
   ggetvp(&xb,&y0,&xe,&yf);
   /* Assure absolute coordinates */
   yb += y0;
   ye += y0;
   gresetposvp();  /* reset vp to absolute coordinates  */
   /* Get optimized buffer */
   buf = (SGUCHAR *) wn_malloc( ghw_blksize(xb,yb,xe,ye) );
   for (x = 0; x <= (xe-xb); x++)
      {
      scroll_hor(xb, yb, xe, ye, (delta > 0) ? 1 : -1, buf);
      if (delay != NULL)
         (*delay)();
      }
   wn_free(buf);
   gsetvp(xb,y0,xe,yf);
   }

/*
   Slide symbol in from the right until it is centered horizontally in
   the viewport. y defines the upper edge of the symbol.

   If *delay() is != NULL the delay function is called between each step

   This implementation takes advantage of that symbols are automatically
   cropped at viewport edges.
*/
void slide_sym_in(GYT y, PGSYMBOL ps, void (*delay)(void))
   {
   GXT x, wvp, sw, xend;
   if (ps == NULL)
      return;
   wvp = ggetvpw();
   sw = gsymw(ps);
   if (sw >= wvp)
      xend = 0;          /* Stop at viewport left edge */
   else
      xend = (wvp-sw)/2; /* Stop when symbol is centred */
   wvp--;
   for(x = wvp;;x--)
      {
      gsetupdate(GUPDATE_OFF);
      gputsym(x,y,ps);
      if (x+sw < wvp)
         {
         /* Clear trailing column */
         gfillvp(x+sw,y,wvp,y+(gsymh(ps)-1),0);
         wvp--;
         }
      gsetupdate(GUPDATE_ON);
      if (delay != NULL)
         (*delay)();
      /* Test at end */
      if (x <= xend)
         break;
      }
   }

