#include <wnpopup.h>
/*
   Output a string containing a list of lines to the current viewport
   The list is starting from linenum.
   The viewport is cleared in advance
   Use delayed update if buffered mode is enabled.
*/
void gputlist(const char *str, SGUINT firstline)
   {
   #ifdef GBUFFER
   GUPDATE old;
   old = gsetupdate(GUPDATE_OFF);  /* Activate delayed update */
   #endif

   /* Turn off auto-scroll and add line-cut so we have full control over the lines */
   gsetmode(ggetmode() | (GNOSCROLL|GLINECUT));
   gclrvp();                       /* Clear viewport */
   gputs(gplinep( str, firstline));/* Output string segments */

   #ifdef GBUFFER
   gsetupdate(old);                /* Restore old delayed update state (show if top level) */
   #endif
   }

void gputlist_vp(SGUCHAR vp, const char *str, SGUINT firstline)
   {
   SGUCHAR oldvp;
   oldvp = gselvp(vp);
   gputlist(str, firstline);
   gselvp(oldvp);
   }

