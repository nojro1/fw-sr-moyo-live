/************************* MENU FUNCTIONS ******************************

   MENU GUI DESIGN EXAMPLE
   -----------------------
   This module contains general functions for:
   - Creating framed viewports used for popup windows, top menus or pull
     down menus using a user defined "style".
   - Report functions for easy alignment of framed viewports next to
     each others.
   - Functions for handling block cursors in vertical or horizontal menus,
   - Functions for easy management of text in menus.

   These functions demonstrates how to implement a minimum menu system.
   - They does not require use of malloc() or RAM buffers to operate.
   - All menu text strings can located in ROM only.
   - Menu texts is simply strings where each line is separated with
     \n characters. No array of pointers to text strings is needed.

   The implementation is therefore optimized for used with small embedded
   systems.

   In this implementation the frame "look" is defined by using a "style"
   descriptor or modifying the default frame "style" in advance.

   As default this popup window implementation does not use any buffers to
   save background information. If the window are going to be "closed" again
   it must have be created on a neutral background defined by background
   pattern in the style setup.

   If GEXTMODE is defined in gdispcfg.h then a pointer (for background
   saving) is added to each viewport (via gvpapp.h). This enables dynamic
   save of the background information before a window frame is opened if
   the FR_BUFFER frame style is defined. This implementation uses malloc() to
   create a buffer of an optimized size and free() when the bacground is
   restored.

   Copyright (C) Ramtex Engineering 2004-2010

***********************************************************************/

#include <wnpopup.h>

/******** WINDOW STYLE HANDLING **********/

/* Default style and font */
#if ((GDISPH > 200) && (GDISPW > 300))
  #define DEFFONT &narrow20
#elif (GDISPH <= 64)
  #define DEFFONT &ms58p
#else
  #define DEFFONT &narrow10
#endif

static GSTYLE wndef_style =
   {DEFFONT, 0, GNORMAL,
   2, FR_W1B1|FR_FILL, FR_NONE,
   #ifdef GEXTMODE
   0,
   #endif
   1,0,1,0};
/* Current style pointer */
static PGSTYLE wncursp = &wndef_style;

/* Current screen background */
static GCOLOR background_colorf;
static GCOLOR background_colorb;
static SGUINT background_pattern;

/* Menu limits common for wnmenu.c & mnu_list.c. Placed here for max reuseability */
GYT upperlimit = 0;
GYT lowerlimit = GDISPH-1;


/*
   Initiate a style structure while doing some basic validation
   The new style become the default style.
   Return the old style pointer.
*/
PGSTYLE wnset_style(PGSTYLE sp, PGFONT font, SGUINT background,
          GMODE mode, GFRAMESTYLE frame, GFRAMESTYLE cursor, SGUCHAR shadow,
          GXT xloffset,GYT ytoffset,GXT xroffset,GYT yboffset)
   {
   PGSTYLE oldsp;
   oldsp = wncursp;                            /* Save old style */
   wncursp = (sp == NULL) ? &wndef_style : sp; /* Update default style */

   /* Update style parameters while doing some basic validation */
   wncursp->font = (font == NULL) ? DEFFONT : font;
   wncursp->background = background;
   wncursp->mode = mode;
   wncursp->cursor_style = cursor;
   #ifdef GEXTMODE
   wncursp->bufferflg = ((frame & FR_BUFFER) != 0) ? 1 : 0;
   #endif
   wncursp->frame = (frame & FR_FILL);
   frame &= GFRAMESTYLE_MASK;
   wncursp->frame |= ((frame >= FR_LAST) ? FR_NONE : frame) ;
   GLIMITU(shadow,15);
   wncursp->shadow = shadow;

   /* Force reasonable offset values */
   GLIMITU(xloffset,GDISPW-1);
   GLIMITU(xroffset,(GDISPW-1)-xloffset);
   GLIMITU(ytoffset,GDISPH-1);
   GLIMITU(yboffset,(GDISPH-1)-yboffset);
   wncursp->xloffset = xloffset;
   wncursp->xroffset = xroffset;
   wncursp->ytoffset = ytoffset;
   wncursp->yboffset = yboffset;
   return oldsp;
   }

/*
   Select the current style used for the viewport windows functions
   Return the old style pointer.
*/
PGSTYLE wnsel_style(PGSTYLE sp)
   {
   PGSTYLE oldsp = wncursp;
   wncursp = (sp == NULL) ? &wndef_style : sp;
   return oldsp;
   }

/*
   Return the current style pointer.
*/
PGSTYLE wnget_style(void)
   {
   if (wncursp == NULL)
      wncursp = &wndef_style;  /* Update default style */
   return wncursp;
   }

/********** FRAME SIZE INFO ************/

/* Report frame thickness for left frame edge*/
GXT wnfrxl( void )
   {
   if (wncursp == NULL)
      wncursp = &wndef_style;
   return (wncursp->xloffset + wnframe_thicknes(wncursp->frame));
   }

/* Report frame thickness for right frame edge*/
GXT wnfrxr( void )
   {
   if (wncursp == NULL)
      wncursp = &wndef_style;
   return (wncursp->xroffset + wnframe_thicknes(wncursp->frame));
   }

/* Report frame thickness for top frame edge */
GYT wnfryt( void )
   {
   if (wncursp == NULL)
      wncursp = &wndef_style;
   return (wncursp->ytoffset + wnframe_thicknes(wncursp->frame));
   }

/* Report frame thickness for bottom frame edge */
GYT wnfryb( void )
   {
   if (wncursp == NULL)
      wncursp = &wndef_style;
   return (wncursp->yboffset + wnframe_thicknes(wncursp->frame));
   }

/********** WINDOW CREATION AND CLOSE ************/

/*
   Create a window viewport using absolute coordinates
   The coordinates are the absolute coordinates for the inner
   viewport (the canvas)
   If scrollb != 0 then a vertical scrollbar is added.

   Use default style
*/
void wnvpcreate( SGUCHAR vp, GXT ltx, GYT lty, GXT rbx, GYT rby)
   {
   /* Select window style */
   SGUCHAR oldvp, shadow;
   GXT xb,xe;
   GYT yb,ye;
   SGUCHAR fs;
   #ifdef GBUFFER
   GUPDATE oldupd;  /* Only need the variable if buffer mode is defined */
   #endif
   #ifdef GEXTMODE
   /* Automatic buffer allocation used */
   PGVPAPP app;     /* Pointer to application specific part of viewport */
                    /* In this application used for pointer to background storage */
   #endif

   if (wncursp == NULL)
      wncursp = &wndef_style;

   if (vp > GNUMVP)
      return; /* Illegal viewport number */

   /* Get frame thicknesses for current style */
   fs = wnframe_thicknes(wncursp->frame);
   xb = fs+wncursp->xloffset;
   xe = fs+wncursp->xroffset;
   yb = fs+wncursp->ytoffset;
   ye = fs+wncursp->yboffset;
   shadow = wncursp->shadow;

   /* Force reasonable x,y values */
   GLIMITU(rbx,(GDISPW-1)-(xe+shadow));
   GLIMITU(rby,(GDISPH-1)-(ye+shadow));
   GLIMITD(rbx,xb);
   GLIMITD(rby,ye);
   GLIMITU(ltx,rbx-xb);
   GLIMITU(lty,rby-yb);

   /* Calculate outer frame coordinates */
   xb = ltx-xb;
   xe = rbx+xe;
   yb = lty-yb;
   ye = rby+ye;

   oldvp = gselvp(vp);
   gresetvp();              /* Absolute coordinates */

   #ifdef GBUFFER
   oldupd = gsetupdate(0);   /* Delayed update */
   #endif

   #ifdef GEXTMODE
   /* Support for background buffer */
   app = ggetapp();  /* Initialize pointer to background data stored in viewport */
   if (wncursp->bufferflg)
      {
      /* Save screen data under viewport, frame and shadow to buffer */
      GBUFINT size;
      size = (GBUFINT) ghw_blksize(xb,yb,xe+shadow,ye+shadow);
      if ((app->buf = (void *) wn_malloc(size)) != NULL)
         ghw_rdblk(xb,yb,xe+shadow,ye+shadow, (SGUCHAR *)app->buf,size);
      }
   else
      app->buf = NULL;
   #endif


   /* Add shadow  (using reset default black on white) */
   while (shadow != 0)
      {
      gmoveto( xb+shadow,ye+shadow);
      glineto( xe+shadow,ye+shadow);
      glineto( xe+shadow,yb+shadow);
      shadow--;
      }

   gsetmode(wncursp->mode); /* Init mode for GNORMAL / GINVERSE fill and draw */
   gselfont(wncursp->font); /* Set font (assures correct cursor calculations) */
   wnframe(xb,yb,xe,ye,wncursp->frame );          /* Add frame according to style */

   if (wncursp->frame & FR_FILL)
      { /* Use frame type defined colors for menu text and background */
      gsetcolorf(wnframe_colorf(wncursp->frame));    /* Set viewport color according to style */
      gsetcolorb(wnframe_colorb(wncursp->frame));
      }
   else
      { /* Use current colors to clear margen and menu area */
      gfillvp(xb+fs,yb+fs,xe-fs,ye-fs,0);
      }

   /* Narrow to inside viewport and set defaults */
   gsetvp(ltx,lty,rbx,rby);
   gselvp(oldvp);       /* Restore vp */
   #ifdef GBUFFER
   gsetupdate(oldupd);  /* Restore delayed update */
   #endif
   }

void wn_background_init(GCOLOR fore, GCOLOR back, SGUINT fillpattern)
   {
   GMODE mode;
   background_colorf = fore;
   background_colorb = back;
   background_pattern = fillpattern;
   gsetcolorb(background_colorb);
   gsetcolorf(background_colorf);
   mode = ggetmode();
   gsetmode( mode & ~GINVERSE);
   gfillvp(0,0,GDISPW-1,GDISPH-1,background_pattern);
   gsetmode( mode );
   }

/*
   "Close" a viewport

   If GEXTMODE is defined in gdispcfg.h and the window was opened with FR_BUFFER
   mode selected then the background information is just restored from a buffer.

   If a background buffer is not used a viewport is "closed" by
   overwriting the viewport and decoration area using the background pattern
   defined by the style. When closing a window the style setting should be the
   same as when the window was opened.
*/
void wnvpclose( SGUCHAR vp)
   {
   SGUCHAR oldvp;
   #ifdef GEXTMODE
   /* Automatic buffer allocation used */
   PGVPAPP app;
   #endif

   if (wncursp == NULL)
      wncursp = &wndef_style;

   if (vp > GNUMVP)
      return; /* Illegal viewport number */

   #ifdef GEXTMODE
   /* Support for background buffer */
   app = ggetapp_vp(vp);
   if (app->buf != NULL)
      {
      ghw_restoreblk((SGUCHAR *)app->buf);  /* Size is stored in buffer, just restore */
      wn_free(app->buf);
      app->buf = NULL;
      return;                     /* Used buffer restore closing mode */
      }
   #endif

   /* Use overwrite closing mode */
   oldvp = gselvp(vp);

   /* update viewport size to outer frame */
   gsetvp(wnxl(vp),
          wnyt(vp),
          wnxr(vp)+wncursp->shadow,
          wnyb(vp)+wncursp->shadow);

   /* Fill background to clear area. The fill pattern must match the */
   /* background setting and not the current window setting, so we clear */
   /* here first any GINVERSE mode setting in the current window */
   gsetmode(ggetmode_vp(vp) & ~GINVERSE);
   gsetcolorb(background_colorb);
   gsetcolorf(background_colorf);
   gfillvp(0,0,ggetvpw()-1,ggetvph()-1, background_pattern);

   gselvp(oldvp);     /* Restore vp */
   }

/*
   Reopen a window based on the stored viewport size and the current
   default style. The canvas is cleared after reopen.
*/
void wnreopen( SGUCHAR vp )
   {
   wnvpcreate( vp, gvpxl(vp), gvpyt(vp), gvpxr(vp), gvpyb(vp));
   }

/*
   Create a viewport window
   The x,y coordinates are the absolute coordinates for the upperleft
   corner of the frame.
   The size of the frame is adjusted according to the style so
   the inner wiewport is width * height

   Use default style
*/
void wnvpcreate_pos( SGUCHAR vp, GXT x, GYT y, GXT width, GYT height)
   {
   SGUCHAR ft;
   GXT sw;
   GYT sh;
   if (wncursp == NULL)
      wncursp = &wndef_style;

   ft = wnframe_thicknes(wncursp->frame);

   /* Size of screen - decorations */
   sw  = (GDISPW-1)-(wncursp->xroffset + wncursp->xloffset + 2*ft + wncursp->shadow);
   sh  = (GDISPH-1)-(wncursp->yboffset + wncursp->ytoffset + 2*ft + wncursp->shadow);

   /* Force reasonable x,y values */
   GLIMITU(x, sw);         /* assure room for bottom, right frame */
   GLIMITU(y, sh);
   GLIMITU(width, sw-x+1); /* Limit viewport size to fit within screen */
   GLIMITU(height,sw-y+1);
   if ((height == 0) || (width == 0))
      return;  /* Some illegal parameter settings detected */
   /* Set x,y to fit top,left frame in place during creation */
   x += wncursp->xloffset+ft;
   y += wncursp->ytoffset+ft;
   wnvpcreate( vp, x, y, x+width-1, y+height-1);
   }

/*
   Create a "menu" viewport which fits tightly around a text string.
   The text string can be formatted with \n characters. The text must
   fit within the screen area.
   Using default style
*/
void wnmenu( SGUCHAR vp, GXT x, GYT y, const char *str )
   {
   SGUCHAR oldvp;
   SGUINT th;
   #ifdef GBUFFER
   GUPDATE oldupd;  /* Only need the variable if buffer mode is defined */
   #endif

   if (str == NULL)
      return;
   if (*str == 0)
      return;

   #ifdef GBUFFER
   oldupd = gsetupdate(0); /* Delayed update */
   #endif

   th = gpstrheight(str);
   oldvp = gselvp(vp);
   gselfont(wncursp->font); /* Initialize style font in advance to calculate size */

   wnvpcreate_pos( vp, x, y, (GXT) gpstrwidth(str), (GYT) gpstrheight(str));
   gputs(str);
   gselvp(oldvp);       /* Restore vp */

   #ifdef GBUFFER
   gsetupdate(oldupd);     /* Restore delayed update */
   #endif
   }

/*
   Create a "menu" viewport which fits tightly around a text string
   and is centered on the screen
   Use default style
*/
void wnmenu_center( SGUCHAR vp, const char *str )
   {
   GXT x,w;
   GYT y,h;
   SGUCHAR oldvp;
   #ifdef GBUFFER
   GUPDATE oldupd;  /* Only needed if buffer mode is defined */
   #endif
   if (str == NULL)
      return;
   if (*str == 0)
      return;

   #ifdef GBUFFER
   oldupd = gsetupdate(0); /* Delayed update */
   #endif

   /* Center inner viewport */
   oldvp = gselvp(vp);
   gselfont(wncursp->font); /* Initialize style font in advance to calculate size */
   h = (GYT) gpstrheight(str);
   w = (GXT) gpstrwidth(str);
   x = (GDISPW-w)/2-1;
   y = (GDISPH-h)/2-1;

   wnvpcreate( vp, x, y, x+w, y+h);
   gputs(str);
   gselvp(oldvp);       /* Restore vp */

   #ifdef GBUFFER
   gsetupdate(oldupd);     /* Restore delayed update */
   #endif
   }


/********** WINDOW POSITION REPORTING ************/

/* Report coordinate for left frame edge*/
GXT wnxl( SGUCHAR vp)
   {
   return gvpxl(vp)-wnfrxl();
   }

/* Report coordinate for right frame edge*/
GXT wnxr( SGUCHAR vp)
   {
   return gvpxr(vp)+wnfrxr();
   }

/* Report coordinate for top frame edge */
GYT wnyt( SGUCHAR vp)
   {
   return gvpyt(vp)-wnfryt();
   }

/* Report coordinate for bottom frame edge */
GYT wnyb( SGUCHAR vp)
   {
   return gvpyb(vp)+wnfryb();
   }


/******* VIEWPORT MENU CURSOR FUNCTIONS *******/

/*
   Set cursor in a horizontal menu as a frame around the menu word at index.

   If the frame style is FR_NONE then toggle a block cursor which covers
   the menu word. The current foreground back ground colors are used.

   The cursor size is adjusted to the size of the word.
   index = 0 toggle the cursor over the first word.
*/
void wntopmenu_cursor(SGUCHAR vp, SGUCHAR index, const char *menustr, GFRAMESTYLE fs)
   {
   SGUCHAR oldvp;
   SGUINT cstart;
   GXT xb,xe,cb,ce;
   GYT yb,ye;
   SGUCHAR fthickness;

   if (wncursp == NULL)
      wncursp = &wndef_style;

   oldvp = gselvp(vp); /* Make the viewport current */
   /* find word positions in the menu string */
   cb = (GXT) gpwordpos(menustr,index,GPWORD_BEGIN,&cstart);     /* Position for word start */
   ce = (GXT) gpwordpos(&menustr[cstart],0,GPWORD_END,NULL) + cb;/* Position for word end */

   /* Use absolute coordinates to include frame offsets */
   ggetvp(&xb,&yb,&xe,&ye);
   gresetposvp();
   if (fs != FR_NONE)
      {
      /* Set frame cursor */
      GMODE mode = ggetmode_vp(vp);        /* Get current GNORMAL / GINVERSE mode */
      /* Use lower left corner to check if a frame is  already drawn */
      if (ggetpixel(xb + cb - wncursp->xloffset, ye + wncursp->yboffset))
         gsetmode( mode ^ GINVERSE); /* Frame was drawn, draw "white" to clear */

      /* Draw frame, include x offsets and top offset to */
      /* compensate for the normal font design style where */
      /* character symbols is left-top aligned */
      wnframe(xb + cb - wncursp->xloffset,
              yb - wncursp->ytoffset ,
              xb + ce + wncursp->xroffset ,
              ye + wncursp->yboffset ,
              fs);
      gsetmode(mode); /* Restore mode setting */
      }
   else
      {
      /* Toggle block cursor */
      /* Invert block, include x offsets and top offset to */
      /* to compensate for the normal font design style where */
      /* character symbols is left-top aligned */
      fthickness = wnframe_thicknes(wncursp->frame);
      ginvertvp(xb + cb - wncursp->xloffset + fthickness,
                yb - wncursp->ytoffset + fthickness,
                xb + ce + wncursp->xroffset - fthickness,
                ye + wncursp->yboffset + fthickness);
      }
   /* Restore settings */
   gsetvp(xb,yb,xe,ye);
   gselvp(oldvp);
   }

/*
   Toggle the block cursor in a vertical menu as a line which fill
   the whole viewport width
   The cursor can be a rectangle cursor or a block cursor.
   The cursor size is adjusted according to the size of the viewport
   and the font height. index = 0 toggle the cursor over the first (top)
   line in the viewport.
*/
void wnmenu_cursor(SGUCHAR vp, SGUCHAR index, GFRAMESTYLE fs)
   {
   SGUCHAR oldvp;
   GXT xb,xe;
   GYT yb,ye;
   SGUCHAR fthickness;
   if (wncursp == NULL)
      wncursp = &wndef_style;

   oldvp = gselvp(vp);
   /* Use absolute coordinates to include frame xoffsets */
   ggetvp(&xb,&yb,&xe,&ye);
   gresetposvp();

   /* Check for index overflow */
   if (index*ggetfh() < ((ye-yb)+1))
      {
      if (fs != FR_NONE)
         {
         /* Rectangle cursor */
         GMODE mode = ggetmode_vp(vp);        /* Get current GNORMAL / GINVERSE mode */
         /* Use lower left corner to check if a frame is already drawn */
         if (ggetpixel(xb - wncursp->xloffset, yb + (index + 1) * ggetfh()))
            gsetmode( mode ^ GINVERSE); /* Frame was drawn, draw "white" to clear */

         /* Draw rectangle, include x offsets and top offset to */
         /* compensate for the normal font design style where */
         /* character symbols is left-top aligned */
         wnframe(xb - wncursp->xloffset,
                 yb + index * ggetfh()-1,
                 xe + wncursp->xroffset,
                 yb + (index + 1) * ggetfh()-1,
                 fs);
         gsetmode(mode); /* Restore mode setting */
         }
      else
         {
         /* Block cursor */
         /* Invert block, include x offsets and top offset to */
         /* to compensate for the normal font design style where */
         /* character symbols is left-top aligned */
         fthickness = wnframe_thicknes(wncursp->frame);
         ginvertvp(xb - wncursp->xloffset + fthickness,
                   yb + index * ggetfh() + fthickness,
                   xe + wncursp->xroffset - fthickness,
                   yb + (index + 1) * ggetfh() - fthickness);
         }
      }
   /* Restore settings */
   gsetvp(xb,yb,xe,ye);
   gselvp(oldvp);
   }

/*
   Change the cursor style for the current menu style
   Return the old cursor style.
   Style can be FR_CURSOR_RECTANGLE or FR_CURSOR_BLOCK
*/
GFRAMESTYLE wnmenu_cursor_style(GFRAMESTYLE style)
   {
   GFRAMESTYLE fs;
   if (wncursp == NULL)
      wncursp = &wndef_style;
   fs = wncursp->cursor_style;
   wncursp->cursor_style = style;
   return fs;
   }

/*
   Output a sting in a named view port on a new line (if it is not
   the first parameter in the viewport)
   Leave the current viewport unchanged
*/
void gputsvpln(SGUCHAR vp, const char *str)
   {
   SGUCHAR ovp = gselvp(vp);     /* Select viewport */
   /* If not first parameter in viewport, print new line */
   if (ggetypos() >= ggetfh()-1)
      gputs("\n");
   gputs((char *) str);     /* Output text */
   gselvp(ovp);
   }

/*
   Clear a text line in a viewport
   (any cursors should have been removed first)
*/
void gclrvpln(SGUCHAR vp, SGUCHAR line)
   {
   SGUCHAR ovp = gselvp(vp);     /* Select viewport */
   GYT yb,ye;
   yb = ggetfh()*line;
   ye = ggetfh()*(line+1)-1;
   gfillvp(0,yb,ggetvpw()-1,ye,0);
   gselvp(ovp);
   }


