/****************************** timer.h ***************************
*
*  General timer and timeout function prototypes
*
*  These functions are OS system and processor independent.
*
*  Created by: RAMTEX Engineering ApS 1999 JK
*
*********************************************************************/

#ifndef TIMER_H
#define TIMER_H

#ifdef __cplusplus
extern "C" {
#endif

/* timdelay.c */
void timedelay(unsigned int time);

/* timeout.c */
typedef struct 
   {
   unsigned long start_time;
   unsigned int wait_time;
   } TIMEOUT;

void timeout_set( TIMEOUT *time, unsigned int wait_time );
int timeout( TIMEOUT *time );

/* tsdiff.c */
unsigned long int tsdiff(unsigned long newtime, unsigned long oldtime);

/* timstamp.c */
void timer_init(void);

#ifdef FASTTIMESTP
extern unsigned long saruntime;
#define timestamp() (saruntime)
#else
extern unsigned long timestamp(void);
#endif


#ifdef __cplusplus
}
#endif

#endif
