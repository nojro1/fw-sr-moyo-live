/*
   Time stamp interface function

   Is here based on 10ms interrupt clocks and the saruntime counter

   Returns the time counter value as an unsigned long
*/

#include <timer.h>

#include <sgtypes.h> /* SGPCMODE switch */

#ifdef SGPCMODE
  #include <time.h>
#else
 /* Global value, is assumed to defined and incremented in, for instance,
   the timer interrupt function in the target system */
 extern unsigned long runtime;
#endif

unsigned long timestamp(void)
   {
   #ifdef SGPCMODE
   /* PC simulation code */
   return (((unsigned long) clock())*100L)/CLOCKS_PER_SEC;

   #else
   /* Targe system code */
   unsigned long tmptime;

   /* Optionally insert (timer) interrupt disable here */
   tmptime = runtime;
   /* Optionally insert (timer) interrupt enable here */
   return tmptime;

   #endif
   }

