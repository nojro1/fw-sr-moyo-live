/*
   Create frames using different "3 dimensional" styles
   or variations of simple rectangle styles.

   Depending on the style the frame will look like :
     plates, inserts, raised contour frames or lines,
     lowered contour rames or lines

   Can frame popup menus, menubars, cursors, buttons etc.

   The coordinates are relative to the current viewport
*/

#include <wnpopup.h>

/* The "3 dimensional" style uses 7 intensity levels with a
   light intensity centered relative to the neutral background level
   The levels are here mapped to the actual color
*/
#ifdef G_IS_BW
#define  WN_L3  1
#define  WN_L2  1
#define  WN_L1  0
#define  WN_N0  1
#define  WN_D1  0
#define  WN_D2  0
#define  WN_D3  0
#else
#define  WN_L3  G_WHITE
#define  WN_L2  G_LLIGHTGREY
#define  WN_L1  G_LIGHTGREY
#define  WN_N0  G_GREY
#define  WN_D1  G_DARKGREY
#define  WN_D2  G_DDARKGREY
#define  WN_D3  G_BLACK
#endif

/*
   Frame style colors.
   GFRAMESTYLE is used as table index. The symbolic names for frame styles are
   defined in wnpopup.h
   The number of structure elements must be equal to FR_LAST
*/
static GCODE GCOLOR gframecol[FR_LAST][4] =
   {
   {0, 0, 0, 0}, /* Dymmy for FR_NONE */

   /* "Flat" frame styles (using current foreground background colors) */
   {1, 0, 0, 0}, /* Thin black line frame */
   {0, 0, 0, 0}, /* Thin white line frame */
   {1, 0, 0, 0}, /* Black outer + white inner line */
   {1, 1, 0, 0}, /* Thick black outer + thin white inner line */
   {1, 0, 1, 0}, /* Double line frame  */
   {1, 1, 0, 1}, /* Thick double line frame */

   {1, 0, 0, 0}, /* Frame dotted */
   {1, 0, 0, 0}, /* Invert block */
   {1, 0, 0, 0}, /* Invert line */

   /* "3D" frame styles */
   {WN_L3, WN_D3, WN_L1, WN_D1}, /* Plate */
   {WN_D3, WN_L3,WN_D1, WN_L2},  /* Insert */
   {WN_D3, WN_D3,WN_D2, WN_D2},  /* Plate (key pressed) */
   {WN_L3, WN_D3, WN_D3, WN_L3}, /* Hill */
   {WN_D3, WN_L3, WN_L3, WN_D3}, /* Vally */
   {WN_L3, WN_D2,WN_N0, WN_N0},  /* Thin plate */
   {WN_D3, WN_L2,WN_N0, WN_N0}   /* Thin insert */
   };

/*
   Frame width in pixels for the above frame styles
   Note :
   The number of elements in this table must match the number of
   structures in gframecol[]
*/
static GCODE SGUCHAR gframethichness[FR_LAST] =
   {0, 1,1,2,3,4,4, 1,1,1, 2,2,2,2,2,2,2};

SGUCHAR wnframe_thicknes(GFRAMESTYLE fs)
   {
   if ((fs &= GFRAMESTYLE_MASK) >= FR_LAST)
      return 0;
   return gframethichness[fs];
   }

/*
  Draw a frame using a frame style.
*/
void wnframe(GXT xb, GYT yb, GXT xe, GYT ye, GFRAMESTYLE fs)
   {
   GCOLOR oldc;
   SGUINT fi, fc, cnt;
   fi = (SGUINT) (fs & GFRAMESTYLE_MASK);
   if ( fs & FR_REMOVE)
      {
      /* Remove an existing frame */
      oldc = gsetcolorf(WN_N0);
      fc = gframethichness[fi];
      while (fc-- > 0)
         {
         grectangle(xb,yb,xe,ye);
         if ( yb < ye )
            yb++;
         if ( yb < ye )
            ye--;
         if ( xb < xe )
            xb++;
         if ( xb < xe )
            xe--;
         }
      gsetcolorf(oldc);
      return;
      }
   switch (fi )
      {
      case FR_DOUBLE1:
      case FR_DOUBLE2:
         fc = 3;
         goto frameloop;
      case FR_W1B1:
         fc = 2;
         goto frameloop;
      case FR_W1B2:
         fc = 1;
         goto frameloop;
      case FR_B1:
      case FR_W1:
         fc = 0;
         /* "Flat" rectangle frames */
         frameloop:
         oldc = gsetcolorf(gframecol[fi][0] ? WN_D3: WN_L3);
         /* Create rectangular frames, from outside in */
         for (cnt = 0;;)
            {
            grectangle(xb++,yb++,xe--,ye--);
            if (cnt >= fc)
               break;
            gsetcolorf(((gframecol[fi][++cnt] != 0)  ? WN_D3: WN_L3));
            }
         if ((fs & FR_FILL) != 0)
            {
            if ((xe >= xb) && (ye >= yb))
               {
               /* Inner area exists */
               #ifdef G_IS_BW
               /* fill it using initial color setting */
               gsetcolorf(oldc);
               gfillvp(xb,yb,xe,ye, 0x0000);
               #else
               /* fill it using color of inner frame */
               gsetcolorf(ggetcolorb());
               gfillvp(xb,yb,xe,ye, 0xffff);
               #endif
               }
            }
         gsetcolorf(oldc);
         break;

      case FR_DOTTED:
         {
         /* Create dotted lines using the current colors */
         if ((xb == xe) || (yb == ye) || ((fs & FR_FILL) != 0))
            {
            /* Single dot line or dotted fill area */
            gfillvp(xb,yb,xe,ye, 0xaa55);
            }
         else
            {
            /* Dot rectangle */
            gfillvp(xb,yb,xe,yb, 0xaa55);
            gfillvp(xb,ye,xe,ye, 0xaa55);
            gfillvp(xb,yb,xb,ye, 0xaa55);
            gfillvp(xe,yb,xe,ye, 0xaa55);
            }
         break;
         }
      case FR_INVERT:
      case FR_INVERT_LINE:
         {
         ginvertvp(xb,yb,xe,ye);
         if (fi == FR_INVERT)
            break;
         ginvertvp(xb+1,yb+1,xe-1,ye-1);
         break;
         }
      case FR_PLATE:
      case FR_INSERT:
      case FR_HILL_LINE:
      case FR_VALLY_LINE:
      case FR_PLATE_THIN:
      case FR_INSERT_THIN:
         {
         /* 3D frames */
         oldc = gsetcolorf(gframecol[fi][0]);
         if (xb == xe)
            {
            /* Vertical line*/
            gmoveto(xb,yb);
            glineto(xe,ye);
            gsetcolorf(gframecol[fi][1]);
            gmoveto(xb+1,yb);
            glineto(xe+1,ye);
            }
         else
         if (yb == ye)
            {
            /* Horizontal line*/
            gmoveto(xb,yb);
            glineto(xe,yb);
            gsetcolorf(gframecol[fi][1]);
            gmoveto(xb,yb+1);
            glineto(xe,ye+1);
            }
         else
            {
            /* Rectangle (outer) */
            gmoveto(xb,ye-1);
            glineto(xb,yb);
            glineto(xe,yb);
            gsetcolorf(gframecol[fi][1]);
            glineto(xe,ye);
            glineto(xb,ye);
            /* Rectangle (inner) */
            gsetcolorf(gframecol[fi][2]);
            gmoveto(xb+1,ye-2);
            glineto(xb+1,yb+1);
            glineto(xe-1,yb+1);
            gsetcolorf(gframecol[fi][3]);
            glineto(xe-1,ye-1);
            glineto(xb+1,ye-1);
            if ((fs & FR_FILL) != 0)
               {
               if ((xe >= xb+4) && (ye >= yb+4))
                  {
                  /* Inner area exists */
                  #ifdef G_IS_BW
                  /* fill it with original color */
                  gsetcolorf(oldc);
                  gfillvp(xb+2,yb+2,xe-2,ye-2, 0);
                  #else
                  /* fill it with user default background or neutral intensity */
                  gsetcolorf((fs & FR_USERCOLOR) ? ggetcolorb() : WN_N0);
                  gfillvp(xb+2,yb+2,xe-2,ye-2, 0xffff);
                  #endif
                  }
               }
            }
         gsetcolorf(oldc);
         break;
         }
      default:
         /* Unknown frame */
         break;
      }
   }

/*
  Return foreground color for 3D frame style
*/
GCOLOR wnframe_colorf(GFRAMESTYLE fs)
   {
   switch ((fs & GFRAMESTYLE_MASK))
      {
      case FR_PLATE:
      case FR_INSERT:
      case FR_HILL_LINE:
      case FR_VALLY_LINE:
      case FR_PLATE_THIN:
      case FR_INSERT_THIN:
      case FR_REMOVE:
         return WN_D3;
      default:
         return ggetcolorf();
      }
   }

/*
  Return background color for 3D frame style
*/
GCOLOR wnframe_colorb(GFRAMESTYLE fs)
   {
   switch ((fs & GFRAMESTYLE_MASK))
      {
      case FR_PLATE:
      case FR_INSERT:
      case FR_HILL_LINE:
      case FR_VALLY_LINE:
      case FR_PLATE_THIN:
      case FR_INSERT_THIN:
      case FR_REMOVE:
         return WN_N0;
      default:
         return ggetcolorb();
      }
   }

/*
   Add frame around an existing vp.
   The vp settings remain unchanged after this call
   If there is not enough screen space for the frame around the viewport
   then the frame is skipped
*/
void wnframe_vp(GFRAMESTYLE fs)
   {
   GXT xb,xe;
   GYT yb,ye;
   SGUCHAR ft;
   if ((ft = wnframe_thicknes(fs)) == 0)
      return; /* No frame */
   ggetvp(&xb,&yb,&xe,&ye);
   if ((xb < ft) ||
      (yb < ft) ||
      (xe >= GDISPW-ft) ||
      (ye >= GDISPH-ft))
      return; /* Not enough space for frame */

   /* Add frame */
   gsetvp(xb-ft,yb-ft,xe+ft,ye+ft);
   wnframe(0,0,ggetvpw()-1,ggetvph()-1,fs);
   gsetvp(xb,yb,xe,ye);
   }

