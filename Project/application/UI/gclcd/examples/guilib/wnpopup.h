#ifndef WNPOPUP_H
#define WNPOPUP_H

#include <gdisp.h>

#ifdef __cplusplus
extern "C" {
#endif

/** Frame functions wnframe.c **/
typedef enum
   {
   FR_NONE = 0,         /* No frame */
   /* "Flat" frames */
   FR_B1,               /* Thin black line frame */
   FR_W1,               /* Thin white line frame */
   FR_W1B1,             /* Black outer + white inner line */
   FR_W1B2,             /* Thich black outer + thin white inner line */
   FR_DOUBLE1,          /* Double line frame */
   FR_DOUBLE2,          /* Thick double line frame */

   /* Dotted frame */
   FR_DOTTED,           /* Dotted rectangle using the current colors */

   /* Inverted block or frame */
   FR_INVERT,
   FR_INVERT_LINE,

   /* "3D" frames */
   FR_PLATE,            /* Plate */
   FR_INSERT,           /* Insert */
   FR_PRESSED,          /* Pressed frame */
   FR_HILL_LINE,        /* Raised frame */
   FR_VALLY_LINE,       /* Lowered frame */
   FR_PLATE_THIN,       /* Thin plate */
   FR_INSERT_THIN,      /* Thin insert */

   FR_LAST,             /* Last frame type value (for limit checking) */

   FR_REMOVE = 0x20,    /* Clear frame flag */
   FR_FILL = 0x40,      /* Fill inner area flag */
   #ifdef GEXTMODE
   FR_BUFFER = 0x80,    /* Use buffer to save background under area defined by window frame. */
                        /* Buffer pointer is saved in extended viewport data */
   #endif
   FR_USERCOLOR = 0x100 /* Fill with users default color settings */
   } GFRAMESTYLE;

#define GFRAMESTYLE_MASK 0x1f
#define GFRAMESTYLE_MASK 0x1f

void wnframe(GXT xb, GYT yb, GXT xe, GYT ye, GFRAMESTYLE fs);
SGUCHAR wnframe_thicknes(GFRAMESTYLE fs);
GCOLOR wnframe_colorf(GFRAMESTYLE fs);
GCOLOR wnframe_colorb(GFRAMESTYLE fs);
void wnframe_vp(GFRAMESTYLE fs);

/* gframedvp.c */
void gframedvp( GXT xb, GYT yb, GXT xe, GYT ye,
      GFRAMESTYLE fs, GCOLOR fore, GCOLOR back, char absolute, char inner);

/* Make switch for color, grey_scale, b&w conditional compilation */
#if (defined(GCOLOR_1) || defined(GCOLOR_2) || defined(GCOLOR_4) || defined(GCOLOR_8) || (GDISPPIXW > 8))
   #define G_IS_COLOR
#else
   #if (GDISPPIXW == 1)
      #define G_IS_BW
   #else
      #define G_IS_GREY_SCALE
   #endif
#endif

/** Menu functions wnpopup.c **/

typedef struct
   {
   PGFONT font;          /* Defalt font */
   SGUINT background;    /* Background pattern for erase */
   GMODE  mode;          /* Alignment modes (0 = GNORMAL) */
   SGUCHAR shadow;       /* Shadow thickness (0 = No shadow)*/
   GFRAMESTYLE frame;     /* One of the frame styles (0 = no frame) */
   GFRAMESTYLE cursor_style;/* Cursor style (0 = block) */
   #ifdef GEXTMODE
   GFRAMESTYLE bufferflg;
   #endif
   GXT xloffset;          /* Offsets from viewport to beginning of frame */
   GXT xroffset;
   GYT ytoffset;
   GYT yboffset;
   }  GSTYLE;

typedef GSTYLE PGENERIC * PGSTYLE;

typedef struct
   {
   GXYT rel_start;   /* cursor start position (0-100)*/
   GXYT rel_endpos;  /* cursor end position   (0-100)*/
   GXT xb;
   GYT yb;
   GXT xe;
   GYT ye;
   } GSCROLLBAR, *PGSCROLLBAR;


/* Window viewport style setting */
PGSTYLE wnset_style(PGSTYLE sp, PGFONT font,SGUINT background,
          GMODE mode, GFRAMESTYLE frame, GFRAMESTYLE fs, SGUCHAR shadow,
          GXT xloffset,GYT ytoffset,GXT xroffset,GYT yboffset);
PGSTYLE wnsel_style(PGSTYLE sp);
PGSTYLE wnget_style(void);

/* Create window viewports and menus */
void wn_background_init(GCOLOR fore, GCOLOR back, SGUINT fillpattern);
void wnvpcreate( SGUCHAR vp, GXT ltx, GYT lty, GXT rbx, GYT rby);
void wnvpcreate_pos( SGUCHAR vp, GXT x, GYT y, GXT width, GYT hight);
void wnmenu( SGUCHAR vp, GXT x, GYT y, const char *str );
void wnmenu_center( SGUCHAR vp, const char *str );

/* Close window viewports and menus */
void wnvpclose( SGUCHAR vp);
void wnreopen( SGUCHAR vp );
#define wnmenu_close(vp) wnvpclose( (vp) )

/* Window frame thickness report functions */
GXT wnfrxl( void );
GXT wnfrxr( void );
GYT wnfryt( void );
GYT wnfryb( void );

/* Outher window frame position report functions */
GXT wnxl( SGUCHAR vp);
GXT wnxr( SGUCHAR vp);
GYT wnyt( SGUCHAR vp);
GYT wnyb( SGUCHAR vp);

void wntopmenu_cursor(SGUCHAR vp, SGUCHAR index, const char *menustr, GFRAMESTYLE fs);
void wnmenu_cursor(SGUCHAR vp, SGUCHAR index, GFRAMESTYLE fs);
GFRAMESTYLE wnmenu_cursor_style(GFRAMESTYLE style);

/** vpalloc.c **/
SGUCHAR wn_get_vpnum(void);
void wn_free_vpnum(SGUCHAR vp);
SGUCHAR wn_vpnum(void);
void wn_vpnuminit(void);

/** wnalloc.c **/
void *wn_malloc(GBUFINT size);
void wn_free(void *buf);
SGUCHAR *vp_malloc(void);
void wn_allocinit(void);

/*********** View port put string functions ***************/
void gputsvpln(SGUCHAR vp, const char *str);
void gclrvpln(SGUCHAR vp, SGUCHAR line);
void gputsclr(const char *str);

/* gpwordp.c */
#define GPWORD_BEGIN 1
#define GPWORD_END   0
SGUINT gpwordpos( const char *str, SGUINT word_num, SGUCHAR wordbegin, SGUINT *char_index);

/* bar.c */
typedef struct _BARPOS
   {
   unsigned int pos;  /* current position */
   GXT xb;            /* Dynamic bar area */
   GYT yb;
   GXT xe;
   GYT ye;
   SGUCHAR scale;
   #ifndef G_IS_BW
   GCOLOR fore;
   GCOLOR back;
   #endif
   } BARPOS;

BARPOS *barh_create(BARPOS *bp, GXT xb, GYT yb, GXT xe, GYT ye, GFRAMESTYLE fs, SGUCHAR scale);
void barh_set(BARPOS *bp, GXT new_value);
void barh_pct(BARPOS *bp, GXT new_value);
BARPOS *barv_create(BARPOS *bp, GXT xb, GYT yb, GXT xe, GYT ye, GFRAMESTYLE fs, SGUCHAR scale);
void barv_set(BARPOS *bp, GYT new_value);
void barv_pct(BARPOS *bp, GYT new_value);


/******* Special menu looks *******/
void wn_popvalue(SGUCHAR vp, GXT x, GYT y, GXT width, GYT height,
   const char *pretext, const char *valuetext, const char *posttext);

#define wn_popvalue_center(vp, width, height, pretext, valuetext, posttext) \
   wn_popvalue((vp), (GDISPW-1), (GDISPH-1), (width), (height), (pretext), (valuetext), (posttext))

/** wnfrsym.c **/
GFRAMESTYLE wnsymframe_style(GFRAMESTYLE fs);
void wnsymframe(GXT x, GYT y, PGSYMBOL ps, SGBOOL updatevp);
SGUCHAR wn_symframe_tickness(void);
SGUINT wnsymframew(PGSYMBOL ps);
SGUINT wnsymframeh(PGSYMBOL ps);

/** gswapcol.c **/
void gswap_color(GXT xs, GYT ys, GXT xe, GYT ye, GCOLOR c1, GCOLOR c2);

/** gtouch.c **/
SGUINT gnormalize( SGUINT touch_x, SGUINT touch_maxx, SGUINT touch_minx, SGUINT pixelmax );
SGBOOL ginsidevp( SGUCHAR vp, GXT x, GYT y);
SGINT gmenuidx( SGUCHAR vp, GXT x, GYT y);
SGINT gtopmenuidx( SGUCHAR vp, const char *menustr, GXT x, GYT y);

/* gtouchkp.c */
unsigned char gtouchkp(unsigned char *edgep, unsigned char *levelp, GXT *xp, GYT *yp);
unsigned char gtouch_click(GXT *xp, GYT *yp);

/** gplinep.c **/
const char *gplinep( const char *str, SGUINT linenum );

/** gplistw.c **/
SGUINT gplistw(PGSTR *strpp, PGFONT pfont);

/** gputlist.c **/
void gputlist(const char *str, SGUINT firstline);
void gputlist_vp(SGUCHAR vp, const char *str, SGUINT firstline);

/* hortxtscroll.c */
SGUCHAR horscroll_start(SGUCHAR viewport, PGCSTR strp);
SGUCHAR horscroll_step(void);

/* scrolhor.c */
void scroll_hor(GXT xb, GYT yb, GXT xe, GYT ye, SGINT delta, SGUCHAR *buf);
void scroll_out(GYT yb, GYT ye, SGINT delta, void (*delay)(void));
void slide_sym_in(GYT y, PGSYMBOL ps, void (*delay)(void));

/* vp key.c */
void key_init_vp(SGUCHAR vp, GXT xb, GYT yb, GXT xe, GYT ye, PGFONT pfont);
void key_press_vp(SGUCHAR vp, SGUCHAR pressed );

#ifdef __cplusplus
}
#endif

#endif
