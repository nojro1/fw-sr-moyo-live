/*
  Functions creating a "slideshow" animation similar to the one 
  used with GIF files.
  All "slide" symbols should have the same size and be located
  together in a font.

     animate_start(..) or animate_center(..) 
       is called when slide show should start
     animate_tick(..) is called when next slide should be shown
       The calling frequency controls the speed of the animation
     animate_stop(..) stop the animation and restores the background
*/
#include <animate.h>
#include <wnpopup.h>

/*
   Start animation using the symbols in a font
   Saves the background to a buffer for later restoring
   The X,Y positions are absolute positions for the upper left corner
*/
void animate_start(ANIMATEP a, GXT x, GYT y, PGFONT slide_ptr)
   {
   GXT xe;
   GYT ye;
   if ((a == NULL) || (slide_ptr == NULL))
      return;

   /* Convert to absolute positions */
   x += gvpxl(ggetvpnum());
   y += gvpyt(ggetvpnum());
   /* Initialize animation structure */
   a->x = x;
   a->y = y;
   a->index = 0;
   a->background = NULL;
   a->slide_ptr = slide_ptr;

   /* Get slide size */
   xe = gfgetfw(slide_ptr);
   ye = gfgetfh(slide_ptr);

   if ((x + xe) >= GDISPW)
      xe = (GDISPW-1);
   else
      xe += x;

   if ((y + ye) >= GDISPH)
      ye = GDISPH-1;
   else
      ye += y;

   /* Get buffer for background and save background */
   a->size = ghw_blksize(x,y,xe,ye);
   if ((a->background = wn_malloc(a->size)) != NULL)
      ghw_rdblk(x,y,xe,ye,(SGUCHAR *)(a->background), a->size);

   /* Show first picture */
   animate_tick(a);
   }

/*
   Start animation at screen center using the symbols in a font.
   Saves the background to a buffer for later restoring
*/
void animate_center(ANIMATEP a, PGFONT slide_ptr)
   {
   animate_start(a, (GDISPW-gfgetfw(slide_ptr))/2-1, (GDISPH-gfgetfh(slide_ptr))/2-1, slide_ptr);
   }


/*
   Update animation with next slide picture.
   Returns the index for the next slide to be shown
   (== 0 when all slides have been shown and the index has wrapped)
*/
unsigned int animate_tick(ANIMATEP a)
   {
   GXT xb,xe;
   GYT yb,ye;
   if (a == NULL)
      return 0;
   if (a->background == NULL)
      return 0; /* Was stopped */

   /* Assure viewport use of absolute coordinates for gputsym */
   ggetvp(&xb,&yb,&xe,&ye);
   gresetposvp();

   gputsym(a->x,a->y, ggetfsymw((SGUCHAR) a->index, a->slide_ptr)); /* Show font symbol */

   /* Advance to next slide */
   if ((SGUINT)(++(a->index)) == (SGUINT) gfontsize(a->slide_ptr))
      a->index = 0; /* End of font reached, restart from begining */

   gsetvp(xb,yb,xe,ye); /* restore viewport */
   return a->index;
   }

/*
   Stop animation,
   Retore background and release ressources
*/
void animate_stop(ANIMATEP a)
   {
   if (a == NULL)
      return;
   if (a->background == NULL)
      return; /* Was already stopped */
   /* Copy background back*/
   ghw_restoreblk((SGUCHAR *)(a->background));
   wn_free(a->background);
   a->background = NULL;
   }
