/*
   Scrollbar functions
*/
#ifndef WNSCROLLBARV_H
#define WNSCROLLBARV_H

#include <gdisp.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Scroll bar control stucture */
typedef struct
   {
   GXT lx;      /* Width of scroll bar */
   GXT rx;
   GYT k1yt;    /* Top key position */
   GYT k1yb;
   GYT k2yt;    /* Bottom key position */
   GYT k2yb;
   GYT cyt;     /* Scroll bar top position */
   GYT ch;      /* Scroll bar cursor height */
   SGUCHAR ks;  /* Keys state 0 = both passive, 1 = key 1 active, 2 = key 2 active */
   } SCROLLBARV, *PSCROLLBARV;

/* wnscrolb.c */
SGUCHAR wnscrollbv_thickness(void);
int  wnscrollbv_init(PSCROLLBARV sb, GXT xe, GYT yb, GYT ye, SGINT ycmax, SGINT ycb, SGINT yce);
void wnscrollbv_key_state(PSCROLLBARV sb, SGUCHAR state);
void wnscrollbv_draw(PSCROLLBARV sb, SGINT ycmax, SGINT ycb, SGINT yce );
SGUCHAR wnscrollbv_is_touch(PSCROLLBARV sb, GXT x, GYT y );

#ifdef __cplusplus
}
#endif
#endif



