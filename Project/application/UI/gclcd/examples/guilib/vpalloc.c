/************************** vpalloc.c ***************************

   Viewport number administration functions

   Most menu designs are hierarchical in nature.
   The functions in this module takes advantage of this fact and
   simplifies the viewport number handling by implementing a
   viewport "heap", so viewport numbers can be "allocated" and
   "deallocated" in a last-out first-in manner.

   Revision data:    200808
   Revision Purpose: The newly alocated viewport are now always reset to default.
                     (Simplify application programming and retain comptability
                     of old application compatible when new library features are added)

*****************************************************************/

#include <wnpopup.h>
#define VPFIRST 1
#define VPLAST  (GNUMVP-1)

static SGUCHAR wn_currentvp = VPFIRST;

/*
   Fetch the next empty viewport
*/
SGUCHAR wn_get_vpnum(void)
   {
   if (wn_currentvp >= VPLAST)
      {
      G_WARNING("vpnum overflow");
      /* We dont reset the viewport here to mimimise any damage caused by overflow */
      }
   else
      {
      /* Clear viewport data */
      #ifdef GFUNC_VP
      wn_currentvp++;
      gresetvp_vp( wn_currentvp );
      #else
      SGUCHAR oldvp;
      wn_currentvp++;
      oldvp = gselvp(wn_currentvp);
      gresetvp();
      gselvp(oldvp);
      #endif
      }

   return wn_currentvp;          /* Return next viewport */
   }

/*
   Deallocate the (last fetched) viewport
*/
void wn_free_vpnum(SGUCHAR vp)
   {
   if (vp != wn_currentvp)
      {
      G_WARNING("WN_VPNUM Mismatch in vp get / put order");
      }
   if (wn_currentvp > VPFIRST)
      wn_currentvp--;  /* Free viewport */
   }


/*
   Return the last allocated viewport
*/
SGUCHAR wn_vpnum(void)
   {
   return wn_currentvp;
   }

/*
  Initialize the viewport buffer
  Must be called once before use of the functionality in this module
*/
void wn_vpnuminit(void)
   {
   wn_currentvp = VPFIRST;
   }

