/*
   This module provides a template for creating a generic touch screen interface.
   In PC simulation mode this module uses the touch simulation input from LCDSERV.EXE

   The output from this module should have be normalized to screen coordinates.
   Optionally by use of the gnormalize(..) function
*/
#include <wnpopup.h>

#ifdef GHW_PCSIM
#include <gsimintf.h>
#endif

/*
   Fetch a touch event (or check for change)

   Pointers for return parameters:
      unsigned char *edge Poiner to touch press change detection (1 = new event i.e. an edge).
      unsigned char *levelp  Pointer to level parameter (1 = is pressed, 0 = is not pressed)
      GXT *xp  X coordinate for (last) pressed position, normalized to screen width
      GYT *yp  Y coordinate for (last) pressed position, normalized to screen height

   Return value:
      1 = change in pressed state or change in x,y position
      0 = no change.

   Note:
      With a finger tapping (point selection) the data returned will be for the touch contact:
            *edgep = 1    (edge)
            *levelp = 1   (pressed)
            *xp,*yp  updated coordinate values.
      followed by an event for touch release
            *edgep = 1    (edge)
            *levelp = 0   (not pressed)
            *xp,*yp  updated coordinate values.

    Note:
      If a finger pressing the touch screen is moved around on the screen then
      the data returned will be:
            *edgep = 0    (no edge)
            *levelp = 1   (pressed)
            *xp,*yp  updated coordinate values.
*/
unsigned char gtouchkp(unsigned char *edgep, unsigned char *levelp, GXT *xp, GYT *yp)
   {
   #ifdef GHW_PCSIM
   unsigned short x, y;
   unsigned char ret;

   /* Simulates touch screen with mouse right-button on LCD simulator screen */
   ret = GSimTouchGet(edgep, levelp, &x, &y);
   /* GSimTouchGet() returns coordinate values normalized for the screen size in pixel */
   /* Here we just convert to GGXT, GYT types*/
   if (xp != NULL) *xp = (GXT) x;
   if (yp != NULL) *yp = (GYT) y;
   return ret;

   #else

   /* Modify this part for the specific touch controller in the target system */
   /* The returned X;Y values should be normalized to the size of the screen in pixels (GDISPW,GDISPH) */
   /* The gnormalize(..) should be used for this purpos */

   /* Here we just insert some valid defaults */
   if (edgep != NULL) *edgep = 0;   /* no edge */
   if (levelp != NULL) *levelp = 0; /* no touch */
   if (xp != NULL) *xp = 0;         /* upper left corner */
   if (yp != NULL) *yp = 0;
   return 0                         /* no change detected */
   #endif
   }

/*
   Detect touch click event (single point event)
*/
unsigned char gtouch_click(GXT *xp, GYT *yp)
   {
   unsigned char edge, level;
   gtouchkp(&edge,&level, xp, yp);
   return ((edge != 0) && (level != 0)) ? 1 : 0;
   }

