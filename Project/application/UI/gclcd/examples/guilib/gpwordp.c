/*
   Function for identifying the position of word elements on for instance a top menu during cursor setting.
   Each word in the string str is assumed to be a menu element.

   Returns the number of pixels from the line start to the beginning of the word_num word in the line
   (word_num = 0 = first word)
   if *char_index is != NULL the value is updated with the character index for the beginning of the word
*/
#include <wnpopup.h>

SGUINT gpwordpos( const char *str, SGUINT word_num, SGUCHAR wordbegin, SGUINT *char_index)
   {
   SGUINT len = 0;
   SGUINT wordcount = 0;
   SGUINT cnt = 0;
   SGUCHAR space = 1; /* Start with skipping any leading spaces */

   if (str == NULL)
      return 0;

   while ((*str != 0) && (*str != '\n'))
      {
      if (*str != ' ')
         {
         if (space == 1)
            {
            /* Space to word edge detected */
            if  ((wordbegin != 0) && (word_num == wordcount))
                break; /* word start found */
            wordcount++;
            }
         space = 0;
         }
      else
         {
         if (space == 0)
            {
            /* Word to space edge detected */
            if  ((wordbegin == 0) && ((word_num+1) == wordcount))
                break; /* word end found */
            }
         space = 1;
         }

      len += (SGUINT) ggetsymw(*str);
      str++;
      cnt++;
      }

   if (char_index != NULL)
      *char_index  = cnt;
   if ((wordbegin == 0) && (len != 0))
      return  len - 1;  /* Last pixel of word character */
   else
      return len;  /* First pixel of word character */
   }

