#include <wnpopup.h>
/********************************* wnfrsym.c ****************************

   Create framed symbols according to a style

   Usefull for creation of softkeys based on key symbols or creation of
   framed background symbols for animation.

   Upon creation the current viewport is narrowed to be inside the symbol
   so any further drawing can be made relative to the (background) symbol
   itself

   Build on to of the general library functions and the functions in
   wnframe.c

*************************************************************************/

static GFRAMESTYLE frame_style = FR_PLATE;

/*
   Set symbol frame style.
   Return the old symbol frame style
*/
GFRAMESTYLE wnsymframe_style(GFRAMESTYLE fs)
   {
   GFRAMESTYLE oldfs = frame_style;
   frame_style = fs & GFRAMESTYLE_MASK;
   return oldfs;
   }

/*
   Return frame thickness for current symbol frame style
*/
SGUCHAR wn_symframe_tickness(void)
   {
   return wnframe_thicknes(frame_style);
   }

/*
   Return symbol pixel width inclusive frame
*/
SGUINT wnsymframew(PGSYMBOL ps)
   {
   return (SGUINT) gsymw(ps) + 2 * wn_symframe_tickness();
   }

/*
   Return symbol pixel height inclusive frame
*/
SGUINT wnsymframeh(PGSYMBOL ps)
   {
   return (SGUINT) gsymh(ps) + 2 * wn_symframe_tickness();
   }

/*
   Create framed symbol

   The X,Y positions is the upper left corner of the frame relative to
   the current viewport

   The current viewport is narrowed to be inside the frame upon return
   if updatevp flag is true
*/
void wnsymframe(GXT x, GYT y, PGSYMBOL ps, SGBOOL updatevp)
   {
   SGUINT xe;
   SGUINT ye;
   SGUCHAR fs;
   SGUCHAR vp;
   #ifdef GBUFFER
   GUPDATE oldupd;  /* Only need the variable if buffer mode is defined */
   oldupd = gsetupdate(GUPDATE_OFF);   /* Delayed update */
   #endif

   /* Convert to absolute coordinates, to ease viewport adjustment */
   vp = ggetvpnum();
   x += gvpxl(vp);
   y += gvpyt(vp);

   /* Calculate end posisions, avoid screen overflow */
   xe = ((SGUINT) x) + wnsymframew(ps) -1;
   ye = ((SGUINT) y) + wnsymframeh(ps) -1;
   if (xe > GDISPW)
      xe = GDISPW-1;
   if (ye > GDISPH)
      ye = GDISPH-1;

   /* Make frame */
   gsetvp(x,y,(GXT)xe,(GYT)ye);
   wnframe(0, 0, ggetvpw()-1,ggetvph()-1, frame_style | FR_FILL);

   /* Show symbol in frame */
   fs = wn_symframe_tickness();
   gputsym((GXT)fs,(GYT)fs,ps);
   if (updatevp)
      gsetvp((GXT)(x+fs),(GYT)(y+fs),(GXT)(xe-fs),(GYT)(ye-fs));

   #ifdef GBUFFER
   gsetupdate(oldupd);
   #endif
   }


