/*
   This example illustrates how to smoothly scroll text string
   in a viewport, in from right edge, out at left edge.
   The look is like a "running text stream"
*/
#include <wnpopup.h>

static SGUCHAR *buf = NULL;
static PGCSTR str;
static GBUFINT bufsize;
static SGUINT symbol_len;
static SGUINT str_pos;
static SGUCHAR vp;

/*
   Intiate horizontal scroll on first line in viewport.
   The scroll steps are processed by calling horscroll_step()

   The viewport size and font must have been initialized in advance,
   and must not be modified during the scroll process.

   A call with strp == NULL terminate a scroll in progress and
   release all internal ressources.

   Returns 0 if initiated but not running (if stopped)
   Returns 1 if scrolling is initiated and running.
*/
SGUCHAR horscroll_start(SGUCHAR viewport, PGCSTR strp)
   {
   SGUCHAR oldvp;
   vp = viewport;
   oldvp = gselvp(vp);

   symbol_len = 0;
   if (buf != NULL)
      {
      free(buf);
      buf = NULL;
      }

   if ((str = strp) == NULL)
      return 0;
   if (vp >= GNUMVP)
      return 0;
   if ((str_pos = ggetvpw()-1) < 2)
      return 0;

   /* Allocate buffer for (first) partial character */
   bufsize = ghw_blksize(gvpxl(vp),gvpyt(vp),gvpxl(vp)+3*ggetfw(),gvpyt(vp)+ggetfh());
   if ((buf = malloc(bufsize)) == NULL)
      return 0;
   gsetmode(GLINECUT | GVPCLR_RIGHT);

   gselvp(oldvp);
   /* Draw first position */
   return horscroll_step();
   }


/*
   Execute a scroll step

   horscroll_start() must be called in advance.

   Returns 1 if text scrolling is still running.
   Returns 0 if stopped (or not initialized)
*/
SGUCHAR horscroll_step(void)
   {
   SGUCHAR oldvp;
   GXT xs;
   GYT ys;

   /* Has scroll been started ? */
   if (buf == NULL)
      return 0;

   if ((str == NULL) && (symbol_len == 0))
      goto force_terminate;  /* Just in case */

   /* Process scroll step */
   oldvp = gselvp(vp);
   if (str_pos > 0)
      {
      if (--str_pos > 0)
         {
         /* Still in scroll-in mode, just output string and use auto crop */
         gsetpos(str_pos,ggetfh()-1);
         gputs(str);
         }
      }
   if (str_pos == 0)
      {
      /* Manage scroll-out */
      #ifdef GBUFFER
      GUPDATE oldupd;  /* Only need the variable if buffer mode is defined */
      oldupd = gsetupdate(0);   /* Delayed update */
      #endif
      xs = gvpxl(vp); /* Absolute vp coordinates for block functions */
      ys = gvpyt(vp);
      if (symbol_len > 0)
        {
         /* Draw first partial character */
         ghw_wrblk(xs,ys,xs+symbol_len-1,ys+ggetfh()-1, buf);
         }

      if (str != NULL)
         {
         /* Draw rest of string from character boundary */
         gsetpos(symbol_len,ggetfh()-1);
         gputs(str);
         }

      #ifdef GBUFFER
      gsetupdate(oldupd);  /* Restore delayed update */
      #endif

      if (symbol_len == 0)
         {
         if (str == NULL)
            {
            /*Both string and partial character is completed */
            gselvp(oldvp);

            force_terminate:
            free(buf);
            buf = NULL;
            return 0;
            }

         /* Get pixel size of first symbol in (rest of) string */
         symbol_len = ggetsymw( *str );
         str++;
         if (*str == 0)
            str = NULL;
         }

      symbol_len--;
      if (symbol_len > 0)
         {
         /* Save partial character after first pixel row */
         ghw_rdblk(xs+1,ys,xs+symbol_len,ys+ggetfh()-1,buf,bufsize);
         }
      }
   gselvp(oldvp);
   return 1;
   }


#if TESTFUNC

#ifdef GHW_PCSIM
#include <windows.h>  // Sleep
#endif

const char testtxt[] = "This is a scrolled text which scroll from right to left";
void main(void)
   {
   ginit();

   /* Prepare a framed viewport in the bottom of the screen */
   gselfont(&ariel9);
   grectangle(0,GDISPH-ggetfh()-2,GDISPW-1,GDISPH-1);
   gsetvp(1,GDISPH-ggetfh()-1,GDISPW-2,GDISPH-2);

   for(;;)
      {
      /* Init scroll test */
      horscroll_start(ggetvpnum(), testtxt);
      do
         {
         #ifdef GHW_PCSIM
         Sleep(75);  // Delay
         #endif
         }
      while(horscroll_step()); // Step look until completed
      }

   }

#endif

