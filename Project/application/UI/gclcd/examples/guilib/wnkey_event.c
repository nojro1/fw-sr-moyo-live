/*
   Functions for event handlin related to soft keys drawn with the
   functions in wnkeyvp.c

   Demonstrates how to create a simple software key event handling
   based on a viewport + a NULL terminated array of one or more pointers
   to text strings holding the key text.
   If the key text array contains of more than one text string a
   a key with multi toggle select is optained.

   Each active (visible) key must use an individual viewport.
   The key look, position, state and content information is stored
   in the viewport plus extension.

   A common key look style is used.
*/
#include <wnmenu.h>
#include <gevents.h>

/* Check that library configuration is right */
#ifndef GEXTMODE
  #error This module require that GEXTMODE is enabled (defined) in gdispcfg.h
#endif


EVENT gm_key_event(SGUCHAR vp, EVENT event, MENUINDEX *selection)
   {
   PGVPAPP app;
   SGUCHAR max;
   app = ggetapp_vp(vp);
   if (app->mnu != NULL)
      return event; /* Wrong viewport, is a menu not a key */

   if ((event == EVENT_KEY_RETURN) && (app->has_focus!=0))
      { /* Keyboard event */
      goto keypressed;
      }

   if (ginsidevp(vp,touch_x,touch_y))
      {
      if (event == EVENT_TOUCH_UP)
         {
         keypressed:
         gm_set_key_pressed(vp, 0);
         max = (SGUCHAR) mnuelem_strp_size((PGCSTR *) app->txtptr);
         max = (app->cursoridx+1)%max; /* Increment with wrap */
         if (max != app->cursoridx)
            { /* Index changed (array had more than one element). Update key's text content */
            gm_set_key_text(vp,max);
            }
         if (selection != NULL)
            *selection = (MENUINDEX)((SGUINT)app->cursoridx);
         return EVENT_SELECTION;
         }
      if (event == EVENT_TOUCH_DRAG)
         return EVENT_KEY_NOEVENT; /* Consume movements on key */
      if (event == EVENT_TOUCH_DOWN)
         {
         gm_set_key_pressed(vp, 1);
         return EVENT_KEY_NOEVENT; /* Consume key release (action is on touch up) */
         }
      }
   else
      {
      /* Lost focus (clicking or dragging outside key area) */
      if (app->has_focus)
         /* Change a pressed scroll bar key to key-not-pressed state */
         gm_set_key_pressed(vp, 0);
      }
   return event;
   }

