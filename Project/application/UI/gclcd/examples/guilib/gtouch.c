/*************************** gtouch.c ***********************

  Functions for matching touch screen coordinates with viewport
  areas and menu items.

  It is assumed that the touch screen controller is returing
  two unsigned integer values.
  gnormalize(..) normalize touch screen x or y values to the screen
                 pixel coordinate ranges used by the LCD driver lib.
  ginsidevp(..)  checks if an absolute x,y point is inside the viewport area
  gmenu_idx(..)  convert an absolute x,y point to a menu index (vertical menu)
  gtopmenu_idx(..) convert an absolute x,y point to a topmenu index (horizontal menu)

*************************************************************/
#include <wnpopup.h>
/*
   Convert a touch screen value to a pixel value between 0 and pixelmax.
   As pixelmax is normally used GDISPW-1 or GDISPH-1 for x and y coordinates
   respectively.
   The return value can be cast to a GXT or GYT type respectively.
*/
SGUINT gnormalize( SGUINT touch_x, SGUINT touch_maxx, SGUINT touch_minx, SGUINT pixelmax )
   {
   SGULONG tmp;
   /* Force resonable values */
   GLIMITU(touch_minx,touch_maxx);
   GLIMITU(touch_x,touch_maxx);
   GLIMITD(touch_x,touch_minx);

   /* Prevent divide by zero error */
   if (touch_minx == touch_maxx)
      {
      G_WARNING("Illegal touch range");
      return 0;
      }
   /* Return normalized coordinate range (first scale up, then down) */
   tmp = ((SGULONG)(touch_x - touch_minx)) * ((SGULONG) pixelmax);
   return (SGUINT) (tmp / ((SGULONG)(touch_maxx-touch_minx)));
   }


/*
   Check if a set of absolute coordinates covers the viewport area
   Return 1 if x,y is inside viewport
   Return 0 if x,y is outside viewport
*/
SGBOOL ginsidevp( SGUCHAR vp, GXT x, GYT y)
   {
   if ((x > gvpxr(vp)) || (x < gvpxl(vp)) || (y > gvpyb(vp)) || (y < gvpyt(vp)))
      return 0; /* Outside viewport area */
   return 1;    /* Inside viewport area */
   }

/*
   Convert a set of absolute coordinates to a menu index.

   The menu index is calculated as the line number from the top of
   the visual viewport where index 0 is the topmost line.

   Returns -1 if x,y coordinates is outside the menu viewport area
*/
SGINT gmenuidx( SGUCHAR vp, GXT x, GYT y)
   {
   if (ginsidevp(vp,x,y) == 0)
      return -1;                       /* Outside viewport */
   return (y-gvpyt(vp))/ggetfh_vp(vp); /* Convert y to a menu index */
   }

/*
   Convert a set of absolute coordinates to a menu index.in a topmenu sting

   The menu index is calculated as the word count from the left of
   the visual viewport where index 0 is the left most word.

   Returns -1 if x,y coordinates is outside the menu viewport area or not
   inside the top menu word boundaries.
*/
SGINT gtopmenuidx( SGUCHAR vp, const char *menustr, GXT x, GYT y)
   {
   SGUCHAR oldvp;
   SGINT index;
   GXT xb,xe,cb,ce;
   GYT yb,ye;
   oldvp = gselvp(vp);         /* Make the viewport current */

   ggetvp(&xb,&yb,&xe,&ye);    /* Get absolute viewport positions */
   if ((menustr == NULL) || (x > xe) || (x < xb) || (y > ye) || (y < yb))
      index = -1;            /* Outside viewport (or illegal parameter) */
   else
   for (index = 0;;index++)
      {
      /* Compare with word positions in the menu string */
      SGUINT cstart;
      cb = (GXT) gpwordpos(menustr,index,GPWORD_BEGIN,&cstart);     /* Position for word start */
      ce = (GXT) gpwordpos(&menustr[cstart],0,GPWORD_END,NULL) + cb;/* Position for word end */
      if ((ce == cb) || (x < cb+xb))
         { /* Not found before end of string, or at inter element position */
         index = -1;
         break;
         }
      if (x <= ce+xb)  /* Found */
         break;
      }

   gselvp(oldvp);
   return index;
   }






