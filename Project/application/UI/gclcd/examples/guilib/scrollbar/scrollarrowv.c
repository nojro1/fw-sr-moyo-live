/***************************** scrollbarv.c ************************

   scrollbarv font table structure definitions.
   This file has been auto-generated with the IconEdit / FontEdit tool.

   Copyright(c) RAMTEX 1998-2009

*****************************************************************/
#include <gdisphw.h>

/* Symbol table entry with fixed sized symbols */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR b[5];       /* Symbol data, "variable length" */
   }
GCODE FCODE scrollarrowvsym[4] =
   {
   #include "scrollarrowv.sym" /* Include symbols */
   };

/* Font structure */
GCODE GFONT FCODE scrollarrowv =
   {
   7,       /* averange width */
   5,       /* height */
   sizeof(scrollarrowvsym[0]) - sizeof(GSYMHEAD), /* number of bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL)scrollarrowvsym, /* pointer to array of SYMBOLS */
   4,      /* num symbols */
   (PGCODEPAGE)(NULL)
   };

