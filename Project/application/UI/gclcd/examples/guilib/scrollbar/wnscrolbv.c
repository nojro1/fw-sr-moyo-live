/*
   Draw and handle scroll bars. Ex as part of a menu view.

   The functions are generic but see menu-list implementation for example on use.
*/
#include <wnmenu.h>

/************* "LOOK" DEFINITIONS ***********/

/* Define scroll bar "look" (in connection with the scroll bar end marker symbols) */
#define FRAME             FR_PLATE
#define COLOR_BACK        G_LLIGHTGREY
#define COLOR_FORE        G_WHITE
#define COLOR_SYMBACK     G_GREY
#define COLOR_SYMFORE     G_BLACK
#define COLOR_SYMFORE_END G_DDARKGREY

/* Symbols for creating arrow marks on scroll bar
   The visual size of a scroll bar can easily be adjusted by creating another
   scrollbar arrow symbol font with a different size */
extern GCODE GFONT FCODE scrollarrowv;

/* Symbol indexes into the scrollbar end marker symbol font*/
#define  ARROW_UP       0x0
#define  ARROW_DOWN     0x1
#define  ARROW_UP_END   0x2
#define  ARROW_DOWN_END 0x3

/******************** End of "LOOK" defintion *******************/

/*
   Convert relative cursor posititions to abosolute pixel positions
   The sb structure must have been initialized in advance.
*/
static void wnscrollbv_curset(PSCROLLBARV sb, SGINT ycmax, SGINT ycb, SGINT yce)
   {
   SGINT ye,ybt,curmin;
   ye = sb->k2yt-sb->k1yb-2; /* Cursor pixel max range */
   /* Normalize cursor start / end */
   yce = (yce*ye)/ycmax;
   ybt = (ycb*ye)/ycmax;
   /* Check to assure a minimum cursor size */
   curmin = 2*wnframe_thicknes(FRAME)+1; /* Min cursor height - 1 */
   if ((yce-ybt) <= curmin)
      {
      /* Use minimum cursor size */
      sb->ch = curmin;
      /* Distribute over reduced range + convert to absolute coordinate */
      sb->cyt = (ycb*(ye-curmin))/ycmax + sb->k1yb+1;
      }
   else
      {
      /* Normal cursor size handling. Convert to abosolute coordinates */
      sb->cyt = ybt+sb->k1yb+1;
      sb->ch = yce-ybt;
      }
   }


/*
   Initialize is a scroll bar structure

   The coordinates define the positions for the upper-right
   corner and the lower right corner of the scroll bar.
   (typically = inner coordinates of a framed viewport)
   The cursor information coordinates defines the cursor start, end index position relative
   to a maximum element index value. Only positive coordinates are valid.

   The PSCROLLBAR structure must be initialized with absolute coordinates
   (Easier touch area detection)

    PSCROLLBAR sb   Scroll bar structure to be initialized
                      Position setup
    GXT xe          Right edge of scroll bar (typically equal to view port right edge)
    GYT yb                Top of scroll bar        (typically equal to view port top edge)
    GYT ye          Bottom of scroll bar     (typically equal to view port bottom edge)
    SGINT ycmax     Maximum index for menu (1-32K)
    SGINT ycb       Top index for view (view mode) or position for cursor (cursor mode).
    SGINT yce       Bottom index for view (view mode) or position for cursor (cursor mode).
*/
int wnscrollbv_init(PSCROLLBARV sb, GXT xe, GYT yb, GYT ye, SGINT ycmax, SGINT ycb, SGINT yce)
   {
   SGUCHAR fs;
   PGSYMBOL psym;
   if (sb == NULL)
      return -1;
   if ((ycmax <= 0) || (ycb > ycmax) || (yce > ycmax))
      return -1; /* Illegal positions settings */

   fs = wnframe_thicknes(FRAME);
   psym = ggetfsym( ARROW_UP, &scrollarrowv);
   sb->lx   = gsymw(psym)+2*fs-1; /* Width of scroll bar */
   sb->k1yb = gsymh(psym);        /* Arrow symbol height */

   /* Room for 2 arrow key symbols with frames, plus a minimum cursor ?*/
   if (((ye-yb+1) < (2*sb->k1yb+4*fs+2)) || (xe < sb->lx))
      return-1; /* No room for scrollbar */

   /* Set key symbol areas */
   sb->rx = xe;         /* Right edge */
   sb->lx = xe-sb->lx;  /* Left edge */
   sb->k1yt = yb;       /* Key 1 top */
   sb->k2yb = ye;       /* Key 2 bottom */
   fs=sb->k1yb+2*fs-1;  /* size incl frames */
   sb->k2yt = ye-fs;    /* Key 2 top */
   sb->k1yb = yb+fs;    /* Key 1 bottom */

   /* Normalize and set cursor size and position */
   wnscrollbv_curset(sb, ycmax, ycb, yce);
   return 0;
   }

/*
   Intern Draw up/down key states (internal)
*/
static void wn_i_scrollbv_key_state(PSCROLLBARV sb, SGUCHAR state)
   {
   wnframe(sb->lx,sb->k1yt, sb->rx,sb->k1yb, (state==1) ? FR_INSERT : FR_PLATE );
   wnframe(sb->lx,sb->k2yt, sb->rx,sb->k2yb, (state==2) ? FR_INSERT : FR_PLATE );
   sb->ks = state;
   }

/*
   Draw up/down key states (public)
   Scroll bar key states:
      0 = both passive
      1 = key 1 active
      2 = key 2 active
*/
void wnscrollbv_key_state(PSCROLLBARV sb, SGUCHAR state)
   {
   GXT xb,xe;
   GYT yb,ye;
   ggetvp(&xb,&yb,&xe,&ye); /* Assure absolute coordinates */
   gresetposvp();

   wn_i_scrollbv_key_state(sb,state);

   gsetvp(xb,yb,xe,ye);     /* Restore */
   }

/*
   Draw whole vertical scroll bar with frame and cursor

   The coordinates define the positions for the upper-right
   corner and the lower right corner of the scroll bar.
   (typically = inner coordinates of a framed viewport)
   The cursor coordinates define the start, end position after the top symbol.
   The function updates an existing scroll bar according to the

   Draw scrollbar in accordance with the SCROLLBAR content and the relative menu list
   size and positions
    PSCROLLBAR sb   Pointer to scrollbar defining the absolute locations of the scrollbar
    SGINT ycmax     Maximum index for menu (1-32K)
    SGINT ycb       Top index for view (view mode) or position for cursor (cursor mode).
    SGINT yce       Bottom index for view (view mode) or position for cursor (cursor mode).
*/
void wnscrollbv_draw(PSCROLLBARV sb, SGINT ycmax, SGINT ycb, SGINT yce )
   {
   PGSYMBOL psym;
   GCOLOR fc,bc;
   SGUCHAR fs;
   GXT xb,xe;
   GYT yb,ye;
   if (sb == NULL)
      return;
   if (sb->rx <= sb->lx)
      return; /* Not initialized */

   /* Normalize and set cursor size and position */
   bc = gsetcolorb(COLOR_SYMBACK);
   fs = (GYT) wnframe_thicknes(FRAME);
   fc = gsetcolorf(COLOR_SYMFORE);

   /* Use absolute coordinates */
   ggetvp(&xb,&yb,&xe,&ye);
   gresetposvp();
   wnscrollbv_curset(sb, ycmax, ycb, yce); /* Sizing and position set of scroll bar cursor */

   /* Scroll bar is ready. Draw in accordance with content. */

   /* Keys in pressed or released state (+ make coordinates absolute) */
   wn_i_scrollbv_key_state(sb,sb->ks);

   /* use absolute coordinates */
   /* Top key as end mark or arrow up */
   psym = ggetfsym((ycb == 0)  ? ARROW_UP_END : ARROW_UP, &scrollarrowv);
   gsetcolorf((ycb == 0) ? COLOR_SYMFORE_END : COLOR_SYMFORE);
   gputsym(sb->lx+fs,sb->k1yt+fs,psym);
   /* Bottom key as end mark or arrow down */
   psym = ggetfsym((yce == ycmax)  ? ARROW_DOWN_END : ARROW_DOWN, &scrollarrowv);
   gsetcolorf((yce == ycmax) ? COLOR_SYMFORE_END : COLOR_SYMFORE);
   gputsym(sb->lx+fs,sb->k2yt+fs,psym);

   gsetcolorb(COLOR_BACK);
   /* Draw top scroll area */
   if (sb->cyt > (sb->k1yb+1))
      gfillvp(sb->lx,sb->k1yb+1,sb->rx,sb->cyt-1,0x0);

   /* Draw scroll cursor */
   wnframe(sb->lx,sb->cyt,sb->rx,sb->cyt+sb->ch,(GFRAMESTYLE)(FRAME | FR_FILL));

   /* Draw bottom scroll area */
   if ((sb->cyt+sb->ch+1) < sb->k2yt)
      gfillvp(sb->lx,sb->cyt+sb->ch+1,sb->rx,sb->k2yt-1,0x0);

   /* Restore */
   gsetvp(xb,yb,xe,ye);
   gsetcolorf(fc);
   gsetcolorb(bc);
   }

/*
   Return thikness of vertical bars and height of horizontal scroll bars
   (Assume rectangular bar end symbols)
*/
GYT wnscrollbv_thickness(void)
   {
   return gfgetfw(&scrollarrowv) + 2*wnframe_thicknes(FRAME);
   }


/*
   Check if a (touch) x,y coordinate pair is inside any of the scroll bar areas
     Return 0 if not inside
     Return 1 if inside top key
     Return 2 if inside bottom key
     Return 3 if inside cursor area
*/
SGUCHAR wnscrollbv_is_touch(PSCROLLBARV sb, GXT x, GYT y )
   {
   if ((sb->rx == sb->lx) || (x > sb->rx) || (x < sb->lx) || (y > sb->k2yb) || (y < sb->k1yt))
      return 0; /* Outside scroll bar area */
   /* Inside scroll bar area. Specify which sub area */
   if (y <= sb->k1yb)
      return 1;
   if (y >= sb->k2yt)
      return 2;
   return 3;
   }

