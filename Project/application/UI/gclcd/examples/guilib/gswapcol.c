#include <gdisp.h>
/*
  Exchange two colors, or replace one color with another
  in the current viewport
*/
void gswap_color(GXT xs, GYT ys, GXT xe, GYT ye, GCOLOR c1, GCOLOR c2)
   {
   GCOLOR f,b;
   /* Modify foreground background colors */
   f=ggetcolorf();
   b=ggetcolorb();
   /* Set foreground background to parameter colors */
   gsetcolorf(c1);
   gsetcolorb(c2);
   /* Exchange fore and background colors, leave other colors untouched */
   ginvertvp(xs,ys,xe,ye);
   /* Restore foreground background colors */
   gsetcolorf(f);
   gsetcolorb(b);
   }


