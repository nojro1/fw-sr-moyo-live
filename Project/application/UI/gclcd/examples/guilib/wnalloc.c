/********************* wnalloc.c **************************

   Allocate a buffer (for background storage)

   The advantage of using these functions is that they provide
   a focus point for easy monitoring or debugging of heap
   operations.


   If WN_USE_MALLOC is defined then these functions maps to the
   standard functions malloc(..) and free(..)


   If WN_USE_MALLOC is not defined then these functions implements
   a simple and memory optimized "heap" by use of a single RAM buffer.
   (The C heap functions therefore does not need to be linked in)

**********************************************************/

#include <wnpopup.h>

#define WN_USE_MALLOC

#ifdef WN_USE_MALLOC
 #include <stdlib.h> /* malloc */
#else
  /* to be implemented */
#endif

/*
   Allocate a buffer
*/
void *wn_malloc(GBUFINT size)
   {
   #ifdef WN_USE_MALLOC
   return malloc(size);
   #else
   /* to be implemented */
   #endif
   }

/*
   Free (last) buffer
*/
void wn_free(void *buf)
   {
   #ifdef WN_USE_MALLOC
   free(buf);
   #else
   /* to be implemented */
   #endif
   }

/*
   Allocate a buffer which fit the current viewport exactly
   The buffer is deallocated with wn_free(..)
*/
SGUCHAR *vp_malloc(void)
   {
   GXT xb,xe;
   GYT yb,ye;
   ggetvp(&xb,&yb,&xe,&ye);
   return (SGUCHAR *) wn_malloc(ghw_blksize(xb,yb,xe,ye));
   }


void wn_allocinit(void)
   {
   /* to be implemented */
   }
 
