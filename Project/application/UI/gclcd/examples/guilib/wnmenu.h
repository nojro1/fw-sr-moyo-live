#ifndef WNMENU_H
#define WNMENU_H

#include <sgtypes.h>
#include <gdisp.h>
#include <wnpopup.h>
#include <wnscrolbv.h>

#ifdef __cplusplus
extern "C" {
#endif

void gm_init(void);
void gm_menu_close( SGUCHAR vp );
SGUCHAR gm_topmenu_open(const char *toptxt);
SGUCHAR gm_submenu_open(GXT x, GYT y, const char *txt);
SGUCHAR gm_submenu3_open(GXT x, GYT y, const char *check_txt, const char *txt, const char *arrow_txt);
void gm_menu_activate( SGUCHAR vp );

/* Limits which protect any topmenu or bottom status bars. */
extern GYT upperlimit;
extern GYT lowerlimit;

typedef enum
   {
   CURSOR_OFF,
   CURSOR_PLATE,
   CURSOR_INSERT,
   CURSOR_FRAME,
   CURSOR_BLOCK
   } GMCURSOR;

void gm_cursor( SGUCHAR vp, SGUCHAR idx, SGBOOL on );

/* Define symbolic name for the gm functions so menu font easily can be changed */
#if (GDISPH > 200)
  #define GMFONT &narrow20
#elif (GDISPH <= 64)
  #define GMFONT &ms58p
#else
  #define GMFONT &narrow10
#endif

#define  ARROWRIGHT  "\x1f"
#define  ARROWLEFT   "\x1e"
#define  ARROWUP     "\x1d"
#define  ARROWDOWN   "\x1c"
#define  ARROWRSTOP  "\x1b"
#define  ARROWLSTOP  "\x1a"
#define  ARROWRETURN "\x19"
#define  ARROWESC    "\x18"
#define  CHECKMARK   "\x17"

/* mnu_list.c */

/* Menu element control */
#define MENUINDEX SGINT
/* Prototype for function drawing menu element content.
   The current viewport will define the element area and be preinitialized with
   settings according to style. Text will use auto clear.
   It is assumed that the data is designed to fit one menu element area
   Returns ==0 if ok
   Returns !=0 if error and menu element rendering should be aborted
*/
typedef int (*MENUFUNC)(MENUINDEX index, void *elementdat);
#define MENU_INIT  ((MENUINDEX)(-1))  /* MENUFUNC is called once at menu init with this parameter */
#define MENU_CLOSE ((MENUINDEX)(-2))  /* MENUFUNC is called once at menu close with this parameter */

typedef struct _MENUCTRLV
   {
   MENUINDEX maxidx;         /* Maximum index (for bottom element) */
   MENUINDEX topidx;         /* Current index for top element */
   MENUINDEX cursoridx;      /* Current index */
   SGUCHAR   lines;          /* Number of visible elements in vertical menu view */
   GYT       lineheight;     /* Pixel height of one (text) line element */
   SCROLLBARV scrollarea;    /* Menu scrollbar definition from <wnscrolbv.h> */
   #ifdef GHW_USING_COLOR
   GCOLOR    cursor_background;
   GCOLOR    cursor_foreground;
   #endif
   } MENUCTRLV, *PMENUCTRLV;

typedef struct _MENUDYN
   {
   MENUCTRLV ctrl;         /* Menu control structure */
   MENUFUNC  elementfunc;  /* User supplied function for fetching element text data */
   PGSTYLE  style;
   } MENUDYN, *PMENUDYN;

int gm_list_init(SGUCHAR vp,
            PMENUDYN md,        /* Pointer to data structure to hold menu control info */
            GXT x, GYT y, GXT width, SGUCHAR lines, GYT lineheight,        /* Location information */
            MENUINDEX maxindex, MENUINDEX topindex, MENUINDEX cursorindex, /* Initial scroll range setup */
            void *elementdat,     /* Pointer passed to element func. Not use by menu itself*/
            MENUFUNC elementfunc /* Element data fetch function */
            );

void gm_list_update(SGUCHAR vp);
void gm_list_close(SGUCHAR vp);

MENUINDEX gm_list_scroll(SGUCHAR vp, MENUINDEX relative);
MENUINDEX gm_list_set_top_index(SGUCHAR vp, MENUINDEX topidx);
MENUINDEX gm_list_set_cur_index(SGUCHAR vp, MENUINDEX curindex);
SGBOOL gm_cursor_enable(SGUCHAR vp, SGBOOL on);

MENUINDEX gm_list_get_cur_index(SGUCHAR vp);
MENUINDEX gm_list_get_top_index(SGUCHAR vp);
MENUINDEX gm_list_get_max_index(SGUCHAR vp);
MENUINDEX gm_list_get_lines(SGUCHAR vp);

/* mnuelem_strp.c */
MENUINDEX mnuelem_strp_size(PGCSTR *strpp);
int mnuelem_strp(MENUINDEX index, void *element_table);
SGUINT mnuelem_strp_maxwidth(PGCSTR *strpp);

/* wnkey.c */
void gm_key_init(SGUCHAR vp,
            GXT x, GYT y,          /* Location information */
            GXT width, GYT height, /* Key size information */
            const char **keytext); /* Pointer to a NULL terminated array with keytext(s) */
void gm_set_key_pressed(SGUCHAR vp, SGUCHAR pressed);
SGUCHAR gm_set_key_text(SGUCHAR vp, SGUCHAR index);
SGUCHAR gm_get_key_index(SGUCHAR vp);
SGUCHAR gm_get_state(SGUCHAR vp);

#ifdef __cplusplus
}
#endif

#endif

