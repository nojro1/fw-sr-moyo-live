/***********************************************************************

   PARAMETER LIST EXAMPLE
   ----------------------
   Example on how to create a popup window for displaying strings
   each containing a list of values in a framed viewport. List elements
   in each column string are seperated with \n characters.

   Three list strings are arranged so each list line consist of the
   fields shown in 3 seperate columns:
      post_text  value_text  pre_text

   void wn_popvalue(SGUCHAR vp, GXT x, GYT y, GXT width, GYT height,
       const char *pretext, const char *valuetext, const char *posttext)

   If the pretext or post text column pointers are NULL the column
   is skipped during menu creation. The function can therefore be used
   for displaing a list of 1, 2 or 3 columns.

     vp   = window viewport
     vp+1 = value viewport (initialized upon return)
     width  = minimum width of viewport (use 0 for auto sizing to text size)
     height = minimum height of viewport (use 0 for auto sizing to text size

   If width is greater than all the text widths then width is used and the
   extra space is added to the value colum widths otherwise the frame is
   narrowed around the text

   If hight is greater than the maximum height of the lines in any of the
   columns then the height value is used. Otherwise the frame height is
   narrowed to fit the maximum text height.

   Copyright (C) Ramtex Engineering 2004

***********************************************************************/

#include <wnpopup.h>

void wn_popvalue(SGUCHAR vp, GXT x, GYT y, GXT width, GYT height,
   const char *pretext, const char *valuetext, const char *posttext)
   {
   SGUCHAR oldvp;
   SGUINT w1,w2,w3,wsp,w4;
   SGUINT h1,h3,h4;
   #ifdef GBUFFER
   GUPDATE oldupd;  /* Only need the variable if buffer mode is defined */
   #endif

   if (vp > GNUMVP-1)
      return; /* Illegal viewport number */

   oldvp = gselvp(vp);
   #ifdef GBUFFER
   oldupd = gsetupdate(0);     /* Delayed update */
   #endif

   /* Preset font (assures correct cursor calculations) */
   gselfont(wnget_style()->font);
   gsetmode(GNORMAL);

   /* Calculate the text sizes */
   if (pretext != NULL)
      {
      w1 = gpstrwidth(pretext);
      w2 = gpstrlen(": ");
      h1 = gpstrheight(pretext);
      }
   else
      w1=w2=h1=0;

   if (valuetext != NULL)
      {
      w3 = gpstrwidth(valuetext);
      h3 = gpstrheight(valuetext);
      }
   else
      w3=h3=gpstrwidth("9"); /* Assure room for a single digit */

   if (posttext != NULL)
      {
      w4 = gpstrwidth(posttext);
      h4 = gpstrheight(posttext);
      wsp = 3;
      }
   else
      w4=h4=wsp=0;

   /* Calculate the maximum width and height */
   if (width < (GXT)(w1+w2+w3+wsp+w4))
      width = (GXT)(w1+w2+w3+wsp+w4);
   if (h1 < h3)
      h1 = h3;
   if (h1 < h4)
      h1 = h4;
   if (height < (GYT)(h1))
      height = (GYT)(h1);

   /* If coordinate overflow then try to center the window */
   /* Setting X,Y so there deliberately is overflow can be used for auto centering */
   if ((width >= GDISPW) || ((GDISPW-width-2) < x))
      x = ((GDISPW-width)/2);
   if ((height >= GDISPH) || ((GDISPH-height-2) < y))
      y = ((GDISPH-height)/2);

   /* Create the window */
   wnvpcreate_pos(vp,x,y,width,height);

   gselvp(vp+1);
   gselfont(wnget_style()->font);
   if (pretext != NULL)
      {
      /* Output pretext */
      SGUCHAR i;
      gsetmode(GNORMAL);
      gsetvp(gvpxl(vp),gvpyt(vp),gvpxl(vp)+w1-1,gvpyb(vp));
      gputs(pretext);

      /* Output colon separator */
      gsetvp(gvpxl(vp)+w1,gvpyt(vp),gvpxl(vp)+w1+w2-1,gvpyb(vp));
      gsetmode(GALIGN_HCENTER);
      for (i = 0; i < h1/ggetfh(); i++)
         gputsvpln(vp+1,":");
      }

   if (posttext != NULL)
      {
      /* Output pretext in rightmost column, column is aligned to the */
      /* right window edge */
      gsetmode(GNORMAL);
      gsetvp(gvpxr(vp)-(w4-1),gvpyt(vp),gvpxr(vp),gvpyb(vp));
      gputs(posttext);
      }

   /* Let value viewport be equal to the area beween the post text */
   /* and pre text columns. Values are right aligned in the viewport */
   gsetmode(GALIGN_RIGHT|GLINECUT);
   gsetvp(gvpxl(vp)+w1+w2,gvpyt(vp),gvpxr(vp)-(w4+wsp)+1,gvpyb(vp));
   if (valuetext != NULL)
      gputs(valuetext);

   /* Leave vp+1 initialized as the value column viewport */
   gselvp(oldvp);       /* Restore vp */

   #ifdef GBUFFER
   gsetupdate(oldupd);  /* Restore delayed update */
   #endif
   }


