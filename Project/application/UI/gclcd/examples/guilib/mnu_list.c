/*
   This implementation illustrate how to create a text menu with a vertical scroll bar
   suitable for handling menus with a large number of menu elements.

   The menu content is treated like a linear array of virtual menu data.
   Each visible menu element is drawn to the screen when needed via a menuelement draw
   function.
   This method enables menu element data to be fetched and formatted "on-the-fly" in
   the menu-elelement draw function only when needed.

   Menu element data fetch format and draw function
   ------------------------------------------------
   The prototype for the menu element fetch and draw function is like this:

      typedef int (*MENUFUNC)(MENUINDEX index, void *elementdat);

   Where index is the elementdat[] array index {0-n} for the element to fetch and format.
   Two special index values are reserved:
        MENU_INIT  the menu function is called one with the value when the menu is created.
        MENU_CLOSE the menu function is called one with the value when the menu is closed.

   The elementdat pointer type is don't care for the menu list functions itself. It is
   just passed from initialization to the menu element function. It can therefore be of any
   type and mean any thing. For instance:
      The base pointer for and array of pointers to string
      The base pointer for a string with \n spearated elements
      The base pointer for an array of raw low-level data to be formatted.
      The file name for a text file to open and show
      etc etc.

   All this knowledge is encabsulated in the (user implemented) menu element "fetch and
   draw" function.
   (In mnuelemstrp.c is an example of a menu element fetch function using a NULL
   terminated array of pointers to strings).

   This method (menu element drawing via generic function) has several advantages:
   -  An menu element can be drawn using any combination of text and graphic symbols.
   -  No RAM buffering is needed for menu content data. Ideal for memory contrained systems.
   -  The the number of elements in the menus list can be abitrary large without
      any requirements for extra memory. Ideal for data base look-up.
   -  Can also be used with simple ROM text string. The menu element draw
      function can just do a simple line index lookup in the text array and do a draw:

   The menu element height is the same for all menu elements.
   Typically the height of one menu element is assumed to be equal to the font height for
   the menu style. One menu element is assumed to show one text line.
   The menu view is always a whole number of menu elements.
   The the function is called current viewport as already been preinitialized with the
   position and style setting. In most cases text output can just use gputs(..)

   The menu contains two view scroll modes:
      View scroll mode
          When cursor support is dispabled the full menu content is scrolled
          at each move event.
      Cursor scroll mode
          When cursor support is enabled the curstor is scrolled first. When the
          cursor reaches the viewport end then the full menu content is scrolled.

    RAMTEX Engineering ApS  2007-2010
*/
#include <wnmenu.h>
#include <wnpopup.h>
#include <string.h>

/* Check that library configuration is right */
#ifndef GEXTMODE
  #error This module require that GEXTMODE is enabled (defined) in gdispcfg.h
#endif

#ifdef GHW_USING_COLOR
GCOLOR gm_background = G_WHITE;
GCOLOR gm_foreground = G_BLACK;
GCOLOR gm_cursor_background = G_BLUE;
GCOLOR gm_cursor_foreground = G_WHITE;
#endif

/* Position offset settings to compensate normal font layout (set before each call of elementdat func) */
GYT linetop  = 1; /* Add pixel line at top */
GXT lineleft = 1; /* Add pixel to left of first symbol */

/*
   Initialize viewport with settings and structures for a vertical menu with optional scroll bars
   The menu is not created yet, so any setting may be adjusted afterward
*/
int gm_list_init(SGUCHAR vp,
            PMENUDYN md,             /* Pointer to data structure to hold menu control info */
            GXT x, GYT y,            /* Location for upper left corner of the menu (frame) */
            GXT width,               /* Inner area width in pixels (excluding an optional scrollbar) */
            SGUCHAR lines,           /* Number of visible menu lines */
            GYT lineheight,          /* Pixel height of one menu line (0 = use style font height) */
            MENUINDEX maxindex,      /* Maximum element index value for scroll */
            MENUINDEX topindex,      /* Element to show at top menu line */
            MENUINDEX cursorindex,   /* Element to hold cursor initially (if outside visible range cursor is turned off) */
            void *elementdat,        /* Data pointer passed to element func. Not use by menu itself */
            MENUFUNC elementfunc     /* Element content draw function */
            )
   {
   SGUINT h;
   PGSTYLE style;
   PGVPAPP app;     /* Pointer to application specific part of viewport */
   SGUCHAR oldvp;
   GXT scrlbw;
   #ifdef GBUFFER
   GUPDATE oldupd;  /* Only need the variable if buffer mode is defined */
   #endif
                    /* In this application used for pointer to background storage, and pointer to md structure */
   if ((lines == 0) || (md == NULL) || (vp >= GNUMVP) || (elementfunc==NULL))
      return -1;

   memset(md,0,sizeof(MENUCTRLV));
   oldvp = gselvp(vp);
   gresetvp();
   app = ggetapp_vp(vp);
   app->cursorvisible = 0;

   /*** Check if parameters creates screen overflow. If overflow do a gracious fall back correction  ***/
   style = wnget_style(); /* Save reference to style used at init time */
   if (lineheight == 0)
      {
      gselfont(style->font);     /* Initialize in advance to calculate font height */
      lineheight = ggetfh() + linetop;
      }

   /* Assure that menu is within limit */
   if (y < upperlimit)
      y = upperlimit;
   h = y+wnfryt()+wnfryb()+style->shadow-1;
   /* Reduce height so there is a whole number of lines */
   while (h+(SGUINT)lines*lineheight > (SGUINT)lowerlimit)
      {
      lines--;
      if (lines == 0)
         return -1;
      }

   /* Fit menu height. Check if scroll bars is needed */
   if (lines >= maxindex+1)
      {
      maxindex = lines-1;  /* Reduce height to actual number of lines */
      scrlbw = 0;       /* No scroll bar needed */
      }
   else
      /* Use scroll bar, add style thickness */
      scrlbw = wnscrollbv_thickness();

   /* Force valid menu indexe */
   if ((topindex + (MENUINDEX)(lines-1)) > maxindex)
      topindex = maxindex - (MENUINDEX)(lines-1);

   /* Prepare menu and cursor control structures */
   md->ctrl.topidx = topindex;
   md->ctrl.maxidx = maxindex;
   md->ctrl.lines = lines;
   md->ctrl.lineheight = lineheight;
   md->ctrl.cursoridx = cursorindex;
   md->elementfunc = elementfunc;
   md->style = style;
   #ifdef GHW_USING_COLOR
   md->ctrl.cursor_background = gm_cursor_background;
   md->ctrl.cursor_foreground = gm_cursor_foreground;
   gsetcolorb(gm_background);
   gsetcolorf(gm_foreground);
   #endif

   if ((cursorindex < topindex) || (cursorindex >= (topindex+lines)))
      {
      app->cursoridx = 0;
      app->cursorvisible = 0;
      }
   else
      {
      app->cursoridx = topindex-cursorindex;
      app->cursorvisible = 1;
      }

   /**** Parameters checked and adjusted. Create frame and draw menu content ****/

   #ifdef GBUFFER
   oldupd = gsetupdate(0);   /* Delayed update */
   #endif
   app->mnu = (void *) md;
   app->txtptr = (const char *) elementdat; /* Storage to menu data */
   app->buf = NULL;

   width = lineleft+width+scrlbw;
   h = lines*lineheight;
   wnvpcreate_pos(vp,x,y,width,h);

   gsetcolorb(gm_background);
   gsetcolorf(gm_foreground);

   /* Setting to ease text line output (auto clear, ignoring \n) */
   gsetmode(ggetmode()|(GLINECUT|GVPCLR|GNOSCROLL));
   if (scrlbw > 0)
      {
      /* Append viewport scrollbar (use absolute x,y coordinates) */
      wnscrollbv_init(&(md->ctrl.scrollarea),
         gvpxr(vp)+md->style->xroffset, gvpyt(vp)-md->style->ytoffset, gvpyb(vp)+md->style->yboffset,
         md->ctrl.maxidx,
        (app->cursorvisible) ? md->ctrl.cursoridx : md->ctrl.topidx,
        (app->cursorvisible) ? md->ctrl.cursoridx : md->ctrl.topidx+md->ctrl.lines-1);
      }

   /* Call elementfunc once to signal init of menu */
   if ((md->elementfunc)(MENU_INIT,(void*)(app->txtptr))==0)
      {
      /* Draw menu content and set scroll bars if used */
      gm_list_update(vp);
      }

   #ifdef GBUFFER
   gsetupdate(oldupd);  /* Restore delayed update */
   #endif
   gselvp(oldvp);
   return 0;
   }

/*
   Draw inner content of scroll menu in accordance with settings.
   Adjust scrollbar accordingly
*/
void gm_list_update(SGUCHAR vp)
   {
   MENUINDEX i;
   PMENUDYN md;
   PGVPAPP app;     /* Pointer to application specific part of viewport */
   GXT xb,xe;
   GYT yb,ye;
   SGUCHAR oldvp,line;
   GXT scrlbw;
   #ifdef GBUFFER
   GUPDATE oldupd;  /* Only need the variable if buffer mode is defined */
   #endif
   if (vp >= GNUMVP)
      return; /* Not a valid viewport */

   oldvp = gselvp(vp);
   app = ggetapp_vp(vp);
   for(;;) /* Dummy loop for error break */
      {
      if ((md = app->mnu) == NULL)
         break;
      if (md->elementfunc == NULL)
         break;
      #ifdef GBUFFER
      oldupd = gsetupdate(0);   /* Delayed update */
      #endif
      gsetcolorb(gm_background);
      gsetcolorf(gm_foreground);
      ggetvp(&xb,&yb,&xe,&ye);
      scrlbw = md->ctrl.scrollarea.rx - md->ctrl.scrollarea.lx; /* 0 if no scrollbars */
      for (i=md->ctrl.topidx, line=0;i<md->ctrl.topidx+md->ctrl.lines; i++,line++)
         {
         #ifdef GHW_USING_COLOR
         GCOLOR cf,cb;
         #endif
         if (i == md->ctrl.cursoridx)
            { /* Prepare for cursor draw */
            app-> cursoridx = md->ctrl.topidx-i; /* Relative offset in view */
            #ifdef GHW_USING_COLOR
            if (app->cursorvisible)
               {
               /* Make saved style color active */
               cf = gsetcolorf(md->ctrl.cursor_foreground);
               cb = gsetcolorb(md->ctrl.cursor_background);
               }
            #endif
            }
         /* Narrow viewport to one line (relax drawing funcion requirements) */
         gsetvp(xb,       yb+md->ctrl.lineheight*line,
                xe-scrlbw,yb+md->ctrl.lineheight*(line+1)-1);
         if (lineleft || linetop)
            gsetpos(lineleft,ggetfh()-1+linetop);
         if ((md->elementfunc)(i,(void*)(app->txtptr))!=0)
            break; /* Premature end of drawing */
         #ifdef GHW_USING_COLOR
         if (i == md->ctrl.cursoridx)
            {
            if (app->cursorvisible)
               {
               gsetcolorf(cf);
               gsetcolorb(cb);
               }
            }
         #endif
         }
      if (scrlbw > 0)
         { /* Draw updated scroll bar */
         wnscrollbv_draw(&(md->ctrl.scrollarea), md->ctrl.maxidx,
           (app->cursorvisible) ? md->ctrl.cursoridx : md->ctrl.topidx,
           (app->cursorvisible) ? md->ctrl.cursoridx : md->ctrl.topidx+md->ctrl.lines-1);
         }
      gsetvp(xb,yb,xe,ye);
      app->has_focus = 1; /* Make focus as default */

      /* Add cursor */
      #ifdef GBUFFER
      gsetupdate(oldupd);  /* Restore delayed update */
      #endif
      break;
      }
   gselvp(oldvp);
   }

/* Close menu */
void gm_list_close(SGUCHAR vp)
   {
   PMENUDYN md;
   PGVPAPP app;     /* Pointer to application specific part of viewport */
   app = ggetapp_vp(vp);
   if ((md = app->mnu) != NULL)
      {
      if (md->elementfunc != NULL)
         (md->elementfunc)(MENU_CLOSE,(void*)(app->txtptr));
      /* Free ressource references */
      app->mnu = NULL;
      app->txtptr = NULL;
      app->cursoridx = 0;
      app->cursorvisible = 0;
      app->has_focus = 0;
      }
   /* Close visible screen and restore background */
   wnvpclose(vp);
   }

/**** List control functions *****/

/*
   Make a relative move of the cursor or view

   If cursor is enabled the movement is relative to the current
   cursor position.
   If cursor is disabled (viewport) the movement is relative to
   the current top view position.

   Return the new absolute position (cursor or top)
*/
static MENUINDEX gmi_list_scroll(SGUCHAR vp, MENUINDEX relative, SGUCHAR cur_top)
   {
   PMENUDYN md;
   PGVPAPP app;     /* Pointer to application specific part of viewport */
   MENUINDEX cur;   /* Maximum index (for bottom element) */
   MENUINDEX top;   /* Current index for top element */
   MENUINDEX lines; /* Current index for top element */
   app = ggetapp_vp(vp);
   if ((md = app->mnu) == NULL)
      return 0;
   cur = md->ctrl.cursoridx;   /* Maximum index (for bottom element) */
   top = md->ctrl.topidx;      /* Current index for top element */
   lines = (MENUINDEX)((SGUINT)md->ctrl.lines)-1;

   if (cur_top)
      {
      /* Cursor has preceedence */
      cur += relative;
      /* Limit range */
      if (cur < 0)
          cur = 0;
      else
      if (cur > md->ctrl.maxidx)
          cur = md->ctrl.maxidx;
      /* Let top index track if cursor move outside view */
      if (cur < top)
         top = cur; /* Let top track cursor up */
      else
      if (cur > top+lines)
         top = cur-lines;
      lines = cur;
      }
   else
      {
      /* View has preceedence */
      top += relative;
      /* Limit range */
      if (top < 0)
          top = 0;
      else
      if (top > md->ctrl.maxidx-lines)
          top = md->ctrl.maxidx-lines;
      /* Let cursor index track if become outside view */
      if (cur < top)
          cur = top; /* Let top track cursor up */
      else
      if (cur > top+lines)
          cur = top-lines;
      lines = top;
      }
   /* Detect changes */
   if ((top != md->ctrl.topidx) || (cur != md->ctrl.cursoridx))
      {
      /* View has changed. Make an update */
      md->ctrl.cursoridx = cur;
      md->ctrl.topidx = top;
      gm_list_update(vp);
      }
   else
      /* No view move, just update key state look */
      wnscrollbv_key_state(&(md->ctrl.scrollarea),md->ctrl.scrollarea.ks);
   return lines; /* absolute position (topidx or curidx) */
   }

/*
   Scroll list menu a relative position up or down.
   Returns the current absolute position. Scroll overflow is prevented.
*/
MENUINDEX gm_list_scroll(SGUCHAR vp, MENUINDEX relative)
   {
   return gmi_list_scroll(vp, relative, (ggetapp_vp(vp)->cursorvisible != 0));
   }


/*
   Set top index to a new position
   (Let cursor track)
*/
MENUINDEX gm_list_set_top_index(SGUCHAR vp, MENUINDEX topidx)
   {
   PMENUDYN md;
   if ((md = ggetapp_vp(vp)->mnu) != NULL)
      {
      if (topidx != md->ctrl.topidx)
         return gmi_list_scroll(vp,topidx - md->ctrl.topidx, 0);
      }
   return 0;
   }


/*
   Set cursor index to a new position
   (Let top view track)
*/
MENUINDEX gm_list_set_cur_index(SGUCHAR vp, MENUINDEX curindex)
   {
   PMENUDYN md;
   if ((md = ggetapp_vp(vp)->mnu) != NULL)
      {
      if (curindex != md->ctrl.cursoridx)
         return gmi_list_scroll(vp,curindex - md->ctrl.cursoridx, 1);
      }
   return 0;
   }

/*
   Enable/disable visual cursor. Returns the old cursor state
*/
SGBOOL gm_cursor_enable(SGUCHAR vp, SGBOOL on)
   {
   PGVPAPP app = ggetapp_vp(vp);
   SGBOOL curena = app->cursorvisible;
   if (curena != on)
      {
      app->cursorvisible = on;
      gm_list_update(vp); /* Just redraw to turn cursor "look" on or off */
      }
   return curena;
   }

/**** List property report functions *****/

/*
   Return index for cursor
*/
MENUINDEX gm_list_get_cur_index(SGUCHAR vp)
   {
   PMENUDYN md;
   if ((md = ggetapp_vp(vp)->mnu) != NULL)
      return md->ctrl.cursoridx;
   return 0;
   }

/*
   Return index for line at top view
*/
MENUINDEX gm_list_get_top_index(SGUCHAR vp)
   {
   PMENUDYN md;
   if ((md = ggetapp_vp(vp)->mnu) != NULL)
      return md->ctrl.topidx;
   return 0;
   }

/*
   Return maximum index for list (as defined upon creation)
*/
MENUINDEX gm_list_get_max_index(SGUCHAR vp)
   {
   PMENUDYN md;
   if ((md = ggetapp_vp(vp)->mnu) != NULL)
      return md->ctrl.maxidx;
   return 0;
   }

/*
   Return number of visible lines
*/
MENUINDEX gm_list_get_lines(SGUCHAR vp)
   {
   PMENUDYN md;
   if ((md = ggetapp_vp(vp)->mnu) != NULL)
      return (MENUINDEX) md->ctrl.lines;
   return 0;
   }




