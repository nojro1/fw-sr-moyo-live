/*
   SPI bus driver functions
   These functions are called by the touch screen driver example

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2010
*/
#ifndef SPIDRV_H
#define SPIDRV_H

#ifdef __cplusplus
extern "C" {
#endif

/* Spi bus drivers */
void spi_cs_on( unsigned char adr );
void spi_cs_off( void );
void spi_tx( unsigned char dat );
unsigned char spi_rx( void );
void spi_reset( void );

/* Touch (interrupt) level */
unsigned char touch_poll( void );

#ifdef __cplusplus
}
#endif

#endif

