/*
   Demo example for calibration and test of touch screen hardware.

   Beside testing the actual touch interface and touch hardware
   it can also be use for evaluating the amount of noise in the touch
   screen circuit. If there is excessive noise during runtime then a 
   software noice filter can be introduced by defining TOUCH_FILTER 
   in touchdrv.c

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2010
*/
#include <gdisp.h>
#include <touchdrv.h>
#ifdef SGPCMODE
#include <stdio.h>
#include <windows.h>
#endif

/*
  Define positions of calibration points
  (Should be close to diagonal screen corners for best result)
*/
#define POINT_AX  10
#define POINT_AY  10
#define POINT_BX  GDISPW-10
#define POINT_BY  GDISPH-10

void wait(void)
   {
   #ifdef SGPCMODE
   Sleep(5);
   #endif
   }

/*
   Calibrate Touch Screen
*/
void calibrate_touch(void)
   {
   unsigned char level;
   // Reset touch driver and define max touch values (= logical screen size)
   gtouch_reset( GDISPW-1, GDISPH-1);

   // Clear full screen
   gresetvp();
   gsetmode(GALIGN_HCENTER|GALIGN_VCENTER);
   gclrvp();

   // Draw calibration point A
   gcircle(POINT_AX,POINT_AY,1,0);
   gputs("A: Press in circle");

   // Wait for touch of calibration point A
   while( gtouch_calib_A( POINT_AX, POINT_AY ) == 0)
      wait();

   // Clear calibration point A
   gsetmode(ggetmode() | GINVERSE);
   gcircle(POINT_AX,POINT_AY,1,0);
   gsetmode(ggetmode() & ~GINVERSE);
   // Wait for touch release
   do
      {
      wait();
      gtouchkp(NULL, &level, NULL, NULL);
      }
   while (level != 0);

   // Draw calibration point B
   gcircle(POINT_BX,POINT_BY,1,0);
   gputs("B: Press in circle");

   // Wait for touch of calibration point B
   while( gtouch_calib_B(POINT_BX, POINT_BY ) == 0)
      wait();

   // Clear calibration point B
   gsetmode(ggetmode() | GINVERSE);
   gcircle(POINT_BX,POINT_BY,1,0);
   gsetmode(ggetmode() & ~GINVERSE);
   // Wait for touch release
   do
      {
      wait();
      gtouchkp(NULL, &level, NULL, NULL);
      }
   while (level != 0);
   // Calibration done
   }

void main(void)
   {
   ginit();

   // Calibrate screen
   calibrate_touch();

   gputs("** Calibration Done **");

   /* Test touch circuit */
   for(;;)
      {
      unsigned char edge,level;
      GXT x;
      GYT y;
      if (gtouchkp(&edge, &level, &x, &y))
         {
         /* Change detected */
         if (level)
            /* Draw pixels while in touch down state */
            gsetpixel(x,y,1);

         #ifdef SGPCMODE
         printf("\nE=%u,L=%u, X=%3u,Y=%3u",
            (unsigned int) edge,
            (unsigned int) level,
            (unsigned int) x,
            (unsigned int) y);
         #endif
         }
      #ifdef SGPCMODE
      Sleep(5);
      #endif
      }


   }
