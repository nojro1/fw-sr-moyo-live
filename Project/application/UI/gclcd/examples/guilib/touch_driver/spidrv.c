/*
   Simulate a SPI bus via processor pins
   These functions are called by the touch screen driver.

   Modify the body of these functions to fit your target hardware

   For illustration the portable SG syntax for I/O operations are used 
   in the example function.
   You should modify the code to use your compilers native syntax for I/O operations.

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2010
*/
#include <spidrv.h>

/* Define SPI bus bit locations in the output register
   (modify to fit the actual hardware) */
#define  SCLK  0x08   /* SPI clock bit (from processor to device clk in */
#define  SDI   0x10   /* SPI data in bit  (from device data out to processor data in) */
#define  SDO   0x20   /* SPI data out bit (from processor data out to device data in) */
#define  TCSN  0x40   /* Touch controller chip select (active low) */
#define  TINT  0x80   /* Touch controller touch interrupt (active low) */

/*#include < Port declaration file for your compiler > */
#include <sgio.h> /* or use the SG function syntax */

/* Port registers used, port functionality: */
/* P1SET  Port set register 1 = set, 0 = unchanged (== OR operation) */
/* P1CLR  Port clr register 0 = clr, 1 = unchanged (== AND operation) */
/* P1IN   Port Read pin directly */
/* P1DIR  Port direction register 1 = Output, 0 = Input */

/*
   Activate chip select
*/
void spi_cs_on( unsigned char adr )
   {
   /* (Insert mask of TINT interrupt here if interrupt is used) */

   /* Assure clock and data is low */
   sgwrby(P1CLR,~(SDO|SCLK));

   /* Set CS low for SPI device (here only one, so we ignore the address ) */
   adr++; /* Normally selects between multiple SPI devices, 
             here just a dummy operation to silence not used warnings */
   sgwrby(P1CLR,~TCSN);
   }

/*
   De-activate chip select
*/
void spi_cs_off( void )
   {
   /* Set CS high for (all) SPI device(s) (here only one) */
   sgwrby(P1SET,TCSN);

   /* (Insert reenable of TINT interrupt here if interrupt is used) */
   }

/*
   Send SPI byte
*/
void spi_tx( unsigned char dat )
   {
   /* Clock out 8 bits */
   /* Loop while clocking data out, msb first */
   register SGUCHAR msk;
   msk=0x80;
   do
      {
      if (((dat & msk) != 0))
         sgwrby(P1SET,SDO);    /* data high  (other bits is unchanged) */
      else
         sgwrby(P1CLR,~SDO);   /* data low  (other bits is unchanged) */
      sgwrby(P1SET,SCLK);      /* clock high (other bits is unchanged) */
      msk >>= 1;
      sgwrby(P1CLR,~SCLK);     /* clock low  (other bits is unchanged) */
      }
   while(msk != 0);
   sgwrby(P1CLR,~SDO);         /* Assure low, if next operation is read */
   }

/*
   Receive SPI byte
*/
unsigned char spi_rx( void )
   {
   /* Clock in 8 bits */
   register SGUCHAR msk,dat;
   /*  Loop while clocking data in, msb first */
   dat = 0;
   msk=0x80;
   sgwrby(P1CLR,~(SCLK|SDO));     /* Assure low */
   do
      {
      sgwrby(P1SET,SCLK);        /* clock high (other bits is unchanged) */
      if ((sgrdby(P1IN) & SDI) != 0) /* Get data */
         dat |= msk;
      sgwrby(P1CLR,~SCLK);      /* clock low  (other bits is unchanged) */
      msk >>= 1;
      }
   while(msk != 0);
   return dat;
   }

/*
   Reset SPI ports for use by touch controller
*/
void spi_reset( void )
   {
   /* Init data port setup (if required by port architecture) */
   /* Make A0,CS,SDA,SCLK,RST to outputs (if required by port architecture) */
   /* Set SCLK SDA CS high and SDI and TINT is input */
   sgwrby(P1DIR, TCSN|SCLK|SDO);  /* Set port directions (1 = output) */
   sgwrby(P1SET, TCSN);           /* Disable (all) CS line(s) */
   sgwrby(P1CLR, ~(SCLK|SDO));    /* Set SPI bus level to passive */

   /* (Insert enable of TINT interrupt here, if interrupt is used) */
   }

/*
   Poll touch interrupt pin level

   Return 1 if touch down (touch is active)
   Return 0 if touch up   (touch is passive)
*/
unsigned char touch_poll( void )
   {
   /* Read level of Touch pin */
   return ((sgrdby(P1IN) & TINT) == 0) ? 1 : 0;
   }


