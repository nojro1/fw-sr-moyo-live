This directory contains a touch screen driver example for use of
the TSC2046 or ADS4876 4 wire (SPI bus) touch screen controller.

The demo in this directory also illustrates how a touch screen can
be runtime calibrated.
For more detailed information please read the touchdrw.c header.

