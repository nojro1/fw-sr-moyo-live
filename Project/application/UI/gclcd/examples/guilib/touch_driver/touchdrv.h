/*
   Touch screen driver with runtime calibration

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2010
*/
#ifndef TOUCHDRV_H
#define TOUCHDRV_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
   {
   signed long  xscale; /* x scaling value (fixed point value * FIXEDPOINTSCALE) */
   signed long  yscale; /* y scaling value (fixed point value * FIXEDPOINTSCALE) */
   signed short mxca;   /* Measured X value for scaling point A */
   signed short myca;   /* Measured Y value for scaling point A */
   signed short xca;    /* Logical X value for scaling point A */
   signed short yca;    /* Logical Y value for scaling point A */
   } TOUCH_CALIB;

/* Storage for touch calibration data. Located in touchdrv.c */
extern TOUCH_CALIB touch_calib;

unsigned char gtouchkp(unsigned char *edgep, unsigned char *levelp, GXT *xp, GYT *yp);
void gtouch_reset( GXT xmax, GYT ymax );
unsigned char gtouch_calib_A( GXT x1, GYT y1 );
unsigned char gtouch_calib_B( GXT x2, GYT y2 );

#ifdef __cplusplus
}
#endif

#endif


