/***************************************************************************

   TOUCH SCREEN INTERFACE DRIVER.
   -----------------------------

   Touch screen controller
   -----------------------
   This driver assume use of the TSC2046 or ADS4876 4 wire (SPI-bus) touch screen controller.
   However the function  touch_measure() contains the only touch controller specific 
   code, so any touch controller chip can be used by updating just this function.

   Fetch a touch screen event (or check for change)
   ------------------------------------------------
   The function gtouchkp(..) provides a generic interface driver for touch
   screens. If handles touch screen data scaling and normalization so it
   fits the pixel size of the screen.
   This implementation can also be compiled for PC mode and use of the PC LCD simulator.

   Touch screen calibration
   ------------------------
   This driver include functions for runtime calibration of the touch screen data.
   The calibration method is based on two diagonal calibration points
   which the user must press, one after the other.
   For best result the calibration points must be placed diagonally
   on the screen (but not nessesarily at the outermost corners).

   The algotitm used provides a ratiometric scaling which enables the touch
   screen hardware and driver chip to have any bit resolution, offset value,
   value range, and coordinate mirroring.

   Example on how to calibrate :
   Assuming that the calibration points is 5 pixels from the edges
   then this calibration procedure can be used:

      // Calibration begin,
      gresetvp();                          // Clear full screen
      gclrvp();
      gtouch_reset( GDISPW-1, GDISPH-1);   // Reset touch driver and define max touch values (= logical screen size)
      gsetpixel(5,5);                      // Draw calibration point A
      while( gtouch_calib_A( 5, 5 ) == 0); // Wait for touch of calibration point A
      gclrvp();
      gsetpixel(GDISPW-5,GDISPH-5);        // Draw calibration point B
      while( gtouch_calib_B(GDISPW-5, GDISPH-5 ) == 0); // Wait for touch of calibration point B
      // Calibration done

   Save and restore of touch screen calibration data
   -------------------------------------------------
   The public TOUCH_CALIB touch_calib structure is the only part which should
   survive a power-down / power-up cycle. 

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2010

***********************************************************************************/
#include <gdisp.h>    /* GXT, GYT, GDISPW, GDISPH definitions */
#include <touchdrv.h>

static unsigned short mx, my; /* Raw touch, measured x and y values */

#ifndef GHW_NOHDW
#include <spidrv.h>

/**** Start of TSC2046 / ADS4876 specific code ****/

/*
   Fetch raw X,Y touch measurement values

   (Note: the actual x,y ranges and offset values for for the touch controller
   is don't care as this will be automatically compensated during the calibration.
   Also any 0 or 180 degree touch pad rotation, compared to the screen, is don't care)

   If the touch x,y values are swapped compared to the logical screen x,y values
   then just swap the MEASURE_Y, MEASURE_X command below.
*/
/* TSC2046 / ADS4876  commands */
#define  MEASURE_Y 0xd0  /* Channel 5, differential measurement, 12 bit shifted 3 positions*/
#define  MEASURE_X 0x90  /* Channel 1, differential measurement, 12 bit shifted 3 positions*/

static void touch_measure(void)
   {
   spi_cs_on(0);  /* Assume 0 is the touch SPI /CS address */
   spi_tx( MEASURE_Y );
   my =  ((unsigned short) spi_rx()) << 8;
   my |= ((unsigned short) spi_rx());
   spi_tx( MEASURE_X );
   mx =  ((unsigned short) spi_rx()) << 8;
   mx |= ((unsigned short) spi_rx());
   spi_cs_off();
   // right align values
   mx >>= 3;
   my >>= 3;
   }

/**** End of TSC2046 / ADS4876 specific code ****/
#endif

/*
   Fetch a touch screen event (or check for change)

   unsigned char gtouchkp(unsigned char *edgep, unsigned char *levelp, GXT *xp, GYT *yp);

   Pointers for return parameters:
      unsigned char *edge    Pointer to touch press change detection (1 = new event i.e. an edge).
      unsigned char *levelp  Pointer to level parameter (1 = is pressed, 0 = is not pressed)
      GXT *xp  X coordinate for (last) pressed position, normalized to screen width
      GYT *yp  Y coordinate for (last) pressed position, normalized to screen height

   Return value:
      1 = change in pressed state or change in x,y position
      0 = no change.
*/

static unsigned short maxx, maxy, oldx, oldy;
static char oldlevel;
#define FIXEDPOINTSCALE 4096  /* 12 bit decimal resolution */

/*
   Touch screen calibration value table.

   If dynamic runtime calibration is used the values below should
   normally be saved to, for instance, an EEPROM and then restored 
   after reset, before use of the touch screen.

   Otherwise the initialization values below should be assigned valid
   calibration values for the given screen so the structure is
   pre-initialized by the compilers startup initialization.
 */
TOUCH_CALIB touch_calib = { 1,1,0,0,0,0 };

#define TOUCHDOWN 1
#define TOUCHUP   0

/* Define use of a software noise filter. 
   = 0 => No noise filter 
   1-n => Noise filter is used. Defines number of averanged measurements
*/
#define TOUCH_FILTER 5

unsigned char gtouchkp(unsigned char *edgep, unsigned char *levelp, GXT *xp, GYT *yp)
   {
   unsigned char changed;

   #ifdef GHW_NOHDW
   /* Use no hardware */
   #ifdef GHW_PCSIM
   /* Use PC simulator */
   unsigned short x,y;
   changed = GSimTouchGet(edgep, &oldlevel, &x, &y);
   oldx = x;
   oldy = y;
   my = y; /* Just for test */
   mx = x;

   #else

   /* Just a stub, insert passive values */
   changed = 0;
   oldx = 0;
   oldy = 0;
   oldlevel = 0;
   
   #endif /* GHW_PCSIM */

   #else /* GHW_NOHDW */ 

   /* Use target hardware */
   changed = 0;
   for(;;)
      {
      if (touch_poll())
         {
         /* Touch down, do X,Y measurement */
         #if (TOUCH_FILTER > 0)
         /* Averange multiple measurements to reduce noise */
         unsigned int xa,ya,i;
         for (i=0,xa=0,ya=0; i < TOUCH_FILTER; i++)
            {
            touch_measure();
            xa += (unsigned int) mx;
            ya += (unsigned int) my;
            }
         my = (unsigned short) (ya/TOUCH_FILTER);
         mx = (unsigned short) (xa/TOUCH_FILTER);
         #else
         /* Do a single x,y measurement */
         touch_measure();
         #endif

         if (touch_poll())
            {
            /* Still in touch down mode after conversion, assume valid data */
            signed short x,y;
            /*printf("\nx,y= %u, %u",(unsigned int) mx,(unsigned int) my);*/

            /*
               Logical_point = ((Scale_factor * (measured_point - measured_A))/FIXEDPOINTSCALE) + logical_A
            */
            x = (signed short)
                (((((signed long) ((unsigned long) mx)) - (signed long) touch_calib.mxca) *
                   touch_calib.xscale) / ((signed long) FIXEDPOINTSCALE))
                   + touch_calib.xca;

            y = (signed short)
                (((((signed long) ((unsigned long) my)) - (signed long) touch_calib.myca) *
                   touch_calib.yscale) / ((signed long) FIXEDPOINTSCALE))
                   + touch_calib.yca;

            /* Force valid limits (just in case of any scale errors or noise) */
            if (x < 0)
               x = 0;
            else if (x > (signed int) maxx)
               x = (signed int) maxx;
            if (y < 0)
               y = 0;
            else if (y > (signed int) maxy)
               y = (signed int) maxy;

            /* Check for drag event */
            if ((oldlevel == TOUCHDOWN) &&
               ((unsigned short) x == oldx) && ((unsigned short) y == oldy))
               {
               /* No drag x,y change, exit */
               if (edgep) *edgep = 0;  /* No change in touch level */
               break;
               }

            if (edgep)
               *edgep = (oldlevel == TOUCHUP) ? 1 : 0;  /* Change in touch level ? */

            /* Save state for change detection */
            oldlevel = TOUCHDOWN;
            oldx = (unsigned short) x;
            oldy = (unsigned short) y;

            changed = 1; /* Signal position drag event */
            break; /* Report states */
            }
         /* else
            Skip measurement if touch-up during conversion */
         }
      if (oldlevel == TOUCHDOWN)
         {
         /* Touch down event has been signaled, now signal touch up event */
         changed = 1;
         oldlevel = TOUCHUP;
         }
      if (edgep) *edgep = changed;
      break; /* Report states */
      }
   #endif

   if (levelp) *levelp = oldlevel;
   if (xp) *xp = (GXT) oldx;
   if (yp) *yp = (GYT) oldy;
   return changed;
   }

void gtouch_reset( GXT xmax, GYT ymax )
   {
   maxx = (unsigned short) xmax;
   maxy = (unsigned short) ymax;
   oldlevel = TOUCHUP;
   oldx = 0;
   oldy = 0;
   spi_reset();

   /* Optionally here insert restore of calibration values from backup */
   }

/*
   Fetch data for first logical calibrartion point

   Returns 1 if screen pressed and data is fetched correctly
   Returns 0 if screen was not pressed or data is illegal
   (> max limits set by gtouch_reset(..))
*/
unsigned char gtouch_calib_A( GXT x1, GYT y1 )
   {
   if ((x1 <= maxx) &&  (y1 <= maxy))
      {
      unsigned char level;
      gtouchkp(NULL, &level, NULL, NULL);
      if (level == TOUCHDOWN)
         {
         touch_calib.mxca = mx;
         touch_calib.myca = my;
         touch_calib.xca = (unsigned short) x1;
         touch_calib.yca = (unsigned short) y1;
         return 1;
         }
      }
   return 0;
   }

/*
   Fetch data for second logical calibrartion point and
   do calculation of scaling values

   Returns 1 if screen pressed and calibrarition is ok
   Returns 0 if screen was not pressed or data is illegal
   (> max limits set by gtouch_reset(..))
*/
unsigned char gtouch_calib_B( GXT x2, GYT y2 )
   {
   if ((x2 <= maxx) &&  (y2 <= maxy))
      {
      unsigned char level;
      gtouchkp(NULL, &level, NULL, NULL);
      if (level == TOUCHDOWN)
         {
         /*
           Scale_factor = (logical_B - logical_A) / (measured_B - measured_A)
           To minimize loss of precision and rounding errors we use a fixed point value (*FIXEDPOINTSCALE)
         */
         touch_calib.xscale = ((((signed long)x2) - ((signed long)touch_calib.xca))*FIXEDPOINTSCALE) / (mx - touch_calib.mxca);
         touch_calib.yscale = ((((signed long)y2) - ((signed long)touch_calib.yca))*FIXEDPOINTSCALE) / (my - touch_calib.myca);
         return 1;
         }
      }
   return 0;
   }

