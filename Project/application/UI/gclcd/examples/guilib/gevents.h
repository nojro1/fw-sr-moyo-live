/******************************* gevents.h ************************************

   Definition of touch screen event identifiers.

   Primay events generated by touch screen, keyboard and timer hardware (keyintf.c)
         EVENT_TOUCH_DOWN,        Basic Touch down event
         EVENT_TOUCH_UP,          Basic Touch up event
         EVENT_TOUCH_DRAG,        Basic Touch drag
         EVENT_TICK,              Timer tick

         EVENT_KEY_RIGHT,         Keyboard cursors
         EVENT_KEY_LEFT,
         EVENT_KEY_UP,
         EVENT_KEY_DOWN,
         EVENT_KEY_RETURN,        Keyboard selection
         EVENT_KEY_ESC,           Keyboard esc

   Derived events created by touch event handler  (gtouchevent.c)
         EVENT_TOUCH_DOWN_REPEAT, Touch down (timing) repeat
         EVENT_TOUCH_FOCUSLOST,   Focus lost event

   Touch-area Init, Update and Closure events used by the demo application
         EVENT_TOUCH_CREATE,      Create touch area (first time draw)
         EVENT_TOUCH_UPDATE,      Explicit update of the content of an area
         EVENT_TOUCH_CLOSE,       Close touch area (final event)

   Other application / target hardware specific events may be added here

   2010 RAMTEX Engineering ApS

********************************************************************************/
#ifndef GEVENTS_H
#define GEVENTS_H

#include <gdisp.h>
#ifdef __cplusplus
   extern "C" {
#endif

/*
   Graphic key and touch event
*/
typedef enum
   {
   EVENT_IDLE,              /* No action, Not in area, key not in active state */

   /* Touch events */
   EVENT_TOUCH_DOWN,        /* Basic Touch down event */
   EVENT_TOUCH_UP,          /* Basic Touch up event */
   EVENT_TOUCH_DRAG,        /* Basic Touch drag */

   /* Touch events, processed relative to a sence area */
   EVENT_TOUCH_DOWN_REPEAT, /* Touch down (timing) repeat (if implementation responce on EVENT_TICK) */
   EVENT_TOUCH_FOCUSLOST,   /* Focus lost event: Area had focus and (touch down
                               outside area or drag to outside) */

   /* Creation and close event (allocate, free ressources) */
   EVENT_TOUCH_CREATE,      /* Create touch area (first time draw) */
   EVENT_TOUCH_UPDATE,      /* Explicit update of the content of an area */
   EVENT_TOUCH_CLOSE,       /* Close touch area (final event)   EVENT_KEY_NOEVENT,

   /* Key board events */
   EVENT_KEY_UP,
   EVENT_KEY_DOWN,
   EVENT_KEY_LEFT,
   EVENT_KEY_RIGHT,
   EVENT_KEY_RETURN,
   EVENT_KEY_ESC,

   /* Derivative event (signal that a menu selection is done) */
   EVENT_SELECTION,

   /* Update tick (for dynamic update views, key down repeat) */
   EVENT_TICK,

   /* End of legal events */
   EVENT_LAST
   } EVENT;

#define  EVENT_KEY_NOEVENT   EVENT_IDLE
/* Touch click detected */
#define  EVENT_TOUCH   EVENT_TOUCH_DOWN

extern GXT touch_x;
extern GYT touch_y;
EVENT wm_keyintf(void);
void wm_keyintf_init(void);

/* mnu_list_event.c (for menu list in mnu_list.c) */
EVENT gm_list_event(SGUCHAR vp, EVENT event, SGINT *selection);
/* wnkey_event.c (for soft key in wnkey.c)*/
EVENT gm_key_event(SGUCHAR vp, EVENT event, SGINT *selection);

#ifdef __cplusplus
   }
#endif
#endif

