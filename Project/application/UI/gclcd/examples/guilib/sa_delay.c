/*
   Implements a delay funtionality

   When used in connection with the PC simulator a
   demo can be aborted in 3 ways:
      By closing the LCD simulator
        By pressing Esc key when the simulator has focus
        By pressing Ctrl-F4 key when the simulator has focus

   When used in connection with the PC simulator a
   delay can be aborted by pressing any key different
   from the above.

   For PC-simulation the compiler switches GHW_PCSIM must be defined
   to enable the abort keys and the SGPCMODE must be defined to
   make the delay.
*/

#ifdef SGPCMODE

#ifdef _WIN32
#include <windows.h>
#endif

#ifdef GHW_PCSIM
#include <gsimintf.h>
#include <gkeycode.h>
#endif

#endif

#include <gdisp.h>

int sa_delay( unsigned int ms )
   {
   #ifdef SGPCMODE
   for(;;)
      {
      #ifdef GHW_PCSIM
      if (GSimKbHit())
         {
         SGUINT val;
         val = GSimKbGet();
         if (( val == GK_ESC) || (val == (GK_F4|GK_CTRL)))
            {
            exit(0);     /* User break request */
            }
         return 1;       /* User continue request */
         }
      #endif

      if (ghw_err())
         {
         exit(0);     /* Hardware error or simulator stopped */
         }

      #ifdef _WIN32
      if (ms <= 100)
         {
         Sleep(ms);
         break;
         }
      else
         {
         Sleep(100);
         ms -= 100;
         }
      #endif
      }
   #else
   /* Implement target processor specific delay here */
   #endif
   return 0;
   }

