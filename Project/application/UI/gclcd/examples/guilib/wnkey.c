/*
   Functions for soft key drawing

   Demonstrates how to create a simple software key event handling
   based on a viewport + a NULL terminated array of one or more pointers
   to text strings holding the key text.
   If the key text array contains of more than one text string a
   a key with multi toggle select is optained.

   Each active (visible) key must use an individual viewport.
   The key look, position, state and content information is stored
   in the viewport plus extension.

   A common key look style is used.

    gm_key_init(..)          Create key, draw first time
    gm_set_key_pressed(..)   Draw pressed / non presseed key look
    gm_set_key_text(..)      Select the text to be shown (assuming that
                             the texts are defined by a text pointer array)
*/
#include <wnmenu.h>

/* Check that library configuration is right */
#ifndef GEXTMODE
  #error This module require that GEXTMODE is enabled (defined) in gdispcfg.h
#endif

static GSTYLE key_style =
    {GMFONT,0,(GVPCLR|GLINECUT|GALIGN_HCENTER|GALIGN_VCENTER),0,FR_PLATE,FR_NONE,
    #ifdef GEXTMODE
    0,
    #endif
    1,1,1,1};

/*
   Create a new key based on the style settings
*/
void gm_key_init(SGUCHAR vp,
            GXT x, GYT y,         /* Location information */
            GXT width, GYT height, /* Key size information */
            const char **keytext   /* Pointer to a NULL terminated array with keytext(s) */
            )
   {
   PGVPAPP app;     /* Pointer to application specific part of viewport */
   SGUCHAR oldvp;
   PGSTYLE oldstyle;
   #ifdef GBUFFER
   GUPDATE oldupd;  /* Only need the variable if buffer mode is defined */
   #endif

   if (vp >= GNUMVP)
      return; /* Not a valid viewport */

   oldvp = gselvp(vp);
   oldstyle = wnsel_style(&key_style);
   if (height == 0)
      {  /* Set height to one text line */
      gselfont(key_style.font);
      height = ggetfh();
      }

   app = ggetapp_vp(vp);
   #ifdef GBUFFER
   oldupd = gsetupdate(0);   /* Delayed update */
   #endif
   /* Set key colors to match the frame type */
   gsetcolorf(wnframe_colorf(FR_PLATE));
   gsetcolorb(wnframe_colorb(FR_PLATE));

   /* Draw key area */
   wnvpcreate_pos(vp,x,y,width,height);

   /* Set application specific key content and indexes */
   app->cursoridx = 0;
   app->cursorvisible = 0; /* Signal up/down state look */
   app->txtptr = (const char *) keytext;  /* Storage to menu data */
   app->mnu = NULL;        /* Not used here, just for completeness */
   app->buf = NULL;

   gm_set_key_text(vp,0);    /* Reset key text */
   gm_set_key_pressed(vp,0); /* Reset key frame to not pressed */

   #ifdef GBUFFER
   gsetupdate(oldupd);  /* Restore delayed update */
   #endif
   wnsel_style(oldstyle);
   gselvp(oldvp);
   }

/*
   Update key frame to signal key up/down state
*/
void gm_set_key_pressed(SGUCHAR vp, SGUCHAR pressed)
   {
   SGUCHAR fs = wnframe_thicknes(FR_PLATE);
   GXT xb,xe;
   GYT yb,ye;
   PGVPAPP app;     /* Pointer to application specific part of viewport */
   SGUCHAR oldvp;

   app = ggetapp_vp(vp);
   if (app->mnu != NULL)
      return; /* Not a key vp, skip */

   /* Draw key state */
   oldvp = gselvp(vp);
   ggetvp(&xb,&yb,&xe,&ye);
   gresetposvp();                 /* Use absolute coordinates */
   wnframe(xb-(fs+key_style.xloffset),
           yb-(fs+key_style.ytoffset),
           xe+(fs+key_style.xroffset),
           ye+(fs+key_style.yboffset),
           pressed ? FR_INSERT : FR_PLATE);
   gsetvp(xb,yb,xe,ye);           /* Restore to key default */

   /* Update key state setting */
   app->cursorvisible = pressed;
   app->has_focus = pressed ? 1 : 0; /* Just for completeness */
   gselvp(oldvp);
   }

/*
  Update or set key text area.
  Returns new current index
*/
SGUCHAR gm_set_key_text(SGUCHAR vp, SGUCHAR index)
   {
   SGUCHAR oldvp;
   PGVPAPP app;        /* Pointer to application specific part of viewport */
   const char **ppstr; /* Pointer to array of key content possibilities */
   #ifdef GBUFFER
   GUPDATE oldupd;  /* Only need the variable if buffer mode is defined */
   #endif
   app = ggetapp_vp(vp);
   if (app->mnu != NULL)
      return 0; /* Not a key vp */
   oldvp = gselvp(vp);
   #ifdef GBUFFER
   oldupd = gsetupdate(0);   /* Delayed update */
   #endif

   if ((ppstr = (const char **)app->txtptr) != NULL)
      {
      int i;
      for (i=0;ppstr[i]!=NULL;i++)
         {
         if (i == index)
            {
            /* Update key area */
            gputs(ppstr[i]);
            app->cursoridx = index;
            }
         }
      }

   #ifdef GBUFFER
   gsetupdate(oldupd);  /* Restore delayed update */
   #endif
   gselvp(oldvp);
   return app->cursoridx;
   }

/*
   Return the key pressed, not pressed mode state.
     0 = not pressed
     1 = pressed
*/
SGUCHAR gm_get_state(SGUCHAR vp)
   {
   return ggetapp_vp(vp)->cursorvisible;
   }

/*
   Return the key text index (toggle index)
     0 = first element
     1 = next element (if two or more elements)
    etc
*/
SGUCHAR gm_get_key_index(SGUCHAR vp)
   {
   return ggetapp_vp(vp)->cursoridx;
   }


