#include <wnpopup.h>
/*
   Create a framed view port.
   Set colors and clear viewport area

   After creation the current viewport area is reduced to the
   inner area, ready for further drawings

   fs = frame style (defined in wnpopup.h)
   absolute == 0  The coordinates are viewport relative (default)
   absolute != 0  The coordinates are absolute
   inner == 0     The coordinates define the outer area (default)
                  (frame is added inwards)
   inner != 0     The coordinates define the inner area
                  (frame is added outwards)
*/
void gframedvp( GXT xb, GYT yb, GXT xe, GYT ye,
      GFRAMESTYLE fs, GCOLOR fore, GCOLOR back, char absolute, char inner)
   {
   SGUCHAR fw;

   if (absolute == 0)
      {
      /* Assume coordinates is relative to current viewport */
      /* Convert to absolute coordinates */
      SGUCHAR vp;
      vp = ggetvpnum();
      xb += gvpxl(vp);
      yb += gvpyt(vp);
      xe += gvpxl(vp);
      ye += gvpyt(vp);
      }

   fw = wnframe_thicknes( fs );

   /* Force valid cordinates, assure space for frame */
   if (inner)
      {
      if (xb+1 < fw) xb = fw-1;
      if (yb+1 < fw) yb = fw-1;
      xb-=fw;
      yb-=fw;
      if ((xe+fw) > GDISPW)
         xe = GDISPW-fw;
      else
         xe+=fw;
      if ((ye+fw) > GDISPH)
         ye = GDISPH-fw;
      else
         ye += fw;
      }
   else
      {
      if (xb+2*fw > xe) xe = xb+2*fw;
      if (yb+2*fw > ye) ye = xb+2*fw;
      }

   /* Draw frame */
   gresetvp();
   wnframe(xb,yb,xe,ye,fs);

   /* Set inner viewport */
   gsetvp(xb+fw,yb+fw,xe-fw,ye-fw);

   /* Set colors and clear area */
   gsetcolorf(fore);
   gsetcolorb(back);
   gclrvp();
   }
