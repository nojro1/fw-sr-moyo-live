/*
   Target keyboard and event driver interface for demo

   This module simulates interface to a small target keyboard with 5 or 6 buttons.
   In PC simulation mode the events are generated via the PC keyboard.
      Arrow up
      Arrow down
      Arrow right
      Arrow left
      Return (accept)
      Backspace (skip)  (may be optional for navigation)
   plus
        Esc for direct exit of PC simulation demo.

   This driver also provides an interface to a touch screen.
   In PC simulation mode touch events are simulated with mouse right-clicks
   on the LCD simulator screen.

   If the target system does not have a touch screen display then handling
   of EVENT_TOUCH events can be deleted.

   Adaptions needed to move the demo to target mode:
      The target keyboard driver should be implemented in this module.
      A (interrupt based) timestamp counter should be implemented in the timeout.c module
      A touch screen controller handler shou be implemented in the gtouchkp.c module

   RAMTEX Engineering ApS  2007-2010
*/
#include <gevents.h>
#include <wnpopup.h>
//#include <timer.h>     /* Timestamp, Timeout feature */

#ifdef GHW_PCSIM
#include <gsimintf.h>  /* Simulator functions */
#include <gkeycode.h>  /* Keys definitions */
#include <windows.h>   /* Sleep() */
#endif

/* Public variables to hold touch event x,y coordinates.
   The x,y values must have been normalized to the screen size by the driver */
GXT touch_x;
GYT touch_y;

//static TIMEOUT ticktime;

/*
    Fetch GUI event  (Keyboard, touch screen, time tick)

    Returns EVENT_KEY_NOEVENT if no GUI event is detected.
*/
EVENT wm_keyintf(void)
   {
   /* Check for touch click event */
   /* Check for touch click event */
   unsigned char edge, level;

   /* Use generic touch interface function */
   if (gtouchkp(&edge, &level, &touch_x, &touch_y))
      {
      /* Change in touch state, convert to events */
      if (edge && level)
         return EVENT_TOUCH_DOWN;
      if (edge && !level)
         return EVENT_TOUCH_UP;
      if (!edge && level)
         return EVENT_TOUCH_DRAG;
      }

   /* Check for keyboard event */
   #ifdef GHW_PCSIM
   if (GSimKbHit())
      {
      /* Get PC key and translate to demo events */
      switch (GSimKbGet())
         {
         case GK_ARROW_RIGHT:
            return EVENT_KEY_RIGHT;
         case GK_ARROW_LEFT:
            return EVENT_KEY_LEFT;
         case GK_ARROW_UP:
            return EVENT_KEY_UP;
         case GK_ARROW_DOWN:
            return EVENT_KEY_DOWN;
         case GK_ENTER:
            return EVENT_KEY_RETURN;
         case 0x08: /* Backspace */
            return EVENT_KEY_ESC;
         case GK_ESC:
            exit(1); /* Skip demo */
            break;
         default:
            break;
         }
      }

   #else
   /* Place target keyboard code here
      Check keys and generate one of these events when the related key is pressed.
       return EVENT_KEY_RIGHT;
       return EVENT_KEY_LEFT;
       return EVENT_KEY_UP;
       return EVENT_KEY_DOWN;
       return EVENT_KEY_RETURN;
       return EVENT_KEY_ESC;
   */
   #endif

   /* Check for a timing event */
   //if (timeout(&ticktime))
   //   {
   //   timeout_set(&ticktime,2);
   //   return EVENT_TICK;
   //   }

   #ifdef GHW_PCSIM
   Sleep(1); /* Leave some time to other PC applications */
   #endif

   /* else just return no event */
   return EVENT_KEY_NOEVENT;
   }

/*
    Initialize GUI event drivers here
*/
void wm_keyintf_init(void)
   {
   #ifdef GHW_PCSIM
   GSimPuts("----\n"
            "Embedded keyboard keys simulated with:\n"
            "    Arrows, Return, BackSpace\n"
            "Touch screen simulated with mouse right-click\n"
            "Exit demo with Esc");
   #else
   /* Insert init of keyboard, timer and touch controller here */
   #endif

   //timeout_set(&ticktime,0);
   }

