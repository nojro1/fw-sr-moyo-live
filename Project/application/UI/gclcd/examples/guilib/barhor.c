/*
   HORIZONTAL BAR IMPLEMENTATION EXAMPLE
   -------------------------------------
   Function examples for creating and updating a horizontal bar.
   This implementation uses the differential inversion method for
   the fastest update speed.

   The entry coordinates defines the bar frame style.

   If scale is != 0 a scale with scale number of intervals is drawn
   along the bar edge.

   The current bar level is stored in the BARPOS structure.
   The same is information about the viewport used during creation
   and information about the size of the dynamic bar area. This
   enables multiple bars to use the same viewport during creation
   and dynamic update. The current viewport number remains unchanged.

   Copyright (c) Ramtex Engineering ApS
*/
#include <gdisp.h>
#include <wnpopup.h>
/*
   Horizontal bar functions,
   The viewport setting is used during a later update
*/
BARPOS *barh_create(BARPOS *bp, GXT xb, GYT yb, GXT xe, GYT ye,
         GFRAMESTYLE fs, SGUCHAR scale)
   {
   SGUCHAR oldvp,vp;
   SGUCHAR framesize;

   if (bp == NULL)
      return bp;
   /* Force resonable parameter values */
   GLIMITU(xe,GDISPW-1);
   GLIMITU(ye,GDISPH-1);
   framesize = wnframe_thicknes(fs);
   GLIMITU(framesize,15);
   GLIMITD(xe,2*framesize);
   GLIMITD(ye,2*framesize);
   GLIMITU(xb,xe-2*framesize);
   GLIMITU(yb,ye-2*framesize);

   #ifndef G_IS_BW
   /* Take copy of current colors for bar to ease update */
   bp->fore = ggetcolorf();
   bp->back = ggetcolorb();
   #endif

   /* Create bar frame area */
   oldvp = gselvp((vp = wn_get_vpnum())); /* Change to temp vp */
   gresetvp();          /* Use default settings */
   gsetvp(xb,yb,xe,ye); /* Define bar frame area */
   #ifndef G_IS_BW
   gsetcolorf(bp->fore);
   gsetcolorb(bp->back);
   #endif
   gclrvp();
   if (framesize)
      wnframe(0, 0, ggetvpw()-1, ggetvph()-1, fs); /* Frame */

   /* Bar creation done, check if scales should be added */

   if (scale)
      {
      /* Draw scale lines inside bar area */
      SGUINT s;
      GXT x,w;
      /* Create horizontal scales */
      grectangle(1, framesize, framesize, ggetvph()-(framesize+1)); /* Nul bar */
      gsetvp(xb+framesize,yb+(framesize-1),xe-framesize,ye-framesize);
      w=ggetvpw();

      /* scale marks must be two pixels apart*/
      if (scale > (unsigned char)(w/2))
          scale = (unsigned char)(w/2);

      /* Draw marks */
      #ifndef G_IS_BW
      gsetcolorf(G_BLACK);
      #endif
      for (s=0; s <= scale; s++)
         {
         x = (GXT)(((SGUINT) w * s) / ((SGUINT) scale));
         if (x > w-1)
            x = w-1;
         ginvertvp(x,0,x,ggetvph()/2);
         }
      #ifndef G_IS_BW
      gsetcolorf(bp->fore);
      #endif
      }
   bp->scale = scale;

   /* Finish by updating the BARPOS structure */
   /* Define dynamic bar area for the bar_set function */
   bp->pos = 0;
   bp->xb = xb + framesize;
   bp->yb = yb + framesize;
   bp->xe = xe - framesize;
   bp->ye = ye - framesize;

   /* Restore old viewport */
   wn_free_vpnum(vp);
   gselvp(oldvp);
   return bp;
   }

/*
   Fast update horizontal bar graph
   The value is pixels relative to the xmax, xmin pixel range
*/
void barh_set(BARPOS *bp, GXT new_value)
   {
   unsigned char oldvp,vp;
   if (bp == NULL)
      return;

   GLIMITU(new_value, (bp->xe - bp->xb));  /* Handle overflow */
   if ((GXT)(bp->pos) == new_value)
      return;  /* Value is unchanged, return */

   /* Select bar vp */
   oldvp = gselvp((vp = wn_get_vpnum()));  /* Change to temp vp */
   gsetvp(bp->xb,bp->yb,bp->xe,bp->ye);
   #ifndef G_IS_BW
   gsetcolorf(bp->fore);
   gsetcolorb(bp->back);
   #endif

   /* Check value against bar size */
   if (new_value > ggetvpw()-1)
      new_value = ggetvpw()-1;

   if ((GXT)(bp->pos) > new_value)
      {
      /* Move position down */
      if (bp->scale)
         ginvertvp(new_value+1, 0, (GXT)(bp->pos), ggetvph()-1);  /* Use color invert to keep scale */
      else
         gfillvp(new_value+1, 0, (GXT)(bp->pos), ggetvph()-1,0xffff); /* Do it faster with fill */
      }
   else
      {
      /* Move position up */
      if (bp->scale)
         ginvertvp((GXT)(bp->pos)+1, 0, new_value, ggetvph()-1);  /* Use color invert to keep scale */
      else
         gfillvp((GXT)(bp->pos)+1, 0, new_value, ggetvph()-1,0x0000); /* Do it faster with fill */
      }

   /* Restore vp */
   wn_free_vpnum(vp);
   gselvp(oldvp);

   /* Save new value */
   bp->pos = (unsigned int) new_value;
   }


/*
   Fast update vertical bar graph
   The value is procent of full scale. Range (0-99).
   Values outside range is turn cated
*/
void barh_pct(BARPOS *bp, GXT new_value)
   {
   GBUFINT i;
   if (bp == NULL)
      return;
   if (new_value >= 99)
      new_value = 99;
   i = ((GBUFINT)(bp->xe - bp->xb))*new_value;
   barh_set(bp,(GXT)(i/100));
   }

