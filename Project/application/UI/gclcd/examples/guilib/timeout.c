/************************* timeout.c **********************************
*
*  timeout_set  Initate a TIMEOUT handler
*  timeout      Poll a TIMEOUT handler for timeout
*
*  The polled timeout handler can handle multible timeout sessions.
*  A TIMEOUT structure is initiated with a call to timeout_set(..)
*  Timeout for the structure can then be polled with calls to timeout(..)
*  (These functions are independent of hardware timer wrap-around)
*
*  The actual time is based on the time tick intervals given by timestamp().
*
*  This module is OS system and target CPU independent
*
*  Created by: RAMTEX Engineering ApS 1997 JK
*
**********************************************************************/
#include <timer.h>

/*
   Initiate a TIMEOUT handler
*/
void timeout_set( TIMEOUT *time, unsigned int wait_time )
   {
   time->wait_time = wait_time;
   time->start_time = timestamp();
   }

/*
   Poll a TIMEOUT handler for timeout.
   Returns 0 if no timeout
   Returns 1 if timeout
*/
int timeout( TIMEOUT *time )
   {
   unsigned long temp;
   temp = timestamp();
   if (tsdiff(temp, time->start_time) >= (unsigned long) time->wait_time)
      return(1);
   return(0);
   }

