/*
   Menu element fetch functions for use with the mnu_list functions.

   Example implementation for use when the menu is defined by an array of pointers to strings.
   The last element of the pointer array is expected to be NULL.
*/
#include <wnmenu.h>
#include <gdisp.h>

/*
   Dynamic menu data fetching routine.
   Format and draw data related to menu element at index
   The current viewport is preinitialized with the current menu "style" settings
   before call of this function.
*/
int mnuelem_strp(MENUINDEX index, void *element_table)
   {
   PGCSTR *strpp;
   if (index < 0)
      return 0;  /* No open or close actions needed. Skip MENU_INIT and MENU_CLOSE */
   strpp = (PGCSTR *)element_table;
   if (strpp != NULL)
      {
      if (strpp[index] != NULL)
         {
         /* Use standard text functions, element is auto cleared so just output */
         gputs(strpp[index]);
         return 0; /* OK return */
         }
      }
   return -1; /* Premature end of table reached (stop element rendering) */
   }

/*
   Menu size calculation function.
   Return size of string pointer array in number of elements.
   (Return 0 in case of error)
*/
MENUINDEX mnuelem_strp_size(PGCSTR *strpp)
   {
   MENUINDEX index = 0;
   if (strpp != NULL)
      {
      while(strpp[index] != NULL)
         index++;
      }
   return index;
   }

/*
   Menu width calculation function.
   Return maximum pixel width for all elements in string pointer arrray
   (Return 0 in case of error)
*/
SGUINT mnuelem_strp_maxwidth(PGCSTR *strpp)
   {
   SGUINT maxw = 0;
   if (strpp != NULL)
      {
      MENUINDEX index = 0;
      /* Select font for current menu "style" before doing the width calculations */
      PGFONT fontold = gselfont( wnget_style()->font );
      while(strpp[index] != NULL)
         {
         SGUINT w;
         w = gpstrlen(strpp[index++]);
         if (w > maxw)
            maxw = w;
         }
      gselfont( fontold ); /* Restore font settings */
      }
   return maxw;
   }


