/*
   VERTICAL BAR IMPLEMENTATION EXAMPLE
   -----------------------------------
   Function examples for creating and updating a vertical bar.
   This implementation uses the differential inversion method for
   the fastest update speed.

   The entry coordinates defines the bar frame style.

   If scale is != 0 a scale with scale number of intervals is drawn
   along the bar edge.

   The current bar level is stored in the BARPOS structure.
   The same is information about the viewport used during creation
   and information about the size of the dynamic bar area. This
   enables multiple bars to use the same viewport during creation
   and dynamic update. The current viewport number remains unchanged.

   Copyright (c) Ramtex Engineering ApS
*/
#include <gdisp.h>
#include <wnpopup.h>
/*
   Horizontal bar functions
*/
BARPOS *barv_create(BARPOS *bp, GXT xb, GYT yb, GXT xe, GYT ye, GFRAMESTYLE fs, SGUCHAR scale)
   {
   SGUCHAR vp, oldvp;
   SGUCHAR framesize;

   if (bp == NULL)
      return bp;
   /* Force resonable parameter values */
   GLIMITU(xe,GDISPW-1);
   GLIMITU(ye,GDISPH-1);
   framesize = wnframe_thicknes(fs);
   GLIMITU(framesize,15);
   GLIMITD(xe,2*framesize);
   GLIMITD(ye,2*framesize);
   GLIMITU(xb,xe-2*framesize);
   GLIMITU(yb,ye-2*framesize);

   #ifndef G_IS_BW
   /* Take copy of current colors for bar to ease update */
   bp->fore = ggetcolorf();
   bp->back = ggetcolorb();
   #endif

   /* Create bar frame area */
   oldvp = gselvp((vp = wn_get_vpnum()));   /* Change to temp vp */
   gresetvp();
   gsetvp(xb,yb,xe,ye);
   #ifndef G_IS_BW
   gsetcolorb(bp->back);
   gsetcolorf(bp->fore);
   #endif
   gclrvp();
   if (framesize)
      wnframe(0, 0, ggetvpw()-1, ggetvph()-1, fs); /* Frame */

   if (scale)
      {
      /* Draw scale lines */
      SGUINT s;
      GYT y,h;
      /* Create vertical scales */
      gsetvp(xb+1,yb+framesize,xe-framesize,ye-framesize);
      h=ggetvph();

      /* scale marks must be two pixels apart*/
      if (scale > (unsigned char)(h/2))
          scale = (unsigned char)(h/2);

      /* Draw marks */
      #ifndef G_IS_BW
      gsetcolorf(G_BLACK);
      #endif
      for (s=0; s <= scale; s++)
         {
         y = (GYT)(((SGUINT) h * s) / ((SGUINT) scale));
         if (y > h-1)
            y = h-1;
         ginvertvp(0,y,ggetvpw()/2,y);
         }
      #ifndef G_IS_BW
      gsetcolorf(bp->fore);
      #endif
      }
   bp->scale = scale;

   /* Finish by updating the BARPOS structure */
   /* Define dynamic bar area for the bar_set function */
   bp->xb = xb + framesize;
   bp->yb = yb + framesize;
   bp->xe = xe - framesize;
   bp->ye = ye - framesize;
   gsetvp(bp->xb,bp->yb,bp->xe,bp->ye);
   ginvertvp(0,ggetvph()-1,ggetvpw()-1,ggetvph()-1); /* Draw 0 level */
   bp->pos = bp->ye - bp->yb;

   /* Restore old viewport */
   wn_free_vpnum(vp);
   gselvp(oldvp);
   return bp;
   }

/*
   Fast update vertical bar graph
   The value is pixels relative to the xmax, xmin pixel range
*/
void barv_set(BARPOS *bp, GYT new_value)
   {
   SGUCHAR oldvp,vp;
   GYT h;
   if (bp == NULL)
      return;

   h = (bp->ye - bp->yb);
   GLIMITU(new_value, h);      /* Handle overflow */
   new_value = h - new_value;  /* Is working from bottom up */
   if ((GYT)(bp->pos) == new_value)
      return;  /* Value is unchanged, return */

   /* Select bar vp, and check value against bar size */
   oldvp = gselvp((vp = wn_get_vpnum()));  /* Change to temp vp */
   gsetvp(bp->xb,bp->yb,bp->xe,bp->ye);
   #ifndef G_IS_BW
   gsetcolorf(bp->fore);
   gsetcolorb(bp->back);
   #endif

   if ((GYT)(bp->pos) > new_value)
      {
      /* Move position up */
      if (bp->scale)
         ginvertvp(0, new_value, ggetvpw()-1, (GYT)(bp->pos)-1); /* Use color invert to keep scale */
      else
         gfillvp(0, new_value, ggetvpw()-1, (GYT)(bp->pos)-1,0xffff); /* Do it faster with fill */
      }
   else
      {
      /* Move position down */
      if (bp->scale)
         ginvertvp(0, (GYT)(bp->pos), ggetvpw()-1, new_value-1);/* Use color invert to keep scale */
      else
         gfillvp(0, (GYT)(bp->pos), ggetvpw()-1, new_value-1,0x0000);/* Do it faster with fill */
      }

   /* Restore vp */
   wn_free_vpnum(vp);
   gselvp(oldvp);

   /* Save new value */
   bp->pos = (unsigned int) new_value;
   }

/*
   Fast update vertical bar graph
   The value is procent of full scale. Range (0-99).
   Values outside range is turn cated
*/
void barv_pct(BARPOS *bp, GYT new_value)
   {
   GBUFINT i;
   if (bp == NULL)
      return;
   if (new_value >= 99)
      new_value = 99;
   i = ((GBUFINT)(bp->ye - bp->yb))*(GBUFINT)new_value;
   barv_set(bp,(GYT)(i/100));
   }

