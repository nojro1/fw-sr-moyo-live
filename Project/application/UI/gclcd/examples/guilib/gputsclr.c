/*************************** gputsclr.c **********************************

   Output a string in a viewport, with auto clear of line area in front
   or after the line where the text is located.

   Especially usefull when updating the same parameter in non-buffered
   mode (GBUFFER undefined).

   For a single line viewport this function have the same effect as
      gclrvp();
      gputs(str);
   but without the visible flickering created by the gclrvp operation.

   It is assumed that the new string is the only string on the viewport line
   and that the line does not contains \r \n characters.

   For multi-line viewport this function enables dynamic update of
   a single line. Especially usefull when updating a parameter having a
   variable pixel length, ex a paramter written using a proportional font.

**************************************************************************/
#include <gdisp.h>
void gputsclr(const char *str)
   {
   SGUINT len,x,w;
   GMODE mode;
   GYT y;
   gsetupdate(GUPDATE_OFF);
   /* Take control over alignment */
   mode = ggetmode();
   gsetmode((mode & ~(GALIGN_LEFT|GALIGN_HCENTER|GALIGN_LEFT))|(GLINECUT|GNOSCROLL));
   /* Find string position on line */
   w=ggetvpw();
   if((len = gpstrlen(str)) > w)
      /* Handle special case of line overflow */
      x = 0;
   else
   /* Set position according to position alignment mode */
   switch (mode & (GALIGN_LEFT|GALIGN_HCENTER|GALIGN_LEFT))
      {
      case GALIGN_RIGHT:
         x = w-len;
         break;
      case GALIGN_HCENTER:
         x = (w-len)/2;
         break;
      default:
         x = 0;
         break;
      }

   /* Output text with optionally clear of front or back part of line. */
   if ((y = ggetypos()) < ggetfh()-1)
      y = ggetfh()-1;
   if (x > 0)
      gfillvp(0,y-(ggetfh()-1),(GXT)(x-1), y, 0);
   gsetpos(x,y);
   gputs(str);
   x = ggetxpos();
   y = ggetypos(); /* Read y again to compensate in the case an (illegal) \n was embedded */
   if (x < w-1)
      gfillvp((GXT)x,y-(ggetfh()-1),w-1, y, 0);

   /* Restore mode states */
   gsetmode(mode);
   gsetupdate(GUPDATE_ON);
   }
