#include <gdisp.h>

#ifndef ANIMATE_H
#define ANIMATE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
   {
   GXT x;              /* Absolute coordinates for upper left corner */
   GYT y;
   PGFONT slide_ptr;   /* Animation slides font */
   unsigned int index; /* Index for next picture */
   GBUFINT size;       /* Buffer size */
   void *background;
   } ANIMATE, *ANIMATEP;

void animate_start(ANIMATEP a, GXT x, GYT y, PGFONT slide_ptr);
void animate_center(ANIMATEP a, PGFONT slide_ptr);
unsigned int animate_tick(ANIMATEP a);
void animate_stop(ANIMATEP a);

#ifdef __cplusplus
}
#endif

#endif /* ANIMATE_H */



