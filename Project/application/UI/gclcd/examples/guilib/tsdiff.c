/************************** tsdiff.c **********************************
*
*  Return difftime a user timestamp value with the current timestamp
*
*  Will handle hardware timer wrap-around
*
*  This module is OS system and target CPU independent
*
*  Created by: RAMTEX Engineering ApS 1997 JK
*
**********************************************************************/
#include <timer.h>

unsigned long int tsdiff(unsigned long newtime, unsigned long oldtime)
   {
   if (newtime < oldtime)
      return((~(oldtime) + 1L) + newtime); /* overrun had occured */
   else
      return(newtime-oldtime);
   }


