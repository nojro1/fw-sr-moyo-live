GENERALLY REUSABLE GUI EXAMPLE MODULES
--------------------------------------

This directory contains examples of how to implement some typical GUI
interface functions on top of the general LCD driver library.

The purpose with these functions is to demonstrate how different
GUI "styles" and "looks" can be implemented on top of the general
LCD driver library.

The demo example published on the web is partly build on top of the
general reuseable GUI functions in this directory.
To see use of several of these GUI menu functions please see the
example project in ..\guilibex\*.*

======================================================================
The functions in the modules below is prototyped in WNPOPUP.H

----

WNFRAME.C
Draw frames of different predefined styles around an area in the current
viewport. Usually used for decoration of menus, text frame cursors, etc.
The possible frame styles are predefined in wnpopup.h

This module can be used independent of the other modules.

NOTE: Frames with 3D effect, "insert" and "plates", require use of
seven "gray-shades" inclusive black and white. Therefore assure that
the color names below which are defined in gdispcfg.h also maps to the
correct color with a given palette. If nessesary adjust either
the palette or the color index mapping.

   G_WHITE       /* White */
   G_LLIGHTGREY  /* Light light-grey */
   G_LIGHTGREY   /* Light grey */
   G_GREY        /* Grey */
   G_DARKGREY    /* Dark-grey */
   G_DDARKGREY   /* Dark dark-grey */
   G_BLACK       /* Black


----

VPALLOC.C
Demonstrates how to create a viewport "heap" for easy management
of viewport numbers. Is assumed to be used in a last-out (last allocated) /
first-in (first free'ed) order.

This module can be used independent of the other modules.

----

WNALLOC.C
Provides a focus point for heap allocation. A good place to detect
heap overflow and locate error messages related to the heap.

Most menu designs are hierarchical in nature.
This module is the place where a simple hierarchical "last-out -
first-in" heap can be simulated by use of a static buffer.

This module can be used independent of the other modules.

-----

GPWORDP.C calculates the graphical pixel positions for the beginning
and end of a word in a string. It can be used for getting the
coordinates when setting block cursors on a topmenu.

This module can be used independent of the other modules.

----

GPLINEP.C returns a pointer to the beginning of the 'linenum' line
segment in a string.
Useful when only the last lines of a text string should be displayed.

GPUTLIST.C Output a string containing a list of lines to the current
viewport or a named viewport. Based on GPLINEP.C

These module can be used independent of the other modules.

----


GPUTSCLR.C Output a string in a viewport, with auto clear of line
area in front or after the line where the text is located.
Especially usefull when updating the same parameter in non-buffered
mode (GBUFFER undefined).

This module can be used independent of the other modules.

----

BARHOR.C
BARVER.C
Show how horizontal or vertical bars can be implemented in a way so
the bar changes can be updated very fast.
Scale marks can be added automatically.

Based on the WNFRAME.C module used for decoration frames and VPALLOC.C
used for temporary viewport allocation.
Except for these the module can be used independent of the other modules,

----

GSWAPCOL.C
Exchange two colors, or replace one color with another
in the current viewport.

This module can be used independent of any other modules.

----

SCROLHOR.C
Functions to scroll a viewport area on the screen horizontally

This module can be used independent of the other modules.

=================================================================

GTOUCH.C
Check touch screen values against a viewport.
Convert touch screen values to a menu index.

The functions are prototyped in WNPOPUP.H

This module build on the GWORDP.C module but can otherwise
be used independent of the other modules


GTOUCHKP.C
Provides a generic interface to a touch screen controller.

This module can be used independent of the other modules.


In PC simulation mode (GHW_PCSIM defined) the touch interface
in the LCDSERVER is used for input (touch events simulated via
mouse right-click on simulated LCD screen)

For target mode the target part of the function must be implemented
for the specific touch controller chip and bus interface used
in the target hardware system.
Note: The x,y coordinate values read from the touch controller hardware
must always be normalized to the display screen size in number of x,y pixels.
Note: The touch controller must be able to (at least) detect a touch
down event.


======================================================================

GUI WINDOWS AND MENU FUNCTIONS.
------------------------------

WNPOPUP.C contains functions for creating typical framed menus like
for instance popup menus pull down menus, top menus, cursor settings,
relative window positioning etc.

The "look" are predefined by a menu style setting.

The functions are generic in the sense that the menus can automatically
be adjusted to the content i.e. the size of the text, the font size and
the frame type.

If compiled with GEXTMODE defined in GDISPCFG.H then popup windows
using dynamic save of the windoww background is enabled. A pointer
to the buffer is located in the extended viewport data defined in
GVPAPP.H

This module builds on top of WNFRAME.C, VPALLOC.C WNALLOC.C GPWORDP.C

----

WNPOPVAL.C

Implements a typical parameter list table in a window.
The data is shown in 3 colums:
     pretext  valuetext posttext

It is build on top of the WNPOPUP.C functions.

----

WNMENU.C

Demonstrates how to implement functions which simplify the
menu management on the application level.

Demonstate a simple way to implement personalized menus, to manage
the menu position handling, and to hide the menu style setting
from the rest of the application.

Here menus are automatically sized to match the menu content.
The menu content is defined by a single C string.
For sub menus each menu item are seperated by the \n character.

It is build on top of the WNPOPUP.C functions.
Functions are prototyped in WNMENU.H

------

WNSCROLBARV.C

Demonstrates how a vertcal scroll bar can be impleneted.
The scroll bar consist of a top arrow key, cursor area, bottom arrow key.

The width of the scroll bar is defined by the arrow key symbols located
in scrollbarv.sym

------

MNUELEM_STRP.C

Contain a menu element drawing function for use with MNU_LIST's
This implementation uses an array of pointers to strings.

Includes also functions for automatic menu sizing during runtime, i.e.
- calculate the total number of menu elements
- find the pixel size of the largest menu element.

------

MNU_LIST.C
MNU_LIST_EVENT.C

Demonstrates how a implement a menu list with can handle an abitrary
number of menu elements without imposing excess RAM/ROM memory consumption.
If needed the menu is drawn with a scroll bar.

Demonstate a simple way to implement personalized menus, to manage
the menu position handling, and to hide the menu style setting
from the rest of the application.

Demonstate how to make "generic" menu handling based on menu element
drawing functions. This mean that all menu elements has the same pixel
size but can have any type of content (text, symbols, graphics etc).

Make use of the extended view port data structure as defined in gvpapp.h

MNU_LIST_EVENT.C
Demonstrates how touch screen events and key board events can be used
for navigaing a menu list created with the functions in MNU_LIST.C

The same basic touch events are assigned differnet behavoir depening
on wether it is on the list area, on cursor or on the scroll bar elements
of the menu list.


------

WNKEY.C
WNKEY_EVENT.C

Demonstrate how to create and handle a simple softkey defined by a viewport.
The key text content is defined via an array of text strings.

If the text array contains more than one text string then a toggle selction
key is achieved. Each key press will show the next key text (with wrap).
The key text state can be read back.

The key state information is saved in an extende viewport as defined
in gvpapp.h

WNKEY_EVENT.C
Demonstrates how touch screen events and key board events can be used
for handling the toggle soft key. The soft key is assumed to be created
with the functions in wnkey.c

------

The fonts NARROWM.* and NARROW20.* are the standard fonts with
extra special symbols added for the GUI examples.

======================================================================

The functions in the modules below are prototyped in TIMER.H
TIMESTAMP.C
TSDIFF.C
TIMEOUT.C

Functions to manage multiple independent timing events in a
(polled) system.

timestamp()   Return a 10ms timestamp value
                                                        In PC simulation mode the PC clock is used.

tsdiff(..)    Calculate the difference between two timestamps with
              secure handling of timestamp wrap-around.

timeout_set(..) Init a TIMEOUT event structure
timeout(..)     Check if timeout has occured.

These modules can be used independent of the other modules.

======================================================================

SA_DELAY.C Simple delay function which implement a system wait.
In PC simulation mode the keyboard is polled for an
exit via an 'Esc' or a 'Ctrl|F4' program exit event.

Primarily intended for use with simple test functions.
If timing events are used in an application it is recommended
that the functions in TIMER.H (listed above) are used instead.

If Simulator keyboard events are used in the PC appliation
then the poll for a program exit key event in the sa_delay.c
module should be comment out.


======================================================================

All examples in this directory are just provided "as is".
They are not supported and may be modified or removed
without notice.


