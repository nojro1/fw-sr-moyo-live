/*
   Demonstrate how to add handling of touch screen events and keyboard navigation
   events to a menu list window.

   The implementation below supports most common event actions on both list
   cursors, list view area, scrollbar up/down keys and scroll bar cursor.

   If an event is processed then the event is consumed.
   If an event is not processed it is returned for further processing.

   If using cursor list mode, then selection of a menu element will
   cause the *selection value to be updated with the index of the selected
   menu element and EVENT_SELECTION is returned.

   2010 RAMTEX Engineering ApS
*/
#include <wnmenu.h>
#include <gevents.h>

/* Check that library configuration is right */
#ifndef GEXTMODE
  #error This module require that GEXTMODE is enabled (defined) in gdispcfg.h
#endif

/*
   Process event inputs.
   Returns
*/
EVENT gm_list_event(SGUCHAR vp, EVENT event, MENUINDEX *selection)
   {
   static SGINT old_touch_y = -1;
   PGVPAPP app;
   PMENUDYN md;
   app = ggetapp_vp(vp);
   if ((md=app->mnu) != NULL)
      { /* Viewport contains a menu */
      switch (event)
         {
         case EVENT_TOUCH_DOWN:
         case EVENT_TOUCH_UP:
         case EVENT_TOUCH_DRAG:
         case EVENT_TOUCH_FOCUSLOST:
            { /* Is a touch event */
            PSCROLLBARV sb;
            SGUCHAR key;
            sb = &md->ctrl.scrollarea;
            if (!ginsidevp(vp,touch_x,touch_y))
               { // Touch was outside viewport area
               if (sb->ks != 0)
                  wnscrollbv_key_state(sb,0);
               old_touch_y = -1;
               return event;
               }
            switch ((key = wnscrollbv_is_touch(sb,touch_x,touch_y)))
               {
               case 1:   /* Key 1 area */
               case 2:   /* Key 2 area */
                  switch (event)
                     {
                     case EVENT_TOUCH_DRAG:
                        if (old_touch_y >0)
                           { /* Dragged from cursor area into key area. Do one more step */
                           gm_list_scroll(vp,(key==1) ? -1 : 1);
                           old_touch_y = -1;
                           }
                        break;
                     case EVENT_TOUCH_DOWN:
                        if (sb->ks != key)
                           wnscrollbv_key_state(sb,key);
                        break;
                     case EVENT_TOUCH_UP:
                        sb->ks = 0;
                        gm_list_scroll(vp,(key==1) ? -1 : 1);
                        break;
                     default:
                        if (sb->ks != 0)
                           wnscrollbv_key_state(sb,0);
                        break;
                     }
                  break;
               case 3:   /* Cursor area */
                  if (sb->ks != 0)
                     wnscrollbv_key_state(sb,0);
                  /* scroll bar drag event handling here (view is positioned relative to cursor) */
                  switch (event)
                     {
                     case EVENT_TOUCH_DOWN:
                         old_touch_y = touch_y;
                         /* Jump to approx relative position if outside cursor */
                         if ((touch_y < md->ctrl.scrollarea.cyt) ||
                             (touch_y > md->ctrl.scrollarea.cyt + md->ctrl.scrollarea.ch))
                            {
                            SGINT y = (md->ctrl.maxidx*(touch_y-(md->ctrl.scrollarea.k1yb+1)))/
                                (md->ctrl.scrollarea.k2yt - md->ctrl.scrollarea.k1yb + 2);
                            if (app->cursorvisible)
                               gm_list_set_cur_index(vp, y);
                            else
                               gm_list_set_top_index(vp, y);
                             }
                         break;
                     case EVENT_TOUCH_DRAG:
                         {
                         if (old_touch_y >= 0)
                           {
                           SGINT ydiff = ((SGINT) touch_y) - old_touch_y;
                           /* Scale relative to total menu index */
                           ydiff =
                              (ydiff * md->ctrl.maxidx)/
                                ((md->ctrl.scrollarea.k2yt-md->ctrl.scrollarea.k1yb)-2);
                           if (ydiff != 0)
                             {
                             gm_list_scroll(vp,ydiff);
                             old_touch_y = (SGINT) touch_y;
                             }
                           }
                         break;
                         }
                     default:
                        old_touch_y = -1;
                     }
                  break;
               default:  /* Menu Line area */
                  if (sb->ks != 0)
                     wnscrollbv_key_state(sb,0);
                  if (app->cursorvisible)
                     {
                     /* Cursor mode selection */
                     if (event == EVENT_TOUCH_UP)
                        { /* Click event on list. Is it inside inner margins ? */
                        GYT yt;
                        MENUINDEX select;
                        if ((gvpyb(vp) + md->style->yboffset) < touch_y)
                           break; /* Outside list margin */
                        if ((yt = gvpyt(vp) + md->style->ytoffset) > touch_y)
                           break; /* Outside list margin */

                        /* Inside margin. Convert to menu index */
                        select = (MENUINDEX)(touch_y - yt)/md->ctrl.lineheight + md->ctrl.topidx;
                        /* Update menu cursor if needed */
                        if (select != md->ctrl.cursoridx)
                           {
                           md->ctrl.cursoridx = select;
                           gm_list_update(vp);
                           }
                        if (selection != NULL)
                           *selection = select;
                        return EVENT_SELECTION;
                        }
                     }
                  else
                     {
                     /* View mode drag event handling here */
                     /* The element line follows touch position in whole line steps */
                     switch (event)
                        {
                        case EVENT_TOUCH_DOWN:
                           old_touch_y = touch_y;
                           break;
                        case EVENT_TOUCH_DRAG:
                           {
                           if (old_touch_y >= 0)
                              {
                              SGINT ydiff = old_touch_y - ((SGINT) touch_y);
                              /* Scale relative to line height */
                              ydiff /= md->ctrl.lineheight;
                              if (ydiff != 0)
                                 {
                                 gm_list_scroll(vp,ydiff);
                                 old_touch_y = (SGINT) touch_y;
                                 }
                              }
                           break;
                           }
                        default:
                           old_touch_y = -1;
                        }
                     }
                  break;
               }
            return EVENT_KEY_NOEVENT;
            }
         case EVENT_KEY_UP:
         case EVENT_KEY_DOWN:
         case EVENT_KEY_RETURN:
         case EVENT_KEY_ESC:
            {
            if (app->has_focus == 0)
               break;
            switch (event)
               {
               case EVENT_KEY_UP:
                  gm_list_scroll(vp,-1);
                  return EVENT_KEY_NOEVENT;
               case EVENT_KEY_DOWN:
                  gm_list_scroll(vp,1);
                  return EVENT_KEY_NOEVENT;
               case EVENT_KEY_RETURN:
                  {
                  if (app->cursorvisible)
                     {
                     if (selection != NULL)
                        *selection = md->ctrl.cursoridx;
                     return EVENT_SELECTION;
                     }
                  }
               case EVENT_KEY_ESC:
                 app->has_focus = 0;
               default:
                  break;
               }
            break;
            }
         default:
            break;
         }
      }
   return event;
   }

