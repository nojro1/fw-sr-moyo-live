/*
   Menu management example

   Demonstate a simple way to implement personalized menus and manage
   the menu position handling.

   Here menus are automatically sized to match the menu content.
   The menu content is defined by a single C string.
   For sub menus each menu item are seperated by the \n character.

   This menu management function examples build on top of the menu
   implementation in wnpopup.c, which again is build on top of the
   standard LCD library functions.

   The high-level interface implemented here is LCD driver independent.

*/

#include <wnmenu.h>
#include <wnpopup.h>

#ifndef WM_USE_POPUP_BUFFER
#define WM_USE_POPUP_BUFFER
#endif

/* Check that library configuration is right */
#ifndef GEXTMODE
  #error This module require that GEXTMODE is enabled (defined) in gdispcfg.h
#endif

#ifndef GFUNC_VP
  #error This module require that GFUNC_VP is enabled (defined) in gdispcfg.h
#endif

/* Limits which protect any topmenu or bottom status bars. */

static GSTYLE topmenu_style;
static GSTYLE submenu_style;

void gm_init(void)
   {
   /* Select 3D frames as the default menu style. Use save of background to buffer */
   wnset_style(&topmenu_style,GMFONT,0,GNORMAL|GLINECUT,FR_PLATE|FR_FILL,FR_PLATE_THIN,0,3,3,2,2);
   #ifdef WM_USE_POPUP_BUFFER
   wnset_style(&submenu_style,GMFONT,0,GNORMAL|GLINECUT,FR_PLATE|FR_FILL|FR_BUFFER,FR_PLATE_THIN,0,1,1,1,1);
   #else
   wnset_style(&submenu_style,GMFONT,0,GNORMAL|GLINECUT,FR_PLATE|FR_FILL,FR_PLATE_THIN,0,1,1,1,1);
   #endif
   upperlimit = 0;
   lowerlimit = GDISPH-1;
   }

/*
   Create a new topmenu.
   Change style to topmenu style
   Return the new viewport number
*/
SGUCHAR gm_topmenu_open(const char *toptxt)
   {
   SGUCHAR vp;
   PGVPAPP app;                   /* Pointer to application specific part of viewport */
   wnsel_style(&topmenu_style);   /* Set style */
   gselvp((vp = wn_get_vpnum())); /* Pick new viewport for menu */
   wnvpcreate_pos(vp,0,upperlimit,GDISPW,gfgetfh(GMFONT)); /* Create menu */
   gputs_vp(vp,(char *)toptxt);   /* Print top menu text */
   upperlimit = wnyb(vp)+1;       /* Reduce limit to under top menu */

   /* Save information for navigation */
   app = ggetapp_vp(vp);  /* Initialize pointer to background data stored in viewport */
   app->txtptr = toptxt;
   app->mnu = NULL;       /* Using plain menu (all elements visible)*/
   app->cursoridx = 0;
   app->cursorvisible = 0;
   return vp;
   }

/*
   Internal function. Check if vp is the topmenu.
*/
static SGBOOL is_topmenu(SGUCHAR vp)
   {
   return (wnyb(vp) < upperlimit) ? 1 : 0;
   }

/*
   Create a new simple submenu below topmenu
   Change style to submenu style
   Return the new viewport number
*/
SGUCHAR gm_submenu_open(GXT x, GYT y, const char *txt)
   {
   SGUCHAR vp;
   PGVPAPP app;    /* Pointer to application specific part of viewport */
   wnsel_style(&submenu_style);/* Set style */
   gselvp((vp = wn_get_vpnum())); /* Pick new viewport for menu */
   wnmenu(vp,x,y+upperlimit,txt); /* Place relative to upperlimit */

   /* Save information for navigation in custom viewport area */
   app = ggetapp_vp(vp);  /* Initialize pointer to background data stored in viewport */
   app->txtptr = txt;
   app->mnu = NULL;       /* Using plain menu (all elements visible)*/
   app->cursoridx = 0;
   app->cursorvisible = 0;
   return vp;
   }

/*
   Create a 3 column submenu,

   The 3 columns is assumbed to be : check_column, text column, arrow column
   An column may be a NULL pointer = an empty column

   Return the new viewport number

*/
SGUCHAR gm_submenu3_open(GXT x, GYT y, const char *check_txt, const char *txt, const char *arrow_txt)
   {
   SGUCHAR vp,vptmp;
   PGVPAPP app;    /* Pointer to application specific part of viewport */
   GXT w1,w2,w3;
   GYT htmp,h;

   wnsel_style(&submenu_style);   /* Set style */
   gselvp((vp = wn_get_vpnum())); /* Pick new viewport for menu */
   gselfont(wnget_style()->font); /* Initialize style font in advance to calculate size */

   /* Size menu info */
   if (check_txt != NULL)
      {
      h = (GYT) gpstrheight(check_txt);
      w1 = (GXT) gpstrwidth(check_txt);
      }
   else
      {
      h = 0;
      w1 = 0;
      }
   if (txt != NULL)
      {
      if ((htmp = (GYT) gpstrheight(txt)) > h)
         h=htmp;
      w2 = (GXT) gpstrwidth(txt);
      }
   else
      w2 = 0;
   if (arrow_txt != NULL)
      {
      if ((htmp = (GYT) gpstrheight(arrow_txt)) > h)
         h = htmp;
      w3 = (GXT) gpstrwidth(arrow_txt);
      }
   else
      w3 = 0;

   /* Create menu to fit around all 3 texts */
   wnvpcreate_pos(vp,x,y+upperlimit,w1+w2+w3,h);

   /* Save information for navigation in custom viewport area */
   app = ggetapp_vp(vp);  /* Initialize pointer to background data stored in viewport */
   app->txtptr = txt;
   app->cursoridx = 0;
   app->cursorvisible = 0;

   /* Output text in 3 columns */
   gselvp((vptmp = wn_get_vpnum())); /* Pick new viewport for column alignment */
   gsetupcpy(vptmp,vp);
   if (w1 != 0)
      {
      gsetvp_vp(vptmp,gvpxl(vp),gvpyt(vp),gvpxl(vp)+w1-1,gvpyb(vp));
      gputs_vp(vptmp,check_txt);
      }
   if (w2 != 0)
      {
      gsetvp_vp(vptmp,gvpxl(vp)+w1,gvpyt(vp),gvpxl(vp)+w1+w2-1,gvpyb(vp));
      gputs_vp(vptmp,txt);
      }
   if (w3 != 0)
      {
      gsetvp_vp(vptmp,gvpxl(vp)+w1+w2,gvpyt(vp),gvpxl(vp)+w1+w2+w3-1,gvpyb(vp));
      gputs_vp(vptmp,arrow_txt);
      }
   /* pop viewport */
   wn_free_vpnum(vptmp);
   gselvp(vp);
   return vp;
   }

/*
  Close the menu defined by the viewport number
*/
void gm_menu_close( SGUCHAR vp )
   {
   if (is_topmenu(vp))
      upperlimit = 0; /* Reset upperlimit*/
   wnvpclose(vp);     /* close menu (and restore background) */
   wn_free_vpnum(vp); /* free viewport nummer */
   }

/*
   Make the menu (style) current
*/
void gm_menu_activate( SGUCHAR vp )
   {
   if (is_topmenu(vp))
      wnsel_style(&topmenu_style);
   else
      wnsel_style(&submenu_style);
   }



