#include <wnpopup.h>
/*
   Return a pointer to the beginning of the 'linenum' line segment in a string.
   Useful when only the last lines of a text string should be displayed.

   linenum is counting from 0 = the current line pointed to by str.
   If linenum > number of lines in string the a pointer to the terminating 0
   is returned.
*/
const char *gplinep( const char *str, SGUINT linenum )
   {
   if (str == NULL)
      return NULL;
   while (linenum > 0)
      {
      while ((*str != '\n') && (*str != 0))
         str++;
      if (*str == 0)
         break;  /* terminating zero reached */
      /* \n reached, Skip \n and increment linecount*/
      linenum--;
      str++;
      }
   return str;
   }

