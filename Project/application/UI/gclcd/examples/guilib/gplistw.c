#include <wnpopup.h>
/*
   Find the maximum string pixel width for a list of stings, using the specified font.

   PGSTR *strpp  The string pointer list must be terminated with a NULL pointer
   PGFONT pfont  Font used for pixel size calculation.
                 If the pfont = NULL then the current viewport font is used
*/
SGUINT gplistw(PGSTR *strpp, PGFONT pfont)
   {
   SGUINT maxwidth = 0;
   if (strpp != NULL)
      {
      PGFONT oldfont;
      if (pfont != NULL)
         oldfont = gselfont(pfont);

      /* Check max pixel line width for all strings in array */
      while (*strpp != NULL)
         {
         SGUINT width = gpstrwidth(*strpp); /* Width of largest line segment */
         if (width > maxwidth)
            maxwidth = width;
         strpp++;
         }

      if (pfont != NULL)
         gselfont(oldfont);
      }
   return maxwidth;
   }

