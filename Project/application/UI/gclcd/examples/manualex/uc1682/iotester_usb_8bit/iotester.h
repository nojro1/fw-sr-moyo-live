#ifndef IOTESTER_H
#define IOTESTER_H
/**********************************************************************/
/*                                                                    */
/*   iotester.c      for IOTester USB                                 */
/*                                                                    */
/*   Define functions functions for IOTester access.                  */
/*                                                                    */
/*   Version number: 1                                                */
/*   Copyright (c) RAMTEX Engineering Aps 2010                        */
/*                                                                    */
/**********************************************************************/

#ifdef __cplusplus
    extern "C" {
#endif

/***
  Define IOTester specific bits for construction of ioreg_designators used by
  I/O access functions (iot_wr(..) iot_rd(.) etc,  or  iowr(..), iord(.) etc)
***/

/* Register sizes and endian */
#define  IOT_REG8       0x00000000  /* 8 bit register   (no endian) */
#define  IOT_REG16L     0x10000000  /* 16 bit register, MSB at low address */
#define  IOT_REG24L     0x20000000  /* 24 bit register, MSB or MSW  at low address */
#define  IOT_REG32L     0x30000000  /* 32 bit register, MSB or MSW  at low address */
#define  IOT_REG16H     0x90000000  /* 16 bit register, MSB at high address */
#define  IOT_REG24H     0xa0000000  /* 24 bit register, MSB or MSW  at high address */
#define  IOT_REG32H     0xb0000000  /* 32 bit register, MSB or MSW  at high address */
#define  IOT_REG16      IOT_REG16L  /* 16 bit register  (= no endian, 16 bit bus or internal registers) */
#define  IOT_REG24      IOT_REG24L  /* 24 bit register, (= no endian, internal registers) */
#define  IOT_REG32      IOT_REG32L  /* 32 bit register, (= no endian, internal registers) */

/* Logic device designators, for use when one PC application uses multiple IOTester devices (2-4) */
#define  IOT_DEVICE0    0x00000000  /* = default */
#define  IOT_DEVICE1    0x04000000
#define  IOT_DEVICE2    0x08000000
#define  IOT_DEVICE3    0x0c000000

/* ioreg_designators type masks (for internal macro implementations) */
#define  IOT_REGMSK     0x30000000  /* Register size (only 1-4 bytes for iotester 32 bit version) */
#define  IOT_ENDIAN     0x80000000  /* 0 = MSB data at high bus address, 1 = MSB data at low bus address  */
#define  IOT_DEVICE_MSK 0x0c000000  /* Device bits */
#define  IOT_ADRMSK     0x03ffffff  /* Address mask for virtual address space inside an iotester */

typedef unsigned long IOREG_DESC;   /* ioreg_designator type used by IOTester */

/***
   Define macros for IOTester ioreg_designator operations
***/
#define  IOT_SIZEOF_REG(ioreg_designator) ((unsigned char)((((unsigned long)(ioreg_designator) >> 28) & 3)+1))
#define  IOT_DEVICE(ioreg_designator)     ((unsigned char) (((unsigned long)(ioreg_designator) >> 26) & 3))
#define  IOT_ADRLIN(baseadr, offset)      (((baseadr) & (~IOT_ADRMSK)) + (((baseadr) + (offset)*IOT_SIZEOF_REG(baseadr)) & IOT_ADRMSK))
#define  IOT_ADRBLOCK(baseadr, x, y, w)   (((baseadr) & (~IOT_ADRMSK)) + (((baseadr) + ((x)+(y)*(w))*IOT_SIZEOF_REG(baseadr)) & IOT_ADRMSK))
#define  IOT_CAST_REG(ioreg_type,  ioreg_designator) ((ioreg_type & (IOT_REGMSK|IOT_ENDIAN)) | (ioreg_designator & ~(IOT_REGMSK|IOT_ENDIAN)))
#define  IOT_CAST_DEV(ioreg_device,ioreg_designator) ((ioreg_device & IOT_DEVICE_MSK) | (ioreg_designator & ~IOT_DEVICE_MSK))

#ifndef IOT_NO_DLL_INTF
/***
   IOTester DLL interface
***/
/* typedef's for dynamic loaded library */
typedef unsigned long (*IOT_LONG_LONG)(IOREG_DESC);
typedef void (*IOT_LONGLONG)(IOREG_DESC, unsigned long);
typedef unsigned long (*IOT_LONG_LONGLONG)(IOREG_DESC, unsigned long);
typedef void (*IOT_LONGLONGLONG)(IOREG_DESC, unsigned long, unsigned long);
typedef void (*IOTBUF)(IOREG_DESC, unsigned long, unsigned int, void *, unsigned long, unsigned int );

typedef void (* IOT_FUNC_PTR)(void);
typedef IOT_FUNC_PTR (*IOT_CHAR_FUNCPTR)(unsigned char, IOT_FUNC_PTR );
typedef unsigned long (*IOT_LONG_CHAR)(unsigned char);
typedef unsigned char (*IOT_CHAR_CHAR)(unsigned char);
typedef int (*IOT_INT_CHAR)(unsigned char);
typedef void (__stdcall * IOT_FUNC_CHAR_PTR)(const char *);
typedef int (*IOT_MESSPTR)( IOT_FUNC_CHAR_PTR );

/* Public function pointers for iot dynamic loaded library */
extern IOT_LONG_LONG     iot_rd;
extern IOT_LONGLONG      iot_wr;
extern IOT_LONGLONG      iot_and;
extern IOT_LONGLONG      iot_or;
extern IOT_LONGLONG      iot_xor;
extern IOT_LONG_LONGLONG iot_rdbuf;
extern IOT_LONGLONGLONG  iot_wrbuf;
extern IOT_LONGLONGLONG  iot_andbuf;
extern IOT_LONGLONGLONG  iot_orbuf;
extern IOT_LONGLONGLONG  iot_xorbuf;
extern IOTBUF            iotbuf_wr;
extern IOTBUF            iotbuf_rd;

extern IOT_CHAR_FUNCPTR  iot_set_callback;
extern IOT_CHAR_CHAR     iot_sync;
extern IOT_MESSPTR       iot_message_init;


/* initialization: */
void __stdcall iot_print_message(const char *str);
unsigned long iot_device_get_serialnumber( unsigned char dev_num );
unsigned char iot_device_get_serialnumber_list(unsigned long *serialnumberslist, unsigned char maxlistsize);
int iot_init_device(unsigned long serial_number, unsigned char device_num);
int iot_init_interrupt(unsigned long phys_device, IOT_FUNC_PTR intrfunc, unsigned short irq_num);
#define iot_init(phys_device) iot_init_interrupt((phys_device), NULL, 0)
/* termination: */
void iot_exit(void);
int iot_close_device(unsigned char device_num);

/* Functions for IOTESTER-LPT interface compatibility */
void iot_set_buswait(unsigned char bus_wr_waitstates, unsigned char bus_rd_waitstates);
unsigned char iot_intr_poll(void);
unsigned char iot_intr_enable(unsigned char enable);
int iot_is_connected(unsigned long serial_number);

#endif /* ! IOT_NO_DLL_INTF */

/*
   Error and status flags for iot_init_device(), iot_init(), iot_is_connected()
*/
#define IOT_ILLEGAL_PARAMETER   -8 /* Illegal parameter used in call to IOTester function (correct your code) */
#define IOT_DLL_ACCESS_FAULT    -7 /* IOTester DLL libray is not found (check installation) */
#define IOT_FUNC_ACCESS_FAULT   -6 /* IOTester DLL function connection failed (check installation version) */
#define IOT_OPR_FAILED          -5 /* IOTester operation failed (generic failure, already rapported) */
#define IOT_DRIVER_ACCESS_FAULT -4 /* Could not establish IOTESTER connection */
#define IOT_NOT_CONNECTED       -3 /* IOTester is not connected to PC (or missing drivers) */
#define IOT_ALREADY_USED        -2 /* IOTester device is already in use (physical device has already got a device number) */
#define IOT_NOT_USED            -1 /* IOTester is connected to PC but not in use by any device */
#define IOT_OK                   0 /* >= 0 -> connection succeded */

/* iot_is_connected() connection test uses returns parameters below
  (If not either IOT_NOT_CONNECTED, IOT_NOT_USED, (or IOT_OPR_FAILED if PC error))
*/
#define IOT_DEVNUM_0             0 /* IOTester is connected and in use as device 0 */
#define IOT_DEVNUM_1             1 /* IOTester is connected and in use as device 1 */
#define IOT_DEVNUM_2             2 /* IOTester is connected and in use as device 2 */
#define IOT_DEVNUM_3             3 /* IOTester is connected and in use as device 3 */

/***
   iot_sync(.) parameter definitions
***/
#define IOT_SYNC         0   /* Synchronize (Keep mode setting) */
#define IOT_SYNC_SLOW    1   /* Synchronize + Set sync at every I/O operation (slow but application controlled timing) */
#define IOT_SYNC_NORMAL  2   /* Synchronize + Set normal mode (most debug friendly) (IOTester default) */
#define IOT_SYNC_DELAYED 3   /* Synchronize + Set Delayed sync mode (for application controlled USB bursts) */
#define IOT_SYNC_FAST    4   /* Synchronize + Set Highest transfer mode ( 10 time faster than IOT_SYNC_NORMAL ) */

/***
   IOTESTER-LPT source code compatibility definitions (dummy definitions)
***/
#define IOT_LPTSEEK     0
#define IOT_LPT1        0
#define IOT_LPT2        0
#define IOT_LPT3        0
#define IOT_IRQ_DEFAULT 0
#define IOT_LPT_IRQ5    0
#define IOT_LPT_IRQ7    0

/* Include internal IOTESTER register definitions */
#include <iotreg.h>

/* Include portable IOHW functions definitions (for PC mode use of IOTester) */
#if !defined( IOTESTER_USB )
   /* Force inclusion of iohw definitions for PC mode */
   #define IOTESTER_USB
#endif

#include <iohw.h>

#ifdef __cplusplus
   }
#endif
#endif
