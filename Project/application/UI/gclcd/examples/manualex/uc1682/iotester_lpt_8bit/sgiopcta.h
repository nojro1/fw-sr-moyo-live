/***********************************************************************
 *
 * STIMGATE I/O port definition for the target system.
 *
 *   Target processor CPU family : IOTESTER
 *   Target processor device     : IOTESTER
 *   Target compiler             : PCMODE
 *
 * This file is included by SGIO.H to preprocess the sgxxx() port functions
 * when the C source file is compiled for execution in the PC.
 *
 * The file can be generated with the SGSETUP program.
 * SGSETUP use this file for back-annotation and to generate a corresponding
 * file: SGIO_TA.H for the target C-compiler.
 * NOTE : Do not modify this file directly, unless you are absolutely sure
 * of what you are doing.
 *
 * V3.03  STIMGATE Copyright (c) RAMTEX International 2006
 *
 **********************************************************************/

 #define GHWWR  (IOT_REG8 + 0x0001) /*Graphic LCD data write*/
 #define GHWRD  (IOT_REG8 + 0x0001) /*Graphic LCD data read*/
 #define GHWSTA (IOT_REG8 + 0x0000) /*Graphic LCD status*/
 #define GHWCMD (IOT_REG8 + 0x0000) /*Graphic LCD status*/

 #define GHWWRW  (IOT_REG16 + 0x0001) /*Graphic LCD data write*/
 #define GHWRDW  (IOT_REG16 + 0x0001) /*Graphic LCD data read*/
 #define GHWSTAW (IOT_REG16 + 0x0000) /*Graphic LCD status*/
 #define GHWCMDW (IOT_REG16 + 0x0000) /*Graphic LCD status*/

/* Compilation test only, not supported by IOTESTER version 4 hdw */
 #define GHWWRDW  (IOT_REG32H + 0x0001) /*Graphic LCD data write*/
 #define GHWRDDW  (IOT_REG32H + 0x0001) /*Graphic LCD data read*/
 #define GHWSTADW (IOT_REG32H + 0x0000) /*Graphic LCD status*/
 #define GHWCMDDW (IOT_REG32H + 0x0000) /*Graphic LCD status*/


