This directory project contains files specific for using the LCD 
driver library together with PC tools (ex Microsoft VC6.x), where the 
LCD display hardware is connected to the PC via the RAMTEX IOTESTER 
tool.
(The RAMTEX IOTESTER tool enable drivers for I/O hardware for 
embedded systems to be developed and tested with ordinary PC 
compiler tools. See more at www.ramtex.dk/iotester/iotester.htm)

----
A similar compiler project should be made for the given target 
processor compiler. 

With a normal embedded compiler project only the following files 
will be used in normal bus connection mode (plus the library files):
   sgio.h
   sgio_ta.h
   sgtypes.h

The LCD controller address definitions in sgio_ta.h should be 
updated to match the given target processor hardware.