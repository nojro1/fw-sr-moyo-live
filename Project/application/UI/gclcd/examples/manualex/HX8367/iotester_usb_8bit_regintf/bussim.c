/***************************** bussim.c ************************************

   Generic template file for external bus simulator drivers.

   The functions bodies below must be implemented as described in the comments.
   Use the proper I/O port instructions given by the actual processor
   architecture, the actual LCD connection method and the I/O access
   capabilities of the target C compiler.

   Alternatively this module can be used as a universal stub during initial
   test compilation of the target software.

   The functions in this module is called by single-chip-processor version
   of the ghwinit.c module.

   Copyright (c) RAMTEX Engineering Aps 2006-2012

****************************************************************************/
#ifndef GHW_NOHDW
#ifdef  GHW_SINGLE_CHIP

#include <bussim.h>
#include < sgio.h >

#ifdef GHW_BUS8
/*
   Simulate a bus write operation for a LCD controller with an Intel
   like bus interface (i.e. use of separate /RD and /WR strobes).
   This access function is 8 bit processor data bus interfaces.

   The address parameter adr is assumed to be either 0 or 1.

   The 8bit bus uses the 8 msb bit of the 16 bit physical bus
*/
void simwrby(SGUCHAR adr, SGUCHAR dat)
   {
   sgwrwo(((adr != 0) ? GHWWRWS : GHWCMDWS),(((SGUINT) dat) << 8));   /* 8 bit register uses high part of bus */
   }

/*
   Simulate a bus read operation for a LCD controller with an Intel
   like bus interface (i.e. use of separate /RD and /WR strobes).
   This access function is 8 bit processor data bus interfaces.

   The address parameter adr is assumed to be either 0 or 1.

   The 8bit bus uses the 8 msb bit of the 16 bit physical bus
*/
SGUCHAR simrdby(SGUCHAR adr)
   {
   return (SGUCHAR)(sgrdwo(((adr != 0) ? GHWRDWS : GHWSTAWS)) >> 8);   /* 8 bit register uses high part of bus */
   }

#else

#error This interface is for GHW_BUS8 configuration only

#endif

/*
  Initialize and reset LCD display.
  Is called before simwrby() and simrdby() is invoked for the first time

  The controller reset line is toggled here if it connected to a bus port.
  (it may alternatively be hard-wired to the reset signal to the processors
  in the target system).

  The sim_reset() function is invoked automatically via the ginit() function.
*/
void sim_reset( void )
   {
   /* 1. Init data port setup (if required by port architecture) */
   /* 2. Make C/D, /RD, /WR, /CE to outputs (if required by port architecture) */
   /* 3. Set LCD reset line /RST active low   (if /RST is connected to a port bit) */
   /* 4. Set LCD reset line /RST passive high (if /RST is connected to a port bit) */
   }

#endif /* GHW_SINGLE_CHIP */
#endif /* GHW_NOHDW */



