/***********************************************************************
 *
 * STIMGATE I/O port definition for the target system.
 *
 *   Target processor CPU family : IOTESTER
 *   Target processor device     : IOTESTER
 *   Target compiler             : PCMODE
 *
 * This file is included by SGIO.H to preprocess the sgxxx() port functions
 * when the C source file is compiled for execution in the PC.
 *
 * The file can be generated with the SGSETUP program.
 * SGSETUP use this file for back-annotation and to generate a corresponding
 * file: SGIO_TA.H for the target C-compiler.
 * NOTE : Do not modify this file directly, unless you are absolutely sure
 * of what you are doing.
 *
 * V3.03  STIMGATE Copyright (c) RAMTEX International 2007
 *
 **********************************************************************/

 #define GHWWRS  (IOT_REG8 + 0x0001) /*Graphic LCD data write*/
 #define GHWRDS  (IOT_REG8 + 0x0001) /*Graphic LCD data read*/
 #define GHWSTAS (IOT_REG8 + 0x0000) /*Graphic LCD status*/
 #define GHWCMDS (IOT_REG8 + 0x0000) /*Graphic LCD status*/

 #define GHWWRWS  (IOT_REG16 + 0x0001) /*Graphic LCD data write*/
 #define GHWRDWS  (IOT_REG16 + 0x0001) /*Graphic LCD data read*/
 #define GHWSTAWS (IOT_REG16 + 0x0000) /*Graphic LCD status*/
 #define GHWCMDWS (IOT_REG16 + 0x0000) /*Graphic LCD status*/

/* Used for 32 bit bus mode (18 bit, 24 bit pr pixel) 
   Compilation test only, not supported by IOTESTER version 1,2 hdw */
 #define GHWWRDWS  (IOT_REG32H + 0x0001) /*Graphic LCD data write*/
 #define GHWRDDWS  (IOT_REG32H + 0x0001) /*Graphic LCD data read*/
 #define GHWSTADWS (IOT_REG32H + 0x0000) /*Graphic LCD status*/
 #define GHWCMDDWS (IOT_REG32H + 0x0000) /*Graphic LCD status*/

