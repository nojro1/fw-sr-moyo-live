#ifndef BUSSIM_H
#define BUSSIM_H
/***************************** bussim.c ************************************

   Definitions for the LCD bus simulator access functions.

   The definitions below can be modified to reflect a given access function
   implementation.

   RAMTEX Engineering Aps 2007-2009

****************************************************************************/

/* Simulated port adresses */
/*************** Do not modify the definitions below ********************/
#ifdef __cplusplus
extern "C" {
#endif
#include <gdisphw.h>   /* GHW_BUS8, GHW_BUS16 switch and SG types */

/* Single chip mode -> access via user supplied access driver functions */

#ifdef GHW_BUS8
/* 8 bit bus mode */

#define GHWWR  0x1
#define GHWRD  0x1
#define GHWSTA 0x0
#define GHWCMD 0x0

void simwrby( SGUCHAR address, SGUCHAR dat);
SGUCHAR simrdby( SGUCHAR address );

#elif defined( GHW_BUS16 )
/* 16 bit bus mode */

#define GHWWRW  0x1
#define GHWRDW  0x1
#define GHWSTAW 0x0
#define GHWCMDW 0x0

void simwrwo( SGUCHAR address, SGUINT dat);
SGUINT simrdwo( SGUCHAR address );

#else
/* 32 bit bus mode
  (display uses the 18 or 24 lsb dat bits depending on controller type */

#define GHWWRDW  0x1
#define GHWRDDW  0x1
#define GHWSTADW 0x0
#define GHWCMDDW 0x0

void simwrdw( SGUCHAR address, SGULONG dat);
SGULONG simrddw( SGUCHAR address );
#endif

void sim_reset( void );

#ifdef __cplusplus
}
#endif

#endif /* BUSSIM_H */
