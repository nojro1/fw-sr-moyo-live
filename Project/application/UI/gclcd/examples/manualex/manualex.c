/*********************************************************************

   This file contains source code for all the examples in the color
   graphic LCD driver manual, modified to be calls to demo functions
   instead of a main function.

   The examples are ordered in alphabetic order.

   Some of the examples requires single stepping with a debugger
   to be able to follow the changes on the LCD display.

   A few examples are designed with a specific color mode in mind, ex RGB,
   and is therefore configuration switch dependent.

   For increased simulation speed full data checking can be turned off
   in gdispcfg.h

   RAMTEX Engineering 2003-2012

*********************************************************************/
#include <stdlib.h>  /* malloc function */
#include <stdio.h>   /* sprintf, swprintf function */
#include <math.h>    /* sin function */

#include <gdisp.h>   /* Common to all LCD functions */

/*
   void gcarc(GXT xc, GYT yc, GXYT r, SGUCHAR arctype)
*/
void gcarc_ex(void)
   {
   GXT x,w;
   GYT y,h;

   ginit();
   /* Draw a scaleable thermometer figure */

   x=40;  /* Define positions and size */
   y = GDISPH/4;
   h = GDISPH/2;
   w = h/10;

   /* Draw figure & decorations */
   gsetcolorf(G_BLACK);
   #if (GDISPPIXW > 2)
   gsetcolorb(G_LIGHTGREY);
   #else
   gsetcolorb(G_WHITE);
   #endif
   gfillvp(x-w,y,x+w,y+h,0);               /* Draw body */
   grectangle(x-w,y,x+w,y+h);
   gcarc(x,y,w,GCARC_LT|GCARC_RT|GFRAME);  /* Draw Top, half circle */
   #if (GDISPPIXW > 2)
   gsetcolorb(G_BLUE);
   #else
   gsetcolorb(G_BLACK);
   #endif
   gcarc(x,y+h,((w*3)/2),GCARC_LT|GCARC_RT|GCARC_LB|GCARC_RB|GFRAME); /* Draw Bottom, Circle */
   gsetvp(x-w+1,y+1,x+w-1,y+h-1);   /* Move viewport to inside bar figure */

   /* Draw some bar levels */
   for (h=2*w; h<ggetvph()-5;h++)
      {
      gfillvp(0,ggetvph()-h,ggetvpw()-1,ggetvph()-1,0x0000);
      }
   }

/*
   void gcircle(SGINT xc, SGINT yc, SGINT r, SGUCHAR fill)
*/
void gcircle_ex(void)
   {
   int i;
   ginit();

   /* gcircle(..) example */
   /* Draw 3 concentric circles */
   for(i=1; i < 4; i++)
      gcircle( 20,20, 5*i, 0);

   /* Draw filled circle */
   gcircle( 45,20, 5, 1);
   }

/*
   void gclrvp(void);
*/
void gclrvp_ex(void)
   {
   ginit();
   gsetcpos(2,3);   /* Write text at pos 2, 3 */
   gputs("Line 1"); /* in default view port */
   gclrvp();        /* Clear viewport */
   gputs("Line 2"); /* Text in uppe left corner */
   }

/* Output after funtion
Line 2
*/

/*
   void gclrtabs(void);
*/
void gclrtabs_ex(void)
   {
   ginit();

   gclrtabs();          /* Clear previous tabs */
   gsettabs(3*GDISPCW); /* Set tabulator spacing
                           = 3 character widths */
   gputs("1\tTab\n2\tTest");
   }

/* Output
1  Tab
2  Test
*/

/*
   void gcursorblink(void);
   This example asumes that GNOCURSOR is undefined
*/
void gcursorblink_ex(void)
   {
   int i,j;
   ginit();

   /* Select a soft font and enable cursor */
   gselfont(&times9);
   gsetcursor( GCURSIZE2 | GCURON | GCURBLINK );

   gputs("Cursor blink");
   for( j=0; j<10; j++ )
      {
      gcursorblink();            /* Toggle SW cursor */
      for( i=0; i<10000; i++ )   /* Make some delay  */
         {
         }
      }
   }

/*
  PGCODEPAGE gfgetcp( PGFONT *fp)
*/
void gfgetcp_ex(void)
   {
   ginit();
   gsetmode(GWORD_WRAP);
   if (ggetcp() != gfgetcp(&times9))
     gputs("Default codepage is NOT times9\n");
   gselfont(&times9);
   if (ggetcp() == gfgetcp(&times9))
     gputs("The times9 codepage is used");
   }

/*
   GYT gfgetfh( PGFONT fp );
*/
void gfgetfh_ex(void)
   {
   ginit();
   /* Must display at least 5 lines
      - select an appropriate font */
   if ( GDISPH/gfgetfh(&times13) >= 5 )
      gselfont(&times13);
   else
      gselfont(&times9);
   gsetvp(0,0, GDISPW-1, ggetfh()*5);
   gputs("Line1\nLine2\nLine3\nLine4\nLine5");
   }

/*
   GYT gfgetfw( PGFONT fp );
*/
void gfgetfw_ex(void)
   {
   ginit();

   /* Make viewport in frame */
   grectangle(0,0, GDISPW-1, GDISPH-1);
   gsetvp(2,2, GDISPW-3, GDISPH-3);

   /* Select font so viewport can contain at least 8 characters */
   if ( ggetvpw()/gfgetfw(&rtmono8_8) >= 8 )
      gselfont(&rtmono8_8);
   else
      gselfont(&msfont);
   gputs("12345678");
   }

/*
  void gfillfsym( GXT xs, GYT ys, GXT xe, GYT ye, SGUINT index, PGFONT pfont );
*/
void gfillfsym_ex(void)
   {
   ginit();
   grectangle(2,2,GDISPW/2,GDISPH-3); /* Draw rectangle */
   gfillfsym( 3, 3, GDISPW/2-1, GDISPH-4, 'A', &ariel18); /* Fill inside area from font symbol graphic */
   }

/*
   void gfillsym( GXT xs, GYT ys, GXT xe, GYT ye, PGSYMBOL psym );
*/
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR b[8];       /* Symbol data, size = (cxpix/8+1)*cypix */
   } fillsym[] =  {
   {
   {8, 8},{
   0x00,    /*  ........  */
   0x44,    /*  .$...$..  */
   0x38,    /*  ..$$$...  */
   0xFE,    /*  $$$$$$$.  */
   0x38,    /*  ..$$$...  */
   0x44,    /*  .$...$..  */
   0x00,    /*  ........  */
   0x00,    /*  ........  */
   }},
   {
   {8, 8},{
   0x00,    /*  ........  */
   0x7E,    /*  .$$$$$$.  */
   0x42,    /*  .$....$.  */
   0x42,    /*  .$....$.  */
   0x42,    /*  .$....$.  */
   0x42,    /*  .$....$.  */
   0x7E,    /*  .$$$$$$.  */
   0x00,    /*  ........  */
   }}
   };

void gfillsym_ex(void)
   {
   ginit();
   gfillsym( 0, 0, 63, 31,  (PGSYMBOL) &fillsym[0]);
   gfillsym( 0, 32, 63, 63, (PGSYMBOL) &fillsym[1]);
   }

/*
   void gfillvp( GXT xs, GYT ys, GXT xe, GYT ye, SGUINT f );
*/
void gfillvp_ex(void)
   {
   ginit();
   /* Create a viewport with a grey frame */
   gfillvp(0,0,GDISPW-1, GDISPH-1, 0xaa55);
   gselvp(1);
   gsetvp(10,10, GDISPW-11, GDISPH-11);
   gfillvp(0,0, ggetvpw()-1, ggetvph()-1, 0x0);
   }

/*
   GCOLOR ggetcolorb(void)
*/
void check_print_color(const char *str)
   {
   if (ggetcolorb() == ggetcolorf()) /* Prevent "invisible" text */
      {
      /* Text will be invisible. Force set of resonable values */
      gsetcolorb(G_WHITE);
      gsetcolorf(G_BLACK);
      }
   gputs(str);
   }

void ggetcolorb_ex(void)
   {
   ginit();
   gputs("Using default colors\n");
   gsetcolorb(G_BLUE);  /* Change foreground and background colors */
   gsetcolorf(G_WHITE);
   check_print_color("Line 2\n");
   gsetcolorf(G_BLUE);          /* Blue on blue would result in "invisible" text */
   check_print_color("Line 3"); /* so this call triggers color reset to B&W */
   }

void select_foreground_color(void)
   {
   GCOLOR c = ggetcolorb();
   if (c == G_BLACK)
      c = G_WHITE;
   else
   if (c == G_RED)
      c = G_WHITE;
   else
   if (c == G_BLUE)
      c = G_WHITE;
   else
   if (c == G_GREEN)
      c = G_WHITE;
   else
   if (c == G_YELLOW)
      c = G_BLACK;
   else
   if (c == G_MAGENTA)
      c = G_BLACK;
   else
   if (c == G_CYAN)
      c = G_BLACK;
   else
   if (c == G_WHITE)
      c = G_BLACK;
   else
      c = G_BLACK;
   gsetcolorf(c);
   }

/*
   GCOLOR ggetcolorf(void)
*/
void ggetcolorf_ex(void)
   {
   const GCOLOR color[] =
   {G_BLACK,G_RED,G_GREEN,G_YELLOW,G_BLUE,G_MAGENTA,G_CYAN,G_WHITE};
   int i;
   ginit();

   /* Test color combinations */
   for(i = 0; i < (sizeof(color)/sizeof(GCOLOR)); i++)
      {
      gsetcolorb(color[i]);
      select_foreground_color();
      gputs(" *");
      }
   }

/*
   PGCODEPAGE ggetcp(void)
*/

/* Print a string with a specific font */
void fp_puts(PGFONT new_fp, char *str)
   {
   PGFONT fpold;
   PGCODEPAGE cpold;

   /* Backup codepage setting */
   cpold = ggetcp();
   /* Backup font and select new font (and codepage) */
   fpold = gselfont(new_fp);

   /* output string with new font */
   gputs(str);

   /* Restore font and codepage setting */
   gselfont(fpold);
   gselcp(cpold);
   }

void ggetcp_ex(void)
   {
   ginit();
   fp_puts(&times9, "\nLine 1");  /* Output with times9 font  */
   fp_puts(&times16,"\nLine 2");  /* Output with times16 font */
   gputs("\nLine 3");             /* Output with default font */
   }

/*
   SGUCHAR ggetcxpos(void);
*/
void ggetcxpos_ex(void)
   {
   ginit();
   gputs("Menu 1");
   /* Place next menu text relative to the first */
   gsetcpos(ggetcxpos() + 2, ggetcypos());
   gputs("Menu 2");
   }

/* Menu 1  Menu 2 */

/*
   SGUCHAR ggetcypos(void);
*/
/*
   List text table elements under each
   other at current X,Y pos
*/
void list_table(char **strp)
   {
   GXT x;
   if (strp == NULL)
      return;
   x = ggetcxpos();      /* Get current position */
   while(*strp != NULL)  /* List elements */
      {
      gputs(*strp++);
      gsetcpos((unsigned char) x,ggetcypos()+1);
      }
   }

void ggetcypos_ex(void)
   {
   static char *strp[] = {"Text 1","Text 2","Text 3", NULL};
   ginit();
   gsetcpos(3,3);
   list_table(strp);
   }
/*
   Text 1
   Text 2
   Text 3
*/

/*
   GYT ggetfh(void);
*/
void ggetfh_ex(void)
   {
   char str[120];
   ginit();
   gselfont(&times9);
   sprintf(str,"Times 9 font h x w is:"
               "\n %u x %u pixels",
               (unsigned int) ggetfh(),
               (unsigned int) ggetfw());
   gputs(str);
   }

/*
  PGPONT ggetfont(void)
*/
void check_font(void)
   {
   if (ggetfont() == &times13)
      {
      gputs("times13 is used");
      }
   else
   if (ggetfont() == &times9)
      {
      gputs("times9 is used");
      }
   else
      {
      gputs("Some other font is used");
      }
   }

void ggetfont_ex(void)
   {
   ginit();
   gselfont(&times9);
   check_font();
   }

/*
   SGUCHAR ggetvpnum( void )
   PGFONT ggetfont( void );
   PGCODEPAGE ggetcp( void );
*/
void make_viewport_rows(SGUCHAR vpstart, SGUCHAR numrows)
   {
   GYT y,yd;
   SGUCHAR oldvp;
   yd = GDISPH/numrows;
   oldvp = ggetvpnum();
   for (y = 0; y < GDISPH; y += yd, vpstart++)
      {
      gselvp(vpstart);
      gsetvp(0,y,GDISPW-1,y+yd-1);
      }
   gselvp(oldvp);
   }

void copy_font_setup(SGUCHAR vp2, SGUCHAR vp1)
   {
   PGFONT fp;
   PGCODEPAGE cp;
   SGUCHAR oldvp;

   oldvp = gselvp(vp1);   /* Get old settings */
   fp = ggetfont();
   cp = ggetcp();

   gselvp(vp2);           /* Copy to new viewport */
   gselfont(fp);
   gselcp(cp);

   gselvp(oldvp);         /* Restore old viewport */
   }

void ggetfont_ex2(void)
   {
   ginit();
   gselfont(&times9);

   /* split screen in 4 horizontal viewports */
   make_viewport_rows(0, 4);

   /* Let viewports 0,2 use the same font */
   copy_font_setup(0,2);

   /* Output some text */
   gselvp(0);
   gputs("First viewport");
   gselvp(1);
   gputs("Viewport 1");
   gselvp(2);
   gputs("Viewport 2");
   gselvp(3);
   gputs("Last viewport");
   }

/*
   GXT ggetfw(void);
*/
void ggetfw_ex(void)
   {
   char str[80];
   ginit();
   gselfont(&times16);
   sprintf(str,"Times 16 font \n h x w is:"
               "\n %u x %u pixels",
               (unsigned int) ggetfh(),
               (unsigned int) ggetfw());
   gputs(str);
   }

/*
   GWCHAR ggetmbc(const char **str)
*/
void ggetmbc_ex(void)
   {
   #ifdef GMULTIBYTE
   const char mbstr[] = {"A\xB0\x47R"};
   const char *mstrp;
   char str[20];
   GWCHAR c;
   unsigned int i = 0;

   ginit();
   mstrp = &mbstr[0];  /* point to multi-byte string */

   /* Display character values as hex */
   while ((c = ggetmbc(&mstrp)) != 0)
      {
      sprintf(str,"ch%u=0x%04X\n", i, (unsigned int) c);
      gputs(str);
      i++;
      }
   #endif
   }

/*
   GXT ggetspch(void);
   void gsetspch(GXT chsp);
*/
void ggetspch_ex( void )
   {
   char str[40];
   static const char txt1[] = "Length of this line:";
   int i;
   ginit();
   gselfont(&ariel9);

   /* Extra character spacing = 0 */
   for (i = 0; i < 4; i++)
      {
      gclrvp();
      gsetspch(i);
      sprintf(str,"Char spacing %u\n",(unsigned int) ggetspch());
      gputs(str);
      sprintf(str,"%s\n%u\n",txt1,(unsigned int)gpstrlen(txt1));
      gputs(str);
      }
   }

/*
   GXT ggetspln(void);
   void gsetspln(GXT chsp);
*/
void ggetspln_ex( void )
   {
   char str[40];
   static const char txt1[] = "Height of\nthese\n3 lines = ";
   int i;
   ginit();
   gselfont(&ariel9);

   /* Extra character spacing = 0*/
   for (i = 0; i < 15; i++)
      {
      gclrvp();
      gsetspln(i);
      sprintf(str,"Line spacing %u\n",(unsigned int) ggetspln());
      gputs(str);
      sprintf(str,"%s%u",txt1,(unsigned int)gpstrheight(txt1));
      gputs(str);
      }
   }


void ggetspln_ex2( void )
   {
   static const char txt1[] = "Length of this line:";
   static const char txt2[] = "Height of these\n 2 lines = ";
   char str[45];
   ginit();
   gselfont(&times9);
   gsetspch(1); /* Add extra character space (+1 pixel column) */
   gsetspln(2); /* Add extra text line space (+2 pixel rows)*/

   sprintf(str,"Char spacing %u. Line spacing %u\n",
      (unsigned int) ggetspch(),
      (unsigned int) ggetspln());
   gputs(str);
   sprintf(str,"%s%u\n",txt1,(unsigned int)gpstrlen(txt1));
   gputs(str);
   sprintf(str,"%s%u\n",txt2,(unsigned int)gpstrheight(txt2));
   gputs(str);
   }

/*
   void ggetsym( GXT xs, GYT ys, GXT xe, GYT ye, PGSYMBOL psym, SGUINT size );
*/
void ggetsym_ex(void)
   {
   /* Example only works if the display screen data can be read */
   #if (!defined( GHW_NO_LCD_READ_SUPPORT ) || defined(GBUFFER))
   char buf[GSYM_SIZE(11,16)]; /* Buffer to hold symbol of specified size */
   int i;
   ginit();

   /* Make figure of 10 boxes above each other */
   grectangle(5,5,15,20);  /* xb=5, yb=5, xe=5+(width-1), ye=5+(height-1) */
   ggetsym(5,5,15,20, (GSYMBOL *) &buf[0], sizeof(buf) );
   for (i=5; i < 25; i += 2)
      gputsym(i,i,(PGSYMBOL) &buf[0]);
   #endif
   }

/*
   GXT ggetsymw( SGUCHAR c );
*/

void ggetsymw_ex(void)
   {
   GXT w1,w2;
   char str[40];

   ginit();

   gselfont(&times9);  /* Select a soft font */
   gputs("Using times9 font\n");
   w1 = ggetsymw('i');
   w2 = ggetsymw('W');
   sprintf(str," 'i' width = %u,\n"
               " 'W' width = %u",
               (unsigned int) w1,
               (unsigned int) w2);
   gputs(str);
   }
/*
 Using times9 font
  'i' width = 2
  'W' width = 8
*/

void ggetsymw_ex2(void)
   {
   GXT w;
   char str[20];
   ginit();
   gselfont(&times13);

   w = ggetsymw('i');
   sprintf(str,"width of 'i': %u\n", (unsigned int) w);
   gputs(str);

   w = ggetsymw(':');
   sprintf(str,"width of ':': %u\n", (unsigned int)w);
   gputs(str);

   w = ggetsymw('8');
   sprintf(str,"width of '8': %u\n", (unsigned int)w);
   gputs(str);
   }

/* Output:
  "width of 'i': 5"
  "width of ':': 3"
  "width of '8': 8"
*/

/*
   SGBOOL ggetpixel( GXT xs, GYT ys );
*/
void ggetpixel_ex(void)
   {
   #ifndef GHW_NO_LCD_READ_SUPPORT
   int i;
   ginit();
   /* Make a background line pattern */
   gfillvp(0,0,GDISPW-1, GDISPH-1, 0x00ff);

   /* Draw double line with inverse background pattern */
   for (i = 0; (i < GDISPH-5) && (i < GDISPW-3); i ++)
      {
      gsetpixel( i, i, !ggetpixel( i, i ));
      gsetpixel( i+2, i, !ggetpixel( i+2, i ));
      }
   #endif
   }


/*
   void ggetvp(GXT *xs, GYT *ys, GXT *xe, GYT *ye )
   void gresetposvp(void)
*/
void addframe(void)
   {
   GXT xs, xe;
   GYT ys, ye;
   /* Get current vp position in full screen coordinates */
   ggetvp(&xs,&ys,&xe,&ye);

   /* Add frame, relative to full screen */
   gresetposvp();
   grectangle(xs,ys,xe,ye);

   /* Adjust viewport to inside frame */
   gsetvp(xs+1,ys+1,xe-1,ye-1);
   }

void ggetvp_ex(void)
   {
   ginit();

   gselfont(&times9);
   addframe(); /* Frame around screen edge */

   gsetvp(10,10,GDISPW-11,13+ggetfh());
   gsetmode(GALIGN_HCENTER);

   addframe(); /* Thick frame */
   addframe();
   gputs(" Framed ");
   }

/*
   GYT ggetvph(void);
*/
void ggetvph_ex(void)
   {
   ginit();
   /* draw a box around the edges of the current viewport */
   grectangle(0,0,ggetvpw()-1,ggetvph()-1);
   }

/*
   SGUCHAR ggetvpnum(void)
*/
/*
   Print value. Use hex formatting for
   viewport number 3-5 else decimal format
*/
void put_value(unsigned int val)
   {
   SGUCHAR vp;
   char str[20];
   vp = ggetvpnum();  /* get current viewport */
   sprintf(str, ((vp >= 3) && (vp <= 5)) ? "%02X" : "%02u", val);
   gputs(str);
   }

void ggetvpnum_ex(void)
   {
   ginit();
   /* Create a viewport at the lower part of the screen */
   gselvp(4);
   gsetvp(0,GDISPH/2, GDISPW-1, GDISPH-1);

   /* Use viewport dependent formatting */
   gselvp(2);
   put_value( 15 );  /* Digit displayed using decimal format */
   gselvp(4);
   put_value( 15 );  /* Digit displayed using hex format */
   }

/*
   GXT ggetvpw(void);
*/
void ggetvpw_ex(void)
   {
   GXT x,xstep;
   ginit();

   /* Make saw tooth figure */
   gmoveto(0,ggetvph()-1);
   for (x = 0, xstep = ggetvpw()/10; x < ggetvpw()-xstep; x+=xstep)
      {
      glineto(ggetxpos()+xstep,0);
      glineto(ggetxpos(),ggetvph()-1);
      }
   }

/*
   GXT ggetxpos(void);
*/
void ggetxpos_ex(void)
   {
   GXT x,xstep;
   ginit();

   /* Make saw tooth figure */
   gmoveto(0,ggetvph()-1);
   for (x = 0, xstep = ggetvpw()/10; x < ggetvpw()-xstep; x+=xstep)
      {
      glineto(ggetxpos()+xstep,0);
      glineto(ggetxpos(),ggetvph()-1);
      }
   }

/*
   GYT ggetypos(void);
*/
void ggetypos_ex(void)
   {
   ginit();
   /* Draw 10 stairs in current viewport */
   gmoveto(0,0);
   while ((ggetxpos() < ggetvpw()-10) &&
          (ggetypos() < ggetvph()-10))
      {
      glineto(ggetxpos()+ggetvpw()/10, ggetypos());
      glineto(ggetxpos(), ggetypos()+ggetvph()/10);
      }
   }

/* Example demonstrates how a area before, above, after or
   below a text can be cleared individually, via use of the
   position rapporting function */

/* Clear rest of text line in the current viewport*/
void gclr_eol(void)
   {
   gfillvp(ggetxpos(),ggetypos()-(ggetfh()-1),ggetvpw()-1, ggetypos(), 0);
   }

/* Clear from end of text to end of viewport */
void gclr_end(void)
   {
   gclr_eol();
   gfillvp(0,ggetypos()+1,ggetvpw()-1, ggetvph()-1, 0);
   }

/* Clear from before cursor position to left viewport edge */
void gclr_bol(void)
   {
   if (ggetxpos() > 0)
      gfillvp(0,ggetypos()-(ggetfh()-1),ggetxpos()-1, ggetypos(), 0);
   }

/* Clear from before cursor position to begin of viewport*/
void gclr_begin(void)
   {
   gclr_bol();
   if (ggetypos() > ggetfh()-1)
      gfillvp(0,0, ggetvpw()-1, ggetypos()-ggetfh(), 0);
   }

/*
   Use of position and size rapporting
     GXT ggetxpos(void);
     GYT ggetypos(void);
     GXT ggetvpw(void);
     GYT ggetfh(void);
   Ex blanking of areas around a text. How to refresh / redraw viewport
   in non-buffered mode without text "blinking"
*/
void ggetxpos_ex2(void)
   {
   ginit();
   gselfont(&times9); /* Select a softfont */
   grectangle(5,5,ggetvpw()-6,5+ggetfh()*3); /* make frame */
   gsetvp(6,6,ggetvpw()-7,4+ggetfh()*3); /* move inside frame */

   /* Fill background so we can see blanking operations, keep cursor position  */
   gfillvp(0,0,ggetvpw()-1,ggetvph()-1,0xaa55);
   gputs("Test 123");
   gclr_eol();

   gfillvp(0,0,ggetvpw()-1,ggetvph()-1,0xaa55);
   gputs("Test 456");
   gclr_end();

   gputs("\n   ");
   gfillvp(0,0,ggetvpw()-1,ggetvph()-1,0xaa55);
   gclr_bol();
   gputs("Test 789");

   gfillvp(0,0,ggetvpw()-1,ggetvph()-1,0xaa55);
   gclr_begin();
   gputs("Test 901");
   }

/*
   SGUINT ghw_blksize( GXT xs, GYT ys, GXT xe, GYT ye );
*/
void ghw_blksize_ex(void)
   {
   SGUCHAR *ptr;
   GBUFINT size;
   ginit();

   if ((ptr = (SGUCHAR *)
        malloc((size= (GBUFINT) ghw_blksize(10,10,50,50)))) != NULL)
      {
      /* Dynamic buffer allocation was ok, read area */
      ghw_rdblk(10,10,50,50,ptr,size);

      /* Fill area with some data */
      gfillvp(10,10,50,50,0x5555);

      ghw_wrblk(10,10,50,50,ptr);  /* Restore old buffer */
      free(ptr);
      }
   }

/*
   SGUCHAR ghw_cont_change(SGCHAR contrast_diff);
*/
void ghw_cont_change_ex(void)
   {
   #ifdef GHW_INTERNAL_CONTRAST
   SGUCHAR old_contrast;
   ginit();
   gputs("Contrast regulation");
   old_contrast = ghw_cont_change(0);  /* Get contrast value */

   while (ghw_cont_change(1) < 99)
      {
      /* Step up */
      }
   while (ghw_cont_change(-1) > 0)
      {
      /* Step down */
      }
   ghw_cont_set(old_contrast);         /* Restore old value */
   #endif
   }

/*
   SGUCHAR ghw_cont_set(SGUCHAR contrast);
*/
void ghw_cont_set_ex(void)
   {
   #ifdef GHW_INTERNAL_CONTRAST
   SGUCHAR old_contrast;
   ginit();
   gputs("Contrast regulation");
   old_contrast = ghw_cont_set(0); /* Test min */
   ghw_cont_set(99);               /* Test max */
   ghw_cont_set(old_contrast);     /* Restore */
   #endif
   }

/*
   void ghw_dispoff(void);
*/
void ghw_dispoff_ex(void)
   {
   ginit();
   gputs("The display is now turned off");
   ghw_dispoff();
   }

/*
   void ghw_dispon(void);
*/
void ghw_dispon_ex(void)
   {
   ginit();
   gputs("The display is now turned off");
   ghw_dispoff();
   gputs("\nThis message is shown when "
         "the display is turned on again");
   ghw_dispon();
   }

/*
   SGUCHAR ghw_err(void);
*/
void ghw_err_ex(void)
   {
   ginit();

   gputs("Test message");

   /* Check if hardware driver was ok */
   if (ghw_err())
      G_ERROR("Hardware error detected");
   }

/*
   SGBOOL ghw_loadsym( PGSYMBOL psymtab, SGUCHAR nosym, SGUCHAR offset);

   This example require that the LCD controller support download of
   hardware fonts
*/

struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR b[8];       /* Symbol data, size = (cxpix/8+1)*cypix */
   } downloadsym[] =  {
   {{8, 8},{
   0x00,    /*  ........  */
   0x40,    /*  .$......  */
   0x20,    /*  ..$.....  */
   0x10,    /*  ...$....  */
   0x20,    /*  ..$.....  */
   0x40,    /*  .$......  */
   0x70,    /*  .$$$....  */
   0x00,    /*  ........  */
   }},
   {{8, 8},{
   0x00,    /*  ........  */
   0x10,    /*  ...$....  */
   0x20,    /*  ..$.....  */
   0x40,    /*  .$......  */
   0x20,    /*  ..$.....  */
   0x10,    /*  ...$....  */
   0x70,    /*  .$$$....  */
   0x00,    /*  ........  */
   }},
   {{8, 8},{
   0x70,    /*  .$$$....  */
   0x88,    /*  $...$...  */
   0x88,    /*  $...$...  */
   0x88,    /*  $...$...  */
   0x88,    /*  $...$...  */
   0x50,    /*  .$.$....  */
   0xD8,    /*  $$.$$...  */
   0x00,    /*  ........  */
   }},
   {{8, 8},{
   0x00,    /*  ........  */
   0x00,    /*  ........  */
   0x48,    /*  .$..$...  */
   0x48,    /*  .$..$...  */
   0x48,    /*  .$..$...  */
   0x48,    /*  .$. $...  */
   0x78,    /*  .$$$$...  */
   0x40,    /*  .$......  */
   }}
   };

#define  GREAT_EQU  "\x80" /* Define symbolic names for symbols */
#define  LESS_EQU   "\x81"
#define  OHM        "\x82"
#define  MICRO      "\x83"

#ifndef GHW_NO_HDW_FONT

void ghw_loadsym_ex(void)
   {
   ginit();

   /* Download special symbols in character generator RAM */
   if (ghw_loadsym((PGSYMBOL) &downloadsym[0], 4, 0x80))
      ghw_puterr("\n Font load error");

   /* Print messages using downloaded symbols */
   gputs("\nZ " GREAT_EQU " 60 " OHM );
   gputs("\nU " LESS_EQU  " 22 " MICRO "V");
   }

#else

void ghw_loadsym_ex(void)
   {
   /* Load symbols is not supported so ghw_loadsym is a error flag constant
      The test message should be generated */
   if (ghw_loadsym((PGSYMBOL) &downloadsym[0], 4, 0x80))
      ghw_puterr("\nDownload\nfont is not\nsupported\nby this\ncontroller");
   }
#endif

/*
   void ghw_puterr( char *str );
*/
void ghw_puterr_ex(void)
   {
   /* Init display and draw an error message
      on the center of the display */
   ghw_puterr("This is a\ncentered\nmessage");
   }

/*
   void ghw_rdblk( GXT xs, GYT ys, GXT xe, GYT ye, SGUCHAR *dest, SGUNIT bufsize );
*/

void ghw_rdblk_ex(void)
   {
   #ifndef GHW_NO_LCD_READ_SUPPORT
   unsigned char blkbuf[GHW_BLK_SIZE(10,10,50,50)];
   ginit();
   /* Draw a cross */
   glineto(GDISPW-1,GDISPH-1);      /* Starts from 0,0 */
   gmoveto(GDISPW-1,0);
   glineto(0,GDISPH-1);

   ghw_rdblk(10,10,50,50,blkbuf,sizeof(blkbuf)); /* Read area */
   gfillvp(10,10,50,50,0x55aa);   /* Fill area */
   /* Do some thing else */
   ghw_wrblk(10,10,50,50,blkbuf); /* Restore old area */
   #endif
   }

/*
  void ghw_restoreblk(SGUCHAR *src)
*/
SGUCHAR *popup_create(const char *str)
   {
   GYT ys,ye;
   GXT xs,xe;
   GBUFINT size;
   SGUCHAR oldvp;
   SGUCHAR *ptr;

   if (str == NULL)
      return NULL;
   oldvp = gselvp(GNUMVP-1); /* Use last viewport as temp viewport */

   /* Set text format */
   gresetvp();
   gselfont(&times9);

   /* get string height & width incl frame */
   ye = gpstrheight(str) + 8;
   xe = (GXT) gpstrwidth(str) + 8;

   /* Calculate centered screen area positions */
   xs = (GDISPW-xe)/2;
   ys = (GDISPH-ye)/2;
   xe = xs+xe-1;
   ye = ys+ye-1;

   /* Save background */
   if ((ptr = (SGUCHAR *)
        malloc((size=(GBUFINT)ghw_blksize(xs,ys,xe,ye)))) != NULL)
      {
      /* Dynamic buffer allocation was ok, backup area */
      ghw_rdblk(xs,ys,xe,ye,ptr,size);

      gsetupdate(GUPDATE_OFF);     /* Activate delayed update (if GBUFFER defined) */

      /* Clear popup area */
      gsetvp(xs,ys,xe,ye);
      gclrvp();

      /* Draw double frame */
      grectangle(0,0,ggetvpw()-1,ggetvph()-1);
      grectangle(2,2,ggetvpw()-3,ggetvph()-3);

      /* Narrow viewport to inside frame, and output text */
      gsetvp(xs+4,ys+4,xe-4,ye-4);
      gputs(str);
      gsetupdate(GUPDATE_ON);      /* Deactivate delayed update. */
                                   /* Flush changes to the LCD display */
      }

   gselvp(oldvp);  /* Restore old viewport */
   return ptr;
   }

void popup_close(SGUCHAR *background)
   {
   if (background != NULL)
      {
      /* restore background on the position
         stored in the buffer by ghw_rdblk(..) */
      ghw_restoreblk(background);
      free((void *) background);
      }
   }

void ghw_restoreblk_ex(void)
   {
   SGUCHAR *ptr1;
   SGUCHAR *ptr2;
   ginit();

   /* Make a background pattern */
   gfillvp(0,0,GDISPW-1, GDISPH-1, 0xaa55);

   /* Show popup message on top of background */
   ptr1 = popup_create("Show\n"
                       "popup\n"
                       "message");

   /* Second popup message on top of the first */
   ptr2 = popup_create("Second info");

   /* Remove second message */
   popup_close(ptr2);

   /* Remove first message */
   popup_close(ptr1);
   }

/*
   GXT gvpxl( SGUCHAR vp );
   GXT gvpxr( SGUCHAR vp );
   GYT gvpyb( SGUCHAR vp );
   GYT gvpyt( SGUCHAR vp );
*/
/*
   Save copy of named viewport content on heap.
   Return heap pointer to be used with ghw_restoreblk(..)
   The pointer must be freed with free(..)
*/
SGUCHAR *save_vp_background( SGUCHAR vp )
   {
   SGUCHAR *p;
   GBUFINT size;
   size = (GBUFINT) ghw_blksize(gvpxl(vp),gvpyt(vp),gvpxr(vp),gvpyb(vp));
   if ((p = (SGUCHAR *) malloc(size)) != NULL)
      ghw_rdblk(gvpxl(vp),gvpyt(vp),gvpxr(vp),gvpyb(vp),p,size);
   return p;
   }

/*
   Creation, save and restore of colored background
   Show output of transperant text on multicolored background.
*/
void ghw_restoreblk_ex2(void)
   {
   const GCOLOR palette[] = {G_RED,G_ORANGE,G_YELLOW,G_GREEN,G_CYAN,G_MAGENTA,G_WHITE};
   GXT x,xe;
   SGINT c;
   SGUCHAR *background;
   ginit();

   /* Make a framed viewport */
   grectangle(9,GDISPH/4,GDISPW-10,(GDISPH*3)/4);
   gsetvp(10,GDISPH/4+1,GDISPW-11,(GDISPH*3)/4-1);
   gselfont(&ariel18);    /* Must be a soft font */

   /* Fill viewport with colored columns */
   for (x=0,c=0;x<ggetvpw();c++,x+=8)
      {
      xe = x+7;
      if (xe >= ggetvpw())
         xe = ggetvpw()-1;
      gsetcolorf(palette[c%(sizeof(palette)/sizeof(palette[0]))]);
      gfillvp(x,0,xe,ggetvph()-1,0xffff);
      }

   /* Take copy of viewport content */
   if ((background = save_vp_background( ggetvpnum() )) != NULL)
      {
      /* Show transperant text output centred in viewport */
      gsetcolorf(G_BLACK);
      gsetmode(GTRANSPERANT|GALIGN_VCENTER|GALIGN_HCENTER|GWORD_WRAP);

      /* Using gputs */
      gputs("TEXT STRING");

      /* Restore background */
      ghw_restoreblk(background);

      /* Use text output centered to ancher point */
      /* Text start, end is alowed to exceed viewport */
      gsetpos(ggetvpw()/2,ggetvph()/2);
      gputsrot("TEXT STRING",0);

      for (c=45; c <= 360; c += 45)
         {
         /* Restore background */
         ghw_restoreblk(background);

         /* rotate text */
         gsetpos(ggetvpw()/2,ggetvph()/2);
         gputsrot("ROTATED TEXT",G_DEGREE_TO_RADIAN(c));
         }

      free(background);
      }
   }

/*
   void ghw_wrblk( GXT xs, GYT ys, GXT xe, GYT ye, SGUCHAR *src );
*/
void ghw_wrblk_ex(void)
   {

   #ifndef GHW_NO_LCD_READ_SUPPORT
   unsigned char blkbuf[GHW_BLK_SIZE(0,0,10,10)];
   ginit();

   glineto(10,10);                /* Draw a cross */
   gmoveto(10,0);
   glineto(0,10);

   ghw_rdblk(0,0,10,10,blkbuf,sizeof(blkbuf));   /* Read area */
   ghw_wrblk(10,10,20,20,blkbuf);        /* Copy area 3 times */
   ghw_wrblk(10,10,20,20,blkbuf);        /* Copy area 3 times */
   ghw_wrblk(10,0,20,10,blkbuf);
   ghw_wrblk(0,10,10,20,blkbuf);

   #endif
   }

/* Four crosses */

/*
   void ghw_wrblk( GXT xs, GYT ys, GXT xe, GYT ye, SGUCHAR *src );
*/
void ghw_wrblk_ex2(void)
   {
   int i;
   GYT y;
   GXT x;
   GBUFINT size;
   SGUCHAR *p;

   ginit();
   y = GDISPH/2-5;
   x = GDISPW/2-7;
   y++;

   /* Draw shadows box */
   gcrectangle(x,  y,  x+13,y+14, G_BLACK);
   gcrectangle(x+1,y+1,x+12,y+13, G_DARKGREY);
   gcrectangle(x+2,y+2,x+11,y+12, G_LIGHTGREY);
   gcrectangle(x+3,y+3,x+10,y+11, G_WHITE);
   gcrectangle(x+4,y+4,x+9, y+10, G_LIGHTGREY);
   gcrectangle(x+5,y+5,x+8, y+9,  G_DARKGREY);
   gcrectangle(x+6,y+6,x+7, y+8,  G_BLACK);

   /* Copy to buffer */
   size = ghw_blksize(x,y,x+13,y+14);
   p = malloc(size);
   ghw_rdblk(x,y,x+13,y+14,p,size);

   /* Paste buffer */
   x = 2;
   y = 4;
   for(i=0; i<4; i++, x+=9, y+= 9)
      ghw_wrblk(x,y,x+13,y+14,p);
   free(p);
   }

/*
   void ghw_wrblk( GXT xs, GYT ys, GXT xe, GYT ye, SGUCHAR *src );

   Test block read - write at all postions over a storage unit
*/
void ghw_wrblk_ex3(void)
   {
   #ifndef GHW_NO_LCD_READ_SUPPORT
   unsigned int s,d;
   unsigned char blkbuf[GHW_BLK_SIZE(0,0,10,10)];
   ginit();

   /* Test block read - write at all postions over a storage unit */
   for (s = 0; s < 8; s++)
     {
     gclrvp();
     /* Draw figure */
     gmoveto(s,s);
     glineto(s+10,s);
     glineto(s+10,s+10);
     glineto(s,s+10);
     glineto(s,s);
     glineto(s+10,s+10);

     /* Read figure */
     ghw_rdblk(s,s,s+10,s+10,blkbuf,sizeof(blkbuf));   /* Read area */

     /* Write figure back */
     for (d = s; d < 8+s; d++)
        ghw_wrblk(d+10,d+15,d+20,d+25,blkbuf);        /* Write area 8 times */
     }
   #endif
   }

/*
   SGUCHAR ginit(void);
*/
void ginit_ex(void)
   {
   if (ginit() != 0)
      {
      G_ERROR("Init error");
      }
   else
      gputs("Init ok");
   }

/*
  ginvertvp( GXT xb, GYT yb, GXT xe, GYT ye)
*/
void ginvertvp_ex(void)
   {
   #ifndef GHW_NO_LCD_READ_SUPPORT
   GXT x = 10;
   GYT y = 15;
   char *str[] = {"MENU1"};
   ginit();

   gselfont(&msfont); /* Select a soft font */

   gsetpos(x+1,y);
   gputs(str[0]);

   /* Invert text area to create a block cursor */
   ginvertvp(x,y-ggetfh(), x+gpstrlen(str[0]),y);

   /* Invert text area again to remove block cursor */
   ginvertvp(x,y-ggetfh(), x+gpstrlen(str[0]),y);
   #endif
   }

/*
  ginvertvp( GXT xb, GYT yb, GXT xe, GYT ye)
*/
#ifndef GHW_NO_LCD_READ_SUPPORT
static void format_bar(SGUCHAR vp, GXT xb, GYT yb, GXT xe, GYT ye,
                       GCOLOR fore, GCOLOR back, GYT *barvalue)
   {
   gselvp(vp);
   gresetvp();                 /* Clear to absolute coodinates */
   grectangle(xb,yb,xe,ye);    /* Draw frame */
   gsetvp(xb+1,yb+1,xe-1,ye-1);/* Move viewport inside frame */
   gsetcolorf(fore);           /* Set bar colors */
   gsetcolorb(back);
   gfillvp(0,0,ggetvpw()-1, ggetvph()-1, 0); /* Clear to background color */
   *barvalue = ggetvph()-1;    /* Save current bar coordinate */
   gfillvp(0, *barvalue, ggetvpw()-1, *barvalue, 0xffff); /* Set bar 0 */
   }

/* Show relative bar value 0-100% */
static void set_bar(SGUCHAR vp, SGUINT value, GYT *barvalue)
   {
   GYT y_value;
   gselvp(vp);
   if (value > 100)  /* Limit to 0-100 range */
      value = 100;

   /* Calculate relative positions, 0 in the bottom */
   y_value = (GYT)(((100 - value) * (ggetvph()-1)) / 100);

   if (*barvalue > y_value)
      {
      ginvertvp(0, y_value, ggetvpw()-1,*barvalue-1); /* Invert to foreground */
      }
   else
   if (*barvalue < y_value)
      {
      ginvertvp(0, *barvalue, ggetvpw()-1,y_value-1); /* Invert to background */
      }
   *barvalue = y_value;
   }
#endif

void ginvertvp2_ex(void)
   {
   #ifndef GHW_NO_LCD_READ_SUPPORT
   GYT bar1,bar2,bar3;
   int i;
   ginit();

   /* Create 3 bar graphs */
   format_bar(1,(GDISPW/10),  GDISPH/10,(GDISPW/10)*3, GDISPH-1, G_RED,  G_YELLOW, &bar1);
   format_bar(2,(GDISPW/10)*4,GDISPH/10,(GDISPW/10)*6, GDISPH-1, G_BLUE, G_YELLOW, &bar2);
   format_bar(3,(GDISPW/10)*7,GDISPH/10,(GDISPW/10)*9, GDISPH-1, G_GREEN,G_YELLOW, &bar3);

   /* Simulate some dynamic inputs for the 3 bar graphs */
   for (i=0; i < GDISPH*5; i++)
      {
      /* Make sine values which vary bars between 0 and 100 */
      set_bar(1, (SGUINT) (sin(3.1415/180.0 * ((double) i  ))*50.0+50.0), &bar1);
      set_bar(2, (SGUINT) (sin(3.1415/180.0 * ((double) i*2))*50.0+50.0), &bar2);
      set_bar(3, (SGUINT) (sin(3.1415/180.0 * ((double) i*3))*50.0+50.0), &bar3);
      }
   #endif
   }


/*
  ginvertvp( GXT xb, GYT yb, GXT xe, GYT ye)
  +
  dynamic cursor setting example on proportional font strings
*/

/*
  Find graphic pixel pos for begin and end of a character in a
  string.

  pos is the string index for the character with cursor.

  xb,xe is updated to the x offset for the character in string.
  relative to the start of the string
*/
void getchpos(const char *str, GXT *xb, GXT *xe, int pos)
   {
   PGSYMBOL ps;
   int cnt = 0;
   *xe = 0;
   while(*str != 0)
      {
      ps = ggetfsym(*str, ggetfont());
      *xb = *xe;
      *xe += gsymw(ps);
      if (cnt == pos)
         {
         *xe -= 1; /* use last pixel pos in this symbol */
         return;
         }
      str++;
      cnt++;
      }
   *xb = *xe; /* Signal cursor is outside string */
   }

/*
  Draw pretext string + string with cursor. The total strings should not wrap.
  pos sets the block cursor at the character index in str2
  A pos larger than the character length of str2 (or a negative value) draw
  the text without the cursor.
*/
void draw_str_with_cursor(const char *str1, const char *str2, int pos)
  {
  GXT x,xb,xe;
  GYT y;
  if (str1 != NULL)
     gputs(str1);                /* Output pretext string */
  if (str2 != NULL)
     {
     x = ggetxpos();             /* String start offset for cursor string */
     y = ggetypos();
     getchpos(str2,&xb,&xe,pos); /* Calculate cursor pos */
     gputs(str2);                /* Output string */
     if (xe != xb)
        {
        /* Set cursor, compensate for character left alignment in symbol */
        ginvertvp((x+xb == 0) ? 0 : x+xb-1, y-(ggetfh()-1),x+xe,y);
        }
     }
  }

void ginvertvp3_ex(void)
  {
  const char str1[] = "Meas value = ";
  const char str2[] = "4567";
  int i;
  ginit();
  gselfont(&ariel9);

  /* Test cursor movement on str2 */
  for (i=0; str2[i] != 0; i++)
     {
     gsetpos(0,ggetfh());
     draw_str_with_cursor(str1,str2,i);
     }
  }

/*
   void glineto( GXT xs, GYT ys );
*/
void glineto_ex(void)
   {
   ginit();
   /* Draw a trapetz */
   gmoveto(10,10);
   glineto(50,10);
   glineto(40,50);
   glineto(0,50);
   glineto(10,10);
   }

/*
   void gmoveto( GXT xe, GYT ye );
*/
void gmoveto_ex(void)
   {
   ginit();
   /* Draw a full display cross */
   glineto(GDISPW-1,GDISPH-1);
   gmoveto(GDISPW-1,0);
   glineto(0,GDISPH-1);
   }

/*
   GYT gpstrheight( PGSTR str )
*/
void gpstrheight_ex(void)
   {
   GYT h;
   static const char str[] = {"Line 1\nLine 2\nLine 3"};
   ginit();

   /* Select current font */
   gselfont(&times9);

   /* string height in pixels */
   h = (GYT) gpstrheight(str);

   /* Make framed viewport */
   grectangle(2,2,GDISPW-3, h+6);
   gsetvp(4,4, GDISPW-5, h+4);

   /* Put text in frame */
   gputs( str );
   }

/*
   int gpstrlen( char *str );
*/
#include <stdio.h>

void gpstrlen_ex(void)
   {
   unsigned int size;
   char str1[] = "Pixel size of message is: ";
   char str2[80];

   ginit();
   gselfont(&times9);

   size = gpstrlen(str1);
   sprintf(str2,"%s %u",str1, size);
   gputs(str2);
   }

/*
  "Pixel size of message is: 107"
*/

/*
   GXT gpstrwidth( PGSTR str )
*/
void gpstrwidth_ex(void)
   {
   GYT h;
   GXT w;
   static const char str[] = {"ABC\nDEFG\nHIJK"};
   ginit();

   /* Select current font */
   gselfont(&times9);

   /* string height & width */
   h = (GYT) gpstrheight(str);
   w = (GXT) gpstrwidth(str);

   /* Fit viewport */
   grectangle(2,2,w+2+4, h+2+4);
   gsetvp(4,4, w+4, h+4);

   /* Put text in viewport */
   gputs( str );
   }

/*
   void gputch( SGUCHAR val );
*/
void gputch_ex(void)
   {
   SGUCHAR i;
   ginit();

   /* Dump ASCII characters */
   for (i=0x20; i < 128; i++)
      gputch(i);
   }

/*
   void gputchrot( char ch, float angle);
*/
void gputchrot_ex(void)
   {
   ginit();
   gselfont(&ariel18);   /* Write �ABC' rotated 90 degree's */
   gsetpos(50,50);
   gputchrot('A',PI/2);  /* Output and auto increment position */
   gputchrot('B',PI/2);
   gputchrot('C',G_DEGREE_TO_RADIAN(90)); /* 90 degree's = PI/2 */
   }

/*
   void gputcsym( PGSYMBOL psym );
*/
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR b[8];       /* Symbol data, size = (cxpix/8+1)*cypix */
   } return_arrow[] = {
   {
   {8, 8},{
   0x00,    /*  ........  */
   0x00,    /*  ........  */
   0x13,    /*  ...$..$$  */
   0x3F,    /*  ..$$$$$$  */
   0x7F,    /*  .$$$$$$$  */
   0x3F,    /*  ..$$$$$$  */
   0x10,    /*  ...$....  */
   0x00     /*  ........  */
   }}
   };

void gputcsym_ex(void)
   {
   ginit();

   gputs("Press ");
   gputcsym((PGSYMBOL) &return_arrow);
   gputs(" to start");
   }

/*
   void gputfsym( GXT x, GYT y, SGUINT index, PGFONT pfont )
*/
void gputfsym_ex(void)
   {
   ginit();

   /* Output symbol directly from font (symbol anker point is upper left corner) */
   gputfsym(10,20, 'A', &ariel18); /* Use specified font */
   gputfsym(0,0, 'A', ggetfont()); /* Use current (default) font*/

   gputs("\n Next symbol:"); /* Some text */
   /* Viewport line cursor is now at end of previous text.
      Add symbol at current viewport line cursor position */
   gputfsym(ggetxpos(),ggetypos()-(ggetfh()-1), 'B', ggetfont());  /* Add to end of text */
   /* In contrast to gputcsym() then gputfsym() leave the cursor unchanged. Just overwrite */
   gputfsym(ggetxpos(),ggetypos()-(ggetfh()-1), 'C', ggetfont());
   }

/*
   void gputs( char *str );
*/
#include <stdio.h>
void gputs_ex(void)
   {
   int i;
   char str[20];
   ginit();

   gputs("gputs handles\n \\n and \\r: \n");
   for (i=1; i < 30; i++)
      {
      sprintf(str,"\r Count : %02u", (unsigned int)i);
      gputs(str);
      }
   }

/*
   Show effect of gsetmode(..) alignement parameters
   on gputs(..) output.
*/
void gputs_allign(void)
   {
   gclrvp();
   /* Make viewport inside a frame */
   grectangle(ggetfw()-2,ggetfh()-2,GDISPW-(ggetfw()-3),ggetfh()*5+3);
   gsetvp(ggetfw(),ggetfh(),GDISPW-ggetfw(),ggetfh()*5);

   /* Cut text at right viewport edge */
   gsetmode( GLINECUT );
   gputs("2 lines to be cut\nat the viewport edge");

   /* Text is right aligned in viewport */
   gclrvp();
   gsetmode( GALIGN_RIGHT );
   gputs("Right 1\nRight 2\n");
   gputs("Test line wrapping");

   /* Text is centered vertically in viewport */
   gclrvp();
   gsetmode( GALIGN_VCENTER);
   gputs("Vertical\nCenter");

   /* Text is align to top or bottom */
   gclrvp();
   gsetmode( GALIGN_BOTTOM | GALIGN_LEFT);
   gputs("Align Bottom");
   gsetmode( GALIGN_TOP | GALIGN_LEFT );
   gputs("Align Top");

   /* Text is centered vertically in viewport at right edge */
   gclrvp();
   gsetmode( GALIGN_VCENTER | GALIGN_RIGHT);
   gputs("Right +\nVertical");

   /* Text is centered horizontally in viewport */
   gclrvp();
   gsetmode( GALIGN_HCENTER);
   gputs("Center\nhorizontal");

   /* Text is centered vertically and horizontally in viewport */
   gclrvp();
   gsetmode( GALIGN_VCENTER | GALIGN_HCENTER);
   gputs("Double\ncenter");
   }

void gputs_ex2(void)
   {
   ginit();

   /* Use SYSFONT */
   gputs_allign();

   /* Select a soft font */
   gresetvp();
   gselfont(&times9);
   gputs_allign();

   /* Check inverse mode + Allignment */
   gclrvp();
   gsetmode( GINVERSE | GALIGN_HCENTER );
   gputs("White on black\n");
   gsetmode( GNORMAL | GALIGN_HCENTER );
   gputs("Black on white");
   }

/*
   void gputsrot( PGCSTR str, float angle);
*/
void gputsrot_ex(void)
   {
   ginit();
   gselfont(&ariel18);    /* Must be a soft font */
   gsetpos(20,20);
   gputsrot("ABC",0);     /* Output text and auto increment position */
   gputsrot("ABC",-PI/2); /* 90 degrees, continue at end of prev. text */
   gputsrot("ABC",-PI);   /* 180 degrees */
   gputsrot("ABC",G_DEGREE_TO_RADIAN(-270)); /* 270 degree's = PI*1.5 */
   }

/* Write multiple lines vertically.
   Automatic alignment by setting text frame connection point */
void gputsrot_ex2(void)
   {
   ginit();
   gselfont(&ariel18);    /* Must be a soft font */
   gsetpos(GDISPW-1,0);   /* upper / right screen corner = anchor point */

   /* Anchor connected to text frame at lower / right corner */
   gsetmode(GALIGN_RIGHT|GALIGN_BOTTOM);

   /* Output rotated multi-line text aligned to upper right corner */
   gputsrot("Line 1\nNext line",PI/2);
   }

/* Output and clear of rotated text.
   Non blinking output when used with buffered mode */
void gputsrot_ex3(void)
   {
   int i;
   char str[] = "Rotated text";
   ginit();
   gselfont(&ariel18);   /* Must be a soft font */
   for (i=0;i<25;i++)
     {
     /* Show rotated text */
     gsetmode( GALIGN_HCENTER );
     gsetpos(GDISPW/2,GDISPH/2);
     gputsrot(str,(float)i*(PI/5)); /* Output text */
     gsetupdate(GUPDATE_ON);

     /*
         Insert any delays / breakpoint here
     */

     /* Delete exact positions of the previous rotated text */
     gsetupdate(GUPDATE_OFF); /* Avoid flicker (buffered mode) */
     gsetmode(GSYMCLR | GALIGN_HCENTER); /* Set clear mode */
     gsetpos(GDISPW/2,GDISPH/2);
     gputsrot(str,(float)i*(PI/5)); /* Clear text area */
     }
   }

/*
   void gputsym( GXT x, GYT y, PGSYMBOL psym );
*/
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR b[8];       /* Symbol data, size = (cxpix/8+1)*cypix */
   } GCODE FCODE arrow[1] = {
   {
   {8, 8},{
   0x08,    /*  ....$...  */
   0x0C,    /*  ....$$..  */
   0x7E,    /*  .$$$$$$.  */
   0x7F,    /*  .$$$$$$$  */
   0x7E,    /*  .$$$$$$.  */
   0x0C,    /*  ....$$..  */
   0x08,    /*  ....$...  */
   0x00     /*  ........  */
   }}
   };

void gputsym_ex(void)
   {
   GXT x;
   ginit();

   /* A line of arrows at the center of the viewport */
   for (x = 0; x < ggetvpw(); x += gsymw((PGSYMBOL) &arrow) )
      gputsym(x, ggetvph()/2, (PGSYMBOL) &arrow);
   }

/*
   void gputsym( GXT x, GYT y, PGSYMBOL psym );
*/

/* The table contains 2 symbols of 15,11 pixels.
   Organized as 2 byte columns and 11 rows */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR b[22];      /* Symbol data (2 bytes pr column * 11 row bytes  = 22 bytes data) */
   } GCODE FCODE speakersym[2] =
   {
   {{15, 11},{
   0x01,0x80,  /*  .......%%.......  */
   0x02,0x8C,  /*  ......%.%...%%..  */
   0x04,0xB0,  /*  .....%..%.%%....  */
   0x78,0x80,  /*  .%%%%...%.......  */
   0x48,0x80,  /*  .%..%...%.......  */
   0x48,0xBC,  /*  .%..%...%.%%%%..  */
   0x48,0x80,  /*  .%..%...%.......  */
   0x78,0x80,  /*  .%%%%...%.......  */
   0x04,0xB0,  /*  .....%..%.%%....  */
   0x02,0x8C,  /*  ......%.%...%%..  */
   0x01,0x80   /*  .......%%.......  */
   }}
   ,
   {{15, 11},{
   0x41,0x90,  /*  .%.....%%..%....  */
   0x22,0xA0,  /*  ..%...%.%.%.....  */
   0x14,0xC0,  /*  ...%.%..%%......  */
   0x78,0x80,  /*  .%%%%...%.......  */
   0x4D,0x80,  /*  .%..%%.%%.......  */
   0x4A,0x80,  /*  .%..%.%.%.......  */
   0x4D,0x80,  /*  .%..%%.%%.......  */
   0x78,0x80,  /*  .%%%%...%.......  */
   0x14,0xC0,  /*  ...%.%..%%......  */
   0x22,0xA0,  /*  ..%...%.%.%.....  */
   0x41,0x90   /*  .%.....%%..%....  */
   }}
   };

void gputsym2_ex(void)
   {
   GYT y = 2;
   ginit();

   /* Display speaker On / Off symbols (position in pixel coordinates) */
   gputsym(7, 3, (PGSYMBOL) &speakersym[0]); /* Show speaker on */
   gputsym(7, 3, (PGSYMBOL) &speakersym[1]); /* Show speaker off */

   /* Symbol embedded in text where hight of symbol > height of text */
   gselfont(&times9);   /* The font size influence the coordinates calculated by gsetcpos */
   gsetcpos(1,(SGUCHAR)(y++));     /* Set position in whole text lines */
   gputs("On ");
   gputcsym((PGSYMBOL) &speakersym[0]); /* Aligned to character bottom */
   gputs(" symbol");
   gsetcpos(1,(SGUCHAR)(y++));     /* Set next text line position */
   gputs("Next line.");
   }


/*
   void gputsym( GXT x, GYT y, PGSYMBOL psym );
*/
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR b[4];       /* Symbol data, size = (cxpix/8+1)*cypix */
   } GCODE FCODE box[1] =
   {{
   {5, 4},{
   0xF8,    /*  $$$$$...  */
   0x88,    /*  $...$...  */
   0x88,    /*  $...$...  */
   0xF8     /*  $$$$$...  */
   }}
   };

void gputsym3_ex(void)
   {
   GXT x;
   GYT y;
   ginit();
   x = 0;

   /* A line of boxes, placed corner by corner down at -45 degrees */
   for (y = 0; (y < ggetvph()) && (x < ggetvpw());
       x += gsymw((PGSYMBOL) &box), y += gsymh((PGSYMBOL) &box))
      gputsym(x, y, (PGSYMBOL) &box);
   }


/*
   void gputsymrot( SGINT x, SGINT y, float angle, PGSYMBOL psymbol,
                    SGINT xanchor, SGINT yanchor, GMODE symflag);
*/
static struct
   {
   GSYMHEAD sh;
   SGUCHAR b[42];
   }
GCODE FCODE testsym =
   {{16, 21},{
   0xFF,0xFF,  /*  %%%%%%%%%%%%%%%%  */
   0xFF,0xFF,  /*  %%%%%%%%%%%%%%%%  */
   0xC0,0x03,  /*  %%............%%  */
   0xCF,0xF3,  /*  %%..%%%%%%%%..%%  */
   0xCF,0xF3,  /*  %%..%%%%%%%%..%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC1,0x83,  /*  %%.....%%.....%%  */
   0xC0,0x03,  /*  %%............%%  */
   0xFF,0xFF,  /*  %%%%%%%%%%%%%%%%  */
   0xFF,0xFF   /*  %%%%%%%%%%%%%%%%  */
   }};

/*
   Output of rotated symbol + show effect of ancher offset
*/
void gputsymrot_ex(void)
   {
   SGINT x,ax, y,ay;
   ginit();
   x = GDISPW/2;
   y = GDISPH/2;
   ax = 20;
   ay = 10;

   /* Mark rotation center */
   gsetpixel((GXT)x,(GYT)y,1);
   gcircle(x,y,3,0);

   /* Rotation ancher pointt: x,y = Screen center,
      Rotation:               0 and 90 degrees
      Connection point .      Upper-left symbol corner
      Ancher offset           ax,ay = 20,10 pixel from rotation center
   */
   gputsymrot(x,y,   0, (PGSYMBOL) &testsym, ax, ay, GALIGN_LEFT|GALIGN_TOP);
   gputsymrot(x,y,PI/2, (PGSYMBOL) &testsym, ax, ay, GALIGN_LEFT|GALIGN_TOP);
   }

/*
   int gpwordlen( char *str );
*/
void gpwordlen_ex(void)
   {
   unsigned int size;
   static char str1[] = "Lenght in pixel of the first word is: ";
   char str2[80];

   ginit();
   gselfont(&times16);

   size = gpwordlen(str1);
   sprintf(str2,"%s %u",str1, size);
   gputs(str2);
   }

/*
   void grectangle( GXT xs, GYT ys, GXT xe, GYT ye );
*/
void grectangle_ex(void)
   {
   ginit();
   gsetcolorf(G_BLUE);

   /* Draw a double boarder around the viewport */
   grectangle(0,0,ggetvpw()-1,ggetvph()-1);
   grectangle(2,2,ggetvpw()-3,ggetvph()-3);
   }

/*
   void gresetvp(void);
*/
void gresetvp_ex(void)
   {
   ginit();

   /* Make a small viewport */
   gsetvp(10,10,60,60);
   gselfont(&times9);
   gputs("Using Times9");

   /* Reset view port to full screen */
   /* Set fonts to defaults */
   /* The old viewport content remains unchanged */
   gresetvp();
   gputs("Using SYSFONT");
   }

/*
 void groundrect( GXT ltx, GYT lty, GXT rbx, GYT rby, GXYT r, SGUCHAR skipflags);
*/
void makekey_vp(SGUCHAR vp, GXT x, GYT y, GXT w, GYT h)
   {
   vp = gselvp(vp); /* Select viewport */
   gresetposvp();   /* Assure absolute coordinates */

   /* Draw key */
   gsetcolorf(G_BLACK);
   gsetcolorb(G_LIGHTGREY);
   groundrect( x, y, x+w+h, y+h, h/2, GFRAME);

   /* Prepare keybody for key text */
   gsetvp( x+h/2, y+1, x+w+h/2, y+h-1);
   gselfont(&ariel9);
   gsetmode(GALIGN_HCENTER|GALIGN_VCENTER|GVPCLR);

   gselvp(vp);
   }

void groundrect_ex(void)
   {
   GXT w;
   GYT h;
   PGCSTR strp[] = {"Key Text 1","New Text","Move Up"};
   ginit();

   /* Scale key size reltive to a softfont font */
   gselfont(&ariel9);
   h = ggetfh()*2;
   w = gpstrlen(strp[0])+10;

   /* Draw keys, assign one viewport for each key */
   makekey_vp(1, 10, 10,     w, h);
   makekey_vp(2, 10, 10+2*h, w, h);

   /* Text on key 1 */
   gselvp(1);
   gputs(strp[0]);

   /* Text on key 2 */
   gselvp(2);
   gputs(strp[1]);

   /* Swap text on key 1 */
   gselvp(1);
   gputs(strp[2]);
   }


/*
 void groundrect( GXT ltx, GYT lty, GXT rbx, GYT rby, GXYT r, SGUCHAR skipflags);
 Show 15 different arc corner combinations
*/
void groundrect_ex2(void)
   {
   GXT x,w;
   GYT y,h;
   GXYT r;

   ginit();

   gsetcolorf(G_BLACK);  /* Frame line color */
   gsetcolorb(G_RGB_TO_COLOR(0xff,0xa0,0x18)); /* Custom fill color */

   #if (GDISPH/4 < 16)
   h=GDISPH/4;
   #else
   h=16;
   #endif

   #if (GDISPW/6 < 40)
   w=GDISPW/6;
   #else
   w = 40;
   #endif

   r = h/2;
   x = 5;
   y = 2;
   /* Draw 15 arc corner combinations */
   groundrect(x, y, x+w, y+h, r, GFRAME);
   y+=h+3;
   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_RB);
   y+=h+3;
   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_LB);

   y=2;
   x+=w+3;

   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_LB | GCARC_RB);
   y+=h+3;
   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_RT);
   y+=h+3;
   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_RT | GCARC_RB);

   y=2;
   x+=w+3;

   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_RT | GCARC_LB);
   y+=h+3;
   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_RT | GCARC_LB | GCARC_RB);
   y+=h+3;
   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_LT);

   y=2;
   x+=w+3;

   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_LT | GCARC_RB);
   y+=h+3;
   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_LT | GCARC_LB);
   y+=h+3;
   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_LT | GCARC_LB | GCARC_RB);

   y=2;
   x+=w+3;

   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_LT | GCARC_RT);
   y+=h+3;
   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_LT | GCARC_RT | GCARC_RB);
   y+=h+3;
   groundrect(x, y, x+w, y+h, r, GFRAME | GCARC_LT | GCARC_RT | GCARC_LB);
   }

/*
   SGUCHAR gscinit(PGSCREENS *scp);
*/
void gscinit_ex(void)
   {
   #ifdef GSCREENS
   PGSCREENS psc;
   psc = malloc(gscsize());        /* Create screen buffer */
   gscinit(psc);                   /* Initialize screen mode */
   gselfont(&msfont);              /* Use a soft font */
   gputs("Hello world 1");         /* Write to screen  */
   gscinit(NULL);  /* Disconnect active screen buffer from library */
   free(psc);                      /* Release screen buffer */

   ginit();                        /* Initialize non-screen mode */
   gputs("Hello world 2");         /* Use SYSFONT */
   gexit();
   #endif
   }

/*
   void gscrestore(PGSCREENS screen);
*/
void gscrestore_ex(void)
   {
   #ifdef GSCREENS
   PGSCREENS psc;
   psc = (PGSCREENS) malloc(gscsize()); /* Create screen buffer */
   gscinit(psc);                   /* Initialize screen mode */
   gselfont(&times9);              /* Use a soft font */
   gsetcolorf(G_BLUE);
   gputs("Hello world\n");         /* Write to screen */

   gscsave(psc);                   /* Save state as a recovery point */
   gscinit(NULL);                  /* Disconnect screen buffer from library
                                      Lets the display content remain unchanged */
   ginit();                        /* Initiate normal non-screen mode */
   gsetcolorf(G_RED);
   gputs("Some output");           /* Using SYSFONT */

   gscrestore(psc);                /* Restore screen mode from buffer */
   gputs("Continue on screen");

   gexit();             /* Remove any pointers the to active screen */
   free(psc);                      /* Release screen buffer */
   #endif
   }

/*
   void gscrollcvp(void);
*/
void gscrollcvp_ex(void)
   {
   ginit();
   gputs("Sysfont\nLine 2\n\n");
   gselfont(&times9);
   gputs("Times9\nLine 4\nLine 5");
   gscrollcvp(); /* Scroll, leave cursor unchanged */
   gputs("Line 6");
   }

/*
   void gscrollcvp(void);
*/
void gscrollcvp2_ex(void)
    {
    GYT y1,y2;
    GXT x1,x2;
    ginit();

   /* Test scroll at all y positions over the character height
      and at all x positions over the character width */
   for (y1 = 1, x1 = 1; y1 <= 9; y1++, x1++)
      {
      /* Reset viewport, Clear screen and init font */
      gresetvp();
      gclrvp();
      gselfont(&times13);
      /* gselfont(&times9); */

      /* Make framed viewport at new position */
      y2 = y1+3*ggetfh()-1;
      x2 = x1+45;
      grectangle(x1-1,y1-1,x2+1,y2+1);
      gsetvp(x1,y1,x2,y2);

      /* Fill viewport with text */
      gsetmode( GINVERSE );
      gputs("Test1");
      gsetmode( GNORMAL );
      gputs("\nTest2");
      gsetmode( GINVERSE );
      gputs("\nTest3");
      gsetmode( GNORMAL );

      /* Test viewport scroll */
      gscrollcvp();
      gscrollcvp();
      gscrollcvp();
      }
   }

/*
   void gscsave( PGSCREENS screen );
*/
void gscsave_ex(void)
   {
   #ifdef GSCREENS
   PGSCREENS psc1,psc2;
   psc1 = (PGSCREENS)malloc(gscsize());/* Create and init screen 1 */
   gscinit(psc1);                   /* so screen 1 is default */
   gselfont(&msfont);               /* Use a soft font */
   gputs("Screen 1");               /* Write to screen 1 */
   gscsave(psc1);                   /* Save screen 1 states */

   psc2 = (PGSCREENS)malloc(gscsize());/* Create and init screen 2 */
   gscinit(psc2);                   /* so screen 2 is default */
   gselfont(&times9);               /* Use a soft-font */
   gputs("Screen 2");               /* Write to screen 2 */
   gscsave(psc2);                   /* Save screen 2 state */

   gscrestore(psc1);                /* Switch to screen 1 */
   gputs("\nSwapped to screen 1");  /* Write to screen 1 */
   gscsave(psc1);                   /* Save screen 1 state */

   gscrestore(psc2);                /* Switch to screen 2 */
   gputs("\n - continue on screen 2");


   gexit();                /* Remove any pointers to active screen */
   free(psc2);
   free(psc1);
   #endif
   }

/*
   SGUINT gscsize(void);
*/
void gscsize_ex(void)
   {
   #ifdef GSCREENS
   PGSCREENS psc;
   GBUFINT size;
   size = gscsize();
   psc = (PGSCREENS)malloc(size); /* Create screen buffer */
   gscinit(psc);                   /* Initialize screen */
   gputs("Hello world");           /* Write to screen  */
   gexit();  /* Remove any library pointers the to active screen */
   free(psc);                      /* Release screen buffer */
   #endif
   }

/*
   Line 2
   Line 3
         Line 4
*/
/*
   PGCODEPAGE gselcp( PGCODEPAGE pcodepage );
   Use of national code pages
*/
struct
   {
   GCPHEAD chp;
   GCP_RANGE cpr[9];
   }
 GCODE CP_8859_danish =
   {
   {9,  /* sizeof(CP_8859_danish.cpr)/sizeof(GCP_RANGE) */
   32}, /* Default character */
   {
   {0x00,0x5A, 0x00}, /* ASCII ' ' - 'Z'  */
   {0x5B,0x5B, 0xC6}, /* Danish letter AE */
   {0x5C,0x5C, 0xD8}, /* Danish letter OE */
   {0x5D,0x5D, 0xC5}, /* Danish letter AA */
   {0x5E,0x7A, 0x5E}, /* ASCII '^' - 'z'  */
   {0x7B,0x7B, 0xE6}, /* Danish letter ae */
   {0x7C,0x7C, 0xF8}, /* Danish letter oe */
   {0x7D,0x7D, 0xE5}, /* Danish letter aa */
   {0x7E,0x7F, 0x7E}
   }};

void gselcp_ex(void)
   {
   int i;
   ginit();

   /* Use a font with ASCII and national symbols */
   gselfont(&cp8859_9);

   /* Override default codepage in font with a
      national codepage for fail safe character conversion */
   gselcp( (PGCODEPAGE) (&CP_8859_danish) );

   /* Test all character values */
   for (i=0x0; i <= 0xff; i++)
      gputch(i);
   }

/*
   PGFONT gselfont( PGFONT pfont );
*/
void gselfont_ex(void)
   {
   ginit();
   gputs("\nUsing SYSFONT");
   gselfont(&rtmono8_8);
   gputs("\nUsing rtmono8_8");
   gselfont(&times9);
   gputs("\nUsing times9");
   gselfont(&times16);
   gputs("\nUsing times16");
   gselfont(&msfont);
   gputs("\nUsing msfont");
   }

/*
   SGUCHAR gselvp( SGUCHAR vp );
*/
void gselvp_ex(void)
   {
   ginit();
   gselvp(1);       /* Viewport 1 = 9x3 characters */
   gsetmode(GWORD_WRAP);
   gsetcvp(0,1,8,5);
   gselvp(2);       /* Viewport 2 = 14x7 characters*/
   gsetmode(GWORD_WRAP);
   gsetcvp(4,4,GDISPW/8,GDISPH/8);

   /* End of initialization. Now make some outputs */
   gselvp(0);    /* Default viewport = full screen */
   gputs("First message");

   gselvp(1);
   gputs("Message in viewport ");/* Strings output */
   gputch('1');                  /* Characer output */

   gselvp(2);
   gputs("This is a long message in"
         " a viewport using word"
         " wrapping");
   }

/*
   GCOLOR gsetcolorb(GCOLOR background_color)
*/
void gsetcolorb_ex(void)
   {
   GCOLOR oldcolor;
   ginit();
   gputs("Text with");
   oldcolor = gsetcolorb(G_RED);
   gputs(" RED ");
   gsetcolorb(oldcolor);
   gputs("background");
   }

/*
   GCOLOR gsetcolorf(GCOLOR foreground_color)
*/
void gsetcolorf_ex(void)
   {
   GCOLOR oldcolor;
   ginit();
   gputs("Text with");
   oldcolor = gsetcolorf(G_RED);
   gputs(" RED ");
   gsetcolorf(oldcolor);
   gputs("foreground");
   }

/*
   GCURSOR gsetcursor( GCURSOR type );
*/
void gsetcursor_ex(void)
   {
   ginit();
   /* Use SYSFONT */
   gsetcursor(GCURSIZE2 | GCURON );
   gputs("Cursor size 2");

   gsetcursor(GCURSIZE8 | GCURON | GCURBLOCK);
   gputs("\nCursor size 8");

   /* Use softfont */
   gselfont(&times9);
   gsetcursor(GCURSIZE2 | GCURON );
   gputs("\nCursor size 2");

   gsetcursor(GCURSIZE8 | GCURON | GCURBLOCK);
   gputs("\nCursor size 8");
   }
/*
   void gsetcpos( SGUCHAR xpos, SGUCHAR ypos );
*/
void gsetcpos_ex(void)
   {
   ginit();
   gsetcpos( 3, 3 );
   gputs("Line 1");
   gsetcpos( 3, 5 );
   gputs("Line 2");
   }
/*
   Line 1

   Line 2
*/
/*
   void gsetcvp( SGUCHAR xs, SGUCHAR ys, SGUCHAR xe, SGUCHAR ye );
*/
void gsetcvp_ex(void)
   {
   ginit();
   gsetcvp( 1, 1, 7, 7 );
   gputs("Test message in viewport");
   }

/*
   GMODE gsetmode( GMODE mode );
*/
void gsetmode_ex(void)
   {
   ginit();

   /* Select a soft font */
   gselfont(&times9);

   gsetmode( GINVERSE );
   gputs("White on black\n");

   gsetmode( GNORMAL );
   gputs("Black on white\n");
   }


/*
   gsetmode_ex2()  Clock view example.
   Create and update a clock counter in the lower right screen corner
   taking advantage of the autoalignment mode features.
   This example assumes that GS_ALIGN and GFUNC_VP is enabled in gdispcfg.h
*/
static void clock_vp_setup(void)
   {
   SGUCHAR oldvp;
   oldvp = gselvp(GNUMVP-1);  /* Using last viewport as clock vp */
   /* Use a font where all digits have the same size */
   gselfont(&msfont78);
   /* Place viewport in lower right corner */
   gsetvp(GDISPW-9*ggetfw(),GDISPH-ggetfh(),GDISPW-1,GDISPH-1);
   /* Assure right alignment and prevent wrapping */
   gsetmode(GNOSCROLL | GALIGN_RIGHT);
   gclrvp();
   gselvp(oldvp);      /* Restore old vp */
   }

static void clock_vp_write(SGUCHAR hour, SGUCHAR min, SGUCHAR sec)
   {
   char str[9];
   /* Do a fast clock string formatting using the time format 01:23:56 */
   str[0] = (hour / 10 ) + '0';
   str[1] = (hour % 10 ) + '0';
   str[2] = ':';
   str[3] = (min / 10 ) + '0';
   str[4] = (min % 10 ) + '0';
   str[5] = ':';
   str[6] = (sec / 10 ) + '0';
   str[7] = (sec % 10 ) + '0';
   str[8] = 0;

   /* As we use automatic right alignment (= automatic x position adjustment),
      and no viewport scroll (= y position is constant)
      and the text size is the same (= old text is overwritte completely),
      then just output the new text to the clock vp */

   #ifdef GFUNC_VP
   /* Used named viewport function */
   gputs_vp(GNUMVP-1, str); /* Write direct to clock vp */
   #else
   {
   /* The same functionality using non _vp functions */
   SGUCHAR oldvp;
   oldvp = gselvp(GNUMVP-1); /* Using last viewport as clock vp */
   gputs(str);               /* Write direct to clock vp */
   gselvp(oldvp);            /* Restore old vp */
   }
   #endif
   }

void gsetmode_ex2(void)
   {
   SGUINT clk;
   ginit();

   clock_vp_setup(); /* Init clock viewport */

   for (clk = 0; clk < 3000; clk+=5)
      clock_vp_write(clk/3600,(clk/60)%60,clk%60); /* Count clock */
   }


/*
   GMODE gsetmode( GMODE mode );
   Blanking of areas around a text by use of GVPCLRxxxx modes
   How to refresh / redraw viewport in non-buffered mode without
   text "blinking" by use of mode switches.
*/
void gsetmode_ex3(void)
   {
   ginit();
   gselfont(&times9); /* Select a softfont */
   grectangle(5,5,ggetvpw()-6,5+ggetfh()*3); /* make frame */
   gsetvp(6,6,ggetvpw()-7,4+ggetfh()*3); /* move inside frame */

   /* Fill background so we can see blanking operations */
   gfillvp(0,0,ggetvpw()-1,ggetvph()-1,0xaa55);

   gsetmode(GALIGN_HCENTER|GALIGN_VCENTER|GVPCLR_UP);
   gputs("GVPCLR_UP");

   gfillvp(0,0,ggetvpw()-1,ggetvph()-1,0xaa55);
   gsetmode(GALIGN_HCENTER|GALIGN_VCENTER|GVPCLR_DOWN);
   gputs("GVPCLR_DOWN");

   gfillvp(0,0,ggetvpw()-1,ggetvph()-1,0xaa55);
   gsetmode(GALIGN_HCENTER|GALIGN_VCENTER|GVPCLR_LEFT);
   gputs("GVPCLR_LEFT");

   gfillvp(0,0,ggetvpw()-1,ggetvph()-1,0xaa55);
   gsetmode(GALIGN_HCENTER|GALIGN_VCENTER|GVPCLR_RIGHT);
   gputs("GVPCLR_RIGHT");

   gfillvp(0,0,ggetvpw()-1,ggetvph()-1,0xaa55);
   gsetmode(GALIGN_HCENTER|GALIGN_VCENTER|GVPCLR);
   gputs("GVPCLR");
   }

/*
   Show effect of mode settings
     GTRANSPERANT
     GTRANSPERANT | GINVERSE
*/
void gsetmode_tr_part1(GMODE mode)
   {
   /* Select font and color  */
   gselfont(&times13);
   gsetcpos(0,0);
   #if (GDISPPIXW > 1)
   gsetcolorf(G_BLACK);
   gsetcolorb(G_BLUE);
   #else
   gsetcolorf(G_BLACK);
   gsetcolorb(G_WHITE);
   #endif

   /* Make some background pattern */
   gsetmode(GNORMAL);
   gfillvp(0,0,ggetvpw()-1, ggetvph()-1,0xaa55);
   #if (GDISPPIXW >= 4)
   gsetcolorf(G_RED);
   #endif

   /* mode is GNORMAL */
   gsetmode(mode);
   gputs("Text normal\n");

   /* Show GINVERSE effect */
   gsetmode(GINVERSE|mode);
   gputs("Text inverse\n");

   #if (GDISPPIXW >= 4)
   /* Now change background color also, just so we here
      can verify the transperancy effect, i.e. this color
      setting must be dont care
   */
   gsetcolorb(G_MAGENTA);
   #endif

   /* Show GTRANSPERANT effect */
   gsetmode(GTRANSPERANT|mode);
   gputs("Text transperant\n");

   /* Show GTRANSPERANT | GINVERSE effect */
   gsetmode(GTRANSPERANT | GINVERSE|mode);
   gputs("Inv.transperant");
   }

void gsetmode_tr_part2(void)
   {
   /* Show now to use the effect for color change */
   gsetcpos(0,0);
   #if (GDISPPIXW >= 4)
   gsetcolorf(G_GREEN);
   #endif
   /* Swap text foreground color, leave background color as is */
   gsetmode(GTRANSPERANT|GVPCLR_RIGHT);
   gputs("Text normal\n");
   /* Swap text background color, leave text color as is  */
   gsetmode(GTRANSPERANT | GINVERSE | GVPCLR_RIGHT);
   gputs("Text inverse");
   }

/*
   Show effect of GNORMAL, GINVERSE and GTRANSPERANT modes
   Show effect of GVPCLR_RIGHT mode
*/
void gsetmode_ex4(void)
   {
   ginit();
   gsetmode_tr_part1(GNORMAL);
   gsetmode_tr_part1(GVPCLR_RIGHT);
   gsetmode_tr_part2();
   }

/* Output a string with wrap rotation on the same line */
void gput_wrap(char *str)
   {
   char first;
   if (str == NULL)
      return;
   gsetupdate(GUPDATE_OFF);

   first = 1;
   while (*str != 0)
      {
      if ((GXT)(ggetvpw()-ggetfw()) < ggetxpos())
         gputs("\r"); /* Not room for one character, restart on line */
      if (first)
         {
         /* First output is made as a single char string with auto clear.
            This both clear the viwport line, and assure correct
            character position for next operation */
         char tmpstr[2];
         gsetmode(GNOSCROLL|GPARTIAL_CHAR|GVPCLR_LEFT|GVPCLR_RIGHT);
         tmpstr[0] = *str;
         tmpstr[1] = 0;
         gputs(tmpstr);
         gsetmode(GNOSCROLL|GPARTIAL_CHAR);
         /* Now line is ready for wrapping text */
         first = 0;
         }
      else
         gputch(*str);
      str++;
      }
   gsetupdate(GUPDATE_ON);
   }

void gsetmode_ex5(void)
   {
   int i;
   ginit();

   /* Create a one line viewport at screen bottom */
   gsetvp(0,GDISPH-ggetfh(),GDISPW-1,GDISPH-1);

   /* Output rotating text */
   for (i=0; i < 100; i++)
      {
      /* Set new "rolling" position */
      gsetpos((GXT)((i*ggetfw())%ggetvpw()),(GYT)(ggetfh()-1));
      gput_wrap("** Wrapping text **");
      }
   }


/*
   gsetmode_ex6() start
   Use dedicated viewport to preserve mode settings across functions.
   Here with a simple menu area which is initialized, decorated, and
   used for output of dynamic data
*/

/* Init menu area. Set start position, max pixel width,
   max number of lines (using the default font) */
static void area_init( SGUCHAR vp, GXT xb, GYT yb, GXT maxwidth, GYT lines)
   {
   SGUCHAR oldvp = gselvp(vp); // Select viewport number
   /* Set area boundaries */
   gresetvp();
   gsetvp(xb,yb,xb+maxwidth-1,yb+lines*ggetfh()-1);
   /* Clear area */
   gclrvp();
   /* Enable auto clear of a text line to the left and right
      when using gputs(). Also prevent auto wrapping and scroll */
   gsetmode(GVPCLR_LEFT|GVPCLR_RIGHT|GLINECUT|GNOSCROLL);
   gselvp(oldvp); /* restore viewport number */
   }

/* Add a decoration frame around a viewport
   while preserving viewport positions */
static void set_vp_frame( SGUCHAR vp )
   {
   GXT xb; GYT yb; GXT xe; GYT ye;
   SGUCHAR oldvp = gselvp(vp); /* Select viewport (= menu area) */
   /* Get current boundary for vp area */
   ggetvp(&xb,&yb,&xe,&ye);
   /* Expand area to include frame space (1 pixel) */
   gsetvp(xb-1,yb-1,xe+1,ye+1);
   /* Draw frame along edges of viewport */
   grectangle(0,0,ggetvpw()-1, ggetvph()-1);
   /* Restore vieport to inner area (protecting frame) */
   gsetvp(xb,yb,xe,ye);
   gselvp(oldvp); /* restore viewport number */
   }

/* Output text on line. First line has index 0 */
static void put_text_line(SGUCHAR vp, char *str, SGUCHAR lineidx)
   {
   SGUCHAR oldvp = gselvp(vp); /* Select viewport (= menu area) */
   if ((lineidx+1) <= ggetvph()/ggetfh())
      {
      /* Position on start of line in (menu) area */
      gsetpos(0,(ggetfh()*(lineidx+1))-1);
      /* Flicker free update of just one text line using auto clear
         of line to viewport boundaries */
      gputs(str);
      }
   gselvp(oldvp); /* restore viewport number */
   }

void gsetmode_ex6(void)
   {
   SGUCHAR menuvp;
   ginit();

   menuvp = 2; /* Use a dedicated viewport for menu */

   /* Init a (menu) area using a separate viewport.
      Here three 75 pixels wide lines */
   area_init( menuvp, 10, 20, 75, 3);
   set_vp_frame(menuvp);

   /* First update of area using a dedicated viewport (2) */
   put_text_line(menuvp, "First Line", 0);
   put_text_line(menuvp, "Next Line", 1);
   put_text_line(menuvp, "Another Line", 2);

   /* We can still directly use text outside area (default viewport)*/
   gputs("Menu drawn");

   /* Now update random lines using auto clear */
   put_text_line(menuvp, "123456", 2);
   put_text_line(menuvp, "New txt", 1);
   put_text_line(menuvp, "789.6", 2);
   }

/*
   void gsetpixel( GXT xs, GYT ys, SGBOOL pixel );
*/
#include <math.h>
#define PIVALUE 3.14159F

void gsetpixel_ex(void)
   {
   float f;
   GXT x;
   GYT y;
   ginit();

   /* Draw a dotted center line */
   gsetcolorf(G_LIGHTGREY);
   gfillvp(0,ggetvph()/2-1,ggetvpw()-1,ggetvph()/2-1,0xaaaa);
   gsetcolorf(G_BLUE);

   /* Pixel draw of a sine period */
   for (x=0; x < ggetvpw(); x++)
      {
      f = PIVALUE * 2.0F;
      f *= x;
      f /= ggetvpw();
      y = ggetvph()/2 - (GYT) (sin(f) * ggetvph()/2);
      gsetpixel( x, y, 1);
      }
   }
/*
   void gsetpos( GXT xpos, GYT ypos );
*/

void underline_str(char *str)
   {
   GXT x;
   GYT y;
   if (str == NULL)
      return;

   /* Make undeline */
   x = ggetxpos();  /* Get graphic cursor */
   y = ggetypos();

   /* Output string */
   gputs(str);

   /* Graphic position has been updated so just draw underline */
   glineto(x,y);
   }

void gsetpos_ex(void)
   {
   ginit();
   gselfont(&times9);  /* Select a soft font */

   /* Set both graphic and text cursor */
   gsetpos(15,15);
   underline_str("Underline");
   gputs(" text");
   }

/*
   void gsettab( GXT s );
*/
void gsettab_ex(void)
   {
   ginit();
   /* Split window in two columns */
   gsettab(ggetvpw()/2);
   gputs("Tab\tTab");
   }
/* Output
Tab     Tab
*/
/*
   void gsettabs( GXT s );
*/
void gsettabs_ex(void)
   {
   ginit();
   gsettabs(3*GDISPCW); /* Set tabulator spacing
                           = 3 character widths */
   gputs("1\tTab\n2\tTest");
   }

/* Output
1  Tab
2  Test
*/

/*
   void ghw_setupdate( SGUCHAR update );
   The gsetupdate() example only works if GBUFFER is defined
*/
void gsetupdate_ex(void)
   {
   ginit();
   gselfont(&msfont78);         /* Use a softfont */

   gputs(  "Line 1");           /* Shown at once */
   gsetupdate(GUPDATE_OFF);     /* Activate delayed update */
   gputs("\nLine 2");           /* Only write to the buffer */
   gputs("\nLine 3");           /* Only write to the buffer */
   gputs("\nLine 4");           /* Only write to the buffer */
   gsetupdate(GUPDATE_ON);      /* Deactivate delayed update. */
                                /* Flush changes to the LCD display */
   }

/*
   void gsetupvp(SGUCHAR vp, GXT xt, GYT yt, GXT xb, GYT b,
                 PGFONT f, PGCODEPAGE cp, SGUCHAR mode);
*/
void gsetupvp_ex(void)
   {
   ginit();
   grectangle(1,1,62,62);   /* Make a frame */
   gsetupvp(1,3,3,60,60,&times9,NULL,GNORMAL);
   gputs("This is a message in a box");
   }
/*
   void gsetvp( GXT xs, GYT ys, GXT xe, GYT ye );
*/
void gsetvp_ex(void)
   {
   ginit();

   gselfont(&times9);
   /* Resize viewport */
   gsetvp(10,10,55,55);

   /* Rectangle along edges of viewport */
   grectangle(0,0,ggetvpw()-1,ggetvph()-1);
   /* Resize viewport to be inside rectangle */
   gsetvp(12,12,53,53);
   gputs("This is a message in a box");
   }

/*
   SGUCHAR gstrlines( PGSTR str )
*/
void gstrlines_ex(void)
   {
   GYT h;
   static const char str1[] = {"Header text"};
   static const char str2[] = {"Line 1\nLine 2\nLine 3"};

   ginit();
   /* Calculate needed hight */
   h = gfgetfh(&times13) * gstrlines(str1) +
       gfgetfh(&times9)  * gstrlines(str2);

   /* Make framed viewport */
   grectangle(0,0,GDISPW-1, h+4);

   gselfont(&times13);     /* Select font before gsetvp so start cursor is adjusted  */
   /* Resize viewport to be inside rectangle */
   gsetvp(2,2, GDISPW-3, h+2);

   /* Put text in frame */
   gputs( str1 );
   gselfont(&times9);
   gputs( "\n" );
   gputs( str2 );
   }

/*
   SGUINT gsymsize( GXT xs, GYT ys, GXT xe, GYT ye );
*/

void gsymsize_ex(void)
   {
   /* Example only works if the display screen data can be read */
   #if (!defined( GHW_NO_LCD_READ_SUPPORT ) || defined(GBUFFER))

   GSYMBOL *ptr;
   unsigned int size;
   ginit();

   /* Calculate needed buffer size */
   size = gsymsize(5,5,15,20);

   if ((ptr = (GSYMBOL *) malloc(size)) != NULL)
      {
      int i;
      /* Make figure of 5 boxes above each other */
      grectangle(5,5,15,20);
      ggetsym(5,5,15,20,ptr,size);
      for (i=5; i < 15; i += 2)
         gputsym(i,i, ptr);
      free((void*)ptr);
      }

   #endif
   }



/*
  Dump default characters
*/
void showsyschars(void)
   {
   int i;
   ginit();  /* Default is now sysfont */
   for (i = 32; i < 128; i++)
      gputch(i);
   gscrollcvp(); /* Scroll, leave cursor unchanged */
   }

/*
  Dump default characters in hardware font
*/
void showsyschars2(void)
   {
   #ifndef GHW_NO_HDW_FONT
   int i;
   char str[10];
   ginit();  /* Default is now sysfont */
   for (i = 32; i < 256; i++)
      {
      if ((i % 16) == 0)
         {
         sprintf(str,"\n0x%02x ", i);
         gputs(str);
         }
      gputch(i);
      }
   #endif
   }
/*
  Demonstrate use of cursor block mode
*/
void gcursor_block_ex(void)
   {
   GXT x;
   GYT y;
   ginit();

   /* Select a soft font and enable cursor */
   gselfont(&msfont78);
   gsetcursor( GCURSIZE2 | GCURON | GCURBLOCK );
   gputs("Cursor Block");
   x = ggetcxpos();
   y = ggetcypos();
   do
      {
      gsetcpos((SGUCHAR) x, (SGUCHAR) y);  /* Move cursor backward */
      }
   while (x-- != 0);

   gputs("\n0123456789");

   y = ggetcypos();
   x = 0;
   do
      {
      gsetcpos((SGUCHAR)x,(SGUCHAR)y);  /* Move cursor forward */
      }
   while (x++ < 10);
   }

/*
   Fill an area in a box with a named color
*/
static void colorbox(GXT xb, GYT yb, GXT xe, GYT ye, GCOLOR col)
   {
   gsetcolorf(G_BLACK);
   grectangle(xb,yb,xe,ye);
   gsetcolorb(col);
   gfillvp(xb+1,yb+1,xe-1,ye-1,0);
   }

/*
   Palette color test.
   Shows all palette colors or grey-scale levels on the actual display.
   Usefull for adjusting a palette color setting for a specific display
   (This function is only for LCD controllers configured for max color
   values <= 256)
*/
void show_palette(void)
   {
   #if (GDISPPIXW <= 8)
   static const char wcnttabel[] = {2,2,4,4,6,8,10,16};
   GXT x,w,wcnt;
   GYT y,h,hcnt;
   GCOLOR col;
   ginit();

   /* Arrange color boxes in x and y direction depending on
      the color resolution selected in GDISPCFG.H */
   wcnt = wcnttabel[GDISPPIXW-1];
   hcnt = (1<<GDISPPIXW)/wcnt;

   w = GDISPW / (wcnt + 2);
   h = GDISPH / (hcnt + 2);
   if (h >= ggetfh())
      { /* Room for a top text. Center text on display */
      gsetmode(GALIGN_HCENTER);
      gputs("Palette colors");
      }
   for (y = 1, col = 0; y <= hcnt; y++)
      for (x = 1; x <= wcnt; x++)
          colorbox(x*w, y*h, (x+1)*w-2, (y+1)*h-2, col++);
   #endif
   }


/*
   Display colors assigned to standard color names in GDISPCFG.H
   Useful when adjusting either the color assignements or the color
   palette for a specific display module / LCD controller configuration.

   For passive STN displays the LCD color is strongly influenced by the LCD
   contrast voltage. The contrast should be adjusted so the G_GREY value
   looks right.
*/
void show_named_colors(void)
   {
   static const GCOLOR color[14] = {
      G_RED,G_ORANGE,G_YELLOW,G_GREEN,G_CYAN,G_BLUE,G_MAGENTA,
      G_WHITE,G_LLIGHTGREY,G_LIGHTGREY,G_GREY,G_DARKGREY,G_DDARKGREY,G_BLACK
      };
   GXT x,w,wcnt;
   GYT y,h,hcnt;
   int col;

   ginit();

   wcnt = 7;
   hcnt = 2;
   w = GDISPW / (wcnt + 2);
   h = GDISPH / (hcnt + 2);
   if (h >= ggetfh())
      { /* Room for a top and bottom text. Center text on display width */
      if (h > gfgetfh( &ariel9))
         gselfont(&ariel9);

      gsetmode(GALIGN_HCENTER|GLINECUT);
      gputs("Show Named Colors");
          /* Bottom text */
          gsetpos(0,GDISPH-1); /* Bottom pixel line */
          gselfont(&SYSFONT);  /* Use default (mono-spaced) font */
          gputs("Bottom Text");
      }

   for (y = 1, col = 0; y <= hcnt; y++)
      for (x = 1; x <= wcnt; x++)
          /* colorbox() is located above in this module */
          colorbox(x*w, y*h, (x+1)*w-2, (y+1)*h-2, color[col++]);
   }

/*
   Fill a rectangular area with a specified color,
   Leave the default color setting unchanged.
*/
void fillbox(GXT xb, GYT yb, GXT xe, GYT ye, GCOLOR color)
   {
   GCOLOR c = gsetcolorf(color);
   gfillvp(xb,yb,xe,ye, 0xffff);
   gsetcolorf(c);
   }

/*
   Test the color resolution capabilities of the display / LCD controller
   which can be configured for RGB mode.
   Grey, Red, Green, Blue intensity level bars are displayed.
   The picture is useful when adjusting the contrast voltage to the
   LCD controller at to set the grey-scale (light intensity) levels.
*/
static void show_rgb_color(void)
   {
   #ifdef GHW_USING_RGB
   GXT x,w,i,max;
   GYT y,ys,h;
   GPALETTE_RGB palette;
   SGUCHAR bc;

   ginit();

   gsetupdate(GUPDATE_OFF);

   max = 32;                 /* Color intervals */
   w = GDISPW/max;           /* Color width adaption */
   x = (GDISPW-w*max)/2;     /* Start position */
   gsetmode(GALIGN_HCENTER|GWORD_WRAP);
   gputs("Show Intensity levels");
   ys = ggetypos()+5;    /* Y position for text end (incl any wrapping) */
   h = (GDISPH-ys)/5;  /* Split rest of screen in 6 areas */

   for(i = 1; i <= max; i++, x+=w)
      {
      y = ys;
      gcsetpixel(x, y-2, G_BLACK); /* Mark color shifts */

      bc = (i*256)/max -1; /* color bit value */
      /* Grey bar */
      palette.r = bc;
      palette.g = bc;
      palette.b = bc;
      fillbox(x,y,x+w-1,y+h,ghw_rgb_to_color(&palette));

      y += h;
      /* red bar */
      palette.r = bc;
      palette.g = 0;
      palette.b = 0;
      fillbox(x,y,x+w-1,y+h,ghw_rgb_to_color(&palette));
      y += h;
      /* green bar */
      palette.r = 0;
      palette.g = bc;
      palette.b = 0;
      fillbox(x,y,x+w-1,y+h,ghw_rgb_to_color(&palette));
      y += h;
      /* blue bar */
      palette.r = 0;
      palette.g = 0;
      palette.b = bc;
      fillbox(x,y,x+w-1,y+h,ghw_rgb_to_color(&palette));
      }
   gsetupdate(GUPDATE_ON);
   #endif
   }

/*
   Named viewport example (GFUNC_VP must be defined)

   Example use 3 viewports and demonstrates use of auto-sizing,
   auto-alignment and use of relative coordinates.
   I.e. the software design method makes the source code
   adaptive to the screen size (faster design and minimum
   future maintenance)

   If you for instance selects other font for TOPFONT
   then the design is automatically adjusted accordingly
*/
#define TOPFONT  &ariel18
#define TEXTFONT &ariel9

void vptst_ex(void)
   {
   #ifdef GFUNC_VP
   ginit();  // Init controller and library

   // Define layout using three viewports for text output

   // Make centeret top text for viewport 0
   gsetupvp(0,0,0,GDISPW-1,gfgetfh(TOPFONT), TOPFONT, NULL, GALIGN_HCENTER);
   // Make viewport below top text, to the left of center
   // Use right aligned text and auto clear during update
   gsetupvp(1,0,gfgetfh(TOPFONT)+2,GDISPW/2-1,GDISPH-1,
      TEXTFONT, NULL, GALIGN_LEFT | GALIGN_TOP | GVPCLR);
   // Make viewport below top text, to the right of center
   // Use right aligned text and auto clear during update
   gsetupvp(2,GDISPW/2,gfgetfh(TOPFONT)+2,GDISPW-1,GDISPH-1,
      TEXTFONT, NULL, GALIGN_RIGHT | GALIGN_TOP |GVPCLR);

   // Layout is created.
   // Now just use the viewports

   gputs_vp(0,"Top text");  // Top text
   // 3 line text for left and right viewport
   gputs_vp(1,"Data text\nLine2 text\nLine2 text");
   gputs_vp(2,"12345\n33333\n4545");
   // New data in viewport (no need to clear area)
   gputs_vp(2,"123.5\n333.3\n8901");

   #endif /* GFUNC_VP*/
   }

/************************************************************

  The rest of the example functions are only relevant if the C
  compiler supports the C wide-char types, i.e. if the C compiler
  is conforming to the C standard ISO/IEC 1998:99 or later revisions.

  GWIDECHAR must be defined in the gdispcfg.h file.

************************************************************/
#ifdef GWIDECHAR
/*
   SGUINT gpstrlen( PGSTR str )
*/
void gpstrlenw_ex(void)
   {
   unsigned int size;
   wchar_t str1[] = L"Pixel size of message is: ";
   wchar_t str2[80];

   ginit();
   gselfont(&times9);

   size = gpstrlenw(str1);
   swprintf(str2,sizeof(str2)/sizeof(wchar_t),L"%s %u",str1, size);
   gputsw(str2);
   }

/*
   SGUCHAR gstrlinesw( PGSTR str )
*/
void gstrlinesw_ex(void)
   {
   GYT h;
   static const wchar_t str1[] = {L"Header text"};
   static const wchar_t str2[] = {L"Line 1\nLine 2\nLine 3"};

   ginit();
   /* Calculate needed hight */
   h = gfgetfh(&times13) * gstrlinesw(str1) +
       gfgetfh(&times9)  * gstrlinesw(str2);

   /* Make framed viewport */
   grectangle(0,0,GDISPW-1, h+4);


   gselfont(&times13);     /* Select font before gsetvp so start cursor is adjusted  */
   gsetvp(2,2, GDISPW-3, h+2);

   /* Put text in frame */
   gputsw( str1 );
   gselfont(&times9);
   gputsw( L"\n" );
   gputsw( str2 );
   }


/*
   GYT gpstrheightw( PGSTR str )
*/
void gpstrheightw_ex(void)
   {
   GYT h;
   static const wchar_t str[] = {L"Line 1\nLine 2\nLine 3"};
   ginit();

   /* Select current font */
   gselfont(&times9);

   /* string height in pixels */
   h = (GYT) gpstrheightw(str);

   /* Make framed viewport */
   grectangle(2,2,GDISPW-3, h+6);
   gsetvp(4,4, GDISPW-5, h+4);

   /* Put text in frame */
   gputsw( str );
   }

/*
   GXT gpstrwidthw( PGSTR str )
*/
void gpstrwidthw_ex(void)
   {
   GYT h;
   GXT w;
   static const wchar_t str[] = {L"ABC\nDEFG\nHIJK"};
   ginit();

   /* Select current font */
   gselfont(&times9);

   /* string height & width */
   h = (GYT) gpstrheightw(str);
   w = (GXT) gpstrwidthw(str);

   /* Fit viewport */
   grectangle(2,2,w+2+4, h+2+4);
   gsetvp(4,4, w+4, h+4);

   /* Put text in viewport */
   gputsw( str );
   }

/*
   void gputchw( SGUCHAR val );
*/
void gputchw_ex(void)
   {
   wchar_t i;
   ginit();

   /* Dump ASCII characters */
   for (i=0x20; i < 128; i++)
      gputchw(i);

   }

/*
   void gputsw( char *str );
*/
#include <stdio.h>
void gputsw_ex(void)
   {
   unsigned int i;
   wchar_t str[20];
   ginit();

   gputsw(L"gputs handles\n \\n and \\r: \n");
   for (i=1; i < 30; i++)
      {
      swprintf(str,sizeof(str)/sizeof(wchar_t),L"\r Count : %02u", i);
      gputsw(str);
      }
   }

/*
   Show effect of gsetmode(..) alignement parameters
   on gputsw(..) output.
*/
void gputsw_allign(void)
   {
   gclrvp();
   /* Make viewport inside a frame */
   grectangle(ggetfw()-1,ggetfh()-1,ggetfw()*11+1,ggetfh()*5+1);
   gsetvp(ggetfw(),ggetfh(),ggetfw()*11,ggetfh()*5);

   /* Cut text at right viewport edge */
   gsetmode( GLINECUT );
   gputsw(L"2 lines to be cut\nat the viewport edge");

   /* Text is right aligned in viewport */
   gclrvp();
   gsetmode( GALIGN_RIGHT );
   gputsw(L"Right 1\nRight 2\n");
   gputsw(L"Test line wrapping");

   /* Text is centered vertically in viewport */
   gclrvp();
   gsetmode( GALIGN_VCENTER);
   gputsw(L"Vertical\nCenter");

   /* Text is centered vertically in viewport at right edge */
   gclrvp();
   gsetmode( GALIGN_VCENTER | GALIGN_RIGHT);
   gputsw(L"Right +\nVertical");

   /* Text is centered horizontally in viewport */
   gclrvp();
   gsetmode( GALIGN_HCENTER);
   gputsw(L"Center\nhorizontal");

   /* Text is centered vertically and horizontally in viewport */
   gclrvp();
   gsetmode( GALIGN_VCENTER | GALIGN_HCENTER);
   gputsw(L"Double\ncenter");
   }

void gputsw_ex2(void)
   {
   ginit();

   /* Use SYSFONT */
   gputsw_allign();

   /* Select a soft font */
   gresetvp();
   gselfont(&times9);
   gputs_allign();

   /* Check inverse mode + Allignment */
   gclrvp();
   gsetmode( GINVERSE | GALIGN_HCENTER );
   gputsw(L"White on black\n");
   gsetmode( GNORMAL | GALIGN_HCENTER );
   gputsw(L"Black on white");
   }

void gpwordlenw_ex(void)
   {
   unsigned int size;
   wchar_t str1[] = L"Lenght in pixel of the first word is: ";
   wchar_t str2[80];

   ginit();
   gselfont(&times16);

   size = gpwordlenw(str1);
   swprintf(str2,sizeof(str2)/sizeof(wchar_t),L"%s %u",str1, size);
   gputsw(str2);
   }

#endif /* GWIDECHAR */

#ifdef GMULTIBYTE_UTF8
/*
  Show multi byte functions.
  Convertion beween wide char strings (UTF-16) and multibyte strings (UTF-8)
    PGWSTR gmbstrcpyw( PGWSTR dstr, PGCSTR sstr );
    PGSTR gwstrcpymb( PGSTR dstr, PGCWSTR sstr );
    GWCHAR ggetmbc(PGCSTR *strp);
    SGUINT gmstrlenw( PGCWSTR str );
*/
void gwstrcpymb_ex(void)
   {
   /* String with Greek A,B,C,D,E in unicode */

   /* This constant initialization of a widechar string require full wchar_t support in the compiler */
   /*  static const GWCHAR wstr1[] = L"\x03b1\x03b2\x03b3\x03b4\x03b5\n"; */

   /* This constant initialization of a widechar string can be used with any compiler */
   static const GWCHAR wstr1[] = {0x03b1,0x03b2,0x03b3,0x03b4,0x03b5,'\n',0};

   char str[80];
   GWCHAR *pw;
   char *pc;
   char *p;

   ginit();
   gselfont(&narrow10);
   gputs("\nShow original widechar string:");
   gselfont(&uni_16x16);
   gputs("\n");
   gputsw(wstr1);    /* Show original wide char string */

   /* Show required storage length */
   gselfont(&narrow10);
   sprintf(str,"Required multibyte storage length %u", (unsigned int) gmstrlenw( wstr1 ));
   gputs(str);

   /* Allocate a temp buffer for multibyte string */
   if ((pc = (char *) malloc((gmstrlenw(wstr1)+1)*sizeof(char))) != NULL)
      {
      gputs("\nConvert to multi-byte string:");
      gselfont(&uni_16x16);
      gputs("\n");
      gwstrcpymb(pc, wstr1); /* Copy and encode to UTF-8 multibyte */
      gputs(pc);             /* Use UTF-8 decoder */

      gselfont(&narrow10);
      sprintf(str,"Dump mb string as %u hex bytes:\n ", (unsigned int) gmstrlen(pc));
      gputs(str);

      p = pc;
      while (*p != 0)
         {
         sprintf(str,"%02x ", ((unsigned int) (*p)) & 0xff);
         gputs(str);
         p++;
         }

      sprintf(str,"\nNumber of logic symbols in mb string: %u", (unsigned int) gmsstrlen(pc));
      gputs(str);

      gputs("\nDump mb string as decoded 16-bit values:\n ");
      p = pc;
      while (*p != 0)
         {
         sprintf(str,"%04x ", (unsigned int) ggetmbc((PGCSTR *)&p) & 0xffff);
         gputs(str);
         }

      /* Allocate a temp buffer for widechar string */
      if ((pw = (GWCHAR*) malloc((gmsstrlen(pc)+1)*sizeof(GWCHAR))) != NULL)
         {
         gputs("\nConvert multibyte string to widechar string:");
         gmbstrcpyw(pw,pc); /* Copy and convert to wide char */
         gselfont(&uni_16x16);
         gputs("\n");
         gputsw(pw);
         free(pw);
         }

      free(pc);
      }
   }
#endif

#if (defined( GHW_LARGE_VIDEOBUF ) && defined(GDISP_PW) && defined(GDISP_PH))
/*
  Only available if the display controller supports use of a video memory
  larger than the physical screen.
*/
/*
   void ghw_set_screenpos(GXT x, GYT y);

   This example assumes that the GDISPW,GDISPH settings in gdispcfg.h is
   configured so the video memory height (the logical height) is twice
   the physical screen height.
*/
void ghw_set_screenpos_ex(void)
   {
   ginit();
   /* Set viewport for upper screen.*/
   ghw_set_screenpos(0,0);    /* Show upper half of screen (default after init) */
   gsetvp(0,0,GDISPW-1,GDISPH/2-1);
   /* Draw something in viewport for upper screen (instant view) */
   gputs("Shown at once");

   /* Set viewport for lower screen. */
   gsetvp(0,GDISPH/2,GDISPW-1,GDISPH-1);
   /* Draw something in viewport for lower screen (delayed view) */
   gputs("Shown delayed");
   gputs("\nSome more text");
   ghw_set_screenpos(0,GDISPH/2); /* Swap display look to lower screen */
   }
#endif


int main(void)
   {
   /* Call all examples */
   #ifdef SGTCDEFH
   sginit();  /* Using STIMGATE Target Controller , Init target interface */
   #endif
   show_named_colors();
   gcarc_ex();
   gcircle_ex();
   gclrvp_ex();
   gclrtabs_ex();
   gcursorblink_ex();
   gfgetcp_ex();
   gfgetfh_ex();
   gfgetfw_ex();
   gfillfsym_ex();
   gfillsym_ex();
   gfillvp_ex();
   ggetcolorb_ex();
   ggetcolorf_ex();
   ggetcp_ex();
   ggetcxpos_ex();
   ggetcypos_ex();
   ggetfh_ex();
   ggetfont_ex();
   ggetfont_ex2();
   ggetfw_ex();
   ggetmbc_ex();
   ggetspch_ex();
   ggetspln_ex();
   ggetspln_ex2();
   ggetsym_ex();
   ggetsymw_ex();
   ggetsymw_ex2();
   ggetpixel_ex();
   ggetvp_ex();
   ggetvph_ex();
   ggetvpnum_ex();
   ggetvpw_ex();
   ggetxpos_ex();
   ggetxpos_ex2();
   ggetypos_ex();
   ghw_blksize_ex();
   ghw_cont_change_ex();
   ghw_cont_set_ex();
   ghw_dispoff_ex();
   ghw_dispon_ex();
   ghw_err_ex();
   ghw_loadsym_ex();
   ghw_puterr_ex();
   ghw_rdblk_ex();
   ghw_restoreblk_ex();
   ghw_restoreblk_ex2();
   ghw_wrblk_ex();
   ghw_wrblk_ex2();
   ghw_wrblk_ex3();
   ginit_ex();
   ginvertvp_ex();
   ginvertvp2_ex();
   ginvertvp3_ex();
   glineto_ex();
   gmoveto_ex();
   gpstrheight_ex();
   gpstrlen_ex();
   gpstrwidth_ex();
   gputch_ex();
   gputchrot_ex();
   gputcsym_ex();
   gputfsym_ex();
   gputs_ex();
   gputs_ex2();
   gputsrot_ex();
   gputsym_ex();
   gputsym2_ex();
   gputsym3_ex();
   gputsymrot_ex();
   gpwordlen_ex();
   grectangle_ex();
   gresetvp_ex();
   groundrect_ex();
   groundrect_ex2();
   gscinit_ex();
   gscrestore_ex();
   gscrollcvp_ex();
   gscrollcvp2_ex();
   gscsave_ex();
   gscsize_ex();
   gselcp_ex();
   gselfont_ex();
   gselvp_ex();
   gsetcolorb_ex();
   gsetcolorf_ex();
   gsetcursor_ex();
   gsetcpos_ex();
   gsetcvp_ex();
   gsetmode_ex();
   gsetmode_ex2();
   gsetmode_ex3();
   gsetmode_ex4();
   gsetmode_ex5();
   gsetmode_ex6();
   gsetpixel_ex();
   gsetpos_ex();
   gsettab_ex();
   gsettabs_ex();
   gsetupdate_ex();
   gsetupvp_ex();
   gsetvp_ex();
   gstrlines_ex();
   gsymsize_ex();
   showsyschars();
   showsyschars2();
   gcursor_block_ex();
   show_palette();
   show_rgb_color();
   vptst_ex();

   G_WARNING("Test");  /* Test Warning message output */
   G_ERROR(  "Test");  /* Test Error message output */

   #ifdef GWIDECHAR
   /* These examples require wide-char support from the compiler */
   gstrlinesw_ex();
   gpstrheightw_ex();
   gpstrwidthw_ex();
   gputchw_ex();
   gputsw_ex();
   gputsw_ex2();
   gpwordlenw_ex();
   #ifdef GMULTIBYTE_UTF8
   gwstrcpymb_ex();
   #endif
   #endif /* GWIDECHAR */

   #if (defined( GHW_LARGE_VIDEOBUF ) && defined(GDISP_PW) && defined(GDISP_PH))
   ghw_set_screenpos_ex();
   #endif /* GHW_LARGE_VIDEOBUF */

   gexit();   /* Last LCD library operation before exiting program */

   #ifdef SGTCDEFH
   sgexit();  /* Using STIMGATE tool, close target interface */
   #endif
   return 0;
   }





