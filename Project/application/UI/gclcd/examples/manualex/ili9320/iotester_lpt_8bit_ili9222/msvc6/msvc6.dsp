# Microsoft Developer Studio Project File - Name="MSVC6" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=MSVC6 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "msvc6.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "msvc6.mak" CFG="MSVC6 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "MSVC6 - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "MSVC6 - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "MSVC6 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /Zp1 /W3 /GX /O2 /I "..\..\..\..\..\INC\\" /I "..\..\..\..\..\FONTS\\" /I "..\\" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "SGPCMODE" /FD /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x406 /d "NDEBUG"
# ADD RSC /l 0x406 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib w32sock.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "MSVC6 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /Zp1 /W3 /Gm /GX /ZI /Od /I "..\..\..\..\..\INC\\" /I "..\..\..\..\..\FONTS\\" /I "..\\" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "SGPCMODE" /D "IOTESTER" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x406 /d "_DEBUG"
# ADD RSC /l 0x406 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib wsock32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "MSVC6 - Win32 Release"
# Name "MSVC6 - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\ghwioini.c
# End Source File
# Begin Source File

SOURCE=..\..\..\manualex.c
# End Source File
# End Group
# Begin Group "controller"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwblkrw.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwbuf.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwcolcv.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwfill.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwgscrl.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\ili9320\ghwinit.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwinv.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwpixel.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwplrgb.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwretgl.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwsymrd.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\s6d0129\ghwsymwr.c
# End Source File
# End Group
# Begin Group "iotester"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\iotester.c
# End Source File
# End Group
# Begin Group "common"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\..\..\common\gcarc.c
# End Source File 
# Begin Source File

SOURCE=..\..\..\..\..\common\groundrec.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gicarc.c
# End Source File 
# Begin Source File

SOURCE=..\..\..\..\..\common\gputsrot.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gputchrot.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gputsymrot.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gsympixrd.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gcgetbak.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gcgetfor.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gchlnsp.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gcpsel.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gcsetbak.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gcsetfor.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfcursor.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfgetcxp.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfgetcyp.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfgeth.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfgetw.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfputch.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfsel.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfsetcp.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfsetp.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfsymw.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gftabs.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\ggcircle.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\ggetfsym.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\ggetmbc.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\ggetmode.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gggetxyp.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\ggline.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\ggpixel.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\ggrect.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\giputsym.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gmbcpyw.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gmstrcpy.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gmstrlen.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gpstrh.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gscreen.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gsetcpy.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gslen.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gsputs.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gstrln.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gsymcput.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gputfsym.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gfillfsym.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gsymfill.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gsymget.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gsymput.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpapp.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpclr.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpcset.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpfill.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpget.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpinit.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpinv.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpmode.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpreset.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpscrol.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpsel.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpset.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpvph.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpvpw.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpxl.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpxr.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpyb.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\common\gvpyt.c
# End Source File
# End Group
# Begin Group "fonts"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\..\..\fonts\ariel18.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\ariel9.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\cp885914.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\cp8859_9.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\mono5_8.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\mono8_8.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\ms58p.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\msfont58.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\msfont78.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\narrow10.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\narrow20.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\times13.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\times16.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\times9.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\fonts\uni_16x16.c
# End Source File
# End Group
# End Target
# End Project
