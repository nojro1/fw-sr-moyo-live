/************************* GHWIOINI.C ******************************

   Inittialize and deinitialize target specific I/O resources.

   When should it be used:
   When the IOTESTER tool is used this file should replaces the
   GHWIOINI.C file provided with the LCD/OLED driver libraries.
   This GHWIOINI.C file is prepared for the IOTESTER tool for use
   with LCD/OLED displays tested from a PC program running on the PC.
   Note the compiler switch IOTESTER must be defined in the compilers
   project preprocessor settings.

   Specific IOTESTER operations:
   The IOTESTER device is located on an LPT port and initialized
   for external bus emulation. Then the LCD reset line is activated.
   Hereafter the LCD display is ready of read and write access.

   Revision date:     30-09-05
   Revision Purpose:  Extended address bus and word mode setup added.

   Version number: 1.10
   Copyright (c) RAMTEX Engineering Aps 2004-2005

*********************************************************************/
#ifndef GHW_NOHDW
//#include <sgio.h>      /* Portable I/O functions + hardware port def */
#endif
#include <gdisphw.h>

#ifdef IOTESTER
#include <iotester.h>
#endif
#ifdef GHW_SINGLE_CHIP
void sim_reset( void );
#endif


#ifdef GHW_PCSIM
#include "windows.h" /* Sleep function */
#endif

#ifdef GBASIC_INIT_ERR
/*
   ghw_io_init()

   This function is called once by ginit() via ghw_init() before any LCD
   controller registers is addressed. Any target system specific
   initialization like I/O port initialization can be placed here.

*/
void ghw_io_init(void)
   {
   #ifndef GHW_NOHDW
   #ifdef IOTESTER
   if (!iot_init(IOT_LPTSEEK))
      {
      /* Select bus mode */
      /* I/O mode (single chip simulation mode) */
//    iot_wr(IOT_MODE_REG, IOT_MODE_IO);         // 30 port pins

      /* 8 bit bus modes */
//    iot_wr(IOT_MODE_REG, IOT_MODE_RDWR_ADR20); // 20 adresses, RD/WR
//    iot_wr(IOT_MODE_REG, IOT_MODE_RDWR_ADR18); // 18 adresses, RD/WR
//    iot_wr(IOT_MODE_REG, IOT_MODE_RDWR_ADR16); // 16 adresses, RD/WR + 2 I/O
//    iot_wr(IOT_MODE_REG, IOT_MODE_RDWR_ADR8);  //  8 adresses, RD/WR + 10 I/O
//    iot_wr(IOT_MODE_REG, IOT_MODE_RDWR_ADR2);  //  2 adresses, RD/WR + 16 I/O

//    iot_wr(IOT_MODE_REG, IOT_MODE_E_RW_ADR20); // 20 adresses, Eclk + R/W
//    iot_wr(IOT_MODE_REG, IOT_MODE_E_RW_ADR18); // 18 adresses, Eclk + R/W
//    iot_wr(IOT_MODE_REG, IOT_MODE_E_RW_ADR16); // 16 adresses, Eclk + R/W
//    iot_wr(IOT_MODE_REG, IOT_MODE_E_RW_ADR8);  //  8 adresses, Eclk + R/W
//    iot_wr(IOT_MODE_REG, IOT_MODE_E_RW_ADR2);  //  2 adresses, Eclk + R/W

      /* 16 bit bus modes */
//    iot_wr(IOT_MODE_REG, IOT_MODE_RDWR_ADR12_D16);// 12 adresses, RD/WR, 16 bit databus
//    iot_wr(IOT_MODE_REG, IOT_MODE_RDWR_ADR10_D16);// 10 adresses, RD/WR, 16 bit databus
      iot_wr(IOT_MODE_REG, IOT_MODE_RDWR_ADR8_D16); //  8 adresses, RD/WR, 16 bit databus + 2 I/O
//    iot_wr(IOT_MODE_REG, IOT_MODE_RDWR_ADR2_D16); //  2 adresses, RD/WR, 16 bit databus + 8 I/O

//    iot_wr(IOT_MODE_REG, IOT_MODE_E_RW_ADR12_D16); // 12 adresses, Eclk + R/W, 16 bit databus
//    iot_wr(IOT_MODE_REG, IOT_MODE_E_RW_ADR10_D16); // 10 adresses, Eclk + R/W, 16 bit databus
//    iot_wr(IOT_MODE_REG, IOT_MODE_E_RW_ADR8_D16);  //  8 adresses, Eclk + R/W, 16 bit databus + 2 I/O
//    iot_wr(IOT_MODE_REG, IOT_MODE_E_RW_ADR2_D16);  //  2 adresses, Eclk + R/W, 16 bit databus + 8 I/O

      iot_wr(IOT_IOSET_REG,  0xc0);  // Bit 7,6 = 1
      iot_wr(IOT_IOCLR_REG,  0xcf);  // Bit 5,4 = 0

      /* Select extend clocks */
//    iot_set_buswait( 12, 8 );  // Optional extend bus cycles ex. so any /WAIT time is honored

      /* Activate reset line (negative pulse) */
      iot_wr(IOT_IOCLR_REG, ~IOT_RESET_BIT);  // Clear reset bit low
      #ifdef GHW_PCSIM
      Sleep(100); // delay 1 ms
      #endif
      iot_wr(IOT_IOSET_REG,  IOT_RESET_BIT);  // Set reset bit high
      #ifdef GHW_PCSIM
      Sleep(300); // delay 10 ms
      #endif

      /* Activate reset line (positive pulse) */
//    iot_wr(IOT_IOSET_REG,  IOT_RESET_BIT);  // Set reset bit high
//    iot_wr(IOT_IOCLR_REG, ~IOT_RESET_BIT);  // Clear reset bit low

      /* Use (A19) address line as reset line (negative pulse)
        (Here assuming that the mode is IOT_MODE_RDWR_ADR20 or IOT_MODE_E_RW_ADR20)
        (Use this method for the RAMTEX PR-SSD1906 board, which uses 19 address lines) */
//    iot_wr(IOT_ADR_REG, 0x000000);
//    iot_wr(IOT_ADR_REG, 0x080000);
      }
   else
      glcd_err = 1;
   #endif

   #ifdef GHW_SINGLE_CHIP
   sim_reset();  /* Initiate LCD bus simulation ports */
   #endif

   /* Insert required target specific code here, if any */

   /* Set LCD reset line /RST active low   (if /RST is connected to a port bit) */
   /* Set LCD reset line /RST passive high (if /RST is connected to a port bit) */
   #endif
   }

/*
  This function is called once by gexit() via ghw_exit() as the last operation
  after all LCD controller operations has stopped.
  Any target system specific de-initialization, like I/O port deallocation
  can be placed here. In most embedded systems this function can be empty.
*/
void ghw_io_exit(void)
   {
   #ifndef GHW_NOHDW
   /* Insert required code here, if any */
   #ifdef IOTESTER
   iot_exit();
   #endif
   #endif
   }

#endif /* GBASIC_INIT_ERR */

