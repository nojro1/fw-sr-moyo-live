Hardware access interface file for RAMTEX display driver libraries
==================================================================

The SG headers are used by all RAMTEX display driver libraries
The purpose with the SG headers is to assure that the final code can be
compiled with both PC compilers and compilers for the target processor.

A description of the SG syntax can be found at:
   www.ramtex.dk/standard/sgsyntax.htm

The SG headers are described in detail in the display driver library main manual.

----

This directory project contains files specific for using the LCD 
driver library together with PC tools (Ex Microsoft VC++ Express), 
where the LCD display hardware is connected to the PC via the 
RAMTEX IOTester tool.

(The RAMTEX IOTester tool enable drivers for I/O hardware for 
embedded systems to be developed and tested with ordinary PC 
compiler tools. See more at www.ramtex.dk/iotester/iotester.htm)

File description
----------------
sgio.h      Hardware access function. For target compilers and IOTester PC mode.
            This header is included by display driver libraries (and other low-level drivers)
sgio_ta.h   Hardware I/O register definitions for target compilers
            This header is included by sgio.h when compiling with the target compiler.
sgiopcta.h  Hardware I/O register definitions for IOTester use
            This header is included by sgio.h when IOTESTER or IOTESTER_USB is defined
sgtypes.h   Fixed sized integer types. Used in drivers and user code. Platform independent.


ghwioini.c  Hardware interface initialization module for IOTester-USB
            The functions in this module is called by all RAMTEX display driver
            libraries before any I/O opearations is started (and after completion
            of the last I/O operation before exit)
            Replace the ghwioini.c included with the display driver libraries with this
            module and define IOTESTER_USB in the compilers preprocessor settings.
