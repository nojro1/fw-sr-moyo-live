/************************** ghwinitctrl.c *****************************

   Low-level generic driver functions for used of the s6d0129 low-level
   driver modules in buffered mode only.

   PC-simulation mode
   ------------------
   This module can be used directly in PC simulation mode (no customization needed)

   Target execution mode
   ---------------------
   This module must customized for the specific display controller hardware
   in accordance with the comments.

   The relevant places are marked with *** HARDWARE CUSTOMISATION ***
   Target processor specific code should be encapsulated within the GHW_NOHDW switches
   to assure the same source module can be used with PC simulation


   Revision date:    25-03-2012
   Revision Purpose: Customized version for use with generic display
                     controller interface only.

   Revision date:    25-03-2012
   Revision Purpose: Implementation for use with IOTester-USB-Disp
                     The IOTester display controller is initialized for use
                     of RGB24 in a packed video buffer.

   Version number: 1.1
   Copyright (c) RAMTEX Engineering Aps 2012

*********************************************************************/

#include <gen_ctrl.h>  /* controller specific definements */

#ifndef GHW_NOHDW
/* #include <my_target.h> */      /* Portable I/O functions + hardware port def for compiler */
#endif

#ifndef GHW_GCTRL_RGB
 #error Unknown controller (or wrong gdispcfg.h file)
#endif

#ifndef GBUFFER
  #error This generic interface require use of buffered mode (define GBUFFER in gdispcfg.h)
#endif

#ifdef GBASIC_INIT_ERR

/************* simulator interface ************************/

#ifdef GHW_PCSIM
/* PC simulator declarations */
void ghw_set_xy_sim(GXT xb, GYT yb);
void ghw_autowr_sim( GCOLOR cval );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
#endif

/******************************************************************************************

   IOTester display controller initialization values for use with external RGB tft display
   These settings should be adjusted to fit the timing requirements with the specific
   display module (see IOTester manual for details)

*******************************************************************************************/

#ifndef GHW_NOHDW

#ifndef IOTESTER_USB
 #error This module is for use with IOTESTER_USB only
#endif

/* Clock and timing setup for a 800 x 480 pixel display (ET070080DM6 ) */

// Using DEN signal only, HSYNC,VSYNC not nessesary, PWR ctrl = 1, LEDCTRL = PWM
/* Clock settings */
/* (IOT_DISP_INV_DAT | IOT_DISP_INV_DEN | IOT_DISP_INV_HS | IOT_DISP_INV_VS | IOT_DISP_INV_CLK ) */
#define DISP_CLKPOL    IOT_DISP_INV_HS | IOT_DISP_INV_VS
#define DISP_FRAMECLK  60   //clkdiv; 60 Recommended
#define DISP_VER_PW    2                                     //verpulsw;  3 recommended
#define DISP_VER_BP    (35-(DISP_VER_PW))                    //verbackp;  33 recommended
#define DISP_VER_FP    (525-(GDISPH+DISP_VER_BP+DISP_VER_PW))// Adjust back porch to get recommended total: 525
#define DISP_VER_HDLY  0                                     //verhordly; (not needed)
#define DISP_HOR_PW    40                                    //horpulsw;   recommended
#define DISP_HOR_FP    110
#define DISP_HOR_BP    (1010-GDISPW)
#define PWMLEVEL       0x10  // 0 - 0x100   Reduce backlight consumption when using IOTester 3V3

#if (GDISPPIXW == 24)
   /* Frame buffer is an array of 24 bit elements */
   #define BUFMEM    IOT_FRAMEBUF24
   #define PIXELSIZE IOT_DISP_PIXELSIZE_24
#elif (GDISPPIXW == 32)
   /* Frame buffer is an array of 32 bit elements */
   #define BUFMEM    IOT_FRAMEBUF32
   #define PIXELSIZE IOT_DISP_PIXELSIZE_32
#else
  #error Unsupported pixel size
#endif

#endif /* GHW_NOHDW */

/*
   Initialize display controller hardware
   --------------------------------------

   This function is activated once when ghw_init() is called via ginit().

   The display controller hardware should be initialized based on the
   gdispcfg.h configuration so it tracks the rest of the library.

   In particular these common defines must typically be considered:

       GDISPW    pixel width of display
       GDISPH    pixel height of display
       GDISPPIXW number of bits pr pixel( ex 16,24,32)
       GCOLOR    type to hold pixel info (sizeof( GCOLOR )*8 must be >= GDISPPIXW )

   Depending on the display controller hardware flexibility then these low-level
   switches in gdispcfg.h are commonly used during hardware configurations of
   display controllers.

*/
void ghw_ctrl_init(void)
   {
   ghw_io_init(); /* Set any other hardware interface lines, display module hardware reset */

   if (glcd_err == 1)
        return;

   #ifndef GHW_NOHDW
   /*** HARDWARE CUSTOMISATION ***
     Insert display controller specific initialization here
   */
   // Enable 18 bit RGB (TFT) bus mode on J2
   iot_wr( IOT_PIN_MODE_REG, DISP18BIT); /* Activate RGB18 controller, rest of pins is I/O */

   iot_wr(IOT_DISP_FRAMEOFFSET, 0);  // Default to first register element in internal RAM buffer array
   iot_wr(IOT_DISP_WIDTH       ,GDISPW-1);  // Display_width  -1 = max x coordinate
   iot_wr(IOT_DISP_HEIGHT      ,GDISPH-1);  // Display_height -1 = max y coordinate
   iot_wr(IOT_DISP_PIXCFG      ,PIXELSIZE |
                                (DISP_CLKPOL)
                              | IOT_DISP_CLK_ALWAYSACTIVE );

   iot_wr(IOT_DISP_FRAMECLK    ,DISP_FRAMECLK );  //clkdiv; Normally approx 50-70 Hz
   iot_wr(IOT_DISP_VER_FP      ,DISP_VER_FP   );  //verfrontp;
   iot_wr(IOT_DISP_VER_BP      ,DISP_VER_BP   );  //verbackp;
   iot_wr(IOT_DISP_VER_PW      ,DISP_VER_PW-1 );  //verpulsw;
   iot_wr(IOT_DISP_VER_HDLY    ,DISP_VER_HDLY );  //verhordly;
   iot_wr(IOT_DISP_HOR_FP      ,DISP_HOR_FP-1 );  //horfrontp;
   iot_wr(IOT_DISP_HOR_BP      ,DISP_HOR_BP-1 );  //horbackp;
   iot_wr(IOT_DISP_HOR_PW      ,DISP_HOR_PW-1 );  //horpulsw;
   iot_wr(IOT_DISP_ON          ,IOT_DISP_START);  // Start display
   iot_wr(IOT_DISP_PWMCTRL,    0x80);   // backlight level

   iot_wr(IOT_DISP_FRAMEOFFSET, 0);  // Default to first register element in internal RAM buffer array
   iot_wr(IOT_DISP_WIDTH       ,GDISPW-1);  // Display_width  -1 = max x coordinate
   iot_wr(IOT_DISP_HEIGHT      ,GDISPH-1);  // Display_height -1 = max y coordinate
   iot_wr(IOT_DISP_PIXCFG      ,(PIXELSIZE |
                               IOT_DISP_CLK_ALWAYSACTIVE |
                               IOT_DISP_INV_VS |
                               IOT_DISP_INV_HS));

   iot_sync(0);
   #endif
   }

/*
   Release any hardware ressources
   ( called by ghw_exit() )
   If not needed by the target system then just let this function be unmodified.
*/
void ghw_ctrl_exit(void)
   {
   ghw_io_exit(); /* reset any hardware interface lines to idle */

   #ifndef GHW_NOHDW
   /*** HARDWARE CUSTOMISATION ***
     Insert display controller shut down (ex stop scan, full power down)
   */

   // Stop and disable display controller
   iot_wr( IOT_DISP_PWMCTRL, 0x0);   // backlight level
   iot_wr( IOT_PIN_MODE_REG, iot_rd( IOT_PIN_MODE_REG) & ~DISPBITS);
   #endif

   #ifdef IOTESTER_USB
   iot_sync(IOT_SYNC);
   #endif
   }

/*
   Turn display off  (public hardware interface function)
*/
void ghw_dispoff(void)
   {
   #ifndef GHW_NOHDW
   /*** HARDWARE CUSTOMISATION ***
      Inset hardware commands to turn off display module, any backlight, etc)
   */
   #endif

   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif
   #ifdef IOTESTER_USB
   iot_sync(IOT_SYNC);
   #endif
   }

/*
   Turn display on   (public hardware interface function)
*/
void ghw_dispon(void)
   {
   #ifndef GHW_NOHDW
   /*** HARDWARE CUSTOMISATION ***
      Inset hardware commands to turn on display modules, any backlight, etc
   */
   #endif

   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif
   #ifdef IOTESTER_USB
   iot_sync(IOT_SYNC);
   #endif
   }

/**************** Data bus transmission and format handling **********************/

/*
   Flush of (logcially horizontal) video line segment to controller hardware.

     *cp     pointer to array of GCOLOR elements
     xb,yb   logical screen pixel position for cp[0]. (0,0 is upper-left corner)
     endidx  index of last cp array element to output (max value is GDISPW-1)

    Any software based RGB color format conversion can be implemented in this
    module as well (ex RGB, BGR swapping, byte flushing order, etc).
*/
void ghw_ctrl_line_wr( GXT xb, GYT yb, GCOLOR *cp, GXT endidx )
   {
   GXT cnt;

   #ifndef GHW_NOHDW
   /*** HARDWARE CUSTOMISATION ***/
   unsigned long index;

   /* Insert init of hardware pointers to start at logical position xb,yb here */

   for (cnt = 0, index = GDISPW*yb+xb;;)  /* Copy to hardware loop */
      {
      /* Insert write to hardware here */
      iot_wrbuf(BUFMEM, index, cp[cnt]);

      if (cnt == endidx) /* End of line segment ? */
         break;         /* Normal exit */
      cnt++;
      index++;
      }
   #endif

   #ifdef GHW_PCSIM
   /* Init simulated hardware pointers to start at logical position xb,yb */
   ghw_set_xy_sim(xb, yb);

   /* Copy data to simulated hardware */
   for (cnt = 0;;)
      {
      /* hardware stream register = cp[cnt]; */
      ghw_autowr_sim(cp[cnt]);
      if (cnt == endidx)
         break;
      cnt++;
      }
   #endif

   }

#endif /* GBASIC_INIT_ERR */




