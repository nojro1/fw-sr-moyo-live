/***********************************************************************
 *
 * This file contains I/O port definition for the target system and is
 * included by sgio.h
 *
 * Below are example definitions for symbolic register names most often
 * use with RAMTEX display driver librarys
 *
 * The example definitions here assume that the LCD I/O registers are
 * memory mapped at fixed addresses, and that the register SELECT line on
 * the display is connected to address bus bit 0 signal.
 *
 * Modify these definitions so they fit the actual target system and the
 * I/O register definition syntax used by the actual target compiler.
 *
 * (The symbolic register names correspond to those in sgiopcta.h)
 *
 * Copyright (c) RAMTEX International 2000-2010
 * Version 1.0
 *
 **********************************************************************/
/* Used for 8 bit bus mode */
#define GHWWR  (* (SGUCHAR volatile *) ( 0x0001 ))
#define GHWRD  (* (SGUCHAR volatile *) ( 0x0001 ))
#define GHWSTA (* (SGUCHAR volatile *) ( 0x0000 ))
#define GHWCMD (* (SGUCHAR volatile *) ( 0x0000 ))

/* Used for 16 bus mode */
#define GHWWRW  (* (SGUINT volatile *) ( 0x0001 ))
#define GHWRDW  (* (SGUINT volatile *) ( 0x0001 ))
#define GHWSTAW (* (SGUINT volatile *) ( 0x0000 ))
#define GHWCMDW (* (SGUINT volatile *) ( 0x0000 ))

/* Used for 32 bus mode (18 bit, 24 bit pr pixel) */
#define GHWWRDW  (* (SGULONG volatile *) ( 0x0001 ))
#define GHWRDDW  (* (SGULONG volatile *) ( 0x0001 ))
#define GHWSTADW (* (SGULONG volatile *) ( 0x0000 ))
#define GHWCMDDW (* (SGULONG volatile *) ( 0x0000 ))


