/****************************************************************************************/
/*                                                                                      */
/*   iotreg.h                                                                           */
/*                                                                                      */
/*   Defininiton of internal registers and register bits in IOTESTER-USB, ver. 1        */
/*   These ioreg_designators definitions are used with iot_xx(..) or ioxx(..) functions */
/*                                                                                      */
/*   This module is automatically included via <iotester.h>. Do not include it directly */
/*   in application modules.                                                            */
/*                                                                                      */
/*   Version number: 1.2                                                                */
/*   Copyright (c) RAMTEX Engineering Aps 2010                                          */
/*                                                                                      */
/****************************************************************************************/
#ifndef IOTREG_HDW_VER1_H
#define IOTREG_HDW_VER1_H

#ifdef __cplusplus
    extern "C" {
#endif

/***
   Define primary address range blocks in the virtual IOTester address range
   (used in the internal registers ioreg_designator definitions below)
***/
#define  IOT_IOREG         0x03ffff80  /* Registers for I/O pin configuration and control */
#define  IOT_INTFUNCREG    0x03ffe000  /* Registers for special internal I/O devices */
#define  IOT_INTFUNCMSK    0x03fff000
#define  IOT_FRAMEBUF      0x01000000  /* Logical frame buffer base when display controller is enabled (IOTester-USB-DISP only) */
#define  IOT_FRAMEBUF_SIZE 0x01000000  /* 16M bytes (IOTester-USB-DISP only) */
#define  IOT_EXTBUS        0x00000000  /* Logical external bus */
#define  IOT_EXTBUS_SIZE   0x01000000  /* 16M bytes */

/***
   Define IOT_PIN_MODE_REG register and bits
   This is the primary register for defining how IOTester pins are used.
***/
#define  IOT_PIN_MODE_REG  (IOT_IOREG|IOT_REG32|0x00)

/* Define bits for data bus setting */
#define  NODBUS       0x0        /* No data bus or adress bus used (IOMODE) */
#define  DBUS8        0x1        /* 8 bit data bus */
#define  DBUS16       0x2        /* 16 bit data bus (word aligned address) */
#define  DBUS16HL     0x3        /* 16 bit data bus+WRH+WRL (byte aligned address) */
#define  DATABUSSIZE  0x00000007
#define  ALTBUS       0x00000200 /* Alternative bus mode bit (+IOTester-LPT compatibility mode) */

/* Define bus clock signal type */
#define  DBUS8080     0x8        /* 1 = 8080 bus type, 0 = 6800 bus type */

/* Define address bus size */
#define  ADRBUSSIZE_MSK 0x000001f0  /* Pins used by adress bus */
#define  ADRBUSSIZE(numadrbits) (((numadrbits) << 4) & ADRBUSSIZE_MSK)

/* Reset IOTester to power-on reset state (override all other mode bits) */
#define  IOTRESET     0x00000800

/* Enable serial busses */
#define  I2CBUS     0x00100000   /* I2C bus enable (configurable pin locations) */
#define  SPIBUS     0x00200000   /* SPI bus enable (configurable pin locations) */

/* Enable Interrupt pin*/
#define  INT_POL    0x00400000   /* Interrupt edge polarity */
#define  INT_ENA    0x00800000   /* Interrupt pin enable (J1,pin 1) */

/* Bits for display controller enabling and pin configuration (IOTester-USB-DISP only) */
#define  DISPOFF     0x00000000
#define  DISP18BIT   0xc0000000   /* TFT 18 bit bus pin layout */
#define  DISP24BIT   0xe0000000   /* TFT 24 bit bus pin layout (alternative pin layout) */
#define  DISPBITS    0xe0000000   /* Mask for reserved LCD mode bits*/

/* Bits for touch controller device enable (IOTester-USB-DISP only) */
#define  TOUCHENA    0x01000000   /* Touch controller pin use J1 pin 1,2,3,4 */

/***
   Define IOT_WAIT_STATES register (external bus clock wait states)
***/
#define  IOT_WAIT_STATES   ( IOT_INTFUNCREG | IOT_REG16 | 0x104 ) /* Number of /WR,/RD or Eclk wait states */

#define  IOT_RDDLY(dly)    (((dly) > 0xf) ?   0xf : (dly))
#define  IOT_WRDLY(dly)    (((dly) > 0xf) ? 0xf00 : ((unsigned short)(dly)  << 8))

/***
   Define IOT_WAIT_STATES and IOT_PIN_WAIT register (external bus WAIT input pin)
***/
#define  IOT_WAIT_CTRL     ( IOT_INTFUNCREG | IOT_REG16 | 0x106 ) /* WAIT pin control */
#define  IOT_PIN_WAIT      ( IOT_INTFUNCREG | IOT_REG32 | 0x108 ) /* Pin loacation for WAIT pin */

#define  IOT_WAITMAX(wait) (((wait) > 0xff) ? 0xff : ((unsigned short)(wait) << 8))
#define  IOT_WAITPOL(pol)  (((pol) !=0) ? 0x8000 : 0)

/***
   J1 port pin configuration registers
***/
#define  IOT_IOSET_REG1    (IOT_IOREG|IOT_REG32|0x20) /* 1 bits = Set output high */
#define  IOT_IOCLR_REG1    (IOT_IOREG|IOT_REG32|0x24) /* 0 bits = Set output low */
#define  IOT_IODIRSET_REG1 (IOT_IOREG|IOT_REG32|0x28) /* 1 bits = Enable output */
#define  IOT_IODIRCLR_REG1 (IOT_IOREG|IOT_REG32|0x2c) /* 0 bits = Disable output */
#define  IOT_IOPUPSET_REG1 (IOT_IOREG|IOT_REG32|0x30) /* 1 bits = Enable pin pull-up */
#define  IOT_IOPUPCLR_REG1 (IOT_IOREG|IOT_REG32|0x34) /* 0 bits = Disable pin pull-up */
#define  IOT_INPUT_REG1    (IOT_IOREG|IOT_REG32|0x38) /* Read pin levels (read only) */

/***
   J2 port pin configuration registers
****/
#define  IOT_IOSET_REG2    (IOT_IOREG|IOT_REG32|0x40) /* 1 bits = Set output high */
#define  IOT_IOCLR_REG2    (IOT_IOREG|IOT_REG32|0x44) /* 0 bits = Set output low */
#define  IOT_IODIRSET_REG2 (IOT_IOREG|IOT_REG32|0x48) /* 1 bits = Enable output */
#define  IOT_IODIRCLR_REG2 (IOT_IOREG|IOT_REG32|0x4C) /* 0 bits = Disable output */
#define  IOT_IOPUPSET_REG2 (IOT_IOREG|IOT_REG32|0x50) /* 1 bits = Enable pin pull-up */
#define  IOT_IOPUPCLR_REG2 (IOT_IOREG|IOT_REG32|0x54) /* 0 bits = Disable pin pull-up */
#define  IOT_INPUT_REG2    (IOT_IOREG|IOT_REG32|0x58) /* Read pin levels (read only) */

/***
   J1, J2 pin name definitions.
   Can used with J1-J2 port registers and with SPI and I2C pin reroute registers
***/

/* Connector select bit for pin reroute registers (I2C+SPI)*/
#define  IOT_J1_PINS   (0)
#define  IOT_J2_PINS   ((unsigned long)(1<<31))

/* Define names for J1 connector pins. i.e. convert from physical pin number to logical register bit */
#define  IOT_J1_PIN1   (IOT_J1_PINS|(1<<17))
#define  IOT_J1_PIN2   (IOT_J1_PINS|(1<<16))
#define  IOT_J1_PIN3   (IOT_J1_PINS|(1<<15))
#define  IOT_J1_PIN4   (IOT_J1_PINS|(1<<14))
#define  IOT_J1_PIN5   (IOT_J1_PINS|(1<<13))
#define  IOT_J1_PIN6   (IOT_J1_PINS|(1<<12))
#define  IOT_J1_PIN7   (IOT_J1_PINS|(1<<11))
#define  IOT_J1_PIN8   (IOT_J1_PINS|(1<<10))
#define  IOT_J1_PIN9   (IOT_J1_PINS|(1<<9))
#define  IOT_J1_PIN10  (IOT_J1_PINS|(1<<8))
#define  IOT_J1_PIN13  (IOT_J1_PINS|(1<<21))
#define  IOT_J1_PIN14  (IOT_J1_PINS|(1<<20))
#define  IOT_J1_PIN15  (IOT_J1_PINS|(1<<19))
#define  IOT_J1_PIN16  (IOT_J1_PINS|(1<<18))
#define  IOT_J1_PIN17  (IOT_J1_PINS|(1<<0))
#define  IOT_J1_PIN18  (IOT_J1_PINS|(1<<1))
#define  IOT_J1_PIN19  (IOT_J1_PINS|(1<<2))
#define  IOT_J1_PIN20  (IOT_J1_PINS|(1<<3))
#define  IOT_J1_PIN21  (IOT_J1_PINS|(1<<4))
#define  IOT_J1_PIN22  (IOT_J1_PINS|(1<<5))
#define  IOT_J1_PIN23  (IOT_J1_PINS|(1<<6))
#define  IOT_J1_PIN24  (IOT_J1_PINS|(1<<7))

/* Define names for J2 connector pins. i.e. convert from physical pin number to logical register bit */
#define  IOT_J2_PIN1   (IOT_J2_PINS|(1<<17))
#define  IOT_J2_PIN2   (IOT_J2_PINS|(1<<16))
#define  IOT_J2_PIN3   (IOT_J2_PINS|(1<<15))
#define  IOT_J2_PIN4   (IOT_J2_PINS|(1<<14))
#define  IOT_J2_PIN5   (IOT_J2_PINS|(1<<13))
#define  IOT_J2_PIN6   (IOT_J2_PINS|(1<<12))
#define  IOT_J2_PIN7   (IOT_J2_PINS|(1<<11))
#define  IOT_J2_PIN8   (IOT_J2_PINS|(1<<10))
#define  IOT_J2_PIN9   (IOT_J2_PINS|(1<<9))
#define  IOT_J2_PIN10  (IOT_J2_PINS|(1<<8))
#define  IOT_J2_PIN13  (IOT_J2_PINS|(1<<18))
#define  IOT_J2_PIN14  (IOT_J2_PINS|(1<<19))
#define  IOT_J2_PIN15  (IOT_J2_PINS|(1<<20))
#define  IOT_J2_PIN16  (IOT_J2_PINS|(1<<21))
#define  IOT_J2_PIN17  (IOT_J2_PINS|(1<<22))
#define  IOT_J2_PIN18  (IOT_J2_PINS|(1<<1))
#define  IOT_J2_PIN19  (IOT_J2_PINS|(1<<2))
#define  IOT_J2_PIN20  (IOT_J2_PINS|(1<<3))
#define  IOT_J2_PIN21  (IOT_J2_PINS|(1<<4))
#define  IOT_J2_PIN22  (IOT_J2_PINS|(1<<5))
#define  IOT_J2_PIN23  (IOT_J2_PINS|(1<<6))
#define  IOT_J2_PIN24  (IOT_J2_PINS|(1<<7))
#define  IOT_J2_PIN26  (IOT_J2_PINS|(1<<23))

/* Port pin used as /RES output (this pin is automatically pulsed low during IOTester power-up) */
#define  IOT_RESET_BIT  IOT_J1_PIN15

/***
   Define I2C bus device registers
***/

/* I2C control register */
#define  IOT_I2C_CTRL     (IOT_INTFUNCREG | IOT_REG8   | 0xfb0)

#define  I2C_SETSTART       0x01  /* Send I2C start (next state is I2CADRESS) */
#define  I2C_SETSTOP        0x02  /* Send I2C stop  (next state is I2CIDLE)*/
#define  I2C_SETRDNACK      0x03  /* Next read is not acknowledged (default is always ack) */
#define  I2C_CTRLMSK        0x03  /* Mask for control bits */

#define  I2C_CLR_ERR        0x08  /* Clear missing ack error */
#define  I2C_SET_FAST_DRIVE 0x10  /* Activate fast drive (equal to small pull up resistors) */
#define  I2C_CLR_FAST_DRIVE 0x20  /* Deactivate fast drive */

/* I2C status register (read only) */
#define  IOT_I2C_STATUS   (IOT_INTFUNCREG | IOT_REG8   | 0xfb4)

#define  I2C_ADRESS         0x00
#define  I2C_READ           0x40
#define  I2C_WRITE          0x80
#define  I2C_IDLE           0xC0
#define  I2C_STATEMSK       0xC0  /* Mask for I2C bus state bits */
#define  I2C_ERR_RDWR       0x20  /* Illegal I2C read-write / write-read sequence */
#define  I2C_ERR_ACK        0x10  /* No ack received from device during write or access */
#define  I2C_FAST_DRIVE     0x08  /* Fast drive activated (equal to small pull up resistors) */
#define  I2C_RDNACK         0x04  /* Send NACK on next byte read (cleared automatically) */

/* I2C data registers (the access size automatically defines the number bytes transmitted) */
#define  IOT_I2C_BUS8     (IOT_INTFUNCREG | IOT_REG8  | 0xfb8)
#define  IOT_I2C_BUS16    (IOT_INTFUNCREG | IOT_REG16 | 0xfb8)
#define  IOT_I2C_BUS24    (IOT_INTFUNCREG | IOT_REG24 | 0xfb8)
#define  IOT_I2C_BUS32    (IOT_INTFUNCREG | IOT_REG32 | 0xfb8)

/* I2C pin reroute setup */
#define  IOT_PIN_I2C_SCL  (IOT_INTFUNCREG | IOT_REG32 | 0xfc0)
#define  IOT_PIN_I2C_SDA  (IOT_INTFUNCREG | IOT_REG32 | 0xfc4)

/***
   Define SPI bus device registers
***/

/* SPI configuration register */
#define  IOT_SPI_CFG        (IOT_INTFUNCREG|IOT_REG8   | 0xfd0)

#define  SPICSLEVEL        0x01 /* Read only mask, Active SPI_CS level */
#define  SPICSCTRL         0x02 /* 0 -> automatic pr byte, 1 -> user controlled pin */
#define  SPICLKLEVEL       0x04 /* 0 -> 101  1 -> 010 */
#define  SPITXLEVEL        0x08 /* 0 -> SPI_SDO = 0 under RX  1 -> SPI_SDO = 1 under RX */
#define  SPITXRX           0x10 /* SPI data is received to fifo while transmitting */
#define  SPIFIFOCLR        0x20 /* Clear receive while transmit fifo */
#define  SPIFIFOFULL       0x40 /* Fifo full status (read only) */
#define  SPIFIFODATA       0x80 /* Fifo not empty status (read only) */

/* SPI variable data size configuration register */
#define  IOT_SPI_CFG_NBIT   (IOT_INTFUNCREG|IOT_REG8  | 0xfd1)

/* SPI data registers (the access size automatically defines the number bytes transmitted) */
#define  IOT_SPI_BUS8       (IOT_INTFUNCREG|IOT_REG8  | 0xfd4)
#define  IOT_SPI_BUS16      (IOT_INTFUNCREG|IOT_REG16 | 0xfd4)
#define  IOT_SPI_BUS24      (IOT_INTFUNCREG|IOT_REG24 | 0xfd4)
#define  IOT_SPI_BUS32      (IOT_INTFUNCREG|IOT_REG32 | 0xfd4)

/* SPI variable data length register */
#define  IOT_SPI_BUSNBIT    (IOT_INTFUNCREG|IOT_REG32 | 0xfd8)

/* SPI pin reroute setup */
#define IOT_PIN_SPI_SCL  (IOT_INTFUNCREG|IOT_REG32 | 0xfe0)
#define IOT_PIN_SPI_SDO  (IOT_INTFUNCREG|IOT_REG32 | 0xfe4)
#define IOT_PIN_SPI_SDI  (IOT_INTFUNCREG|IOT_REG32 | 0xfe8)
#define IOT_PIN_SPI_SCS  (IOT_INTFUNCREG|IOT_REG32 | 0xfec)

/***
   IOT_TIMESTAMP  1 ms timestamp counter device
   IOT_TIMER      10 ms down counter, with interrupt possibilities
***/
#define  IOT_TIMESTAMP     ( IOT_INTFUNCREG | IOT_REG32 | 0x200 ) /* 32 bit 1ms time stamp counter */
#define  IOT_TIMER         ( IOT_INTFUNCREG | IOT_REG32 | 0x204 ) /* 16 bit 10ms down counter Read only */
#define  IOT_TIMER_CTRL    ( IOT_INTFUNCREG | IOT_REG32 | 0x208 ) /* Timer config */

/* IOT_TIMER_CTRL bits */
#define  IOT_TIMER_MASK          0xffff  /* 16 bit counter value */
#define  IOT_TIMER_PERIODIC  0x80000000  /* Auto reload timer */
#define  IOT_TIMER_SYNCUPD   0x40000000  /* Syncronous auto reload mode */

/***
  IOT_INT0CNT  INTR pin event counter register
***/
#define  IOT_INT0CNT        (IOT_INTFUNCREG | IOT_REG32 | 0x10C)

/***
   Interrupt controller registers
***/
#define  IOT_INTREQ        (IOT_IOREG|IOT_REG32|0x0C)
#define  IOT_INTENA_CLR    (IOT_IOREG|IOT_REG32|0x10)
#define  IOT_INTENA_SET    (IOT_IOREG|IOT_REG32|0x14)

/* Interrrupt controller data */
#define  IOT_NUMBER_INTERRUPTS 31   /* Last possible vector */

/* Interrupt vector numbers */
#define  IOTV_IRQ           0
#define  IOTV_TIMER         1
#define  IOTV_TOUCH         3

/* Convert device vector number (0-31) to a logical vector number (0-31)+(32*device_n)
   Used when multiple logical IOTester devices are used by the same PC */
#define  IOT_DEVICE_VEC( device_designator, vectornum) \
         ((vectornum) + (IOT_DEVICE(device_designator)*(IOT_NUMBER_INTERRUPTS+1)))

/* interrupt bit masks */
#define  IOTIE_IRQ          (1<<IOTV_IRQ)   /* External pin interrupt */
#define  IOTIE_TIMER        (1<<IOTV_TIMER) /* Timer interrupt */
#define  IOTIE_RESERVED2    (1<<2 )
#define  IOTIE_TOUCH        (1<<IOTV_TOUCH) /* Touch controller interrupt (IOTester-USB-DISP only) */
#define  IOTIE_RESERVED4    (1<<4 )
#define  IOTIE_RESERVED5    (1<<5 )
#define  IOTIE_RESERVED6    (1<<6 )
#define  IOTIE_RESERVED7    (1<<7 )
#define  IOTIE_RESERVED8    (1<<8 )
#define  IOTIE_RESERVED9    (1<<9 )
#define  IOTIE_RESERVED10   (1<<10)
#define  IOTIE_RESERVED11   (1<<11)
#define  IOTIE_RESERVED12   (1<<12)
#define  IOTIE_RESERVED13   (1<<13)
#define  IOTIE_RESERVED14   (1<<14)
#define  IOTIE_RESERVED15   (1<<15)
#define  IOTIE_RESERVED16   (1<<16)
#define  IOTIE_RESERVED17   (1<<17)
#define  IOTIE_RESERVED18   (1<<18)
#define  IOTIE_RESERVED19   (1<<19)
#define  IOTIE_RESERVED20   (1<<20)
#define  IOTIE_RESERVED21   (1<<21)
#define  IOTIE_RESERVED22   (1<<22)
#define  IOTIE_RESERVED23   (1<<23)
#define  IOTIE_RESERVED24   (1<<24)
#define  IOTIE_RESERVED25   (1<<25)
#define  IOTIE_RESERVED26   (1<<26)
#define  IOTIE_RESERVED27   (1<<27)
#define  IOTIE_RESERVED28   (1<<28)
#define  IOTIE_RESERVED29   (1<<29)
#define  IOTIE_RESERVED30   (1<<30)
#define  IOTIE_GLOBAL       ((unsigned long)(1<<31))
#define  IOTIE_ALL          (IOTIE_IRQ|IOTIE_TIMER|IOTIE_TOUCH)


/***
   Copy / Fill accelerator registers (IOTester-USB-DISP only)
***/
#define  IOT_ACC_DEST      ( IOT_INTFUNCREG | IOT_REG32 | 0x110 ) /* Destination Base address for fill+copy operation (+ register size) */
#define  IOT_ACC_DEST_XCNT ( IOT_INTFUNCREG | IOT_REG32 | 0x114 ) /* Number of X elements (= area width) */
#define  IOT_ACC_DEST_YCNT ( IOT_INTFUNCREG | IOT_REG32 | 0x118 ) /* Number of base address increments (= area height) */
#define  IOT_ACC_DEST_YINC ( IOT_INTFUNCREG | IOT_REG32 | 0x11C ) /* Base address increments after X writes (= line width) */
#define  IOT_ACC_SRC       ( IOT_INTFUNCREG | IOT_REG32 | 0x120 ) /* Source base address for copy operation (+ register size) */
#define  IOT_ACC_SRC_XCNT  ( IOT_INTFUNCREG | IOT_REG32 | 0x124 ) /* Number of X elements (= area width) */
#define  IOT_ACC_SRC_YCNT  ( IOT_INTFUNCREG | IOT_REG32 | 0x128 ) /* Number of base address increments (= area height) */
#define  IOT_ACC_SRC_YINC  ( IOT_INTFUNCREG | IOT_REG32 | 0x12c ) /* Base address increments after X writes (= source line width) */
#define  IOT_ACC_FILL_DATA ( IOT_INTFUNCREG | IOT_REG32 | 0x138 ) /* Fill data (1,2,3 or 4 bytes used, depending on IOT_ACC_FILL_DATA size) */
#define  IOT_ACC_START     ( IOT_INTFUNCREG | IOT_REG32 | 0x13C ) /* Fill / copy start register */

/* IOT_ACC_START bits */
#define  IOT_ACC_FILL      0x1  /* Start fill operation command */
#define  IOT_ACC_COPY      0x3  /* Start copy/move operation command */
#define  IOT_ACC_DEST_INC  0x80 /* Flag for auto increment during of external bus write (fill or copy oper) */
#define  IOT_ACC_SRC_INC   0x40 /* Flag for auto increment during of external bus read (copy opr ) */

/***
   Display controller registers  (IOTester-USB-DISP only)
***/
#define  IOT_DISP_FRAMEOFFSET     (IOT_INTFUNCREG|IOT_REG32 | 0xf90)
#define  IOT_DISP_WIDTH           (IOT_INTFUNCREG|IOT_REG16 | 0xfa4)
#define  IOT_DISP_HEIGHT          (IOT_INTFUNCREG|IOT_REG16 | 0xfa6)
#define  IOT_DISP_PIXCFG          (IOT_INTFUNCREG|IOT_REG16 | 0xfaa)
#define  IOT_DISP_FRAMECLK        (IOT_INTFUNCREG|IOT_REG16 | 0xfac)
#define  IOT_DISP_VER_FP          (IOT_INTFUNCREG|IOT_REG16 | 0xfae)
#define  IOT_DISP_VER_BP          (IOT_INTFUNCREG|IOT_REG16 | 0xfb0)
#define  IOT_DISP_VER_PW          (IOT_INTFUNCREG|IOT_REG16 | 0xfb2)
#define  IOT_DISP_VER_HDLY        (IOT_INTFUNCREG|IOT_REG16 | 0xfb4)
#define  IOT_DISP_HOR_FP          (IOT_INTFUNCREG|IOT_REG16 | 0xfb6)
#define  IOT_DISP_HOR_BP          (IOT_INTFUNCREG|IOT_REG16 | 0xfb8)
#define  IOT_DISP_HOR_PW          (IOT_INTFUNCREG|IOT_REG16 | 0xfba)
#define  IOT_DISP_PWMCTRL         (IOT_INTFUNCREG|IOT_REG16 | 0xfbc)
#define  IOT_DISP_ON              (IOT_INTFUNCREG|IOT_REG16 | 0xfbe)
#define  IOT_DISP_J2_PINS_NOTUSED (IOT_INTFUNCREG|IOT_REG32 | 0xf9c)

/* IOT_DISP_PIXCFG  bit settings */
#define  IOT_DISP_PIXELSIZE_24     (0x5 << 5)  /* TFT 24 Bits (RGB packed 24 Bits TFT)  */
#define  IOT_DISP_PIXELSIZE_32     (0x6 << 5)  /* TFT 32 bits (RGB unpacked 24 Bits TFT)*/
#define  IOT_DISP_PIXELSIZE_MSK    (0x7 << 5)  /* Pixel size bits (mask reserved bits)  */

#define  IOT_DISP_INV_DAT          (0x1 <<  8) /* Invert display bus data polarity  */
#define  IOT_DISP_INV_VS           (0x1 <<  9) /* Invert display bus VSYNC polarity */
#define  IOT_DISP_INV_HS           (0x1 << 10) /* Invert display bus HSYNC polarity */
#define  IOT_DISP_INV_CLK          (0x1 << 11) /* Invert display bus PCLK polarity */
#define  IOT_DISP_INV_DEN          (0x1 << 12) /* Invert display bus DEN polarity */
#define  IOT_DISP_CLK_ALWAYSACTIVE (0x1 << 15) /* Display pclk always active */

/* IOT_DISP_ON  Display Control bit */
#define  IOT_DISP_START            (0x1)      /* 1 = start configuration */

/***
   Define base address for frame buffer (to be used as base for I/O buffer array access)
   (IOTester-USB-DISP only)
***/
#define  IOT_FRAMEBUF8      (IOT_FRAMEBUF | IOT_REG8)
#define  IOT_FRAMEBUF16     (IOT_FRAMEBUF | IOT_REG16L)
#define  IOT_FRAMEBUF24     (IOT_FRAMEBUF | IOT_REG24L)
#define  IOT_FRAMEBUF32     (IOT_FRAMEBUF | IOT_REG32L)
#define  IOT_FRAMEBUF16H    (IOT_FRAMEBUF | IOT_REG16H)
#define  IOT_FRAMEBUF24H    (IOT_FRAMEBUF | IOT_REG24H)
#define  IOT_FRAMEBUF32H    (IOT_FRAMEBUF | IOT_REG32H)

/***
   Touch screen controller registers (IOTester-USB-DISP only)
***/
#define  IOT_TOUCH        (IOT_INTFUNCREG|IOT_REG32 | 0xf80)
#define  IOT_TOUCH_CFG    (IOT_INTFUNCREG|IOT_REG32 | 0xf84)
#define  IOT_TOUCH_LEVEL    0x10000000  /* Current touch down level */
#define  IOT_TOUCH_DRAG     0x20000000  /* Drag event (at touch down) detected */
#define  IOT_TOUCH_DOWN     0x40000000  /* Touch down edge event detected */
#define  IOT_TOUCH_UP       0x80000000  /* Touch up edge event detected */
#define  IOT_TOUCH_Y(val)   ((val) & 0xfff)
#define  IOT_TOUCH_X(val)  (((val) >> 12) & 0xfff)

/***
   Hardware serial number information register
***/
#define  IOT_HDWINFO     (IOT_INTFUNCREG|IOT_REG32|0x100)

/***
   IOTESTER_LPT compatible registers (for backward compatibility with old IOTester-LPT code)
   In future designs use of IOT_PIN_MODE_REG is recommended instead of IOT_MODE_REG
***/
#define  IOT_IOCLR_REG   (IOT_IOREG|IOT_REG32|0x68) /* Functionality = IOT_IOCLR_REG1 */
#define  IOT_IOSET_REG   (IOT_IOREG|IOT_REG32|0x6c) /* Functionality = IOT_IOSET_REG1 */
#define  IOT_IODIR_REG   (IOT_IOREG|IOT_REG32|0x70) /*  */
#define  IOT_INPUT_REG   (IOT_IOREG|IOT_REG32|0x74) /* Functionaity = IOT_INPUT_REG Read only register  */
#define  IOT_MODE_REG    (IOT_IOREG|IOT_REG8 |0x78) /* Write only register */
#define  IOT_ADR_REG     (IOT_IOREG|IOT_REG24|0x79) /* Write only register */

/* IOT_MODE_REG bits */
#define  BUSENA   0x80
#define  BUS7_2   0x10
#define  BUS15_8  0x20
#define  BUS17_16 0x40
#define  BUS19_18 0x08  /* cs /res is used, inverse compatibility mode */
#define  BM8080   0x04
#define  INTENA   0x02  /* J1.1 (A17 disable) */
#define  WM       0x01

/* Predefined setup modes using IOT_MODE_REG */
#define  IOT_MODE_IO             0x00  /* I/O only mode */
/* 8 bit data bus modes */
#define  IOT_MODE_RDWR_ADR20     0xF4  /* Compatibility mode with PR boards */
#define  IOT_MODE_RDWR_ADR18     0xFC
#define  IOT_MODE_RDWR_ADR16     0xBC
#define  IOT_MODE_RDWR_ADR8      0x9C
#define  IOT_MODE_RDWR_ADR2      0x8C
#define  IOT_MODE_E_RW_ADR20     0xF0  /* Compatibility mode with PR boards */
#define  IOT_MODE_E_RW_ADR18     0xF8
#define  IOT_MODE_E_RW_ADR16     0xB8
#define  IOT_MODE_E_RW_ADR8      0x98
#define  IOT_MODE_E_RW_ADR2      0x88
/* 16 bit data bus modes (compatibility modes with IOTESTER-LPT */
#define  IOT_MODE_RDWR_ADR12_D16 0xF5
#define  IOT_MODE_RDWR_ADR10_D16 0xFD
#define  IOT_MODE_RDWR_ADR8_D16  0xBD
#define  IOT_MODE_RDWR_ADR2_D16  0xAD
#define  IOT_MODE_E_RW_ADR12_D16 0xF1
#define  IOT_MODE_E_RW_ADR10_D16 0xF9
#define  IOT_MODE_E_RW_ADR8_D16  0x99
#define  IOT_MODE_E_RW_ADR2_D16  0x89
/* Enable interrupt (override use of P17 as A17) */
#define  IOT_MODE_IRQ_ENABLE     0x02

#ifdef __cplusplus
   }
#endif

#endif /* IOTREG_HDW_VER1_H */

