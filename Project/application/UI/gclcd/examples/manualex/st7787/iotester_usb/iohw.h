/************************************* iohw.h *************************************

   This is an <iohw.h> implementation for use with IOTester_USB

   Include this header in every I/O driver module using the io..() or iot_xxx(..)
   functions.

   ---------

   The C standardization commitee has proposed the <iohw.h> header and functions
   as a method to make I/O driver source code portable accross platforms and compilers.

   The <iohw.h> header functionality is described in the Technical Report:
     ISO/IEC TR 18037

   A version of the TR 18037 document can for instance be downloaded from here:
         http://www.open-std.org/jtc1/sc22/WG14/www/docs/n1169.pdf

   ----------

   This iohw.h implementation contains two parts to facilitate test of "embedded"
   code in the PC environment, while still promoting driver source code portability:

   -  A PC-mode part for use in the PC environment with IOTester.
      Maps the <iohw.h> ioxx(..) functions to the IOTester iot_xx(..) functions
      i.e. both sets of I/O functions is active in the PC environemet

      To activate PC-mode IOTESTER_USB must be defined in the PC compilers pre_processor
      settings, or <iotester.h> must have been included before <iohw.h>

   -  A target-mode part for processors / compilers using memory mapped I/O and
      a simple assignment syntax for I/O register operations (default)

      The target mode part furthermore map the IOTester specific functions to
      "nothing":
      -  Map ioxx(..) functions to the target compilers synctax for I/O access.
      -  Map iot_xx(..) functions to nothing  (no code generated)
      i.e. only ioxx(..) functions generate machine code in target

   Version number: 1
   Copyright (c) RAMTEX Engineering Aps 2010

************************************************************************************/
#ifndef IOHW_H
#define IOHW_H

/*
   In PC mode it is assumed that IOTESTER or IOTESTER_USB is defined in the
   compilers preprocessor settings, or that <iohw.h> is included via <iotester.h>
*/
#if (defined( IOTESTER ) || defined( IOTESTER_USB ) || defined( IOTESTER_H ) )

/*** Start PC mode part for use with IOTester ***/

/* In PC mode include the iotester functions and ioreg_designator definitions for internal registers */
/* (iotester.h is not used in target compilation mode) */
#include <iotester.h>

#ifdef __cplusplus
   extern "C" {
#endif
/* Macro implementation of the <iohw.h> functions */

/* Single I/O register element operations */
#define  iord(  ioreg_desc )       iot_rd( (ioreg_desc) )
#define  iowr(  ioreg_desc, dat )  iot_wr( (ioreg_desc), (dat))
#define  ioor(  ioreg_desc, dat )  iot_or( (ioreg_desc), (dat))
#define  ioand( ioreg_desc, dat )  iot_and((ioreg_desc), (dat))
#define  ioxor( ioreg_desc, dat )  iot_xor((ioreg_desc), (dat))

#define  iordl(  ioreg_desc )       iot_rd( (ioreg_desc) )
#define  iowrl(  ioreg_desc, dat )  iot_wr( (ioreg_desc), (dat))
#define  ioorl(  ioreg_desc, dat )  iot_or( (ioreg_desc), (dat))
#define  ioandl( ioreg_desc, dat )  iot_and((ioreg_desc), (dat))
#define  ioxorl( ioreg_desc, dat )  iot_xor((ioreg_desc), (dat))

/* I/O register array element operations */
#define  iordbuf(  ioreg_desc,  index )      iot_rdbuf( (ioreg_desc), (index))
#define  iowrbuf(  ioreg_desc, index, dat )  iot_wrbuf( (ioreg_desc), (index), (dat))
#define  ioorbuf(  ioreg_desc, index, dat )  iot_orbuf( (ioreg_desc), (index), (dat))
#define  ioandbuf( ioreg_desc, index, dat )  iot_andbuf((ioreg_desc), (index), (dat))
#define  ioxorbuf( ioreg_desc, index, dat )  iot_xorbuf((ioreg_desc), (index), (dat))

#define  iordbufl(  ioreg_desc,  index )      iot_rdbuf( (ioreg_desc), (index))
#define  iowrbufl(  ioreg_desc, index, dat )  iot_wrbuf( (ioreg_desc), (index), (dat))
#define  ioorbufl(  ioreg_desc, index, dat )  iot_orbuf( (ioreg_desc), (index), (dat))
#define  ioandbufl( ioreg_desc, index, dat )  iot_andbuf((ioreg_desc), (index), (dat))
#define  ioxorbufl( ioreg_desc, index, dat )  iot_xorbuf((ioreg_desc), (index), (dat))

/* No iogroup support required with IOTESTER */
#define  iogroup_acquire( iogroup_designator ) { /* Nothing */ }
#define  iogroup_map( iogroup_defignator_d, iogroup_defignator_s ) { /* Nothing */ }

#ifdef __cplusplus
   }
#endif

/**** End PC mode part ***/

#else

/**** Start Target mode part for use with the target processor compiler ***/

#ifdef __cplusplus
    extern "C" {
#endif

/*
   Map <iohw.h> function for memory mapped I/O.
   This implemntation assume that the target processor compiler use use an assignment 
   syntax via constant pointers.

   This <iohw.h> implementation assume that the symbolic names for an I/O register
   access definition ( = ioreg_designator ) is done in a way so the symbolic name
   specifies both the I/O register size and the physical address in the target system.
   For instance like this:
     #define MY_REGISTER_NAME_8BIT  ((volatile unsigned char  *) 0x00000000 )
     #define MY_REGISTER_NAME_16BIT ((volatile unsigned short *) 0x00000004 )
     #define MY_REGISTER_NAME_32BIT ((volatile unsigned long  *) 0x00000008 )

  Note: If your target compiler already has a <iohw.h> implementation then use your
  compilers iohw.h definition, or just comment out the iohw definitions below
*/
/* Functions for single I/O registers */
#define  iord(  ioreg_desc )       ((ioreg_desc)[0])
#define  iowr(  ioreg_desc, dat )  (((ioreg_desc)[0]) = (dat))
#define  ioor(  ioreg_desc, dat )  (((ioreg_desc)[0]) |= (dat))
#define  ioand( ioreg_desc, dat )  (((ioreg_desc)[0]) &= (dat))
#define  ioxor( ioreg_desc, dat )  (((ioreg_desc)[0]) ^= (dat))

#define  iordl(  ioreg_desc )      ((ioreg_desc)[0])
#define  iowrl(  ioreg_desc, dat ) (((ioreg_desc)[0]) = (dat))
#define  ioorl(  ioreg_desc, dat ) (((ioreg_desc)[0]) |= (dat))
#define  ioandl( ioreg_desc, dat ) (((ioreg_desc)[0]) &= (dat))
#define  ioxorl( ioreg_desc, dat ) (((ioreg_desc)[0]) ^= (dat))

/* Type for use as buffer index */
typedef  unsigned int ioindex_t;

/* Functions for operation om an I/O register array (a buffer) */
#define  iordbuf(  ioreg_desc, index )       (((ioreg_desc)[(index)])
#define  iowrbuf(  ioreg_desc, index, dat )  (((ioreg_desc)[(index)])  = (dat))
#define  ioorbuf(  ioreg_desc, index, dat )  (((ioreg_desc)[(index)]) |= (dat))
#define  ioandbuf( ioreg_desc, index, dat )  (((ioreg_desc)[(index)]) &= (dat))
#define  ioxorbuf( ioreg_desc, index, dat )  (((ioreg_desc)[(index)]) ^= (dat))

#define  iordbufl(  ioreg_desc, index )       (((ioreg_desc)[(index)])
#define  iowrbufl(  ioreg_desc, index, dat )  (((ioreg_desc)[(index)])  = (dat))
#define  ioorbufl(  ioreg_desc, index, dat )  (((ioreg_desc)[(index)]) |= (dat))
#define  ioandbufl( ioreg_desc, index, dat )  (((ioreg_desc)[(index)]) &= (dat))
#define  ioxorbufl( ioreg_desc, index, dat )  (((ioreg_desc)[(index)]) ^= (dat))

/* No iohw.h iogroup(..) support provided in this implementation */
#define  iogroup_acquire( iogroup_defignator ) { /* Nothing */ }
#define  iogroup_map( iogroup_defignator, iogroup_defignator ) { /* Nothing */ }

/*
   Map the IOTester specific functions, normally defined by iotester.h, to empty
   definitions during target mode compilation (no code generation)
*/
#define  iot_rd( ioreg_desc )      0
#define  iot_wr( ioreg_desc, dat)  { /* Nothing */ }
#define  iot_or( ioreg_desc, dat)  { /* Nothing */ }
#define  iot_and(ioreg_desc, dat)  { /* Nothing */ }
#define  iot_xor(ioreg_desc, dat)  { /* Nothing */ }

#define  iot_rdbuf( ioreg_desc, index)      0
#define  iot_wrbuf( ioreg_desc, index, dat)  { /* Nothing */ }
#define  iot_orbuf( ioreg_desc, index, dat)  { /* Nothing */ }
#define  iot_andbuf(ioreg_desc, index, dat)  { /* Nothing */ }
#define  iot_xorbuf(ioreg_desc, index, dat)  { /* Nothing */ }

#define  iotbuf_wr( ioreg_desc, index, adr_inc, ptr, length, size) { /* Nothing */ }
#define  iotbuf_rd( ioreg_desc, index, adr_inc, ptr, length, size) { /* Nothing */ }
#define  iot_sync( dat )             { /* Nothing */ }
#define  iot_set_callback( no, vek ) { /* Nothing */ }
#define  iot_device_intr_init( dev, vek, vekno ) { /* Nothing */ }
#define  iot_device_exit()  { /* Nothing */ }
#define  iot_intr_poll()    { /* Nothing */ }
#define  iot_intr_enable(e) { /* Nothing */ }
#define  iot_print_message(str) { /* Nothing */ }
#define  iot_device_get_serialnumber( dev_num ) 0
#define  iot_device_get_serialnumber_list(serlist, maxsize) 0
#define  iot_init_interrupt(phys_device, ptr, no ) 0
#define  iot_init(phys_device) 0
#define  iot_init_device(phys_device, devno) 0
#define  iot_close_device(devno)
#define  iot_is_connected(serial_number) -3

#ifdef __cplusplus
   }
#endif

/**** target mode part ***/

#endif

#endif /* IOHW_H */

