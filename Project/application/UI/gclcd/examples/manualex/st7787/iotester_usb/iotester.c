/**********************************************************************/
/*                                                                    */
/*   iotester.c      for IOTester USB                                 */
/*                                                                    */
/*   Initialization and termination of the iotester function.         */
/*                                                                    */
/*   Include this module in PC projects that uses the IOTester-USB    */
/*                                                                    */
/*   It might be necessary to disable precompiled headers for this    */
/*   module in some C++ configurations.                               */
/*                                                                    */
/*   Do not include this module in the target compiler project        */
/*                                                                    */
/*   Revision date:                                                   */
/*   Revision Purpose:  .                                             */
/*                                                                    */
/*   Version number: 1.2                                              */
/*   Copyright (c) RAMTEX Engineering Aps 2010                        */
/*                                                                    */
/**********************************************************************/

#include <windows.h>
#include <iotester.h>
#include <stdio.h>  /* printf */

/*
   Message output function.
   IOTESTER messages from the DLL is routed to this callback function.
   The link to this function is initiated via iot_message_init.

   To disable all IOTESTER messages just comment out the body of this function.

   To reroute all IOTESTER messages to, for instance, another window or to a
   log file then comment out the printf statement and let this function call
   the appropiate output function instead,
   or define EXTERN_IOT_PRINT_MESSAGE in compiler switches and implement
   your own version of iot_print_message(..)
*/
#ifndef EXTERN_IOT_PRINT_MESSAGE
void __stdcall iot_print_message(const char *str)
   {
   printf("\n%s",str);  // Deliver message on stdout stream
   }
#endif

/* pointer for dynamic loaded library */
HINSTANCE  iot_usbiolib = 0;

/* Public function pointers for iot dynamic loaded library */
IOT_LONG_LONG     iot_rd;
IOT_LONGLONG      iot_wr;
IOT_LONGLONG      iot_and;
IOT_LONGLONG      iot_or;
IOT_LONGLONG      iot_xor;
IOT_LONG_LONGLONG iot_rdbuf;
IOT_LONGLONGLONG  iot_wrbuf;
IOT_LONGLONGLONG  iot_andbuf;
IOT_LONGLONGLONG  iot_orbuf;
IOT_LONGLONGLONG  iot_xorbuf;
IOTBUF            iotbuf_rd;
IOTBUF            iotbuf_wr;
IOT_CHAR_FUNCPTR  iot_set_callback;
IOT_CHAR_CHAR     iot_sync;

/* Local message interface */
static IOT_MESSPTR iot_message_init;

/* Local interface functions */
typedef unsigned char (*IOT_DEVSNLIST)(unsigned long *, unsigned char);
typedef int (*IOT_IS_CON)(unsigned long);
typedef int (*IOT_INIT_DEV)(unsigned long, unsigned char);
typedef int (*IOT_INTINSTALL)(unsigned long, IOT_FUNC_PTR, unsigned short);
typedef void (*IOT_VOID)(void);
static IOT_DEVSNLIST     dll_device_get_serialnumber_list;
static IOT_IS_CON        dll_is_connected;
static IOT_INIT_DEV      dll_init_device;
static IOT_INTINSTALL    iot_device_intr_init;
static IOT_VOID          iot_device_exit;
static IOT_INT_CHAR      dll_close_device;
static IOT_LONG_CHAR     dll_device_get_serialnumber;


/**********************************************************************

   iot_loadlibrary_if_req

   Internal function that load the iotester_usb.dll if it not has been done

   returns -1 if the load fails

**********************************************************************/

static int iot_loadlibrary_if_req(void)
   {
   if ( iot_usbiolib == 0 )
      iot_usbiolib = LoadLibrary( TEXT("iotester_usb"));

   if ( iot_usbiolib == 0)
      {
      iot_print_message("Error: IOTESTER_USB.DLL could not be loaded. Check installation");
      return IOT_DLL_ACCESS_FAULT;
      }
   return 0;
   }

/*
   Free DLL library connections
*/
static void iot_freelibrary(void)
   {
   // Close call back from library
   if (iot_message_init != NULL)
      {
      iot_message_init( NULL );
      iot_message_init = NULL;
      }
   // Close DLL vectors used by this module
   iot_usbiolib = NULL;
   iot_rd = NULL;
   iot_wr = NULL;
   dll_device_get_serialnumber = NULL;
   dll_device_get_serialnumber_list = NULL;
   dll_is_connected = NULL;
   dll_init_device = NULL;
   if (iot_usbiolib != NULL)
      {
      FreeLibrary( iot_usbiolib );
      iot_usbiolib = NULL;
      }
   }

/**********************************************************************

   iot_loadlibfunctions

   Internal function that loads the dll function pointers.

   returns -1 if the load fails

**********************************************************************/

static int iot_loadlibfunctions(void)
   {
   if ( iot_usbiolib == 0)
      return -1;

   /* get function pointer for public functions in dynamic loaded library */
   if (iot_rd == NULL)     iot_rd = (IOT_LONG_LONG)GetProcAddress( iot_usbiolib, "iot_rd" );
   if (iot_wr == NULL)     iot_wr = (IOT_LONGLONG)GetProcAddress( iot_usbiolib, "iot_wr" );
   if (iot_and == NULL)    iot_and = (IOT_LONGLONG)GetProcAddress( iot_usbiolib, "iot_and" );
   if (iot_or == NULL)     iot_or = (IOT_LONGLONG)GetProcAddress( iot_usbiolib, "iot_or" );
   if (iot_xor == NULL)    iot_xor = (IOT_LONGLONG)GetProcAddress( iot_usbiolib, "iot_xor" );
   if (iot_rdbuf == NULL)  iot_rdbuf = (IOT_LONG_LONGLONG)GetProcAddress( iot_usbiolib, "iot_rdbuf" );
   if (iot_wrbuf == NULL)  iot_wrbuf = (IOT_LONGLONGLONG)GetProcAddress( iot_usbiolib, "iot_wrbuf" );
   if (iot_andbuf == NULL) iot_andbuf = (IOT_LONGLONGLONG)GetProcAddress( iot_usbiolib, "iot_andbuf" );
   if (iot_orbuf == NULL)  iot_orbuf = (IOT_LONGLONGLONG)GetProcAddress( iot_usbiolib, "iot_orbuf" );
   if (iot_xorbuf == NULL) iot_xorbuf = (IOT_LONGLONGLONG)GetProcAddress( iot_usbiolib, "iot_xorbuf" );
   if (iotbuf_wr == NULL)  iotbuf_wr = (IOTBUF)GetProcAddress( iot_usbiolib, "iotbuf_wr" );
   if (iotbuf_rd == NULL)  iotbuf_rd = (IOTBUF)GetProcAddress( iot_usbiolib, "iotbuf_rd" );
   if (iot_sync == NULL)   iot_sync = (IOT_CHAR_CHAR)GetProcAddress( iot_usbiolib, "iot_sync" );
   if (iot_set_callback == NULL)      iot_set_callback = (IOT_CHAR_FUNCPTR)GetProcAddress( iot_usbiolib, "iot_set_callback" );
   if (iot_device_intr_init == NULL)  iot_device_intr_init = (IOT_INTINSTALL)GetProcAddress( iot_usbiolib, "iot_device_intr_init" );
   if (iot_device_exit == NULL)       iot_device_exit = (IOT_VOID)GetProcAddress( iot_usbiolib, "iot_device_exit" );
   /* get function pointer for functions in dynamic loaded library (used by this module only) */
   if (dll_device_get_serialnumber == NULL) dll_device_get_serialnumber = (IOT_LONG_CHAR)GetProcAddress( iot_usbiolib, "iot_device_get_serialnumber" );
   if (dll_device_get_serialnumber_list == NULL)  dll_device_get_serialnumber_list = (IOT_DEVSNLIST)GetProcAddress( iot_usbiolib, "iot_device_get_serialnumber_list" );
   if (dll_is_connected == NULL) dll_is_connected = (IOT_IS_CON)GetProcAddress( iot_usbiolib, "iot_is_connected" );
   if (dll_init_device == NULL)  dll_init_device  = (IOT_INIT_DEV)GetProcAddress( iot_usbiolib, "iot_init_device" );
   if (dll_close_device == NULL) dll_close_device = (IOT_INT_CHAR)GetProcAddress( iot_usbiolib, "iot_close_device" );
   if (iot_message_init == NULL)
      {
      iot_message_init = (IOT_MESSPTR)GetProcAddress( iot_usbiolib, "iot_message_init" );
      iot_message_init( iot_print_message ); /* Reroute DLL messages to local print function */
      }

   if (( iot_device_intr_init == NULL )            ||
      ( iot_device_exit == NULL )                  ||
      ( iot_rd == NULL )                           ||
      ( iot_wr == NULL )                           ||
      ( iot_and == NULL )                          ||
      ( iot_or == NULL )                           ||
      ( iot_xor == NULL )                          ||
      ( iot_rdbuf == NULL )                        ||
      ( iot_wrbuf == NULL )                        ||
      ( iot_andbuf == NULL )                       ||
      ( iot_orbuf == NULL )                        ||
      ( iot_xorbuf == NULL )                       ||
      ( iot_message_init == NULL )                 ||
      ( iotbuf_wr == NULL )                        ||
      ( iotbuf_rd == NULL )                        ||
      ( iot_sync == NULL )                         ||
      ( iot_set_callback == NULL )                 ||
      ( iot_device_get_serialnumber == NULL )      ||
      ( dll_device_get_serialnumber_list == NULL ) ||
      ( dll_is_connected == NULL )                 ||
      ( dll_init_device == NULL )                  ||
      ( dll_close_device == NULL )
      )
      {
      iot_freelibrary(); /* Some error. Release any ressouces partly allocated */
      iot_print_message("Error: Wrong IOTESTER_USB.DLL version. Check installation");
      return IOT_FUNC_ACCESS_FAULT;      /* cannot load the iotester.dll functions. Check the dll version */
      }

   /* use functions in dynamic loaded library */
   return 0;
   }


/**********************************************************************

   iot_device_get_serialnumber_list

   Initiates a list of serial numbers to all attached IOTESTER devices.
   The list can be made before initializing the IOTESTER devices and
   therefore be used for a specific initialization using iot_init_device().

   This list is made independent of the logical device numbers.

   serialnumberslist  Pointer array where this function inserts serial
                      numbers as unsigned long values.
                      (Note: IOTester serial numbers are hex numbers)

   maxlistsize        The maximum number of elements in the
                      serialnumberslist.

   return             The number of attached IOTESTERs up to maxlistsize.
                      Return 0 if no IOTesters found (or a driver error)

**********************************************************************/

unsigned char iot_device_get_serialnumber_list(unsigned long *serialnumberslist, unsigned char maxlistsize)
   {
   if (!dll_device_get_serialnumber_list)
      {
      if (iot_loadlibrary_if_req())
         return 0;

      dll_device_get_serialnumber_list = (IOT_DEVSNLIST)GetProcAddress( iot_usbiolib, "iot_device_get_serialnumber_list" );

      if (!dll_device_get_serialnumber_list)
         {
         iot_print_message("Error: Wrong IOTESTER_USB.DLL version. Check installation");
         return 0;
         }
      }

   return dll_device_get_serialnumber_list( serialnumberslist, maxlistsize );
   }

unsigned long iot_device_get_serialnumber( unsigned char dev_num )
   {
   if (dll_device_get_serialnumber)
      return dll_device_get_serialnumber( dev_num );
   return 0;
   }

/**********************************************************************

   iot_is_connected

   Poll for connection of an IOTESTER device with a specific serial number.
   This request can be made before initializing the IOTESTER devices.
   When used after the IOTESTER devices are initialised, this function
   returns the device number 0-3 where it is available or -1 if it is not
   an initialized device.

   Return parameter
      IOT_NO_DRIVER      -3   IOTester is not connected to PC (or missing drivers)
      IOT_NOT_CONNECTED  -2   IOTester is not connected to PC (or missing drivers)
      IOT_NOT_USED       -1   IOTester is connected to PC but not in use by any device
      IOT_DEVNUM_0        0   IOTester is connected and in use as device 0
      IOT_DEVNUM_1        1   IOTester is connected and in use as device 1
      IOT_DEVNUM_2        2   IOTester is connected and in use as device 2
      IOT_DEVNUM_3        3   IOTester is connected and in use as device 3

**********************************************************************/

int iot_is_connected(unsigned long serial_number)
   {
   if (!dll_is_connected)
      {
      if (iot_loadlibrary_if_req())
         return IOT_DLL_ACCESS_FAULT;

      dll_is_connected = (IOT_IS_CON)GetProcAddress( iot_usbiolib, "iot_is_connected" );

      if (!dll_is_connected)
         {
         iot_print_message("Error: Wrong IOTESTER_USB.DLL version. Check installation");
         return IOT_FUNC_ACCESS_FAULT;
         }
      }

   return dll_is_connected( serial_number );
   }

/**********************************************************************

   iot_init_device

   Initialize the IOTESTER device with the specified serial number and
   assign it to the specified device number 0-3.

   If the serial number is zero, the first found (lowest serial number)
   is assigned to the specified device number.

   The same IOTESTER cannot be assigned to multiple device numbers 0-3.

   serial_number:   The IOtester serialnumber (is a HEX number)
                    Use 0 for autofind (use IOTester with the lowest serial number)

   device_num:      The device number 0-3 to address the IOTESTER

**********************************************************************/

int iot_init_device(unsigned long serial_number, unsigned char device_num)
   {
   if (!dll_init_device)
      {
      if (iot_loadlibrary_if_req())
         return IOT_DLL_ACCESS_FAULT;

      if (iot_loadlibfunctions() != 0)
         return IOT_FUNC_ACCESS_FAULT;
      }

   return dll_init_device( serial_number, device_num );
   }

/*********************************************************************

   iot_init( serial_number ) is a macro which maps to this function.

   Call iot_init(..) function to initialize the iot_... functions and establish
   connection to the IOTester(s)

   serial_number:   The IOtester serialnumber (is a HEX number)
                    Use 0 for autofind (use IOTester with the lowest serial number)

   The following paramters is for IOTESTER-LPT backward compatibility.
   intrfunc         Assign one interupt vector. Use NULL if no interrupt vector is assigned here.
   irq_num          Interrupt vector number.    Use 0 if no interrupt vector is used.

   returns: Error status, see iotester.h, zero on succes.
*********************************************************************/

int iot_init_interrupt(unsigned long serial_number, IOT_FUNC_PTR intrfunc, unsigned short irq_num)
   {
   if (iot_loadlibrary_if_req())
      return IOT_DLL_ACCESS_FAULT;      /* cannot load the iotester.dll, check the installation and path */

   if (iot_loadlibfunctions() != 0)
      return IOT_FUNC_ACCESS_FAULT;

   /* Initialize IOTESTER device(s) */
   return iot_device_intr_init( serial_number, intrfunc, irq_num );
   }

/*********************************************************************

   iot_close_device

   Close the connection to the IOTESTER at the specified device number.

   Other IOTESTER devices continue unaffected.

   When this call closes the last IOTESTER device connection, the
   library and hardware ressources are released.

   device_num:      The device number 0-3 to address the IOTESTER

   returns:         Error status, see iotester.h, zero on succes.

/*********************************************************************/

int iot_close_device(unsigned char device_num)
   {
   int retval = 0;

   if (iot_usbiolib != NULL)
      {
      if (dll_close_device != NULL)
         retval = dll_close_device(device_num);

      if (retval < 0)
         {                          /* all devices are closed */
         retval += 100;
         iot_freelibrary();
         }
      }
   return retval;
   }


/*********************************************************************

   iot_exit

   Call  this  function  to release library and hardware resources
   before  exiting  program  execution,  do  not  call any iot_...
   function after this call.

/*********************************************************************/

void iot_exit(void)
   {
   if (iot_usbiolib != NULL)
      {
      if (iot_device_exit != NULL)
         iot_device_exit();
      iot_freelibrary();
      }
   }

/*
   Set wait states for external bus, i.e. extend the /RD and /WR pulse widths
   0 is the fastest, 15 is the slowest.

   In future designs write directly to the IOT_WAIT_STATES register.

   (Only for compatibility with IOTESTER_LPT, is not described in the documentation)
*/
void iot_set_buswait(unsigned char bus_wr_waitstates, unsigned char bus_rd_waitstates)
   {
   if (iot_wr != NULL)
      iot_wr(IOT_WAIT_STATES, ((((unsigned long) bus_wr_waitstates) << 8) | bus_rd_waitstates));
   }


/*
   Poll IOTESTER interrupt request status for interrupt line (J1,pin 1)
   Test for interrrupt pending on target
   Returns the intrrupt number 1-n if there is an interrupt
   Returns 0 if no interrupts

   In future designs read directly from the IOT_INTREG register.

   (Only for compatibility with IOTESTER_LPT, is not described in the documentation)
*/
unsigned char iot_intr_poll(void)
   {
   unsigned long retval;
   if (iot_rd  == NULL)
      return 0;
   // Convert request to a IOTESTER_USB command
   retval = iot_rd(IOT_INTREQ);
   return (retval & IOTIE_IRQ) ? 1 : 0;
   }

/*
   Global enable/disable of IOTESTER interrupt irq requests.
   (For interrupt line (J1,pin 1))

   Returns the previous interrupt enable status.
   Reset default is disabled

   In future designs write directly to the IOT_INTENA_SET and/or IOT_INTENA_CLR register.

   (Only for compatibility with IOTESTER_LPT, is not described in the documentation)
*/
unsigned char iot_intr_enable(unsigned char enable)
   {
   unsigned long retval;
   if ((iot_rd  == NULL) || (iot_wr == NULL))
      return 0;

   // Convert request to IOTESTER_USB commands
   retval = iot_rd(IOT_INTENA_SET);
   if (enable)
      iot_wr(IOT_INTENA_SET, (IOTIE_IRQ|IOTIE_GLOBAL));
   else
      iot_wr(IOT_INTENA_CLR, (IOTIE_IRQ|IOTIE_GLOBAL));
   return ((retval & (IOTIE_IRQ|IOTIE_GLOBAL))==(IOTIE_IRQ|IOTIE_GLOBAL)) ? 1 : 0;
   }


