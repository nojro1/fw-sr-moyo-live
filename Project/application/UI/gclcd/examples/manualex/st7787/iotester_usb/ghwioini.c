/************************* GHWIOINI.C ******************************

   Inittialize and deinitialize target specific I/O resources.

   When should it be used:
   When the IOTESTER tool is used this file should replaces the
   GHWIOINI.C file provided with the LCD/OLED driver libraries.
   This GHWIOINI.C file is prepared for the IOTESTER tool for use
   with LCD/OLED displays tested from a PC program running on the PC.
   Note the compiler switch IOTESTER must be defined in the compilers
   project preprocessor settings.

   Specific IOTESTER operations:
   The IOTESTER device is located on an USB port and initialized
   for external bus emulation. Then the LCD reset line is activated.
   Hereafter the LCD display is ready of read and write access.

   Revision date:     30-09-05
   Revision Purpose:  Extended address bus and word mode setup added.

   Revision date:     24-06-10
   Revision Purpose:  Adapted for IOTester-USB dual bus mode

   Version number: 1.10
   Copyright (c) RAMTEX Engineering Aps 2004-2010

*********************************************************************/
#ifndef GHW_NOHDW
//#include <sgio.h>      /* Portable I/O functions + hardware port def */
#endif
#include <gdisphw.h>

#ifdef IOTESTER_USB
#include <iotester.h>
#endif
#ifdef GHW_SINGLE_CHIP
void sim_reset( void );
#endif


#ifdef GBASIC_INIT_ERR
/*
   ghw_io_init()

   This function is called once by ginit() via ghw_init() before any LCD
   controller registers is addressed. Any target system specific
   initialization like I/O port initialization can be placed here.

*/
void ghw_io_init(void)
   {
   #ifndef GHW_NOHDW
   #ifdef IOTESTER_USB
   if (!iot_init(IOT_LPTSEEK))
      {
      unsigned long t2;

      /* Select bus mode */
      #ifdef GHW_BUS16
      // 16 bit bus on J1
      iot_wr( IOT_PIN_MODE_REG, DBUS16|DBUS8080|ADRBUSSIZE(1));
      #else
      // 8 bit bus on J1 (on lsb part of 16 bit bus)
      iot_wr( IOT_PIN_MODE_REG, DBUS8 |DBUS8080|ADRBUSSIZE(1));
      #endif

      /* Activate reset line (negative pulse) */
      iot_wr(IOT_IOCLR_REG1, ~IOT_RESET_BIT);  // Clear reset bit low

      // Wait 100 ms
      t2 = iot_rd(IOT_TIMESTAMP) + 100;
      while( iot_rd(IOT_TIMESTAMP) < t2);

      iot_wr(IOT_IOSET_REG1,  IOT_RESET_BIT);  // Set reset bit high

      // Wait 100 ms
      t2 = iot_rd(IOT_TIMESTAMP) + 100;
      while( iot_rd(IOT_TIMESTAMP) < t2);

      /* Use IOT_SYNC_NORMAL (default) when doing I/O level debugging,
         Use of IOT_SYNC_FAST is recommended only when not debugging on port access level.
         Explanation:
         Outputs in IOT_SYNC_FAST mode are approx 10 times faster than IOT_SYNC_NORMAL mode,
         but has the limitation that a debugger break point may stop the PC USB communication
         pipeline before the PC program states / I/O hardware states are in sync
         (i.e. some output has not yet been send over the USB bus to the hardware)
         Read about iot_sync(..) in the IOTester manual. */

      iot_sync(IOT_SYNC_NORMAL); /* Default, easiest debug */
      /* iot_sync(IOT_SYNC_FAST); */ /* fastest output */
      }
   else
      glcd_err = 1;
   #endif

   #ifdef GHW_SINGLE_CHIP
   sim_reset();  /* Initiate LCD bus simulation ports */
   #endif

   /* Insert required target specific code here, if any */

   /* Set LCD reset line /RST active low   (if /RST is connected to a port bit) */
   /* Set LCD reset line /RST passive high (if /RST is connected to a port bit) */
   #endif
   }

/*
  This function is called once by gexit() via ghw_exit() as the last operation
  after all LCD controller operations has stopped.
  Any target system specific de-initialization, like I/O port deallocation
  can be placed here. In most embedded systems this function can be empty.
*/
void ghw_io_exit(void)
   {
   #ifndef GHW_NOHDW
   /* Insert required code here, if any */
   #ifdef IOTESTER_USB
   iot_exit();
   #endif
   #endif
   }

#endif /* GBASIC_INIT_ERR */

