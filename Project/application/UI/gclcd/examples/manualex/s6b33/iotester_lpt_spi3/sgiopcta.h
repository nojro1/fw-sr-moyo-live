/***********************************************************************
 *
 * STIMGATE I/O port definition for the target system.
 *
 *   Target processor CPU family : IOTESTER
 *   Target processor device     : IOTESTER
 *   Target compiler             : PCMODE
 *
 * This file is included by SGIO.H to preprocess the sgxxx() port functions
 * when the C source file is compiled for execution in the PC.
 *
 * The file can be generated with the SGSETUP program.
 * SGSETUP use this file for back-annotation and to generate a corresponding
 * file: SGIO_TA.H for the target C-compiler.
 * NOTE : Do not modify this file directly, unless you are absolutely sure
 * of what you are doing.
 *
 * V3.03  STIMGATE Copyright (c) RAMTEX International 2007
 *
 **********************************************************************/

 /* 
    No extra I/O port definitions is needed. 
    Uses the intenal IOTESTER register definitions in IOTESTER.H 
 */
