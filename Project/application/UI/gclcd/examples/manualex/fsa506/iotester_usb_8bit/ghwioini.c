/************************* GHWIOINI.C ******************************

   Inittialize and deinitialize target specific I/O resources.

   When should it be used:
   When the IOTESTER_USB tool is used this file should replaces the
   GHWIOINI.C file provided with the LCD/OLED driver libraries.
   This GHWIOINI.C file is prepared for the IOTESTER_USB tool for use
   with LCD/OLED displays tested from a PC program running on the PC.

   The compiler switch IOTESTER_USB must be defined in the compiler
   project preprocessor settings.

   Specific IOTESTER operations:
   The IOTESTER device is located on a USB port and initialized
   for external bus emulation. Then the LCD reset line is activated.
   Hereafter the LCD display is ready for read and write access.

   Version number: 1.1
   Copyright (c) RAMTEX Engineering Aps 2010

*********************************************************************/
#ifndef GHW_NOHDW
//#include <sgio.h>      /* Portable I/O functions + hardware port def */
#endif
#include <gdisphw.h>

#ifdef IOTESTER_USB
#include <iotester.h>
#endif
#ifdef GHW_SINGLE_CHIP
void sim_reset( void );
#endif

#ifdef GBASIC_INIT_ERR
/*
   ghw_io_init()

   This function is called once by ginit() via ghw_init() before any LCD
   controller registers is addressed. Any target system specific
   initialization like I/O port initialization can be placed here.

*/
void ghw_io_init(void)
   {
   #ifndef GHW_NOHDW
   #ifdef IOTESTER_USB
   if (!iot_init(0))
      {
      unsigned long t1;
      /* Select bus mode */

      /* I/O mode (single chip simulation mode) */
      //iot_wr( IOT_PIN_MODE_REG, IOT_MODE_IO);                   // 45 port pins

      /* 8 bit bus modes examples */
      //iot_wr( IOT_PIN_MODE_REG, DBUS8 |DBUS8080|ADRBUSSIZE(1));  //  1 address,   RD/WR + 32 I/O
      //iot_wr( IOT_PIN_MODE_REG, DBUS8 |DBUS8080|ADRBUSSIZE(24)); // 24 addresses, RD/WR +  16 I/O
      //iot_wr( IOT_PIN_MODE_REG, DBUS8 |ADRBUSSIZE(1));           //  2 addresses, Eclk+R/W + 23 I/O
      //iot_wr( IOT_PIN_MODE_REG, DBUS8 |ADRBUSSIZE(24));          // 24 addresses, Eclk+R/W + 16 I/O

      /* 16 bit bus modes examples */
      //iot_wr( IOT_PIN_MODE_REG, DBUS16 |DBUS8080|ADRBUSSIZE(1));  //  1 addresses, /RD+/WR + 32 I/O
      //iot_wr( IOT_PIN_MODE_REG, DBUS16 |DBUS8080|ADRBUSSIZE(24)); // 24 addresses, /RD+/WR +  8 I/O
      //iot_wr( IOT_PIN_MODE_REG, DBUS16 |ADRBUSSIZE(1));           //  2 addresses, Eclk+R/W + 23 I/O
      //iot_wr( IOT_PIN_MODE_REG, DBUS16 |ADRBUSSIZE(24));          // 24 addresses, Eclk+R/W +  8 I/O

      /* 8 bit alternative bus modes examples */
      iot_wr( IOT_PIN_MODE_REG, ALTBUS|DBUS8|DBUS8080|ADRBUSSIZE(8));  // 8 addresses on J1, RD/WR + 25 I/O
      //iot_wr( IOT_PIN_MODE_REG, ALTBUS|DBUS8|ADRBUSSIZE(8));           // 8 addresses on J1, Eclk+R/W + 25 I/O

      //iot_wr( IOT_WAIT_STATES , IOT_RDDLY(2) | IOT_WRDLY(2));

      /* Activate reset line */
      iot_wr(IOT_IODIRSET_REG1, IOT_RESET_BIT);    // Set reset line to output
      //iot_wr(IOT_IOSET_REG1,  IOT_RESET_BIT);  // Set reset bit high
      iot_wr(IOT_IOCLR_REG1, ~IOT_RESET_BIT);    // Clear reset bit low

      // Wait 100 ms
      t1 = iot_rd(IOT_TIMESTAMP) + 100;
      while( iot_rd(IOT_TIMESTAMP) < t1);

      iot_wr(IOT_IOSET_REG1,IOT_RESET_BIT);  // Set reset bit high
      //iot_wr(IOT_IOCLR_REG1,~IOT_RESET_BIT);  // Clear reset bit low

      // Wait 100 ms
      t1 = iot_rd(IOT_TIMESTAMP) + 100;
      while( iot_rd(IOT_TIMESTAMP) < t1);

      /* Use IOT_SYNC_NORMAL (default) when doing I/O level debugging,
         Use of IOT_SYNC_FAST is recommended only when not debugging on port access level.
         Explanation:
         Outputs in IOT_SYNC_FAST mode are approx 10 times faster than IOT_SYNC_NORMAL mode,
         but has the limitation that a debugger break point may stop the PC USB communication
         pipeline before the PC program states / I/O hardware states are in sync
         (i.e. some output has not yet been send over the USB bus to the hardware)
         Read about iot_sync(..) in the IOTester manual. */

      /*iot_sync(IOT_SYNC_NORMAL);*/ /* Easiest driver level debug (auto sync after each output) */
      iot_sync(IOT_SYNC_FAST);       /* Fastest display driver output (high-level function do sync) */
      }
   else
      glcd_err = 1;
   #endif

   #ifdef GHW_SINGLE_CHIP
   sim_reset();  /* Initiate LCD bus simulation ports */
   #endif

   /* Insert required target specific code here, if any */

   /* Set LCD reset line /RST active low   (if /RST is connected to a port bit) */
   /* Set LCD reset line /RST passive high (if /RST is connected to a port bit) */
   #endif
   }

/*
  This function is called once by gexit() via ghw_exit() as the last operation
  after all LCD controller operations has stopped.
  Any target system specific de-initialization, like I/O port deallocation
  can be placed here. In most embedded systems this function can be empty.
*/
void ghw_io_exit(void)
   {
   #ifndef GHW_NOHDW
   /* Insert required code here, if any */
   #ifdef IOTESTER_USB
   iot_exit();
   #endif
   #endif
   }

#endif /* GBASIC_INIT_ERR */

