/***********************************************************************
 *
 * This file contains I/O port definition for the target system and is
 * included by sgio.h
 *
 * This file is normally generated automatically by the SGSETUP tool using
 * the sgio_pc.h header file as a template. The definitions here correspond
 * to I/O port definitions in sgio_pc.h but are here using the I/O register
 * definition syntax for the target compiler.
 *
 * Modify these definitions so they fit the actual target system and the
 * I/O register definition syntax used by the actual target compiler.
 *
 * Copyright (c) RAMTEX International 2006
 * Version 1.0
 *
 **********************************************************************/

/* The example definitions here assume that the LCD I/O registers are
 * memory mapped at fixed addresses, and that the register SELECT line on
 * the display is connected to address bus bit 0 signal for 8 bit bus mode
 * bit 1 for 16 bit bus mode, and bit 1 for 32 bit bus mode
 */

/* Used only in GHW_BUS8 mode */
#define GHWWR  (* (SGUCHAR volatile *) ( 0x0001 ))
#define GHWRD  (* (SGUCHAR volatile *) ( 0x0001 ))
#define GHWSTA (* (SGUCHAR volatile *) ( 0x0000 ))
#define GHWCMD (* (SGUCHAR volatile *) ( 0x0000 ))

/* Used only in GHW_BUS16 mode */
#define GHWWRW  (* (SGUINT volatile *) ( 0x0002 ))
#define GHWRDW  (* (SGUINT volatile *) ( 0x0002 ))
#define GHWSTAW (* (SGUINT volatile *) ( 0x0000 ))
#define GHWCMDW (* (SGUINT volatile *) ( 0x0000 ))

/* Used only in GHW_BUS32 mode */
#define GHWWRDW  (* (SGULONG volatile *) ( 0x0004 ))
#define GHWRDDW  (* (SGULONG volatile *) ( 0x0004 ))
#define GHWSTADW (* (SGULONG volatile *) ( 0x0000 ))
#define GHWCMDDW (* (SGULONG volatile *) ( 0x0000 ))



