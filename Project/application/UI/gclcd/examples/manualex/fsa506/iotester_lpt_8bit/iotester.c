/**********************************************************************/
/*                                                                    */
/*   iotester.c                                                       */
/*                                                                    */
/*   Initialization and termination of the iotester function.         */
/*                                                                    */
/*   Include this module in your project that uses the lpt connected  */
/*   iotester.                                                        */
/*                                                                    */
/*   It might be necessary to disable precompiled headers for this    */
/*   module in some C++ configurations.                               */
/*                                                                    */
/*   Revision date:     09-09-05                                      */
/*   Revision Purpose:  Added support for IOTESTER hardware ver 4.    */
/*                      - 20 bit address bus mode                     */
/*                      - 16 bit databus mode                         */
/*                      - Target interrupt support                    */
/*                      - Message call back function                  */
/*                                                                    */
/*   Version number: 4                                                */
/*   Copyright (c) RAMTEX Engineering Aps 1998-2005                   */
/*                                                                    */
/**********************************************************************/

#include <windows.h>
#include "iotester.h"
#include <stdio.h>  /* printf */

/*
   Message output function.
   IOTESTER messages from the DLL is routed to this callback function.
   The link to this function is initiated via iot_message_init.

   To disable all IOTESTER messages just comment out the body of this function.

   To reroute all IOTESTER messages to, for instance, another window or to a
   log file then comment out the printf statement and let this function call
   the appropiate output function instead.
*/
static void __stdcall iot_print_message(const char *str)
   {
   printf("\n%s",str);  // Deliver message on stdout stream
   }


/* pointer for dynamic loaded library */
static HINSTANCE  h_lptiolib = NULL;   //could be HINSTANCE type instead of HMODULE
/* function pointers for iot dynamic loaded library */
/* could happen non-extern with extern declarations in iotester.h */
IOT_INTINSTALL    iot_device_intr_init;
IOT_VOID          iot_device_exit;
IOT_CHARCHAR      iot_set_buswait;
IOT_LONG_LONG     iot_rd;
IOT_LONGLONG      iot_wr;
IOT_LONGLONG      iot_and;
IOT_LONGLONG      iot_or;
IOT_LONGLONG      iot_xor;
IOT_LONG_LONGLONG iot_rdbuf;
IOT_LONGLONGLONG  iot_wrbuf;
IOT_LONGLONGLONG  iot_andbuf;
IOT_LONGLONGLONG  iot_orbuf;
IOT_LONGLONGLONG  iot_xorbuf;
IOT_CHAR_CHAR     iot_intr_enable;
IOT_CHAR          iot_intr_poll;
IOT_INTCHAR       iot_outportb;
IOT_INT_SH        iot_inportb;

/* Local message interface */
typedef void (__stdcall * IOT_FUNC_CHAR_PTR)(const char *);
typedef int (*IOT_MESSPTR)( IOT_FUNC_CHAR_PTR );
static IOT_MESSPTR iot_message_init;

/**********************************************************************/
/*                                                                    */
/*   iot_init                                                         */
/*                                                                    */
/*   Call this function to initialize the iot_... functions.          */
/*                                                                    */
/*   lptport:  The  address  of  the  lpt centronics port, use 0 for  */
/*   autofind.                                                        */
/*                                                                    */
/*   returns: Error status, see iotester.h, zero on succes.           */
/**********************************************************************/

int iot_init_interrupt(unsigned short lptport, IOT_FUNC_PTR intrfunc, unsigned short irq_num)
   {
   if ( h_lptiolib != NULL )
      return IOT_OK;

   h_lptiolib = LoadLibraryA( "iotester.dll" );

   if ( h_lptiolib == NULL )
      return IOT_DLL_ACCESS_FAULT;      /* cannot load the iotester.dll, check the installation and path */

   /* get function pointer in dynamic loaded library */

   if ((( iot_device_intr_init = (IOT_INTINSTALL)GetProcAddress( h_lptiolib, "iot_device_intr_init" )) == NULL ) ||
       (( iot_device_exit = (IOT_VOID)GetProcAddress( h_lptiolib, "iot_device_exit" )) == NULL )     ||
       (( iot_set_buswait = (IOT_CHARCHAR)GetProcAddress( h_lptiolib, "iot_set_buswait" )) == NULL ) ||
       (( iot_rd = (IOT_LONG_LONG)GetProcAddress( h_lptiolib, "iot_rd" )) == NULL )                  ||
       (( iot_wr = (IOT_LONGLONG)GetProcAddress( h_lptiolib, "iot_wr" )) == NULL )                   ||
       (( iot_and = (IOT_LONGLONG)GetProcAddress( h_lptiolib, "iot_and" )) == NULL )                 ||
       (( iot_or = (IOT_LONGLONG)GetProcAddress( h_lptiolib, "iot_or" )) == NULL )                   ||
       (( iot_xor = (IOT_LONGLONG)GetProcAddress( h_lptiolib, "iot_xor" )) == NULL )                 ||
       (( iot_rdbuf = (IOT_LONG_LONGLONG)GetProcAddress( h_lptiolib, "iot_rdbuf" )) == NULL )        ||
       (( iot_wrbuf = (IOT_LONGLONGLONG)GetProcAddress( h_lptiolib, "iot_wrbuf" )) == NULL )         ||
       (( iot_andbuf = (IOT_LONGLONGLONG)GetProcAddress( h_lptiolib, "iot_andbuf" )) == NULL )       ||
       (( iot_orbuf = (IOT_LONGLONGLONG)GetProcAddress( h_lptiolib, "iot_orbuf" )) == NULL )         ||
       (( iot_xorbuf = (IOT_LONGLONGLONG)GetProcAddress( h_lptiolib, "iot_xorbuf" )) == NULL )       ||
       (( iot_intr_enable = (IOT_CHAR_CHAR)GetProcAddress( h_lptiolib, "iot_intr_enable" )) == NULL )||
       (( iot_intr_poll = (IOT_CHAR)GetProcAddress( h_lptiolib, "iot_intr_poll" )) == NULL )         ||
       (( iot_message_init = (IOT_MESSPTR)GetProcAddress( h_lptiolib, "iot_message_init" )) == NULL )||

       (( iot_outportb = (IOT_INTCHAR)GetProcAddress( h_lptiolib, "iot_outportb" )) == NULL )        ||
       (( iot_inportb = (IOT_INT_SH)GetProcAddress( h_lptiolib, "iot_inportb" )) == NULL ))
      {
      FreeLibrary( h_lptiolib );
      h_lptiolib = NULL;
      return IOT_FUNC_ACCESS_FAULT;      /* cannot load the iotester.dll functions check the dll version */
      }

   /* use functions in dynamic loaded library */

   /* Reroute DLL messages to local print function */
   iot_message_init( iot_print_message );

   /* Initialize IOTESTER device(s) */
   return iot_device_intr_init( lptport, intrfunc, irq_num );
   }


/**********************************************************************/
/*                                                                    */
/*   iot_exit                                                         */
/*                                                                    */
/*   Call  this  function  to release library and hardware resources  */
/*   before  exiting  program  execution,  do  not  call any iot_...  */
/*   function after this call.                                        */
/**********************************************************************/

void iot_exit(void)
   {
   if (h_lptiolib != NULL)
      {
      if (iot_device_exit != NULL)
         iot_device_exit();

      if (iot_message_init != NULL)
         iot_message_init( NULL );

      FreeLibrary( h_lptiolib );
      h_lptiolib = NULL;
      }
   }

