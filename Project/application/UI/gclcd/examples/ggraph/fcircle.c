/*
   Draw circle.
   This functions uses integer coordinates so the center optionally
   can be placed outside the current viewport (and screen).

   This implementation does not use the math.h library or the
   trigometric functions and is therefore relative fast.

   05-05-2008 A similar functionality is now included in the 
              standard library function gcircle(..)
*/
#include <ggraph.h>
#include <gdisp.h>

static int xe, ye;
static SGUCHAR corners;

static void setpixel(int x, int y)
   {
   if ((x < 0)  ||
       (x > xe) ||
       (y < 0)  ||
       (y > ye))
       return; /* point is outside viewport */

   /* point is inside viewport, draw it */
   gsetpixel( (GXT) x, (GYT) y, 1);
   }

/*
  Draw mirror points
  (reduces calculations to a 0-45 degree arc)
*/
static void plotarcs(int xc, int yc, int x1, int y1)
   {
   if (corners & G_ARC_UR)
      {
      setpixel( xc+x1, yc-y1);
      setpixel( xc+y1, yc-x1);
      }
   if (corners & G_ARC_UL)
      {
      setpixel( xc-x1, yc-y1);
      setpixel( xc-y1, yc-x1);
      }
   if (corners & G_ARC_BR)
      {
      setpixel( xc+x1, yc+y1);
      setpixel( xc+y1, yc+x1);
      }
   if (corners & G_ARC_BL)
      {
      setpixel( xc-x1, yc+y1 );
      setpixel( xc-y1, yc+x1 );
      }
   }

/*
   Fast draw circle.
   This functions uses integer coordinates so the center optionally
   can be placed outside the current viewport (and screen).
*/
void fcircle(int xc, int yc, int r)
   {
   int x, y;
   double p;

   /* Set viewport size limits in advance to speed drawing */
   xe = (int)((unsigned int) ggetvpw()-1);
   ye = (int)((unsigned int) ggetvph()-1);
   corners = (SGUCHAR) (G_ARC_UL | G_ARC_UR | G_ARC_BL | G_ARC_BR);

   x = 0;
   y = r;
   p = (double)(1 - r);

   /* Plot on axis */
   plotarcs(xc,yc,x,y);

   /* Plot a 45 angle, and mirror the rest */
   while(x < y)
      {
      if(p < 0.0)
         {
         x = x + 1;
         p = p + (double) (2 * x + 1);
         }
      else
         {
         x = x + 1;
         y = y - 1;
         p = p + (double)(2*(x - y) + 1);
         }
      plotarcs(xc,yc,x,y);
      }
   }
