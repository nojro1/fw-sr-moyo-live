/*
   Graphic support functions
*/
#ifndef GGRAPH_H
#define GGRAPH_H


#ifdef __cplusplus
extern "C" {
#endif

void fcircle(int xc, int yc, int r);

#define G_ARC_UL 0x1
#define G_ARC_UR 0x2
#define G_ARC_BL 0x4
#define G_ARC_BR 0x8


#ifdef PI
  #undef PI
#endif
#define  PI 3.1415926535

/*
   Detect if 3 points 0,1,2 defines either
      A courter-clock-wise rotation   Return  1
      A clock-wise rotation           Return -1
      Is lying on a straight line     Return 0
*/
int ccw(int x0,int y0,  /* Point 0 */
        int x1,int y1,  /* Point 1 */
        int x2,int y2); /* Point 2 */

/*
   Check if two line segments intersect
   (i.e. if the two lines either crosses each other or the endpoint of
    one line lays on the other line)
*/
int intersect( int xb1,int yb1, int xe1,int ye1,   /* Line segment 1 */
               int xb2,int yb2, int xe2,int ye2 ); /* Line segment 2 */

/* Swap two points of type int */
#define SWAP_POINTS( x1,y1, x2,y2 ) \
      { \
      int t;  \
      t = x1; \
      x1 = x2;\
      x2 = t; \
      t = y1; \
      y1 = y2;\
      y2 = t; \
      }

#ifdef __cplusplus
}
#endif

#endif  /* GGRAPH_H */

