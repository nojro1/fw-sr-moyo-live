#include <ggraph.h>
/*
   Check if two line segments intersect
   (i.e. if the two lines either cross each other or one endpoint of
    one line lays on the other line)
*/
int intersect( int xb1,int yb1, int xe1,int ye1,   /* Line segment 1 */
               int xb2,int yb2, int xe2,int ye2)   /* Line segment 2 */
   {
   /* If both end points of each line have different ccw values of
      the other, then the lines must intersect. See ccw.c */
   return ((ccw( xb1,yb1, xe1,ye1, xb2,yb2 ) * ccw( xb1,yb1, xe1,ye1, xe2,ye2 )) <= 0) &&
           (ccw( xb2,yb2, xe2,ye2, xb1,yb1 ) * ccw( xb2,yb2, xe2,ye2, xe1,ye1 )) <= 0);
   }

