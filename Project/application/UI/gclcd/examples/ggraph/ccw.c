#include <ggraph.c>

/*
   Detect if 3 points 0,1,2 defines either
      a courter-clock-wise rotation   Return  1
      a clock-wise rotation           Return -1
      or is lying on a straight line  Return 0
*/
int ccw(int x0,int y0,  /* Point 0 */
        int x1,int y1,  /* Point 1 */
        int x2,int y2)  /* Point 2 */
   {
   int dx1, dx2, dy1, dy2;
   /* Normalize relative to x0,y0 to minimize calculations */
   dx1 = x1 - x0;
   dy1 = y1 - y0;
   dx2 = x2 - x0;
   dy2 = y2 - y0;
   /* compare based on slopes of interconnecting lines */
   if (dx1*dy2 > dy1*dx2) return 1;
   if (dx1*dy2 < dy1*dx2) return -1;
   if ((dx1*dx2 < 0) || (dy1*dy2 < 0)) return -1;
   if ((dx1*dx1 + dy1*dy1) < (dx2*dx2 + dy2*dy2) return 1;
   return 0; /* points is on a straight line */
   }

