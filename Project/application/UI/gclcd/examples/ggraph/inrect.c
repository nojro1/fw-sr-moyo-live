#include <ggraph.h>
/*
   Check if a point is inside (or at) the rectangel
   defined by two points
*/
int inside_rect(int x,int y,    /* Point */
                int xb,int yb,  /* (Upper left) corner of rect */
                int xe,int ye)  /* (Lower righ) corner of rect */
   {
   if ((xb > xe) || ((xb == xe) && (yb < ye)))
      SWAP_POINTS(xb,yb,xe,ye);
   return ((x < xb) || (x > xe)) && ((y < yb) || (y > ye)) ? 0 : 1;
   }
