/*
   Interface function for virtual (external) memory

   The getvmem(..) function must be implemented by the library user
   for the specific type of storage device.

   This implementation use file storage to emulate / test the virtual font storage
   feaures and demonstrates how to make a software implementation which can dispatch
   between different access drivers for different memory types

*/
#include <getvmem.h>
#include <stdio.h>
#include <tstvfont1.h>  /* Defines the GFONTDEVICE identifier */
/* If more virtual font images are used, then add the virtual font header(s) here */

#ifdef GVIRTUAL_FONTS

static FILE *filep = NULL;

/*
   Get data from virtual memory

   buf      = destination pointer to RAM storage location
   index    = source index for data in virtual font to be loaded to RAM
   numbytes = number of bytes to load to buffer
*/
void getvmem(GFONTDEVICE vf_device, SGUCHAR *buf, GFONTBUFIDX index, SGUINT numbytes)
   {
   switch (vf_device)
      {
      case (TSTVFONT1_ID):
         // Use file load to simulate load from font
         if (filep == NULL)
            {
            printf("\nFile has not been opened");
            break;
            }
         if (buf == NULL)
            {
            printf("\nInvalid virtual font read buffer");
            break;
            }
         if (fseek( filep, index, SEEK_SET) == 0)
            fread( buf, 1, numbytes, filep);
         else
            printf("\nFont symbol load failed");
         break;
      /* Service cases for other storage devices can be inserted here */
      default:
         printf("\nUnknown font device");
         break;
      }
   }

/*
   Called by application before first use of virtual font
   Return  = 0 if no open errors
   Return != 0 if any open errors
*/
int getvmem_open( GFONTDEVICE vf_device )
   {
   switch (vf_device)
      {
      case (TSTVFONT1_ID):
          // Open file in binary mode
         if ((filep = fopen("tstvfont1.bin","rb")) != NULL)
            return 0;
         // Not in default directory. Try MSVC default debug directory.
         if ((filep = fopen("debug/tstvfont1.bin","rb")) != NULL)
            return 0;
         // Not in debug directory. Try MSVC relative path to example
         if ((filep = fopen("../../../../tstvfont1.bin","rb")) != NULL)
            return 0;
         // Not found. Try MSVC relative path to example
         if ((filep = fopen("../../../tstvfont1.bin","rb")) != NULL)
            return 0;
         // Giving up
         printf("\nImage file not found");
         break;

      /* Service cases for other storage devices can be inserted here */
      default:
         printf("\nUnknown font device");
      }
   return 1;
   }


/*
   Optionally called by application after last use of virtual font
*/
void getvmem_close( GFONTDEVICE vf_device )
   {
   switch (vf_device)
      {
      case (TSTVFONT1_ID):
         if (filep != NULL)
            {
            fclose(filep);
            filep = NULL;
            }
         break;
      /* Service cases for other storage devices can be inserted here */
      default:
         break;
      }
   }

#endif

