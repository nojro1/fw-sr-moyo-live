Test of virtual font support in external memory
===============================================
For details about use of Virtual Fonts you should read the application
note: gclcd/vfont/virtual_fonts.pdf

This example
------------
The example uses fonts in 2 storage locations

   Standard fonts in ROM
   Virtual fonts in external memory device image (tstvfont1)

A device id defined with the virtual font enables use of more than
one storage device. The use of this parameter is controlled by the
getvmem.c implementation for the given target system. 
In this examples the default (0) is used.

The example illustrates how the display driver interfaces
could be created and how to test the different use of fonts
- without and without code page
- symbols in fonts.
- with C strings and wchar_t strings.


Virtual Fonts used by example
-----------------------------
The example source code structures are prepared for using more than 
one virtual font storage device but only one is actually used.

tstvfont1 has device ID = 0 and contains
   vmono5_8,         /* Font with no codepage (from standard font) */
   vtimes13,         /* Font with code page (from standard font)*/
   vuni_16x16,       /* Larger font with codepage (from standard font)*/
   bwsym_64x64       /* Custom symbol font from .sym file*/
   logo              /* Custom symbol font from b&w bmp file */
   test_4bit         /* Custom symbol using 4 bit palette (gcolor_4.pal)*/
   test_8bit         /* Custom symbol using 8 bit rgb (8 bit bmp converted to rgb8 ) */
   test_24bit        /* Custom symbol using 16 bit rgb (24 bit bmp converted to rgb16 */


Generation of virtual font files
--------------------------------
The virtual font files are generated via the configuration script
file:
   testfonts\testfonts.vfg
Read the comments in this script file for details.

The virtual font configuration script file can be processed via the batch file 
   genvf.bat
It invokes the genvf.exe file as this command line
  ..\..\..\bin\genvf.exe testfonts.vfg

The output files are stored in this directory. The .bin file must be visible
from the project exe directory (must be copied to the exe directory)

---

RAMTEX International ApS (August 2008)

