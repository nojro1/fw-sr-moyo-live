/*********************************************************************

   This program demonstrates the use of virtual fonts.

   RAMTEX Engineering 2008-2009

*********************************************************************/
#include <gdisp.h>    /* Common to all LCD functions */
#include <gi_fonts.h> /* Prototype standard fonts    */
#ifdef SGPCMODE
#include <windows.h>  /* Sleep() */
#endif

#ifndef GVIRTUAL_FONTS
  #error This example assume that GVIRTUAL_FONTS is defined in gdispcfg.h
#endif

#ifdef GVIRTUAL_FONTS
/* Prototype virtual fonts */
#include <getvmem.h>
#include <tstvfont1.h>
#endif


static void wait(int ms)
   {
   #ifdef SGPCMODE
   Sleep(ms);
   #endif
   }

static void room_for_text(SGUCHAR lines)
   {
   if ((SGUINT)lines*ggetfh() + (SGUINT)ggetypos() > (SGUINT)ggetvph() ) /* Room for text ? */
      {
      wait(2);
      gclrvp(); /* If not start from top after a delay */
      }
   gsetmode(GWORD_WRAP | ggetmode()); /* Assure use of word wrap */
   }


/*
   Save copy of named viewport content on heap.
   Return heap pointer to be used with ghw_restoreblk(..)
   The pointer must be freed with free(..)
*/
SGUCHAR *save_vp_background( SGUCHAR vp )
   {
   SGUCHAR *p;
   GBUFINT size;
   size = (GBUFINT) ghw_blksize(gvpxl(vp),gvpyt(vp),gvpxr(vp),gvpyb(vp));
   if ((p = (SGUCHAR *) malloc(size)) != NULL)
      ghw_rdblk(gvpxl(vp),gvpyt(vp),gvpxr(vp),gvpyb(vp),p,size);
   return p;
   }

static void test_vfont1(void)
   {
   /* Use some virtual fonts */
   gsetmode(GWORD_WRAP);
   gselfont((PGFONT)&vmono5_8);
   room_for_text(2);
   gputs("\nVirtual font of mono5_8");
   wait(2000);
   gselfont((PGFONT)&vtimes13);
   room_for_text(2);
   gputs("\nvtimes13 = virtual font of times13");
   wait(2000);

   #ifdef GWIDECHAR
   gselfont((PGFONT)&vuni_16x16);
   room_for_text(2);
   gputsw(L"\nvuni_16x16 = Large font for wide char");
   #endif

   wait(2000);
   }

/*
   Show symbol in a one symbol font at the center of the screen
*/
static void show_symbol(PGFONT symfont)
   {
   /* Use a symbol in a virtual font */
   PGSYMBOL psym;
   GXT x;
   GYT y;
   psym = ggetfsym(0,symfont);
   /* Center symbol. Locate oversized symbols in upper left corner. */
   x = gsymw(psym);
   x = (x < ggetvpw()) ? (ggetvpw()-x)/2+1 : 0;
   y = gsymh(psym);
   y = (y < ggetvph()) ? (ggetvph()-y)/2+1 : 0;

   gclrvp();
   /* Show symbol */
   gputsym(x,y,psym);
   }

void show_rotating_sym(int sangle, int eangle, SGINT x, SGINT y, PGSYMBOL psym)
   {
   /* Show symbol */
   int i;
   for (i=sangle;i<=eangle;i+=3)
      {
      /* Show rotated text */
      gputsymrot(x,y, G_DEGREE_TO_RADIAN(i), psym, 0, 0, GALIGN_VCENTER|GALIGN_HCENTER);
      gsetupdate(GUPDATE_ON);
      /* Insert any delays here */
      if (i >= eangle)
         wait(300);
      else
         wait(1);
      /* Continue and delete previous rotated text */
      gsetupdate(GUPDATE_OFF); /* Avoid flicker. Only buffered mode */
      gputsymrot(x,y, G_DEGREE_TO_RADIAN(i), psym, 0, 0, GALIGN_VCENTER|GALIGN_HCENTER|GSYMCLR);
      }
   }

/*
   Show symbol in a one symbol font at the center of the screen
   Rotate the symbol 360 degrees in a number of steps. Pause when
   aligned to axis.
*/
static void show_symbol_rot(PGFONT symfont)
   {
   /* Use a symbol in a virtual font */
   PGSYMBOL psym;
   psym = ggetfsym(0,symfont);
   show_rotating_sym(  0,  90, ggetvpw()/2, ggetvph()/2, psym);
   show_rotating_sym( 90, 180, ggetvpw()/2, ggetvph()/2, psym);
   show_rotating_sym(180, 270, ggetvpw()/2, ggetvph()/2, psym);
   show_rotating_sym(270, 360, ggetvpw()/2, ggetvph()/2, psym);
   wait(1500);
   gsetupdate(GUPDATE_ON);
   }

static void show_transperant_grey(PGFONT symfont)
   {
   const GCOLOR palette[] = {G_RED,G_ORANGE,G_YELLOW,G_GREEN,G_CYAN,G_MAGENTA,G_WHITE};
   PGSYMBOL psym;
   GXT x,xe;
   GYT y;
   SGUINT c;
   SGUCHAR *background;

   psym = ggetfsym(0,symfont);
   gresetvp();
   gsetcolorb(G_WHITE);
   gclrvp();

   /* Create vertical colored bars */
   for (x=ggetvpw()/2-16,c=0;c<4;c++,x+=8)
      {
      xe = x+7;
      gsetcolorf(palette[c%(sizeof(palette)/sizeof(palette[0]))]);
      gfillvp(x,0,xe,ggetvph()-1,0xffff);
      }

   if ((background = save_vp_background( ggetvpnum() )) != NULL)
      {
      x = gsymw(psym);
      xe = x;
      x = (x < ggetvpw()) ? (ggetvpw()-x)/2+1 : 0;
      y = gsymh(psym);
      y = (y < ggetvph()) ? (ggetvph()-y)/2+1 : 0;
      /* Grey-level symbol with solid colors */
      gsetcolorf(G_BLUE);
      gsetcolorb(G_CYAN);
      gputsym(x,y,psym);
      wait(1000);
      /* Assign other colors for grey-level symbol */
      gsetcolorf(G_BLACK);
      gsetcolorb(G_WHITE);
      gputsym(x,y,psym);
      wait(1000);
      /* Show grey-level symbol with transperant background */
      gsetupdate(GUPDATE_OFF);
      ghw_restoreblk(background);
      gsetmode(GTRANSPERANT);
      gputsym(x,y,psym);
      gsetupdate(GUPDATE_ON);
      wait(1500);

      /* Demonstrate use of gputsymrot and transperance */
      for(c=0;c<(SGUINT)xe-10;c++)
         {
         gsetupdate(GUPDATE_OFF);
         ghw_restoreblk(background);
         gputsymrot((SGINT)x-(SGINT)c,(SGINT)y,0,psym,0,0,GALIGN_TOP|GALIGN_LEFT|GTRANSPERANT);
         gsetupdate(GUPDATE_ON);
         wait(10);
         }
      free(background);
      }
   }


/*
   Output a symbol with transperant pixels
   Temporary activate "trasperant" mode during symbol output
   Temporary sets the symbol color used for "transperant" pixels
*/
void gputsym_transperant(GXT x, GYT y, PGSYMBOL psym, GCOLOR trans_color)
  {
  GCOLOR oldcolor;
  GMODE oldmode;

  // Switch to transperant mode
  oldmode = ggetmode();
  gsetmode(oldmode | GTRANSPERANT);

  // Select color used to signal transperant symbol pixels
  oldcolor = gsetcolorb(trans_color);

  // Output symbol
  gputsym(x,y,psym);

  // Restore background for normal draw
  gsetmode(oldmode);
  gsetcolorb(oldcolor);
  }

void test_put_transperant(PGFONT symfont, GCOLOR trans_color)
   {
   /* Use a symbol in a virtual font */
   PGSYMBOL psym;
   GXT x;
   GYT y;

   gresetvp();
   gsetcolorb(G_ORANGE);
   gclrvp();

   psym = ggetfsym(0,symfont);
   /* Center symbol. Locate oversized symbols in upper left corner. */
   x = gsymw(psym);
   x = (x < ggetvpw()) ? (ggetvpw()-x)/2+1 : 0;
   y = gsymh(psym);
   y = (y < ggetvph()) ? (ggetvph()-y)/2+1 : 0;

   gsetmode(GALIGN_HCENTER|GALIGN_TOP|GVPCLR_RIGHT|GVPCLR_LEFT);
   gputs("Black = transperance");

   /* Show symbol (with transperant) */
   gputsym_transperant(x,y,psym, trans_color);
   wait(2000);

   /* Show symbol (without transperant) */
   gputs("No transperance");
   gputsym(x,y,psym);
   wait(2000);
   }

static void test_vfont2(void)
   {
   gclrvp();
   gselfont(&SYSFONT);
   gputs("Show symbols in virtual memory");

   wait(2000);
   show_symbol((PGFONT) &bwsym_64x64);
   wait(2000);

   show_symbol((PGFONT) &logo);
   wait(2000);

   #if (GDISPPIXW >= 4)
   /* Test 4 bit palette symbol */
   show_symbol((PGFONT) &test_4bit);
   wait(2000);
   #endif

   #if (GDISPPIXW >= 8)
   /* Test 8 bit RGB symbol */
   show_symbol((PGFONT) &test_8bit);
   wait(2000);
   #endif

   #ifdef GHW_USING_RGB
   test_put_transperant((PGFONT)&test_4bit, G_BLACK);
   wait(2000);

   show_transperant_grey((PGFONT)&grey16sym);
   wait(2000);

   /* Test 16 bit RGB symbol */
   show_symbol((PGFONT) &tulip120);  /* Converted to 16 bit */
   wait(2000);

   show_symbol((PGFONT) &violet128); /* Converted to 16 bit */
   wait(2000);
   #endif
   show_symbol_rot((PGFONT) &violet128);
   }


void show_png_fontlist(void)
   {
   PGFONTV testfonts[] = {(PGFONTV)&test_bw,
                          (PGFONTV)&test_greya,
                          (PGFONTV)&test_pal8,
                          (PGFONTV)&test_rgb,
                          (PGFONTV)&test_rgba};
   GXT x,w;
   GYT y,h;
   PGSYMBOL psym;
   int i;
   gresetvp();
   gsetcolorb(G_WHITE);
   gclrvp();

   gsetmode(GALIGN_HCENTER|GALIGN_TOP|GVPCLR_RIGHT|GVPCLR_LEFT);
   gputs("PNG symbols");

   for (i = 0,x=2,y=2+ggetfh(); i < sizeof(testfonts)/sizeof(testfonts[0]); i++)
      {
      psym = ggetfsym(0,(PGFONT)testfonts[i]);
      w= gsymw(psym);
      if (x+w+2 > ggetvpw())
         {
         h= gsymh(psym);
         if (y + h + 2 > ggetvph())
            return;
         x = 2;
         y = y+h+2;
         }
      gputsym(x,y,psym);
      x = x + 2 + w;
      }
   wait(2000);
   }

int main(void)
   {
   ginit();

   /* Use some normal fonts */
   gsetmode(GWORD_WRAP);
   gputs("First text with compiled-in fonts");
   gputs("\nSYSFONT");
   wait(1000);
   gselfont((PGFONT)&times13);
   room_for_text(1);
   wait(1000);
   gputs("\nTimes13");
   gselfont(&SYSFONT);
   gputs("\n");
   room_for_text(2);
   wait(2000);
   gputs("\nNext show Virtual Fonts");
   wait(2000);
   /* Open vfont bin file */
   if (getvmem_open(TSTVFONT1_ID) != 0)
      {
      gputs("\nError Could not access virtual font image");
      return 1;
      }

   /* Access to virtual font image is ok, Continue test */
   test_vfont1();
   test_vfont2();

   show_png_fontlist();
   /* Close vfont (file) */
   getvmem_close(TSTVFONT1_ID);

   gsetmode(GALIGN_VCENTER|GALIGN_HCENTER);
   gputs("\n DONE");
   return 0;
   }
