/******************************* vftxtparse.c ************************************

   This example demonstrate how to use the "file" functions to implement
   a simple, flexible  and generic (processor and C compiler independent),
   way to parce and load configuration data from a text file.

   This module provides generic functions for:
   -  Skip white characters and C-style comments, return copy of delimitor character
   -  Load and consume next C delimitor character
   -  Load a ASCII decimal digit, leave untouched if not a valid decimal character
   -  Load a C-style text string, leave untouched if not a valid ".." string
   -  Load a C-style token to string, leave untouched if not a valid C-style token

**********************************************************************************/
#include <gvfile.h>

#if defined( GVIRTUAL_FILES_STATIC ) || defined( GVIRTUAL_FILES )
/*
   Skip C style comments,
   Skip leading "white" characters ,
   Return at start of first token found (not a comment, not a "white")
   The token kept on "file" for further processing

   The return paramter depends on 'terminate_at_eol'  parameter

   if terminate_at_eol == 0
      \n characters treated as "white" character and just consumed.
   if terminate_at_eol == 1
      \n characters treated as processing terminator
   all other is treated as start of new token.
   A copy of the first token character is returned for easy evaluation

   The value -1 is returned if at end of file
*/
SGUINT gvf_skip_space_comments(GV_FILE *vf, SGUCHAR terminate_at_eol)
   {
   SGINT ch;
   for(;;)
      {
      ch = gvfgetch(vf);  // Next character
      if ((ch == ' ') || (ch == '\r') || (ch == '\t'))
         continue; /* Skip whites ('\n' is kept) for processing below */
      if (ch == '/')
         { /* Comment start */
         ch = gvfgetch(vf); // Next character
         if (ch == '/')
            { // Start of single-line comment, terminator is \n (New line)
            for(;;)
               {
               if ((ch = gvfgetch(vf)) < 0) return -1; /* End of files no more tokens */
               if (ch == '\n') break;                  /* End of line */
               }
            }
         else
         if (ch == '*')
            { /* Start of multi-line C comment, terminator is '*','/' */
            SGUCHAR prevch = 0;
            for(;;)
               {
               if ((ch = gvfgetch(vf)) < 0)
                  return -1; /* EOF before end of C comment (illegal, but just in case) */
               if ((ch == '/') && (prevch == '*'))
                  break; /* End of comment, consume it */
               prevch = ch;
               }
            continue; /* Resume processing, More whites or comments may follow */
            }
         }
      if (ch == '\n') /* End of line */
         {
         if (terminate_at_eol)
            return ch; /* Stop processing. Return \n, consumed from file */
         continue; /* \n is treated as white, just consume */
         }
      if (ch >= 0)
         gvfungetch(vf);  // Not EOF, Push start of next token back to be evaluated later
      return ch;  // Return a copy of character for easy evaluation
      }
   }

/*
   Load and consume next (expected) separator
   Parsing rules:
     Preceeding white spaces and C-style comments are ignored

   Returns and consume the next (non-white) character
   Returns -1 at EOF
*/
SGINT gvf_get_separator(GV_FILE *vf)
   {
   gvf_skip_space_comments(vf,0);
   return gvfgetch(vf);
   }

/*
   Load a (optionally signed) decimal digit from text file and convert to SGLONG
   Parsing rules:
     Preceeding white spaces and C-style comments are ignored
     First character must be '+','-',{'0'-'9'}
     Next characters can be {'0'-'9'}
     All other characters terminates the digit

   Returns 0 if digit isok
   Returns 1 if not a digit
*/
SGUCHAR gvf_get_digit( GV_FILE *vf, SGLONG *pval)
   {
   SGLONG val = 0;
   SGCHAR minus = 0; // Negative digit flag
   SGINT ch;

   // Is next character a digit or sign, if not leave it in stream
   ch = gvf_skip_space_comments(vf, 0);
   if ((ch < '0') || (ch > '9'))
      { // Not a digit
      if ((ch == '-') || (ch =='+'))
         { // Is a sign, process and consume
         if (ch == '-')
            minus = 1;
         ch = gvfgetch(vf);
         }
      else
         return 1; // Was not a digit
      }

   // Next character is a digit, consume and process it
   for(;;)
      {
      ch = gvfgetch(vf); // Next character
      if ((ch < '0') || (ch > '9'))
         break; // Digit terminator reached
      val = val*10 + (ch - '0');
      }
   gvfungetch(vf);  // Push back terminator character for later processing

   if (pval != NULL)
      {
      if (minus == 0)
         *pval = val;
      else
         *pval = val*-1;
      }
   return 0;
   }

/*
   Load a C style text string to a buffer
   Parsing rules:
     Preceeding white spaces and C-style comments are ignored
     The string must be embedded in "" and be located on one "file" line
     The following special C style character sequences are processed
       '\''n' => \n
       '\''r' => \r
       '\''t' => \t
       '\''"' => "
       '\''\' => \

   A valid string is always consumed normally from "file", even if buf==NULL,
   or buflen == 0 (i.e. a parameter can be skipped)

   If the buffer is too small to hold the string the fall-back procedure
   is to chrop the string and still NUL terminated it.

   Returns 0 if string is ok
   Returns 1 if not a string
*/
SGUCHAR gvf_get_text( GV_FILE *vf, SGUCHAR *buf, SGUINT buflen)
   {
   SGUINT i;
   SGINT ch;

   if (gvf_skip_space_comments(vf, 0) != '\"')
      return 1; // Not a string
   gvfseek(vf,gvftell(vf)+1);  // Skip string identifier

   if (buf == NULL)
      buflen = 0;  // Just so only one parameter need to checked in the loop

   for(i=0;;)    // load until next " or end of line
      {
      ch = gvfgetch(vf);
      // Normal end-of-string
      if (ch == '\"')  break;
      // Handle illegal end-of-file or end-of-line as end-of-string
      if ((ch < 0) || (ch == '\n')) break;
      // Handle a few C string escape scequences
      if (ch == '\\')
         {
         ch = gvfgetch(vf);
         switch (ch)
            {
            case 'n':
               ch = '\n'; // C string \n to new-line character
               break;
            case 'r':
               ch = '\r'; // C string \r to char-return character
               break;
            case 't':
               ch = '\t'; // C string \t to tab character
               break;
            case '\\':    // C string \\ to \ character
            case '"':     // C string \" to " character
               break;
            default:
               ch = '?';
               break;     // Unknown or illegal eschape
            }
         }
      if (i < buflen)
         buf[i++]=(SGUCHAR) ch; // Add to buffer
      }
   if (buflen > 0)
      buf[(i < buflen) ? i : buflen-1]=0;
   return 0;
   }

/*
   Fetch next C-style token (symbolic name)
      First character must be '_',{'A'-'Z'},{'a'-'z'}
      Next characters can be '_',{'A'-'Z'},{'a'-'z'},{'0'-'9'}
      All other characters terminates the token
      Preceeding white spaces and C-style comments are ignored

   A valid token is always consumed normally from "file", even if buf==NULL,
   or buflen == 0 (i.e. a parameter can be skipped)

   If the buffer is too small to hold the token the fall-back procedure
   is to chrop the token and still NUL terminated it.

   Returns 0 if token is ok
   Returns 1 if not a C-style token
*/
SGUCHAR gvf_get_token( GV_FILE *vf, SGUCHAR *buf, SGUINT buflen )
   {
   SGINT ch;
   SGUINT i;
   ch = gvf_skip_space_comments(vf,0);
   if (!(((ch >= 'A') && (ch <= 'Z'))||((ch >= 'a') && (ch <= 'z'))||(ch == '_')))
      return 1; // Not a token or EOF, leave it in stream

   if (buf == NULL)
      buflen = 0;  // Just so only one parameter need to checked in the loop

   for(i=0;;) // Load and consume until illegal token character (or EOF)
      {
      ch = gvfgetch(vf);
      if (((ch >= 'A') && (ch <= 'Z'))||
          ((ch >= 'a') && (ch <= 'z'))||
          ((ch >= '0') && (ch <= '9'))||
          (ch == '_'))
         {
         if (i < buflen)
            buf[i++]=(SGUCHAR) ch; // Add to buffer
         }
      else
         break; // a delimitor character or EOF reached.
      }
   if (buflen > 0)
      buf[(i < buflen) ? i : buflen-1]=0;
   return 0;
   }

#endif /* GVIRTUAL_FILES_STATIC || GVIRTUAL_FILES */

