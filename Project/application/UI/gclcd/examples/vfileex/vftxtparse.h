#ifndef GENVF_TSTVFILE_H
#define GENVF_TSTVFILE_H
/******************************* vftxtparse.c ************************************

   This module prototype generic functions for:
   -  Skip white characters and C-style comments, return copy of delimitor character
   -  Load and consume next C delimitor character
   -  Load a ASCII decimal digit, leave untouched if not a valid decimal character
   -  Load a C-style text string, leave untouched if not a valid ".." string
   -  Load a C-style token to string, leave untouched if not a valid C-style token

**********************************************************************************/
#include <gvfile.h>

#ifdef __cplusplus
extern "C" {
#endif

#if defined( GVIRTUAL_FILES_STATIC ) || defined( GVIRTUAL_FILES )

SGUINT gvf_skip_space_comments( GV_FILE *vf, SGUCHAR terminate_at_eol );
SGINT gvf_get_separator( GV_FILE *vf );
SGUCHAR gvf_get_digit( GV_FILE *vf, SGLONG *pval );
SGUCHAR gvf_get_text( GV_FILE *vf, SGUCHAR *buf, SGUINT buflen );
SGUCHAR gvf_get_token( GV_FILE *vf, SGUCHAR *buf, SGUINT buflen );

#endif /* GVIRTUAL_FILES_STATIC || GVIRTUAL_FILES */

#ifdef __cplusplus
}
#endif

#endif /* GENVF_TSTVFILE_H */

