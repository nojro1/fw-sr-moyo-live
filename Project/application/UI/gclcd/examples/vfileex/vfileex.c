/******************************* vfileex.c *************************************

   This example demonstrates how a text file intepreter can be made
   which can load a configuration structure from an text file.

   The "file" data loader ignore C-style comments and white characters.
   The data element are defined by C separator types '{' ',' '}' only.

   This example demonstrates how the same file format can also be directly
   compiled-in to insert reset defaults in the application.

   The examples build on the generic "file" intepreter functions in vftxtparse.c

   To observe the functionality of each funtion is recommended to single step
   through the example.

********************************************************************************/

#include <gdisp.h>
#include <vftxtparse.h>
#include <string.h>    // memcpy(..)
#include <stdio.h>     // sprintf(..)

#ifndef GVIRTUAL_FILES
  #error "This example requires that GVIRTUAL_FILES is defined in gdispcfg.h"
#endif

/* Define data structure to be load */
typedef struct
   {
   SGLONG scale_mul;  // Integer
   SGLONG scale_div;  // Integer
   SGUCHAR entity[5]; // NUL terminated string
   } AD_SCALE;

// Emulate scaling data for two AD convertes configurations.
// Hint: Properly formatted then a config data file can be used both as
// Virutal File data loaded at runtime, and for compilate time initialization
AD_SCALE adscale[2] =
   {
   // Trick. Use same config file format as the configuration loader file
   // to do compile time initialization of the applications power-up reset defaults
   #include <testfiles/config_setup.txt>
   };

/*
   Walk "file" and load the data to AD_SCALE setup structure from a "file" block.
   (Here the "file" data content format is assumed to be the same as compiled-in for adscale[] defaults)
   When load is ok, then update destination, else keep old data (default) intact
*/
char load_AD_SCALE(GV_FILE *vf, AD_SCALE *pad)
   {
   AD_SCALE ad; // Local copy for load (to preserve original in case of error)
   for(;;)
      { // Fetch data for AD_SCALE element
      if (gvf_get_separator(vf) != '{') break;              // Get structure start delimitor
      if (gvf_get_digit(vf,&ad.scale_mul)) break;           // Get scale_mul parameter
      if (gvf_get_separator(vf) != ',') break;              // Get parameter separator
      if (gvf_get_digit(vf,&ad.scale_div)) break;           // Get scale_mul parameter
      if (gvf_get_separator(vf) != ',') break;              // Get parameter separator
      if (gvf_get_text(vf,&ad.entity[0],
                        sizeof(ad.entity)/sizeof(ad.entity[0]))) break; // Get entity text string
      if (gvf_get_separator(vf) != '}') break;              // Get structure end delimitor

      // Complete structure loaded ok, copy to destination
      memcpy(pad,&ad,sizeof(ad));
      return 0; // No errors detected
      }
   return 1; // Load or file format error
   }

/*
   Find A/D configuration "file" in Virtual Font image and
   load it to the configuration structure
*/
char load_config(PGCSTR config_name)
   {
   GV_FILE vf;

   if (gvfopenrd( &vf, config_name, 0) == 0)
      {
          SGUINT i;
      for (i = 0;;)
         {
         if (load_AD_SCALE(&vf, &adscale[i])) break;     // Get array element
         if (++i >= sizeof(adscale)/sizeof(adscale[0]))
            return 0;  // Complete structure loaded (normal exit)
         if (gvf_get_separator(&vf) != ',') break;       // Get separator to next A/D element
         }
      }
   return 1; // Some "file" load error or too few elements in config file
   }

/********************  end of loader part **********************/
/****************  demonstrate config loader  ******************/

/*
   Format and show one config data element in viewport
*/
void print_AD_SCALE(AD_SCALE *ad, SGUINT num)
   {
   char buf[40];
   sprintf(buf,"%u %ld, %ld, \"%s\"",
         num,
         (unsigned long) ad->scale_mul,
         (unsigned long) ad->scale_div,
         &ad->entity[0]);
   gputs(buf);
   }

/*
   Show whole A/D configuration on screen
*/
void show_config(char *toptext)
   {
   GYT y;
   // Init full screen viewport
   gresetvp();
   gselfont(&ms58p);
   gsetmode(GLINECUT);
   gclrvp();

   // Print header
   gputs(toptext);
   y = ggetypos()+1;
   grectangle(0,y,GDISPW-1,y); // Decoration line
   // Set data viewport to screen below
   gsetvp(0,y+2,GDISPW-1,GDISPH-1);

   // Dump AD configuration data
   for (y = 0;;)
      {
      print_AD_SCALE(&adscale[y],y);
      if (++y >= sizeof(adscale)/sizeof(adscale[0]))
         break; // Last element printed
      // More data, move to next line, scroll if needed
      gputs("\n");
      }
   }

int main(void)
   {
   ginit();

   /* Open access to .bin image */
   if (getvmem_open(0) != 0)
      return -1;

   // Show compiled-in configuration default
   show_config("A/D reset defaults");

   // Load configuration from "file" block (overwrite default configuration)
   // Here assume file is renamed to 'AD_setup' by GENVF.EXE during the Virtual Font image generation
   if (load_config("AD_setup"))
      { // Load failed (not found or illegal data)
      gclrvp();
      gputs(" Load error");
      }
   else
      {
      // Show loaded configuration data
      // (No change if config_setup.txt and AD_setup "file" is equal)
      show_config("A/D config loaded");
      }

   /* Close vfont access */
   getvmem_close(0);
   return 0;
   }

