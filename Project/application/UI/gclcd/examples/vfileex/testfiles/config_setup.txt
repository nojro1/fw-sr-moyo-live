/*
    Data for interpretor example demonstrating how a text file
    intepreter can load configuration structure data from an text file.
    The configuration data is for an array of structures arranged like this
         {
         {digit,digit,textstring},
         {digit,digit,textstring}
         }
    C-style comments and white characters are ignored
    The data element are defined by C separator types '{' ',' '}'

    This file file format enable it to also be compiled-in with the 
    application in order to inserts reset startup defaults
*/
// AD0 input calibration and entity, 
// Test Structure data on same line
 { 12343, 1024, "km/t"},  // scale_mul, scale_div, entity

// AD1 input calibration and entity, 
// Test structure data can span multiple lines
 {
 100,  // scale_mul,
 256,  // scale_div,
 "A"   // entity string
 }
