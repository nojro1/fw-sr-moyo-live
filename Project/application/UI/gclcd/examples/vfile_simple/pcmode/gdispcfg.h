/*
   Reduced library configuration files.
   For use of Virtual Files only (ex during application startup)

   RAMTEX Engineering Aps 2013
*/
#ifndef GDISPCFG_H
#define GDISPCFG_H

#define GVIRTUAL_FILES         /* Enable virtual "file" support (dynamic lookup) */
#define GVIRTUAL_FILES_STATIC  /* Enable static virtual "file" support (fast lookup) */

#ifndef GHW_PCSIM
   /* Memory type qualifiers only for target compilation mode (dont care in PC simulation mode) */
   /* The following definements allow you to optimize the parameter passing and to use target compiler specific
      memory allocation keywords (default will be used for PC simulator mode) */

   /* Keyword used for fast variables optimization (critical params)*/
   #define GFAST  /* nothing */
   /* Keyword used for very fast variables optimization (critical params)*/
   #define GVFAST /* nothing */
   /* Type qualifier used on pointer (parameter) to strings when the object is not
      modified by function (part of C strong prototyping) */
   #define GCONSTP const
   /* type qualifier used for fixed data (graphic tables etc) */
   #define GCODE  const
   /* Memory type qualifier used for fixed data (if GCODE setting is not enough) */
   #define FCODE  /* nothing */
   /* Memory type qualifier used for pointer to fixed data (if GCODE var * is not enough) */
   #define PFCODE /* nothing */
   /* Keyword used for generic pointers to data strings, if generic pointers is not default */
   #define PGENERIC /* nothing */
#endif

/* End of gdispcfg.h */
#endif
