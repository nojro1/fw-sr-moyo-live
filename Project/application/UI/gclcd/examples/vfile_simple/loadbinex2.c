/***************** Generic dynamic "file" loader example ***************************

   Example on how to load a named "file" from a Virtual Font image to memory.

   This binary loader example only require these 3 Virtual File modules to
   work and can be used without any other graphic library modules.

        getvmem.c  // hardware interface to memory
        gfontv.c
        gvfread.c

   The switch GVIRTUAL_FILES must be defined in gdispcfg.h

   RAMTEX Engineering Aps 2013

*************************************************************************************/
#include <gvfile.h>

#ifdef GVIRTUAL_FILES
/*
   Generic load of dynamic (named) "file" data block
*/
SGUCHAR load_named_binblk( SGUCHAR *buf, SGULONG bufsize, PGCSTR binname)
   {
   SGUCHAR retval = 0;
   GV_FILE vf;
   if (gvfopenrd( &vf, binname, 0))
      retval = 1;  // File not found
   else
   // Load file
   if (gvfread(&vf,0,buf,bufsize)==0)
      retval = 1;  // Illegal parameters
   return retval;
   }

#endif


