/************ Minimum static "file" loader example ****************************

   Example on how to load a static located "file" from a Virtual Font image to
   a memory buffer.

   This binary loader example only require these 3 small Virtual File modules
   to work and can be used without any other graphic library modules.

        getvmem.c  // hardware interface to memory
        gvfopendc.c
        gvfread.c

   The switch GVIRTUAL_FILES_STATIC must be defined in gdispcfg.h

   Hint: If a static located file block is always the first data object in
   the Virtual Font image *.bin file, then the location will always be the
   same across different *.bin file revisions.

   RAMTEX Engineering Aps 2013

******************************************************************************/
#include <gdisp.h>
#include <gvfile.h>

#ifdef GVIRTUAL_FILES_STATIC

/*
   Generic load of static "file" data block
*/
SGUCHAR loadbinblk( SGUCHAR *buf, SGULONG bufsize, PGV_FILE_DESC filedesc)
   {
   GV_FILE vf;
   gvfopen_desc( &vf, filedesc);
   if (gvfread(&vf,0,buf,bufsize)==0)
      return 1;  // Illegal parameters
   return 0;     // Load done
   }

#endif /* GVIRTUAL_FILES_STATIC */

