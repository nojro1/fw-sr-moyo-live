/*
   How to use load of Virtual Files data block without the GUI library
   -------------------------------------------------------------------

   The examples below demonstrate how a the Virtual Files features can
   be used for loading data blocks, even before the graphic display
   driver library is initialized.

   As no LCD screen is available during load this PC mode demo use the
   PC console screen for debug output.

   RAMTEX Engineering Aps 2013
*/
#include <gvfile.h>
#include <stdio.h>   // printf(..), Use PC console to show result of test

// Prototype loade functions
SGUCHAR loadbinblk( SGUCHAR *buf, SGULONG bufsize, PGV_FILE_DESC filedesc); // loadbinex1.c
SGUCHAR load_named_binblk( SGUCHAR *buf, SGULONG bufsize, PGCSTR binname);  // loadbinex2.c

#include <tstvfile.h>  // contains the test1_txt "file" location info

/*
   Example load of static "file" data block
   (Static location lookup, very fast, very low-overhead data fetch)
*/
void loadbinblk_ex(void)
   {
   #ifdef GVIRTUAL_FILES_STATIC
   SGUCHAR buf[100];
   G_OBJ_CLR(buf);  /* Clear buffer, ease debug, assure NUL termination */
   printf("Binary static \"file\":\n");

   // Load "file" defined by a static "file" location descriptor
   if (loadbinblk(&buf[0],sizeof(buf)-1,&test1_txt))
      printf("Open or load failed");
   else
      { // Use data block
        // Here just dump data assuming it is text
      printf("%s",buf);
      }
   #endif /* GVIRTUAL_FILES_STATIC */
   }

/*
   Example load of "named file" data block
   (Dynamic location lookup, flexible)
*/
void loaddynbinblk_ex(void)
   {
   #ifdef GVIRTUAL_FILES
   SGUCHAR buf[100];
   G_OBJ_CLR(buf);  /* Clear buffer, ease debug, assure NUL termination */
   printf("Binary \"named file\" load:\n");

   // Load "file" identified by a "filename"
   if (load_named_binblk(&buf[0],sizeof(buf)-1,"test2.txt"))
      printf("\"file\" open or load failed");
   else
      { // Use data block
      // Here just dump data to screen assuming it is text
      printf("%s",buf);
      }
   #endif
   }

/*
   Example load of "named file" data block where the "file" is
   identified via a directory index defined in tstvfile.h
   (Dynamic location lookup, flexible)

   (FILE_STORE_DIR must have been used during *.bin file creation)
*/
void loaddynbinblk_idx_ex(SGUINT idx)
   {
   #ifdef GVIRTUAL_FILES
   SGUCHAR buf[100];
   G_OBJ_CLR(buf);  /* Clear buffer, ease debug, assure NUL termination */
   printf("Binary \"named file\" load:\n");

   if (idx >= GVF_NUMBER_OF_FILES)
      return; // Index larger than number of named "files" in .bin image

   // Load "file" identified by a "file directory" index
   if (load_named_binblk(&buf[0],sizeof(buf)-1, file_dir[idx]))
      printf("\"file\" open or load failed");
   else
      { // Use data block
      // Here just dump data to screen assuming it is text
      printf("%s",buf);
      }
   #endif
   }

int main(void)
   {
   /* Open access to .bin image */
   if (getvmem_open(0) != 0)
      return -1;

   loadbinblk_ex();     // Static file load
   loaddynbinblk_ex();  // Dynamic named file load

   printf("\n");

   // Dynamic named file load via file index (use integer handles)
   // The symbolic enum handles makes it possible to check at compile 
   // time if used "files" is included in the VF *.bin image
   loaddynbinblk_idx_ex(GVF_test1_txt);  
   loaddynbinblk_idx_ex(GVF_test2_txt);

   /* Close vfont access */
   getvmem_close(0);
   return 0;
   }
