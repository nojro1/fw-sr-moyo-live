GUI library demo
----------------

The project in this directory demonstrates use of the general
example functions in  gclcd\examples\guilib\*.* build on top
of the general library features.

The project example has focus on these topics:
-  How to create a top menu, pull down menus and popup menus
   with the library.
-  How to add handling of keyboard and touch screen events
   in a GUI system.
-  How to create a GUI event handler using your own style and
   with a minimum of RAM memory overhead.

The application code in this demo consist of these modules:
wm_main.c      // Main loop (assuming use of a polled system)
wmevent.c      // GUI menu and event handling
keyintf.c      // Keyboard interface. Collect events from the (simulated)
               // keyboard, touch screen, and timer.
wmadmenu.c     // Popup view with dynamic bars. Demo activated via wmevent.c
wmexample.h    // Define of public function prototypes, public data and
               // event enumeration. Includes other library headers.

For more implementation details on the event system design
please read the header in wmevent.c

gdispcfg.h configuration
------------------------
Changes from default gdispcfg.h

- Increase number of viewports to 10
          #define GNUMVP 10
- Enable use of _vp functions
          #define GFUNC_VP     /* Enable named viewport functions xxx_vp()*/
- Enable use of buffered mode (optional)
         #define GBUFFER /* Extern buffer for data manipulation, fast */
- If the display has a protrait mode / landscape layout selection possibility,
  then change settings to landscape mode. 90 degree rotation is typically
  done as:
          #define GHW_ROTATED and swap GDISPH, GDISPW definitions values


Moving to target
----------------
As provided the project uses the PC LCD screen simulator.
In order to move this project to a target system the following target
hardware specific drivers must be created:

   -  Keyboard driver returning (at least) these 5 or 6 navigation events
         Up, Down, Left, Right, Return(accept) [, Backspace(skip)]

   -  Touch screen driver returning an event (EVENT_TOUCH) on touch down
      and update a global touch x,y coordinate set with x,y values normalized
      to the screen pixel size {0 to GDISPW-1},{0 to GDISPH-1}.

      This event is optional. If the target display does not have a touch screen
      then the touch related code can be commented out in the keyintf.c and
      wmevent.c modules (make a search for EVENT_TOUCH), or you can leave it
      as it in the source as it will just be "dead code" when no EVENT_TOUCH
      event are generated in target.

   -  Timer driver which returns a timer tick event (EVENT_TICK)
      ex every 20 or 100 ms.

      This event may be optional depending on the application as it is only
      required by GUI view support functions. In this project the timer
      tick event drives the dynamics of the example in wmadmenu.c module.


How to show dynamic values (an optimization hint)
-------------------------------------------------
One particular use of EVENT_TICK is to only update views showing
dynamic data when it is needed!

Following these two rules may save a huge amount of processing power
in a target system:
-  Never refresh the screen faster than the human eye can follow.
   (EVENT_TICK can be used for generating the appropriate update
    intervals)
-  Only refresh screen if the raw data has changed.

To dectect data change a view_update_flag could be set by low-level
system when there is new data and cleared again by the GUI view function.

However a much better solution (especially with relatively slow
changing data) is to operate with a set of "shadow data" to the
raw data. 
Example:
         int value;        /* updated by ex AD converter driver */
         int shadow_value; /* detect changes, managed by GUI view */

         void show_value( char first_time )
            {
            if ((value != shadow_value) || /* Detect any change in value */
                (first_time != 0))         /* Show always when view is opened */
               {
               char str[10];
               shadow_value = value; /* Take copy of data */
               /* Then process the shadow copy of data for the view. This assure
                  that data is consistent (stable) during the formatting process */
               /* ex Format value (here with two decimals as 199.99) and place
                  it on line 2 in current viewport */
               sprintf(str,"%3u.%02u", shadow_value/100, shadow_value%100);
               gsetpos(0,ggetfh()*2); /* Set line position*/
               gputs(str);
               }
            // Other data
            }

Note about on using update under interrupt
------------------------------------------
The 'shadow method' above is also an efficient way to create an interface 
between 'interrupt level' and 'main level' in a target system.
If 'value' in the example is updated by an interrupt driver then note 
that it is only the assignment statement itself ( shadow_value = value; )
which may need to be protected from interrupt intervention (this can
minimize the time duration interrupts are disabled considerably).

However wether interrupt disable/enable is actually needed also depends
on the target processor CPU. 
In this example protection is only needed if the 'int' type is not mapped 
to a single register in the CPU. If an 'int' type equals a native register 
size in the processor then read and write operations is done by a single 
machine operation and will therefore always be atomic. 
In such cases interrupt disable/enable are NOT required.

