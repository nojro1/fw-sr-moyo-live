/*
   This example demonstrates how to use the menu functions in the
   GUILIB directory.
   The event handler example here demonstrates how a simple GUI
   event handler can be implemented.

   "Style","look" and "feel"
   --------------------------
   The demo_event() function here implement most of the "GUI
   personality" as it is seen and felt by a user. It is also
   here "ease of use" can be implemented ex via "short cuts", or
   via an event processing where "multible different, but intuitive,
   user events" results in the same event action.

   Menus are implemented on a last-opened - first-closed basis.
   The advantage is that this give a very efficient reuse of
   viewports and (if menu buffers are used) of the heap memory.

   The example demonstrates how key events and touch screen
   events can be handled in the event processing for a given
   menu state.

   Events
   ------
   It is assumend that the target system have a very simple
   keyboard with 6 keys, plus (optionally) a touch screen.
   The keys are:  Arrow Up, Arrow Down, Arrow Left, Arrow Right,
   Enter (accept), Backspace (skip).

   A screen touch (down click) event causes the event action
   to be decided based on the touch x,y position and the current
   menu state.

   A time tick event is not used or processed in this event handler,
   but may be passed down to data display event functions displaying
   dynamic infomation in a window.

   Design concept
   --------------
   The event handler here is designed for use in a simple
   polled menu system.
   The event handler function therefore returns at once when
   completed. No waiting points are taken and no process changes
   are done.
   i.e. this design method is particulary suitable for small
   systems without any task scheduling or operating system.

   All menu states are selected via a single state variable:
         MNU_STATES mnu_state

   The advantage is a fast routing from entry to event handling
   code, and a relatively simple program structure (primarily a
   switch statement), plus that it is reasonably easy to a get
   good overview of all event processing specific to the individual
   menu state.
   The disadvantage is that this event handler module tend to
   grow large with increasing number of menus.

   Each menu state has its own event processing. The advantage
   is that it makes it relatively simple to fine tune the event
   system for the best "look and feel". The disadvantage is some
   degree of repetition of nearly equal code fragments.

   The menu features in the guilib directory demonstrates how menu
   contents can be defined simply by use of C strings where each item
   is separated with \n (for vertical menus) or by spaces (for
   top menu).
   This mean that most menu data can be just ROM-only objects. In this
   way each menu may only require a few bytes of RAM.

   Memory optimization
   -------------------
   For minimum RAM consumption pull-down and popup menus should
   be create on a neutral background, and opened next to each other
   with no menu overlap.In this way there is no need for buffering the
   background under pull-down menus and popup menus.
   For minimum RAM memory requirement menu window buffering
   can then be turned off in the style setting.
   (Note this is independent of the GBUFFER library settings)

   If menu overlap is used in the design then menu background
   buffering is required. Use of menu overlap is often required
   with small screen sizes.
*/
#include <wmexample.h>

#if ((GDISPW < 128) && !defined( WM_USE_POPUP_BUFFER ))
  #error Buffered pop windows required, add WM_USE_POPUP_BUFFER to preprocessor definitions
#endif


/* Some external state flags which are modified by menu */
int filter_a_flag = 0;
int equalizer_flag = 0;
int datacom_select = 1;

/***************** menu texts ******************/

/* Menu text for top menu */
#if (GDISPW < 132)
static const char topmenu[]        = "Config  Proc.  Sens.";
#else
static const char topmenu[]        = "Config  Process  Sensors";
#endif

/* Menu texts for sub menu 1 */
static const char submenu1[]       = "Show A/D ch \nEqualizer mode\nFilter A\nData Com.";
static const char submenu0[]       = "Show A/D ch \nLinear mode\nFilter A\nData Com.";
static const char submenu_chk1[]   = "\n\n" CHECKMARK "\n" ;
static const char submenu_chk0[]   = "\n\n\n ";
static const char submenu_arrows[] = "" ARROWRIGHT "\n\n\n" ARROWRIGHT;

static const char submenu_datacom[] = " Baudrate \n 19200\n 38400\n 57600\n 115200";

static const char process_txt[] = "Screen released for use as full screen canvas.\n"
                                  "Ready for any drawing operations\n"
                                  "Here just press Return to restore menu system";

/************** menu state variables *********/
typedef enum
   {
   MNU_INIT,
   MNU_TOP,
   MNU_SUB,
   MNU_SUB_DATACOM,
   MNU_POP_AD_MENU,
   MNU_POP_SENSORS,
   MNU_PROCESS
   } MNU_STATES;

static MNU_STATES mnu_state;

/* Temporary variables to hold information about
   viewport numbers and index for opened menus */
static SGUCHAR topvp, subvp, subvp2, popupvp;  /* View port numbers for menus */
static SGUCHAR topidx,subidx, subidx2;         /* Current indexes for select menus */

/********************* Support functions *******************/

/*
   Return number of items in (open) menu viewport
   Assumes that the menu is created with a \n formatted string
*/
static SGUCHAR menuitems(SGUCHAR mnu_vp)
   {
   SGUCHAR ret;
   mnu_vp = gselvp(mnu_vp);
   ret = ggetvph()/ggetfh();
   gselvp(mnu_vp);
   return ret;
   }

/*
   Create submenu with variable content.
   Here the text content depends on some system flag states
*/
static SGUCHAR sub_menu1_make(void)
   {
   /* Here use a 3 column menu type */
   return gm_submenu3_open(0,0,
        /* check column (2 states) */
        filter_a_flag ?  &submenu_chk1[0] : &submenu_chk0[0],
        /* Menu main content (2 states) */
        equalizer_flag ? &submenu1[0] : &submenu0[0],
        /* Submenu indicators (1 state) */
        &submenu_arrows[0]);
   }

/*
   Update (optinally resize) sub menu
*/
static void sub_menu1_update(void)
   {
   gsetupdate(GUPDATE_OFF);             /* Activate delayed update */
   gm_menu_close(subvp);                /* Remove old menu */
   subvp = sub_menu1_make();            /* Recreate menu (with new size and content) */
   wnmenu_cursor(subvp, subidx, FR_B1); /* Restore cursor at position */
   gsetupdate(GUPDATE_ON);              /* Deactivate delayed update (flush to screen) */
   }

/*
   Sub menu 1 demonstrates different event action methods
      - Selection of next deeper submenu level
      - Toggling of a menu item text
      - Tick on-off a menu item
      - Change of Cursor look depending of state
      - close menu, return to topmenu

   Sub menu 1 event handling:

   Key Up, Key Down      move cursor
   Key Up at top element return to top menu
   Key Backspace         return to top menu
   Key Return            process menu item at index
   Key Left              process menu item at index

   Touch click at index       process menu item at index
   Touch click outside index  move index to menu item
   Touch click at topmenu     return to topmenu
*/
static void sub_menu1_event(EVENT event)
   {
   switch (event)
      {
      case EVENT_KEY_DOWN:
         if (subidx < menuitems(subvp)-1)      /* Move cursor down */
            {
            wnmenu_cursor(subvp, subidx, FR_B1|FR_REMOVE);
            wnmenu_cursor(subvp, ++subidx, FR_B1);
            }
         break;
      case EVENT_KEY_UP:      /* Move cursor up */
         if (subidx > 0)
            {
            wnmenu_cursor(subvp, subidx, FR_B1|FR_REMOVE);
            wnmenu_cursor(subvp, --subidx, FR_B1);
            break;
            }
         /* Up at top element, fall through and make it a submenu close event */

      case EVENT_KEY_ESC:     /* Return to topmenu */
         event_key_esc:
         mnu_state = MNU_TOP;
         gm_menu_close(subvp);      /* Close submenu */
         gm_menu_activate( topvp ); /* Make top menu style the active default */
         /* Swap topmenu cursor type back to non-selected */
         wntopmenu_cursor(topvp,topidx,&topmenu[0],FR_INSERT_THIN|FR_REMOVE);
         wntopmenu_cursor(topvp,topidx,&topmenu[0],FR_PLATE_THIN);
         break;

      case EVENT_TOUCH:       /* Touch event detected */
         {
         SGINT index;
         if ((index = gmenuidx(subvp,touch_x,touch_y)) < 0)
            {
            /* Click was not inside submenu, Check if click is inside topmenu */
            if (ginsidevp(topvp,touch_x,touch_y))
               goto event_key_esc; /* Yes, was a topmenu click. Process it as a menu exit event */
            break;
            }
         if (index != subidx)
            { /* Click done at another item in menu, make a cursor jump */
            if (index < menuitems(subvp))  /* Limit for overflow, just in case */
               {
               wnmenu_cursor(subvp, subidx, FR_B1|FR_REMOVE);
               subidx = index;
               wnmenu_cursor(subvp, subidx, FR_B1);
               }
            break;
            }
         /* Click at cursor element, fall through and make it a selection */
         }
      case EVENT_KEY_RIGHT:    /* Process selection */
      case EVENT_KEY_LEFT:
      case EVENT_KEY_RETURN:
         switch (subidx)
            {
            case 0: /* Select popup screen with dynamic bars*/

               /* Change submenu cursor to dotted */
               wnmenu_cursor(subvp, subidx, FR_DOTTED);
               mnu_state = MNU_POP_AD_MENU;

               /* Create bar popup menu with dynamic placement relative to submenu and top menu */
               #ifdef WM_USE_POPUP_BUFFER
               /* At small screens overlab submenu. Require use of a style which save */
               /* popup background to buffer for later restore. (WM_USE_POPUP_BUFFER defined in compiler setting) */
                         #if (GDISPH < 80)
               popupvp = ad_popup_create(wnxr(subvp)/10, 4, GDISPW*4/5, GDISPH-(5+4));
                         #else
               popupvp = ad_popup_create(wnxr(subvp)/3, wnyb(topvp)+wnfryb()+2, GDISPW*2/3, GDISPH*3/4);
               #endif
               #else
               /* At larger screens align popup view can be beside submenu and under topmenu so */
               /* there is no menu overlab. Then no RAM buffer for popup background storage is needed */
               popupvp = ad_popup_create(wnxr(subvp)+1, wnyb(topvp)+wnfryb()+2, GDISPW*2/4, GDISPH*3/4);
               #endif
               break;

            case 1:  /* Toggle menu text (with optional submenu resize) */
               equalizer_flag = (++equalizer_flag) & 1;
               sub_menu1_update();
               break;

            case 2:  /* Modify check mark (with optional submenu resize) */
               filter_a_flag = (++filter_a_flag) & 1;
               sub_menu1_update();
               break;

            case 3:  /* Open Comm submenu, next to submenu 1, below topmenu */
               wnmenu_cursor(subvp, subidx, FR_DOTTED);
               mnu_state = MNU_SUB_DATACOM;
               /* Use a simple text selection menu for Data com*/
               #if (((GDISPH < 80) || (GDISPW < 132))&& defined(WM_USE_POPUP_BUFFER))
               subvp2 = gm_submenu_open(wnxr(subvp)/3, 0, &submenu_datacom[0]);
               #else
               subvp2 = gm_submenu_open(wnxr(subvp)+1, wnyb(topvp)+wnfryb()+2, &submenu_datacom[0]);
               #endif
               subidx2 = datacom_select + 1;          /* Set index, Skip top title */
               wnmenu_cursor(subvp2, subidx2, FR_B1); /* Activate cursor */
               break;
            default:
               break;
            }
         break;
      default:
         break;
      }
   }

/*
   Sub menu 2 demonstrates implementation of a typical select menu
      - navigation and navigation stop at menu edge items
      - save or skip of new item selection at menu termination
      - close menu, return to "calling menu"

   Sub menu 2 event handling:

   Key Up, Key Down      move cursor
   Key Backspace         return to sub menu1
   Key Left              return to sub menu1
   Key Return            process menu item at index and return to submenu1

   Touch click at index       process menu item at index and return to submenu1
   Touch click outside index  move index to menu item
   Touch click at submenu1    return to submenu 1
*/
static void sub_menu2_event(EVENT event)
   {
   switch (event)
      {
      case EVENT_KEY_DOWN:
         if (subidx2 < menuitems(subvp2)-1)      /* Move cursor down */
            {
            wnmenu_cursor(subvp2, subidx2, FR_B1|FR_REMOVE);
            wnmenu_cursor(subvp2, ++subidx2, FR_B1);
            }
         break;
      case EVENT_KEY_UP:      /* Move cursor up (minus menu title) */
         if (subidx2 > 1)
            {
            wnmenu_cursor(subvp2, subidx2, FR_B1|FR_REMOVE);
            wnmenu_cursor(subvp2, --subidx2, FR_B1);
            }
         break;

      case EVENT_TOUCH:       /* Touch event detected */
         {
         SGINT index;
         if ((index = gmenuidx(subvp2,touch_x,touch_y)) < 0)
            {
            /* Click was not inside submenu2, Check if click is inside submenu1 */
            if (ginsidevp(subvp,touch_x,touch_y))
               goto event_key_esc; /* Yes, was a submenu click. Process it as a menu exit event */
            break;
            }

         if (index != subidx2)
            { /* Click done at another item in menu, make a cursor jump */
            if ((index > 0) && (index < menuitems(subvp2)))  /* Limit for overflow, skip title */
               {
               wnmenu_cursor(subvp2, subidx2, FR_B1|FR_REMOVE);
               subidx2 = index;
               wnmenu_cursor(subvp2, subidx2, FR_B1);
               }
            break;
            }
         /* Click at cursor element, fall through and make it a selection */
         }
      case EVENT_KEY_RETURN:  /* Accept data */
         datacom_select = subidx2 - 1;   /* Save modification */
         /* Optional Add additional state update setting here */
         /* Fall down to close menu */
      case EVENT_KEY_LEFT:    /* Return to submenu 1 */
      case EVENT_KEY_ESC:
         event_key_esc:
         mnu_state = MNU_SUB;
         gm_menu_close(subvp2);      /* Close submenu */
         /* Swap submenu cursor type back to solid frame */
         wnmenu_cursor(subvp, subidx, FR_DOTTED|FR_REMOVE);
         wnmenu_cursor(subvp, subidx, FR_B1);
         break;
      default:
         break;
      }
   }

static void topmenu_close_view(void)
   {
   gm_menu_close(topvp);              /* Close popup menu */
   gselvp(0);                         /* Select default viewport */
   gresetvp();                        /* Init default settings */
   gselfont(TEXTSMALL);
   gclrvp();
   }

static void topmenu_restore_view(void)
   {
   mnu_state = MNU_TOP;
   wn_background_init(G_BLACK,G_WHITE,0);
   topvp = gm_topmenu_open(topmenu);
   wntopmenu_cursor(topvp,topidx,&topmenu[0],FR_PLATE_THIN);
   }

/*************************** main GUI event handler **********************

   Public menu event handler.
   Process all menu related events and manage all menu states.

   The input event may be keyboard events, touch screen events,
   or a time tick event.
   Input events not relevant for the given menu state are ignored (not processed)

   Returns at once when completed

**************************************************************************/
void demo_event(EVENT event)
   {
   switch (mnu_state)
      {
      /* Do first time initialization. Is only activated once after startup */
      case MNU_INIT:
         gresetvp();
         gsetupdate(GUPDATE_OFF);
         wn_background_init(G_BLACK,G_WHITE,0);
         gm_init();  /* Initialize menu GUI lib */

         /* Create top menu */
         mnu_state = MNU_TOP;
         topvp = gm_topmenu_open(topmenu);

         /* Init cursor */
         topidx = 0;
         wntopmenu_cursor(topvp,topidx,&topmenu[0],FR_PLATE_THIN);
         gsetupdate(GUPDATE_ON);
         if (event == EVENT_KEY_NOEVENT)
            break;

      case MNU_TOP:
         /* Handling of top menu events */
         /* Left right = move cursor, Return or Arrow down select item */
         switch (event)
            {
            case EVENT_KEY_LEFT:
               if (topidx > 0)
                  { /* Move cursor to left is not at first element */
                  wntopmenu_cursor(topvp,topidx,&topmenu[0],FR_PLATE_THIN|FR_REMOVE);
                  wntopmenu_cursor(topvp,--topidx,&topmenu[0],FR_PLATE_THIN);
                  }
               break;
            case EVENT_KEY_RIGHT:
               if (topidx < 2)
                  { /* Move cursor to right if not at last element */
                  wntopmenu_cursor(topvp,topidx,&topmenu[0],FR_PLATE_THIN|FR_REMOVE);
                  wntopmenu_cursor(topvp,++topidx,&topmenu[0],FR_PLATE_THIN);
                  }
               break;
            case EVENT_TOUCH:  /* Touch event detected */
               {
               SGINT index;
               if ((index = gtopmenuidx(topvp,&topmenu[0],touch_x,touch_y)) < 0)
                  break;  /* Click was not at topmenu element */
               if (index != topidx)
                  { /* Click done at another item in top menu, make a cursor jump */
                  if (index <= 2)  /* Limit for overflow, just in case */
                     {
                     wntopmenu_cursor(topvp,topidx,&topmenu[0],FR_PLATE_THIN|FR_REMOVE);
                     topidx = index;
                     wntopmenu_cursor(topvp,topidx,&topmenu[0],FR_PLATE_THIN);
                     }
                  break;
                  }
               /* Click was at cursor element, fall through and make it a selection event */
               }
            case EVENT_KEY_DOWN:
            case EVENT_KEY_RETURN:
               {
               wntopmenu_cursor(topvp,topidx,&topmenu[0],FR_INSERT_THIN); /* Set active cursor*/
               switch (topidx)
                  {
                  case 0:
                     mnu_state = MNU_SUB;
                     subvp = sub_menu1_make();
                     /* Activate frame cursor at first line in sub menu */
                     subidx = 0;
                     wnmenu_cursor(subvp, subidx, FR_B1);
                     break;
                  case 1:
                     topmenu_close_view();
                     mnu_state = MNU_PROCESS;
                     /* Full screen canvas ready. Make some centered text as demo output */
                     gsetmode(GALIGN_HCENTER|GALIGN_VCENTER|GWORD_WRAP);
                     gputs(&process_txt[0]);
                     break;
                  case 2:
                     /* Not implemented yet. Just restore cursor and ignore */
                     wntopmenu_cursor(topvp,topidx,&topmenu[0],FR_PLATE_THIN); /* Set passive cursor*/
                     break;
                  default:
                     break;
                  }
               }
            default:
               break;
            }
         break;

      /* Handling of pull down menu events */
      case MNU_SUB:          /* Process submenu events */
         /* Handling of sub menu events */
         sub_menu1_event(event);
         break;

      case MNU_SUB_DATACOM:
         sub_menu2_event(event);
         break;

      /* Handling of AD bar menu events */
      case MNU_POP_AD_MENU:         /* Process popup AD bar events */
         switch (event)
            {
            case EVENT_TOUCH:       /* Touch event detected */
               {
               if (ginsidevp(popupvp,touch_x,touch_y))
                  break;
               /* Click outside window, make it a close event */
               }
            case EVENT_KEY_RETURN:  /* Return to select menu */
            case EVENT_KEY_ESC:
            case EVENT_KEY_LEFT:
               mnu_state = MNU_SUB;
               gm_menu_close(popupvp);              /* Close popup menu */
               wnmenu_cursor(subvp, subidx, FR_B1); /* Set frame cursor */
               break;
            case EVENT_TICK:
               ad_popup_update(); /* Update dynamics in popup window */
            default:
               break;
            }
         break;

      /* Handling of popup menu events */
      case MNU_POP_SENSORS: /* Process popup download events */
         /* Not implemented yet */
         break;

      /* Handling of processing events */
      case MNU_PROCESS:        /* Process popup pressure events */
         switch (event)
            {
            case EVENT_TOUCH:
            case EVENT_KEY_RETURN:
            case EVENT_KEY_ESC:
               topmenu_restore_view(); /* Exit process canvas*/
            default:
               break;
            }
         break;
      default:
         break;
      }
   }

