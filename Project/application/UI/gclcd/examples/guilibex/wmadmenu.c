/*
   Popup view with bars showing "A/D converter levels"
   Support functions called from wmevent.c

   Demonstate use of bar functions and how to create a scaleable
   view based on the library report functions and relative size
   and position calculations.
   This design method creates view functions which provide a
   high degree of automatic sizing to different (screen) sizes
   and screen positions (and a high potential for reuse across projects).
   No fixed coordinate values are used in the view creation.

   2007 RAMTEX Engineering ApS
*/
#include <wmexample.h>
#include <math.h> /* sin() used for creating simulated A/D data */

/* Check configuration settings */
#if (GNUMVP < 8)
 #error Change GNUMVP configuration in gdispcfg.h to a value >= 10 before compiling this demo
#endif

#ifndef GEXTMODE
 #error Enable GEXTMODE in gdispcfg.h.
#endif

static BARPOS ch1,ch2,ch3,ch4;

/*
   Create a popup window, set static info and prepare for bargraph demo

   Most positions and size setting are done relative to each other so the menu
   witn some limitations is dynamically adaptive to screen size adjustments.

   Return viewport number
*/
#define MODE_SELECT_HEIGHT 100

SGUCHAR ad_popup_create(GXT x, GYT y, GYT w, GYT h)
   {
   SGUCHAR vp,vptmp;
   GXT tw,bw;
   GYT fh,ft,ht;

   ht = h;
   ft = wnframe_thicknes(FR_VALLY_LINE);
   tw = 5*gfgetfw(TEXTSMALL); /* text area width (fixed) */
   bw = (w-tw-2*ft)/13;       /* bar area width (dynamic) */

   gselvp((vp = wn_get_vpnum())); /* Pick new viewport for menu */
   vptmp = wn_get_vpnum();
   wnvpcreate_pos(vp,x,y,w,h);    /* Create popup canvas */

   if (ht >= MODE_SELECT_HEIGHT)
      {
      /* height is large enough for some extra decorations */
      gsetmode(GALIGN_HCENTER);
      gselfont(TEXTFAT);
      fh = ggetfh() + 2*ft;

      /* Draw 2 decoration frames */
      wnframe(0,  0,  w-1, fh , FR_VALLY_LINE);
      wnframe(0,fh+1, w-1, h-1, FR_VALLY_LINE);

      /* Draw top text */
      gsetpos(0,ggetfh()+1);
      gputs("Output Levels");
      /* Create a new viewport inside decorations to ease position settings */
      gselvp(vptmp);
      gsetvp(gvpxl(vp)+tw+ft+1,gvpyt(vp)+fh+ft+2,gvpxr(vp)-ft-1,gvpyb(vp)-ft-1);
      }
   else
      /* Create a new viewport inside decorations to ease position settings */
      {
      gselvp(vptmp);
      gsetvp(gvpxl(vp)+tw+ft+1,gvpyt(vp)+ft+1,gvpxr(vp)-ft-1,gvpyb(vp)-ft-1);
      }

   /* Calculate parameters for bar distribution */
   gselfont(TEXTSMALL);
   fh = ggetfh();
   x = gvpxl(vptmp)+bw;
   y = gvpyt(vptmp) + fh + 1;
   h = gvpyt(vptmp) + ggetvph() - 3;

   /* Create 4 vertical bars */
   gsetcolorb(G_WHITE);
   gsetcolorf(G_RED);
   barv_create(&ch1, x, y, x+bw*2, h, FR_W1B1, (ht >= 128) ? 5 : 4);
   gsetcolorf(G_GREEN);
   x+= bw*3;
   barv_create(&ch2, x, y, x+bw*2, h, FR_W1B1, (ht >= 128) ? 5 : 4);
   gsetcolorf(G_YELLOW);
   x+= bw*3;
   barv_create(&ch3, x, y, x+bw*2, h, FR_W1B1, (ht >= 128) ? 5 : 4);
   gsetcolorf(G_BLUE);
   x+= bw*3;
   barv_create(&ch4, x, y, x+bw*2, h, FR_W1B1, (ht >= 128) ? 5 : 4);

   /* Create bar column txt */
   gsetcolorf(G_BLACK);
   gsetcolorb(G_GREY);
   x = ((2*bw)-ggetfw())/2+bw;
   gsetpos(x,fh);
   gputs("A");
   x+=3*bw;
   gsetpos(x,fh);
   gputs("B");
   x+=3*bw;
   gsetpos(x,fh);
   gputs("C");
   x+=3*bw;
   gsetpos(x,fh);
   gputs("D");

   /* Narrow area to ease text scale right alignment */
   gsetvp(gvpxl(vp)+ft+1,y-ggetfh()/2+1,gvpxl(vptmp),h+ggetfh()/2-2);
   gsetmode(GALIGN_RIGHT);
   y = fh;
   if (ht >= MODE_SELECT_HEIGHT)
      {
      fh = (ggetvph()-fh)/5;
      gputs("100%");
      y+=fh;
      gsetpos(0,y);
      gputs("80%");
      y+=fh;
      gsetpos(0,y);
      gputs("60%");
      y+=fh;
      gsetpos(0,y);
      gputs("40%");
      y+=fh;
      gsetpos(0,y);
      gputs("20%");
      y+=fh;
      gsetpos(0,y-1);
      gputs("0%");
      }
   else
      {
      fh = (ggetvph()-fh)/2;
      gputs("100%");
      y+=fh;
      gsetpos(0,y);
      gputs("50%");
      y+=fh;
      gsetpos(0,y-1);
      gputs("0%");
      }

   wn_free_vpnum(vptmp);
   return vp;
   }

/*
   Simulate update of dynamic bar events
*/
void ad_popup_update( void )
   {
   static unsigned int i = 0;
   /* Make sine values which vary bars between 0 and 100 */
   barv_pct(&ch1, (GYT) (sin(3.1415/180.0 * ((double) i  ))*50.0+50.0));
   barv_pct(&ch2, (GYT) (sin(3.1415/180.0 * ((double) i*2))*50.0+50.0));
   barv_pct(&ch3, (GYT) (sin(3.1415/180.0 * ((double) i*4))*50.0+50.0));
   barv_pct(&ch4, (GYT) (sin(3.1415/180.0 * ((double) i*3))*50.0+50.0));
   i++;
   }

