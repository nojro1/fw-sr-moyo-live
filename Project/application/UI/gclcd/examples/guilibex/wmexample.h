/*
   Header file for demo
*/
#ifndef WMEX_H
#define WMEX_H

#include <wnpopup.h>  /* GUI example prototypes */
#include <wnmenu.h>   /* Menu handling example prototypes */

#ifdef __cplusplus
extern "C" {
#endif

/* Define menu events.
   Events from a simple front panel with 6 buttons
      Arrow up
      Arrow down
      Arrow right
      Arrow left
      Return (accept)
      Esc (skip / exit )

   A tick event can be used for timing of dynamic menus
   A touch event indicates that the touch screen has been pressed
*/
typedef enum
   {
   /* Front panel key events */
   EVENT_KEY_NOEVENT,
   EVENT_KEY_UP,
   EVENT_KEY_DOWN,
   EVENT_KEY_LEFT,
   EVENT_KEY_RIGHT,
   EVENT_KEY_RETURN,
   EVENT_KEY_ESC,
   /* Update tick (for dynamic update views) */
   EVENT_TICK,
   /* Touch click detected */
   EVENT_TOUCH,
   /* End of legal events */
   EVENT_LAST
   } EVENT;



/* Use symbolic font names to make demo adaptable to different screen sizes */
#if  ((GDISPH <= 64) || (GDISPW <= 64))
 #define  TEXTSMALL  &footnote
 #define  TEXTFAT    &footnote
#elif ((GDISPH <= 128) || (GDISPW <= 128))
 #define  TEXTSMALL  &footnote
 #define  TEXTFAT    &ms58p
#elif ((GDISPH <= 160) || (GDISPW <= 160))
 #define  TEXTSMALL  &ms58p
 #define  TEXTFAT    &cp8859_9
#else
 #define  TEXTSMALL  &SYSFONT
 #define  TEXTFAT    &narrow20
#endif

/* AD popup menu */
SGUCHAR ad_popup_create(GXT x, GYT y, GYT w, GYT h);
void    ad_popup_update( void );
/* */
void demo_event(EVENT event);


extern GXT touch_x;
extern GYT touch_y;
EVENT wm_keyintf(void);
void wm_keyintf_init(void);

#ifdef __cplusplus
}
#endif


#endif

