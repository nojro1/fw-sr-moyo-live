/*
   Main entry for demo program.
   Do primary initialization and contains the main loop for event polling.

   For detailed explanations see readme.txt and the header in wmevent.c

   2007 RAMTEX Engineering ApS
*/

#include <wmexample.h>

void main(void)
   {
   ginit();                       /* Init display driver */
   wm_keyintf_init();             /* Init keyboard, (plus touch and timer drivers)*/
   demo_event(EVENT_KEY_NOEVENT); /* Init event handler and menu system */

   /* Place other target initialization here */

   /* Main poll loop */
   for(;;)
      {
      EVENT event;
      /* Check for menu related event here (to minimize overhead) */
      if ((event = wm_keyintf()) != EVENT_KEY_NOEVENT)
         demo_event(event);  /* Process menu event */

      /* Place other target handling here */
      }
   }
