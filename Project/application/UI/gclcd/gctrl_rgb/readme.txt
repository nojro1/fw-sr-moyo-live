GENERIC RGB DISPLAY CONTROLLER INTERFACE
========================================
This directory contains files for use of the S6D0129 driver
library in buffered mode as a "generic" library for RGB display
controllers. The high-level functionality is similar to use of 
the normal S6D0129 libray in "buffered mode". 

RAM requirement
---------------
The S6D0129 "generic" driver implementation require that the processor
system has RAM enough to hold a full copy of video buffer in
normal processor RAM.
The RAM byte size for the RAM video buffer is:
   pixel_width * pixel_height * sizeof(pixel_storage_unit)
where sizeof(pixel_storage_unit) is 2 for RGB16 and 4 for RGB18 or RGB24

Use as "generic" display controller during PC simulation
--------------------------------------------------------
The "generic" library can support all display screen sizes and
is therefore ideal when GUI application development should start
before the final display hardware is selected or available.

Simple hardware interface
--------------------------
The "generic" driver is intended for use with a custom display
controller and provide a simple, uniform interface to the
display controller hardware layer.

The few display controller hardware dependent functions are
isolated in one C module. The function bodies is to be
customized for the specific display controller hardware.

From a library point-of-view all display controller hardware
handling and video data updates only use write operations. No
hardware read access are needed. This simplify adaption of the library 
to different types of display controller data bus types, ex
(parallel bus, SPI bus, wireless links, use DMA transfer etc)

Generic RGB controller interface low-level modules
==================================================
Only the low-level initialization and configuration modules in
this directory and sub-directories are specific for the generic
RGB display controller interface:

   ghwbuf.c                 (hardware independent)
   ghwinit.c                (hardware independent)
   ghwinitctrl.c            (hardware specific - to be customized)
   ccfg_genctrl\ghwioini.c  (optionally hardware specific)

The rest of the low-level drivers are common with the S6D0129
LCD controller and should be taken from the \gclcd\s6d0129\
directory and sub-directories. Typically:

   ghwblkrw.c
   ghwfill.c
   ghwgscrl.c
   ghwinv.c
   ghwpixel.c
   ghwretgl.c
   ghwsymrd.c
   ghwsymwr.c

   gsimintf.c
   sim0129.c

The S6D0129 PC simulator is also used for PC simulation with the
generic RGB display controller interface.

Buffered mode operation and flushing
------------------------------------
The S6D0129 maintain and updates the video buffer in RAM.
The updated area is then be flushed to the display controller
hardware via a custom hardware interface function.

The flushing to hardware operation takes place, either upon return
from the high-level library function, or if delayed update is used,
when the application calls gsetupdate(..).
For further details please read the printed manual.


PC simulation mode
===================
The default ghwinitctrl.c module can be used directly in PC
simulation mode in the same way as the normal S6D0129 driver
library.

The switch GHW_PCSIM turns on calls to the simulator inferface
The switch GHW_NOHDW can be used for turning off target processor
specific source code.


IOTester tool and PC mode execution
===================================
The switch IOTESTER_USB activates code used with the RAMTEX IOTester tool.
(For details see www.ramtex.dk/iotester/iotester.htm)

In short the IOTester provides a configurable 8 or 16 bit "embedded bus"
and enable display controller hardware modules to be connected to the PC.
and controlled directly from an application running on the PC, 
i.e. similar to PC simulation mode, but with the real "look and feel".

Beside parallel buses the IOTester-USB tool also provides 
configurable serial busses like SPI and I2C
--

With the switch combination
    GHW_PCSIM
    IOTESTER_USB
it is possible to monitor the GUI application execution
simultaneously on both the PC LCD simulation server window
and on the real display hardware.

Generic hardware interface functions and customization
======================================================

All hardware access and hardware specific I/O is isolated in the
module

   ghwinitctrl.c

The function bodies in this module should be customized for the
specific display controller hardware / target processor system
in accordance with the comments.

The recommended places is marked with a comment starting with:
  *** HARDWARE CUSTOMIZATION ***

The two interface functions for display controller
initialization and flushing must always be customized:

 /* Init display controller in correspondence with the gdispcfg.h settings */
 void ghw_ctrl_init(void);

 /* Flush a linear RAM buffer line segment to display controller video memory */
 void ghw_ctrl_line_wr( GXT xb, GYT yb, GCOLOR *cp, GXT endidx );

This three interface functions only need to be implemented if
the specific functionality is needed by the application /
hardware type.

 /* Turn off display (pulic library function) */
 void ghw_dispoff(void);

 /* Turn on display (public library function) */
 void ghw_dispon(void);

 /* Release display controller hardware ressources */
 void ghw_ctrl_exit(void);


Programming hints
-----------------
In ghwinitctrl.c the compilers native syntax for I/O access may be used.

For source code portability with PC simulation mode it is recommended
to keep target specific code within the compilation switches

  #ifndef GHW_NOHDW

  // your target specific source code

  #endif /* !GHW_NOHDW */

Of SG access interface files only sgtypes.h are used by the RAMTEX library.
The rest of the sgxxx.h files can be ignored.



