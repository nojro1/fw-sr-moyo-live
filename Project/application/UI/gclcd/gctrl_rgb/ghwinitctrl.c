/************************** ghwinitctrl.c *****************************

   Low-level generic driver functions for used of the s6d0129 low-level
   driver modules in buffered mode only.

   PC-simulation mode
   ------------------
   This module can be used directly in PC simulation mode (no customization needed)

   Target execution mode
   ---------------------
   This module must customized for the specific display controller hardware
   in accordance with the comments.

   The relevant places are marked with *** HARDWARE CUSTOMIZATION ***
   Target processor specific code should be encapsulated within the GHW_NOHDW switches
   to assure the same source module can be used with PC simulation as well.

   Revision date:    25-03-2012
   Revision Purpose: Customized version for use with generic display
                     controller interface only.

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2012

*********************************************************************/

#include <gen_ctrl.h>  /* controller specific definements */

#ifndef GHW_NOHDW
/* #include <my_target.h> */      /* Portable I/O functions + hardware port def for compiler */
#endif

#ifndef GHW_GCTRL_RGB
 #error Unknown controller (or wrong gdispcfg.h file)
#endif

#ifndef GBUFFER
  #error This generic interface require use of buffered mode (define GBUFFER in gdispcfg.h)
#endif

#ifdef GBASIC_INIT_ERR

/************* simulator interface ************************/

#ifdef GHW_PCSIM
/* PC simulator declarations */
void ghw_set_xy_sim(GXT xb, GYT yb);
void ghw_autowr_sim( GCOLOR cval );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
#endif

/*
   Initialize display controller hardware
   --------------------------------------

   This function is activated once when ghw_init() is called via ginit().

   The display controller hardware should be initialized based on the
   gdispcfg.h configuration so it tracks the rest of the library.

   In particular these common defines must typically be considered:

       GDISPW    pixel width of display
       GDISPH    pixel height of display
       GDISPPIXW number of bits pr pixel( ex 16,24,32)
       GCOLOR    type to hold pixel info (sizeof( GCOLOR )*8 must be >= GDISPPIXW )

   Depending on the display controller hardware flexibility then these low-level
   switches in gdispcfg.h are commonly used during hardware configurations of
   display controllers.
   However the switches are not used by the rest of the library, and can therefore be ignored
   if not needed by the application, or the display module (screen layout) type is always fixed.

      GHW_MIRROR_VER   Mirror the vertical display scan   (= increment or decrement of x scan counter)
      GHW_MIRROR_HOR   Mirror the horizontal display scan (= increment or decrement of y scan counter)
                       (mirroring both scan directions = 180 degree rotation of image view)
      GHW_ROTATED      Rotate display scan 90 (270) degrees (= increment y scan counter before x scan counter )
                       (remember to swap values used in GDISPH,GDISPW definitions)
      GHW_COLOR_SWAP   Define/undefine to change R,G,B order to B,G,R order (adaption to physical screen color lane layout)
      GHW_INV_VDATA    Invert video data (adaption to display module signal levels)
*/
void ghw_ctrl_init(void)
   {
   ghw_io_init(); /* Set any hardware interface lines, display module hardware reset */

   #ifndef GHW_NOHDW
   /*** HARDWARE CUSTOMIZATION ***
     Insert display controller specific initialization here
   */
   #endif

   #ifdef IOTESTER_USB
   iot_sync(IOT_SYNC);
   #endif
   }

/*
   Release any hardware ressources
   ( called by ghw_exit() )
   If not needed by the target system then just let this function be unmodified.
*/
void ghw_ctrl_exit(void)
   {
   ghw_io_exit(); /* reset any hardware interface lines to idle */

   #ifndef GHW_NOHDW
   /*** HARDWARE CUSTOMIZATION ***
     Insert display controller shut down (ex stop scan, full power down)
   */
   #endif

   #ifdef IOTESTER_USB
   iot_sync(IOT_SYNC);
   #endif
   }

/*
   Turn display off  (public hardware interface function)
*/
void ghw_dispoff(void)
   {
   #ifndef GHW_NOHDW
   /*** HARDWARE CUSTOMIZATION ***
      Inset hardware commands to turn off display module, any backlight, etc)
   */
   #endif

   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif
   #ifdef IOTESTER_USB
   iot_sync(IOT_SYNC);
   #endif
   }

/*
   Turn display on   (public hardware interface function)
*/
void ghw_dispon(void)
   {
   #ifndef GHW_NOHDW
   /*** HARDWARE CUSTOMIZATION ***
      Inset hardware commands to turn on display modules, any backlight, etc
   */
   #endif

   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif
   #ifdef IOTESTER_USB
   iot_sync(IOT_SYNC);
   #endif
   }

/**************** Data bus transmission and format handling **********************/

/*
   Flush of (logially horizontal) video line segment to controller hardware.

     *cp     pointer to array of GCOLOR elements
     xb,yb   logical screen pixel position for cp[0]. (0,0 is upper-left corner)
     endidx  index of last cp array element to output (max value is GDISPW-1)

    Any software based RGB color format conversion can be implemented in this
    module as well (ex RGB, BGR swapping, byte flushing order, etc).
*/
void ghw_ctrl_line_wr( GXT xb, GYT yb, GCOLOR *cp, GXT endidx )
   {
   GXT cnt;

   #ifdef GHW_PCSIM
   /* Init simulated hardware pointers to start at logical position xb,yb */
   ghw_set_xy_sim(xb, yb);

   /* Copy data to simulated hardware */
   for (cnt = 0;;)
      {
      /* hardware stream register = cp[cnt]; */
      ghw_autowr_sim(cp[cnt]);
      if (cnt == endidx)
         break;
      cnt++;
      }
   #endif

   #ifndef GHW_NOHDW
   /*** HARDWARE CUSTOMIZATION ***/

   /* Insert init of hardware pointers to start at logical position xb,yb here */

   for (cnt = 0;;)  /* Copy to hardware loop */
      {
      /* Insert write to hardware here */

      /* hardware_stream_register = cp[cnt]; */

      if (cnt == endidx) /* End of line segment ? */
         return;         /* Normal exit */
      cnt++;
      }
   #endif
   }

#endif /* GBASIC_INIT_ERR */




