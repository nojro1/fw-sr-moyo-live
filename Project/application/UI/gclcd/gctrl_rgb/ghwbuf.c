/*********************** ghwbuf.c *****************************

   Low-level driver functions for the S6D0129 LCD display buffer
   handling. Constraints low-level functions for optimized buffer
   handling in buffered mode (GBUFFER defined) when softfonts and
   graphic is used.

   NOTE: These functions are only called by the GDISP high-level
   functions. They should not be used directly from user programs.

   Revision date:    25-03-2012
   Revision Purpose: Customized version for use with generic display
                     controller interface only.

   Version number: 1.00
   Copyright (c) RAMTEX Engineering Aps 2007-2012

*********************************************************************/
#include <gdisphw.h>    /* HW driver prototypes and types */
#include <gen_ctrl.h>   /* controller specific definements */

#ifdef GBASIC_INIT_ERR
/****************************************************************
 ** functions for internal implementation
 ****************************************************************/

/*
   Update HW with buffer content if buffered driver else nothing
*/
#ifdef GBUFFER

void ghw_updatehw(void)
   {
   if (ghw_upddelay)
      return;

   GBUF_CHECK();
   glcd_err = 0;

   /* update invalid rect */
   if (( irby >= ilty ) && ( irbx >= iltx ))
      {
      if( irby >= GDISPH ) irby = GDISPH-1;
      if( irbx >= GDISPW ) irbx = GDISPW-1;

      /* Loop update block rows */
      for (irbx -= iltx; ilty <= irby; ilty++)
         {
         /* Flush line segment */
         ghw_ctrl_line_wr( iltx, ilty, &gbuf[GINDEX(iltx,ilty)], irbx);
         }

      /* Invalidate dirty area range */
      iltx = 1;
      ilty = 1;
      irbx = 0;
      irby = 0;
      }
   #if (defined(_WIN32) && defined(GHW_PCSIM))
   GSimFlush();
   #endif
   #ifdef IOTESTER_USB
   iot_sync(IOT_SYNC);
   #endif
   }

/*
   Set updatehw to instant update or delayed update
      1 = Normal screen update from buffer
      0 = Update from buffer stopped until normal update is selected again

   Activated from gsetupdate(on);
*/
GUPDATE ghw_setupdate( GUPDATE update )
   {
   GUPDATE old_update;
   old_update = (GUPDATE) (ghw_upddelay == 0);
   if ((update != 0) && (ghw_upddelay != 0))
      {
      /* Update is re-activated, make a screen update. */
      ghw_upddelay = 0;  /* Flag = 0 -> no delayed update */
      ghw_updatehw();
      }
   else
      ghw_upddelay = (update == 0);
   return old_update;
   }
#endif  /* GBUFFER */

#endif  /* GBASIC_INIT_ERR */


