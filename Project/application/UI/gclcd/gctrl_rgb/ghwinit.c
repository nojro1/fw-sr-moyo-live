/************************** ghwinit.c *****************************

   Low-level generic driver functions for used of the s6d0129 low-level
   driver modules in buffered mode only.


   Revision date:    25-03-2012
   Revision Purpose: Customized version for use with generic display
                     controller interface and buffered mode only.
                     This module is made independent of a particular hardware.
                     All hardware dependent I/O and initialization is now located
                     in the ghwinitctrl.c module.

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2012

*********************************************************************/

#ifdef SGPCMODE
#include "windows.h"  /* Sleep(..) function used by ghw_cmd_wait(..)*/
#endif

#include <gen_ctrl.h>   /* controller specific definements */

#ifdef GHW_PCSIM
/* PC simulator declaration */
void ghw_init_sim( SGUINT dispw, SGUINT disph );
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye);
void ghw_exit_sim(void);
#endif

#ifdef GBASIC_INIT_ERR

/***********************************************************************/
/** All static LCD driver data is located here in this ghwinit module **/
/***********************************************************************/

/* Active foreground and background color */
GCOLOR ghw_def_foreground;
GCOLOR ghw_def_background;

#if (GHW_PALETTE_SIZE > 0)
/* Default soft palette
   The palette file can be edited directly with the ColorIconEdit program
*/
static GCODE GPALETTE_RGB FCODE ghw_palette[GHW_PALETTE_SIZE] =
     #include <gcolor_4.pal>
     ;

/* Operative palette (current palette used for color lookup) */
GCOLOR ghw_palette_opr[GHW_PALETTE_SIZE];
#endif

/* Use software font */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR  b[8];           /* Symbol data, fixed size = 8 bytes */
   }
GCODE FCODE sysfontsym[0x80] =
   {
   /* The default font MUST be a monospaced black & white (two-color) font */
   #include <sfs0129.sym> /* System font symbol table */
   };

/* Default system font */
GCODE GFONT FCODE SYSFONT =
   {
   6,      /* width */
   8,      /* height */
   sizeof(sysfontsym[0])-sizeof(GSYMHEAD), /* number of data bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL) sysfontsym,  /* pointer to array of SYMBOLS */
   0x80,   /* num symbols in sysfontsym[] */
   NULL    /* pointer to code page */ /* NULL means code page is not used */
   };

#ifdef GBUFFER
   #ifdef GHW_ALLOCATE_BUF
      /* <stdlib.h> is included via gdisphw.h */
      GCOLOR *gbuf = NULL;           /* Graphic buffer pointer */
      static SGBOOL gbuf_owner = 0;   /* Identify pointer ownership */
   #else
      GCOLOR gbuf[GBUFSIZE];         /* Graphic buffer */
   #endif
   GXT GFAST iltx,irbx;     /* "Dirty area" speed optimizers in buffered mode */
   GYT GFAST ilty,irby;
   SGBOOL  ghw_upddelay;    /* Flag for delayed update */
#else
   #if ((defined( GBASIC_TEXT ) || defined(GSOFT_FONTS) || defined(GGRAPHIC)) && !defined(GHW_NO_LCD_READ_SUPPORT))
   GCOLOR ghw_tmpbuf[GDISPW]; /* Row line buffer (for fast block read-modify-write) */
   #endif
#endif /* GBUFFER */


#ifdef GHW_INTERNAL_CONTRAST
static SGUCHAR ghw_contrast;/* Current contrast value */
#endif

SGBOOL glcd_err;            /* Internal error */
#ifndef GNOCURSOR
GCURSOR ghw_cursor;         /* Current cursor state */
#endif

/* Bit mask values */
GCODE SGUCHAR FCODE sympixmsk[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

/***********************************************************************/
/**        s6d0129 Initialization and error handling functions       **/
/***********************************************************************/

SGBOOL ghw_newcolor;

/*
   Change default (palette) colors
*/
void ghw_setcolor(GCOLOR fore, GCOLOR back)
   {
   /* Update active colors */
   #if (GDISPPIXW == 24)
   /* Mask unused bits to ease color comparation */
   fore &= 0xfcfcfc;
   back &= 0xfcfcfc;
   #elif (GDISPPIXW == 32)
   fore |= 0xff000000;
   back |= 0xff000000;
   #endif
   if ((ghw_def_foreground != fore) || (ghw_def_background != back))
      {
      ghw_newcolor = 1;
      ghw_def_foreground = fore;
      ghw_def_background = back;
      }
   }


/*
   Convert RGB palette color to controller configuration specific GCOLOR value
*/
GCOLOR ghw_rgb_to_color( GCONSTP GPALETTE_RGB *palette )
   {
   if (palette == NULL)
      return (GCOLOR) 0;

   return G_RGB_TO_COLOR(palette->r,palette->g,palette->b);
   }

#if (GHW_PALETTE_SIZE > 0)
/*
   Load a new palette or update the existing palette
   (Palette is only used with symbols using 2 or 4 bits pr pixel)
*/
SGBOOL ghw_palette_wr(SGUINT start_index, SGUINT num_elements, GCONSTP GPALETTE_RGB PFCODE *palette)
   {
   if ((num_elements == 0) || (palette == NULL) ||
       (start_index >= sizeof(ghw_palette_opr)/sizeof(ghw_palette_opr[0])))
      {
      glcd_err = 1;
      return 1;
      }

   if ((start_index + num_elements) > sizeof(ghw_palette_opr)/sizeof(ghw_palette_opr[0]))
         num_elements = sizeof(ghw_palette_opr)/sizeof(ghw_palette_opr[0]) - start_index;

   glcd_err = 0;

   /* (Partial) update of operative palette values */
   while(num_elements-- > 0)
      {
      ghw_palette_opr[start_index++] = G_RGB_TO_COLOR(palette->r,palette->g,palette->b);
      palette++;
      }

   return glcd_err;
   }
#endif

/*
   Fast set or clear of LCD module RAM buffer
   Internal ghw function
*/
static void ghw_bufset(GCOLOR color)
   {
   GBUFINT cnt;
   cnt = 0;
   do
      {
      /* Clear using X,Y autoincrement */
      gbuf[cnt] = color; /* Set ram buffer as well */
      }
   while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH)); /* Loop until x+y wrap */

   iltx = 0;
   ilty = 0;
   irbx = GDISPW-1;
   irby = GDISPH-1;
   ghw_upddelay = 0;
   ghw_updatehw();  /* Flush initialized buffer to display hdw or simulator memory */
   }

/*
   Initialize display, clear ram  (low-level)
   Clears glcd_err status before init

   Return 0 if no error,
   Return != 0 if some error
*/
SGBOOL ghw_init(void)
   {
   glcd_err = 0;

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL)
      {
      /* Allocate graphic ram buffer */
      if ((gbuf = (GCOLOR *)calloc(ghw_gbufsize(),1)) == NULL)
         glcd_err = 1;
      else
         gbuf_owner = 1;
      }
   #endif

   if (glcd_err != 0)
      return 1;

   #ifdef GHW_PCSIM
   /* Tell simulator about the visual LCD screen organization */
   ghw_init_sim( GDISPW, GDISPH );
   ghw_set_xyrange_sim(0, 0, GDISPW-1, GDISPH-1);
   #endif
   /* Set default colors */
   ghw_setcolor( GHW_PALETTE_FOREGROUND, GHW_PALETTE_BACKGROUND );

   #if (GHW_PALETTE_SIZE > 0)
   /* Load palette */
   ghw_palette_wr(0, sizeof(ghw_palette)/sizeof(ghw_palette[0]), (GCONSTP GPALETTE_RGB PFCODE *)&ghw_palette[0]);
   #endif

   /* Initialize controller according to configuration file */
   ghw_ctrl_init();
   ghw_bufset( ghw_def_background );

   #ifndef GNOCURSOR
   ghw_cursor = GCURSIZE1;    /* Cursor is off initially */
   /* ghw_cursor = GCURSIZE1 | GCURON; */ /* Uncomment to set cursor on initially */
   #endif

   ghw_dispon();  /* Turn on display */

   return (glcd_err != 0) ? 1 : 0;
   }

/*
   Return last error state. Called from applications to
   check for LCD HW or internal errors.
   The error state is reset by ghw_init and all high_level
   LCD functions.

   Return == 0 : No errors
   Return != 0 : Some errors
*/
SGUCHAR ghw_err(void)
   {
   #if (defined(_WIN32) && defined( GHW_PCSIM))
   if (GSimError())
      return 1;
   #endif
   return (glcd_err == 0) ? 0 : 1;
   }


/*
   Display a (fatal) error message.
   The LCD display module is always cleared and initialized to
   the system font in advance.
   The error message is automatically centered on the screen
   and any \n characters in the string is processed.

   str = ASCII string to write at display center
*/
void ghw_puterr( PGCSTR str )
   {
   PGCSTR idx;
   SGUINT xcnt;
   GXT xp;
   GYT yp,h,y, sidx;
   PGSYMBYTE psym;
   GCOLOR pval;
   SGUCHAR val;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   #endif

   if (ghw_init() != 0)  /* (Re-) initialize display */
      return;            /* Some initialization error */

   /* Count number of lines in string */
   idx=str;
   if (idx == NULL)
      return;
   xcnt = 1;
   while(*idx)
      {
      if (*(idx++) == '\n')
         xcnt++;
      }

   /* Set start character line */
   h = SYSFONT.symheight;
   yp = (xcnt*h > GDISPH) ? 0 : ((GDISPH-1)-xcnt*h)/2;
   /* Set character height in pixel lines */

   idx=str;
   do
      {
      xcnt=0;  /* Set start x position so line is centered */
      while ((idx[xcnt]!=0) && (idx[xcnt]!='\n') && (xcnt < GDISPBW))
         {
         xcnt++;
         }

      /* Calculate start position for centered line */
      xp = (GDISPW-xcnt*SYSFONT.symwidth)/2;

      /* Display text line */
      while (xcnt-- > 0)
         {
         /* Point to graphic content for character symbol */
         psym = &(sysfontsym[(*idx) & 0x7f].b[0]);

         /* Update "dirty area" markers */
         invalrect( xp, yp );
         invalrect( xp+SYSFONT.symwidth-1, yp+(h-1) );

         /* Display rows in symbol */
         for (y = 0; y < h; y++)
            {
            /* Get symbol row value */
            val = *psym++;
            /* Initiate LCD controller address pointer */
            gbufidx = GINDEX(xp, (GBUFINT)yp+y );

            /* Display colums in symbol row */
            for (sidx = 0; sidx < SYSFONT.symwidth; sidx++)
               {
               if ((val & sympixmsk[sidx]) != 0)
                  pval = ghw_def_foreground;
               else
                  pval = ghw_def_background;

               /* End of symbol or end of byte reached */
               gbuf[gbufidx++] = pval;
               }
            }

         idx++;
         xp += SYSFONT.symwidth; /* Move to next symbol in line */
         }

      /* Next text line */
      yp += h;
      if (*idx == '\n')
         idx++;
      }
   while ((*idx != 0) && (yp < GDISPH));

   ghw_updatehw();  /* Flush to display hdw or simulator */
   }

/*
   Release low-level display ressources (called by gexit() )
*/
void ghw_exit(void)
   {
   ghw_ctrl_exit();    /* Release any LCD hardware resources, if required */

   #if defined( GHW_ALLOCATE_BUF)
   if (gbuf != NULL)
      {
      if (gbuf_owner != 0)
         {
         /* Buffer is allocated by ginit, so release graphic buffer here */
         free(gbuf);
         gbuf_owner = 0;
         }
      gbuf = NULL;
      }
   #endif

   #ifdef GHW_PCSIM
   ghw_exit_sim(); /* Release simulator resources */
   #endif
   }

#ifndef GNOCURSOR
/*
   Replace cursor type data (there is no HW cursor support in s6d0129)
*/
void ghw_setcursor( GCURSOR type)
   {
   ghw_cursor = type;
   #ifdef GHW_ALLOCATE_BUF
   if (gbuf == NULL)
      glcd_err = 1;
   #endif
   }
#endif


#if defined( GHW_ALLOCATE_BUF)
/*
   Size of buffer requied to save the whole screen state
*/
GBUFINT ghw_gbufsize( void )
   {
   return (GBUFINT) (((GBUFINT)GBUFSIZE) * sizeof(GCOLOR) + (GBUFINT) sizeof(GHW_STATE));
   }

#ifdef GSCREENS
/*
   Check if screen buf owns the screen ressources.
*/
SGUCHAR ghw_is_owner( SGUCHAR *buf )
   {
   return (((GCOLOR *)buf == gbuf) && (gbuf != NULL)) ? 1 : 0;
   }

/*
   Save the current state to the screen buffer
*/
SGUCHAR *ghw_save_state( SGUCHAR *buf )
   {
   GHW_STATE *ps;
   if (!ghw_is_owner(buf))
      return NULL;

   ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);
   ps->upddelay = (ghw_upddelay != 0);
   #ifndef GNOCURSOR
   ps->cursor = ghw_cursor;
   #endif
   ps->foreground = ghw_def_foreground; /* Palette may vary, save it */
   ps->background = ghw_def_background;
   return (SGUCHAR *) gbuf;
   }

/*
   Set state to buf.
   If buffer has not been initiated by to a screen before, only
   the pointer is updated. Otherwise the the buffer
*/
void ghw_set_state(SGUCHAR *buf, SGUCHAR doinit)
   {
   if (gbuf != NULL)
      {
      /* The LCD controller has been initiated before */
      if (gbuf_owner != 0)
         {
         /* Buffer was allocated by ginit, free it so screen can be used instead*/
         free(gbuf);
         gbuf_owner = 0;
         gbuf = NULL;
         }
      }

   if (doinit != 0)
      {
      /* First screen initialization, just set buffer pointer and
         leave rest of initialization to a later call of ghw_init() */
      gbuf = (GCOLOR *) buf;
      gbuf_owner = 0;
      }
   else
      {
      if ((gbuf = (GCOLOR *) buf) != NULL)
         {
         GHW_STATE *ps;
         ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);

         #ifndef GNOCURSOR
         ghw_cursor = ps->cursor;
         #endif
         ghw_upddelay = 0;        /* Force update of whole screen */
         iltx = 0;
         ilty = 0;
         irbx = GDISPW-1;
         irby = GDISPH-1;
         ghw_updatehw();
         ghw_upddelay = (ps->upddelay != 0) ? 1 : 0;
         /* Restore drawing color */
         ghw_setcolor(ps->foreground, ps->background);
         }
      }
   }
#endif  /* GSCREENS */
#endif  /* GHW_ALLOCATE_BUF */

#endif /* GBASIC_INIT_ERR */



