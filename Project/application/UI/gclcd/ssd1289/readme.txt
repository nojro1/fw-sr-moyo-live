For SSD1289 only the low-level intitialization and configuration files 
in this directory and subdirectories are specific for the SSD1289 display 
controller family. 

The rest of the low-level drivers are common with the S6D0129 LCD 
controller and should be taken from the  \GCLCD\S6D0129\ directory
and sub directories:
	ghwblkrw.c
	ghwbuf.c
	ghwfill.c
	ghwgscrl.c
	ghwinv.c
	ghwpixel.c
	ghwretgl.c
	ghwsymrd.c
	ghwsymwr.c
	gfgio\ghwioini.c

      gsimintf.c
      sim0129.c

The S6D0129 PC simulator is also used for SSD1289 simulation.
