/***************************** bussim.c ************************************

   Generic template file for external bus simulator drivers.

   The functions bodies below must be implemented as described in the comments.
   Use the proper I/O port instructions given by the actual processor
   architecture, the actual LCD connection method and the I/O access
   capabilities of the target C compiler.

   Alternatively this module can be used as a universal stub during initial
   test compilation of the target software.

   The functions in this module is called by single-chip-processor version
   of the ghwinit.c module.

   Copyright (c) RAMTEX Engineering Aps 2006-2009

****************************************************************************/
#ifndef GHW_NOHDW
#ifdef  GHW_SINGLE_CHIP

#include <bussim.h>

/*#include < Port declaration file for your compiler > */
#include "misc.h"


#ifdef GHW_BUS8
/*
   Simulate a bus write operation for a LCD controller with an Intel
   like bus interface (i.e. use of separate /RD and /WR strobes).
   This access function is 8 bit processor data bus interfaces.

   The address parameter adr is assumed to be either 0 or 1.
*/

static GPIO_InitTypeDef GPIO_InitStructure;

void simwrby(SGUCHAR adr, SGUCHAR dat)
{
    /* A: Set C/D line according to adr, Set /CE line active low */
    if (0==adr)
        GPIO_WriteBit(GPIOD, GPIO_Pin_11, Bit_RESET);
    else
        GPIO_WriteBit(GPIOD, GPIO_Pin_11, Bit_SET);
    
    GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_RESET);
        
    /* B1: Make data port an output (if required by port architecture) */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    
    /* B2: Write data to data port */
    GPIO_WriteBit(GPIOE, GPIO_Pin_11, (dat&0x01)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_12, (dat&0x02)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_13, (dat&0x04)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_14, (dat&0x08)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_15, (dat&0x10)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOD, GPIO_Pin_8, (dat&0x20)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOD, GPIO_Pin_9, (dat&0x40)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOD, GPIO_Pin_10, (dat&0x80)==0 ? Bit_RESET : Bit_SET);
    
    /* C: Set /WR active low, (Delay min 80 ns), */
    GPIO_WriteBit(GPIOD, GPIO_Pin_5, Bit_RESET);
    for(SGUCHAR i=0; i<10; i++)
    {
        GPIO_WriteBit(GPIOD, GPIO_Pin_5, Bit_RESET);
    }
    
    /* D: Set /WR passive high */
    GPIO_WriteBit(GPIOD, GPIO_Pin_5, Bit_SET);
    /* E: Set /CE passive high */
    GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_SET);
   }

/*
   Simulate a bus read operation for a LCD controller with an Intel
   like bus interface (i.e. use of separate /RD and /WR strobes).
   This access function is 8 bit processor data bus interfaces.

   The address parameter adr is assumed to be either 0 or 1.
*/
SGUCHAR simrdby(SGUCHAR adr)
   {
   SGUCHAR dat = 0;
   /* a: Set C/D line according to adr. Set /CE line active low */
   if (0==adr)
        GPIO_WriteBit(GPIOD, GPIO_Pin_11, Bit_RESET);
    else
        GPIO_WriteBit(GPIOD, GPIO_Pin_11, Bit_SET);
    
    GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_RESET);

   /* b: Make data port an input (if required by port architecture) */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

   /* c: Set /RD active low, (Delay min 150ns), */
    GPIO_WriteBit(GPIOD, GPIO_Pin_4, Bit_RESET);
    for(SGUCHAR i=0; i<10; i++)
    {
        GPIO_WriteBit(GPIOD, GPIO_Pin_4, Bit_RESET);
    }
   /* d: Read data from data port */
    dat = GPIO_ReadOutputDataBit(GPIOE, GPIO_Pin_11);
    dat |= (GPIO_ReadOutputDataBit(GPIOE, GPIO_Pin_12) << 1);
    dat |= (GPIO_ReadOutputDataBit(GPIOE, GPIO_Pin_13) << 2);
    dat |= (GPIO_ReadOutputDataBit(GPIOE, GPIO_Pin_14) << 3);
    dat |= (GPIO_ReadOutputDataBit(GPIOE, GPIO_Pin_15) << 4);
    dat |= (GPIO_ReadOutputDataBit(GPIOD, GPIO_Pin_8) << 5);
    dat |= (GPIO_ReadOutputDataBit(GPIOD, GPIO_Pin_9) << 6);
    dat |= (GPIO_ReadOutputDataBit(GPIOD, GPIO_Pin_10) << 7);
    
    /* e1:Set /RD passive high, */
    GPIO_WriteBit(GPIOD, GPIO_Pin_4, Bit_SET);
   /* e2:Set /CE passive high (could be ignored) */
    GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_RESET);
   return dat;
   }

#elif defined( GHW_BUS16 )
/* 16 bit bus mode */
static GPIO_InitTypeDef GPIO_InitStructure;

/*
   Simulate a bus write operation for a LCD controller with an Intel
   like bus interface (i.e. use of separate /RD and /WR strobes).
   This access function is 16 bit processor data bus interfaces.

   The address parameter adr is assumed to be either 0 or 1.
*/
void simwrwo(SGUCHAR adr, SGUINT dat)
   {
   /* A: Set C/D line according to adr, Set /CE line active low */
    if (0==adr)
        GPIO_WriteBit(GPIOD, GPIO_Pin_11, Bit_RESET);
    else
        GPIO_WriteBit(GPIOD, GPIO_Pin_11, Bit_SET);
    
    GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_RESET);
        
    /* B1: Make data port an output (if required by port architecture) */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    
    /* B2: Write data to data port */
    GPIO_WriteBit(GPIOD, GPIO_Pin_14, (dat&0x0001)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOD, GPIO_Pin_15, (dat&0x0002)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOD, GPIO_Pin_0, (dat&0x0004)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOD, GPIO_Pin_1, (dat&0x0008)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_7, (dat&0x0010)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_8, (dat&0x0020)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_9, (dat&0x0040)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_10, (dat&0x0080)==0 ? Bit_RESET : Bit_SET);
    
    GPIO_WriteBit(GPIOE, GPIO_Pin_11, (dat&0x0100)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_12, (dat&0x0200)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_13, (dat&0x0400)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_14, (dat&0x0800)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOE, GPIO_Pin_15, (dat&0x1000)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOD, GPIO_Pin_8, (dat&0x2000)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOD, GPIO_Pin_9, (dat&0x4000)==0 ? Bit_RESET : Bit_SET);
    GPIO_WriteBit(GPIOD, GPIO_Pin_10, (dat&0x8000)==0 ? Bit_RESET : Bit_SET);
    
    /* C: Set /WR active low, (Delay min 80 ns), */
    GPIO_WriteBit(GPIOD, GPIO_Pin_5, Bit_RESET);
    for(SGUCHAR i=0; i<10; i++)
    {
        GPIO_WriteBit(GPIOD, GPIO_Pin_5, Bit_RESET);
    }
    
    /* D: Set /WR passive high */
    GPIO_WriteBit(GPIOD, GPIO_Pin_5, Bit_SET);
    /* E: Set /CE passive high */
    GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_SET);
   }

/*
   Simulate a bus read operation for a LCD controller with an Intel
   like bus interface (i.e. use of separate /RD and /WR strobes).
   This access function is 16 bit processor data bus interfaces.

   The address parameter adr is assumed to be either 0 or 1.
*/
SGUINT simrdwo(SGUCHAR adr)
   {
   SGUINT dat = 0;
   /* a: Set C/D line according to adr. Set /CE line active low */
   if (0==adr)
        GPIO_WriteBit(GPIOD, GPIO_Pin_11, Bit_RESET);
    else
        GPIO_WriteBit(GPIOD, GPIO_Pin_11, Bit_SET);
    
    GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_RESET);

   /* b: Make data port an input (if required by port architecture) */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOE, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_Init(GPIOD, &GPIO_InitStructure);

   /* c: Set /RD active low, (Delay min 150ns), */
    GPIO_WriteBit(GPIOD, GPIO_Pin_4, Bit_RESET);
    for(SGUCHAR i=0; i<1; i++)
    {
        GPIO_WriteBit(GPIOD, GPIO_Pin_4, Bit_RESET);
    }
    
   /* d: Read data from data port */
    dat = GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_14);
    dat |= (GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_15) << 1);
    dat |= (GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_0) << 2);
    dat |= (GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_1) << 3);
    dat |= (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_7) << 4);
    dat |= (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_8) << 5);
    dat |= (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_9) << 6);
    dat |= (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_10) << 7);
    
    dat |= (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_11) << 8);
    dat |= (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_12) << 9);
    dat |= (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_13) << 10);
    dat |= (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_14) << 11);
    dat |= (GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_15) << 12);
    dat |= (GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_8) << 13);
    dat |= (GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_9) << 14);
    dat |= (GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_10) << 15);
    
    /* e1:Set /RD passive high, */
    GPIO_WriteBit(GPIOD, GPIO_Pin_4, Bit_SET);
   /* e2:Set /CE passive high (could be ignored) */
    GPIO_WriteBit(GPIOD, GPIO_Pin_7, Bit_SET);
   return dat;
   }

#else
/* 32 bit bus mode (the display may use the lsb bits of the dat parameter) */

/*
   Simulate a bus write operation for a LCD controller with an Intel
   like bus interface (i.e. use of separate /RD and /WR strobes).
   This access function is 32 (18) bit processor data bus interfaces.

   The address parameter adr is assumed to be either 0 or 1.
*/
void simwrdw(SGUCHAR adr, SGLONG dat)
   {
   /* A: Set C/D line according to adr, Set /CE line active low */
   /* B1: Make data port an output (if required by port architecture) */
   /* B2: Write data to data port */
   /* C: Set /WR active low, (Delay min 80 ns), */
   /* D: Set /WR passive high */
   /* E: Set /CE passive high */
   }

/*
   Simulate a bus read operation for a LCD controller with an Intel
   like bus interface (i.e. use of separate /RD and /WR strobes).
   This access function is 32 (18) bit processor data bus interfaces.

   The address parameter adr is assumed to be either 0 or 1.
*/
SGULONG simrddw(SGUCHAR adr)
   {
   SGULONG dat = 0;
   /* a: Set C/D line according to adr. Set /CE line active low */
   /* b: Make data port an input (if required by port architecture) */
   /* c: Set /RD active low, (Delay min 150ns), */
   /* d: Read data from data port */
   /* e1:Set /RD passive high, */
   /* e2:Set /CE passive high (could be ignored) */
   return dat;
   }

#endif

/*
  Initialize and reset LCD display.
  Is called before simwrby() and simrdby() is invoked for the first time

  The controller reset line is toggled here if it connected to a bus port.
  (it may alternatively be hard-wired to the reset signal to the processors
  in the target system).

  The sim_reset() function is invoked automatically via the ginit() function.
*/
void sim_reset( void )
   {
   /* 1. Init data port setup (if required by port architecture) */
   /* 2. Make C/D, /RD, /WR, /CE to outputs (if required by port architecture) */
   /* 3. Set LCD reset line /RST active low   (if /RST is connected to a port bit) */
   /* 4. Set LCD reset line /RST passive high (if /RST is connected to a port bit) */
   }

#endif /* GHW_SINGLE_CHIP */
#endif /* GHW_NOHDW */



