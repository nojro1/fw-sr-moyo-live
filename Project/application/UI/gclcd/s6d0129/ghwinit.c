/************************** ghwinit.c *****************************

   Low-level driver functions for the s6d0129 LCD display controller
   initialization and error handling.

   The s6d0129 controller is assumed to be used with a LCD module.

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is copied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)


   Revision date:     08-08-2007
   Revision Purpose:  S6D0118 support added.

   Revision date:    28-08-07
   Revision Purpose: GDISPPIXW 18 mode inserted. Generalized simulator interface used
                     and register definitions made local

   Revision date:    03-09-07
   Revision Purpose: Minor optimizations. Corrections to 18 & 24 bit-pr-pixel modes

   Revision date:    07-05-09
   Revision Purpose: The symbol software palette (data and functions) can
                     be optimized away if not used by defining
                     GHW_PALETTE_SIZE as 0 in gdispcfg.h
   Revision date:    11-11-10
   Revision Purpose: ghw_rgb_to_color(..) updated to use G_RGB_TO_COLOR(r,g,b) macro.
   Revision date:    17-04-11
   Revision Purpose: ghw_rgb_to_color(..) updated to use G_RGB_TO_COLOR(r,g,b) macro.

   Version number: 1.6
   Copyright (c) RAMTEX Engineering Aps 2007-2010

*********************************************************************/

#ifdef SGPCMODE
#include "windows.h"  /* Sleep(..) function used by ghw_cmd_wait(..)*/
#endif

#include <s6d0129.h>  /* controller specific definements */

/*#define WR_RD_TEST*/  /* Define to include write-read-back test in ghw_init() */

#if !(defined( GHW_S6D0129 ) || defined( GHW_S6D0118 ) || \
      defined( GHW_R61506  ) || defined( GHW_TL1771) || \
      defined( GHW_SSD1286 ) || defined( GHW_SSD1285 ) || defined( GHW_SSD1283 ))
  #error Unknown controller. Controller and bustype must be selected in gdispcfg.h
#endif

/* Common registers default S6D0129, S6D0118, R61506 */
#define  GCTRL_START_OSC      0x00  /* wr only */
#define  GCTRL_DEVICE_CODE    0x00  /* rd only */
#define  GCTRL_DRV_OUTPUT_CTRL 0x01  /* Driver output control */
#define  GCTRL_LCD_DRIVE_CTRL 0x02  /*  */
#define  GCTRL_ENTRYMODE      0x03  /*  */
#define  GCTRL_DISP_CTRL      0x07  /*  */
#define  GCTRL_BLANK_CTRL     0x08  /*  */
#define  GCTRL_GATE_NON_DISP  0x09  /*  */
#define  GCTRL_FRAME_CTRL     0x0b  /*  */
#define  GCTRL_DRV_OUT_CTRL   0x0d  /* S60129 only */
#define  GCTRL_EXT_INTF       0x0c  /* R61506, TL1771 only */
#define  GCTRL_PWR_CTRL0      0x10  /*  */
#define  GCTRL_PWR_CTRL1      0x11  /*  */
#define  GCTRL_PWR_CTRL2      0x12  /* R61506, TL1771 only  */
#define  GCTRL_PWR_CTRL3      0x13  /*  */
#define  GCTRL_PWR_CTRL4      0x14  /* S6D0129, TL1771 only */
#define  GCTRL_RAMWR          0x22  /* Set or get GRAM data */
#define  GCTRL_RAMRD          0x22  /* Set or get GRAM data */

#define  GCTRL_GAMMA_0        0x30  /* Set gamma ctrl */
#define  GCTRL_GAMMA_1        0x31  /* Set gamma ctrl */
#define  GCTRL_GAMMA_2        0x32  /* Set gamma ctrl */
#define  GCTRL_GAMMA_3        0x33  /* Set gamma ctrl */
#define  GCTRL_GAMMA_4        0x34  /* Set gamma ctrl */
#define  GCTRL_GAMMA_5        0x35  /* Set gamma ctrl */
#define  GCTRL_GAMMA_6        0x36  /* Set gamma ctrl */
#define  GCTRL_GAMMA_7        0x37  /* Set gamma ctrl */
#define  GCTRL_GAMMA_8        0x38  /* Set gamma ctrl */
#define  GCTRL_GAMMA_9        0x39  /* Set gamma ctrl */

#define  GCTRL_GATE_SCAN      0x40  /*  */
#define  GCTRL_VSCROLL        0x41  /*  */

#if defined( GHW_S6D0129 )

/* S6D0129 specific registers */

#define  GCTRL_RAM_ADR_L      0x20  /* Set GRAM address */
#define  GCTRL_RAM_ADR_H      0x21  /* Set GRAM address */

#define  GCTRL_SCR_DRV1_END   0x42  /*  */
#define  GCTRL_SCR_DRV1_STRT  0x43  /*  */
#define  GCTRL_SCR_DRV2_END   0x44  /*  */
#define  GCTRL_SCR_DRV2_STRT  0x45  /*  */
#define  GCTRL_H_WIN_ADR      0x46  /* end,begin */
#define  GCTRL_V_WIN_ADR_END  0x47  /* end */
#define  GCTRL_V_WIN_ADR_STRT 0x48  /* begin */

/* GCTRL_DISP_CTRL */
#define  GDISP_ON    0x0017
#define  GDISP_OFF   0x0016

#else

/* R61506, S6D0118 specific registers */
#define  GCTRL_GRAYSCALE_CTRL 0x15  /*  */
#define  GCTRL_PWR_SEQ_CTRL   0x17  /*  */
#define  GCTRL_RAM_ADR        0x21  /* Set GRAM address */

#define  GCTRL_SCR_DRV1       0x42  /* end, start */
#define  GCTRL_SCR_DRV2       0x43  /* end, start */
#define  GCTRL_H_WIN_ADR      0x44  /* end, begin */
#define  GCTRL_V_WIN_ADR      0x45  /* end, begin  */

/* GCTRL_DISP_CTRL */
#define  GDISP_ON    0x0077
#define  GDISP_OFF   0x0076

#if (GDISPPIXW != 16)
  #error Wrong GDISPPIXW configuration. The pixel bit width is not supported.
#endif

#endif


/*
   Define pixel width, height of internal video memory ( only used for the overflow check below)
   Note: If another display controller variant is used the adjust the GCTRLW, GCTRLH definitions
         below accordingly to match the size of the pixel video RAM in the controller.
   Note: If the physical memory range limits are exceeded at runtime then some controllers stop working.
*/
#if defined( GHW_S6D0118 )
  #define  GCTRLW 176
  #define  GCTRLH 240
#elif defined( GHW_R61506 )
  #define  GCTRLW 176
  #define  GCTRLH 132
#elif (defined( GHW_TL1771 ) || defined( GHW_SSD1285))
  #define  GCTRLW 128
  #define  GCTRLH 160
#elif defined( GHW_SSD1283)
   #define  GCTRLW 132
   #define  GCTRLH 132
   #define  GHW_R61506 /* Behave like GHW_R61506 except for size */
#elif defined( GHW_SSD1286)
   #define  GCTRLW 132
   #define  GCTRLH 176
   #define  GHW_R61506 /* Behave like GHW_R61506 except for size */
#else
   /* S6D0129 (240x320) */
  #define  GCTRLW 240
  #define  GCTRLH 320
#endif

/* Fix missing definitions in gdispcfg.h */
#ifndef GHW_XOFFSET
   #define GHW_XOFFSET 0
#endif
#ifndef GHW_YOFFSET
   #define GHW_YOFFSET 0
#endif

/* Check display size settings */
#ifdef GHW_ROTATED
  #if (((GDISPH+GHW_YOFFSET) > GCTRLW) || ((GDISPW+GHW_YOFFSET) > GCTRLH))
    #error (GDISPW, GDISPH, GHW_XOFFSET, GHW_YOFFSET, GHW_ROTATED configuration exceeds controller memory limits)
  #endif
#else
  #if (((GDISPW+GHW_XOFFSET) > GCTRLW) || ((GDISPH+GHW_YOFFSET) > GCTRLH))
    #error (GDISPW, GDISPH, GHW_XOFFSET, GHW_YOFFSET, GHW_ROTATED configuration exceeds controller memory limits)
  #endif
#endif


/********************* Chip access definitions *********************/

#ifndef GHW_NOHDW
   #if defined( GHW_SINGLE_CHIP)
      /* User defined access types and simulated register address def */
      #include <bussim.h>
      #ifdef GHW_BUS8
        #define  sgwrby(a,d) simwrby((a),(d))
        #define  sgrdby(a)   simrdby((a))
      #elif defined (GHW_BUS32)
        #define  sgwrdw(a,d) simwrdw((a),(d))
        #define  sgrddw(a)   simrddw((a))
      #else
        #define  sgwrwo(a,d) simwrwo((a),(d))
        #define  sgrdwo(a)   simrdwo((a))
      #endif
   #else
      /* Portable I/O functions + hardware port def */
      #include <sgio.h>
   #endif
#else
   #undef GHW_SINGLE_CHIP /* Ignore single chip mode */
#endif

/***********************************************************************/
/** All static LCD driver data is located here in this ghwinit module **/
/***********************************************************************/

#ifdef GBASIC_INIT_ERR

/* Active foreground and background color */
GCOLOR ghw_def_foreground;
GCOLOR ghw_def_background;

#if (GHW_PALETTE_SIZE > 0)
/* Default soft palette
   The palette file can be edited directly with the ColorIconEdit program
*/
static GCODE GPALETTE_RGB FCODE ghw_palette[GHW_PALETTE_SIZE] =
     #include <gcolor_4.pal>
     ;

/* Operative palette (current palette used for color lookup) */
GCOLOR ghw_palette_opr[GHW_PALETTE_SIZE];
#endif

/* Use software font */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR  b[8];           /* Symbol data, fixed size = 8 bytes */
   }
GCODE FCODE sysfontsym[0x80] =
   {
   /* The default font MUST be a monospaced black & white (two-color) font */
   #include <sfs0129.sym> /* System font symbol table */
   };

/* Default system font */
GCODE GFONT FCODE SYSFONT =
   {
   6,      /* width */
   8,      /* height */
   sizeof(sysfontsym[0])-sizeof(GSYMHEAD), /* number of data bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL) sysfontsym,  /* pointer to array of SYMBOLS */
   0x80,   /* num symbols in sysfontsym[] */
   NULL    /* pointer to code page */ /* NULL means code page is not used */
   };

#ifdef GBUFFER
   #ifdef GHW_ALLOCATE_BUF
      /* <stdlib.h> is included via gdisphw.h */
      GCOLOR *gbuf = NULL;           /* Graphic buffer pointer */
      static SGBOOL gbuf_owner = 0;   /* Identify pointer ownership */
   #else
      GCOLOR gbuf[GBUFSIZE];         /* Graphic buffer */
   #endif
   GXT GFAST iltx,irbx;     /* "Dirty area" speed optimizers in buffered mode */
   GYT GFAST ilty,irby;
   SGBOOL  ghw_upddelay;    /* Flag for delayed update */
#else
   #if ((defined( GBASIC_TEXT ) || defined(GSOFT_FONTS) || defined(GGRAPHIC)) && !defined(GHW_NO_LCD_READ_SUPPORT))
   GCOLOR ghw_tmpbuf[GDISPW]; /* Row line buffer (for fast block read-modify-write) */
   #endif
#endif /* GBUFFER */


#ifdef GHW_INTERNAL_CONTRAST
static SGUCHAR ghw_contrast;/* Current contrast value */
#endif

SGBOOL glcd_err;            /* Internal error */
#ifndef GNOCURSOR
GCURSOR ghw_cursor;         /* Current cursor state */
#endif

#ifdef GHW_PCSIM
/* PC simulator declaration */
void ghw_init_sim( SGUINT dispw, SGUINT disph );
void ghw_exit_sim(void);
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye);
void ghw_set_xy_sim(GXT xb, GYT yb);
void ghw_autowr_sim( GCOLOR cval );
GCOLOR ghw_autord_sim( void );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
#endif
/**********************************************************************/
/** Low level s6d0129 interface functions used only by ghw_xxx modules **/
/**********************************************************************/

/* Bit mask values */
GCODE SGUCHAR FCODE sympixmsk[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

typedef struct
   {
   SGUCHAR index;
   SGUCHAR delay;
   SGUINT value;
   } S1D_REGS;

/* Array of configuration descriptors, the registers are initialized in the order given in the table */
static GCODE S1D_REGS FCODE as1dregs[] =
   {
   #if defined( GHW_R61506 ) || defined( GHW_TL1771 )
    /* R61506 initialization */

   {GCTRL_START_OSC      ,10, 0x0001},
   {GCTRL_DISP_CTRL      , 0, 0x0001},
   {GCTRL_PWR_SEQ_CTRL   , 0, 0x0001},

   #ifdef GHW_TL1771
   {GCTRL_PWR_CTRL4      , 0, 0x1506},
   #endif

   {GCTRL_PWR_CTRL0      , 0, 0x24C0},
   {GCTRL_PWR_CTRL1      , 0, 0x0001},
   {GCTRL_PWR_CTRL2      , 0, 0x001A},
   {GCTRL_PWR_CTRL3      , 0, 0x0828},
   {GCTRL_PWR_CTRL2    , 150, 0x003A},

   #if   ( defined( GHW_MIRROR_VER ) &&  defined( GHW_MIRROR_HOR ))
   {GCTRL_DRV_OUTPUT_CTRL, 0, 0x0310},
   #elif ( defined( GHW_MIRROR_VER ) && !defined( GHW_MIRROR_HOR ))
   {GCTRL_DRV_OUTPUT_CTRL, 0, 0x0210},
   #elif (!defined( GHW_MIRROR_VER ) &&  defined( GHW_MIRROR_HOR ))
   {GCTRL_DRV_OUTPUT_CTRL, 0, 0x0110},
   #else
   {GCTRL_DRV_OUTPUT_CTRL, 0, 0x0010},
   #endif

   #ifdef GHW_COLOR_SWAP

   #ifdef GHW_ROTATED
   {GCTRL_ENTRYMODE      , 0, 0x1038}, /* 16 bit Color mode (horizontal inc)*/
   #else
   {GCTRL_ENTRYMODE      , 0, 0x1030}, /* 16 bit Color mode (horizontal inc)*/
   #endif

   #else

   #ifdef GHW_ROTATED
   {GCTRL_ENTRYMODE      , 0, 0x0038}, /* 16 bit Color mode (horizontal inc)*/
   #else
   {GCTRL_ENTRYMODE      , 0, 0x0030}, /* 16 bit Color mode (horizontal inc)*/
   #endif

   #endif

   {GCTRL_BLANK_CTRL     , 0, 0x0404}, /* Front,back porch = 8*/
   {GCTRL_FRAME_CTRL     , 0, 0x000A},
   {GCTRL_DRV_OUT_CTRL   , 0, 0x5559},

   #ifndef GHW_TL1771
   {GCTRL_GRAYSCALE_CTRL , 0, 0x0000},
   #endif

   {GCTRL_GAMMA_0        , 0, 0x0000},
   {GCTRL_GAMMA_1        , 0, 0x0000},
   {GCTRL_GAMMA_2        , 0, 0x0303},
   {GCTRL_GAMMA_3        , 0, 0x0100},
   {GCTRL_GAMMA_4        , 0, 0x0404},
   {GCTRL_GAMMA_5        , 0, 0x0707},
   {GCTRL_GAMMA_6        , 0, 0x0707},
   {GCTRL_GAMMA_7        , 0, 0x0001},
   {GCTRL_GAMMA_8        , 0, 0x1f04},
   {GCTRL_GAMMA_9        , 0, 0x040f},

   {GCTRL_GATE_SCAN      , 0, 0x0000},
   {GCTRL_VSCROLL        , 0, 0x0000},

   #ifdef GHW_ROTATED
   {GCTRL_SCR_DRV1       , 0, ((GDISPW-1)<<8)},
   {GCTRL_H_WIN_ADR      , 0, ((GDISPH-1)<<8)},
   {GCTRL_V_WIN_ADR      , 0, ((GDISPW-1)<<8)},
   #else
   {GCTRL_SCR_DRV1       , 0, ((GDISPH-1)<<8)},
   {GCTRL_H_WIN_ADR      , 0, ((GDISPW-1)<<8)},
   {GCTRL_V_WIN_ADR      , 0, ((GDISPH-1)<<8)},
   #endif
   {GCTRL_RAM_ADR        , 0, 0x0000},

   {GCTRL_LCD_DRIVE_CTRL , 0, 0x0600},
   {GCTRL_DISP_CTRL ,      1, 0x0021},
   {GCTRL_DISP_CTRL ,    100, 0x0072},
   {GCTRL_LCD_DRIVE_CTRL , 0, 0x0700},
   {GCTRL_DISP_CTRL ,      0, GDISP_OFF}
   /*{GCTRL_DISP_CTRL ,      0, GDISP_ON} */

   #else  /*** Default controller (SS6D0129) ***/

   /* GHW_S60129 setup */
   {GCTRL_START_OSC      ,10, 0x0001},
   {GCTRL_PWR_CTRL1      , 0, 0x1a00},
   {GCTRL_PWR_CTRL4      , 0, 0x1506},
   {GCTRL_PWR_CTRL0      ,60, 0x0900},
   {GCTRL_PWR_CTRL3      ,60, 0x0040},
   {GCTRL_PWR_CTRL3      ,60, 0x0060},
   {GCTRL_PWR_CTRL3      ,60, 0x0070},
   {GCTRL_PWR_CTRL1      , 0, 0x2404},
   {GCTRL_PWR_CTRL0      , 0, 0x1c00},

   #if   ( defined( GHW_MIRROR_VER ) &&  defined( GHW_MIRROR_HOR ))
   {GCTRL_DRV_OUTPUT_CTRL, 0, 0x0327},
   #elif ( defined( GHW_MIRROR_VER ) && !defined( GHW_MIRROR_HOR ))
   {GCTRL_DRV_OUTPUT_CTRL, 0, 0x0227},
   #elif (!defined( GHW_MIRROR_VER ) &&  defined( GHW_MIRROR_HOR ))
   {GCTRL_DRV_OUTPUT_CTRL, 0, 0x0127},
   #else
   {GCTRL_DRV_OUTPUT_CTRL, 0, 0x0027},
   #endif


   {GCTRL_LCD_DRIVE_CTRL , 0, 0x0700},

   #ifdef GHW_COLOR_SWAP

   #ifdef GHW_ROTATED

   #if (GDISPPIXW<=16)
   {GCTRL_ENTRYMODE      , 0, 0x1038}, /* 16 bit Color mode (horizontal inc)*/
   #else
   {GCTRL_ENTRYMODE      , 0, 0x9038}, /* 24 (18) bit Color mode (horizontal inc)*/
   #endif

   #else

   #if (GDISPPIXW<=16)
   {GCTRL_ENTRYMODE      , 0, 0x1030}, /* 16 bit Color mode (horizontal inc)*/
   #else
   {GCTRL_ENTRYMODE      , 0, 0x9030}, /* 24 (18) bit Color mode (horizontal inc)*/
   #endif

   #endif /* GHW_ROTATED */

   #else  /* GHW_COLOR_SWAP */

   #ifdef GHW_ROTATED

   #if (GDISPPIXW<=16)
   {GCTRL_ENTRYMODE      , 0, 0x0038}, /* 16 bit Color mode (horizontal inc)*/
   #else
   {GCTRL_ENTRYMODE      , 0, 0x8038}, /* 24 (18) bit Color mode (horizontal inc)*/
   #endif

   #else

   #if (GDISPPIXW<=16)
   {GCTRL_ENTRYMODE      , 0, 0x0030}, /* 16 bit Color mode (horizontal inc)*/
   #else
   {GCTRL_ENTRYMODE      , 0, 0x8030}, /* 24 (18) bit Color mode (horizontal inc)*/
   #endif

   #endif /* GHW_ROTATED */
   #endif /* GHW_COLOR_SWAP */
   {GCTRL_DISP_CTRL      , 0, 0x0000}, /* Disp drive off */
   {GCTRL_BLANK_CTRL     , 0, 0x0808}, /* Front,back porch = 8*/
   {GCTRL_GATE_NON_DISP  , 0, 0x0000},
   {GCTRL_FRAME_CTRL     , 0, 0x0000},
   {GCTRL_EXT_INTF       , 0, 0x0000},

   {GCTRL_GATE_SCAN      , 0, 0x0000},
   {GCTRL_VSCROLL        , 0, 0x0000},

   #ifdef GHW_S6D0118

   #ifdef GHW_ROTATED
   {GCTRL_SCR_DRV1       , 0, ((GDISPW-1)<<8)},
   {GCTRL_H_WIN_ADR      , 0, ((GDISPH-1)<<8)},
   {GCTRL_V_WIN_ADR      , 0, ((GDISPW-1)<<8)},
   #else
   {GCTRL_SCR_DRV1       , 0, ((GDISPH-1)<<8)},
   {GCTRL_H_WIN_ADR      , 0, ((GDISPW-1)<<8)},
   {GCTRL_V_WIN_ADR      , 0, ((GDISPH-1)<<8)},
   #endif
          /* S6D0118 end */
   #else  /* S6D0129 default */

   #ifdef GHW_ROTATED
   {GCTRL_SCR_DRV1_END   , 0, (GDISPW-1)},
   {GCTRL_SCR_DRV1_STRT  , 0, 0x0000},
   {GCTRL_SCR_DRV2_END   , 0, (GDISPH-1)},
   {GCTRL_SCR_DRV2_STRT  , 0, 0x0000},
   {GCTRL_H_WIN_ADR      , 0, ((GDISPH-1)<<8)},
   {GCTRL_V_WIN_ADR_END  , 0,  (GDISPW-1)},
   {GCTRL_V_WIN_ADR_STRT , 0, 0x0000},
   #else
   {GCTRL_SCR_DRV1_END   , 0, (GDISPH-1)},
   {GCTRL_SCR_DRV1_STRT  , 0, 0x0000},
   {GCTRL_SCR_DRV2_END   , 0, (GDISPW-1)},
   {GCTRL_SCR_DRV2_STRT  , 0, 0x0000},
   {GCTRL_H_WIN_ADR      , 0, ((GDISPW-1)<<8)},
   {GCTRL_V_WIN_ADR_END  , 0,  (GDISPH-1)},
   {GCTRL_V_WIN_ADR_STRT , 0, 0x0000},
   #endif

   #endif /**/

   {GCTRL_DISP_CTRL      , 0, 0x0014},        /* Disp drive on, scan halted, reverse rgb */
   {GCTRL_DISP_CTRL      , 0, GDISP_OFF},     /* Disp drive on,blank disp */

   #ifdef GHW_S6D0118
   {GCTRL_RAM_ADR        , 0, 0x0000},
   #else
   {GCTRL_RAM_ADR_L      , 0, 0x0000},
   {GCTRL_RAM_ADR_H      , 0, 0x0000},
   #endif
   {GCTRL_GAMMA_0        , 0, 0x0000},
   {GCTRL_GAMMA_1        , 0, 0x0000},
   {GCTRL_GAMMA_2        , 0, 0x0303},
   {GCTRL_GAMMA_3        , 0, 0x0100},
   {GCTRL_GAMMA_4        , 0, 0x0404},
   {GCTRL_GAMMA_5        , 0, 0x0707},
   {GCTRL_GAMMA_6        , 0, 0x0707},
   {GCTRL_GAMMA_7        , 0, 0x0001},
   {GCTRL_GAMMA_8        , 0, 0x1f04},
   {GCTRL_GAMMA_9        , 0, 0x040f}
   #endif /*** Default controller ***/
   };

/*
   Send a command
*/
static void ghw_cmd_dat_wr(SGUCHAR cmd, SGUINT cmddat)
   {
   #ifndef GHW_NOHDW

   #ifdef GHW_BUS8
   sgwrby(GHWCMD, 0x00);         /* Msb */
   sgwrby(GHWCMD, cmd);          /* Lsb */
   sgwrby(GHWWR, (SGUCHAR) (cmddat >> 8));   /* Msb */
   sgwrby(GHWWR, (SGUCHAR) cmddat); /* Lsb */
   #elif defined( GHW_BUS16 )
   sgwrwo(GHWCMDW, (SGUINT) cmd);
   sgwrwo(GHWWRW,  cmddat);
   #else
   sgwrdw(GHWCMDDW, ((SGUINT) cmd)<< 1);
   sgwrdw(GHWWRDW,  ((cmddat << 2) & 0x3fc0) | ((cmddat << 1) & 0x1fe));
   #endif

   #else  /* GHW_NOHDW */
   cmd++; /* silience 'not used' warning */
   cmddat++;
   #endif
   }

static void ghw_cmd_wr(SGUCHAR cmd)
   {
   #ifndef GHW_NOHDW

   #if defined(GHW_BUS8)
   /* 8 bit data bus */
   sgwrby(GHWCMD, 0);            /* Msb */
   sgwrby(GHWCMD, cmd);  /* Lsb */
   #elif defined( GHW_BUS16 )
   /* 16 bit data bus */
   sgwrwo(GHWCMDW, cmd);
   #else
   /* 32 bit data bus (display connected to 18 lsb bits) */
   sgwrdw(GHWWRDW,  ((cmd << 2) & 0x3fc0) | ((cmd << 1) & 0x1fe));
   #endif

   #else  /* GHW_NOHDW */
   cmd++; /* silience 'not used' warning */
   #endif
   }

/*
   Set the y range.
   The row position is set to y.
   After last write on row y2 the write position is reset to y
   Internal ghw function
*/
void ghw_set_xyrange(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   #ifdef GHW_PCSIM
   ghw_set_xyrange_sim( xb, yb, xe, ye);
   #endif

   #ifndef GHW_NOHDW

   #ifdef GHW_ROTATED
   /* Set range (rotated display) */

   /* Set window range */
   ghw_cmd_dat_wr(GCTRL_H_WIN_ADR,(((SGUINT)ye+GHW_YOFFSET) << 8) | (yb+GHW_YOFFSET));

   #if defined( GHW_S6D0129 )

   /* Height > 256, use split registers */
   ghw_cmd_dat_wr(GCTRL_V_WIN_ADR_END, (SGUINT)xe+GHW_XOFFSET);
   ghw_cmd_dat_wr(GCTRL_V_WIN_ADR_STRT,(SGUINT)xb+GHW_XOFFSET);
   /* Set address pointer to start of range */
   ghw_cmd_dat_wr(GCTRL_RAM_ADR_L, (SGUINT)yb+GHW_YOFFSET);
   ghw_cmd_dat_wr(GCTRL_RAM_ADR_H, (SGUINT)xb+GHW_XOFFSET);

   #else

   /* Use single address register */
   ghw_cmd_dat_wr(GCTRL_V_WIN_ADR,(((SGUINT)xe+GHW_XOFFSET) << 8) | (xb+GHW_XOFFSET));
   /* Set address pointer to start of range */
   ghw_cmd_dat_wr(GCTRL_RAM_ADR, (((SGUINT)xb+GHW_XOFFSET) << 8) |  (yb+GHW_YOFFSET));

   #endif

   #else
   /* Set range (normal display) */

   /* Set window range */
   ghw_cmd_dat_wr(GCTRL_H_WIN_ADR,(((SGUINT)xe+GHW_XOFFSET) << 8) | (xb+GHW_XOFFSET));

   #if defined( GHW_S6D0129 )

   ghw_cmd_dat_wr(GCTRL_V_WIN_ADR_END, (SGUINT)ye+GHW_YOFFSET);
   ghw_cmd_dat_wr(GCTRL_V_WIN_ADR_STRT,(SGUINT)yb+GHW_YOFFSET);

   /* Set address pointer to start of range */
   ghw_cmd_dat_wr(GCTRL_RAM_ADR_L, (SGUINT)xb+GHW_XOFFSET);
   ghw_cmd_dat_wr(GCTRL_RAM_ADR_H, (SGUINT)yb+GHW_YOFFSET);

   #else

   ghw_cmd_dat_wr(GCTRL_V_WIN_ADR,(((SGUINT)ye+GHW_YOFFSET) << 8) | (yb+GHW_YOFFSET));
   /* Set address pointer to start of range */
   ghw_cmd_dat_wr(GCTRL_RAM_ADR, (((SGUINT)yb+GHW_YOFFSET) << 8) |  (xb+GHW_XOFFSET));

   #endif

   #endif /* GHW_ROTATED */

   /* Prepare for auto write */
   ghw_cmd_wr(GCTRL_RAMWR);

   #endif /* GHW_NOHDW */
   }

/*
   Write databyte to controller (at current position) and increment
   internal xadr.

   Internal ghw function
*/
void ghw_auto_wr(GCOLOR dat)
   {
   #ifdef GHW_PCSIM
   ghw_autowr_sim( dat );
   #endif

   #ifndef GHW_NOHDW

   #if ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
      /* data format RRRRRGGGGGGBBBBB*/
      sgwrby(GHWWR, (SGUCHAR)(dat>>8));     /* MSB */
      sgwrby(GHWWR, (SGUCHAR)(dat & 0xff)); /* LSB */
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS8 ))
      /* data format RRRRRRGGGGGGBBBBBB*/
      sgwrby(GHWWR, (SGUCHAR)(dat>>10));  /* MSB (RRRRRR**) */
      sgwrby(GHWWR, (SGUCHAR)(dat>>4));   /*     (GGGGGG**) */
      sgwrby(GHWWR, (SGUCHAR)(dat<<2));   /* LSB (BBBBBB**) */
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS8 ))
      /* data format RRRRRR**GGGGGG**BBBBBB***/
      sgwrby(GHWWR, (SGUCHAR)(dat>>16));  /* MSB (RRRRRR**) */
      sgwrby(GHWWR, (SGUCHAR)(dat>>8));   /*     (GGGGGG**) */
      sgwrby(GHWWR, (SGUCHAR)(dat));      /* LSB (BBBBBB**) */
   #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
      /* 16 bit bus mode */
      sgwrwo(GHWWRW, dat);           /* 16 bit color */
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS16 ))
      /* dat = RRRRRRGGGGGGBBBBBB */
      /* pack to controller specific format */
      /* MSB (RRRRRRGGGGGGBBBB) */
      /* LSB (BB**************) */
      sgwrwo(GHWWRW, (SGUINT) (dat >> 2));
      sgwrwo(GHWWRW, (SGUINT) (dat << 14));
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS16 ))
      {
      SGUINT tmp;
      /* dat = generic 24 bit RGB = RRRRRR**GGGGGG**BBBBBB** */
      /* pack to controller specific format */
      /* MSB (RRRRRRGGGGGGBBBB) */
      /* LSB (BB**************) */
      tmp = (((((SGUINT)dat) >> 8) & 0xf100) |
             ((((SGUINT)dat) >> 6) & 0x03f0) |
             ((((SGUINT)dat) >> 4) & 0x000f));
      sgwrwo(GHWWRW, tmp);
      sgwrwo(GHWWRW, ((SGUINT)dat) << 12);
      }
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS32 ))
      sgwrdw(GHWWRDW, (SGULONG) dat);
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS32 ))
      /* 24 bit rgb collected to 18 */
      /* dat = generic 24 bit RGB = RRRRRR**GGGGGG**BBBBBB** */
      dat = ((dat & 0xfc0000) >> 6) |
            ((dat & 0x00fc00) >> 4) |
            ((dat & 0x0000fc) >> 2);
      sgwrdw(GHWWRDW, dat);           /* 18 bit color */
   #else
       #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
   #endif

   #endif /* GHW_NOHDW */
   }

/*
   Read databyte from controller at specified address
   (controller does not auto increment during read)
   The address must be within the update window range

   Internal ghw function
*/
GCOLOR ghw_rd(GXT xb, GYT yb)
   {
   GCOLOR ret;

   #ifdef GHW_PCSIM
   ghw_set_xy_sim( xb, yb);
   #endif

   #ifdef GHW_S6D0129
     #ifdef GHW_ROTATED
     ghw_cmd_dat_wr(GCTRL_RAM_ADR_L, (SGUINT)(yb+GHW_YOFFSET));
     ghw_cmd_dat_wr(GCTRL_RAM_ADR_H, (SGUINT)(xb+GHW_XOFFSET));
     #else
     ghw_cmd_dat_wr(GCTRL_RAM_ADR_L, (SGUINT)(xb+GHW_XOFFSET));
     ghw_cmd_dat_wr(GCTRL_RAM_ADR_H, (SGUINT)(yb+GHW_YOFFSET));
     #endif
   #else
     #ifdef GHW_ROTATED
     ghw_cmd_dat_wr(GCTRL_RAM_ADR, (((SGUINT)xb+GHW_XOFFSET) << 8) |  (yb+GHW_YOFFSET));
     #else
     ghw_cmd_dat_wr(GCTRL_RAM_ADR, (((SGUINT)yb+GHW_YOFFSET) << 8) |  (xb+GHW_XOFFSET));
     #endif
   #endif

   /* Prepare for auto read */
   ghw_cmd_wr(GCTRL_RAMRD);

   #ifndef GHW_NOHDW
      #if   ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
         ret = sgrdby(GHWRD); /* dummy read */
         ret = sgrdby(GHWRD); /* dummy read */
         ret = (((GCOLOR) sgrdby(GHWRD)) << 8); /* MSB*/
         ret |=  (GCOLOR) sgrdby(GHWRD);        /* LSB*/
      #elif ((GDISPPIXW == 18) && defined( GHW_BUS8 ))
         /* Read 3 times, assemble to 18 bit */
         ret = sgrdby(GHWRD); /* dummy read */
         ret = sgrdby(GHWRD); /* dummy read */
         ret = sgrdby(GHWRD); /* dummy read */
         /* Left aligned color info */
         ret =  (((GCOLOR) (sgrdby(GHWRD) & 0xfc)) << 10); /* MSB (RRRRRR**) */
         ret |= (((GCOLOR) (sgrdby(GHWRD) & 0xfc)) << 4);  /*     (GGGGGG**) */
         ret |= (((GCOLOR) (sgrdby(GHWRD) & 0xfc)) >> 2);  /* LSB (BBBBBB**) */
      #elif ((GDISPPIXW == 24) && defined( GHW_BUS8 ))
         /* 24 bit color mode (3 lsb bytes is r,g,b) */
         ret = sgrdby(GHWRD); /* dummy read */
         ret = sgrdby(GHWRD); /* dummy read */
         ret = sgrdby(GHWRD); /* dummy read */
         /* Left aligned color info */
         ret =  (((GCOLOR) sgrdby(GHWRD)) << 16); /* MSB (RRRRRR**) */
         ret |= (((GCOLOR) sgrdby(GHWRD)) << 8);  /*     (GGGGGG**) */
         ret |=   (GCOLOR) sgrdby(GHWRD);         /* LSB (BBBBBB**) */
      #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
         /* 16 bit color mode */
         ret =  (GCOLOR)sgrdwo(GHWRDW);    /* Dummy read */
         ret =  (GCOLOR) sgrdwo(GHWRDW);   /* 16 bit color */
      #elif ((GDISPPIXW == 18) && defined( GHW_BUS16 ))
         /* 18 bit color mode */
         ret = sgrdwo(GHWRDW); /* dummy read */
         ret = sgrdwo(GHWRDW); /* dummy read */
         /* MSB (RRRRRRGGGGGGBBBB) */
         /* LSB (BB**************) */
         /* assemble to 18 bit from controller specific format */
         /* dat = generic 18 bit RGB = RRRRRRGGGGGGBBBBBB */
         ret =  ((GCOLOR) sgrdwo(GHWRDW)) << 2;
         ret |= ((GCOLOR) ((sgrdwo(GHWRDW)>>14)&0x3));
      #elif ((GDISPPIXW == 24) && defined( GHW_BUS16 ))
         {
         /* 18 (24) bit color mode (3 lsb bytes is r,g,b) */
         SGUINT dat1;
         SGUINT dat2;
         dat1 = sgrdwo(GHWRDW); /* dummy read */
         dat2 = sgrdwo(GHWRDW); /* dummy read */
         dat1 = sgrdwo(GHWRDW);
         dat2 = sgrdwo(GHWRDW);
         /* MSB (RRRRRRGGGGGGBBBB) */
         /* LSB (BB**************) */
         /* unpack from controller specific format */
         /* dat = generic 24 bit RGB = RRRRRR**GGGGGG**BBBBBB** */
         ret = (((GCOLOR)(dat1 & 0xfc00)) << 8) |
               (((GCOLOR)(dat1 & 0x03f0)) << 6) |
               (((GCOLOR)(dat1 & 0x000f)) << 4);
         ret = ret | (((GCOLOR)dat2) >> 12);
         }
      #elif ((GDISPPIXW == 18) && defined( GHW_BUS32 ))
         ret =  (GCOLOR) sgrddw(GHWRDDW);   /* Dummy read */
         ret =  (GCOLOR) sgrddw(GHWRDDW);   /* 18 bit color */
      #elif ((GDISPPIXW == 24) && defined( GHW_BUS32 ))
         /* 18 bit collective  split to 24 bit rgb*/
         ret =  (GCOLOR) sgrddw(GHWRDDW);   /* Dummy read */
         ret =  (GCOLOR) sgrddw(GHWRDDW);   /* 18 bit color */
         ret = ((ret << 6) & 0xfc0000) + ((ret << 4) & 0xfc00) + ((ret << 2)&0xfc);
      #else
          #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
      #endif

     #if (defined( GHW_R61506 ) || defined( GHW_TL1771 ))
      #ifdef GHW_COLOR_SWAP
      /* R61506 RGB swapping is only done in hardware during write */
      /* Do B<->R swapping in sw here so read data == write data */
      /* (R61506 only supports 16 bit color mode) */
      ret = ((ret >> 11) & 0x001f) |
            ((ret << 11) & 0xf800) |
             (ret & 0x07e0);
      #endif
     #endif

   #else
     #ifdef GHW_PCSIM
     ret = ghw_autord_sim();
     #else
     ret = 0;
     #endif
   #endif /* GHW_NOHDW */
   return ret;
   }

/***********************************************************************/
/**        s6d0129 Initialization and error handling functions       **/
/***********************************************************************/

SGBOOL ghw_newcolor;

/*
   Change default (palette) colors
*/
void ghw_setcolor(GCOLOR fore, GCOLOR back)
   {
   /* Update active colors */
   #if (GDISPPIXW == 24)
   /* Mask unused bits to ease color comparation */
   fore &= 0xfcfcfc;
   back &= 0xfcfcfc;
   #elif (GDISPPIXW == 32)
   fore |= 0xff000000;
   back |= 0xff000000;
   #endif
   if ((ghw_def_foreground != fore) || (ghw_def_background != back))
      {
      ghw_newcolor = 1;
      ghw_def_foreground = fore;
      ghw_def_background = back;
      }
   }


/*
   Convert RGB palette color to controller configuration specific GCOLOR value
*/
GCOLOR ghw_rgb_to_color( GCONSTP GPALETTE_RGB *palette )
   {
   if (palette == NULL)
      return (GCOLOR) 0;

   return G_RGB_TO_COLOR(palette->r,palette->g,palette->b);
   }

#if (GHW_PALETTE_SIZE > 0)
/*
   Load a new palette or update the existing palette
   (Palette is only used with symbols using 2 or 4 bits pr pixel)
*/
SGBOOL ghw_palette_wr(SGUINT start_index, SGUINT num_elements, GCONSTP GPALETTE_RGB PFCODE *palette)
   {
   if ((num_elements == 0) || (palette == NULL) ||
       (start_index >= sizeof(ghw_palette_opr)/sizeof(ghw_palette_opr[0])))
      {
      glcd_err = 1;
      return 1;
      }

   if ((start_index + num_elements) > sizeof(ghw_palette_opr)/sizeof(ghw_palette_opr[0]))
         num_elements = sizeof(ghw_palette_opr)/sizeof(ghw_palette_opr[0]) - start_index;

   glcd_err = 0;

   /* (Partial) update of operative palette values */
   while(num_elements-- > 0)
      {
      ghw_palette_opr[start_index++] = G_RGB_TO_COLOR(palette->r,palette->g,palette->b);
      palette++;
      }

   return glcd_err;
   }
#endif
/*
   Fast set or clear of LCD module RAM buffer
   Internal ghw function
*/
static void ghw_bufset(GCOLOR color)
   {
   GBUFINT cnt;
   cnt = 0;
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   do
      {
      /* Clear using X,Y autoincrement */
      ghw_auto_wr(color);  /* Set LCD buffer */
      #ifdef GBUFFER
      gbuf[cnt] = color; /* Set ram buffer as well */
      #endif
      }
   while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH)); /* Loop until x+y wrap */
   }


#ifdef WR_RD_TEST
/*
   Make write-readback test on controller memory.

   This test will fail if some databus and control signals is not connected correctly.

   This test will fail if 16/8 bit bus mode selection in the configuration settings
   does not match the actual bus configuration for the hardware (display and processor
   16/8 bit bus width, 8080/6800 bus type settings, word / byte address offsets, etc).

   This test may fail if illegal GCTRLW, GCTRLH, GHW_XOFFSET, GHW_YOFFSET
   configuration settings cause overrun of the on-chip video RAM.

   This test can be exectuted correctly with only logic power on the display module.
   No high-level voltages are nessesary for the test to run (although nothing then can
   be shown on the display)

   Return 0 if no error,
   Return != 0 if some readback error is detected (the bit pattern may give information
   about connector pins in error)

   NOTE:
   This function should be commented out in serial mode. In serial mode
   s6d0129 does not provide read-back facility and this test will always
   fail.
*/
static GCOLOR ghw_wr_rd_test(void)
   {
   #ifndef GHW_NOHDW
   int i;
   GCOLOR msk,result;
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   /*printf("\n"); */

   #if (GDISPPIXW > 16)
   /* 24 (18) bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%06x ", (unsigned long) msk); */
      ghw_auto_wr(~msk);
      /*printf(" 0x%06x\n", (unsigned long) (~msk)); */
      msk <<= 1;
      }
   /*printf("\n"); */
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val;
      val = ghw_rd(i*2,0);
      result |= (val ^ msk);
      /*printf("0x%06lx ",  (unsigned long) val); */
      val = ghw_rd(i*2+1,0);
      /*printf(" 0x%06lx\n", (unsigned long) val ); */
      result |= (val ^ (~msk));
      msk <<= 1;
      }
   result &= GHW_COLOR_CMP_MSK; /* Mask bits unused by controller during read back */
   #else
   /* 16 bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%04x ", (unsigned int) msk); */
      ghw_auto_wr(~msk);
      /*printf(" 0x%04x\n", (unsigned int) (~msk & 0xffff)); */
      msk <<= 1;
      }
   /*printf("\n"); */
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val;
      val = ghw_rd(i*2,0);
      result |= (val ^ msk);
      /*printf("0x%04x ",  (unsigned short) val); */
      val = ghw_rd(i*2+1,0);
      /*printf(" 0x%04x\n", (unsigned short) val ); */
      result |= (val ^ (~msk));
      msk <<= 1;
      }
   #endif
   return result;  /* 0 = Nul errors */
   #else
   return 0; /* 0 = Nul errors */
   #endif
   }

#endif /* WR_RD_TEST */

/*
   Wait a number of milli seconds
*/
static void ghw_cmd_wait(SGUCHAR ms)
   {
   #ifdef SGPCMODE
   Sleep(ms); /* delay x1 ms */
   #else
   SGUINT wait1ms;
   while (ms-- != 0)
      {
      wait1ms = 2000;     /* Adjust to achieve a 1 ms loop below */
      while( wait1ms != 0)
         wait1ms--;
      }
   #endif
   }

/*
   Initialize display, clear ram  (low-level)
   Clears glcd_err status before init

   Return 0 if no error,
   Return != 0 if some error
*/
SGBOOL ghw_init(void)
   {
   short i;

   #ifdef GBUFFER
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;
   ghw_upddelay = 0;
   #endif

   glcd_err = 0;
   ghw_io_init(); /* Set any hardware interface lines, controller hardware reset */

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL)
      {
      /* Allocate graphic ram buffer */
      if ((gbuf = (GCOLOR *)calloc(ghw_gbufsize(),1)) == NULL)
         glcd_err = 1;
      else
         gbuf_owner = 1;
      }
   #endif

   if (glcd_err != 0)
      return 1;

   #ifdef GHW_PCSIM
   /* Tell simulator about the visual LCD screen organization */
   ghw_init_sim( GDISPW, GDISPH );
   #endif
   /* Set default colors */
   ghw_setcolor( GHW_PALETTE_FOREGROUND, GHW_PALETTE_BACKGROUND );

   #if (GHW_PALETTE_SIZE > 0)
   /* Load palette */
   ghw_palette_wr(0, sizeof(ghw_palette)/sizeof(ghw_palette[0]), (GCONSTP GPALETTE_RGB PFCODE *)&ghw_palette[0]);
   #endif

   /* Initialize controller according to configuration file */
   for (i=0; i < sizeof(as1dregs)/sizeof(S1D_REGS); i++)
      {
      ghw_cmd_dat_wr(as1dregs[i].index,as1dregs[i].value);
      if (as1dregs[i].delay != 0)
         ghw_cmd_wait( as1dregs[i].delay );
      }

   /*
      Stimuli test loops for initial oscilloscope test of display interface bus signals
      Uncomment to use the test loop for the given data bus width.
      It is recommended to check all display bus signals with each of the I/O access
      statements in the loop one by one.
   */
   /*
   #ifdef GHW_BUS8
   for(;;)
      {
      SGUCHAR dat;
      sgwrby(GHWCMD,0xff);
      sgwrby(GHWWR,0x00);
      dat = sgrdby(GHWSTA);
      dat = sgrdby(GHWRD);
      }
   #endif
   #ifdef GHW_BUS16
   for(;;)
      {
      SGUINT dat;
      sgwrwo(GHWCMDW,0xffff);
      sgwrwo(GHWWRW,0x0000);
      dat = sgrdwo(GHWSTAW);
      dat = sgrdwo(GHWRDW);
      }
   #endif
   #ifdef GHW_BUS32
   for(;;)
      {
      SGUINT dat;
      sgwrdw(GHWCMDDW,0xffff);
      sgwrdw(GHWWRDW,0x0000);
      dat = sgrddw(GHWSTADW);
      dat = sgrddw(GHWRDDW);
      }
   #endif
   */


   #ifdef WR_RD_TEST
   /*
      NOTE:
      The call of ghw_wr_rd_test() should be commented out in serial mode.
      In serial mode the display controller  does not provide read-back facility
      and this test will always fail.
   */
   if (ghw_wr_rd_test() != ((GCOLOR) 0))
      {
      /* Controller memory write-readback error detected
      (Check the cable or power connections to the display) */
      G_WARNING("Hardware interface error\nCheck display connections\n");  /* Test Warning message output */
      glcd_err = 1;
      return 1;
      }
   #endif

   ghw_bufset( ghw_def_background );
   ghw_dispon();

   #ifndef GNOCURSOR
   ghw_cursor = GCURSIZE1;    /* Cursor is off initially */
   /* ghw_cursor = GCURSIZE1 | GCURON; */ /* Uncomment to set cursor on initially */
   #endif

   ghw_updatehw();  /* Flush to display hdw or simulator */


   return (glcd_err != 0) ? 1 : 0;
   }

/*
   Return last error state. Called from applications to
   check for LCD HW or internal errors.
   The error state is reset by ghw_init and all high_level
   LCD functions.

   Return == 0 : No errors
   Return != 0 : Some errors
*/
SGUCHAR ghw_err(void)
   {
   #if (defined(_WIN32) && defined( GHW_PCSIM))
   if (GSimError())
      return 1;
   #endif
   return (glcd_err == 0) ? 0 : 1;
   }


/*
   Display a (fatal) error message.
   The LCD display module is always cleared and initialized to
   the system font in advance.
   The error message is automatically centered on the screen
   and any \n characters in the string is processed.

   str = ASCII string to write at display center
*/
void ghw_puterr( PGCSTR str )
   {
   PGCSTR idx;
   SGUINT xcnt;
   GXT xp;
   GYT yp,h,y, sidx;
   PGSYMBYTE psym;
   GCOLOR pval;
   SGUCHAR val;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   #endif

   if (ghw_init() != 0)  /* (Re-) initialize display */
      return;            /* Some initialization error */

   /* Count number of lines in string */
   idx=str;
   if (idx == NULL)
      return;
   xcnt = 1;
   while(*idx)
      {
      if (*(idx++) == '\n')
         xcnt++;
      }

   /* Set start character line */
   h = SYSFONT.symheight;
   yp = (xcnt*h > GDISPH) ? 0 : ((GDISPH-1)-xcnt*h)/2;
   /* Set character height in pixel lines */

   idx=str;
   do
      {
      xcnt=0;  /* Set start x position so line is centered */
      while ((idx[xcnt]!=0) && (idx[xcnt]!='\n') && (xcnt < GDISPBW))
         {
         xcnt++;
         }

      /* Calculate start position for centered line */
      xp = (GDISPW-xcnt*SYSFONT.symwidth)/2;

      /* Display text line */
      while (xcnt-- > 0)
         {
         /* Point to graphic content for character symbol */
         psym = &(sysfontsym[(*idx) & 0x7f].b[0]);
         ghw_set_xyrange(xp,yp,xp+SYSFONT.symwidth-1,yp+(h-1));

         /* Display rows in symbol */
         for (y = 0; y < h; y++)
            {
            /* Get symbol row value */
            val = *psym++;
            /* Initiate LCD controller address pointer */
            #ifdef GBUFFER
            gbufidx = GINDEX(xp, (GBUFINT)yp+y );
            #endif

            /* Display colums in symbol row */
            for (sidx = 0; sidx < SYSFONT.symwidth; sidx++)
               {
               if ((val & sympixmsk[sidx]) != 0)
                  pval = ghw_def_foreground;
               else
                  pval = ghw_def_background;

               /* End of symbol or end of byte reached */
               #ifdef GBUFFER
               gbuf[gbufidx++] = pval;
               #endif
               ghw_auto_wr(pval);
               }
            }

         idx++;
         xp += SYSFONT.symwidth; /* Move to next symbol in line */
         }

      /* Next text line */
      yp += h;
      if (*idx == '\n')
         idx++;
      }
   while ((*idx != 0) && (yp < GDISPH));

   ghw_updatehw();  /* Flush to display hdw or simulator */
   }

void ghw_exit(void)
   {
   #if defined( GHW_ALLOCATE_BUF)
   if (gbuf != NULL)
      {
      if (gbuf_owner != 0)
         {
         /* Buffer is allocated by ginit, so release graphic buffer here */
         free(gbuf);
         gbuf_owner = 0;
         }
      gbuf = NULL;
      }
   #endif
   ghw_io_exit();         /* Release any LCD hardware resources, if required */
   #ifdef GHW_PCSIM
   ghw_exit_sim(); /* Release simulator resources */
   #endif
   }

#ifndef GNOCURSOR
/*
   Replace cursor type data (there is no HW cursor support in s6d0129)
*/
void ghw_setcursor( GCURSOR type)
   {
   ghw_cursor = type;
   #ifdef GHW_ALLOCATE_BUF
   if (gbuf == NULL)
      glcd_err = 1;
   #endif
   }
#endif


/*
   Turn display off
   (Minimize power consumption)
*/
void ghw_dispoff(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif
   ghw_cmd_dat_wr(GCTRL_DISP_CTRL, GDISP_OFF);     /* Blank display */
   }

/*
   Turn display on
*/
void ghw_dispon(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif
   ghw_cmd_dat_wr(GCTRL_DISP_CTRL, GDISP_ON);     /* Restore display */
   }

#if defined( GHW_ALLOCATE_BUF)
/*
   Size of buffer requied to save the whole screen state
*/
GBUFINT ghw_gbufsize( void )
   {
   return (GBUFINT) (((GBUFINT)GBUFSIZE) * sizeof(GCOLOR) + (GBUFINT) sizeof(GHW_STATE));
   }

#ifdef GSCREENS
/*
   Check if screen buf owns the screen ressources.
*/
SGUCHAR ghw_is_owner( SGUCHAR *buf )
   {
   return (((GCOLOR *)buf == gbuf) && (gbuf != NULL)) ? 1 : 0;
   }

/*
   Save the current state to the screen buffer
*/
SGUCHAR *ghw_save_state( SGUCHAR *buf )
   {
   GHW_STATE *ps;
   if (!ghw_is_owner(buf))
      return NULL;

   ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);
   ps->upddelay = (ghw_upddelay != 0);
   #ifndef GNOCURSOR
   ps->cursor = ghw_cursor;
   #endif
   ps->foreground = ghw_def_foreground; /* Palette may vary, save it */
   ps->background = ghw_def_background;
   return (SGUCHAR *) gbuf;
   }

/*
   Set state to buf.
   If buffer has not been initiated by to a screen before, only
   the pointer is updated. Otherwise the the buffer
*/
void ghw_set_state(SGUCHAR *buf, SGUCHAR doinit)
   {
   if (gbuf != NULL)
      {
      /* The LCD controller has been initiated before */
      if (gbuf_owner != 0)
         {
         /* Buffer was allocated by ginit, free it so screen can be used instead*/
         free(gbuf);
         gbuf_owner = 0;
         gbuf = NULL;
         }
      }

   if (doinit != 0)
      {
      /* First screen initialization, just set buffer pointer and
         leave rest of initialization to a later call of ghw_init() */
      gbuf = (GCOLOR *) buf;
      gbuf_owner = 0;
      }
   else
      {
      if ((gbuf = (GCOLOR *) buf) != NULL)
         {
         GHW_STATE *ps;
         ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);

         #ifndef GNOCURSOR
         ghw_cursor = ps->cursor;
         #endif
         ghw_upddelay = 0;        /* Force update of whole screen */
         iltx = 0;
         ilty = 0;
         irbx = GDISPW-1;
         irby = GDISPH-1;
         ghw_updatehw();
         ghw_upddelay = (ps->upddelay != 0) ? 1 : 0;
         /* Restore drawing color */
         ghw_setcolor(ps->foreground, ps->background);
         }
      }
   }
#endif  /* GSCREENS */
#endif  /* GHW_ALLOCATE_BUF */

#endif /* GBASIC_INIT_ERR */



