/******************************************************************

   s6d0129 LCD controller simulator for the RAMTEX LCD driver library

   The s6d0129 is simulated by the use of a buffer:

    controller_video_ram  simulates the RAM internally in controller


   Revision date:     11-04-07
   Revision Purpose:  Added support for SSD1289 switch

   Revision Purpose:  18 bit pr pixel data simulation added.
                      Interface made generic to handle simulation of multiple controllers
                      Now support controllers with an without automatic address
                      inc during read and with asymetric read back bits.
   Revision date:     14-01-10
   Revision Purpose:  GHW_LARGE_VIDEOBUF switch support. Handle video RAM buffers larger
                      than the physical screen.
   Revision date:
   Revision Purpose:

   Version number: 1.3
   Copyright (c) RAMTEX Aps 2007-2010

******************************************************************/

#ifndef GHW_PCSIM
#error Only include this file in PC simulation mode
#endif

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <gdisphw.h>     /* swprintf */ /* GHW_FAST_SIM_UPDATE */
#include <s6d0129.h>     /* GHW_COLOR_CMP_MSK, GHW_NO_RDINC switches */

void ghw_autowr_sim( GCOLOR cval );
static int graph_init = 0; /* don't init graphic twice */

#ifdef _WIN32
   #include <gsimintf.h>
   void simputs( SGINT sgstream, const char *chp )
      {
      sgstream = sgstream; /* remove warning */
      GSimPuts(chp);
      }
   #define far /* nothing */
#else
   #error no simulation for this compiler
#endif

void simprintf( SGINT sgstream, const char far *fmt, ...)
   {
   va_list argptr;
   int form_len;
   static char printf_buf[100];

   va_start(argptr, fmt);
   form_len = vsprintf(printf_buf, fmt, argptr);
   va_end(argptr);

   if (form_len >= 100)
      simputs(-1,"\nERROR: Buffer overrun in simsprintf()");

   simputs( sgstream, printf_buf );
   }

/* Internal s6d0129 data simulation */
static GSIM_RGB_PARAM *controller_video_ram; /* s6d0129 simulator module buffer */

/* Copy of user LCD size variables (allows the simulator to be a lib) */
/* Size of video ram in controller */
static SGUINT gdisph;      /* Initiated with user gdisph */
static SGUINT gdispw;      /* Initiated with user gdispw */

/* Viewport area control for physical screen */
static SGUINT gdispph;     /* Initiated with user gdisp_ph */
static SGUINT gdisppw;     /* Initiated with user gdisp_pw */
static SGUINT gdisppx;     /* Initiated with user gdisp_px */
static SGUINT gdisppy;     /* Initiated with user gdisp_py */

/* Auto increment simulation control */
static SGUINT xpos, winxb, winxe;
static SGUINT ypos, winyb, winye;

static SGBOOL onoff;       /* Display on / off */
static SGBOOL winena;      /* Update viewport initialized */

static void clr_screen(void)
   {
   SGUINT x;
   SGUINT y;
   GSIM_RGB_PARAM val;
   if (graph_init == 0) return;   /* Display have not been initiated yet */
   val.par = 0L;
   for (x = 0; x < gdisppw; x++)
      {
      for (y = 0; y < gdispph; y++)
         GSimWrRGBBit(x,y,val);
      }
   GSimFlush();
   }

/* Redraw screen content */
static void redraw_screen(void)
   {
   SGUINT x;
   SGUINT y;
   if (graph_init == 0) return;   /* Display have not been initiated yet */
   for (x = 0; x < gdisppw; x++)
      {
      for (y = 0; y < gdispph; y++)
         GSimWrRGBBit(x,y,controller_video_ram[x+gdisppx + (y+gdisppy)*gdispw]);
      }
   GSimFlush();
   }

/*
   Simulate display on / off
*/
void ghw_dispoff_sim( void )
   {
   /* Off */
   if (onoff)
      clr_screen();
   onoff = 0;
   }

void ghw_dispon_sim( void )
   {
   /* On */
   if (onoff == 0)
      {
      onoff = 1;
      redraw_screen();
      }
   }

/*
   Set physical screen viewport position
*/
void ghw_set_screenpos_sim(GXT xb, GYT yb)
   {
   GLIMITU(xb,gdispw-gdisppw);
   GLIMITU(yb,gdisph-gdispph);
   gdisppx = xb;
   gdisppy = yb;
   redraw_screen();
   }

/*
   Init cursor and autowrap limits
*/
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   GLIMITU(xb,gdispw-1);
   GLIMITU(xe,gdispw-1);
   GLIMITU(yb,gdisph-1);
   GLIMITU(ye,gdisph-1);
   winxb = xb;
   winxe = xe;
   winyb = yb;
   winye = ye;
   xpos = xb;
   ypos = yb;
   winena = 1;
   }

/*
   Init cursor
*/
void ghw_set_xy_sim(GXT xb, GYT yb)
   {
   GLIMITU(xb,gdispw-1);
   GLIMITU(yb,gdisph-1);
   xpos = xb;
   ypos = yb;
   #ifndef GHW_NO_RDINC
   winena = 0;
   #endif
   }

/*
   Simulate internal pointer handling with auto wrap
*/
static void addrinc(void)
   {
   xpos++;
   if (winena)
      {
      if (xpos > winxe)
         {
         xpos = winxb;
         if (++ypos > winye)
            ypos = winyb;
         }
      }
   else
      {
      if (xpos >= GDISPW)
         {
         xpos = 0;
         if (++ypos >= GDISPH)
            ypos = 0;
         }
      }

   }


/*
   Write to update PC screen
   Simulate autoincrement
*/
void ghw_autowr_sim( GCOLOR cval )
   {
   GSIM_RGB_PARAM val;
   if (graph_init == 0)
      return;   /* Display have not been initiated yet */

    #if (GDISPPIXW==8)
    val.rgb.r = (SGUCHAR) (cval & 0xe0);
    val.rgb.g = (SGUCHAR)((cval << 3) & 0xe0);
    val.rgb.b = (SGUCHAR)((cval << 6) & 0xc0);
    /* Just expand colors to simulator resolution */
    val.rgb.r |= (SGUCHAR) (val.rgb.r >> 3) | (val.rgb.r >> 6);
    val.rgb.g |= (SGUCHAR) (val.rgb.g >> 3) | (val.rgb.g >> 6);
    val.rgb.b |= (SGUCHAR) (val.rgb.b >> 2) | (val.rgb.b >> 4) | (val.rgb.b >> 6);
    #elif (GDISPPIXW==16)
      /* 16 bit resolution */
      val.rgb.r = (SGUCHAR) ((cval >> 8) & 0xf8);
      val.rgb.g = (SGUCHAR) ((cval >> 3) & 0xfc);
      val.rgb.b = (SGUCHAR) ((cval << 3) & 0xf8);
      /* Just expand colors to simulator resolution */
      val.rgb.r |= (SGUCHAR) (val.rgb.r >> 5);
      val.rgb.g |= (SGUCHAR) (val.rgb.g >> 6);
      val.rgb.b |= (SGUCHAR) (val.rgb.b >> 5);

    #elif (GDISPPIXW==18)
      /* 18 bit resolution */
      val.rgb.r = (SGUCHAR) ((cval >> 10)& 0xfc);
      val.rgb.g = (SGUCHAR) ((cval >> 4) & 0xfc);
      val.rgb.b = (SGUCHAR) ((cval << 2) & 0xfc);

      #if (GHW_COLOR_CMP_MSK == 0x3ffff)
      /* Just expand colors to simulator resolution */
      val.rgb.r |= (SGUCHAR) (val.rgb.r >> 6);
      val.rgb.g |= (SGUCHAR) (val.rgb.g >> 6);
      val.rgb.b |= (SGUCHAR) (val.rgb.b >> 6);
      #else
      /* Set LSB bits if fewer bits are actively used by hardware */
      if (val.rgb.r & 0x80)
         val.rgb.r |= (SGUCHAR) ~((GHW_COLOR_CMP_MSK >> 10) & 0xfc);
      else
         val.rgb.r &= (SGUCHAR)  ((GHW_COLOR_CMP_MSK >> 10) & 0xfc);

      if (val.rgb.g & 0x80)
         val.rgb.g |= (SGUCHAR) ~((GHW_COLOR_CMP_MSK >> 4) & 0xfc);
      else
         val.rgb.g &= (SGUCHAR)  ((GHW_COLOR_CMP_MSK >> 4) & 0xfc);

      if (val.rgb.b & 0x80)
         val.rgb.b |= (SGUCHAR) ~((GHW_COLOR_CMP_MSK << 2) & 0xfc);
      else
         val.rgb.b &= (SGUCHAR)  ((GHW_COLOR_CMP_MSK << 2) & 0xfc);
      #endif

    #elif (GDISPPIXW==24)
      /* 24 bit resolution */
      val.rgb.r = (SGUCHAR) (cval >> 16);
      val.rgb.g = (SGUCHAR) (cval >> 8);
      val.rgb.b = (SGUCHAR) (cval);

      /* Set LSB bits if fewer bits are actively used by hardware */
      #if (GHW_COLOR_CMP_MSK != 0xffffff)
      if (val.rgb.r & 0x80)
         val.rgb.r |= (SGUCHAR) ~(GHW_COLOR_CMP_MSK >> 16);
      else
         val.rgb.r &= (SGUCHAR)  (GHW_COLOR_CMP_MSK >> 16);

      if (val.rgb.g & 0x80)
         val.rgb.g |= (SGUCHAR) ~(GHW_COLOR_CMP_MSK >> 8);
      else
         val.rgb.g &= (SGUCHAR)  (GHW_COLOR_CMP_MSK >> 8);

      if (val.rgb.b & 0x80)
         val.rgb.b |= (SGUCHAR) ~(GHW_COLOR_CMP_MSK);
      else
         val.rgb.b &= (SGUCHAR)  (GHW_COLOR_CMP_MSK);
      #endif
    #else
      #error GDISPPIXW setting illegal for this simulator
    #endif

   controller_video_ram[xpos + ypos * gdispw] = val;

   if (winena)
      {
      if (onoff)
         {
         /* Screen is turned on */
         if ((xpos >= gdisppx) && (ypos >= gdisppy) &&
             (xpos <  gdisppx+gdisppw) && (ypos < gdisppy+gdispph))
            /* Pixel inside visible screen, update simulator as well */
            GSimWrRGBBit( xpos-gdisppx, ypos-gdisppy, val);
         }
      #ifndef GHW_FAST_SIM_UPDATE
      GSimFlush();
      #endif
      }

   addrinc();
   }

/*
   Read back data
*/
GCOLOR ghw_autord_sim( void )
   {
   GPALETTE_RGB palette;
   register GSIM_RGB_PARAM  *cp;
   if (graph_init == 0)
      return 0;   /* Display have not been initiated yet */
   cp = &controller_video_ram[xpos + ypos*gdispw];
   #ifndef GHW_NO_RDINC
   addrinc();
   #endif
   palette.r = cp->rgb.r;
   palette.g = cp->rgb.g;
   palette.b = cp->rgb.b;
   return ghw_rgb_to_color( &palette );
   }

/*
   This function is called last in ghw_init after all s6d0129 display
   module parameters has been initiated.

   NOTE: The user code may cause that this function is in-worked more
   than once
*/
/*
   Init simulator so screen may be smaller than the screen buffer
*/
void ghw_init_sim_viewport( SGUINT dispw, SGUINT disph, SGUINT disppw, SGUINT dispph  )
   {
   /* Logical screen data setup(ram buffer sizes) */
   if (dispw < disppw)
      dispw = disppw;
   if (disph < dispph)
      disph = dispph;
   gdisph = disph;
   gdispw = dispw;

   /* Physical screen data setup */
   gdispph = dispph;
   gdisppw = disppw;
   gdisppx = 0;
   gdisppy = 0;

   #ifdef _WIN32
   /* Blank display */
   if( !graph_init )
      {
      if (controller_video_ram != NULL)
         free( controller_video_ram );  /* The size may have changed */

      if ((controller_video_ram = malloc(dispw*disph*sizeof(GSIM_RGB_PARAM))) == NULL)
         {
         graph_init = 0;
         return; /* Simulator init error */
         }
      /* Init connection to LCD simulator (only once to uptimize speed */
      if (!GSimInitC((unsigned short) gdisppw, (unsigned short) gdispph, PALETTE_RGB))
         {
         graph_init = 1;
         }
      }

   clr_screen();
   onoff = 0;
   xpos = 0;
   ypos = 0;
   winena = 0;
   winxb = 0;
   winxe = gdispw-1;
   winyb = 0;
   winye = gdisph-1;
   #else
   #error no simulation for this compiler
   #endif
   }

/*
   Init simulator so "physical" screen and screen buffer has the same size
*/
void ghw_init_sim( SGUINT dispw, SGUINT disph )
   {
   ghw_init_sim_viewport( dispw, disph, dispw, disph );
   }

/*
   This function is activated via ghw_exit() when gexit() is called
*/
void ghw_exit_sim( void )
   {
   graph_init = 0;
   if (controller_video_ram != NULL)
      {
      free( controller_video_ram );  /* Deallocate video memory */
      controller_video_ram = NULL;
      }

   #if defined( _WIN32 )
   GSimClose();
   #endif
   }

