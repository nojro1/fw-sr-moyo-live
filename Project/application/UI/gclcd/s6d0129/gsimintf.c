/****************************** GSIMINTF.C *****************************

   Socket interface to the external LCD simulator program.
   Commands and data to/from the simulator is send via sockets messages.

   If the LCD application is developed as a console application (recommended)
   the following library file must be included in the link

     For MSVC 6.x and Borland C++ 5.0 :  WSOCK32.LIB

   Revision date:     11-10-04
   Revision Purpose:  GsimFlush updated for segmented transmission to
                      handle large graphic LCD screens.
   Revision date:     171204
   Revision Purpose:  RGB mode optimized, buffer allocations optimized
   Revision date:     190407
   Revision Purpose:  Touch screen simulation interface added.
   Revision date:     080808
   Revision Purpose:  Fix connection error when MSVC unicode string modes were used
   Revision date:     030109
   Revision Purpose:  Fix flush error with large video buffers.
   Revision date:     20-07-09
   Revision Purpose:  Assure process release for Simulator in tight touch poll loops

   Version 5.2
   Copyright RAMTEX International ApS 2001-2009

************************************************************************/
#ifndef GHW_PCSIM
#error Only include this file in Gdisp simulation mode
#endif

#ifndef _WIN32
#error Only include this file in WIN32 projects
#endif

#include <stdio.h>
#include <winsock.h>
#include <time.h>
#include <tchar.h>

#include <gdispcfg.h>  /* GHW_MINIMIZE_CONSOLE definition */
#include <gsimintf.h>

/***** Start of private communication structures used by the LCD simulator *****/
/***** These data structures are only used by this module                  *****/

/* The the Simulator use <LOW>..<HIGH> repesentation (= 80x86 endian) */
typedef union    /* 8 / 16 bit IO conversion union */
   {
   unsigned short w;          /* unsigned int */
   short i;                   /* int */
   struct
      {
      unsigned char b0;       /* LSB byte */
      unsigned char b1;       /* MSB byte */
      }b;
   } u16;

#define  MAX_INFO_LENGTH 2000

/* LCD commands */
typedef enum
   {
   LCD_SIZE = 256,  /* LCD (re-size) L_SIZE */
   LCD_READY,       /* READY command (responce) */
   LCD_CLR,         /* LCD clr cmd  (no data) */
   LCD_INFO_CLR,    /* Info Clear (no data) */

   LCD_BUF,         /* LCD graphic data buffer L_BUF */
   LCD_BUF_SYNC,    /* LCD graphic data buffer L_BUF followed by wait for READY responce */
   LCD_INFO,        /* Info C-string */
   LCD_GETKEY,      /* Get key command */
   LCD_KEY,         /* Key (responce) */
   LCD_SIZE_COLOR,  /* LCD (re-size) L_SIZE and set color */
   LCD_PALETTE,     /* Load LCD palette */
   LCD_GETTOUCH,    /* Get touch command */
   LCD_TOUCH        /* Touch (responce) */
   } LCD_COMMAND;

typedef struct
   {
   u16 len;  /* Length of total data package (including the length itself) */
   u16 cmd;  /* Command LCD_COMMAND */
   } L_HDR;

/* Structure for transmission of size info */
typedef struct
   {
   u16 w;    /* New width  in LCD pixels */
   u16 h;    /* New height in LCD pixels */
   } L_SIZE;

/* Structure for transmission of size info */
typedef struct
   {
   u16 w;      /* New width  in LCD pixels */
   u16 h;      /* New height in LCD pixels */
   u16 p_mode; /* New pallette mode */
   } LC_SIZE;

/* Structure for transmission of a coordinate pair */
typedef struct
   {
   u16 x1;
   u16 y1;
   u16 x2;
   u16 y2;
   } L_XY;

/* Socket LCD update structure */
typedef struct
   {
   L_XY pos;              /* Pixel coordinates for upper left and lower right corner */
   unsigned char dat[1];  /* (start of) LCD pixel data (8 bit horizontal, byte aligned) */
   } L_LCD;

/* Socket LCD data union */
typedef union
   {
   L_SIZE size;             /* New LCD size */
   LC_SIZE size_p;          /* New LCD size and color mode m */
   L_LCD  graph;            /* LCD data */
   unsigned char buf[1];    /* (start of) generic buffer */
   char str[1];             /* (start of) C string for info message */
   } L_BUF;

/* Socket LCD tx structure */
typedef struct
   {
   L_HDR hdr;
   L_BUF buf;
   } L_TXBUF;

#define L_MAXBUF 0xffffUL

/***************** end communication structures *******************/

/***************** Private data *******************/
/*
   The default socket address setup parameters for the LCDSERV server are:
      Name_or_IP : localhost
      Port       : 3000

   If another socket address is wanted the two address definitions
   GSimServer and  GSimPort  below should be modified accordingly.

   When the socket address setup is modified from the default then
   LCDSERV.EXE must be started with three command line parameters:
           0  address_name  port
   similar to:
      LCDSERV.EXE 0 localhost 3000
   or
      LCDSERV.EXE 0 127.0.0.1 3000
*/
static const char *GSimServer = "localhost";
static unsigned short GSimPort = 3000;
static SOCKET GSimSocket = INVALID_SOCKET;
static L_TXBUF *txbuf = NULL;        /* transmission buffer */
static unsigned char *lcdbuf = NULL; /* LCD screen copy */
static unsigned short lcdw = 0;      /* LCD screen pixel size (initiate defaults) */
static unsigned short lcdh = 0;
static unsigned short lcdbw = 0;     /* LCD screen byte size */
static GSIM_PALETTE_MODE lcdp = PALETTE_NONE;
static unsigned short pixelmsk;
static unsigned short ilty = 0;      /* "Dirty area" pixel coordinates */
static unsigned short iltx = 0;
static unsigned short irbx = 0;
static unsigned short irby = 0;
static unsigned short oldkey = 0;
static unsigned char  GSimErr = 1;

#define BUFSIZE 80                      /* buffer size for console window titles */
static _TCHAR console_title[BUFSIZE];   /* hold console window title (application name) */
static HWND console_hwnd;               /* handle to console window */


/* Helper macro for displaying errors */
#define PRINTERROR(s)   \
   { \
   if (console_hwnd) \
      ShowWindow( console_hwnd, SW_RESTORE ); /* Restore console window */ \
   fprintf(stderr,"\n%s",s); \
   Sleep(2000); /* Show message in at least 2 seconds */ \
   }

/***************** Private functions *******************/

/*
   This module finds the handle to the console window so it can be
   minimized automatically.
   The trick used is to give the console window a unique name temporarily
   so we can use FindWindow() to find it and get its handle for further use.
*/
static void get_console_hwnd(void)
   {
   #ifdef GHW_MINIMIZE_CONSOLE
   _TCHAR NewWindowTitle[BUFSIZE];              /* contains fabricated WindowTitle */
   GetConsoleTitle(console_title, BUFSIZE-1);   /* fetch current window title */
   wsprintf(NewWindowTitle,_T("%d/%d"),         /* format a "unique" NewWindowTitle */
      GetTickCount(),GetCurrentProcessId());
   SetConsoleTitle(NewWindowTitle);         /* Temp change current window title */
   Sleep(40);                               /* ensure window title has been updated */
   console_hwnd=FindWindow(NULL, NewWindowTitle); /* look for NewWindowTitle */
   SetConsoleTitle(console_title);          /* restore original window title */

   #ifdef _UNICODE
      {
      /* MSVC wide-char compilation mode used, compress info string to a char string for simulator */
      _TCHAR *tcp;
      unsigned char *cp;
      for(tcp = &console_title[0], cp = (unsigned char *) (&console_title[0]); (*tcp & 0xff) != 0; cp++, tcp++)
         {
         if ((*tcp) >= ((_TCHAR) 0xfe))
            { /* Extended file names are not supported */
            strcpy((char *)console_title,"LCD application");
            return;
            }
         *cp = (unsigned char) (*tcp);
         }
      *cp = 0;
      }
   #endif
   #else
   strcpy((char *)console_title,"LCD application");
   console_hwnd = 0;
   #endif
   }

/*
   Init connection to LCD simulator server.
   The simulator server is supposed to be up running already.
*/
static unsigned char socket_init(void)
   {
   int nRet;
   WSADATA wsaData;
   WORD wVersionRequested;
   LPHOSTENT lpHostEntry;
   SOCKADDR_IN saServer;
   GSimErr = 1;

   /* Initialize WinSock and check the version */
   wVersionRequested = MAKEWORD(1,1);
   WSAStartup(wVersionRequested, &wsaData);
   if (wsaData.wVersion < wVersionRequested)
      {
      fprintf(stderr,"\nERROR Wrong WinSock version\n");
      return 1;
      }

   /* Find the server */
   if ((lpHostEntry = gethostbyname(GSimServer)) == NULL)
      {
      PRINTERROR("ERROR Could not locate server\n LCDSERV.EXE must be up running on this machine in advance");
      return 1;
      }

   /* Create a TCP/IP stream socket */
   if((GSimSocket = socket(PF_INET,          /* Address family */
                         SOCK_STREAM,        /* Socket type */
                         IPPROTO_TCP))       /* Protocol */
       == INVALID_SOCKET)
      {
      PRINTERROR("ERROR socket creation");
      return 1;
      }

   /* Fill in the address structure */
   saServer.sin_family = PF_INET;
   saServer.sin_addr = *((LPIN_ADDR)*lpHostEntry->h_addr_list); /* Server's address */
   saServer.sin_port = htons(GSimPort);                         /* Port number */

   /* connect to the server */
   nRet = connect(GSimSocket,               /* Socket */
                 (LPSOCKADDR)&saServer,    /* Server address */
                  sizeof(struct sockaddr)); /* Length of server address structure */
   if (nRet == SOCKET_ERROR)
      {
      PRINTERROR("ERROR connecting to LCD simulator failed\n LCDSERV.EXE must be up running on this machine");
      closesocket(GSimSocket);
      GSimSocket = 0;
      return 1;
      }
   get_console_hwnd();            /* Get handle so we can minimize window */
   if (console_hwnd)
      ShowWindow( console_hwnd, SW_MINIMIZE );
   GSimErr = 0;
   return 0;
   }

/*
   Wait for one byte READY responce
   Return 1 if error,
   Return 0 if no-error
*/
static unsigned char wait_ready(void)
   {
   int nRet;
   txbuf->hdr.len.w = 0xFFFF; /* Make some values to be overwritten */
   txbuf->hdr.cmd.w = 0;
   nRet = recv(GSimSocket, (char *) txbuf, 4, 0);
   if (nRet == SOCKET_ERROR )
      {
      closesocket(GSimSocket);
      GSimSocket = 0;
      GSimErr = 1;
      return 1;
      }
   return (unsigned char)(((nRet == 4) && (txbuf->hdr.len.w == 0) && (txbuf->hdr.cmd.w == LCD_READY)) ? 0 : 1);
   }

/*
   Transmit data to socket
   Buffer length len is allowed to be be zero (i.e. command only).
   A buf NULL pointer indicates that txbuf is used.

   The socket is closed if any error is detected.

   Return ==0 if no errors
   Return !=0 if errors
*/
static unsigned char GSimTx(unsigned short cmd, unsigned short len, const unsigned char *buf)
   {
   int nRet, txlen;
   if ((GSimErr == 1) || (GSimSocket == INVALID_SOCKET))
      return 1;

   GSimErr = 1;
   /* Make header */
   txbuf->hdr.len.w = len;
   txbuf->hdr.cmd.w = cmd;

   /* Transmit header only or header + txbuf */
   txlen = buf ? sizeof(L_HDR) : len + sizeof(L_HDR);
   nRet = send(GSimSocket, (const char *) txbuf, txlen, 0);
   if (nRet == SOCKET_ERROR)
      {
      PRINTERROR("ERROR socket send");
      closesocket(GSimSocket);
      return 1;
      }
   if (nRet != txlen )
      {
      PRINTERROR("ERROR socket in number of bytes send");
      closesocket(GSimSocket);
      return 1;
      }

   if (buf)
      {
      /* Transmit foreign buffer */
      nRet = send(GSimSocket, (const char *) buf, len, 0);
      if (nRet == SOCKET_ERROR)
         {
         PRINTERROR("ERROR socket send");
         closesocket(GSimSocket);
         return 1;
         }
      if (nRet != len )
         {
         PRINTERROR("ERROR socket in number of bytes send");
         closesocket(GSimSocket);
         return 1;
         }
      }
   GSimErr = 0;
   return 0;
   }

/***************** Public functions *******************/

/*
  Init connection to simulator for a LCD display of w*h pixels
     Return ==0 if no errors
     Return !=0 if errors
*/
unsigned char GSimInitC(unsigned short w, unsigned short h, GSIM_PALETTE_MODE p)
   {
   unsigned long lcdbufsize;
   oldkey = 0;
   if (GSimSocket != INVALID_SOCKET)
      {
      /* A LCD simulator connection already exist */
      if ((lcdw == w) && (lcdh == h) && (lcdp == p))
         {
         /* The LCD size and palette is the same so just clear the content */
         if ((GSimGClr() == 0) && (GSimPutsClr() == 0))
            {  /* Communication was ok */
            char str[BUFSIZE+15];
            sprintf(str,"%s re-init.", (char *) console_title);
            return GSimPuts(str);
            }
         }
      GSimClose(); /* Different size or communication failed, so clear and retry */
      }

   /* Create socket connection */
   if (socket_init())
      return 1;

   /* Allocate lcd buffer */
   if (lcdbuf != NULL)
      free(lcdbuf);
   lcdbufsize = 0L;
   lcdp = p;
   if (lcdp == PALETTE_NONE)
      {
      lcdbw = (unsigned short)((w+7)/8);
      pixelmsk = (unsigned short) ~(1);
      }
   else
      {
      if (lcdp == PALETTE_RGB)
         lcdbw = (unsigned short)(w * 3);
      else
         lcdbw = (unsigned short)(((((unsigned int) w) *((unsigned int)lcdp))+7)/8);
      /* Convert PALETTE type to a mask value */
      if (lcdp <= PALETTE_1)
         pixelmsk = (unsigned short)~(7); /* 8 pixel in byte */
      else
      if (lcdp <= PALETTE_2)
         pixelmsk = (unsigned short)~(3); /* 4 pixel in byte */
      else
      if (lcdp <= PALETTE_4)
         pixelmsk = (unsigned short)~(1); /* 2 pixel in byte */
      else
         pixelmsk = (unsigned short)~(0); /* 1 pixel in byte */
      }
   lcdw = w;
   lcdh = h;
   if ((lcdbuf = (unsigned char *) calloc(lcdbw,lcdh)) == NULL)
      {
      PRINTERROR("ERROR during buffer allocation");
      GSimClose();
      return 1;
      }

   /* Make "dirty-area" rectangle empty */
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;

   /* Allocate transmission buffer of optimized size */
   if (txbuf != NULL)
      free(txbuf);

   if (lcdp == PALETTE_RGB)
      {
      lcdbufsize = ((unsigned long) lcdw)*((unsigned long)lcdh)*4;
      if (lcdbufsize < (MAX_INFO_LENGTH + sizeof(L_TXBUF)))
         lcdbufsize = MAX_INFO_LENGTH + sizeof(L_TXBUF); /* Assure minimun limit */
      if (lcdbufsize > L_MAXBUF+1)
         lcdbufsize = L_MAXBUF+1;       /* Limit to max handled by buffer format */
      }
   else
      {
      lcdbufsize = MAX_INFO_LENGTH + sizeof(L_TXBUF);
      if (lcdbufsize < ((unsigned long)(lcdbw*lcdh) + sizeof(L_TXBUF)))
          lcdbufsize = (unsigned long)(lcdbw*lcdh) + sizeof(L_TXBUF);  /* Max size controlled by display data */
      if (lcdbufsize < (3*256 + sizeof(L_TXBUF)))
          lcdbufsize = 3*256 + sizeof(L_TXBUF);       /* Max size controlled by palette data */
      }

   if ((txbuf = (L_TXBUF *) malloc(lcdbufsize)) == NULL)
      {
      PRINTERROR("ERROR during buffer allocation");
      GSimClose();
      return 1;
      }

   /* Wait for server responce */
   if (!wait_ready())
      {
      /* Set/update LCD screen size */
      txbuf->buf.size_p.w.w = w;
      txbuf->buf.size_p.h.w = h;
      txbuf->buf.size_p.p_mode.w = (unsigned short) p;
      if (!GSimTx(LCD_SIZE_COLOR, sizeof(LC_SIZE), NULL))
         {
         if (!wait_ready())
            {
            char str[BUFSIZE+15];
            sprintf(str,"\r%s connected.", (char *) console_title);
            GSimPuts(str);
            return 0;
            }
         }
      }

   PRINTERROR("ERROR no server responce");
   GSimClose();
   return 1;
   }

/*
   Close connection to LCD display simulator
   If the program is just terminated the connection will be closed autoamtically.
*/
void GSimClose(void)
   {
   /* Release buffers */
   if (lcdbuf)
      free(lcdbuf);
   lcdbuf = NULL;
   if (txbuf)
      free(txbuf);
   txbuf = NULL;
   lcdw = lcdh = 0;
   /* Close connection */
   if (GSimSocket != INVALID_SOCKET)
      {
      closesocket(GSimSocket);
      GSimSocket = INVALID_SOCKET;
      /* Release WinSock */
      WSACleanup();
      }

   console_hwnd = 0;
   oldkey = 0;
   }

/*
   Return the current error state
*/
unsigned char GSimError(void)
   {
   return GSimErr;
   }

/*
   Clear LCD simulator info window and the keyboard queue

   Return ==0 if no errors
   Return !=0 if errors
*/
unsigned char GSimPutsClr(void)
   {
   return GSimTx(LCD_INFO_CLR, 0, NULL);
   }

/*
   Clear LCD simulator Graphic Window

   Return ==0 if no errors
   Return !=0 if errors
*/
unsigned char GSimGClr(void)
   {
   return GSimTx(LCD_CLR, 0, NULL);
   }

/*
   Transmit an information string to the
   LCD simulator info window

   Return ==0 if no errors
   Return !=0 if errors
*/
unsigned char GSimPuts(const char *str)
   {
   if ((str == NULL) || (*str == 0))
      return 0;
   return GSimTx(LCD_INFO, (unsigned short) (strlen(str)+1), (const unsigned char *)str);
   }


/*
   Graphic data is transfered bit wise to the LCD buffer so the simulator
   interface is independent of any particular byte orientation in the LCD
   controller.

   The "dirty" area of the buffer must later be flushed to the LCD simulator

   Returns 1 if parameter errors or simulator is uninitialized
   Returns 0 if no errors.
*/
unsigned char GSimWrBit(unsigned short x, unsigned short y, unsigned char val)
   {
   static const unsigned char mask[]  = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
   static const unsigned char mask2[] = {0xC0,0x30,0x0C,0x03};
   static const unsigned char mask4[] = {0xF0,0x0F};

   if ((lcdbuf == NULL) || (x >= lcdw) || (y >= lcdh))
      return 1;  /* Parameter error or missing initialization */

   switch (lcdp)
      {
      case PALETTE_NONE:
      case PALETTE_1:
         if ((val & 0x1) != 0)
            lcdbuf[x/8+lcdbw*y] |= mask[x & 0x7];
         else
            lcdbuf[x/8+lcdbw*y] &= ~mask[x & 0x7];
         break;
      case PALETTE_2:
         val = (unsigned char)((val & 0x3) << (6-(x & 0x3)*2));
         lcdbuf[x/4+lcdbw*y] = (unsigned char)((lcdbuf[x/4+lcdbw*y] & ~mask2[x & 0x3]) | val);
         break;
      case PALETTE_4:
         val &= 0xf;
         if ((x & 0x1) == 0)
            val <<= 4;
         lcdbuf[x/2+lcdbw*y] = (unsigned char)((lcdbuf[x/2+lcdbw*y] & ~mask4[x & 0x1]) | val);
         break;
      case PALETTE_8:
         lcdbuf[x+lcdbw*y] = val;
         break;
      default:
         return 1; /* Illegal function for mode */
      }

   /* Update "dirty area" markers */
   if(  irbx < iltx )
       iltx = irbx = x;
   else if( x < iltx )
       iltx = x;
   else if( x > irbx )
       irbx = x;

   if( irby < ilty)
       ilty = irby = y;
   else if( y < ilty )
       ilty = y;
   else if( y > irby )
       irby = y;
   return 0;
   }

/*
   Graphic RGB data is transfered bit wise to the LCD buffer so the simulator
   interface is independent of any particular byte orientation in the LCD
   controller.

   The "dirty" area of the buffer must later be flushed to the LCD simulator

   Returns 1 if parameter errors or simulator is uninitialized
   Returns 0 if no errors.
*/
unsigned char GSimWrRGBBit(unsigned short x, unsigned short y, GSIM_RGB_PARAM val)
   {
   unsigned char *bufp;
   if ((lcdbuf == NULL) || (x >= lcdw) || (y >= lcdh) || lcdp != PALETTE_RGB)
      return 1;  /* Parameter error or missing initialization */

   bufp = &lcdbuf[x*3+lcdbw*y];
   *bufp++ = val.rgb.r;
   *bufp++ = val.rgb.g;
   *bufp++ = val.rgb.b;

   /* Update "dirty area" markers */
   if( irbx < iltx )
       iltx = irbx = x;
   else if( x < iltx )
       iltx = x;
   else if( x > irbx )
       irbx = x;

   if( irby < ilty)
       ilty = irby = y;
   else if( y < ilty )
       ilty = y;
   else if( y > irby )
       irby = y;
   return 0;
   }

/*
   Update palette data.
   The number of palette elements must match the palette mode
*/
unsigned char GSimWrPalette(const GSIM_PALETTE_RGB *palette, unsigned short no_elements)
   {
   unsigned short i;
   unsigned char *cp;
   if(lcdbuf == NULL)
      return 1;

   if ((lcdp != PALETTE_1) &&
       (lcdp != PALETTE_2) &&
       (lcdp != PALETTE_4)  &&
       (lcdp != PALETTE_8))
       return 1;    /* Command illegal for mode */

   /* power_of_2(palette_mode) == palette table size */
   if (((unsigned short) ((1<<lcdp))) !=  no_elements)
       return 1;    /* Palette size is not correct for mode */

   /* Load palette to transmission buffer */
   cp = &txbuf->buf.buf[0];
   for (i = 0; i < no_elements; i++, palette++)
      {
      *cp++ = palette->r;
      *cp++ = palette->g;
      *cp++ = palette->b;
      }

   GSimTx(LCD_PALETTE, (unsigned short) (i*3), NULL);
   if (GSimErr == 0)
      wait_ready();

   return GSimErr;
   }

/*
   Flush "dirty" part of graphic buffer to the LCD simulator

   Return 1 if simulator is uninitialized or communication errors
   Return 0 if no errors.
*/
unsigned char GSimFlush(void)
   {
   unsigned short x,ilx,irx;
   unsigned char *txp;
   unsigned long i;
   if(lcdbuf == NULL)
      return 1;
   if((irby < ilty ) || ( irbx < iltx ))
      return 0; /* No new data to flush */

   /* Align update retangle to horizontal byte limits */
   iltx &= pixelmsk;   /* Start from a whole buffer byte */

   do
      {
      /* Copy coordinates to buffer header */
      txbuf->buf.graph.pos.x1.w = iltx;
      txbuf->buf.graph.pos.y1.w = ilty;
      txbuf->buf.graph.pos.x2.w = irbx;
      txbuf->buf.graph.pos.y2.w = irby;

      /* Copy dirty area data to transmission buffer */
      i=0;
      txp = &txbuf->buf.graph.dat[0];

      if (lcdp == PALETTE_RGB)
         {
         unsigned char r,g,b;
         unsigned char *bufp;
         /* Save dirty area using color counting / compression */
         for (; ilty <= irby; ilty++)
            {
            if ((i + ((irbx-iltx)+1)*4 + sizeof(L_LCD)) > L_MAXBUF)
               {
               txbuf->buf.graph.pos.y2.w = (unsigned short)((ilty != 0) ? ilty-1 : 0);
               break;  /* Buffer length type limit reached, use segmented transmit */
               }
            bufp = &lcdbuf[iltx*3+ilty*lcdbw];
            for (x = iltx; x <= irbx; x++)
               {
               r = *bufp++;
               g = *bufp++;
               b = *bufp++;
               if (iltx == x)
                  {
                  /* First pixel on line, just save it */
                  i+=4;
                  txp[0] = r;
                  txp[1] = g;
                  txp[2] = b;
                  txp[3] = 0;
                  }
               else
                  {
                  /* Check against previous pixel */
                  if ((txp[0] == r) &&
                      (txp[1] == g) &&
                      (txp[2] == b) &&
                      (txp[3] < 255))
                     {
                     txp[3]++;   /* The color is the same, just increment count */
                     }
                  else
                     {
                     /* Use a new position */
                     txp = &txp[4];
                     i+=4;
                     txp[0] = r;
                     txp[1] = g;
                     txp[2] = b;
                     txp[3] = 0;
                     }
                  }
               }
            txp = &txp[4]; /* Use a new position for next line */
            }
         }
      else
         {
         /* Convert to whole byte coordinates */
         if (lcdp == PALETTE_NONE)
            {
            ilx = (unsigned short)(iltx / 8);
            irx = (unsigned short)(irbx / 8);
            }
         else
            {
            ilx = (unsigned short)(iltx / (8/(unsigned short) lcdp));
            irx = (unsigned short)(irbx / (8/(unsigned short) lcdp));
            }
         for (; ilty <= irby; ilty++)
            {
            if ((i + (irx-ilx)+1) >  L_MAXBUF)
               {
               txbuf->buf.graph.pos.y2.w = (unsigned short)((ilty != 0) ? ilty-1 : 0);
               break;  /* Buffer length type limit reached, use segmented transmit */
               }

            for (x = ilx; x <= irx; x++)
               {
               *txp++ = lcdbuf[x+ilty*lcdbw];
               i++;
               }
            }
         }

      /* transmit data and wait for responce */
      GSimTx(LCD_BUF_SYNC, (unsigned short)(sizeof(L_LCD) + i - 1), NULL);
      if (GSimErr == 0)
         wait_ready();
      }
   while( ilty <= irby );

   /* Clear dirty area */
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;
   return GSimErr;
   }

/*
   Get a keycode or 0 if no keys are pending in
   the servers keyboard queue
*/
static int get_key(void)
   {
   int nRet;
   GSimTx(LCD_GETKEY, 0, NULL);

   txbuf->hdr.len.w = 0xFFFF; /* Make some values to be overwritten */
   txbuf->hdr.cmd.w = 0;
   nRet = recv(GSimSocket, (char *) txbuf, 6, 0);
   if (nRet == SOCKET_ERROR )
      {
      closesocket(GSimSocket);
      GSimSocket = 0;
      return 1;
      }
   if ((nRet == 6) && (txbuf->hdr.len.w == 2) && (txbuf->hdr.cmd.w == LCD_KEY))
      {
      nRet = txbuf->buf.buf[0] * 256 + txbuf->buf.buf[1];
      return nRet;
      }
   else
      {
      PRINTERROR("ERROR during keyboard poll");
      return -1;
      }
   }

/*
   Get a key from LCD server
*/
unsigned short GSimKbGet(void)
   {
   int key;
   if ((oldkey != 0) || (GSimErr != 0))
      {
      key = oldkey;
      oldkey = 0;
      return (unsigned short) key;
      }
   while ((key = get_key()) >= 0)
      {
      if (key != 0)
         return (unsigned short) key;
      Sleep(20);   /* Give more processing time to other applications */
      }
   return 0;
   }

/*
   Check a key from LCD server
*/
unsigned short GSimKbHit(void)
   {
   int key;
   if (GSimErr != 0)
      return 0;
   if (oldkey != 0)
      return 1;
   if ((key = get_key()) < 0)
      return 0;  /* Some server connection error */
   oldkey = (unsigned short) key;
   return (unsigned short)((oldkey != 0) ? 1 : 0);
   }

/*
   Simulates a 10ms timerstamp counter
*/
unsigned long GSimTimestamp(void)
   {
   return (((unsigned long) clock())*100L)/CLOCKS_PER_SEC;
   }

/*
   Simulates a touch screen input.
   Returns 1 if a touch change (an edge or pressed coordinate change) has been detected
   Returns 0 if the touch level is unchanged (unpressed, or pressed and same position).

   Edge  = 1, Touch screen pressed changed state.
   Edge  = 0, No touch state change.
   Level = 1, Touch screen pressed (x,y values is a (new) valid position)
   Level = 0, No touch (x,y values is the position where last touch stopped)
*/
unsigned char GSimTouchGet(unsigned char *edgep, unsigned char *levelp, unsigned short *xp, unsigned short *yp)
   {
   int nRet;
   static unsigned short oldx = 0;
   static unsigned short oldy = 0;
   unsigned short x;
   unsigned short y;

   GSimTx(LCD_GETTOUCH, 0, NULL);

   txbuf->hdr.len.w = 0xFFFF; /* Make some values to be overwritten */
   txbuf->hdr.cmd.w = 0;
   nRet = recv(GSimSocket, (char *) txbuf, 4+6, 0);
   if (nRet == SOCKET_ERROR )
      {
      closesocket(GSimSocket);
      GSimSocket = 0;
      return 0;
      }
   if ((nRet == 4+6) && (txbuf->hdr.len.w == 6) && (txbuf->hdr.cmd.w == LCD_TOUCH))
      {
      if (edgep != NULL)
         *edgep = txbuf->buf.buf[0];                   /* level */
      if (levelp != NULL)
         *levelp = txbuf->buf.buf[1];                  /* level */
      x = txbuf->buf.buf[2] * 256 + txbuf->buf.buf[3]; /* x pos */
      y = txbuf->buf.buf[4] * 256 + txbuf->buf.buf[5]; /* y pos */
      if (xp != NULL)
         *xp = x;
      if (yp != NULL)
         *yp = y;
      nRet = ((txbuf->buf.buf[0] != 0) || (x != oldx) || (y != oldy)) ? 1 : 0; /* Change detection */
      oldx = x;
      oldy = y;
      if (nRet == 0)
         Sleep(1); /* Assure time is released for Simulator update in a poll loop */
      return (unsigned char) nRet;
      }
   else
      {
      PRINTERROR("ERROR during touch poll");
      return 0;
      }
   }
