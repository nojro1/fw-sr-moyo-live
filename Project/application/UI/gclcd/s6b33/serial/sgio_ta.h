/***********************************************************************
 *
 * This file contains I/O port definition for the target system and is
 * included by sgio.h
 *
 * This file is normally generated automatically by the SGSETUP tool using
 * the sgio_pc.h header file as a template. The definitions here correspond
 * to I/O port definitions in sgio_pc.h but are here using the I/O register
 * definition syntax for the target compiler.
 *
 * Modify these definitions so they fit the actual target system and the
 * I/O register definition syntax used by the actual target compiler.
 *
 * Copyright (c) RAMTEX International 2006
 * Version 1.0
 *
 **********************************************************************/

/* The example definitions here is for compilation test only.
   Different compilers may use different syntaxes for I/O access
 */

/* Simulate IOTESTER registers */
#define IOT_IOSET_REG3 (* (SGUCHAR volatile *) ( 12UL+3 ))
#define IOT_IOSET_REG2 (* (SGUCHAR volatile *) ( 12UL+2 ))
#define IOT_IOCLR_REG3 (* (SGUCHAR volatile *) ( 8UL+3 ))
#define IOT_IOCLR_REG2 (* (SGUCHAR volatile *) ( 8UL+2 ))




