#ifndef BUSSIM_H
#define BUSSIM_H
/***************************** bussim.c ************************************

   Definitions for the OLED bus simulator access functions.

   The definitions below should be modified to reflect the port layout in
   the target system and the given access function implementation.

   The defintions below assume the following hardware bit functionality of
   the "address" byte parameter to the access functions.

      A0 = RS  (Data/command select)

   RAMTEX Engineering Aps 2004

****************************************************************************/

/* Define SPI3 address for display controller, modify to fit hardware */

/*************** Do not modify the definitions below ********************/

#include <gdisphw.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Single chip mode -> access via user supplied access driver functions */
/* Serial mode only supports */

#define GHWRD  1  /* RS=1 */
#define GHWWR  1  /* RS=1 */
#define GHWSTA 0  /* RS=0 */
#define GHWCMD 0  /* RS=0 */

void simwrby(   SGUCHAR address, SGUCHAR dat);
void sim_reset( void );

#ifdef __cplusplus
}
#endif

#endif /* BUSSIM_H */
