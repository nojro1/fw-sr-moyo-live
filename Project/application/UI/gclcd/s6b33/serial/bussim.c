/***************************** bussim.c ************************************

   Generic template file for external bus simulator drivers.
   Implemented for serial (SPI-3) bus simulation using a parallel port.

   The functions below must be implemented as described in the comments.
   Use the proper I/O port instructions given by the actual processor
   architecture, the actual LCD connection method and the I/O access
   capabilities of the target C compiler.

   The portable sgwrby(..) and sgrdby(..) syntax is used for illustration.
   This syntax can be replaced by the compilers native syntax for I/O access.

   The functions in this module is called by the ghwinit.c module when the
   compiler switch GHW_SINGLE_CHIP is defined and GHW_NOHDW is undefined.

   This implementation is for use with the IOTESTER tool.
   (The IOTESTER operates with bit-block-set and bit-block-clear registers)
   The program structure may be reused with other processor platform
   optionally by use of the target compilers native syntax for bit I/O access

   In SPI-3 mode the RS (address value) is transmitted as first bit in an
   9 bit frame (address + 8 data bit)
   The GHW_BUS8 configuration mode should be used so all data and 
   instructions transfered to this driver is 8 bit.

   The driver assumes that the address parameter is either 0 or 1.

   Copyright (c) RAMTEX Engineering Aps 2008

****************************************************************************/
#ifndef GHW_NOHDW
#ifdef  GHW_SINGLE_CHIP

/* Define SPI bus bit locations in the output register
   (modify to fit the actual hardware) */
/* Data port bits */
#define  SDO   0x80   /* SPI data out bit (from processor) */
#define  SCLK  0x40   /* SPI clock bit */
/* Control bits */
#define  CS    0x04   /* LCD controller chip select */
#define  RST   0x08   /* LCD controller reset (negative pulse) */

#include <bussim.h>
#include <sgio.h> /* target I/O interface functions */

/*
   Simulate a serial bus write operation for a LCD controller via
   an I/O register interface.

   In this impelementation example the output register is assumed to
   support readback so unused register bits are not modified.

   The parallel port for write is here called P1 and accesed using the SG syntax.
   (Modify to a compiler specific syntax if nessesary to fit the
    actual hardware and compiler)

*/
static void tx_spi_bit(SGBOOL dat)
   {
   if (dat != 0)
      sgwrby(IOT_IOSET_REG3,SDO);  /* Set data high  (other bits is unchanged) */
   else
      sgwrby(IOT_IOCLR_REG3,~SDO); /* Set data high  (other bits is unchanged) */
   sgwrby(IOT_IOSET_REG3, SCLK);   /* Set clock high (other bits is unchanged) */
   sgwrby(IOT_IOCLR_REG3,~SCLK);   /* Set clock low  (other bits is unchanged) */
   }

/*
   Write SPI byte from hardware (SDI receive is ignored)
*/
static void tx_spi_byte(SGUCHAR dat)
   {
   /* Loop while clocking data out, msb first */
   register SGUCHAR msk;
   msk=0x80;
   do
      {
      tx_spi_bit(((msk & dat) != 0));
      msk >>= 1;
      }
   while(msk != 0);
   }

/*
   Write word to 3-wire SPI bus.
   The address is clocked as first bit, followed by data
*/
void simwrby(SGUCHAR adr, SGUCHAR dat)
   {
   /* Read state of unused port bits and initiate used bit for SPI access */
   sgwrby(IOT_IOCLR_REG2,~CS);   /* Set cs low  (other bits is unchanged) */
   tx_spi_bit(adr);              /* Send device access bit */
   tx_spi_byte(dat);             /* Send byte */
   sgwrby(IOT_IOSET_REG2, CS);   /* Set cs high  (other bits is unchanged) */
   }

/*
   Initialize and reset LCD display.
   Is called before simwrby() is invoked for the first time

   The LCD reset line is toggled here if it connected to a bus port.
   (it may alternatively be hard-wired to the reset signal to the processors
   in the target system).

   The sim_reset() function is invoked automatically via the ghw_io_init() function.
*/
void sim_reset( void )
   {
   /* Init data port setup (if required by port architecture) */
   /* Make CS,SDA,SCLK,RST to outputs (if required by port architecture) */
   sgwrby(IOT_IOCLR_REG3,~(SDO|SCLK));/* Set clock and data low  (other bits is unchanged) */
   sgwrby(IOT_IOSET_REG2,CS);         /* Set cs high             (other bits is unchanged) */

   sgwrby(IOT_IOCLR_REG2,~RST);       /* Toggle reset low->high  (other bits is unchanged) */
   sgwrby(IOT_IOSET_REG2,RST);
   }

#endif /* GHW_SINGLE_CHIP */
#endif /* GHW_NOHDW */


