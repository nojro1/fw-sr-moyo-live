/************************** ghwinit.c *****************************

   Low-level driver functions for the s6b33 LCD display controller
   initialization and error handling.


   The following LCD module characteristics MUST be correctly defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is copied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)

   Revision date:    18-04-08
   Revision Purpose: Driver module modified and specialized for s6b33 family
                     (All other low-level driver modules are equal to the S6D0129 lib)

    Revision date:    07-05-09
    Revision Purpose: The symbol software palette (data and functions) can
                      be optimized away if not used by defining
                      GHW_PALETTE_SIZE as 0 in gdispcfg.h
    Revision date:    11-11-10
    Revision Purpose: ghw_rgb_to_color(..) updated to use G_RGB_TO_COLOR(r,g,b) macro.

    Version number: 1.4
    Copyright (c) RAMTEX Engineering Aps 2008-2010

*********************************************************************/

#ifdef SGPCMODE
#include "windows.h"  /* Sleep(..) function used by ghw_cmd_wait(..)*/
#endif

#include <gdisphw.h>  /* HW driver prototypes and types */
#include <s6d0129.h>  /* controller specific definements */

/*#define WR_RD_TEST*/  /* Define to include write-read-back test in ghw_init() Not supported in serial mode */

/* Fix missing definitions in gdispcfg.h */
#ifndef GHW_XOFFSET
   #define GHW_XOFFSET 0
#endif
#ifndef GHW_YOFFSET
   #define GHW_YOFFSET 0
#endif

#ifdef GHW_S6B33F
 /* Primary command registers used by drawing and display handling functions
    (beside initialization) */
 #define GCTRL_H_WIN_ADR      0x43   /* Horizontal range address end,begin */
 #define GCTRL_V_WIN_ADR      0x42   /* Vertical range address end,begin */
 #define GDISP_ON             0x51
 #define GDISP_OFF            0x50
 #define GCTRL_CONTRAST1      0x2A
 #define GCTRL_CONTRAST2      0x2B
#else
 #error Unknown controller. Controller and bustype must be selected in gdispcfg.h
#endif

/*
   Define pixel width, height of internal video memory ( only used for the overflow check below)
   Note: If another display controller variant is used the adjust the GCTRLW, GCTRLH definitions
         below accordingly to match the size of the pixel video RAM in the controller.
   Note: If the physical memory range limits are exceeded at runtime then some controllers stop working.
*/
#define  GCTRLW 132
#define  GCTRLH 162

/* Check display size settings */
#ifdef GHW_ROTATED
  #if (((GDISPH+GHW_YOFFSET) > GCTRLW) || ((GDISPW+GHW_YOFFSET) > GCTRLH))
    #error (GDISPW, GDISPH, GHW_XOFFSET, GHW_YOFFSET, GHW_ROTATED configuration exceeds controller memory limits)
  #endif
#else
  #if (((GDISPW+GHW_XOFFSET) > GCTRLW) || ((GDISPH+GHW_YOFFSET) > GCTRLH))
    #error (GDISPW, GDISPH, GHW_XOFFSET, GHW_YOFFSET, GHW_ROTATED configuration exceeds controller memory limits)
  #endif
#endif


/********************* Chip access definitions *********************/

#ifndef GHW_NOHDW
   #if defined( GHW_SINGLE_CHIP)
      /* User defined access types and simulated register address def */
      #include <bussim.h>
      #ifdef GHW_BUS8
        #define  sgwrby(a,d) simwrby((a),(d))
        #define  sgrdby(a)   simrdby((a))
      #else
        #define  sgwrwo(a,d) simwrwo((a),(d))
        #define  sgrdwo(a)   simrdwo((a))
      #endif
   #else
      /* Portable I/O functions + hardware port def */
      #include <sgio.h>
   #endif
#else
   #undef GHW_SINGLE_CHIP /* Ignore single chip mode */
#endif

/***********************************************************************/
/** All static LCD driver data is located here in this ghwinit module **/
/***********************************************************************/

#ifdef GBASIC_INIT_ERR

/* Active foreground and background color */
GCOLOR ghw_def_foreground;
GCOLOR ghw_def_background;

#if (GHW_PALETTE_SIZE > 0)
/* Default grey scale palette
   The palette file can be edited directly with the ColorIconEdit program
*/
static GCODE GPALETTE_RGB FCODE ghw_palette[16] =
     #include <gcolor_4.pal>
     ;

/* Operative palette (current palette used for color lookup) */
GCOLOR ghw_palette_opr[16];
#endif

/* Use software font */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR  b[8];           /* Symbol data, fixed size = 8 bytes */
   }
GCODE FCODE sysfontsym[0x80] =
   {
   /* The default font MUST be a monospaced black & white (two-color) font */
   #include <sfs0129.sym> /* System font symbol table */
   };

/* Default system font */
GCODE GFONT FCODE SYSFONT =
   {
   6,      /* width */
   8,      /* height */
   sizeof(sysfontsym[0])-sizeof(GSYMHEAD), /* number of data bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL) sysfontsym,  /* pointer to array of SYMBOLS */
   0x80,   /* num symbols in sysfontsym[] */
   NULL    /* pointer to code page */ /* NULL means code page is not used */
   };

#ifdef GBUFFER
   #ifdef GHW_ALLOCATE_BUF
      /* <stdlib.h> is included via gdisphw.h */
      GCOLOR *gbuf = NULL;           /* Graphic buffer pointer */
      static SGBOOL gbuf_owner = 0;  /* Identify pointer ownership */
   #else
      GCOLOR gbuf[GBUFSIZE];         /* Graphic buffer */
   #endif
   GXT GFAST iltx,irbx;     /* "Dirty area" speed optimizers in buffered mode */
   GYT GFAST ilty,irby;
   SGBOOL  ghw_upddelay;    /* Flag for delayed update */
#else
   GCOLOR ghw_tmpbuf[GDISPW]; /* Row line buffer (for block read-modify-write) */
#endif /* GBUFFER */


#ifdef GHW_INTERNAL_CONTRAST
static SGUCHAR ghw_contrast;/* Current contrast value */
#endif

SGBOOL glcd_err;            /* Internal error */
#ifndef GNOCURSOR
GCURSOR ghw_cursor;         /* Current cursor state */
#endif

#ifdef GHW_PCSIM
/* PC simulator declaration */
void ghw_init_sim( SGUINT dispw, SGUINT disph );
void ghw_exit_sim(void);
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye);
void ghw_set_xy_sim(GXT xb, GYT yb);
void ghw_autowr_sim( GCOLOR cval );
GCOLOR ghw_autord_sim( void );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
#endif
/****************************************************************/
/** Low level interface functions used only by ghw_xxx modules **/
/****************************************************************/

/* Bit mask values */
GCODE SGUCHAR FCODE sympixmsk[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};


typedef struct
   {
   SGUCHAR value;
   SGUCHAR delay;
   } S1D_REGS;


#if ( defined( GHW_COLOR_SWAP ))
  #define GRGB 0x02
#else
  #define GRGB 0x00
#endif

#if ( defined( GHW_MIRROR_VER ))
  #define GMY 0x01
#else
  #define GMY 0x00
#endif

#if ( defined( GHW_MIRROR_HOR ))
  #define GMX 0x04
#else
  #define GMX 0x00
#endif

#ifdef GHW_ROTATED
#define DISPH GDISPW
#define DISPW GDISPH
#else
#define DISPH GDISPH
#define DISPW GDISPW
#endif

/* Fix missing definitions in gdispcfg.h (automate offset setting with rotation) */
#ifdef GHW_ROTATED
   #ifndef GHW_XOFFSET
     #ifdef GHW_MIRROR_HOR
     #define GHW_XOFFSET (GCTRLW-DISPW)
     #else
      #define GHW_XOFFSET 0
     #endif
   #endif
   #ifndef GHW_YOFFSET
     #ifdef GHW_MIRROR_VER
      #define GHW_YOFFSET 0
     #else
      #define GHW_YOFFSET (GCTRLH-DISPH)
     #endif
   #endif
#else
   #ifndef GHW_XOFFSET
     #ifdef GHW_MIRROR_HOR
      #define GHW_XOFFSET (GCTRLW-DISPW)
     #else
      #define GHW_XOFFSET 0
     #endif
   #endif
   #ifndef GHW_YOFFSET
      #ifdef GHW_MIRROR_VER
       #define GHW_YOFFSET 0
      #else
       #define GHW_YOFFSET (GCTRLH-DISPH)
      #endif
   #endif
#endif

/* Array of configuration descriptors, the registers are initialized in the order given in the table */
static GCODE S1D_REGS FCODE as1dregs[] =
   {
   {0x2c,20}, /* Standby off cmd */

   {0x02,00}, /* Oscillator mode cmd */
   {0x01,20}, /* osc on, delay 20 ms */

   {0x26,00}, /* DCDC1 on */
   {0x01,20}, /* delay 20 ms */

   {0x26,00}, /* AMP on */
   {0x09,20}, /* delay 20 ms */

   {0x26,00}, /* DCDC2 on */
   {0x0b,20}, /* delay 20 ms */

   {0x26,00}, /* DCDC3 on */
   {0x0f,20}, /* delay 20 ms */

   {0x20,00}, /* DC-DC select */
   {0x5,00},  /* 0x0 = x1, 0x5 = x1.5, 0xa = x2 */

   {0x22,00}, /* Bias */
   {0x11,00}, /* 0x00 = 3/4V1, 0x11 = V1, 0x22 = 5/6V1 */

   {0x2a,00}, /* Contrast (preliminary, modified later by contrast function) */
   {170,00},

   {0x10,00}, /* Driver output mode (Note CDIR pin should be 0 for this setting to work) */
   #if (DISPH <= 96)
   {0x30 | GMY | GMX | GRGB,00},
   #elif (DISPH <= 132)
   {0x00 | GMY | GMX | GRGB,00},
   #elif (DISPH <= 144)
   {0x10 | GMY | GMX | GRGB,00},
   #else
   {0x20 | GMY | GMX | GRGB,00},
   #endif

   {0x30,00},  /* Addressing mode */
   #if (GDISPPIXW == 16)
   {0x1c,00},
   #elif (GDISPPIXW == 8)
   {0xdd,00},
   #else
   #error Illegal color depth resolution in gdispcfg.h
   #endif

   {0x32,00},  /* Row vector mode */
   {0x01,00},  /* Interlatched sequence */

   {0x55,00},  /* Partial display off */
   {0x00,00},  /* */

   {0x40,00},    /* Entry mode */
   #ifdef GHW_ROTATED
   {0x02,00},    /* Y address counter mode */
   #else
   {0x00,00},    /* X address counter mode */
   #endif

   {0x53,00},
   {0x00,100}   /* 0 = normal disp, 1 = inverse, 2 = off, 3 = on*/
   };


static void ghw_cmd_wr(SGUCHAR cmd)
   {
   #ifndef GHW_NOHDW
   #if defined(GHW_BUS8)
   /* 8 bit data bus */
   sgwrby(GHWCMD, cmd);  /* Lsb */
   #else
   /* 16 bit data bus */
   sgwrwo(GHWCMDW, (SGUINT) cmd);
   #endif
   #else  /* GHW_NOHDW */
   cmd++; /* silience 'not used' warning */
   #endif
   }

/*
   Set the y range.
   The row position is set to y.
   After last write on row y2 the write position is reset to y
   Internal ghw function
*/
void ghw_set_xyrange(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   #ifdef GHW_PCSIM
   ghw_set_xyrange_sim( xb, yb, xe, ye);
   #endif

   #ifndef GHW_NOHDW

   /* Set window range */
   #if defined(GHW_BUS8)

   /* 8 bit data bus */
   sgwrby(GHWCMD, GCTRL_H_WIN_ADR);
   #ifdef GHW_ROTATED
   sgwrby(GHWCMD, yb+GHW_XOFFSET);
   sgwrby(GHWCMD, ye+GHW_XOFFSET);
   #else
   sgwrby(GHWCMD, xb+GHW_XOFFSET);
   sgwrby(GHWCMD, xe+GHW_XOFFSET);
   #endif

   sgwrby(GHWCMD, GCTRL_V_WIN_ADR);
   #ifdef GHW_ROTATED
   sgwrby(GHWCMD, xb+GHW_YOFFSET);
   sgwrby(GHWCMD, xe+GHW_YOFFSET);
   #else
   sgwrby(GHWCMD, yb+GHW_YOFFSET);
   sgwrby(GHWCMD, ye+GHW_YOFFSET);
   #endif

   #else
   /* 16 bit data bus */
   sgwrwo(GHWCMDW, (SGUINT)GCTRL_H_WIN_ADR);
   #ifdef GHW_ROTATED
   sgwrwo(GHWCMDW, (SGUINT)(yb+GHW_XOFFSET));
   sgwrwo(GHWCMDW, (SGUINT)(ye+GHW_XOFFSET));
   #else
   sgwrwo(GHWCMDW, (SGUINT)(xb+GHW_XOFFSET));
   sgwrwo(GHWCMDW, (SGUINT)(xe+GHW_XOFFSET));
   #endif

   sgwrwo(GHWCMDW, (SGUINT)GCTRL_V_WIN_ADR);
   #ifdef GHW_ROTATED
   sgwrwo(GHWCMDW, (SGUINT)(xb+GHW_YOFFSET));
   sgwrwo(GHWCMDW, (SGUINT)(xe+GHW_YOFFSET));
   #else
   sgwrwo(GHWCMDW, (SGUINT)(yb+GHW_YOFFSET));
   sgwrwo(GHWCMDW, (SGUINT)(ye+GHW_YOFFSET));
   #endif
   #endif

   #endif /* GHW_NOHDW */
   }

void ghw_setxypos(GXT xb, GYT yb)
   {
   #ifdef GHW_PCSIM
   ghw_set_xy_sim( xb, yb);
   #endif
   ghw_set_xyrange(xb, yb, GDISPW-1, GDISPH-1);
   }

/*
   Write databyte to controller (at current position) and increment
   internal xadr.

   Internal ghw function
*/
void ghw_auto_wr(GCOLOR dat)
   {
   #ifdef GHW_PCSIM
   ghw_autowr_sim( dat );
   #endif

   #ifndef GHW_NOHDW
    #if ((GDISPPIXW == 8) && defined( GHW_BUS8 ))
    sgwrby(GHWWR, (SGUCHAR )(dat));
    #elif ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
    sgwrby(GHWWR, (SGUCHAR )(dat >> 8));
    sgwrby(GHWWR, (SGUCHAR )(dat));
    #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
    /* 16 bit bus mode, 16 bit color */
    sgwrwo(GHWWRW, dat);           /* 16 bit color */
    #else
    #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
    #endif
   #endif /* GHW_NOHDW */
   }

#if (!defined( GHW_NO_LCD_READ_SUPPORT ) && (!defined( GBUFFER ) || defined(WR_RD_TEST)))

/*
   Perform required dummy reads after column position setting
*/
void ghw_auto_rd_start(void)
   {
   #ifndef GHW_NOHDW
   /* One dummy read after setup */
   GCOLOR ret = 0;
   #if (defined( GHW_BUS8 ))
   ret += (GCOLOR) sgrdby(GHWRD);  /* Dummy read + Silence "not used" warning */
   #else
   ret += (GCOLOR) sgrdwo(GHWRDW); /* Dummy read + Silence "not used" warning */
   #endif
   #endif
   }

/*
   Read databyte from controller at specified address
   Only used in non-buffered mode or during test

   Internal ghw function
*/
GCOLOR ghw_auto_rd(void)
   {
   GCOLOR ret;
   #ifndef GHW_NOHDW
    #if   ((GDISPPIXW == 8) && defined( GHW_BUS8 ))
    ret = (GCOLOR) sgrdby(GHWRD);
    #elif ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
    ret = (((GCOLOR) sgrdby(GHWRD)) << 8); /* MSB*/
    ret |=  (GCOLOR) sgrdby(GHWRD);        /* LSB*/
    #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
    /* 16 bit bus mode, 16 bit color */
    ret =  (GCOLOR) sgrdwo(GHWRDW); /* 16 bit color */
    #else
    #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
    #endif

   #else /* GHW_NOHDW */

    #ifdef GHW_PCSIM
     ret = ghw_autord_sim();
    #else
     ret = 0;
    #endif

   #endif /* GHW_NOHDW */
   return ret;
   }
#endif

/***********************************************************************/
/**        s6d0129 Initialization and error handling functions       **/
/***********************************************************************/

/*
   Change default (palette) colors
*/
void ghw_setcolor(GCOLOR fore, GCOLOR back)
   {
   /* Update active colors */
   ghw_def_foreground = fore;
   ghw_def_background = back;
   }

/*
   Convert an RGB structure to a color value using the current color mode
*/
GCOLOR ghw_rgb_to_color( GCONSTP GPALETTE_RGB *palette )
   {
   if (palette == NULL)
      return 0;

   #if (GDISPPIXW == 8)
   return (GCOLOR)(palette->r & 0xe0) |
           ((palette->g >> 3) & 0x1c) |
           ((palette->b) >> 6) & 0x3;
   #else (GDISPPIXW == 16)
   return
      (((GCOLOR)(palette->r & 0xf8)) << 8) |
      (((GCOLOR)(palette->g & 0xfc)) << 3) |
       ((GCOLOR)(palette->b >> 3));
   #endif
   }

#if (GHW_PALETTE_SIZE > 0)
/*
   Load a new palette or update the existing palette
   (Palette is only used with symbols using 2 or 4 bits pr pixel)
*/
SGBOOL ghw_palette_wr(SGUINT start_index, SGUINT num_elements, GCONSTP GPALETTE_RGB PFCODE *palette)
   {
   if ((num_elements == 0) ||
       ((start_index + num_elements) > 16) ||
       (palette == NULL))
      {
      glcd_err = 1;
      return 1;
      }
   glcd_err = 0;

   /* (Partial) update of operative palette values */
   while(num_elements-- > 0)
      {
      /* Make local palette copy here to be compatible with compilers
         having a non-standard conforming handling of pointer
         (i.e when PFCODE memory qualifer is used) */
      GPALETTE_RGB pal;
      pal.r = palette->r;
      pal.g = palette->g;
      pal.b = palette->b;
      ghw_palette_opr[start_index++] = ghw_rgb_to_color(&pal);
      palette++;

      /* ghw_palette_opr[start_index++] = ghw_rgb_to_color(&palette++); */
      }

   return glcd_err;
   }
#endif

/*
   Fast set or clear of LCD module RAM buffer
   Internal ghw function
*/
static void ghw_bufset(GCOLOR color)
   {
   /* Use hardware accelerator logic */
   GBUFINT cnt;
   cnt = 0;
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   do
      {
      /* Clear using X,Y autoincrement */
      ghw_auto_wr(color);  /* Set LCD buffer */
      #ifdef GBUFFER
      gbuf[cnt] = color; /* Set ram buffer as well */
      #endif
      }
   while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH)); /* Loop until x+y wrap */
   }


#ifdef WR_RD_TEST

#include <stdio.h>
/*
   Make write-readback test on controller memory.

   This test will fail if some databus and control signals is not connected correctly.

   This test will fail if 16/8 bit bus mode selection in the configuration settings
   does not match the actual bus configuration for the hardware (display and processor
   16/8 bit bus width, 8080/6800 bus type settings, word / byte address offsets, etc).

   This test may fail if illegal GCTRLW, GCTRLH, GHW_XOFFSET, GHW_YOFFSET
   configuration settings cause overrun of the on-chip video RAM.

   This test can be exectuted correctly with only logic power on the display module.
   No high-level voltages are nessesary for the test to run (although nothing then can
   be shown on the display)

   Return 0 if no error,
   Return != 0 if some readback error is detected (the bit pattern may give information
   about connector pins in error)
*/
static GCOLOR ghw_wr_rd_test(void)
   {
   #ifndef GHW_NOHDW
   int i;
   GCOLOR msk,result;
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);

   /* 16 bit color mode */
   printf("\n");
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      printf("0x%04x ", (unsigned int) msk);
      ghw_auto_wr(~msk);
      printf(" 0x%04x\n", (unsigned int) (~msk & 0xffff));
      msk <<= 1;
      }
   printf("\n");
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);

   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val;
      val = ghw_auto_rd();
      result |= (val ^ msk);
      printf("0x%04x ",  (unsigned short) val);
      val = ghw_auto_rd();
      printf(" 0x%04x\n", (unsigned short) val );
      result |= (val ^ (~msk));
      msk <<= 1;
      }
   return result;  /* 0 = Nul errors */
   #else
   return 0; /* 0 = Nul errors */
   #endif
   }

#endif /* WR_RD_TEST */

/*
   Wait a number of milli seconds
*/
static void ghw_cmd_wait(SGUCHAR ms)
   {
   #ifdef SGPCMODE
   Sleep(ms); /* delay x 1 ms */
   #else
   SGUINT wait1ms;
   while (ms-- != 0)
      {
      wait1ms = 2000;     /* Adjust to achieve a 1 ms loop below */
      while( wait1ms != 0)
         wait1ms--;
      }
   #endif
   }

/*
   Initialize display, clear ram  (low-level)
   Clears glcd_err status before init

   Return 0 if no error,
   Return != 0 if some error
*/
SGBOOL ghw_init(void)
   {
   short i;
   #ifdef GBUFFER
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;
   ghw_upddelay = 0;
   #endif

   glcd_err = 0;
   ghw_io_init(); /* Set any hardware interface lines, controller hardware reset */

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL)
      {
      /* Allocate graphic ram buffer */
      if ((gbuf = (GCOLOR *)calloc(ghw_gbufsize(),1)) == NULL)
         glcd_err = 1;
      else
         gbuf_owner = 1;
      }
   #endif

   if (glcd_err != 0)
      return 1;

   #ifdef GHW_PCSIM
   /* Tell simulator about the visual LCD screen organization */
   ghw_init_sim( GDISPW, GDISPH );
   #endif
   /* Set default colors */
   ghw_setcolor( (GCOLOR) GHW_PALETTE_FOREGROUND, (GCOLOR) GHW_PALETTE_BACKGROUND );

   #if (GHW_PALETTE_SIZE > 0)
   /* Load palette */
   ghw_palette_wr(0, sizeof(ghw_palette)/sizeof(GPALETTE_RGB), (GCONSTP GPALETTE_RGB PFCODE *)&ghw_palette[0]);
   #endif

   /* Initialize controller according to configuration file */
   for (i=0; i < sizeof(as1dregs)/sizeof(S1D_REGS); i++)
      {
      ghw_cmd_wr(as1dregs[i].value);
      if (as1dregs[i].delay != 0)
         ghw_cmd_wait( as1dregs[i].delay );
      }

   /*
      Stimuli test loops for initial oscilloscope test of display interface bus signals
      Uncomment to use the test loop for the given data bus width.
      It is recommended to check all display bus signals with each of the I/O access
      statements in the loop one by one.
   */
   /*
   #ifdef GHW_BUS8
   for(;;)
      {
      SGUCHAR dat;
      sgwrby(GHWCMD,0x00);
      sgwrby(GHWWR,0xff);
      dat = sgrdby(GHWSTA);
      dat = sgrdby(GHWRD);
      }
   #endif
   */
   /*
   #ifdef GHW_BUS16
   for(;;)
      {
      SGUINT dat;
      sgwrwo(GHWCMDW,0xffff);
      sgwrwo(GHWWRW,0x0000);
      dat = sgrdwo(GHWSTAW);
      dat = sgrdwo(GHWRDW);
      }
   #endif
   */

   ghw_dispon(); /* Turn on here for test */

   #ifdef WR_RD_TEST
   /*
      NOTE:
      The call of ghw_wr_rd_test() should be commented out in serial mode.
      In serial mode the display controller  does not provide read-back facility
      and this test will always fail.
   */
   if (ghw_wr_rd_test() != ((GCOLOR) 0))
      {
      /* Controller memory write-readback error detected
      (Check the cable or power connections to the display) */
      G_WARNING("Hardware interface error\nCheck display connections\n");  /* Test Warning message output */
      glcd_err = 1;
      return 1;
      }
   #endif

   ghw_cont_set(70);
/*   ghw_dispon(); */
/*   #if (GDISPPIXW == 16) */
/*   ghw_bufset( 0xf800 ); */
/*   ghw_bufset( 0x001f ); */
/*   #else */
/*   ghw_bufset( 0xe0 ); */
/*   ghw_bufset( 0x03 ); */
/*   #endif */

   ghw_bufset( ghw_def_background );
   ghw_dispon();

   #ifndef GNOCURSOR
   ghw_cursor = GCURSIZE1;    /* Cursor is off initially */
   /* ghw_cursor = GCURSIZE1 | GCURON; */ /* Uncomment to set cursor on initially */
   #endif

   ghw_updatehw();  /* Flush to display hdw or simulator */

   return (glcd_err != 0) ? 1 : 0;
   }

/*
   Return last error state. Called from applications to
   check for LCD HW or internal errors.
   The error state is reset by ghw_init and all high_level
   LCD functions.

   Return == 0 : No errors
   Return != 0 : Some errors
*/
SGUCHAR ghw_err(void)
   {
   #if (defined(_WIN32) && defined( GHW_PCSIM))
   if (GSimError())
      return 1;
   #endif
   return (glcd_err == 0) ? 0 : 1;
   }


/*
   Display a (fatal) error message.
   The LCD display module is always cleared and initialized to
   the system font in advance.
   The error message is automatically centered on the screen
   and any \n characters in the string is processed.

   str = ASCII string to write at display center
*/
void ghw_puterr( PGCSTR str )
   {
   PGCSTR idx;
   SGUINT xcnt;
   GXT xp;
   GYT yp,h,y, sidx;
   PGSYMBYTE psym;
   GCOLOR pval;
   SGUCHAR val;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   #endif

   if (ghw_init() != 0)  /* (Re-) initialize display */
      return;            /* Some initialization error */

   /* Count number of lines in string */
   idx=str;
   if (idx == NULL)
      return;
   xcnt = 1;
   while(*idx)
      {
      if (*(idx++) == '\n')
         xcnt++;
      }

   /* Set start character line */
   h = SYSFONT.symheight;
   yp = (xcnt*h > GDISPH) ? 0 : ((GDISPH-1)-xcnt*h)/2;
   /* Set character height in pixel lines */

   idx=str;
   do
      {
      xcnt=0;  /* Set start x position so line is centered */
      while ((idx[xcnt]!=0) && (idx[xcnt]!='\n') && (xcnt < GDISPBW))
         {
         xcnt++;
         }

      /* Calculate start position for centered line */
      xp = (GDISPW-xcnt*SYSFONT.symwidth)/2;

      /* Display text line */
      while (xcnt-- > 0)
         {
         /* Point to graphic content for character symbol */
         psym = &(sysfontsym[(*idx) & 0x7f].b[0]);
         ghw_set_xyrange(xp,yp,xp+SYSFONT.symwidth-1,yp+(h-1));

         /* Display rows in symbol */
         for (y = 0; y < h; y++)
            {
            /* Get symbol row value */
            val = *psym++;
            /* Initiate LCD controller address pointer */
            #ifdef GBUFFER
            gbufidx = GINDEX(xp, (GBUFINT)yp+y );
            #endif

            /* Display colums in symbol row */
            for (sidx = 0; sidx < SYSFONT.symwidth; sidx++)
               {
               if ((val & sympixmsk[sidx]) != 0)
                  pval = ghw_def_foreground;
               else
                  pval = ghw_def_background;

               /* End of symbol or end of byte reached */
               #ifdef GBUFFER
               gbuf[gbufidx++] = pval;
               #endif
               ghw_auto_wr(pval);
               }
            }

         idx++;
         xp += SYSFONT.symwidth; /* Move to next symbol in line */
         }

      /* Next text line */
      yp += h;
      if (*idx == '\n')
         idx++;
      }
   while ((*idx != 0) && (yp < GDISPH));

   ghw_updatehw();  /* Flush to display hdw or simulator */
   }

void ghw_exit(void)
   {
   #if defined( GHW_ALLOCATE_BUF)
   if (gbuf != NULL)
      {
      if (gbuf_owner != 0)
         {
         /* Buffer is allocated by ginit, so release graphic buffer here */
         free(gbuf);
         gbuf_owner = 0;
         }
      gbuf = NULL;
      }
   #endif
   ghw_io_exit();         /* Release any LCD hardware resources, if required */
   #ifdef GHW_PCSIM
   ghw_exit_sim(); /* Release simulator resources */
   #endif
   }

#ifndef GNOCURSOR
/*
   Replace cursor type data (there is no HW cursor support in s6d0129)
*/
void ghw_setcursor( GCURSOR type)
   {
   ghw_cursor = type;
   #ifdef GHW_ALLOCATE_BUF
   if (gbuf == NULL)
      glcd_err = 1;
   #endif
   }
#endif


/*
   Turn display off
   (Minimize power consumption)
*/
void ghw_dispoff(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif
   ghw_cmd_wr(GDISP_OFF);     /* Blank display */
   }

/*
   Turn display on
*/
void ghw_dispon(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif
   ghw_cmd_wr(GDISP_ON);     /* Restore display */
   }

#ifdef GHW_INTERNAL_CONTRAST
/*
   Set contrast (Normalized value range [0 : 99] )
   Returns the old value.
*/
SGUCHAR ghw_cont_set(SGUCHAR contrast)
   {
   SGUCHAR tmp;
   GLIMITU(contrast,99);
   tmp = ghw_contrast;
   ghw_contrast = contrast;

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL) {glcd_err = 1; return contrast;}
   #endif

   ghw_cmd_wr(GCTRL_CONTRAST1);
   ghw_cmd_wr((SGUCHAR)((((SGUINT) contrast) *255) / 100));  /* Set contrast level (1-255) */

   return tmp;
   }

/*
   Change contrast (Normalized value range [-99 : +99] )
   Returns the old value.
*/
SGUCHAR ghw_cont_change(SGCHAR contrast_diff)
   {
   SGINT tmp = (SGINT) ghw_contrast;
   tmp += (SGINT) contrast_diff;
   GLIMITU(tmp,99);
   GLIMITD(tmp,0);
   return ghw_cont_set((SGUCHAR)tmp);
   }
#endif /* GHW_INTERNAL_CONTRAST */

#if defined( GHW_ALLOCATE_BUF)
/*
   Size of buffer requied to save the whole screen state
*/
GBUFINT ghw_gbufsize( void )
   {
   return (GBUFINT) GBUFSIZE * sizeof(GCOLOR) + (GBUFINT) sizeof(GHW_STATE);
   }

#ifdef GSCREENS
/*
   Check if screen buf owns the screen ressources.
*/
SGUCHAR ghw_is_owner( SGUCHAR *buf )
   {
   return (((GCOLOR *)buf == gbuf) && (gbuf != NULL)) ? 1 : 0;
   }

/*
   Save the current state to the screen buffer
*/
SGUCHAR *ghw_save_state( SGUCHAR *buf )
   {
   GHW_STATE *ps;
   if (!ghw_is_owner(buf))
      return NULL;

   ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);
   ps->upddelay = (ghw_upddelay != 0);
   #ifndef GNOCURSOR
   ps->cursor = ghw_cursor;
   #endif
   ps->foreground = ghw_def_foreground; /* Palette may vary, save it */
   ps->background = ghw_def_background;
   return (SGUCHAR *) gbuf;
   }

/*
   Set state to buf.
   If buffer has not been initiated by to a screen before, only
   the pointer is updated. Otherwise the the buffer
*/
void ghw_set_state(SGUCHAR *buf, SGUCHAR doinit)
   {
   if (gbuf != NULL)
      {
      /* The LCD controller has been initiated before */
      if (gbuf_owner != 0)
         {
         /* Buffer was allocated by ginit, free it so screen can be used instead*/
         free(gbuf);
         gbuf_owner = 0;
         gbuf = NULL;
         }
      }

   if (doinit != 0)
      {
      /* First screen initialization, just set buffer pointer and
         leave rest of initialization to a later call of ghw_init() */
      gbuf = (GCOLOR *) buf;
      gbuf_owner = 0;
      }
   else
      {
      if ((gbuf = (GCOLOR *) buf) != NULL)
         {
         GHW_STATE *ps;
         ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);

         #ifndef GNOCURSOR
         ghw_cursor = ps->cursor;
         #endif
         ghw_upddelay = 0;        /* Force update of whole screen */
         iltx = 0;
         ilty = 0;
         irbx = GDISPW-1;
         irby = GDISPH-1;
         ghw_updatehw();
         ghw_upddelay = (ps->upddelay != 0) ? 1 : 0;
         /* Restore drawing color */
         ghw_setcolor(ps->foreground, ps->background);
         }
      }
   }
#endif  /* GSCREENS */
#endif  /* GHW_ALLOCATE_BUF */

#endif /* GBASIC_INIT_ERR */


