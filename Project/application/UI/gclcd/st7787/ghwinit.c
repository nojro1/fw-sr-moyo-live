/************************** ghwinit.c *****************************

   Low-level driver functions for the ST7787 LCD display controller
   initialization and error handling.

   Notes: Data read require a dummy read before first read
          ST7787 always return read data in the 18 bit pixel format
          (not documented in the ST7787 data sheet 2007.6.14)

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is copied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)


   ST7787 chip and driver support these mode combinations
        8 bit bus, 16 bit color
        8 bit bus, 18 bit color (mapping to 24 bit mode done by software)
        8 bit bus, 24 bit color
        16 bit bus, 16 bit color
        32 bit bus. 18 bit color (18 lsb bus bits are used)
        32 bit bus. 24 bit color (18 mapping to 24 bit mode in software)

   Revision date:    03-09-07
   Revision Purpose: Minor optimizations. Corrections to 18 & 24 bit-pr-pixel modes
   Revision date:    03-09-07
   Revision Purpose: Minor optimizations. Corrections to 18 & 24 bit-pr-pixel modes
   Revision date:    07-05-09
   Revision Purpose: The symbol software palette (data and functions) can
                     be optimized away if not used by defining
                     GHW_PALETTE_SIZE as 0 in gdispcfg.h
   Revision date:    11-11-10
   Revision Purpose: ghw_rgb_to_color(..) updated to use G_RGB_TO_COLOR(r,g,b) macro.

   Version number: 1.5
   Copyright (c) RAMTEX Engineering Aps 2007-2010

*********************************************************************/

#ifdef SGPCMODE
#include "windows.h"  /* Sleep(..) function used by ghw_cmd_wait(..)*/
#endif

#include <gdisphw.h>  /* HW driver prototypes and types */
#include <s6d0129.h>  /* Controller specific definements */

/* #define WR_RD_TEST */   /* Define to include write-read-back test in ghw_init() */

/* Mirror and rotation definition bits (to simplify initialization below) */
#ifdef GHW_COLOR_SWAP
   #define  RGB_BIT 0x08
#else
   #define  RGB_BIT 0x00
#endif
#ifdef GHW_MIRROR_HOR
   #define  MX_BIT 0x40
#else
   #define  MX_BIT 0x00
#endif
#ifdef GHW_MIRROR_VER
   #define  MY_BIT 0x80
#else
   #define  MY_BIT 0x00
#endif
#ifdef GHW_ROTATED
   #define  MV_BIT 0x20
#else
   #define  MV_BIT 0x00
#endif
#define    MSF_BIT 0x01

#ifdef GHW_ST7787
   /* Define those registers (commands) used by driver */
   #define GCTRL_RESET     0x01
   #define GCTRL_SLPIN     0x10
   #define GCTRL_SLPOUT    0x11
   #define GCTRL_NORON     0x13
   #define GCTRL_INVON     0x21
   #define GCTRL_GAMSET    0x26
   #define GCTRL_DISPOFF   0x28
   #define GCTRL_DISPON    0x29
   #define GCTRL_CASET     0x2A
   #define GCTRL_RASET     0x2B
   #define GCTRL_RAMWR     0x2C
   #define GCTRL_RAMRD     0x2E
   #define GCTRL_RGBCTRL   0x2D
   #define GCTRL_MADCTRL   0x36
   #define GCTRL_COLMOD    0x3A
   #define GCTRL_RGBCTR    0xB0
   #define GCTRL_FRAMERATE 0xB1
   #define GCTRL_SETDISP   0xB2
   #define GCTRL_SETCYC    0xB4
   #define GCTRL_SETVCOM   0xB6


   #define GCTRL_PWRCTR1   0xC0
   #define GCTRL_PWRCTR2   0xC1
   #define GCTRL_PWRCTR3   0xC2
   #define GCTRL_PWRCTR4   0xC3
   #define GCTRL_PWRCTR5   0xC4

   #define GCTRL_VCOM_MULTI_MODE 0xFB
   #define GCTRL_VMCTR1    0xC5
   #define GCTRL_VMCTR2    0xC6
   #define GCTRL_DISSET5   0xB6

   #define GCTRL_GAMCTRP1  0xE0
   #define GCTRL_GAMCTRN1  0xE1

   #define GCTRL_VSYNCOUT  0xBC

#else
  #error Unknown controller, Controller and bustype must be selected in gdispcfg.h
#endif

#ifdef GBASIC_INIT_ERR


/*
   Define pixel width, height of internal video memory ( only used for the overflow check below)
   Note: If another display controller variant is used the adjust the GCTRLW, GCTRLH definitions
         below accordingly to match the size of the pixel video RAM in the controller.
   Note: If the physical memory range limits are exceeded at runtime then some controllers stop working.
*/
#define GCTRLH 320

/* Fix missing definitions in gdispcfg.h */
#ifndef GHW_XOFFSET
   #define GHW_XOFFSET 0
#endif
#ifndef GHW_YOFFSET
   #define GHW_YOFFSET 0
#endif

/* Set size of active internal RAM, Check display size settings */
#ifdef GHW_ROTATED
  #if ((GDISPH+GHW_YOFFSET) <= 180)
    #define GCTRLW 180
  #elif ((GDISPH+GHW_YOFFSET) <= 208)
    #define GCTRLW 208
  #else
    #define GCTRLW 240
  #endif
  #if (((GDISPH+GHW_YOFFSET) > GCTRLW) || ((GDISPW+GHW_YOFFSET) > GCTRLH))
    #error (GDISPW, GDISPH, GHW_XOFFSET, GHW_YOFFSET, GHW_ROTATED configuration exceeds controller memory limits)
  #endif
   /* Map and swap offsets */
  #define G_XOFFSET GHW_YOFFSET
  #define G_YOFFSET GHW_XOFFSET
#else
  #if ((GDISPW+GHW_XOFFSET) <= 180)
    #define GCTRLW 180
  #elif  ((GDISPW+GHW_XOFFSET) <= 208)
    #define GCTRLW 208
  #else
    #define GCTRLW 240
  #endif
  #if (((GDISPW+GHW_XOFFSET) > GCTRLW) || ((GDISPH+GHW_YOFFSET) > GCTRLH))
    #error (GDISPW, GDISPH, GHW_XOFFSET, GHW_YOFFSET, GHW_ROTATED configuration exceeds controller memory limits)
  #endif
  /* Map offsets */
  #define G_XOFFSET GHW_YOFFSET
  #define G_YOFFSET GHW_XOFFSET
#endif

/********************* Chip access definitions *********************/

#ifndef GHW_NOHDW
   #if defined( GHW_SINGLE_CHIP)
      /* User defined access types and simulated register address def */
      #include <bussim.h>
      #ifdef GHW_BUS8
        #define  sgwrby(a,d) simwrby((a),(d))
        #define  sgrdby(a)   simrdby((a))
      #elif defined (GHW_BUS32)
        #define  sgwrdw(a,d) simwrdw((a),(d))
        #define  sgrddw(a)   simrddw((a))
      #else /* bus 16 */
        #define  sgwrwo(a,d) simwrwo((a),(d))
        #define  sgrdwo(a)   simrdwo((a))
      #endif
   #else
      /* Portable I/O functions + hardware port def */
      #include <sgio.h>
   #endif
#else
   #undef GHW_SINGLE_CHIP /* Ignore single chip mode */
#endif

/***********************************************************************/
/** All static LCD driver data is located here in this ghwinit module **/
/***********************************************************************/

/* Active foreground and background color */
GCOLOR ghw_def_foreground;
GCOLOR ghw_def_background;

#ifdef GHW_BUS16
static SGUCHAR ghw_oddpos; /* Flag for odd pos used during read back */
#endif

#if (GHW_PALETTE_SIZE > 0)
/* Default soft palette
   The palette file can be edited directly with the ColorIconEdit program
*/
static GCODE GPALETTE_RGB FCODE ghw_palette[16] =
     #include <gcolor_4.pal>
     ;

/* Operative palette (current palette used for color lookup) */
GCOLOR ghw_palette_opr[16];
#endif

/* Use software font */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR  b[8];           /* Symbol data, fixed size = 8 bytes */
   }
GCODE FCODE sysfontsym[0x80] =
   {
   /* The default font MUST be a monospaced black & white (two-color) font */
   #include <sfs0129.sym> /* System font symbol table */
   };

/* Default system font */
GCODE GFONT FCODE SYSFONT =
   {
   6,      /* width */
   8,      /* height */
   sizeof(sysfontsym[0])-sizeof(GSYMHEAD), /* number of data bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL) sysfontsym,  /* pointer to array of SYMBOLS */
   0x80,   /* num symbols in sysfontsym[] */
   NULL    /* pointer to code page */ /* NULL means code page is not used */
   };

#ifdef GBUFFER
   #ifdef GHW_ALLOCATE_BUF
      /* <stdlib.h> is included via gdisphw.h */
      GCOLOR *gbuf = NULL;           /* Graphic buffer pointer */
      static SGBOOL gbuf_owner = 0;   /* Identify pointer ownership */
   #else
      GCOLOR gbuf[GBUFSIZE];         /* Graphic buffer */
   #endif
   GXT GFAST iltx,irbx;     /* "Dirty area" speed optimizers in buffered mode */
   GYT GFAST ilty,irby;
   SGBOOL  ghw_upddelay;    /* Flag for delayed update */
#else
   GCOLOR ghw_tmpbuf[GDISPW]; /* Row line buffer (for block read-modify-write) */
#endif /* GBUFFER */


#ifdef GHW_INTERNAL_CONTRAST
static SGUCHAR ghw_contrast;/* Current contrast value */
#endif

SGBOOL glcd_err;            /* Internal error */
#ifndef GNOCURSOR
GCURSOR ghw_cursor;         /* Current cursor state */
#endif

#ifdef GHW_PCSIM
/* PC simulator declaration */
void ghw_init_sim( SGUINT dispw, SGUINT disph );
void ghw_exit_sim(void);
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye);
void ghw_set_xy_sim(GXT xb, GYT yb);
void ghw_autowr_sim( GCOLOR cval );
GCOLOR ghw_autord_sim( void );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
#endif
/**********************************************************************/
/** Low level ST7787 interface functions used only by ghw_xxx modules **/
/**********************************************************************/

/* Bit mask values */
GCODE SGUCHAR FCODE sympixmsk[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

typedef struct
   {
   SGUCHAR index;
   SGUCHAR delay;
   SGUCHAR value;
   } S1D_REGS;

/************************ Use command interface *****************/

#define CMD 0
#define DAT 1

static GCODE S1D_REGS FCODE as1dregs[] =
   {
   /*
   The initialization sequence below is suitable for most display modules.

   If another initialization sequence is recommended by a display vendor
   for a specific display module, then you may replace the initialization
   sequence below with the recommended setting.

   It works in this way:
   Each line will send one configuration byte to the display module and
   optionally introduce a delay after the intialization. The first parameter
   selects if the byte should be written to the controllers command register
   or data register address.
      { cmd/data select,  delay (0 = no delay), register value},

   For a detailed explanation of each command and datathe hardware manual for
   the display controller should be consulted.
   */
   {CMD,200,GCTRL_RESET},   /* 0x01 Reset */
   {CMD,150,GCTRL_SLPOUT},  /* 0x11 Sleep out */

   {CMD,  0,GCTRL_PWRCTR1},
   {DAT,  0,0x0},

   {CMD,  0,GCTRL_PWRCTR3},
   {DAT,  0,0x01},
   {DAT,  0,0xd4},
   {DAT,  0,0x85},
   {DAT,  0,0x00},
   {DAT,  0,0x00},

   {CMD,  0,0xf4},  /* Secret command */
   {DAT,  0,0xff},
   {DAT, 200,0x3f},

   {CMD,  0,GCTRL_VCOM_MULTI_MODE},
   {DAT,  0,0x7F},

   {CMD,  0,GCTRL_VMCTR1},
   {DAT,  0,0xC0},
   {DAT,  0,0x1A},

   {CMD,  0,GCTRL_VMCTR2},
   {DAT,  0,0x28},
   {DAT,  0,0x00},

   {CMD,  0,GCTRL_DISSET5},
   {DAT,  0,0x0f},
   {DAT,  0,0x04},

   {CMD,  0,GCTRL_GAMCTRP1},
   {DAT,  0,0x0F},
   {DAT,  0,0x04},
   {DAT,  0,0x1B},
   {DAT,  0,0x1D},
   {DAT,  0,0x1D},
   {DAT,  0,0x1A},
   {DAT,  0,0x18},
   {DAT,  0,0x19},
   {DAT,  0,0x18},
   {DAT,  0,0x0B},
   {DAT,  0,0x0F},
   {DAT,  0,0x07},
   {DAT,  0,0x00},

   {CMD,  0, GCTRL_GAMCTRN1},
   {DAT,  0, 0x08},
   {DAT,  0, 0x02},
   {DAT,  0, 0x19},
   {DAT,  0, 0x1D},
   {DAT,  0, 0x1B},
   {DAT,  0, 0x19},
   {DAT,  0, 0x17},
   {DAT,  0, 0x17},
   {DAT,  0, 0x17},
   {DAT,  0, 0x0B},
   {DAT,  0, 0x07},
   {DAT,  0, 0x0F},
   {DAT,  0, 0x08},

   {CMD,  0, GCTRL_VSYNCOUT},

   #if (GDISPPIXW == 16)
   {CMD,  0, GCTRL_COLMOD},
   {DAT,  0, 0x55},
   #endif

   {CMD,  0, GCTRL_MADCTRL},
   {DAT,  0,(MY_BIT|MX_BIT|MV_BIT|RGB_BIT)},   /* MY,MX,MV,ML,BGR,SS,0,0 */

 /*  {CMD,  0, GCTRL_DISPON}, */
   };



/*
   Send a command
*/
static void ghw_cmd(SGUCHAR cmd)
   {
   #ifndef GHW_NOHDW
   #ifdef GHW_BUS8
   sgwrby(GHWCMD,cmd);    /* Register */
   #elif  defined( GHW_BUS32 )
   sgwrdw(GHWCMDDW,(SGULONG) cmd);    /* Register */
   #else
   sgwrwo(GHWCMDW,(SGUINT) cmd);    /* Register */
   #endif
   #else  /* GHW_NOHDW */
   cmd++; /* silience 'not used' warning */
   #endif
   }


/* Write command data byte */
static void ghw_cmddat(SGUCHAR dat)
   {
   #ifndef GHW_NOHDW
   #ifdef GHW_BUS8
   sgwrby(GHWWR, dat);
   #elif  defined (GHW_BUS32)
   sgwrdw(GHWWRDW,(SGULONG) dat); /* Data */
   #else
   sgwrwo(GHWWRW, (SGUINT) dat);
   #endif
   #endif
   }

/* Make a single data read operation */
static SGUCHAR ghw_rddat( void )
   {
   #ifndef GHW_NOHDW
   #ifdef GHW_BUS8
   return sgrdby(GHWRD);
   #elif  defined (GHW_BUS32)
   return (SGUCHAR) sgrddw(GHWRDDW);
   #else
   return (SGUCHAR)(sgrdwo(GHWRDW));
   #endif
   #else /* GHW_NOHDW */
   return 0;
   #endif
   }

/*
   Set the y range.
   The row position is set to y.
   After last write on row y2 the write position is reset to y
   Internal ghw function
*/
void ghw_set_xyrange(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   #ifdef GHW_PCSIM
   ghw_set_xyrange_sim( xb, yb, xe, ye);
   #endif

   ghw_cmd(GCTRL_CASET);
   ghw_cmddat((SGUCHAR)(((SGUINT) (xb+G_XOFFSET)) >> 8));
   ghw_cmddat((SGUCHAR) (xb+G_XOFFSET));
   ghw_cmddat((SGUCHAR)(((SGUINT) (xe+G_XOFFSET)) >> 8));
   ghw_cmddat((SGUCHAR) (xe+G_XOFFSET));

   ghw_cmd(GCTRL_RASET);
   ghw_cmddat((SGUCHAR)(((SGUINT) (yb+G_YOFFSET)) >> 8));
   ghw_cmddat((SGUCHAR) (yb+G_YOFFSET));
   ghw_cmddat((SGUCHAR)(((SGUINT) (ye+G_YOFFSET)) >> 8));
   ghw_cmddat((SGUCHAR) (ye+G_YOFFSET));

   ghw_cmd(GCTRL_RAMWR); /* Reset address position to range start */
   }

void ghw_setxypos(GXT xb, GYT yb)
   {
   ghw_set_xyrange(xb, yb, GDISPW-1, GDISPH-1);
   }

/*
   Write databyte to controller (at current position) and increment
   internal xadr.

   Internal ghw function
*/
void ghw_auto_wr(GCOLOR dat)
   {
   #ifdef GHW_PCSIM
   ghw_autowr_sim( dat );
   #endif

   #ifndef GHW_NOHDW
   #if ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
      sgwrby(GHWWR, (SGUCHAR )(dat >> 8));
      sgwrby(GHWWR, (SGUCHAR )(dat));
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS8 ))
      /* 6+6+6 + format conversion to 24 bit */
      sgwrby(GHWWR, (SGUCHAR)(dat>>10));    /* MSB */
      sgwrby(GHWWR, (SGUCHAR)(dat>>4));
      sgwrby(GHWWR,((SGUCHAR)(dat))<<2);  /* LSB */
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS8 ))
      /* 6+6+6 + format conversion  */
      sgwrby(GHWWR, (SGUCHAR)(dat>>16)); /* MSB */
      sgwrby(GHWWR, (SGUCHAR)(dat>>8));
      sgwrby(GHWWR, (SGUCHAR)(dat));     /* LSB */
   #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
      /* 16 bit bus mode, 16 bit color */
      sgwrwo(GHWWRW, dat);           /* 16 bit color */
   /*  ((GDISPPIXW == 18) && defined( GHW_BUS16 ))
       not supported for speed reasons as this mode require read-modify-write operations */
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS32 ))
      /* 32 bit bus mode, 18 bit color */
      sgwrdw(GHWWRDW, dat);           /* 18 bit color */
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS32 ))
      /* 32 bit bus mode, 24 bit color */
      /* 24 bit rgb collected to 18 */
      /* dat = generic 24 bit RGB = RRRRRR**GGGGGG**BBBBBB** */
      dat = ((dat & 0xfc0000) >> 6) |
            ((dat & 0x00fc00) >> 4) |
            ((dat & 0x0000fc) >> 2);
      sgwrdw(GHWWRDW, dat);           /* 18 bit color */
   #else
       #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
   #endif
   #endif /* GHW_NOHDW */
   }

/*
   Perform required dummy reads after column position setting
*/
void ghw_auto_rd_start(void)
   {
   ghw_cmd(GCTRL_RAMRD);
   ghw_rddat(); /* Single dummy read operation */
   #ifdef GHW_BUS16
   ghw_oddpos = 0;
   #endif
   }

/*
   Read at address set by previous (write auto-increment) operation
   The address is not incremented
*/
GCOLOR ghw_auto_rd(void)
   {
   #ifndef GHW_NOHDW
      GCOLOR ret;
      #if ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
         /* Data is returned as 6,6,6, Convert to 16 bit RGB(5:6:5) */
         ret  = ((GCOLOR)(sgrdby(GHWRD) & 0xf8))<<8;
         ret |= ((GCOLOR)(sgrdby(GHWRD) & 0xfc))<<3;
         ret |= ((GCOLOR)(sgrdby(GHWRD) & 0xf8))>>3;
      #elif ((GDISPPIXW == 18) && defined( GHW_BUS8 ))
         /* Data is returned as 6,6,6, Convert to 18 bit RGB(6:6:6) */
         ret  = ((GCOLOR)(sgrdby(GHWRD) & 0xfc))<<10;
         ret |= ((GCOLOR)(sgrdby(GHWRD) & 0xfc))<<4;
         ret |= ((GCOLOR)(sgrdby(GHWRD) & 0xfc))>>2;
      #elif ((GDISPPIXW == 24) && defined( GHW_BUS8 ))
         /* Data is returned as 6,6,6, Convert to 24 bit RGB(8:8:8) */
         ret  = ((GCOLOR)sgrdby(GHWRD) & 0xfc)<<16;
         ret |= ((GCOLOR)sgrdby(GHWRD) & 0xfc)<<8;
         ret |= ((GCOLOR)sgrdby(GHWRD) & 0xfc);
      #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
         {
         /* Convert to 16 bit RGB(5:6:5) */
         /* Read as 18 bit odd/even format, independent of mode settings */
         /*  word 0 = RRRRRRxxGGGGGGxx  even pixel msb */
         /*  word 1 = BBBBBBxxRRRRRRxx  even pixel lsb + odd pixel msb */
         /*  word 2 = GGGGGGxxBBBBBBxx  odd pixel lsb */
         static SGUINT dold;
         SGUINT dnew;
         if (ghw_oddpos)
            {
            ghw_oddpos = 0;
            dnew = sgrdwo(GHWRDW);
            ret = (((GCOLOR) (dold & 0x00f8)) << 8) |
                  (((GCOLOR) (dnew & 0xfc00)) >> 5) |
                  (((GCOLOR) (dnew & 0x00f8)) >> 3);
            }
         else
            {
            ghw_oddpos = 1;
            dnew = sgrdwo(GHWRDW);
            dold = sgrdwo(GHWRDW);
            ret =  ((GCOLOR) (dnew & 0xf800)) |
                  (((GCOLOR) (dnew & 0x00fc)) << 3) |
                  (((GCOLOR) (dold & 0xf800)) >> 11);
            }
         }
      #elif ((GDISPPIXW == 18) && defined( GHW_BUS32 ))
         /* 18 bit collective */
         ret =  (GCOLOR) (sgrddw(GHWRDDW) & 0x3ffff);
      #elif ((GDISPPIXW == 24) && defined( GHW_BUS32 ))
         /* 18 bit collective  split to 24 bit rgb*/
         ret =  (GCOLOR) sgrddw(GHWRDDW);
         ret = ((ret << 6) & 0xfc0000) + ((ret << 4) & 0xfc00) + ((ret << 2)&0xfc);
      #else
         #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
      #endif

      return ret;
   #else
     #ifdef GHW_PCSIM
      #if (GDISPPIXW == 24)
      return ghw_autord_sim() & 0xfcfcfc; /* Simulate controller limitations */
      #else
      return ghw_autord_sim();
      #endif
     #else
      return 0;
     #endif
   #endif /* GHW_NOHDW */
   }


/***********************************************************************/
/**        ST7787 Initialization and error handling functions       **/
/***********************************************************************/

/*
   Change default (palette) colors
*/
void ghw_setcolor(GCOLOR fore, GCOLOR back)
   {
   #if (GDISPPIXW == 24)
   /* Mask unused bits */
   fore &= 0xfcfcfc;
   back &= 0xfcfcfc;
   #endif
   /* Update active colors */
   ghw_def_foreground = fore;
   ghw_def_background = back;
   }

/*
   Convert an RGB structure to a color value using the current color mode
*/
GCOLOR ghw_rgb_to_color( GCONSTP GPALETTE_RGB *palette )
   {
   if (palette == NULL)
      return 0;

   return G_RGB_TO_COLOR(palette->r,palette->g,palette->b);
   }


#if (GHW_PALETTE_SIZE > 0)
/*
   Load a new palette or update the existing palette
   (Palette is only used with symbols using 2 or 4 bits pr pixel)
*/
SGBOOL ghw_palette_wr(SGUINT start_index, SGUINT num_elements, GCONSTP GPALETTE_RGB PFCODE *palette)
   {
   if ((num_elements == 0) ||
       ((start_index + num_elements) > 16) ||
       (palette == NULL))
      {
      glcd_err = 1;
      return 1;
      }
   glcd_err = 0;

   /* (Partial) update of operative palette values */
   while(num_elements-- > 0)
      {
      /* Make local palette copy here to be compatible with compilers
         having a non-standard conforming handling of pointer
         (i.e when PFCODE memory qualifer is used) */
      GPALETTE_RGB pal;
      pal.r = palette->r;
      pal.g = palette->g;
      pal.b = palette->b;
      ghw_palette_opr[start_index++] = ghw_rgb_to_color(&pal);
      palette++;

      /* ghw_palette_opr[start_index++] = ghw_rgb_to_color(&palette++); */
      }

   return glcd_err;
   }
#endif

/*
   Fast set or clear of LCD module RAM buffer
   Internal ghw function
*/

/*
   Observed problems with ST7787 read back after full screen write

   If the full screen buffer in ST7787 is filled with white color
   (color value 0xffff for 16 bit mode, or 0x3ffff for 18 bit mode)
   then read of of the screen buffer content will return a grey color
   value instead (color value 0x8410 as 16 bit value, or 0x208020 as 18 bit value)

   The problem is not observed :
      if the color is different from white,
      if the fill area is less than the full screen area

   A fix is to decrement the lsb bit of each r,g,b color part
   when the default background color is defined as white,
   or (as used here) to assure that the full screen is never updated in one step
*/

static void ghw_bufset(GCOLOR color)
   {
   /* Use hardware accelerator logic */
   GBUFINT cnt;
   #ifdef GBUFFER
   cnt = 0;
   do
      {
      /* Clear using X,Y autoincrement */
      gbuf[cnt] = color;      /* Set ram buffer as well */
      }
   while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH)); /* Loop until x+y wrap */
   #endif

   #ifndef GHW_PCSIM
   if (color != G_BLACK) /* hardware buffer is cleared to black by hardware
                           Only nessesary to clear buffer here if another color is used */
   #endif
      {
      /* Clear buffer in two steps to avoid ST7787 read back error*/
      ghw_set_xyrange(0,0,GDISPW-1,GDISPH/2-1);
      cnt = 0;
      do
         {
         /* Clear using X,Y autoincrement */
         ghw_auto_wr(color);      /* Set LCD buffer */
         }
      while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH/2)); /* Loop until x+y wrap */

      ghw_set_xyrange(0,GDISPH/2,GDISPW-1,GDISPH-1);
      cnt = 0;
      do
         {
         /* Clear using X,Y autoincrement */
         ghw_auto_wr(color);      /* Set LCD buffer */
         }
      while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH/2)); /* Loop until x+y wrap */
      ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
      }
   }

#ifdef WR_RD_TEST
/*
   Make write-readback test on controller memory.

   This test returns ok (== 0) when the write-readback test succeded. This indicates that
   the processor / display hardware interface / library configuration combination is
   working ok.

   This test will fail if some databus or control signals is not connected correctly.

   This test will fail if 16/8 bit bus mode selection and color resolution settings
   in the library configuration file does not match the actual bus and color resolution
   configuration for the hardware selected via chip pins. (ex display and processor
   16/8 bit bus width, 8080/6800 bus type settings, word / byte address offsets, etc).

   Note that often a display module vendor have hardwired some chip interface
   configuration signals in the display module. Therefore often only one or a few of
   the library configuration possibilities will be avaiable with a given display module
   hardware.

   This test may fail if illegal GCTRLW, GCTRLH, GHW_XOFFSET, GHW_YOFFSET
   configuration settings cause overrun of the on-chip video RAM.

   This test can be exectuted correctly with only logic power on the display module.

   Return 0 if no error,
   Return != 0 if some readback error is detected (the bit pattern may give information
   about connector pins in error)
*/
/*
void ghw_mem_dump(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   unsigned long i,imax;
   imax = (SGULONG) (xe-xb+1)*(ye-yb+1);

   ghw_set_xyrange(xb,yb,xe,ye);
   ghw_auto_rd_start();
   for (i = 0; i < imax; i++)
      {
      GCOLOR val;
      if (i%8 == 0)
         printf("\n");
      val = ghw_auto_rd();
       printf("0x%04x ", (unsigned int) val);
      }
   }
*/

static GCOLOR ghw_wr_rd_test(void)
   {
   #ifndef GHW_NOHDW
   int i;
   GCOLOR msk,result;

   /*printf("\n"); */
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);

   #if (GDISPPIXW > 16)
   /* 24 (18) bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%06lx ", (unsigned long) msk); */
      ghw_auto_wr(~msk);
      /*printf(" 0x%06lx\n", (unsigned long) (~msk)); */
      msk <<= 1;
      }
   /*printf("\n"); */

   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   ghw_auto_rd_start();
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val1,val2;
      val1 = ghw_auto_rd();
      val2 = ghw_auto_rd();
      result |= (val1 ^ msk);
      result |= (val2 ^ (~msk));
      /*printf("0x%06lx ",  (unsigned long) val1); */
      /*printf(" 0x%06lx\n", (unsigned long) val2); */
      msk <<= 1;
      }

   result &= GHW_COLOR_CMP_MSK;  /* Mask bits unused by controller */

   #else
   /* 16 bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%04x ", (unsigned short) msk); */
      ghw_auto_wr(~msk);
      /*printf(" 0x%04x\n", (unsigned short) (~msk & 0xffff)); */
      msk <<= 1;
      }
   /* printf("\n"); */
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   ghw_auto_rd_start();
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val;
      val = ghw_auto_rd();
      result |= (val ^ msk);
      /*printf("0x%04x ",  (unsigned short) val); */
      val = ghw_auto_rd();
      /*printf(" 0x%04x\n", (unsigned short) val ); */
      result |= (val ^ (~msk));
      msk <<= 1;
      }
   #endif
   return result;  /* 0 = Nul errors */
   #else
   return 0; /* 0 = Nul errors */
   #endif
   }

#endif /* WR_RD_TEST */

/*
   Wait a number of milli seconds
*/
static void ghw_cmd_wait(SGUCHAR ms)
   {
   #ifdef SGPCMODE
   Sleep(ms); /* delay x1 ms */
   #else
   SGUINT wait1ms;
   while (ms-- != 0)
      {
      wait1ms = 2000;     /* Adjust to achieve a 1 ms loop below */
      while( wait1ms != 0)
         wait1ms--;
      }
   #endif
   }

/*
   Initialize display, clear ram  (low-level)
   Clears glcd_err status before init

   Return 0 if no error,
   Return != 0 if some error
*/
SGBOOL ghw_init(void)
   {
   short i;

   #ifdef GBUFFER
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;
   ghw_upddelay = 0;
   #endif

   glcd_err = 0;
   ghw_io_init(); /* Set any hardware interface lines, controller hardware reset */

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL)
      {
      /* Allocate graphic ram buffer */
      if ((gbuf = (GCOLOR *)calloc(ghw_gbufsize(),1)) == NULL)
         glcd_err = 1;
      else
         gbuf_owner = 1;
      }
   #endif

   if (glcd_err != 0)
      return 1;

   #ifdef GHW_PCSIM
   /* Tell simulator about the visual LCD screen organization */
   ghw_init_sim( GDISPW, GDISPH );
   #endif
   /* Set default colors */
   ghw_setcolor( GHW_PALETTE_FOREGROUND, GHW_PALETTE_BACKGROUND );

   #if (GHW_PALETTE_SIZE > 0)
   /* Load palette */
   ghw_palette_wr(0, sizeof(ghw_palette)/sizeof(GPALETTE_RGB), (GCONSTP GPALETTE_RGB PFCODE *)&ghw_palette[0]);
   #endif

   /*
      Internal hardware-clear of video memory RAM takes approx 100 ms
      from hardware reset is released.
      This memory clearing to black is aborted by sending the initialization
      commands.

      If a delay after reset is already introdued by other parts of the
      system you may reduce the wait time parameter in the call below or
      comment out the call

      The delay is not needed if the default background color is different
      from black. A software clear of video memory is then used instead.
   */
   #if (GHW_PALETTE_BACKGROUND == G_BLACK)
   ghw_cmd_wait(100); /* Wait for hardware clear of video memory to black to complete */
   #endif

   /* Initialize controller according to configuration file */
   for (i=0; i < sizeof(as1dregs)/sizeof(S1D_REGS); i++)
      {
      if (as1dregs[i].index == CMD)
         ghw_cmd(as1dregs[i].value);
      else
         ghw_cmddat(as1dregs[i].value);
      if (as1dregs[i].delay != 0)
         ghw_cmd_wait( as1dregs[i].delay );
      }

   /*
      Stimuli test loops for initial oscilloscope test of display interface bus signals
      Uncomment to use the test loop for the given data bus width.
      It is recommended to check all display bus signals with each of the I/O access
      statements in the loop one by one.
   */
   /*
   #ifdef GHW_BUS8
   for(;;)
      {
      SGUCHAR dat;
      sgwrby(GHWCMD,0xff);
      sgwrby(GHWWR,0x00);
      dat = sgrdby(GHWSTA);
      dat = sgrdby(GHWRD);
      }
   #endif
   #ifdef GHW_BUS16
   for(;;)
      {
      SGUINT dat;
      sgwrwo(GHWCMDW,0xffff);
      sgwrwo(GHWWRW,0x0000);
      dat = sgrdwo(GHWSTAW);
      dat = sgrdwo(GHWRDW);
      }
   #ifdef GHW_BUS32
   for(;;)
      {
      SGULONG dat;
      sgwrdw(GHWCMDDW,0x3ffff);
      sgwrdw(GHWWRDW,0x0000);
      dat = sgrddw(GHWSTADW);
      dat = sgrddw(GHWRDDW);
      }
   #endif
   */


   #ifdef WR_RD_TEST
   /*
      NOTE:
      The call of ghw_wr_rd_test() should be commented out in serial mode.
      In serial mode the display controller  does not provide read-back facility
      and this test will always fail.
   */
   if (ghw_wr_rd_test() != ((GCOLOR) 0))
      {
      /* Controller memory write-readback error detected
      (Check the cable or power connections to the display) */
      G_WARNING("Hardware interface error\nCheck display connections\n");  /* Test Warning message output */
      glcd_err = 1;
      return 1;
      }
   #endif

   ghw_bufset( ghw_def_background );
   ghw_dispon();

   #ifndef GNOCURSOR
   ghw_cursor = GCURSIZE1;    /* Cursor is off initially */
   /* ghw_cursor = GCURSIZE1 | GCURON; */ /* Uncomment to set cursor on initially */
   #endif

   ghw_updatehw();  /* Flush to display hdw or simulator */


   return (glcd_err != 0) ? 1 : 0;
   }

/*
   Return last error state. Called from applications to
   check for LCD HW or internal errors.
   The error state is reset by ghw_init and all high_level
   LCD functions.

   Return == 0 : No errors
   Return != 0 : Some errors
*/
SGUCHAR ghw_err(void)
   {
   #if (defined(_WIN32) && defined( GHW_PCSIM))
   if (GSimError())
      return 1;
   #endif
   return (glcd_err == 0) ? 0 : 1;
   }


/*
   Display a (fatal) error message.
   The LCD display module is always cleared and initialized to
   the system font in advance.
   The error message is automatically centered on the screen
   and any \n characters in the string is processed.

   str = ASCII string to write at display center
*/
void ghw_puterr( PGCSTR str )
   {
   PGCSTR idx;
   SGUINT xcnt;
   GXT xp;
   GYT yp,h,y, sidx;
   PGSYMBYTE psym;
   GCOLOR pval;
   SGUCHAR val;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   #endif

   if (ghw_init() != 0)  /* (Re-) initialize display */
      return;            /* Some initialization error */

   /* Count number of lines in string */
   idx=str;
   if (idx == NULL)
      return;
   xcnt = 1;
   while(*idx)
      {
      if (*(idx++) == '\n')
         xcnt++;
      }

   /* Set start character line */
   h = SYSFONT.symheight;
   yp = (xcnt*h > GDISPH) ? 0 : ((GDISPH-1)-xcnt*h)/2;
   /* Set character height in pixel lines */

   idx=str;
   do
      {
      xcnt=0;  /* Set start x position so line is centered */
      while ((idx[xcnt]!=0) && (idx[xcnt]!='\n') && (xcnt < GDISPBW))
         {
         xcnt++;
         }

      /* Calculate start position for centered line */
      xp = (GDISPW-xcnt*SYSFONT.symwidth)/2;

      /* Display text line */
      while (xcnt-- > 0)
         {
         /* Point to graphic content for character symbol */
         psym = &(sysfontsym[(*idx) & 0x7f].b[0]);
         ghw_set_xyrange(xp,yp,xp+SYSFONT.symwidth-1,yp+(h-1));

         /* Display rows in symbol */
         for (y = 0; y < h; y++)
            {
            /* Get symbol row value */
            val = *psym++;
            /* Initiate LCD controller address pointer */
            #ifdef GBUFFER
            gbufidx = GINDEX(xp, (GBUFINT)yp+y );
            #endif

            /* Display colums in symbol row */
            for (sidx = 0; sidx < SYSFONT.symwidth; sidx++)
               {
               if ((val & sympixmsk[sidx]) != 0)
                  pval = ghw_def_foreground;
               else
                  pval = ghw_def_background;

               /* End of symbol or end of byte reached */
               #ifdef GBUFFER
               gbuf[gbufidx++] = pval;
               #endif
               ghw_auto_wr(pval);
               }
            }

         idx++;
         xp += SYSFONT.symwidth; /* Move to next symbol in line */
         }

      /* Next text line */
      yp += h;
      if (*idx == '\n')
         idx++;
      }
   while ((*idx != 0) && (yp < GDISPH));

   ghw_updatehw();  /* Flush to display hdw or simulator */
   }

void ghw_exit(void)
   {
   #if defined( GHW_ALLOCATE_BUF)
   if (gbuf != NULL)
      {
      if (gbuf_owner != 0)
         {
         /* Buffer is allocated by ginit, so release graphic buffer here */
         free(gbuf);
         gbuf_owner = 0;
         }
      gbuf = NULL;
      }
   #endif
   ghw_io_exit();         /* Release any LCD hardware resources, if required */
   #ifdef GHW_PCSIM
   ghw_exit_sim(); /* Release simulator resources */
   #endif
   }

#ifndef GNOCURSOR
/*
   Replace cursor type data (there is no HW cursor support in ST7787)
*/
void ghw_setcursor( GCURSOR type)
   {
   ghw_cursor = type;
   #ifdef GHW_ALLOCATE_BUF
   if (gbuf == NULL)
      glcd_err = 1;
   #endif
   }
#endif


/*
   Turn display off
*/
void ghw_dispoff(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif

   ghw_cmd(GCTRL_DISPOFF);
   }

/*
   Turn display on (to be used after a ghw_pwroff() call)
*/
void ghw_dispon(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif

   ghw_cmd(GCTRL_DISPON);
   }

#if defined( GHW_ALLOCATE_BUF)
/*
   Size of buffer requied to save the whole screen state
*/
GBUFINT ghw_gbufsize( void )
   {
   return (GBUFINT) GBUFSIZE * sizeof(GCOLOR) + (GBUFINT) sizeof(GHW_STATE);
   }

#ifdef GSCREENS
/*
   Check if screen buf owns the screen ressources.
*/
SGUCHAR ghw_is_owner( SGUCHAR *buf )
   {
   return (((GCOLOR *)buf == gbuf) && (gbuf != NULL)) ? 1 : 0;
   }

/*
   Save the current state to the screen buffer
*/
SGUCHAR *ghw_save_state( SGUCHAR *buf )
   {
   GHW_STATE *ps;
   if (!ghw_is_owner(buf))
      return NULL;

   ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);
   ps->upddelay = (ghw_upddelay != 0);
   #ifndef GNOCURSOR
   ps->cursor = ghw_cursor;
   #endif
   ps->foreground = ghw_def_foreground; /* Palette may vary, save it */
   ps->background = ghw_def_background;
   return (SGUCHAR *) gbuf;
   }

/*
   Set state to buf.
   If buffer has not been initiated by to a screen before, only
   the pointer is updated. Otherwise the the buffer
*/
void ghw_set_state(SGUCHAR *buf, SGUCHAR doinit)
   {
   if (gbuf != NULL)
      {
      /* The LCD controller has been initiated before */
      if (gbuf_owner != 0)
         {
         /* Buffer was allocated by ginit, free it so screen can be used instead*/
         free(gbuf);
         gbuf_owner = 0;
         gbuf = NULL;
         }
      }

   if (doinit != 0)
      {
      /* First screen initialization, just set buffer pointer and
         leave rest of initialization to a later call of ghw_init() */
      gbuf = (GCOLOR *) buf;
      gbuf_owner = 0;
      }
   else
      {
      if ((gbuf = (GCOLOR *) buf) != NULL)
         {
         GHW_STATE *ps;
         ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);

         #ifndef GNOCURSOR
         ghw_cursor = ps->cursor;
         #endif
         ghw_upddelay = 0;        /* Force update of whole screen */
         iltx = 0;
         ilty = 0;
         irbx = GDISPW-1;
         irby = GDISPH-1;
         ghw_updatehw();
         ghw_upddelay = (ps->upddelay != 0) ? 1 : 0;
         /* Restore drawing color */
         ghw_setcolor(ps->foreground, ps->background);
         }
      }
   }
#endif  /* GSCREENS */
#endif  /* GHW_ALLOCATE_BUF */

#endif /* GBASIC_INIT_ERR */

