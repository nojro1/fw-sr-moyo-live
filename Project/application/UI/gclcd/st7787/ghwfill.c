/************************** ghwfill.c *****************************

   Fill box area with a pattern.

   The box area may have any pixel boundary, however the pattern is
   always aligned to the physical background, which makes patching
   of the background easier with when using multiple partial fills.

   The pattern word is used as a 2 character pattern.
   The LSB byte of pattern are used on even pixel lines and the MSB byte
   are used on odd pixel lines making it easy to make a homogene two color
   bit raster (for instance when pat = 0x55aa or = 0xaa55)

   ---------

   The hx8312 controller is assumed to be used with a LCD module.

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is compied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)


   Revision date:
   Revision Purpose:

   Version number: 1.00
   Copyright (c) RAMTEX Engineering Aps 2007

*********************************************************************/
#include <gdisphw.h>   /* HW driver prototypes and types */
#include <s6d0129.h>   /* Controller specific definements */

#ifdef GBASIC_TEXT

#if ( defined( GHW_ACCELERATOR ) && defined (GBUFFER) )
extern SGBOOL ghw_upddelay;
#endif

static void ghw_fill2(GXT ltx, GYT lty, GXT rbx, GYT rby, SGUINT pattern)
   {
   GYT y;
   GXT x;
   SGUCHAR msk,pat;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   GBUF_CHECK();
   #endif

   glcd_err = 0;

   /* Force reasonable values */
   GLIMITU(ltx,GDISPW-1);
   GLIMITU(lty,GDISPH-1);
   GLIMITD(rby,lty);
   GLIMITU(rby,GDISPH-1);
   GLIMITD(rbx,ltx);
   GLIMITU(rbx,GDISPW-1);

   #ifdef GBUFFER
   invalrect( ltx, lty );
   invalrect( rbx, rby );
   #else
   ghw_set_xyrange(ltx,lty,rbx,rby);
   #endif

   for (y = lty; y <= rby; y++)
      {
      #ifdef GBUFFER
      gbufidx = GINDEX(ltx,y);
      #endif
      msk = sympixmsk[GPIXEL(ltx)];
      pat = ((y & 1) != 0) ? pattern / 256 : pattern & 0xff;

      /* loop fill of full pixels */
      for (x = ltx; x <= rbx; x++ )
         {
         #ifdef GBUFFER
         gbuf[gbufidx++] = (pat & msk) ? ghw_def_foreground : ghw_def_background;;
         #else
         ghw_auto_wr(((pat & msk) ? ghw_def_foreground : ghw_def_background));
         #endif
         if ((msk >>= 1) == 0)
            msk = sympixmsk[GPIXEL(ltx)];
         }
      }
   }

void ghw_fill(GXT ltx, GYT lty, GXT rbx, GYT rby, SGUINT pattern)
   {
   /* Handle work around of hardware bug in the ST7787 chip */
   if ((ltx == 0) && (lty == 0) && (rbx == GDISPW-1) && (rby == GDISPH-1))
      {
      /*
         Observed problems with ST7787 read values after full screen write

         If the full screen buffer in ST7787 is filled with white color
         (color value 0xffff for 16 bit mode, or 0x3ffff for 18 bit mode)
         then read of of the screen buffer content will return a grey color
         value instead (color value 0x8410 as 16 bit value, or 0x208020 as 18 bit value)
         (This may give problems in connection ex with scroll or write back of
         the background under a popup-window)

         The problem is not observed :
            if the color is different from white, red, green, blue
            if the fill area is less than the full screen area

         A fix is to decrement the lsb bit of each r,g,b color part
         when the default background color is defined as white,
         or (as used here) to assure that the full screen is never
         updated in one step
      */

      /* Do fill in two steps */
      ghw_fill2(ltx, lty, rbx, rby/2-1, pattern);
      ghw_fill2(ltx, rby/2, rbx, rby, pattern);
      }
   else
      ghw_fill2(ltx, lty, rbx, rby, pattern);
   }

#endif /* GBASIC_TEXT */

