/************************** ghwinit.c *****************************

   Low-level driver functions for the hx8312 LCD display controller
   initialization and error handling.

   The hx8312 controller is assumed to be used with a LCD module.

   Notes: Data read require a dummy read before first read

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is copied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)


   Revision date:       03-09-07
   Revision Purpose:    Generalized interface to S6D0129 lowlevel driver.
   Revision date:       07-05-09
   Revision Purpose:    The symbol software palette (data and functions) can
                        be optimized away if not used by defining
                        GHW_PALETTE_SIZE as 0 in gdispcfg.h

   Revision date:    11-11-10
   Revision Purpose: ghw_rgb_to_color(..) updated to use G_RGB_TO_COLOR(r,g,b) macro.

   Version number: 1.3
   Copyright (c) RAMTEX Engineering Aps 2007-2010

*********************************************************************/

#ifdef SGPCMODE
#include "windows.h"  /* Sleep(..) function used by ghw_cmd_wait(..)*/
#endif

#include <gdisphw.h>  /* HW driver prototypes and types */
#include <s6d0129.h>  /* Controller specific definements */

/*#define WR_RD_TEST*/   /* Define to include write-read-back test in ghw_init() */

#ifndef GHW_HX8312
  #error Unknown controller, Controller and bustype must be selected in gdispcfg.h
#else
#define  GCTRL_REG1             0x00  /* 0   */
#define  GCTRL_REG2             0x01  /* 1   */
#define  GCTRL_RGB_INTF         0x02  /* 2   */
#define  GCTRL_RESET            0x03  /* 3   */
#define  GCTRL_RAM_ACCESS_CTL   0x05  /* 5   */
#define  GCTRL_DATA_REV         0x06  /* 6   */
#define  GCTRL_DISP_XSIZE       0x0d  /* 13  */
#define  GCTRL_DISP_NO_COLOR    0x0e  /* 14  */
#define  GCTRL_DISP_COLOR_8     0x0f  /* 15  */
#define  GCTRL_SCR1_STRT_H      0x10  /* 16  */
#define  GCTRL_SCR1_STRT_L      0x11  /* 17  */
#define  GCTRL_SCR2_STRT_H      0x12  /* 18  */
#define  GCTRL_SCR2_STRT_L      0x13  /* 19  */
#define  GCTRL_SCR1_END_H       0x14  /* 20  */
#define  GCTRL_SCR1_END_L       0x15  /* 21  */
#define  GCTRL_SCR2_END_H       0x16  /* 22  */
#define  GCTRL_SCR2_END_L       0x17  /* 23  */
#define  GCTRL_PWR_CTRL1        0x18  /* 24  */
#define  GCTRL_PWR_CTRL2        0x19  /* 25  */
#define  GCTRL_PWR_CTRL3        0x1a  /* 26  */
#define  GCTRL_PWR_CTRL4        0x1b  /* 27  */
#define  GCTRL_PWR_CTRL5        0x1c  /* 28  */
#define  GCTRL_PWR_CTRL6        0x1d  /* 29  */
#define  GCTRL_PWR_CTRL8        0x1e  /* 30  */
#define  GCTRL_PWR_CTRL9        0x1f  /* 31  */
#define  GCTRL_PWR_CTRL10       0x20  /* 32  */
#define  GCTRL_PWR_CTRL11       0x24  /* 36  */
#define  GCTRL_PWR_CTRL12       0x25  /* 37  */
#define  GCTRL_PWR_CTRL14       0x28  /* 40  */
#define  GCTRL_OSC_CTRL         0x2b  /* 43  */
#define  GCTRL_CALIB_REG        0x2d  /* 45  */
#define  GCTRL_ID_REG1          0x31  /* 49  */
#define  GCTRL_ID_REG2          0x32  /* 50  */ /* status check */
#define  GCTRL_NLINE_INV        0x33  /* 51  */
#define  GCTRL_PART_GATE1       0x34  /* 52  */
#define  GCTRL_PART_GATE2       0x35  /* 53  */
#define  GCTRL_GATE_SCAN        0x37  /* 55  */
#define  GCTRL_GATE_CTRL        0x3b  /* 59  */
#define  GCTRL_RGB_STRT_H       0x3c  /* 60  */
#define  GCTRL_RGB_STRT_L       0x3d  /* 61  */
#define  GCTRL_RGB_END_H        0x3e  /* 62  */
#define  GCTRL_RGB_END_L        0x3f  /* 63  */
#define  GCTRL_HOR_BACK_PORCH   0x40  /* 64  */
#define  GCTRL_VER_BACK_PORCH   0x41  /* 65  */
#define  GCTRL_RAM_ADR_X        0x42  /* 66  */ /* Set GRAM address */
#define  GCTRL_RAM_ADR_YH       0x43  /* 67  */ /* Set GRAM address */
#define  GCTRL_RAM_ADR_YL       0x44  /* 68  */ /* Set GRAM address */
#define  GCTRL_H_WIN_ADR_START  0x45  /* 69  */ /* begin */
#define  GCTRL_H_WIN_ADR_END    0x46  /* 70  */ /* end */
#define  GCTRL_V_WIN_ADR_STRT_H 0x47  /* 71  */ /* begin */
#define  GCTRL_V_WIN_ADR_STRT_L 0x48  /* 72  */ /* begin */
#define  GCTRL_V_WIN_ADR_END_H  0x49  /* 73  */ /* end */
#define  GCTRL_V_WIN_ADR_END_L  0x4a  /* 74  */ /* end */
#define  GCTRL_SCR_DRV_STRT_H   0x4b  /* 75  */ /* Scroll area start */
#define  GCTRL_SCR_DRV_STRT_L   0x4c  /* 76  */ /*  */
#define  GCTRL_SCR_DRV_LINE_H   0x4d  /* 77  */ /* Scroll line no */
#define  GCTRL_SCR_DRV_LINE_L   0x4e  /* 78  */ /*  */
#define  GCTRL_SCR_DRV_STEP_H   0x4f  /* 79  */ /* Scroll step no */
#define  GCTRL_SCR_DRV_STEP_L   0x50  /* 80  */ /*  */
#define  GCTRL_DUMMY_LINES      0x76  /* 118 */ /*  */
#define  GCTRL_GATE_ON_H        0x86  /* 134 */
#define  GCTRL_GATE_ON_L        0x87  /* 135 */
#define  GCTRL_SOURCE_ON_INTV   0x88  /* 136 */
#define  GCTRL_GATE_ON_INTV     0x89  /* 137 */
#define  GCTRL_CLOCK_REG_1      0x8b  /* 139 */
#define  GCTRL_CLOCK_REG_2      0x8c  /* 140 */
#define  GCTRL_LINE_FREQ_REG    0x8d  /* 141 */

#define  GCTRL_GAMMA_1          0x8f  /* 143 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_2          0x90  /* 144 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_3          0x91  /* 145 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_4          0x92  /* 146 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_5          0x93  /* 147 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_6          0x94  /* 148 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_7          0x95  /* 149 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_8          0x96  /* 150 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_9          0x97  /* 151 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_10         0x98  /* 152 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_11         0x99  /* 153 */ /* Set gamma ctrl */
#define  GCTRL_GAMMA_12         0x9a  /* 154 */ /* Set gamma ctrl */

#define  GCTRL_EXTMODE          0x9d  /* 157 */
#define  GCTRL_OFFMODE          0xc0  /* 192 */
#define  GCTRL_RGBMODE          0xc1  /* 193 */

#endif


/*
   Define pixel width, height of internal video memory ( only used for the overflow check below)
   Note: If another display controller variant is used the adjust the GCTRLW, GCTRLH definitions
         below accordingly to match the size of the pixel video RAM in the controller.
   Note: If the physical memory range limits are exceeded at runtime then some controllers stop working.
*/
   /* HX8312 (240x320) (208x320)  (180x320) */

#define GCTRLH 320

/* Set size of active internal RAM, Check display size settings */
#ifdef GHW_ROTATED
  #if ((GDISPH+GHW_YOFFSET) <= 180)
    #define GCTRLW 180
  #elif ((GDISPH+GHW_YOFFSET) <= 208)
    #define GCTRLW 208
  #else
    #define GCTRLW 240
  #endif
  #if (((GDISPH+GHW_YOFFSET) > GCTRLW) || ((GDISPW+GHW_YOFFSET) > GCTRLH))
    #error (GDISPW, GDISPH, GHW_XOFFSET, GHW_YOFFSET, GHW_ROTATED configuration exceeds controller memory limits)
  #endif
#else
  #if ((GDISPW+GHW_XOFFSET) <= 180)
    #define GCTRLW 180
  #elif  ((GDISPW+GHW_XOFFSET) <= 208)
    #define GCTRLW 208
  #else
    #define GCTRLW 240
  #endif
  #if (((GDISPW+GHW_XOFFSET) > GCTRLW) || ((GDISPH+GHW_YOFFSET) > GCTRLH))
    #error (GDISPW, GDISPH, GHW_XOFFSET, GHW_YOFFSET, GHW_ROTATED configuration exceeds controller memory limits)
  #endif
#endif

#if (defined( GHW_BUS32 ) && (GDISPPIXW <= 16))
  #error "Illegal color / bus width combination in gdispcfg.h"
#endif

/********************* Chip access definitions *********************/

#ifndef GHW_NOHDW
   #if defined( GHW_SINGLE_CHIP)
      /* User defined access types and simulated register address def */
      #include <bussim.h>
      #ifdef GHW_BUS8
        #define  sgwrby(a,d) simwrby((a),(d))
        #define  sgrdby(a)   simrdby((a))
      #elif GHW_BUS32
        #define  sgwrdw(a,d) simwrdw((a),(d))
        #define  sgrddw(a)   simrddw((a))
      #else
        #define  sgwrwo(a,d) simwrwo((a),(d))
        #define  sgrdwo(a)   simrdwo((a))
      #endif
   #else
      /* Portable I/O functions + hardware port def */
      #include <sgio.h>
   #endif
#else
   #undef GHW_SINGLE_CHIP /* Ignore single chip mode */
#endif

/* Fix missing definitions in gdispcfg.h */
#ifndef GHW_XOFFSET
   #define GHW_XOFFSET 0
#endif
#ifndef GHW_YOFFSET
   #define GHW_YOFFSET 0
#endif

/***********************************************************************/
/** All static LCD driver data is located here in this ghwinit module **/
/***********************************************************************/

#ifdef GBASIC_INIT_ERR

/* Active foreground and background color */
GCOLOR ghw_def_foreground;
GCOLOR ghw_def_background;

#if (GHW_PALETTE_SIZE > 0)
/* Default grey scale palette
   The palette file can be edited directly with the ColorIconEdit program
*/
static GCODE GPALETTE_RGB FCODE ghw_palette[16] =
     #include <gcolor_4.pal>
     ;

/* Operative palette (current palette used for color lookup) */
GCOLOR ghw_palette_opr[16];
#endif

/* Use software font */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR  b[8];      /* Symbol data, fixed size = 8 bytes */
   }
GCODE FCODE sysfontsym[0x80] =
   {
   /* The default font MUST be a monospaced black & white (two-color) font */
   #include <sfs0129.sym> /* System font symbol table */
   };

/* Default system font */
GCODE GFONT FCODE SYSFONT =
   {
   6,      /* width */
   8,      /* height */
   sizeof(sysfontsym[0])-sizeof(GSYMHEAD), /* number of data bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL) sysfontsym,  /* pointer to array of SYMBOLS */
   0x80,   /* num symbols in sysfontsym[] */
   NULL    /* pointer to code page */ /* NULL means code page is not used */
   };

#ifdef GBUFFER
   #ifdef GHW_ALLOCATE_BUF
      /* <stdlib.h> is included via gdisphw.h */
      GCOLOR *gbuf = NULL;           /* Graphic buffer pointer */
      static SGBOOL gbuf_owner = 0;   /* Identify pointer ownership */
   #else
      GCOLOR gbuf[GBUFSIZE];         /* Graphic buffer */
   #endif
   GXT GFAST iltx,irbx;     /* "Dirty area" speed optimizers in buffered mode */
   GYT GFAST ilty,irby;
   SGBOOL  ghw_upddelay;    /* Flag for delayed update */
#else
   GCOLOR ghw_tmpbuf[GDISPW]; /* Row line buffer (for block read-modify-write) */
#endif /* GBUFFER */


#ifdef GHW_INTERNAL_CONTRAST
static SGUCHAR ghw_contrast;/* Current contrast value */
#endif

SGBOOL glcd_err;            /* Internal error */
#ifndef GNOCURSOR
GCURSOR ghw_cursor;         /* Current cursor state */
#endif

#ifdef GHW_PCSIM
/* PC simulator declaration */
void ghw_init_sim( SGUINT dispw, SGUINT disph );
void ghw_exit_sim(void);
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye);
void ghw_set_xy_sim(GXT xb, GYT yb);
void ghw_autowr_sim( GCOLOR cval );
GCOLOR ghw_autord_sim( void );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
#endif
/**********************************************************************/
/** Low level hx8312 interface functions used only by ghw_xxx modules **/
/**********************************************************************/

/* Bit mask values */
GCODE SGUCHAR FCODE sympixmsk[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

typedef struct
   {
   SGUCHAR index;
   SGUCHAR delay;
   SGUCHAR value;
   } S1D_REGS;

/* Array of configuration descriptors, the registers are initialized in the order given in the table */
static GCODE S1D_REGS FCODE as1dregs[] =
   {
   {GCTRL_RESET,            10, 0x01},  /* 3  Reset on*/
   {GCTRL_RESET,             0, 0x00},  /* 3  Reset off */
   {GCTRL_REG1,              0, 0xa0},  /* 0  Standby mode cancel */
   #if (( defined( GHW_ROTATED ) && !defined( GHW_MIRROR_HOR )) || \
        (!defined( GHW_ROTATED ) && !defined( GHW_MIRROR_VER)))
   {GCTRL_REG2,             10, 0x10},  /* 1  start osc , set mirror x=0,y=0, enable gamma adjustment */
   #else
   {GCTRL_REG2,             10, 0x50},  /* 1  start osc , set mirror x=0,y=0, enable gamma adjustment */
   #endif

   #ifdef GHW_ROTATED
   {GCTRL_RAM_ACCESS_CTL,    0, 0x14 }, /* 5  0x10 = WAS = 1, 0x4 = AM = 1 -> inc y first */
   #else
   {GCTRL_RAM_ACCESS_CTL,    0, 0x10 }, /* 5  0x10 = WAS = 1, 0x4 = AM = 0 -> inc x first */
   #endif
   {GCTRL_RGB_INTF,          0, 0x0 },  /* 2 D0=0 -> writes to data RAM via system interface */
   {GCTRL_DATA_REV,          0, 0x0 },  /* 6   */

   /* Set active display memory size */
   #if (GCTRLW <= 180)
   {GCTRL_DISP_XSIZE,        0, 0x04 },  /* 13 max X size of display = 180 */
   #elif (GCTRLW <= 208)
   {GCTRL_DISP_XSIZE,        0, 0x02 },  /* 13 max X size of display = 208 */
   #else
   {GCTRL_DISP_XSIZE,        0, 0x00 },  /* 13 max X size of display = 240 */
   #endif

   #ifdef GHW_COLOR_SWAP
   {GCTRL_RGBMODE,           0, 0x01},  /* 193 RGB color swap */
   #else
   {GCTRL_RGBMODE,           0, 0x00},  /* 193 RGB color swap */
   #endif
   {GCTRL_OSC_CTRL,          0, 0x0a},  /* 43  */
   {GCTRL_EXTMODE,           0, 0x01},  /* 157 MSBF = 1 -> Use upper 6 bit in color byte */
   {GCTRL_PWR_CTRL6,         0, 0x08},  /* 29, Gate scan mode */
   {GCTRL_GATE_ON_H,         0, 0x00},  /* 134 */
   {GCTRL_GATE_ON_L,         0, 0x39},  /* 135 */
   {GCTRL_SOURCE_ON_INTV,    0, 0x02},  /* 136 default */
   {GCTRL_GATE_ON_INTV,      0, 0x05},  /* 137 */
   {GCTRL_LINE_FREQ_REG,     0, 0x01},  /* 141 */
   {GCTRL_CLOCK_REG_1,       0, 0x30},  /* 139 */
   {GCTRL_NLINE_INV,         0, 0x01},  /*  51 */
   {GCTRL_GATE_SCAN,         0, 0x01},  /*  55 */
   {GCTRL_DUMMY_LINES,       0, 0x00},  /* 118 */
   {GCTRL_DISP_NO_COLOR,     0, 0x00},  /*  14 */
   {GCTRL_DISP_COLOR_8,      0, 0x00},  /*  15 */
   {GCTRL_SCR1_STRT_H,       0, 0x00},  /*  16 */
   {GCTRL_SCR1_STRT_L,       0, 0x00},  /*  17 */
   {GCTRL_SCR2_STRT_H,       0, 0x00},  /*  18 */
   {GCTRL_SCR2_STRT_L,       0, 0x00},  /*  19 */
   {GCTRL_SCR1_END_H,        0, 0x00},  /*  20 */
   {GCTRL_SCR1_END_L,        0, 0x00},  /*  21 */
   {GCTRL_SCR2_END_H,        0, 0x00},  /*  22 */
   {GCTRL_SCR2_END_L,        0, 0x00},  /*  23 */
   {GCTRL_PART_GATE1,        0, 0x01},  /*  52 */
   {GCTRL_PART_GATE2,        0, 0x00},  /*  53 */
   {GCTRL_SCR_DRV_STRT_H,    0, 0x00},  /*  75 */
   {GCTRL_SCR_DRV_STRT_L,    0, 0x00},  /*  76 */
   {GCTRL_SCR_DRV_LINE_H,    0, 0x00},  /*  77 */
   {GCTRL_SCR_DRV_LINE_L,    0, 0x00},  /*  78 */
   {GCTRL_SCR_DRV_STEP_H,    0, 0x00},  /*  79 */
   {GCTRL_SCR_DRV_STEP_L,    0, 0x00},  /*  80 */

   { GCTRL_RAM_ADR_X,        0, 0x00},  /*  66 x address */
   { GCTRL_RAM_ADR_YH,       0, 0x00},  /*  67 y address h */
   { GCTRL_RAM_ADR_YL,       0, 0x00},  /*  68 y address l */

   { GCTRL_H_WIN_ADR_START,  0, 0x00},  /*  69 min xadr */
   { GCTRL_V_WIN_ADR_STRT_H, 0, 0x00},  /* 71 min yadr h*/
   { GCTRL_V_WIN_ADR_STRT_L, 0, 0x00},  /* 72 min yadr l*/
   { GCTRL_RGB_STRT_H,       0, 0x00},  /* 60 rgb start linie h */
   { GCTRL_RGB_STRT_L,       0, 0x00},  /* 61 rgb start linie l */

   #ifdef GHW_ROTATED
   { GCTRL_H_WIN_ADR_END,    0, (GDISPH-1)},      /* 70 max eadr*/
   { GCTRL_V_WIN_ADR_END_H,  0, (GDISPW-1)/256},  /* 73 max yadr h */
   { GCTRL_V_WIN_ADR_END_L,  0, (GDISPW-1)&0xff}, /* 74 mac yadr h */
   { GCTRL_RGB_END_H,        0, (GDISPW-1)/256},  /* 62 rgb end line h*/
   { GCTRL_RGB_END_L,        0, (GDISPW-1)&0xff}, /* 63 rgb end line l */
   #else
   { GCTRL_H_WIN_ADR_END,    0, (GDISPW-1)},      /* 70 max eadr*/
   { GCTRL_V_WIN_ADR_END_H,  0, (GDISPH-1)/256},  /* 73 max yadr h */
   { GCTRL_V_WIN_ADR_END_L,  0, (GDISPH-1)&0xff}, /* 74 mac yadr h */
   { GCTRL_RGB_END_H,        0, (GDISPH-1)/256},  /* 62 rgb end line h*/
   { GCTRL_RGB_END_L,        0, (GDISPH-1)&0xff}, /* 63 rgb end line l */
   #endif

   {GCTRL_HOR_BACK_PORCH,    0, 0x02} , /* 64 horizontal backporch setting */
   {GCTRL_VER_BACK_PORCH,    0, 0x02},  /* 65 vertical backporch setting */
   {GCTRL_GAMMA_1,           0, 0x10},  /* 143 gamma control */
   {GCTRL_GAMMA_2,           0, 0x67},  /* 144 gamma control */
   {GCTRL_GAMMA_3,           0, 0x07},  /* 145 gamma control */
   {GCTRL_GAMMA_4,           0, 0x56},  /* 146 gamma control */
   {GCTRL_GAMMA_5,           0, 0x07},  /* 147 gamma control */
   {GCTRL_GAMMA_6,           0, 0x01},  /* 148 gamma control */
   {GCTRL_GAMMA_7,           0, 0x76},  /* 149 gamma control */
   {GCTRL_GAMMA_8,           0, 0x65},  /* 150 gamma control */
   {GCTRL_GAMMA_9,           0, 0x00},  /* 151 gamma control */
   {GCTRL_GAMMA_10,          0, 0x00},  /* 152 gamma control */
   {GCTRL_GAMMA_11,          0, 0x00},  /* 153 gamma control */
   {GCTRL_GAMMA_12,          0, 0x00},  /* 154 gamma control */
   {GCTRL_OFFMODE,           0, 0x00},  /* 192 off mode */

   {GCTRL_PWR_CTRL14,        0, 0x18},  /*  40 power supply ctrl */
   {GCTRL_PWR_CTRL3,         0, 0x05},  /*  26 power supply ctrl */
   {GCTRL_PWR_CTRL14,        0, 0x73},  /*  28 power supply ctrl */
   {GCTRL_PWR_CTRL11,        0, 0x74},  /*  36 power supply ctrl */
   {GCTRL_PWR_CTRL12,        0, 0x05},  /*  37 power supply ctrl */
   {GCTRL_PWR_CTRL2,         0, 0x00},  /*  25 power supply ctrl */
   {GCTRL_PWR_CTRL1,        10, 0xc1},  /*  24 power supply ctrl vr1,vr2 regulators on */
   {GCTRL_PWR_CTRL8,         0, 0x01},  /*  30 power supply ctrl */
   {GCTRL_PWR_CTRL1,         0, 0xc5},  /*  24 power supply ctrl DDVD on*/
   {GCTRL_PWR_CTRL1,        10, 0xe5},  /*  24 power supply ctrl VCL turn on */
   {GCTRL_PWR_CTRL1,        10, 0xf5},  /*  24 power supply ctrl VH/VGL turn on */
   {GCTRL_PWR_CTRL4,         0, 0x09},  /*  27 power supply ctrl set vcomh voltage */
   {GCTRL_PWR_CTRL9,         0, 0x11},  /*  31 power supply ctrl set vcoml aplitude */
   {GCTRL_PWR_CTRL10,        0, 0x11},  /*  32 power supply ctrl vcom start */
   {GCTRL_PWR_CTRL8,        10, 0x81},  /*  30 power supply ctrl */
   {GCTRL_GATE_CTRL,         0, 0x00}   /*  59 */
   };


/*
   Send a command
*/
static void ghw_cmd_wr(SGUCHAR cmd, SGUCHAR cmddat)
   {
   #ifndef GHW_NOHDW
   #ifdef GHW_BUS8
   sgwrby(GHWCMD,cmd);    /* Register */
   sgwrby(GHWCMD,cmddat); /* Data */
   #elif defined( GHW_BUS32 )
   sgwrdw(GHWCMDDW, (SGULONG)(((SGUINT) cmd)*256 | cmddat)); /* register,data */
   #else
   sgwrwo(GHWCMDW, ((SGUINT) cmd)*256 | cmddat); /* register,data */
   #endif
   #else
   cmd++;   /* Silence 'not used' warning */
   cmddat++;
   #endif
   }

/*
   Set the y range.
   The row position is set to y.
   After last write on row y2 the write position is reset to y
   Internal ghw function
*/
void ghw_set_xyrange(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   #ifdef GHW_PCSIM
   ghw_set_xyrange_sim( xb, yb, xe, ye);
   #endif

   #ifndef GHW_NOHDW

   ghw_cmd_wr(GCTRL_RAM_ACCESS_CTL, 0x14 ); /* 5  0x10 = WAS = 1, 0x4 = AM = 1 -> inc y first */

   #ifdef GHW_ROTATED
   /* Set range (rotated display) */

   /* Set window range */
   ghw_cmd_wr(GCTRL_H_WIN_ADR_START,yb+GHW_YOFFSET);
   ghw_cmd_wr(GCTRL_H_WIN_ADR_END,  ye+GHW_YOFFSET);
   ghw_cmd_wr(GCTRL_RAM_ADR_X,      yb+GHW_YOFFSET);

   #ifdef GHW_MIRROR_HOR
   /* Mirror logical x coordinates */

   ghw_cmd_wr(GCTRL_V_WIN_ADR_STRT_H,(((GDISPW-1)-xb)+GHW_XOFFSET)/256);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_STRT_L,(((GDISPW-1)-xb)+GHW_XOFFSET)&0xff);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_END_H, (((GDISPW-1)-xe)+GHW_XOFFSET)/256);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_END_L, (((GDISPW-1)-xe)+GHW_XOFFSET)&0xff);
   /* Set address pointer to start of range */
   ghw_cmd_wr(GCTRL_RAM_ADR_YH, (((GDISPW-1)-xb)+GHW_XOFFSET)/256);
   ghw_cmd_wr(GCTRL_RAM_ADR_YL, (((GDISPW-1)-xb)+GHW_XOFFSET)&0xff);

   #else
   /* Normal logical x coordinates */

   ghw_cmd_wr(GCTRL_V_WIN_ADR_STRT_H,(xb+GHW_XOFFSET)/256);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_STRT_L,(xb+GHW_XOFFSET)&0xff);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_END_H, (xe+GHW_XOFFSET)/256);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_END_L, (xe+GHW_XOFFSET)&0xff);
   /* Set address pointer to start of range */
   ghw_cmd_wr(GCTRL_RAM_ADR_YH, (xb+GHW_XOFFSET)/256);
   ghw_cmd_wr(GCTRL_RAM_ADR_YL, (xb+GHW_XOFFSET)&0xff);
   #endif /* GHW_MIRROR_HOR */

   #else /* GHW_ROTATED */

   /* Set range (normal display) */
   ghw_cmd_wr(GCTRL_RAM_ACCESS_CTL, 0x10 ); /* 5  0x10 = WAS = 1, 0x4 = AM = 0 -> inc x first */

   /* Set window range */
   ghw_cmd_wr(GCTRL_H_WIN_ADR_START,xb+GHW_XOFFSET);
   ghw_cmd_wr(GCTRL_H_WIN_ADR_END,  xe+GHW_XOFFSET);
   ghw_cmd_wr(GCTRL_RAM_ADR_X,      xb+GHW_XOFFSET);

   #ifdef GHW_MIRROR_VER
   /* Mirror logical y coordinates */

   ghw_cmd_wr(GCTRL_V_WIN_ADR_STRT_H,(((GDISPH-1)-yb)+GHW_YOFFSET)/256);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_STRT_L,(((GDISPH-1)-yb)+GHW_YOFFSET)&0xff);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_END_H, (((GDISPH-1)-ye)+GHW_YOFFSET)/256);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_END_L, (((GDISPH-1)-ye)+GHW_YOFFSET)&0xff);

   /* Set address pointer to start of range */
   ghw_cmd_wr(GCTRL_RAM_ADR_YH, (((GDISPH-1)-yb)+GHW_YOFFSET)/256);
   ghw_cmd_wr(GCTRL_RAM_ADR_YL, (((GDISPH-1)-yb)+GHW_YOFFSET)&0xff);

   #else

   /* Normal logical y coordinates */
   ghw_cmd_wr(GCTRL_V_WIN_ADR_STRT_H,(yb+GHW_YOFFSET)/256);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_STRT_L,(yb+GHW_YOFFSET)&0xff);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_END_H, (ye+GHW_YOFFSET)/256);
   ghw_cmd_wr(GCTRL_V_WIN_ADR_END_L, (ye+GHW_YOFFSET)&0xff);

   /* Set address pointer to start of range */
   ghw_cmd_wr(GCTRL_RAM_ADR_YH, (yb+GHW_YOFFSET)/256);
   ghw_cmd_wr(GCTRL_RAM_ADR_YL, (yb+GHW_YOFFSET)&0xff);

   #endif /* GHW_MIRROR_VER */

   #endif /* GHW_ROTATED */

   #endif /* GHW_NOHDW */
   }

/*
   Write databyte to controller (at current position) and increment
   internal xadr.

   Internal ghw function
*/
void ghw_auto_wr(GCOLOR dat)
   {
   #ifdef GHW_PCSIM
   ghw_autowr_sim( dat );
   #endif

   #ifndef GHW_NOHDW
   #if ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
      /* 8 bit twice */
      sgwrby(GHWWR, (SGUCHAR)(dat>>8));     /* MSB */
      sgwrby(GHWWR, (SGUCHAR)(dat & 0xff)); /* LSB */
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS8 ))
      /* 8+8+2 */
      sgwrby(GHWWR, (SGUCHAR)(dat>>10));    /* MSB */
      sgwrby(GHWWR, (SGUCHAR)(dat>>2));
      sgwrby(GHWWR, (SGUCHAR)(dat) & 0x3);  /* LSB */
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS8 ))
      /* 6+6+6 */
      sgwrby(GHWWR, (SGUCHAR)(dat>>16) & 0xfc);  /* MSB (RRRRRR**) */
      sgwrby(GHWWR, (SGUCHAR)(dat>>8) & 0xfc);   /*     (GGGGGG**) */
      sgwrby(GHWWR, (SGUCHAR)(dat) & 0xfc);      /* LSB (BBBBBB**) */
   #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
      /* 16 bit collective */
      sgwrwo(GHWWRW, dat);
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS16 ))
      /* 16+2 */
      sgwrwo(GHWWRW, (SGUINT)(dat >> 2));
      sgwrwo(GHWWRW, (SGUINT)(dat) & 3);
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS16 ))
      /* 24 bit rgb collected to 16+2 */
      {
      SGUINT tmp;
      /* dat = generic 24 bit RGB = RRRRRR**GGGGGG**BBBBBB** */
      /* pack to controller specific format */
      /* MSB (RRRRRRGGGGGGBBBB) */
      /* LSB (**************BB) */
      tmp = (((((SGUINT)dat) >> 8) & 0xf100) |
             ((((SGUINT)dat) >> 6) & 0x03f0) |
             ((((SGUINT)dat) >> 4) & 0x000f));
      sgwrwo(GHWWRW, tmp);
      sgwrwo(GHWWRW, (((SGUINT)dat) >> 2) & 0x0003);
      }
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS32 ))
      /* 18 bit collective */
      sgwrdw(GHWWRDW, dat);      /* 18 lsb bit used */
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS32 ))
      /* 24 bit rgb collected to 18 */
      /* dat = generic 24 bit RGB = RRRRRR**GGGGGG**BBBBBB** */
      dat = ((dat & 0xfc0000) >> 6) |
            ((dat & 0x00fc00) >> 4) |
            ((dat & 0x0000fc) >> 2);
      sgwrdw(GHWWRDW, dat);           /* 18 bit color */
   #else
       #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
   #endif

   #endif /* GHW_NOHDW */
   }

/* Perform required dummy reads after column position setting */
void ghw_auto_rd_start(void)
   {
   #ifndef GHW_NOHDW
   SGUCHAR ret;
   #ifdef GHW_BUS8
   ret = sgrdby(GHWRD);     /* Do a dummy read (after a position setting) */
   #elif defined( GHW_BUS32 )
   ret = sgrddw(GHWRDDW);   /* Do a dummy read (after a position setting) */
   #else
   ret = sgrdwo(GHWRDW);    /* Do a dummy read (after a position setting) */
   #endif
   ret++; /* Silence "not used" warning */
   #endif
   }

/*
   Read at address set by previous (write auto-increment) operation
   The address is not incremented
*/
GCOLOR ghw_auto_rd(void)
   {
   #ifndef GHW_NOHDW
      GCOLOR ret;
      #if ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
         /* 8 bit twice */
         ret = (((GCOLOR) sgrdby(GHWRD)) << 8); /* MSB*/
         ret |= ((GCOLOR) sgrdby(GHWRD));       /* LSB*/
      #elif ((GDISPPIXW == 18) && defined( GHW_BUS8 ))
         /* 8+8+2 */
         ret  = ((SGULONG)sgrdby(GHWRD))<<10;
         ret |= ((SGULONG)sgrdby(GHWRD))<<2;
         ret |= ((SGULONG)(sgrdby(GHWRD) & 0x3));
      #elif ((GDISPPIXW == 24) && defined( GHW_BUS8 ))
         /* 6+6+6 */
         ret  = ((SGULONG)sgrdby(GHWRD))<<16;
         ret |= ((SGULONG)sgrdby(GHWRD))<<8;
         ret |= ((SGULONG)sgrdby(GHWRD));
         ret &= 0xfcfcfc; /* Mask unused bits */
      #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
         /* 16 bit collective */
         ret =  (GCOLOR) sgrdwo(GHWRDW);
      #elif ((GDISPPIXW == 18) && defined( GHW_BUS16 ))
         /* 16+2 */
         ret =  ((GCOLOR) sgrdwo(GHWRDW)) << 2;
         ret |=  (GCOLOR) (sgrdwo(GHWRDW) & 3);
      #elif ((GDISPPIXW == 24) && defined( GHW_BUS16 ))
         /* 16+2 split to 24 bit rgb */
         ret =  ((GCOLOR) sgrdwo(GHWRDW)) << 2;
         ret |=  (GCOLOR) (sgrdwo(GHWRDW) & 3);
         ret = ((ret << 6) & 0xfc0000) + ((ret << 4) & 0xfc00) + ((ret << 2)&0xfc);
      #elif ((GDISPPIXW == 18) && defined( GHW_BUS32 ))
         /* 18 bit collective */
         ret =  (GCOLOR) (sgrddw(GHWRDDW) & 0x3ffff);
      #elif ((GDISPPIXW == 24) && defined( GHW_BUS32 ))
         /* 18 bit collective  split to 24 bit rgb*/
         ret =  (GCOLOR) sgrddw(GHWRDDW);
         ret = ((ret << 6) & 0xfc0000) + ((ret << 4) & 0xfc00) + ((ret << 2)&0xfc));
      #else
          #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
      #endif

      /* Enable this code if needed with the actual display module */
      #ifdef GHW_COLOR_SWAP
         /* no RGB hdw swap during read back, so do this in software */
         #if   (GDISPPIXW == 16)
         ret = ((ret >> 11) & 0x1f) + (ret & 0x7e0) + ((ret << 11) &0xf800);
         #elif (GDISPPIXW == 18)
         ret = ((ret >> 10) & 0x3f) + (ret & 0xfc0) + ((ret << 10) &0x3f000);
         #elif (GDISPPIXW == 24)
         ret = ((ret >> 16) & 0xfc) + (ret & 0xfc00) + ((ret << 16) &0xfc0000);
         #endif
      #endif
      return ret;
   #else
     #ifdef GHW_PCSIM
      return ghw_autord_sim();
     #else
      return 0;
     #endif
   #endif /* GHW_NOHDW */
   }

void ghw_setxypos(GXT xb, GYT yb)
   {
   #ifdef GHW_PCSIM
   ghw_set_xy_sim( xb, yb);
   #endif

   #ifdef GHW_ROTATED
   ghw_cmd_wr(GCTRL_RAM_ACCESS_CTL, 0x04 ); /* 5  0x10 = WAS = 1, 0x4 = AM = 1 -> inc y first */
   ghw_cmd_wr(GCTRL_RAM_ADR_X,   yb+GHW_YOFFSET);
   #ifdef GHW_MIRROR_HOR
   ghw_cmd_wr(GCTRL_RAM_ADR_YH, (((GDISPW-1)-xb)+GHW_XOFFSET)/256);
   ghw_cmd_wr(GCTRL_RAM_ADR_YL, (((GDISPW-1)-xb)+GHW_XOFFSET)&0xff);
   #else
   ghw_cmd_wr(GCTRL_RAM_ADR_YH, (xb+GHW_XOFFSET)/256);
   ghw_cmd_wr(GCTRL_RAM_ADR_YL, (xb+GHW_XOFFSET)&0xff);
   #endif

   #else
   ghw_cmd_wr(GCTRL_RAM_ACCESS_CTL, 0x00 ); /* 5  0x10 = WAS = 1, 0x4 = AM = 1 -> inc y first */
   ghw_cmd_wr(GCTRL_RAM_ADR_X,   xb+GHW_XOFFSET);
   #ifdef GHW_MIRROR_VER
   ghw_cmd_wr(GCTRL_RAM_ADR_YH, (((GDISPH-1)-yb)+GHW_YOFFSET)/256);
   ghw_cmd_wr(GCTRL_RAM_ADR_YL, (((GDISPH-1)-yb)+GHW_YOFFSET)&0xff);
   #else
   ghw_cmd_wr(GCTRL_RAM_ADR_YH, (yb+GHW_YOFFSET)/256);
   ghw_cmd_wr(GCTRL_RAM_ADR_YL, (yb+GHW_YOFFSET)&0xff);
   #endif

   #endif
   }

/***********************************************************************/
/**        hx8312 Initialization and error handling functions         **/
/***********************************************************************/

/*
   Change default (palette) colors
*/
void ghw_setcolor(GCOLOR fore, GCOLOR back)
   {
   /* Update active colors */
   #if (GDISPPIXW == 24)
   /* Mask unused bits */
   fore &= 0xfcfcfc;
   back &= 0xfcfcfc;
   #endif
   ghw_def_foreground = fore;
   ghw_def_background = back;
   }

/*
   Convert an RGB structure to a color value using the current color mode
*/
GCOLOR ghw_rgb_to_color( GCONSTP GPALETTE_RGB *palette )
   {
   if (palette == NULL)
      return 0;

   #if (GDISPPIXW == 8)
   return (GCOLOR)(palette->r & 0xe0) |
           ((palette->g >> 3) & 0x1c) |
           ((palette->b) >> 6) & 0x3;
   #elif (GDISPPIXW == 16)
   return
      (((GCOLOR)(palette->r & 0xf8)) << 8) |
      (((GCOLOR)(palette->g & 0xfc)) << 3) |
       ((GCOLOR)(palette->b >> 3));
   #elif (GDISPPIXW == 18)
   return
      (((GCOLOR)(palette->r & 0xfc)) << 10) |
      (((GCOLOR)(palette->g & 0xfc)) << 4) |
       ((GCOLOR)(palette->b >> 2));
   #else
   return
      (((GCOLOR)(palette->r)) << 16) |
      (((GCOLOR)(palette->g)) << 8 ) |
       ((GCOLOR)(palette->b));
   #endif
   }

#if (GHW_PALETTE_SIZE > 0)
/*
   Load a new palette or update the existing palette
   (Palette is only used with symbols using 2 or 4 bits pr pixel)
*/
SGBOOL ghw_palette_wr(SGUINT start_index, SGUINT num_elements, GCONSTP GPALETTE_RGB PFCODE *palette)
   {
   if ((num_elements == 0) ||
       ((start_index + num_elements) > 16) ||
       (palette == NULL))
      {
      glcd_err = 1; /* Some parameter error */
      return 1;
      }
   glcd_err = 0;

   /* (Partial) update of operative palette values */
   while(num_elements-- > 0)
      {
      /* Make local palette copy here to be compatible with compilers
         having a non-standard conforming handling of pointer
         (i.e when PFCODE memory qualifer is used) */
      GPALETTE_RGB pal;
      pal.r = palette->r;
      pal.g = palette->g;
      pal.b = palette->b;
      ghw_palette_opr[start_index++] = ghw_rgb_to_color(&pal);
      palette++;

      /* ghw_palette_opr[start_index++] = ghw_rgb_to_color(&palette++); */
      }

   return glcd_err;
   }
#endif

/*
   Fast set or clear of LCD module RAM buffer
   Internal ghw function
*/
static void ghw_bufset(GCOLOR color)
   {
   /* Use hardware accelerator logic */
   GBUFINT cnt;
   cnt = 0;
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   #ifdef GBUFFER
   #ifndef GHW_PCSIM
   if (color != G_BLACK) /* hardware buffer is cleared to black by hardware Only nessesary to clear buffer */
   #endif
      {
      do
         {
         /* Clear using X,Y autoincrement */
         gbuf[cnt] = color;      /* Set ram buffer as well */
         ghw_auto_wr(color);      /* Set LCD buffer */
         }
      while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH)); /* Loop until x+y wrap */
      }
   #ifndef GHW_PCSIM
   else
      {
      do
         {
         /* Clear using X,Y autoincrement */
         gbuf[cnt] = color;      /* Set ram buffer as well */
         }
      while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH)); /* Loop until x+y wrap */
      }
   #endif
   #else
   #ifndef GHW_PCSIM
   if (color != G_BLACK) /* hardware buffer is cleared to black by hardware Only nessesary to clear buffer */
   #endif
      {
      do
         {
         /* Clear using X,Y autoincrement */
         ghw_auto_wr(color);      /* Set LCD buffer */
         }
      while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH)); /* Loop until x+y wrap */
      }
   #endif
   }

#ifdef WR_RD_TEST
/*
   Make write-readback test on controller memory.

   This test returns ok (== 0) when the write-readback test succeded. This indicates that
   the processor / display hardware interface / library configuration combination is
   working ok.

   This test will fail if some databus or control signals is not connected correctly.

   This test will fail if 16/8 bit bus mode selection and color resolution settings
   in the library configuration file does not match the actual bus and color resolution
   configuration for the hardware selected via chip pins. (ex display and processor
   16/8 bit bus width, 8080/6800 bus type settings, word / byte address offsets, etc).

   Note that often a display module vendor have hardwired some HX8312 chip interface
   configuration signals in the display module. Therefore often only one or a few of the
   library configuration possibilities will be avaiable with a given display module
   hardware.

   This test may fail if illegal GCTRLW, GCTRLH, GHW_XOFFSET, GHW_YOFFSET
   configuration settings cause overrun of the on-chip video RAM.

   This test can be exectuted correctly with only logic power on the display module.

   Return 0 if no error,
   Return != 0 if some readback error is detected (the bit pattern may give information
   about connector pins in error)
*/
static GCOLOR ghw_wr_rd_test(void)
   {
   #ifndef GHW_NOHDW
   int i;
   GCOLOR msk,result;

   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);

   #if (GDISPPIXW > 16)
   /* 24 (18) bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%06lx ", (unsigned long) msk); */
      ghw_auto_wr(~msk);
      /*printf(" 0x%06lx\n", (unsigned long) (~msk)); */
      msk <<= 1;
      }
   printf("\n");

   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   ghw_auto_rd_start();
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val1,val2;
      val1 = ghw_auto_rd();
      val2 = ghw_auto_rd();
      result |= (val1 ^ msk);
      result |= (val2 ^ (~msk));
      /*printf("0x%06lx ",  (unsigned long) val1); */
      /*printf(" 0x%06lx\n", (unsigned long) val2); */
      msk <<= 1;
      }
   result &= GHW_COLOR_CMP_MSK;  /* Mask bits unused by controller */
   #else
   /* 16 bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /* printf("0x%04x ", (unsigned short) msk); */
      ghw_auto_wr(~msk);
      /* printf(" 0x%04x\n", (unsigned short) (~msk & 0xffff)); */
      msk <<= 1;
      }
   /* printf("\n"); */
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   ghw_auto_rd_start();
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val;
      val = ghw_auto_rd();
      result |= (val ^ msk);
      /* printf("0x%04x ",  (unsigned short) val); */
      val = ghw_auto_rd();
      /* printf(" 0x%04x\n", (unsigned short) val ); */
      result |= (val ^ (~msk));
      msk <<= 1;
      }
   #endif
   return result;  /* 0 = Nul errors */
   #else
   return 0; /* 0 = Nul errors */
   #endif
   }

#endif /* WR_RD_TEST */

/*
   Wait a number of milli seconds
*/
static void ghw_cmd_wait(SGUCHAR ms)
   {
   #ifdef SGPCMODE
   Sleep(ms); /* delay 1 ms */
   #else
   SGUINT wait1ms;
   while (ms-- != 0)
      {
      wait1ms = 2000;     /* Adjust to achieve a 1 ms loop below */
      while( wait1ms != 0)
         wait1ms--;
      }
   #endif
   }

/*
   Initialize display, clear ram  (low-level)
   Clears glcd_err status before init

   Return 0 if no error,
   Return != 0 if some error
*/
SGBOOL ghw_init(void)
   {
   short i;

   #ifdef GBUFFER
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;
   ghw_upddelay = 0;
   #endif

   glcd_err = 0;
   ghw_io_init(); /* Set any hardware interface lines, controller hardware reset */

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL)
      {
      /* Allocate graphic ram buffer */
      if ((gbuf = (GCOLOR *)calloc(ghw_gbufsize(),1)) == NULL)
         glcd_err = 1;
      else
         gbuf_owner = 1;
      }
   #endif

   if (glcd_err != 0)
      return 1;

   #ifdef GHW_PCSIM
   /* Tell simulator about the visual LCD screen organization */
   ghw_init_sim( GDISPW, GDISPH );
   #endif
   /* Set default colors */
   ghw_setcolor( GHW_PALETTE_FOREGROUND, GHW_PALETTE_BACKGROUND );

   #if (GHW_PALETTE_SIZE > 0)
   /* Load palette */
   ghw_palette_wr(0, sizeof(ghw_palette)/sizeof(GPALETTE_RGB), (GCONSTP GPALETTE_RGB PFCODE *)&ghw_palette[0]);
   #endif

   /* Initialize controller according to configuration file */
   for (i=0; i < sizeof(as1dregs)/sizeof(S1D_REGS); i++)
      {
      ghw_cmd_wr(as1dregs[i].index,as1dregs[i].value);
      if (as1dregs[i].delay != 0)
         ghw_cmd_wait( as1dregs[i].delay );
      }

   /*
      Stimuli test loops for initial oscilloscope test of display interface bus signals
      Uncomment to use the test loop for the given data bus width.
      It is recommended to check all display bus signals with each of the I/O access
      statements in the loop one by one.
   */
   /*
   #ifdef GHW_BUS8
   for(;;)
      {
      SGUCHAR dat;
      sgwrby(GHWCMD,0xff);
      sgwrby(GHWWR,0x00);
      dat = sgrdby(GHWSTA);
      dat = sgrdby(GHWRD);
      }
   #endif
   #ifdef GHW_BUS16
   for(;;)
      {
      SGUINT dat;
      sgwrwo(GHWCMDW,0xffff);
      sgwrwo(GHWWRW,0x0000);
      dat = sgrdwo(GHWSTAW);
      dat = sgrdwo(GHWRDW);
      }
   #endif
   */


   #ifdef WR_RD_TEST
   /*
      NOTE:
      The call of ghw_wr_rd_test() should be commented out in serial mode.
      In serial mode the display controller  does not provide read-back facility
      and this test will always fail.
   */
   ghw_dispon();
   if (ghw_wr_rd_test() != ((GCOLOR) 0))
      {
      /* Controller memory write-readback error detected
      (Check the cable or power connections to the display) */
      G_WARNING("Hardware interface error\nCheck display connections\n");  /* Test Warning message output */
      glcd_err = 1;
      return 1;
      }
   #endif

   ghw_bufset( ghw_def_background );
   ghw_dispon();

   #ifndef GNOCURSOR
   ghw_cursor = GCURSIZE1;    /* Cursor is off initially */
   /* ghw_cursor = GCURSIZE1 | GCURON; */ /* Uncomment to set cursor on initially */
   #endif

   ghw_updatehw();  /* Flush to display hdw or simulator */


   return (glcd_err != 0) ? 1 : 0;
   }

/*
   Return last error state. Called from applications to
   check for LCD HW or internal errors.
   The error state is reset by ghw_init and all high_level
   LCD functions.

   Return == 0 : No errors
   Return != 0 : Some errors
*/
SGUCHAR ghw_err(void)
   {
   #if (defined(_WIN32) && defined( GHW_PCSIM))
   if (GSimError())
      return 1;
   #endif
   return (glcd_err == 0) ? 0 : 1;
   }


/*
   Display a (fatal) error message.
   The LCD display module is always cleared and initialized to
   the system font in advance.
   The error message is automatically centered on the screen
   and any \n characters in the string is processed.

   str = ASCII string to write at display center
*/
void ghw_puterr( PGCSTR str )
   {
   PGCSTR idx;
   SGUINT xcnt;
   GXT xp;
   GYT yp,h,y, sidx;
   PGSYMBYTE psym;
   GCOLOR pval;
   SGUCHAR val;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   #endif

   if (ghw_init() != 0)  /* (Re-) initialize display */
      return;            /* Some initialization error */

   /* Count number of lines in string */
   idx=str;
   if (idx == NULL)
      return;
   xcnt = 1;
   while(*idx)
      {
      if (*(idx++) == '\n')
         xcnt++;
      }

   /* Set start character line */
   h = SYSFONT.symheight;
   yp = (xcnt*h > GDISPH) ? 0 : ((GDISPH-1)-xcnt*h)/2;
   /* Set character height in pixel lines */

   idx=str;
   do
      {
      xcnt=0;  /* Set start x position so line is centered */
      while ((idx[xcnt]!=0) && (idx[xcnt]!='\n') && (xcnt < GDISPBW))
         {
         xcnt++;
         }

      /* Calculate start position for centered line */
      xp = (GDISPW-xcnt*SYSFONT.symwidth)/2;

      /* Display text line */
      while (xcnt-- > 0)
         {
         /* Point to graphic content for character symbol */
         psym = &(sysfontsym[(*idx) & 0x7f].b[0]);
         ghw_set_xyrange(xp,yp,xp+SYSFONT.symwidth-1,yp+(h-1));

         /* Display rows in symbol */
         for (y = 0; y < h; y++)
            {
            /* Get symbol row value */
            val = *psym++;
            /* Initiate LCD controller address pointer */
            #ifdef GBUFFER
            gbufidx = GINDEX(xp, yp+y );
            #endif

            /* Display colums in symbol row */
            for (sidx = 0; sidx < SYSFONT.symwidth; sidx++)
               {
               if ((val & sympixmsk[sidx]) != 0)
                  pval = ghw_def_foreground;
               else
                  pval = ghw_def_background;

               /* End of symbol or end of byte reached */
               #ifdef GBUFFER
               gbuf[gbufidx++] = pval;
               #endif
               ghw_auto_wr(pval);
               }
            }

         idx++;
         xp += SYSFONT.symwidth; /* Move to next symbol in line */
         }

      /* Next text line */
      yp += h;
      if (*idx == '\n')
         idx++;
      }
   while ((*idx != 0) && (yp < GDISPH));

   ghw_updatehw();  /* Flush to display hdw or simulator */
   }

void ghw_exit(void)
   {
   #if defined( GHW_ALLOCATE_BUF)
   if (gbuf != NULL)
      {
      if (gbuf_owner != 0)
         {
         /* Buffer is allocated by ginit, so release graphic buffer here */
         free(gbuf);
         gbuf_owner = 0;
         }
      gbuf = NULL;
      }
   #endif
   ghw_io_exit();         /* Release any LCD hardware resources, if required */
   #ifdef GHW_PCSIM
   ghw_exit_sim(); /* Release simulator resources */
   #endif
   }

#ifndef GNOCURSOR
/*
   Replace cursor type data (there is no HW cursor support in hx8312)
*/
void ghw_setcursor( GCURSOR type)
   {
   ghw_cursor = type;
   #ifdef GHW_ALLOCATE_BUF
   if (gbuf == NULL)
      glcd_err = 1;
   #endif
   }
#endif


/*
   Turn display off
   (Minimize power consumption)
*/
void ghw_dispoff(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif

   #if (( defined(GHW_ROTATED) && !defined(GHW_MIRROR_VER)) || \
        (!defined(GHW_ROTATED) &&  defined(GHW_MIRROR_HOR)))
   ghw_cmd_wr(GCTRL_REG1,0x80);
   /* Wait min 2 frame scans */
   ghw_cmd_wait(45);
   ghw_cmd_wr(GCTRL_GATE_CTRL,0x00);
   #else
   ghw_cmd_wr(GCTRL_REG1,0xa0);
   /* Wait min 2 frame scans */
   ghw_cmd_wait(45);
   ghw_cmd_wr(GCTRL_GATE_CTRL,0x00);
   #endif
   }

/*
   Turn display on
*/
void ghw_dispon(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif

   #if (( defined(GHW_ROTATED) && !defined(GHW_MIRROR_VER)) || \
        (!defined(GHW_ROTATED) &&  defined(GHW_MIRROR_HOR)))
   ghw_cmd_wr(GCTRL_REG1,0x80);
   ghw_cmd_wr(GCTRL_GATE_CTRL,0x01);
   /* Wait min 2 frame scans */
   ghw_cmd_wait(45);
   ghw_cmd_wr(GCTRL_REG1,0x00);
   #else
   ghw_cmd_wr(GCTRL_REG1,0xa0);
   ghw_cmd_wr(GCTRL_GATE_CTRL,0x01);
   /* Wait min 2 frame scans */
   ghw_cmd_wait(45);
   ghw_cmd_wr(GCTRL_REG1,0x20);
   #endif
   }

#if defined( GHW_ALLOCATE_BUF)
/*
   Size of buffer requied to save the whole screen state
*/
GBUFINT ghw_gbufsize( void )
   {
   return (GBUFINT) GBUFSIZE * sizeof(GCOLOR) + (GBUFINT) sizeof(GHW_STATE);
   }

#ifdef GSCREENS
/*
   Check if screen buf owns the screen ressources.
*/
SGUCHAR ghw_is_owner( SGUCHAR *buf )
   {
   return (((GCOLOR *)buf == gbuf) && (gbuf != NULL)) ? 1 : 0;
   }

/*
   Save the current state to the screen buffer
*/
SGUCHAR *ghw_save_state( SGUCHAR *buf )
   {
   GHW_STATE *ps;
   if (!ghw_is_owner(buf))
      return NULL;

   ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);
   ps->upddelay = (ghw_upddelay != 0);
   #ifndef GNOCURSOR
   ps->cursor = ghw_cursor;
   #endif
   ps->foreground = ghw_def_foreground; /* Palette may vary, save it */
   ps->background = ghw_def_background;
   return (SGUCHAR *) gbuf;
   }

/*
   Set state to buf.
   If buffer has not been initiated by to a screen before, only
   the pointer is updated. Otherwise the the buffer
*/
void ghw_set_state(SGUCHAR *buf, SGUCHAR doinit)
   {
   if (gbuf != NULL)
      {
      /* The LCD controller has been initiated before */
      if (gbuf_owner != 0)
         {
         /* Buffer was allocated by ginit, free it so screen can be used instead*/
         free(gbuf);
         gbuf_owner = 0;
         gbuf = NULL;
         }
      }

   if (doinit != 0)
      {
      /* First screen initialization, just set buffer pointer and
         leave rest of initialization to a later call of ghw_init() */
      gbuf = (GCOLOR *) buf;
      gbuf_owner = 0;
      }
   else
      {
      if ((gbuf = (GCOLOR *) buf) != NULL)
         {
         GHW_STATE *ps;
         ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);

         #ifndef GNOCURSOR
         ghw_cursor = ps->cursor;
         #endif
         ghw_upddelay = 0;        /* Force update of whole screen */
         iltx = 0;
         ilty = 0;
         irbx = GDISPW-1;
         irby = GDISPH-1;
         ghw_updatehw();
         ghw_upddelay = (ps->upddelay != 0) ? 1 : 0;
         /* Restore drawing color */
         ghw_setcolor(ps->foreground, ps->background);
         }
      }
   }
#endif  /* GSCREENS */
#endif  /* GHW_ALLOCATE_BUF */

#endif /* GBASIC_INIT_ERR */

