/************************** ghwinit.c *****************************

   Low-level driver functions for the FSA506 display controller
   initialization and error handling.

   Notes: Data read require a dummy read before first read

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is copied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)

   Revision date:    11-11-10
   Revision Purpose: ghw_rgb_to_color(..) updated to use G_RGB_TO_COLOR(r,g,b) macro.

   Version number: 1.1
   Copyright (c) RAMTEX Engineering Aps 2010

*********************************************************************/

#ifdef SGPCMODE
#include "windows.h"  /* Sleep(..) function used by ghw_cmd_wait(..)*/
#endif

#include <s6d0129.h>  /* Controller specific definements */

/*#define WR_RD_TEST*/  /* Define to include write-read-back test in ghw_init() */

#ifndef GHW_FSA506
  #error Unknown controller, Controller and bustype must be selected in gdispcfg.h
#endif

/***** Fix missing definitions in gdispcfg.h *****/
#ifndef GHW_XOFFSET
   #define GHW_XOFFSET 0
#endif
#ifndef GHW_YOFFSET
   #define GHW_YOFFSET 0
#endif

/* Correct missing physical size definitions */
#ifndef GDISP_PW
   #define GDISP_PW 320
#endif
#ifndef GDISP_PH
   #define GDISP_PH 240
#endif

/* Swap physical screen size with rotated logical orientations */
#if (defined(GHW_ROTATE_90 ) || defined(GHW_ROTATE_270))
   #define DISPW GDISP_PH
   #define DISPH GDISP_PW
#else
   #define DISPW GDISP_PW
   #define DISPH GDISP_PH
#endif

/* Check size settings */
#if ((GDISPW < DISPW) || (GDISPH < DISPH) || \
    ((GDISPW*GDISPH) > (640*240)) || ((DISPW*DISPH) > (640*240)))
  #error Illegal size or rotation settings in gdispcfg.h
#endif

#if (!defined( GHW_LARGE_VIDEOBUF ) && ((GDISPW != DISPW) || (GDISPH != DISPH)))
   #error Illegal size or rotation settings in gdispcfg.h
#endif


/* Define display control registers for control of primary drawing operations */
#define  GCTRL_H_WIN_ADR_STRT_H 0x00  /* begin */
#define  GCTRL_H_WIN_ADR_STRT_L 0x01  /* begin */
#define  GCTRL_H_WIN_ADR_END_H  0x02  /* end */
#define  GCTRL_H_WIN_ADR_END_L  0x03  /* end */
#define  GCTRL_V_WIN_ADR_STRT_H 0x04  /* begin */
#define  GCTRL_V_WIN_ADR_STRT_L 0x05  /* begin */
#define  GCTRL_V_WIN_ADR_END_H  0x06  /* end */
#define  GCTRL_V_WIN_ADR_END_L  0x07  /* end */

#define  GCTRL_DISPMODE         0x10  /* Display enable */

#define  GCTRL_RAM_ENA          0xc1  /* Enable GRAM access */
#define  GCTRL_RAM_DIS          0x80  /* Disable GRAM access */

#define  GCTRL_H_WIN_POS_H      0x30  /* begin x */
#define  GCTRL_H_WIN_POS_L      0x31  /* begin x */
#define  GCTRL_V_WIN_POS_H      0x32  /* begin y */
#define  GCTRL_V_WIN_POS_L      0x33  /* begin y */

/********************* Chip access definitions *********************/

#ifndef GHW_NOHDW
   #if defined( GHW_SINGLE_CHIP)
      /* User defined access types and simulated register address def */
      #include <bussim.h>
      #ifdef GHW_BUS8
        #define  sgwrby(a,d) simwrby((a),(d))
        #define  sgrdby(a)   simrdby((a))
      #elif defined (GHW_BUS32)
        #define  sgwrdw(a,d) simwrdw((a),(d))
        #define  sgrddw(a)   simrddw((a))
      #else /* bus 16 */
        #define  sgwrwo(a,d) simwrwo((a),(d))
        #define  sgrdwo(a)   simrdwo((a))
      #endif
   #else
      /* Portable I/O functions + hardware port def */
      #include <sgio.h>
   #endif
#else
   #undef GHW_SINGLE_CHIP /* Ignore single chip mode */
#endif

/***********************************************************************/
/** All static LCD driver data is located here in this ghwinit module **/
/***********************************************************************/

#ifdef GBASIC_INIT_ERR

/* Active foreground and background color */
GCOLOR ghw_def_foreground;
GCOLOR ghw_def_background;

#if (GHW_PALETTE_SIZE > 0)
/* Default soft palette
   The palette file can be edited directly with the ColorIconEdit program
*/
static GCODE GPALETTE_RGB FCODE ghw_palette[16] =
     #include <gcolor_4.pal>
     ;

/* Operative palette (current palette used for color lookup) */
GCOLOR ghw_palette_opr[16];
#endif

/* Use software font */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR  b[8];           /* Symbol data, fixed size = 8 bytes */
   }
GCODE FCODE sysfontsym[0x80] =
   {
   /* The default font MUST be a monospaced black & white (two-color) font */
   #include <sfs0129.sym> /* System font symbol table */
   };

/* Default system font */
GCODE GFONT FCODE SYSFONT =
   {
   6,      /* width */
   8,      /* height */
   sizeof(sysfontsym[0])-sizeof(GSYMHEAD), /* number of data bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL) sysfontsym,  /* pointer to array of SYMBOLS */
   0x80,   /* num symbols in sysfontsym[] */
   NULL    /* pointer to code page */ /* NULL means code page is not used */
   };

#ifdef GBUFFER
   #ifdef GHW_ALLOCATE_BUF
      /* <stdlib.h> is included via gdisphw.h */
      GCOLOR *gbuf = NULL;           /* Graphic buffer pointer */
      static SGBOOL gbuf_owner = 0;   /* Identify pointer ownership */
   #else
      GCOLOR gbuf[GBUFSIZE];         /* Graphic buffer */
   #endif
   GXT GFAST iltx,irbx;     /* "Dirty area" speed optimizers in buffered mode */
   GYT GFAST ilty,irby;
   SGBOOL  ghw_upddelay;    /* Flag for delayed update */
#else
   GCOLOR ghw_tmpbuf[GDISPW]; /* Row line buffer (for block read-modify-write) */
#endif /* GBUFFER */


#ifdef GHW_INTERNAL_CONTRAST
static SGUCHAR ghw_contrast;/* Current contrast value */
#endif

SGBOOL glcd_err;            /* Internal error */
#ifndef GNOCURSOR
GCURSOR ghw_cursor;         /* Current cursor state */
#endif

#ifdef GHW_PCSIM
/* PC simulator declaration */
void ghw_init_sim_viewport( SGUINT dispw, SGUINT disph, SGUINT disppw, SGUINT dispph );
void ghw_exit_sim(void);
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye);
void ghw_set_xy_sim(GXT xb, GYT yb);
void ghw_set_screenpos_sim(GXT xb, GYT yb);
void ghw_autowr_sim( GCOLOR cval );
GCOLOR ghw_autord_sim( void );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
#endif
/**********************************************************************/
/** Low level hx8312 interface functions used only by ghw_xxx modules **/
/**********************************************************************/

/* Bit mask values */
GCODE SGUCHAR FCODE sympixmsk[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

typedef struct
   {
   SGUCHAR index;
   SGUCHAR value;
   } S1D_REGS;


/* Array of configuration descriptors, the registers are initialized in the order given in the table */
static GCODE S1D_REGS FCODE as1dregs[] =
   {
   {0x40, 0x12}, /* PLL + Drive */
   {0x41, 0x01}, /* PLLsetup 1 */
   {0x42, 0x01}, /* PLLsetup 2 */

   {0x00, 0x00},              /* Hor start */
   {0x01, 0x00},              /*  */
   {0x02, ((GDISPW-1)>>8)},   /* Hor end */
   {0x03, ((GDISPW-1)&0xff)}, /*  */
   {0x04, 0x00},              /* Ver start */
   {0x05, 0x00},              /*  */
   {0x06, ((GDISPH-1)>>8)},   /* Ver end */
   {0x07, ((GDISPH-1)&0xff)}, /*  */

   {0x08, (GDISPW>>8)},   /* Hor RAM resolution */
   {0x09, (GDISPW&0xff)}, /*  */

   {0x0a, 0x00}, /* Memory write start, msb */
   {0x0b, 0x00}, /*  */
   {0x0c, 0x00}, /* lsb  */

//   {0x10, 0x0d}, /* Init with display visible, Output mode and clock div */
   {0x10, 0x05}, /* Init with blank display, Output mode and clock div */
   {0x11, 0x05}, /* RGB,BGR order */

   {0x12, 0x00}, /* Hor sync start msb */
   {0x13, 0x00}, /* lsb */

   {0x14, 0x00}, /* Hor sync pulse w */
   {0x15, 0x10}, /*  */

   {0x16, 0x00}, /* DE start pos */
   {0x17, 0x38}, /*  */

   {0x18, (DISPW>>8)},   /* DE hor active region */
   {0x19, (DISPW&0xff)}, /*  */

   {0x1a, ((DISPW+120)>>8) }, /* Hor total in pixels */
   {0x1b, (DISPW+120)}, /*  */

   {0x1c, 0x00}, /* Vsync pulse start */
   {0x1d, 0x00}, /*  */

   {0x1e, 0x00}, /* Vsync pulse width */
   {0x1f, 0x08}, /*  */

   {0x20, 0x00}, /* DE vertical start pos */
   {0x21, 0x12}, /*  */

   {0x22, (DISPH >> 8)}, /* DE vertical active region */
   {0x23,  DISPH & 0xf0}, /*  */

   {0x24,((DISPH+25)>>8)}, /* Ver total in lines */
   {0x25, (DISPH+25)&0xff}, /*  */

   {0x26, 0x00}, /* Memory read start address */
   {0x27, 0x00}, /*  */
   {0x28, 0x00}, /*  */

   #if   defined(GHW_ROTATE_270)
   {0x2d, 0x09}, /* Power on, Clock polarity, 270 degree image rotation */
   #elif defined(GHW_ROTATE_180)
   {0x2d, 0x0b}, /* Power on, Clock polarity, 180 degree image rotation */
   #elif defined(GHW_ROTATE_90 )
   {0x2d, 0x0a}, /* Power on, Clock polarity, 90 degree image rotation */
   #else
   {0x2d, 0x08}, /* Power on, Clock polarity, 0 degree image rotation */
   #endif

   {0x30, 0x00}, /* RAM X position for screen image start */
   {0x31, 0x00}, /*  */

   {0x32, 0x00}, /* RAM Y position for screen image start */
   {0x33, 0x00}, /*  */

   {0x34, GDISPW>>8 },   /* Horizontal RAM resolution in pixels */
   {0x35, GDISPW&0xff},  /*  */
   {0x36, ((640*240)/GDISPW)>>8},   /* Vertical RAM resolution in lines */
   {0x37, ((640*240)/GDISPW)&0xff}  /*  */
   };

/*
   Send a command
*/
static void ghw_cmd(SGUCHAR cmd)
   {
   #ifndef GHW_NOHDW

   #ifdef GHW_BUS8
   sgwrby(GHWCMD,cmd);    /* Register */
   #elif  defined( GHW_BUS32 )
   sgwrdw(GHWCMDDW,(SGULONG) cmd);    /* Register */
   #else
   sgwrwo(GHWCMDW,(SGUINT) cmd);    /* Register */
   #endif

   #else  /* GHW_NOHDW */
   cmd++; /* silience 'not used' warning */
   #endif
   }

/*
   Send a command + data
*/
static void ghw_cmd_wr(SGUCHAR cmd, SGUCHAR cmddat)
   {
   #ifndef GHW_NOHDW

   #ifdef GHW_BUS8
   sgwrby(GHWCMD,cmd);                /* Register */
   sgwrby(GHWWR,cmddat);              /* Data */
   #elif  defined (GHW_BUS32)
   sgwrdw(GHWCMDDW,(SGULONG) cmd);    /* Register */
   sgwrdw(GHWWRDW,(SGULONG) cmddat);  /* Data */
   #else
   sgwrwo(GHWCMDW,(SGUINT) cmd);      /* Register */
   sgwrwo(GHWWRW,(SGUINT) cmddat);    /* Data */
   #endif

   #else  /* GHW_NOHDW */
   cmd++; /* silience 'not used' warning */
   cmddat++;
   #endif
   }

static SGUCHAR ghw_cmd_rd(SGUCHAR cmd)
   {
   #ifndef GHW_NOHDW

   #ifdef GHW_BUS8
   sgwrby(GHWCMD,cmd);                /* Register */
   return sgrdby(GHWRD);              /* Data */
   #elif  defined (GHW_BUS32)
   sgwrdw(GHWCMDDW,(SGULONG) cmd);    /* Register */
   return (SGULONG) sgrddw(GHWRDDW);  /* Data */
   #else
   sgwrwo(GHWCMDW,(SGUINT) cmd);      /* Register */
   return (SGUINT) sgrdwo(GHWRDW);    /* Data */
   #endif

   #else  /* GHW_NOHDW */
   cmd++; /* silience 'not used' warning */
   return 0;
   #endif
   }


/* Write command data byte */
static void ghw_cmddat(SGUCHAR dat)
   {
   #ifndef GHW_NOHDW

   #ifdef GHW_BUS8
   sgwrby(GHWWR, dat);
   #elif  defined (GHW_BUS32)
   sgwrdw(GHWWRDW,(SGULONG) dat); /* Data */
   #else
   sgwrwo(GHWWRW, (SGUINT) dat);
   #endif

   #endif
   }

#if (defined(GBUFFER) || !defined( GHW_NO_LCD_READ_SUPPORT ))

/* Make a single data read operation */
static SGUCHAR ghw_rddat( void )
   {
   #ifndef GHW_NOHDW

   #ifdef GHW_BUS8
   return sgrdby(GHWRD);
   #elif  defined (GHW_BUS32)
   return (SGUCHAR) sgrddw(GHWRDDW);
   #else
   return (SGUCHAR)(sgrdwo(GHWRDW));
   #endif

   #else /* GHW_NOHDW */
   return 0;
   #endif
   }

#endif

/*
   Set the y range.
   The row position is set to y.
   After last write on row y2 the write position is reset to y
   Internal ghw function
*/
void ghw_set_xyrange(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   #ifdef GHW_PCSIM
   ghw_set_xyrange_sim( xb, yb, xe, ye);
   #endif

   ghw_cmd_wr(GCTRL_H_WIN_ADR_STRT_H,(SGUCHAR)(((SGUINT) (xb+GHW_XOFFSET)) >> 8));
   ghw_cmd_wr(GCTRL_H_WIN_ADR_STRT_L,(SGUCHAR) (xb+GHW_XOFFSET));
   ghw_cmd_wr(GCTRL_H_WIN_ADR_END_H ,(SGUCHAR)(((SGUINT) (xe+GHW_XOFFSET)) >> 8));
   ghw_cmd_wr(GCTRL_H_WIN_ADR_END_L ,(SGUCHAR) (xe+GHW_XOFFSET));

   ghw_cmd_wr(GCTRL_V_WIN_ADR_STRT_H,(SGUCHAR)(((SGUINT) (yb+GHW_YOFFSET)) >> 8));
   ghw_cmd_wr(GCTRL_V_WIN_ADR_STRT_L,(SGUCHAR) (yb+GHW_YOFFSET));
   ghw_cmd_wr(GCTRL_V_WIN_ADR_END_H ,(SGUCHAR)(((SGUINT) (ye+GHW_YOFFSET)) >> 8));
   ghw_cmd_wr(GCTRL_V_WIN_ADR_END_L ,(SGUCHAR) (ye+GHW_YOFFSET));

   ghw_cmd(GCTRL_RAM_ENA);   /* Assume graphic data operations follows */
   }

void ghw_setxypos(GXT xb, GYT yb)
   {
   #ifdef GHW_PCSIM
   ghw_set_xy_sim( xb, yb);
   #endif
   ghw_set_xyrange(xb, yb, GDISPW-1, GDISPH-1);
   }

/*
   Write databyte to controller (at current position) and increment
   internal xadr.

   Internal ghw function
*/
void ghw_auto_wr(GCOLOR dat)
   {
   #ifdef GHW_PCSIM
   ghw_autowr_sim( dat );
   #endif

   #ifndef GHW_NOHDW

   #if (defined( GHW_BUS8 ) && (GDISPPIXW == 18))
      /* 8 bit bus mode ( 18 bit color stored as r,g,b) */
      sgwrby(GHWWR, (SGUCHAR)(dat>>16)); /* MSB */
      sgwrby(GHWWR, (SGUCHAR)(dat>>8));
      sgwrby(GHWWR, (SGUCHAR)(dat));     /* LSB */
   #elif (defined( GHW_BUS8 ) && (GDISPPIXW == 16))
      sgwrby(GHWWR, (SGUCHAR)(dat>>8));
      sgwrby(GHWWR, (SGUCHAR)(dat));     /* LSB */
   #elif (defined( GHW_BUS16 ) && (GDISPPIXW == 16))
      /* 16 bit bus mode, 16 bit color */
      sgwrwo(GHWWRW, dat);           /* 16 bit color */
   #elif (defined( GHW_BUS16 ) && (GDISPPIXW == 18))
      /* 16 bit bus mode, 18 bit color */
      sgwrwo(GHWWRW, dat);           /* 16 lsb bit */
      sgwrwo(GHWWRW, (dat>>16));     /*  2 msb bit */
   #elif (defined( GHW_BUS32 ) && (GDISPPIXW == 18))
      /* 32 bit bus mode, 18 bit color */
      sgwrdw(GHWWRDW, dat);           /* 18 bit color */
   #else
      #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
   #endif

   #endif /* GHW_NOHDW */
   }

#if (defined(GBUFFER) || !defined( GHW_NO_LCD_READ_SUPPORT ))
/*
   Perform required dummy reads after column position setting
   Note HX8346 only required one dummy read in 8 bit mode
   although the datasheet (ver 2007-4-18) say 3 reads.
*/
void ghw_auto_rd_start(void)
   {
   ghw_cmd(GCTRL_RAM_ENA);
   }

/*
   Read at address set by previous (write auto-increment) operation
*/
GCOLOR ghw_auto_rd(void)
   {
   #ifndef GHW_NOHDW
      #if (defined( GHW_BUS8 ) && (GDISPPIXW == 18))
         GCOLOR ret;
         /* 8 bit bus mode */
         /* 18 bit color mode  */
         /* Left aligned color info */
         ret =  (((GCOLOR) (sgrdby(GHWRD) & 0x03)) << 16);  /* MSB (******RR) */
         ret |= ((GCOLOR) sgrdby(GHWRD) << 8);              /*     (RRRRGGGG) */
         ret |= ((GCOLOR) sgrdby(GHWRD));                   /* LSB (GGBBBBBB) */
         return ret;
      #elif (defined( GHW_BUS8 ) && (GDISPPIXW == 16))
         GCOLOR ret;
         ret  = (((GCOLOR) sgrdby(GHWRD)) << 8);   /*     (RRRRRGGG) */
         ret |=  ((GCOLOR) sgrdby(GHWRD));         /*     (GGGBBBBB) */
         return ret;
      #elif (defined( GHW_BUS16 ) && (GDISPPIXW == 16))
         return sgrdwo(GHWRDW);
      #elif (defined( GHW_BUS16 ) && (GDISPPIXW == 18))
         GCOLOR ret;
         ret =  ((GCOLOR) sgrdwo(GHWRDW));        /* 18 bit color 16 msb */
         ret |= ((GCOLOR)(sgrdwo(GHWRDW) & 0x3)) << 16;  /* + 2 lsb */
         return ret;
      #elif (defined( GHW_BUS32 ) && (GDISPPIXW == 18))
         return sgrddw(GHWRDDW) & 0x3ffff;
      #else
         #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
      #endif

   #else
     #ifdef GHW_PCSIM
      return ghw_autord_sim();
     #else
      return 0;
     #endif
   #endif /* GHW_NOHDW */
   }

#endif /* GBUFFER || !GHW_NO_LCD_READ_SUPPORT */


/*********************** local configuration functions *******************/

/***********************************************************************/
/**        hx8312 Initialization and error handling functions       **/
/***********************************************************************/

/*
   Change default (palette) colors
*/
void ghw_setcolor(GCOLOR fore, GCOLOR back)
   {
   /* Update active colors */
   ghw_def_foreground = fore;
   ghw_def_background = back;
   }

/*
   Convert an RGB structure to a color value using the current color mode
*/
GCOLOR ghw_rgb_to_color( GCONSTP GPALETTE_RGB *palette )
   {
   if (palette == NULL)
      return 0;

   return G_RGB_TO_COLOR(palette->r,palette->g,palette->b);
   }

#if (GHW_PALETTE_SIZE > 0)
/*
   Load a new palette or update the existing palette
   (Palette is only used with symbols using 2 or 4 bits pr pixel)
*/
SGBOOL ghw_palette_wr(SGUINT start_index, SGUINT num_elements, GCONSTP GPALETTE_RGB PFCODE *palette)
   {
   if ((num_elements == 0) ||
       ((start_index + num_elements) > 16) ||
       (palette == NULL))
      {
      glcd_err = 1;
      return 1;
      }
   glcd_err = 0;

   /* (Partial) update of operative palette values */
   while(num_elements-- > 0)
      {
      /* Make local palette copy here to be compatible with compilers
         having a non-standard conforming handling of pointer
         (i.e when PFCODE memory qualifer is used) */
      GPALETTE_RGB pal;
      pal.r = palette->r;
      pal.g = palette->g;
      pal.b = palette->b;
      ghw_palette_opr[start_index++] = ghw_rgb_to_color(&pal);
      palette++;

      /* ghw_palette_opr[start_index++] = ghw_rgb_to_color(&palette++); */
      }

   return glcd_err;
   }
#endif

/*
   Fast set or clear of LCD module RAM buffer
   Internal ghw function
*/
static void ghw_bufset(GCOLOR color)
   {
   GBUFINT cnt;
   cnt = 0;
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   do
      {
      /* Clear using X,Y autoincrement */
      #ifdef GBUFFER
      gbuf[cnt] = color;      /* Set ram buffer as well */
      #endif
      ghw_auto_wr(color);      /* Set LCD buffer */
      }
   while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH)); /* Loop until x+y wrap */
   }

#if (defined( WR_RD_TEST ) && !defined(GHW_NO_LCD_READ_SUPPORT))
/*
   Make write-readback test on controller memory.

   This test returns ok (== 0) when the write-readback test succeded. This indicates that
   the processor / display hardware interface / library configuration combination is
   working ok.

   This test will fail if some databus or control signals is not connected correctly.

   This test will fail if 16/8 bit bus mode selection and color resolution settings
   in the library configuration file does not match the actual bus and color resolution
   configuration for the hardware selected via chip pins. (ex display and processor
   16/8 bit bus width, 8080/6800 bus type settings, word / byte address offsets, etc).

   Note that often a display module vendor have hardwired some HX8312 chip interface
   configuration signals in the display module. Therefore often only one or a few of the
   library configuration possibilities will be avaiable with a given display module
   hardware.

   This test may fail if illegal GCTRLW, GCTRLH, GHW_XOFFSET, GHW_YOFFSET
   configuration settings cause overrun of the on-chip video RAM.

   This test can be exectuted correctly with only logic power on the display module.

   Return 0 if no error,
   Return != 0 if some readback error is detected (the bit pattern may give information
   about connector pins in error)
*/
static GCOLOR ghw_wr_rd_test(void)
   {
   #ifndef GHW_NOHDW
   int i;
   GCOLOR msk,result;

   /*printf("\n");*/
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);

   #if (GDISPPIXW > 16)
   /* 18 bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%06lx ", (unsigned long) msk);*/
      ghw_auto_wr(~msk);
      /*printf(" 0x%06lx\n", (unsigned long) (~msk));*/
      msk <<= 1;
      }
   /*printf("\n");*/

   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   ghw_auto_rd_start();
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val1,val2;
      val1 = ghw_auto_rd();
      val2 = ghw_auto_rd();
      result |= (val1 ^ msk);
      result |= (val2 ^ (~msk));
      /*printf("0x%06lx ",  (unsigned long) val1);*/
      /*printf(" 0x%06lx\n", (unsigned long) val2);*/
      msk <<= 1;
      }
   result &= GHW_COLOR_CMP_MSK;  /* Mask bits unused by controller */

   #else
   /* 16 bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%04x ", (unsigned short) msk);*/
      ghw_auto_wr(~msk);
      /*printf(" 0x%04x\n", (unsigned short) (~msk & 0xffff));*/
      msk <<= 1;
      }
   /*printf("\n"); */
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   ghw_auto_rd_start();
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val;
      val = ghw_auto_rd();
      result |= (val ^ msk);
      /*printf("0x%04x ",  (unsigned short) val);*/
      val = ghw_auto_rd();
      /*printf(" 0x%04x\n", (unsigned short) val );*/
      result |= (val ^ (~msk));
      msk <<= 1;
      }
   #endif
   return result;  /* 0 = Nul errors */
   #else
   return 0; /* 0 = Nul errors */
   #endif
   }

#endif /* WR_RD_TEST */

/*
   Initialize display, clear ram  (low-level)
   Clears glcd_err status before init

   Return 0 if no error,
   Return != 0 if some error
*/
SGBOOL ghw_init(void)
   {
   short i;

   #ifdef GBUFFER
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;
   ghw_upddelay = 0;
   #endif

   glcd_err = 0;
   ghw_io_init(); /* Set any hardware interface lines, controller hardware reset */

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL)
      {
      /* Allocate graphic ram buffer */
      if ((gbuf = (GCOLOR *)calloc(ghw_gbufsize(),1)) == NULL)
         glcd_err = 1;
      else
         gbuf_owner = 1;
      }
   #endif

   if (glcd_err != 0)
      return 1;

   #ifdef GHW_PCSIM
   ghw_init_sim_viewport( GDISPW, GDISPH, DISPW, DISPH);
   /* If simulator should always show the logical screen size, rather than
     the physical screen size then use this setup instead */
   /* ghw_init_sim_viewport( GDISPW, GDISPH, GDISPW, GDISPH); */
   #endif
   /* Set default colors */
   ghw_setcolor( GHW_PALETTE_FOREGROUND, GHW_PALETTE_BACKGROUND );

   #if (GHW_PALETTE_SIZE > 0)
   /* Load palette */
   ghw_palette_wr(0, sizeof(ghw_palette)/sizeof(GPALETTE_RGB), (GCONSTP GPALETTE_RGB PFCODE *)&ghw_palette[0]);
   #endif

   /* Initialize controller according to configuration file */
   /* Register mode */
   for (i=0; i < sizeof(as1dregs)/sizeof(as1dregs[0]); i++)
      {
      ghw_cmd_wr(as1dregs[i].index,as1dregs[i].value);
      }

   /*
      Stimuli test loops for initial oscilloscope test of display interface bus signals
      Uncomment to use the test loop for the given data bus width.
      It is recommended to check all display bus signals with each of the I/O access
      statements in the loop one by one.
   */
   /*
   #ifdef GHW_BUS8
   for(;;)
      {
      SGUCHAR dat;
      sgwrby(GHWCMD,0xff);
      sgwrby(GHWWR,0x00);
      dat = sgrdby(GHWSTA);
      dat = sgrdby(GHWRD);
      }
   #endif
   #ifdef GHW_BUS16
   for(;;)
      {
      SGUINT dat;
      sgwrwo(GHWCMDW,0xffff);
      sgwrwo(GHWWRW,0x0000);
      dat = sgrdwo(GHWSTAW);
      dat = sgrdwo(GHWRDW);
      }
   #endif
   */


   #if (defined( WR_RD_TEST ) && !defined(GHW_NO_LCD_READ_SUPPORT))
   /*
      NOTE:
      The call of ghw_wr_rd_test() should be commented out in serial mode.
      In serial mode the display controller  does not provide read-back facility
      and this test will always fail.
   */
   if (ghw_wr_rd_test() != ((GCOLOR) 0))
      {
      /* Controller memory write-readback error detected
      (Check the cable or power connections to the display) */
      G_WARNING("Hardware interface error\nCheck display connections\n");  /* Test Warning message output */
      glcd_err = 1;
      return 1;
      }
   #endif

   ghw_dispon();
   ghw_bufset( ghw_def_background );
   ghw_dispon();

   #ifndef GNOCURSOR
   ghw_cursor = GCURSIZE1;    /* Cursor is off initially */
   /* ghw_cursor = GCURSIZE1 | GCURON; */ /* Uncomment to set cursor on initially */
   #endif

   ghw_updatehw();  /* Flush to display hdw or simulator */


   return (glcd_err != 0) ? 1 : 0;
   }

/*
   Return last error state. Called from applications to
   check for LCD HW or internal errors.
   The error state is reset by ghw_init and all high_level
   LCD functions.

   Return == 0 : No errors
   Return != 0 : Some errors
*/
SGUCHAR ghw_err(void)
   {
   #if (defined(_WIN32) && defined( GHW_PCSIM))
   if (GSimError())
      return 1;
   #endif
   return (glcd_err == 0) ? 0 : 1;
   }


/*
   Display a (fatal) error message.
   The LCD display module is always cleared and initialized to
   the system font in advance.
   The error message is automatically centered on the screen
   and any \n characters in the string is processed.

   str = ASCII string to write at display center
*/
void ghw_puterr( PGCSTR str )
   {
   PGCSTR idx;
   SGUINT xcnt;
   GXT xp;
   GYT yp,h,y, sidx;
   PGSYMBYTE psym;
   GCOLOR pval;
   SGUCHAR val;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   #endif

   if (ghw_init() != 0)  /* (Re-) initialize display */
      return;            /* Some initialization error */

   /* Count number of lines in string */
   idx=str;
   if (idx == NULL)
      return;
   xcnt = 1;
   while(*idx)
      {
      if (*(idx++) == '\n')
         xcnt++;
      }

   /* Set start character line */
   h = SYSFONT.symheight;
   yp = (xcnt*h > GDISPH) ? 0 : ((GDISPH-1)-xcnt*h)/2;
   /* Set character height in pixel lines */

   idx=str;
   do
      {
      xcnt=0;  /* Set start x position so line is centered */
      while ((idx[xcnt]!=0) && (idx[xcnt]!='\n') && (xcnt < GDISPBW))
         {
         xcnt++;
         }

      /* Calculate start position for centered line */
      xp = (GDISPW-xcnt*SYSFONT.symwidth)/2;

      /* Display text line */
      while (xcnt-- > 0)
         {
         /* Point to graphic content for character symbol */
         psym = &(sysfontsym[(*idx) & 0x7f].b[0]);
         ghw_set_xyrange(xp,yp,xp+SYSFONT.symwidth-1,yp+(h-1));

         /* Display rows in symbol */
         for (y = 0; y < h; y++)
            {
            /* Get symbol row value */
            val = *psym++;
            /* Initiate LCD controller address pointer */
            #ifdef GBUFFER
            gbufidx = GINDEX(xp, (GBUFINT)yp+y );
            #endif

            /* Display colums in symbol row */
            for (sidx = 0; sidx < SYSFONT.symwidth; sidx++)
               {
               if ((val & sympixmsk[sidx]) != 0)
                  pval = ghw_def_foreground;
               else
                  pval = ghw_def_background;

               /* End of symbol or end of byte reached */
               #ifdef GBUFFER
               gbuf[gbufidx++] = pval;
               #endif
               ghw_auto_wr(pval);
               }
            }

         idx++;
         xp += SYSFONT.symwidth; /* Move to next symbol in line */
         }

      /* Next text line */
      yp += h;
      if (*idx == '\n')
         idx++;
      }
   while ((*idx != 0) && (yp < GDISPH));

   ghw_updatehw();  /* Flush to display hdw or simulator */
   }

void ghw_exit(void)
   {
   #if defined( GHW_ALLOCATE_BUF)
   if (gbuf != NULL)
      {
      if (gbuf_owner != 0)
         {
         /* Buffer is allocated by ginit, so release graphic buffer here */
         free(gbuf);
         gbuf_owner = 0;
         }
      gbuf = NULL;
      }
   #endif
   ghw_io_exit();         /* Release any LCD hardware resources, if required */
   #ifdef GHW_PCSIM
   ghw_exit_sim(); /* Release simulator resources */
   #endif
   }

#ifndef GNOCURSOR
/*
   Replace cursor type data (there is no HW cursor support in hx8312)
*/
void ghw_setcursor( GCURSOR type)
   {
   ghw_cursor = type;
   #ifdef GHW_ALLOCATE_BUF
   if (gbuf == NULL)
      glcd_err = 1;
   #endif
   }
#endif


/*
   Turn display off
*/
void ghw_dispoff(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif

   ghw_cmd_wr(GCTRL_DISPMODE,ghw_cmd_rd(GCTRL_DISPMODE) & ~0x08);
   }

/*
   Turn display on (to be used after a ghw_pwroff() call)
*/
void ghw_dispon(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif

   ghw_cmd_wr(GCTRL_DISPMODE,ghw_cmd_rd(GCTRL_DISPMODE) | 0x08);
   }

#ifdef GHW_LARGE_VIDEOBUF
/*
   Adjust position for physical screens "look" into a larger video memory buffer
   (default is 0,0)
*/
void ghw_set_screenpos(GXT x, GYT y)
   {
   if (x > (GDISPW-DISPW))
      x = (GDISPW-DISPW);
   if (y > (GDISPH-DISPH))
      y = (GDISPH-DISPH);

   ghw_cmd_wr(GCTRL_H_WIN_POS_H, (x >> 8));
   ghw_cmd_wr(GCTRL_H_WIN_POS_L, (x & 0xff));
   ghw_cmd_wr(GCTRL_V_WIN_POS_H, (y >> 8));
   ghw_cmd_wr(GCTRL_V_WIN_POS_L, (y & 0xff));

   #ifdef GHW_PCSIM
   ghw_set_screenpos_sim(x, y);
   #endif
   }
#endif

#if defined( GHW_ALLOCATE_BUF)
/*
   Size of buffer requied to save the whole screen state
*/
GBUFINT ghw_gbufsize( void )
   {
   return (GBUFINT) GBUFSIZE * sizeof(GCOLOR) + (GBUFINT) sizeof(GHW_STATE);
   }

#ifdef GSCREENS
/*
   Check if screen buf owns the screen ressources.
*/
SGUCHAR ghw_is_owner( SGUCHAR *buf )
   {
   return (((GCOLOR *)buf == gbuf) && (gbuf != NULL)) ? 1 : 0;
   }

/*
   Save the current state to the screen buffer
*/
SGUCHAR *ghw_save_state( SGUCHAR *buf )
   {
   GHW_STATE *ps;
   if (!ghw_is_owner(buf))
      return NULL;

   ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);
   ps->upddelay = (ghw_upddelay != 0);
   #ifndef GNOCURSOR
   ps->cursor = ghw_cursor;
   #endif
   ps->foreground = ghw_def_foreground; /* Palette may vary, save it */
   ps->background = ghw_def_background;
   return (SGUCHAR *) gbuf;
   }

/*
   Set state to buf.
   If buffer has not been initiated by to a screen before, only
   the pointer is updated. Otherwise the the buffer
*/
void ghw_set_state(SGUCHAR *buf, SGUCHAR doinit)
   {
   if (gbuf != NULL)
      {
      /* The LCD controller has been initiated before */
      if (gbuf_owner != 0)
         {
         /* Buffer was allocated by ginit, free it so screen can be used instead*/
         free(gbuf);
         gbuf_owner = 0;
         gbuf = NULL;
         }
      }

   if (doinit != 0)
      {
      /* First screen initialization, just set buffer pointer and
         leave rest of initialization to a later call of ghw_init() */
      gbuf = (GCOLOR *) buf;
      gbuf_owner = 0;
      }
   else
      {
      if ((gbuf = (GCOLOR *) buf) != NULL)
         {
         GHW_STATE *ps;
         ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);

         #ifndef GNOCURSOR
         ghw_cursor = ps->cursor;
         #endif
         ghw_upddelay = 0;        /* Force update of whole screen */
         iltx = 0;
         ilty = 0;
         irbx = GDISPW-1;
         irby = GDISPH-1;
         ghw_updatehw();
         ghw_upddelay = (ps->upddelay != 0) ? 1 : 0;
         /* Restore drawing color */
         ghw_setcolor(ps->foreground, ps->background);
         }
      }
   }
#endif  /* GSCREENS */
#endif  /* GHW_ALLOCATE_BUF */

#endif /* GBASIC_INIT_ERR */


