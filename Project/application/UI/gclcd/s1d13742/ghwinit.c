/************************** ghwinit.c *****************************

   Low-level driver functions for the S1D13742 LCD display controller
   initialization and error handling.

   Notes: Data read require a dummy read before first read
          S1D13742 always return read data in the 18 bit pixel format
          (not documented in the S1D13742 data sheet 2007.6.14)

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is copied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)


   S1D13742 chip and driver support these mode combinations
        8 bit bus, 16 bit color   8+8
        8 bit bus, 18 bit color   6+6+6
        8 bit bus, 24 bit color   8+8+8
        16 bit bus, 16 bit color  16
        32 bit bus. 18 bit color  12+6
        32 bit bus. 24 bit color  16+8

   Revision date:    19-11-2008
   Revision Purpose: Correction made for 24 bit-pr-pixel, 16 bit data bus mode,

   Revision date:    07-05-09
   Revision Purpose: The symbol software palette (data and functions) can
                     be optimized away if not used by defining
                     GHW_PALETTE_SIZE as 0 in gdispcfg.h
   Revision date:    11-11-10
   Revision Purpose: ghw_rgb_to_color(..) updated to use G_RGB_TO_COLOR(r,g,b) macro.

   Version number: 1.3
   Copyright (c) RAMTEX Engineering Aps 2008-2010

*********************************************************************/

#ifdef SGPCMODE
#include "windows.h"  /* Sleep(..) function used by ghw_cmd_wait(..)*/
#endif
#include <gdisphw.h>  /* HW driver prototypes and types */
#include <s6d0129.h>  /* Controller specific definements */

//#define WR_RD_TEST    /* Define to include write-read-back test in ghw_init() */

#ifdef GHW_S1D13742
  #if (GDISPPIXW > 16) && ((GDISPW*GDISPH*2)>640*1024)
    #error Screen size too large for color resolution, change gdispcfg.h setup
  #endif
#else
  #error Unknown controller, Controller and bustype must be selected in gdispcfg.h
#endif


#ifdef GBASIC_INIT_ERR

/********************* Chip access definitions *********************/

#ifndef GHW_NOHDW
   #if defined( GHW_SINGLE_CHIP)
      /* User defined access types and simulated register address def */
      #include <bussim.h>
      #ifdef GHW_BUS8
        #define  sgwrby(a,d) simwrby((a),(d))
        #define  sgrdby(a)   simrdby((a))
      #elif defined (GHW_BUS32)
        #define  sgwrdw(a,d) simwrdw((a),(d))
        #define  sgrddw(a)   simrddw((a))
      #else /* bus 16 */
        #define  sgwrwo(a,d) simwrwo((a),(d))
        #define  sgrdwo(a)   simrdwo((a))
      #endif
   #else
      /* Portable I/O functions + hardware port def */
      #include <sgio.h>
   #endif
#else
   #undef GHW_SINGLE_CHIP /* Ignore single chip mode */
#endif

/***********************************************************************/
/** All static LCD driver data is located here in this ghwinit module **/
/***********************************************************************/

/* Active foreground and background color */
GCOLOR ghw_def_foreground;
GCOLOR ghw_def_background;

#ifdef GHW_BUS16
static SGUCHAR ghw_oddpos; /* Flag for odd pos used during read back */
#endif

#if (GHW_PALETTE_SIZE > 0)
/* Default soft palette
   The palette file can be edited directly with the ColorIconEdit program
*/
static GCODE GPALETTE_RGB FCODE ghw_palette[16] =
     #include <gcolor_4.pal>
     ;

/* Operative palette (current palette used for color lookup) */
GCOLOR ghw_palette_opr[16];
#endif

/* Use software font */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR  b[8];           /* Symbol data, fixed size = 8 bytes */
   }
GCODE FCODE sysfontsym[0x80] =
   {
   /* The default font MUST be a monospaced black & white (two-color) font */
   #include <sfs0129.sym> /* System font symbol table */
   };

/* Default system font */
GCODE GFONT FCODE SYSFONT =
   {
   6,      /* width */
   8,      /* height */
   sizeof(sysfontsym[0])-sizeof(GSYMHEAD), /* number of data bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL) sysfontsym,  /* pointer to array of SYMBOLS */
   0x80,   /* num symbols in sysfontsym[] */
   NULL    /* pointer to code page */ /* NULL means code page is not used */
   };

#ifdef GBUFFER
   #ifdef GHW_ALLOCATE_BUF
      /* <stdlib.h> is included via gdisphw.h */
      GCOLOR *gbuf = NULL;           /* Graphic buffer pointer */
      static SGBOOL gbuf_owner = 0;   /* Identify pointer ownership */
   #else
      GCOLOR gbuf[GBUFSIZE];         /* Graphic buffer */
   #endif
   GXT GFAST iltx,irbx;     /* "Dirty area" speed optimizers in buffered mode */
   GYT GFAST ilty,irby;
   SGBOOL  ghw_upddelay;    /* Flag for delayed update */
#else
   GCOLOR ghw_tmpbuf[GDISPW]; /* Row line buffer (for block read-modify-write) */
#endif /* GBUFFER */


#ifdef GHW_INTERNAL_CONTRAST
static SGUCHAR ghw_contrast;/* Current contrast value */
#endif

SGBOOL glcd_err;            /* Internal error */
#ifndef GNOCURSOR
GCURSOR ghw_cursor;         /* Current cursor state */
#endif

#ifdef GHW_PCSIM
/* PC simulator declaration */
void ghw_init_sim( SGUINT dispw, SGUINT disph );
void ghw_exit_sim(void);
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye);
void ghw_set_xy_sim(GXT xb, GYT yb);
void ghw_autowr_sim( GCOLOR cval );
GCOLOR ghw_autord_sim( void );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
#endif
/**********************************************************************/
/** Low level S1D13742 interface functions used only by ghw_xxx modules **/
/**********************************************************************/

/* Bit mask values */
GCODE SGUCHAR FCODE sympixmsk[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

typedef struct
   {
   SGUCHAR index;
   SGUCHAR value;
   } S1D_REGS;

/************************ Use command interface *****************/

/* Bit defines to ease setup configuration below */
#ifdef GHW_LOW_VSYNC
  #define VS 0x00
#else
  #define VS 0x80
#endif

#ifdef GHW_LOW_HSYNC
  #define HS 0x00
#else
  #define HS 0x80
#endif

#ifdef GHW_ROTATE_90
   #define ROTATE 1
#elif  defined (GHW_ROTATE_180)
   #define ROTATE 2
#elif  defined (GHW_ROTATE_270)
   #define ROTATE 3
#else
   #define ROTATE 0
#endif

#define  GCTRL_DISPOFF  (0x80 | ROTATE)
#define  GCTRL_DISPON   (0x0  | ROTATE)

/* Registers used actively via high-level features */
#define GCTRL_PLL_M_DIVIDER 0x04
#define GCTRL_DISPMODE      0x34
#define GCTRL_WINDOW_REGS   0x38
#define GCTRL_DATAPORT      0x48
#define GCTRL_READADR       0x4A

#ifndef GHW_HOR_OFFSET
  #define GHW_HOR_OFFSET 1
#endif
#ifndef GHW_VER_OFFSET
  #define GHW_VER_OFFSET 30
#endif

static GCODE S1D_REGS FCODE as1dregs[] =
   {
   /*
   The initialization sequence below is suitable for most display modules.

   If another initialization sequence is recommended by a display vendor
   for a specific display module, then you may replace the initialization
   sequence below with the recommended setting.

   Each line will send one configuration byte to the display module
   For a detailed explanation of each command and datathe hardware manual for
   the display controller should be consulted.
   */
   { 0x56,             0x02 },   /* Power Save Register                                */
   /* This PLL setup is for a 4MHz clock input, 640x480 display and a 50MHz PLL clock output */
   { 0x04,             0x03 },   /* PLL M-Divider Register (4 MHz input clk)           */
   /* This PLL setup is for a 12MHz clock input, 640x480 display and a 50MHz PLL clock output */
/* { 0x04,             0x0B },*/ /* PLL M-Divider Register (12 MHz input clk)          */
   { 0x06,             0xF8 },   /* PLL Setting Register 0                             */
   { 0x08,             0x80 },   /* PLL Setting Register 1                             */
   { 0x0A,             0x28 },   /* PLL Setting Register 2                             */
   { 0x0C,             0x00 },   /* PLL Setting Register 3                             */
   { 0x0E,             0x31 },   /* PLL Setting Register 4                             */
   { 0x12,             0x09 },   /* Clock Source Select Register                       */
   { 0x56,             0x00 },   /* Power Save Register                                */
   { 0xFC,             0x0A },   /* Delay (in us)                                      */

   { 0x14,             0x00 },   /* Panel Type Register                                */
   #if  (defined( GHW_ROTATE_270 ) || defined ( GHW_ROTATE_90))
   { 0x16,GDISPH/8          },   /* Horizontal Display Width Register                  */
   { 0x1A,(GDISPW & 0xff)   },   /* Vertical Display Height Register 0                 */
   { 0x1C,(GDISPW/256)      },   /* Vertical Display Height Register 1                 */
   #else
   { 0x16,GDISPW/8          },   /* Horizontal Display Width Register                  */
   { 0x1A,(GDISPH & 0xff)   },   /* Vertical Display Height Register 0                 */
   { 0x1C,(GDISPH/256)      },   /* Vertical Display Height Register 1                 */
   #endif
   { 0x18,GHW_HOR_NON_DISPLAY }, /* Horizontal Non-Display Period Register             */
   { 0x1E,GHW_VER_NON_DISPLAY }, /* Vertical Non-Display Period Register               */
   { 0x20,        HS | 0x04 },   /* HS Pulse Width Register (in pclks)                 */
   { 0x22,GHW_HOR_OFFSET    },   /* HS Pulse Start Position Register 0                 */
   { 0x24,        VS | 0x04 },   /* VS Pulse Width Register (in lines)                 */
   { 0x26,GHW_VER_OFFSET    },   /* VS Pulse Start Position Register 0                 */
   #ifdef GHW_INV_VDATA
   { 0x28,             0x80 },   /* PCLK Polarity Register (data out on falling edge)  */
   #else
   { 0x28,             0x00 },   /* PCLK Polarity Register (data out on rising edge)   */
   #endif

   #ifdef GHW_BUS8
   #if (GDISPPIXW == 24)
   { 0x2A,             0x13 },   /* Input Mode Register (24 bpp ram data)              */
   #elif (GDISPPIXW == 18)
   { 0x2A,             0x12 },   /* Input Mode Register (18 bpp ram data)              */
   #else
   { 0x2A,             0x01 },   /* Input Mode Register (16 bpp ram data)              */
   #endif
   #else  /*GHW_BUS8*/
   #if (GDISPPIXW == 24)
   { 0x2A,             0x17 },   /* Input Mode Register (24 bpp ram data)              */
   #elif (GDISPPIXW == 18)
   { 0x2A,             0x16 },   /* Input Mode Register (18 bpp ram data)              */
   #else
   { 0x2A,             0x01 },   /* Input Mode Register (16 bpp ram data)              */
   #endif
   #endif /*GHW_BUS8*/

   { 0x2C,             0x00 },   /* Input YUV/RGB Translate Mode Register 0            */
   { 0x2E,             0x05 },   /* YUV/RGB Translate Mode Register 1                  */
   { 0x30,             0x00 },   /* U Data Fix Register                                */
   { 0x32,             0x00 },   /* V Data Fix Register                                */
   { 0x34,           ROTATE },   /* Display (rotation) Mode Register                   */
   { 0x36,             0x00 },   /* Special Effects Register                           */
   { 0x38,             0x00 },   /* Window X Start Position Register 0                 */
   { 0x3A,             0x00 },   /* Window X Start Position Register 1                 */
   { 0x3C,             0x00 },   /* Window Y Start Position Register 0                 */
   { 0x3E,             0x00 },   /* Window Y Start Position Register 1                 */
   #if  (defined( GHW_ROTATE_270 ) || defined ( GHW_ROTATE_90))
   { 0x40,((GDISPH-1)&0xff) },   /* Window X End Position Register 0                   */
   { 0x42,((GDISPH-1)/256)  },   /* Window X End Position Register 1                   */
   { 0x44,((GDISPW-1)&0xff) },   /* Window Y End Position Register 0                   */
   { 0x46,((GDISPW-1)/256)  },   /* Window Y End Position Register 1                   */
   #else
   { 0x40,((GDISPW-1)&0xff) },   /* Window X End Position Register 0                   */
   { 0x42,((GDISPW-1)/256)  },   /* Window X End Position Register 1                   */
   { 0x44,((GDISPH-1)&0xff) },   /* Window Y End Position Register 0                   */
   { 0x46,((GDISPH-1)/256)  },   /* Window Y End Position Register 1                   */
   #endif
   { 0x4A,             0x00 },   /* Memory Read Address Register 0                     */
   { 0x4C,             0x00 },   /* Memory Read Address Register 1                     */
   { 0x4E,             0x00 },   /* Memory Read Address Register 2                     */
   { 0x50,             0x00 },   /* Gamma Correction Enable Register                   */
   { 0x52,             0x00 },   /* Gamma Correction Table Index Register              */
   { 0x54,             0x00 },   /* Gamma Correction Table Data Register               */
   { 0x58,             0x00 },   /* Non-Display Period Control / Status Register       */
   { 0x5A,             0x00 },   /* General Purpose IO Pins Configuration Register 0   */
   { 0x5C,             0x00 },   /* General Purpose IO Pins Status/Control Register 0  */
   { 0x5E,             0x00 },   /* GPIO Positive Edge Interrupt Trigger Register      */
   { 0x60,             0x00 },   /* GPIO Negative Edge Interrupt Trigger Register      */
   { 0x62,             0x00 },   /* GPIO Interrupt Status Register                     */
   { 0x64,             0xFF },   /* GPIO Pull Down Control Register 0                  */
   { 0xFE,             0x00 },   /* LCD Panel Power On Delay (in ms)                   */
   { 0x56,             0x00 }    /* Enter normal mode */
   };

/*
   Send a command
*/

#ifndef GHW_NOHDW

 #ifdef GHW_BUS8
  /* Write command byte */
  #define ghw_cmd(cmd)    sgwrby(GHWCMD,(SGUCHAR)(cmd))
  /* Write command data byte */
  #define ghw_cmddat(dat) sgwrby(GHWWR, (SGUCHAR)(dat))
  #define ghw_sta()       ((SGUCHAR) sgrdby(GHWRD))
 #else  /* GHW_BUS16 */
  #define ghw_cmd(cmd)    sgwrwo(GHWCMDW,(SGUINT)(cmd))
  #define ghw_cmddat(dat) sgwrwo(GHWWRW, (SGUINT)(dat))
  #define ghw_sta()       ((SGUCHAR) sgrdwo(GHWRDW))
 #endif

#else
  #define ghw_cmd(cmd)    { /* Nothing */ }
  #define ghw_cmddat(dat) { /* Nothing */ }
  #define ghw_sta() 0
#endif /* GHW_NOHDW */

/*
   Set the x,y windows range.
   Internal ghw function
   Takes advantages of the index register auto increment feature
*/
void ghw_set_xyrange(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   #ifdef GHW_PCSIM
   ghw_set_xyrange_sim( xb, yb, xe, ye);
   #endif
   ghw_cmd(GCTRL_WINDOW_REGS);
   ghw_cmddat((SGUCHAR) (xb & 0xff));         /* X start */
   ghw_cmddat((SGUCHAR)(xb >> 8));
   ghw_cmddat((SGUCHAR) (yb & 0xff));         /* Y start */
   ghw_cmddat((SGUCHAR)(yb >> 8));
   ghw_cmddat((SGUCHAR) (xe & 0xff));         /* X end */
   ghw_cmddat((SGUCHAR)(xe >> 8));
   ghw_cmddat((SGUCHAR) (ye & 0xff));         /* Y end */
   ghw_cmddat((SGUCHAR)(ye >> 8));
   /* Index register now points on memory data port register GCTRL_DATAPORT */
   }

void ghw_setxypos(GXT xb, GYT yb)
   {
   #ifdef GHW_PCSIM
   ghw_set_xy_sim( xb, yb);
   #endif
   ghw_set_xyrange(xb, yb, GDISPW-1, GDISPH-1);
   }

/*
   Write databyte to controller (at current position) and increment
   internal xadr.

   Internal ghw function
*/
void ghw_auto_wr(GCOLOR dat)
   {
   #ifdef GHW_PCSIM
   ghw_autowr_sim( dat );
   #endif

   #ifndef GHW_NOHDW
   #if ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
      sgwrby(GHWWR, (SGUCHAR )(dat >> 8));
      sgwrby(GHWWR, (SGUCHAR )(dat));
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS8 ))
      /* 6+6+6 format conversion  */
      sgwrby(GHWWR, (SGUCHAR)(dat>>10)); /* r */
      sgwrby(GHWWR, (SGUCHAR)(dat>> 4)); /* g */
      sgwrby(GHWWR,((SGUCHAR)(dat))<<2); /* b */
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS8 ))
      /* 8+8+8 + format conversion  */
      sgwrby(GHWWR, (SGUCHAR)(dat>>16)); /* r */
      sgwrby(GHWWR, (SGUCHAR)(dat>>8));  /* g */
      sgwrby(GHWWR, (SGUCHAR)(dat));     /* b */
   #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
      /* 16 bit bus mode, 16 bit color */
      sgwrwo(GHWWRW, (SGUINT) dat);
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS16 ))
      /* 16 bit bus mode, 18 bit color */
      /* 6 + (6+6) color */
      sgwrwo(GHWWRW, (SGUINT) (dat >> 10));
      sgwrwo(GHWWRW, (SGUINT) (((dat << 4) & 0xfc00) | ((dat << 2) & 0xfc)));
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS16 ))
      /* 16 bit bus mode, 24 bit color */
      sgwrwo(GHWWRW, (SGUINT)(dat>>16));
      sgwrwo(GHWWRW, (SGUINT) dat);
   #else
       #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
   #endif
   #endif /* GHW_NOHDW */
   }


/*
   Read databyte from controller at specified address

   During write S1D13742-S1D13743 handle swirvel view (image rotation) in
   hardware BEFORE storing data in video memory.
   During video memory read S1D13742-S1D13743 uses the PHYSICAL video address
   and increments the physical address (and not the logical x,y position)
   The read address must therefore be recalculated and initialized at each
   pixel read to follow logical x,y positions.

   Internal ghw function
*/
GCOLOR ghw_rd(GXT xb, GYT yb)
   {
   #ifndef GHW_NOHDW
   GCOLOR ret;
   GBUFINT pos;
   #endif

   #ifdef GHW_PCSIM
   ghw_set_xy_sim( xb, yb);
   #endif

   #ifndef GHW_NOHDW
   /* Set memory read position
    (the swirwel view setup only works with write, so the read position
    setup is different for different swirwel view setups.
    Further more data is always read as 16 bits */
   #if defined( GHW_ROTATE_90 )
   pos = (((GBUFINT)((GDISPW-1)-xb))*GDISPH + (GBUFINT)yb)<<1;
   #elif defined( GHW_ROTATE_270 )
   pos = (((GBUFINT)xb)*GDISPH + (GBUFINT)((GDISPH-1)-yb))<<1;
   #elif defined( GHW_ROTATE_180 )
   pos = (((GBUFINT)((GDISPW-1)-xb))+((GBUFINT)((GDISPH-1)-yb))*GDISPW)<<1;
   #else
   pos = (((GBUFINT)xb) + ((GBUFINT)yb)*GDISPW)<<1;
   #endif
   ghw_cmd(GCTRL_READADR);
   ghw_cmddat((SGUCHAR) pos );
   ghw_cmddat((SGUCHAR)(pos>>8));
   ghw_cmddat((SGUCHAR)(pos>>16));
   ghw_cmd(GCTRL_DATAPORT);

   /* Read data */
   #ifdef GHW_BUS8
   ret  = ((GCOLOR)sgrdby(GHWRD));
   ret |= ((GCOLOR)sgrdby(GHWRD))<<8;
   #else
   ret  = (GCOLOR)sgrdwo(GHWRDW);
   #endif

   #if (GDISPPIXW == 18)
      /* Data is returned as 5,6,5, Convert to 18 bit RGB(6:6:6) */
      ret = ((ret & 0xf800)<<2) | ((ret & 0x7ff) << 1);
      /* Let extra LSB color bit track MSB color bits to get full saturation */
      ret |= ((ret >> 5) & 0x01001);
   #elif (GDISPPIXW == 24)
      /* Data is returned as 5,6,5, Convert to 24 bit RGB(8:8:8) */
      ret = ((ret & 0xf800)<<8) | ((ret & 0x07e0) << 5) | ((ret & 0x1f) << 3);
      /* Let extra LSB color bit track MSB color bits to get full saturation */
//      ret |= ((ret >> 5) & 0x070307);
   #endif

   return ret;
   #else
     #ifdef GHW_PCSIM
      return ghw_autord_sim();
     #else
      return 0;
     #endif
   #endif /* GHW_NOHDW */
   }

/***********************************************************************/
/**        S1D13742 Initialization and error handling functions       **/
/***********************************************************************/

/*
   Change default (palette) colors
*/
void ghw_setcolor(GCOLOR fore, GCOLOR back)
   {
   /* Update active colors */
   ghw_def_foreground = fore;
   ghw_def_background = back;
   }

/*
   Convert an RGB structure to a color value using the current color mode
*/
GCOLOR ghw_rgb_to_color( GCONSTP GPALETTE_RGB *palette )
   {
   if (palette == NULL)
      return 0;

   return G_RGB_TO_COLOR(palette->r,palette->g,palette->b);
   }

#if (GHW_PALETTE_SIZE > 0)
/*
   Load a new palette or update the existing palette
   (Palette is only used with symbols using 2 or 4 bits pr pixel)
*/
SGBOOL ghw_palette_wr(SGUINT start_index, SGUINT num_elements, GCONSTP GPALETTE_RGB PFCODE *palette)
   {
   if ((num_elements == 0) ||
       ((start_index + num_elements) > 16) ||
       (palette == NULL))
      {
      glcd_err = 1;
      return 1;
      }
   glcd_err = 0;

   /* (Partial) update of operative palette values */
   while(num_elements-- > 0)
      {
      /* Make local palette copy here to be compatible with compilers
         having a non-standard conforming handling of pointer
         (i.e when PFCODE memory qualifer is used) */
      GPALETTE_RGB pal;
      pal.r = palette->r;
      pal.g = palette->g;
      pal.b = palette->b;
      ghw_palette_opr[start_index++] = ghw_rgb_to_color(&pal);
      palette++;

      /* ghw_palette_opr[start_index++] = ghw_rgb_to_color(&palette++); */
      }

   return glcd_err;
   }
#endif


/*
   Fast set or clear of LCD module RAM buffer
   Internal ghw function
*/
static void ghw_bufset(GCOLOR color)
   {
   /* Use hardware accelerator logic */
   GXT x; GYT y;
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   for (y=0; y < GDISPH; y++)
      {
      for(x=0; x < GDISPW; x++)
         {
         ghw_auto_wr(color);  /* Set LCD buffer */
         #ifdef GBUFFER
         gbuf[x+y*GDISPW] = color; /* Set ram buffer as well */
         #endif
         }
      }
   }

#ifdef WR_RD_TEST
/*
   Make write-readback test on controller memory.

   This test returns ok (== 0) when the write-readback test succeded. This indicates that
   the processor / display hardware interface / library configuration combination is
   working ok.

   This test will fail if some databus or control signals is not connected correctly.

   This test will fail if 16/8 bit bus mode selection and color resolution settings
   in the library configuration file does not match the actual bus and color resolution
   configuration for the hardware selected via chip pins. (ex display and processor
   16/8 bit bus width, 8080/6800 bus type settings, word / byte address offsets, etc).

   This test may fail if illegal GCTRLW, GCTRLH, GHW_XOFFSET, GHW_YOFFSET
   configuration settings cause overrun of the on-chip video RAM.

   This test can be exectuted correctly with only logic power on the display controller.

   Return 0 if no error,
   Return != 0 if some readback error is detected (the bit pattern may give information
   about connector pins in error)
*/
static GCOLOR ghw_wr_rd_test(void)
   {
   #ifndef GHW_NOHDW
   int i;
   GCOLOR msk,result;
   /* Write command data byte */
   /*printf("\n");*/
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);

   /* write */
   #if (GDISPPIXW <= 16)
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      //printf("0x%04x ",   (unsigned short) msk);
      ghw_auto_wr(~msk);
      //printf(" 0x%04x\n", (unsigned short) (~msk));
      msk <<= 1;
      }
   printf("\n");
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val;
      val = ghw_rd(i*2,0);
      result |= (val ^ msk);
      //printf("0x%04x ",  (unsigned int) val);
      val = ghw_rd(i*2+1,0);
      //printf(" 0x%04x\n", (unsigned int) val );
      result |= (val ^ (~msk));
      msk <<= 1;
      }
   #else
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      //printf("0x%06lx ", (unsigned long) msk);
      ghw_auto_wr(~msk);
      //printf(" 0x%06lx\n", (unsigned long) (~msk) & 0xfffff);
      msk <<= 1;
      }
   /*printf("\n");*/
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val;
      val = ghw_rd(i*2,0);
      result |= (val ^ msk);
      //printf("0x%06lx ",  (unsigned long) val);
      val = ghw_rd(i*2+1,0);
      //printf(" 0x%06lx\n", (unsigned long) val & 0xffffff);
      result |= (val ^ (~msk));
      msk <<= 1;
      }
   /* Mask bits to compensate for that read is always done as 16 bits */
   result &= GHW_COLOR_CMP_MSK;
   #endif
   return result;  /* 0 = Nul errors */
   #else
   return 0; /* 0 = Nul errors */
   #endif
   }

#endif /* WR_RD_TEST */

/*
   Initialize display, clear ram  (low-level)
   Clears glcd_err status before init

   Return 0 if no error,
   Return != 0 if some error
*/
SGBOOL ghw_init(void)
   {
   SGUINT i;

   #ifdef GBUFFER
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;
   ghw_upddelay = 0;
   #endif

   glcd_err = 0;
   ghw_io_init(); /* Set any hardware interface lines, controller hardware reset */

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL)
      {
      /* Allocate graphic ram buffer */
      if ((gbuf = (GCOLOR *)calloc(ghw_gbufsize(),1)) == NULL)
         glcd_err = 1;
      else
         gbuf_owner = 1;
      }
   #endif

   if (glcd_err != 0)
      return 1;

   #ifdef GHW_PCSIM
   /* Tell simulator about the visual LCD screen organization */
   ghw_init_sim( GDISPW, GDISPH );
   #endif
   /* Set default colors */
   ghw_setcolor( GHW_PALETTE_FOREGROUND, GHW_PALETTE_BACKGROUND );
   #if (GHW_PALETTE_SIZE > 0)
   /* Load palette */
   ghw_palette_wr(0, sizeof(ghw_palette)/sizeof(GPALETTE_RGB), (GCONSTP GPALETTE_RGB PFCODE *)&ghw_palette[0]);
   #endif
   #if !defined(GHW_NOHDW) && defined(GHW_S1D13742)
   // Check read of chip version register
   ghw_cmd(0); // Revision code register
   if ((ghw_sta() & 0xfc) != 0x80)
      {
      // Illegal product ID returned
      // (Check the chip, cable or power connections)
      G_WARNING("Illegal display controller product ID returned");  // Test Warning message output
      glcd_err = 1;
      return 1;
      }
   #endif

   /* Initialize controller according to configuration file */
   for (i=0; i < sizeof(as1dregs)/sizeof(S1D_REGS); i++)
      {
      if (as1dregs[i].index > 0x80)
         {
         SGUINT dly = as1dregs[i].value*10;
         SGUCHAR tmp = 0;
         while(--dly > 0);
            {
            tmp |= ghw_sta(); /* Use status register read as delay element */
            }
         }
      else
         {
         ghw_cmd(as1dregs[i].index);
         ghw_cmddat(as1dregs[i].value);
         }
      }

   #if !defined(GHW_NOHDW) && defined(GHW_S1D13742)
   /* Check that PLL is stable (so that RAM buffer can be accessed) */
   ghw_cmd(GCTRL_PLL_M_DIVIDER); /* PLL M divider */
   if ((ghw_sta() & 0x80) == 0)
      {
      for(i=(SGUINT)0xffff; ((ghw_sta() & 0x80) == 0); i--)
         {
         if (i==0)
            {
            G_WARNING("PLL does not lock");  /* Test Warning message output */
            glcd_err = 1;
            return 1;
            }
         }
      }
   #endif

   /*
      Stimuli test loops for initial oscilloscope test of display interface bus signals
      Uncomment to use the test loop for the given data bus width.
      It is recommended to check all display bus signals with each of the I/O access
      statements in the loop one by one.
   */
   /*
   #ifdef GHW_BUS8
   for(;;)
      {
      SGUCHAR dat;
      sgwrby(GHWCMD,0xff);
      sgwrby(GHWWR,0x00);
      dat = sgrdby(GHWSTA);
      dat = sgrdby(GHWRD);
      }
   #endif
   #ifdef GHW_BUS16
   for(;;)
      {
      SGUINT dat;
      sgwrwo(GHWCMDW,0xffff);
      sgwrwo(GHWWRW,0x0000);
      dat = sgrdwo(GHWSTAW);
      dat = sgrdwo(GHWRDW);
      }
   #endif
   */


   #ifdef WR_RD_TEST
   /*
      Do write-read test on video memory (check bus access interface)
      This test does not need any display mounted.
   */
   if (ghw_wr_rd_test() != ((GCOLOR) 0))
      {
      /* Controller memory write-readback error detected
      (Check the cable or power connections to the display) */
      G_WARNING("Hardware interface error\nCheck display connections\n");  /* Test Warning message output */
      glcd_err = 1;
      return 1;
      }
   #endif

/*
   ghw_bufset( G_GREEN );
   ghw_bufset( G_RED );
   ghw_bufset( G_BLUE );
*/

   ghw_bufset( ghw_def_background );
   ghw_dispon();

   #ifndef GNOCURSOR
   ghw_cursor = GCURSIZE1;    /* Cursor is off initially */
   /* ghw_cursor = GCURSIZE1 | GCURON; */ /* Uncomment to set cursor on initially */
   #endif

   ghw_updatehw();  /* Flush to display hdw or simulator */

   return (glcd_err != 0) ? 1 : 0;
   }


/*
   Return last error state. Called from applications to
   check for LCD HW or internal errors.
   The error state is reset by ghw_init and all high_level
   LCD functions.

   Return == 0 : No errors
   Return != 0 : Some errors
*/
SGUCHAR ghw_err(void)
   {
   #if (defined(_WIN32) && defined( GHW_PCSIM))
   if (GSimError())
      return 1;
   #endif
   return (glcd_err == 0) ? 0 : 1;
   }


/*
   Display a (fatal) error message.
   The LCD display module is always cleared and initialized to
   the system font in advance.
   The error message is automatically centered on the screen
   and any \n characters in the string is processed.

   str = ASCII string to write at display center
*/
void ghw_puterr( PGCSTR str )
   {
   PGCSTR idx;
   SGUINT xcnt;
   GXT xp;
   GYT yp,h,y, sidx;
   PGSYMBYTE psym;
   GCOLOR pval;
   SGUCHAR val;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   #endif

   if (ghw_init() != 0)  /* (Re-) initialize display */
      return;            /* Some initialization error */

   /* Count number of lines in string */
   idx=str;
   if (idx == NULL)
      return;
   xcnt = 1;
   while(*idx)
      {
      if (*(idx++) == '\n')
         xcnt++;
      }

   /* Set start character line */
   h = SYSFONT.symheight;
   yp = (xcnt*h > GDISPH) ? 0 : ((GDISPH-1)-xcnt*h)/2;
   /* Set character height in pixel lines */

   idx=str;
   do
      {
      xcnt=0;  /* Set start x position so line is centered */
      while ((idx[xcnt]!=0) && (idx[xcnt]!='\n') && (xcnt < GDISPBW))
         {
         xcnt++;
         }

      /* Calculate start position for centered line */
      xp = (GDISPW-xcnt*SYSFONT.symwidth)/2;

      /* Display text line */
      while (xcnt-- > 0)
         {
         /* Point to graphic content for character symbol */
         psym = &(sysfontsym[(*idx) & 0x7f].b[0]);
         ghw_set_xyrange(xp,yp,xp+SYSFONT.symwidth-1,yp+(h-1));

         /* Display rows in symbol */
         for (y = 0; y < h; y++)
            {
            /* Get symbol row value */
            val = *psym++;
            /* Initiate LCD controller address pointer */
            #ifdef GBUFFER
            gbufidx = GINDEX(xp, (GBUFINT)yp+y );
            #endif

            /* Display colums in symbol row */
            for (sidx = 0; sidx < SYSFONT.symwidth; sidx++)
               {
               if ((val & sympixmsk[sidx]) != 0)
                  pval = ghw_def_foreground;
               else
                  pval = ghw_def_background;

               /* End of symbol or end of byte reached */
               #ifdef GBUFFER
               gbuf[gbufidx++] = pval;
               #endif
               ghw_auto_wr(pval);
               }
            }

         idx++;
         xp += SYSFONT.symwidth; /* Move to next symbol in line */
         }

      /* Next text line */
      yp += h;
      if (*idx == '\n')
         idx++;
      }
   while ((*idx != 0) && (yp < GDISPH));

   ghw_updatehw();  /* Flush to display hdw or simulator */
   }

void ghw_exit(void)
   {
   #if defined( GHW_ALLOCATE_BUF)
   if (gbuf != NULL)
      {
      if (gbuf_owner != 0)
         {
         /* Buffer is allocated by ginit, so release graphic buffer here */
         free(gbuf);
         gbuf_owner = 0;
         }
      gbuf = NULL;
      }
   #endif
   ghw_io_exit();         /* Release any LCD hardware resources, if required */
   #ifdef GHW_PCSIM
   ghw_exit_sim(); /* Release simulator resources */
   #endif
   }

#ifndef GNOCURSOR
/*
   Replace cursor type data (there is no HW cursor support in S1D13742)
*/
void ghw_setcursor( GCURSOR type)
   {
   ghw_cursor = type;
   #ifdef GHW_ALLOCATE_BUF
   if (gbuf == NULL)
      glcd_err = 1;
   #endif
   }
#endif


/*
   Turn display off
*/
void ghw_dispoff(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif

   ghw_cmd(GCTRL_DISPMODE);
   ghw_cmddat(GCTRL_DISPOFF);
   }

/*
   Turn display on
*/
void ghw_dispon(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif

   ghw_cmd(GCTRL_DISPMODE);
   ghw_cmddat(GCTRL_DISPON);
   }

#if defined( GHW_ALLOCATE_BUF)
/*
   Size of buffer requied to save the whole screen state
*/
GBUFINT ghw_gbufsize( void )
   {
   return (GBUFINT) GBUFSIZE * sizeof(GCOLOR) + (GBUFINT) sizeof(GHW_STATE);
   }

#ifdef GSCREENS
/*
   Check if screen buf owns the screen ressources.
*/
SGUCHAR ghw_is_owner( SGUCHAR *buf )
   {
   return (((GCOLOR *)buf == gbuf) && (gbuf != NULL)) ? 1 : 0;
   }

/*
   Save the current state to the screen buffer
*/
SGUCHAR *ghw_save_state( SGUCHAR *buf )
   {
   GHW_STATE *ps;
   if (!ghw_is_owner(buf))
      return NULL;

   ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);
   ps->upddelay = (ghw_upddelay != 0);
   #ifndef GNOCURSOR
   ps->cursor = ghw_cursor;
   #endif
   ps->foreground = ghw_def_foreground; /* Palette may vary, save it */
   ps->background = ghw_def_background;
   return (SGUCHAR *) gbuf;
   }

/*
   Set state to buf.
   If buffer has not been initiated by to a screen before, only
   the pointer is updated. Otherwise the the buffer
*/
void ghw_set_state(SGUCHAR *buf, SGUCHAR doinit)
   {
   if (gbuf != NULL)
      {
      /* The LCD controller has been initiated before */
      if (gbuf_owner != 0)
         {
         /* Buffer was allocated by ginit, free it so screen can be used instead*/
         free(gbuf);
         gbuf_owner = 0;
         gbuf = NULL;
         }
      }

   if (doinit != 0)
      {
      /* First screen initialization, just set buffer pointer and
         leave rest of initialization to a later call of ghw_init() */
      gbuf = (GCOLOR *) buf;
      gbuf_owner = 0;
      }
   else
      {
      if ((gbuf = (GCOLOR *) buf) != NULL)
         {
         GHW_STATE *ps;
         ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);

         #ifndef GNOCURSOR
         ghw_cursor = ps->cursor;
         #endif
         ghw_upddelay = 0;        /* Force update of whole screen */
         iltx = 0;
         ilty = 0;
         irbx = GDISPW-1;
         irby = GDISPH-1;
         ghw_updatehw();
         ghw_upddelay = (ps->upddelay != 0) ? 1 : 0;
         /* Restore drawing color */
         ghw_setcolor(ps->foreground, ps->background);
         }
      }
   }
#endif  /* GSCREENS */
#endif  /* GHW_ALLOCATE_BUF */

#endif /* GBASIC_INIT_ERR */



