/************************** ghwinit.c *****************************

   Low-level driver functions for the uc1682 LCD display controller
   initialization and error handling.

   The uc1682 controller is assumed to be used with a LCD module.

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is copied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)


   Creation date:
   Revision date:     4-09-04
   Revision Purpose:  Bug in simulator interface removed. Software
                      tracking of block window wrapping corrected.
   Revision date:     030804
   Revision Purpose:  Simulator update simplified
   Revision date:     27-10-04
   Revision Purpose   GPALETTE_GREY type used for palette to save a
                      few ROM bytes
                      GHW_INVERSE_DISP,GHW_XOFFSET,GHW_YOFFSET added
   Revision date:     14-05-2009
   Revision Purpose:  Interface incoorporated as part of the S6D0129 library
   Revision date:     11-11-10
   Revision Purpose:  ghw_rgb_to_color(..) updated to use G_RGB_TO_COLOR(r,g,b) macro.

   Version number: 1.4
   Copyright (c) RAMTEX Engineering Aps 2004-2010

*********************************************************************/
#define GHWINIT_C

#include <gdisphw.h>  /* HW driver prototypes and types */
#include <s6d0129.h>   /* uc1682 controller specific definements */

/********************* Chip access definitions *********************/

#ifndef GHW_UC1682
  #error Unknown controller. Controller and bustype must be selected in gdispcfg.h
#endif

#define WR_RD_TEST  /* Define to include write-read-back test in ghw_init() */

#ifndef GHW_NOHDW
   #if defined( GHW_SINGLE_CHIP)
      /* User defined access types and simulated register address def */
      #include <bussim.h>
      #define  sgwrby(a,d) simwrby((a),(d))
      #define  sgrdby(a)   simrdby((a))
   #else
      /* Portable I/O functions + hardware port def */
      #include <sgio.h>
   #endif
#else
   #undef GHW_SINGLE_CHIP /* Ignore single chip mode */
#endif

#ifndef GHW_XOFFSET
  #ifdef GHW_MIRROR_HOR
     #define GHW_XOFFSET (104-GDISPW)
  #else
     #define GHW_XOFFSET 0
  #endif
#endif

#ifndef GHW_YOFFSET
  #ifdef GHW_MIRROR_VER
     #define GHW_YOFFSET (80-GDISPH)
  #else
     #define GHW_YOFFSET 0
  #endif
#endif

/* UC1682 register definitions */

/* Status flags */
#define GCTRL_ID 0x80
#define GCTRL_MX 0x40
#define GCTRL_MY 0x20
#define GCTRL_WA 0x10
#define GCTRL_DE 0x08
#define GCTRL_WS 0x04
#define GCTRL_OD 0x02
#define GCTRL_OS 0x01

/* Interenal registers */
#define GCTRL_COL_LSB       0x00  /* 0000 0000b   b[3:0] = CA[3:0] ,column lsb */
#define GCTRL_COL_MSB       0x10  /* 0001 0000b   b[2:0] = CA[6:4] ,column msb */
#define GCTRL_TC            0x24  /* 0010 0100b   b[1:0] = TC[1:0] ,temp. compensation */
#define GCTRL_PANEL_LOAD    0x28  /* 0010 1000b   b[1:0] = PC[1:0] ,panel loading */
#define GCTRL_PUMP_CON      0x2C  /* 0010 1100b   b[1:0] = PC[3:2] ,pump control */
#define GCTRL_LINE_LSB      0x40  /* 0100 0000b   b[3:0] = SL[3:0] ,scroll line LSB */
#define GCTRL_LINE_MSB      0x50  /* 0101 0000b   b[2:0] = SL[6:4] ,scroll line MSB */
#define GCTRL_ROW_LSB       0x60  /* 0110 0000b   b[3:0] = PA[3:0] ,set page address */
#define GCTRL_ROW_MSB       0x70  /* 0111 0000b   b[2:0] = PA[6:4] ,set page address */
#define GCTRL_PAR_CON       0x84  /* 1000 0100b   b[1:0] = LC[9:8] ,partial control */
#define GCTRL_ADDR_CON      0x88  /* 1000 1000b   b[2:0] = AC[2:0] ,ram address control */
#define GCTRL_FIXED_LINE    0x90  /* 1001 0000b   b[3;0] = FL[3:0] ,fixed line */
#define GCTRL_LINE_RATE     0xA0  /* 1010 0000b   b[1:0] = LC[4:3] ,set line rate */
#define GCTRL_ALL_ON        0xA4  /* 1010 0100b   b[ 0 ] = DC[ 1 ] ,all pixel on */
#define GCTRL_INVERSE       0xA6  /* 1010 0110b   b[ 0 ] = DC[ 0 ] ,inverse display */
#define GCTRL_ENABLE        0xA8  /* 1010 1000b   b[1:0] = DC[4:2] ,display enable */
#define GCTRL_COLOR_MASK    0xB0  /* 1011 0000b   b[2:0] = MSK[2:0],color mask */
#define GCTRL_MAP_CON       0xC0  /* 1100 0000b   b[2:0] = LC[2:0] ,mapping control */
#define GCTRL_COLOR_PATTERN 0xD0  /* 1101 0000b   b[ 0 ] = LC[ 5 ] ,color pattern */
#define GCTRL_COLOR_MODE    0xD4  /* 1101 0100b   b[1:0] = LC[7:6] ,color mode */
#define GCTRL_RESET         0xE2  /* 1110 0010b   system reset sequence */
#define GCTRL_NOP           0xE3  /* 1110 0011b   no operation */
#define GCTRL_BIAS          0xE8  /* 1110 1000b   b[1:0] = BR[1:0] ,bias ratio */
#define GCTRL_CURSOR_UPDATE 0xEE  /* 1110 1110b   b[ 0 ] = AC[ 3 ] ,set/reset cursor update mode */
#define GCTRL_E_WINDOW      0xF8  /* 1111 1000b   b[ 0 ] = AC[ 4 ] ,enable window program */

/* double bytes command list ------------------------------------------------ */
#define GCTRL_APC           0x30  /* 0011 0000b    apc[r][7:0] ,r=00 or 01 */
                                  /*     advance program control */
#define GCTRL_GAIN_PM       0x81  /* 1000 0001b   b[7:0] = pm[7:0] ,Vref potential meter */
#define GCTRL_CEN           0xF1  /* 1111 0001b   com end */
                                  /*    b[6:0] = CEN[6:0] (79) */
#define GCTRL_DST           0xF2  /* 1111 0010b   partial display start */
                                  /*    b[6:0] = DST[6:0] (0 ) */
#define GCTRL_DEN           0xF3  /* 1111 0011b   partial display end */
                                  /*    b[6:0] = DEN[6:0] (79) */
#define GCTRL_WPC0          0xF4  /* 1111 0100b   window program starting column address */
                                  /*    b[7:0] = WPC0[7:0] */
#define GCTRL_WPR0          0xF5  /* 1111 0101b   window program starting row address */
                                  /*    b[7:0] = WPP0[7:0] */
#define GCTRL_WPC1          0xF6  /* 1111 0110b   window program ending column address */
                                  /*    b[7:0] = WPC1[7:0] (103) */
#define GCTRL_WPR1          0xF7  /* 1111 0111b   window program ending row address */
                                  /*    b[7:0] = WPP1[7:0] (79) */

/***********************************************************************/
/** All static LCD driver data is located here in this ghwinit module **/
/***********************************************************************/

#ifdef GBASIC_INIT_ERR

/* Active foreground and background color */
GCOLOR ghw_def_foreground;
GCOLOR ghw_def_background;

#if (GHW_PALETTE_SIZE > 0)
/* Default palette used when displaying color symbols using 2 or 4 bits pr pixel.
   The palette file can be edited directly with the ColorIconEdit program
*/
static GCODE GPALETTE_RGB FCODE ghw_palette[16] =
     #include <gcolor_4.pal>
     ;

/* Operative palette (copy of grey scale palette for palette read back) */
GCOLOR ghw_palette_opr[16];
#endif

/* Use software font */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR  b[8];           /* Symbol data, fixed size = 8 bytes */
   }
 GCODE FCODE sysfontsym[0x80] =
   {
   /* The default font MUST be a monospaced black & white (two-color) font */
   #include <sfs0129.sym> /* System font symbol table */
   };

/* Default system font */
GCODE GFONT FCODE SYSFONT =
   {
   8,      /* width */
   8,      /* height */
   sizeof(sysfontsym[0])-sizeof(GSYMHEAD), /* number of data bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL) sysfontsym,  /* pointer to array of SYMBOLS */
   0x80,   /* num symbols in sysfontsym[] */
   NULL    /* pointer to code page */ /* NULL means code page is not used */
   };

#ifdef GBUFFER
   #ifdef GHW_ALLOCATE_BUF
      /* <stdlib.h> is included via gdisphw.h */
      GCOLOR *gbuf = NULL;           /* Graphic buffer pointer */
      static SGBOOL gbuf_owner = 0;   /* Identify pointer ownership */
   #else
      GCOLOR gbuf[GBUFSIZE];         /* Graphic buffer */
   #endif
   GXT GFAST iltx,irbx;     /* "Dirty area" speed optimizers in buffered mode */
   GYT GFAST ilty,irby;
   SGBOOL  ghw_upddelay;    /* Flag for delayed update */
#else
   GCOLOR ghw_tmpbuf[GDISPW]; /* Row line buffer (for fast block read-modify-write) */
#endif /* GBUFFER */

#ifdef GHW_INTERNAL_CONTRAST
static SGUCHAR ghw_contrast;/* Current contrast value */
#endif

SGBOOL glcd_err;            /* Internal error */
#ifndef GNOCURSOR
GCURSOR ghw_cursor;         /* Current cursor state */
#endif

#ifdef GHW_PCSIM
/* PC simulator declaration */
void ghw_init_sim( SGUINT dispw, SGUINT disph );
void ghw_exit_sim(void);
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye);
void ghw_set_xy_sim(GXT xb, GYT yb);
void ghw_autowr_sim( GCOLOR cval );
GCOLOR ghw_autord_sim( void );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
#endif
/**********************************************************************/
/** Low level uc1682 interface functions used only by ghw_xxx modules **/
/**********************************************************************/


/* Bit mask values */
GCODE SGUCHAR FCODE sympixmsk[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

/*
   Send a command
*/
static void ghw_cmd(SGUCHAR cmd)
   {
   #ifndef GHW_NOHDW
   sgwrby(GHWCMD, cmd);
   #endif
   }

/*
   Send a double byte command to uc1682

   Internal ghw function
*/
static void ghw_dcmd( SGUCHAR cmd1, SGUCHAR cmd2 )
   {
   #ifndef GHW_NOHDW
   sgwrby(GHWCMD, cmd1);
   sgwrby(GHWCMD, cmd2);
   #endif
   }

/*
   Define cursor auto wrap area
*/
void ghw_set_xyrange(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   #ifdef GHW_PCSIM
   ghw_set_xyrange_sim( xb, yb, xe, ye);
   #endif
   #ifndef GHW_NOHDW
   /* Set range */
   sgwrby(GHWCMD,GCTRL_E_WINDOW | 0);  /* Reset window program */
   sgwrby(GHWCMD,GCTRL_WPC0);
   sgwrby(GHWCMD,(xb+=GHW_XOFFSET));
   sgwrby(GHWCMD,GCTRL_WPR0);
   sgwrby(GHWCMD,(yb+=GHW_YOFFSET));
   sgwrby(GHWCMD,GCTRL_WPC1);
   sgwrby(GHWCMD,xe+GHW_XOFFSET);
   sgwrby(GHWCMD,GCTRL_WPR1);
   sgwrby(GHWCMD,ye+GHW_YOFFSET);
   sgwrby(GHWCMD,GCTRL_E_WINDOW | 1); /* Enable window program */

   /* Move cursor to upper left corner of range */
   sgwrby(GHWCMD, GCTRL_COL_LSB | (xb & 0xf));
   sgwrby(GHWCMD, GCTRL_COL_MSB | ((xb >> 4) & 0xf));
   sgwrby(GHWCMD, GCTRL_ROW_LSB | (yb & 0xf));
   sgwrby(GHWCMD, GCTRL_ROW_MSB | ((yb >> 4) & 0xf));
   #endif
   }

void ghw_setxypos(GXT xb, GYT yb)
   {
   ghw_set_xyrange(xb, yb, GDISPW-1, GDISPH-1);
   }

/*
   Write databyte to controller (at current position) and increment
   internal xadr.

   Internal ghw function
*/
void ghw_auto_wr(GCOLOR dat)
   {
   #ifdef GHW_PCSIM
   ghw_autowr_sim( dat );
   #endif

   #ifndef GHW_NOHDW
   #if   (GDISPPIXW <= 8)
   sgwrby(GHWWR,dat);                    /* */
   #elif (GDISPPIXW <= 16)
   sgwrby(GHWWR, (SGUCHAR)(dat>>8));     /* MSB*/
   sgwrby(GHWWR, (SGUCHAR)(dat & 0xff)); /* LSB*/
   #else
   sgwrby(GHWWR, (SGUCHAR)(dat>>16));    /* R */
   sgwrby(GHWWR, (SGUCHAR)(dat>>8));     /* G */
   sgwrby(GHWWR, (SGUCHAR)(dat));        /* B */
   #endif
   #endif
   }


#if (defined( GBASIC_TEXT ) || defined( GGRAPHICS ))

#if !defined( GBUFFER ) || defined( WR_RD_TEST )

/* Perform required dummy reads after column position setting */
void ghw_auto_rd_start(void)
   {
   #ifndef GHW_NOHDW
   sgrdby(GHWRD);    /* Do a dummy read (after a position setting) */
   #endif
   }

/*
   Read databyte from controller at current position and increment position
   The UC1682 does not have a symetric read back feature.
   Convert UC1682 read data to the current write format
*/
GCOLOR ghw_auto_rd(void)
   {
   #ifndef GHW_NOHDW
   SGUCHAR rb1,rb2;
   rb1 = sgrdby(GHWRD);
   rb2 = sgrdby(GHWRD);
   #if (GDISPPIXW <= 8)

   /* Convert 16 bit read data to 8 bit color */
   return (GCOLOR)((rb1 & 0xe0) | ((rb1 << 1) & 0x1c) | ((rb2 >> 5) & 0x3));

   #elif (GDISPPIXW <= 16)

   /* Convert 16 bit read data to 16 bit (write) color */
   rb2 = ((rb1 << 7) & 0x80) | ((rb2 >> 1) & 0x40) | ((rb2 >> 2) & 0x1c);
   rb1 = (rb1 & 0xf0) | ((rb1 >> 1) & 0x7);
   return ((GCOLOR) rb1)*256 | ((GCOLOR) rb2);

   #else
   /* Convert 16 bit read data to 24 bit rgb value */
   return (((GCOLOR) (rb1 & 0xf0)) << 16) |
          (((GCOLOR) (rb1 & 0x0f)) << 12) | (((GCOLOR) (rb1 & 0x80)) << 4) |
          (((GCOLOR) (rb2 & 0x70)) << 1);
   #endif

   #else  /* GHW_NOHDW */

   #ifdef GHW_PCSIM

   #if (GDISPPIXW == 24)
   return ghw_autord_sim() & 0xf8fcf8; /* Simulate controller limitations */
   #else
   return ghw_autord_sim();
   #endif

   #else
   return 0;
   #endif

   #endif  /* GHW_NOHDW */


   }
#endif  /* GBUFFER || WR_RD_TEST */


#endif  /* GBASIC_TEXT || GGRAPHICS */

/***********************************************************************/
/**        uc1682 Initialization and error handling functions       **/
/***********************************************************************/

/*
   Change default (palette) colors
*/
void ghw_setcolor(GCOLOR fore, GCOLOR back)
   {
   /* Force resonable palette values */
   GLIMITU(fore,GPIXMAX);
   GLIMITU(back,GPIXMAX);
   /* Update active colors */
   ghw_def_foreground = fore;
   ghw_def_background = back;
   }

/*
   Convert an RGB structure to a color value using the current color mode
*/
GCOLOR ghw_rgb_to_color( const GPALETTE_RGB *palette )
   {
   if (palette == NULL)
      return 0;

   return G_RGB_TO_COLOR(palette->r,palette->g,palette->b);
   }

#if (GHW_PALETTE_SIZE > 0)
/*
   Load a new palette or update the existing palette
   (Palette is only used with symbols using 2 or 4 bits pr pixel)
*/
SGBOOL ghw_palette_wr(SGUINT start_index, SGUINT num_elements, const GPALETTE_RGB *palette)
   {
   if ((num_elements == 0) ||
       ((start_index + num_elements) > 16) ||
       (palette == NULL))
      {
      glcd_err = 1;
      return 1;
      }
   glcd_err = 0;

   /* (Partial) update of operative palette values */
   while(num_elements-- > 0)
      ghw_palette_opr[start_index++] = ghw_rgb_to_color(palette++);

   return glcd_err;
   }
#endif

/*
   Fast set or clear of LCD module RAM buffer
   Internal ghw function
*/
static void ghw_bufset(GCOLOR color)
   {
   GBUFINT cnt;
   cnt = 0;
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   do
      {
      /* Clear using X,Y autoincrement */
      ghw_auto_wr(color);  /* Set LCD buffer */
      #ifdef GBUFFER
      gbuf[cnt] = color; /* Set ram buffer as well */
      #endif
      }
   while (++cnt < ((GBUFINT) GDISPW) * ((GBUFINT) GDISPH)); /* Loop until x+y wrap */
   }

#ifdef WR_RD_TEST
/*
   Make write-readback test on controller memory.

   This test will fail if some databus and control signals is not connected correctly.

   This test will fail if 16/8 bit bus mode selection in the configuration settings
   does not match the actual bus configuration for the hardware (display and processor
   16/8 bit bus width, 8080/6800 bus type settings, word / byte address offsets, etc).

   This test may fail if illegal GCTRLW, GCTRLH, GHW_XOFFSET, GHW_YOFFSET
   configuration settings cause overrun of the on-chip video RAM.

   This test can be exectuted correctly with only logic power on the display module.
   No high-level voltages are nessesary for the test to run (although nothing then can
   be shown on the display)

   Return 0 if no error,
   Return != 0 if some readback error is detected (the bit pattern may give information
   about connector pins in error)

   NOTE:
   This function should be commented out in serial mode. In serial mode
   s6d0129 does not provide read-back facility and this test will always
   fail.
*/
static GCOLOR ghw_wr_rd_test(void)
   {
   #ifndef GHW_NOHDW
   int i;
   GCOLOR msk,result;

   /*printf("\n"); */
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);

   #if (GDISPPIXW > 16)
   /* 24 (18) bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%06lx ", (unsigned long) msk); */
      ghw_auto_wr(~msk);
      /*printf(" 0x%06lx\n", (unsigned long) (~msk)); */
      msk <<= 1;
      }
   /*printf("\n"); */

   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   ghw_auto_rd_start();
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val1,val2;
      val1 = ghw_auto_rd();
      val2 = ghw_auto_rd();
      result |= (val1 ^ msk);
      result |= (val2 ^ (~msk));
      /*printf("0x%06lx ",  (unsigned long) val1); */
      /*printf(" 0x%06lx\n", (unsigned long) val2); */
      msk <<= 1;
      }

   result &= GHW_COLOR_CMP_MSK;  /* Mask bits unused by controller */

   #else
   /* 16 bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%04x ", (unsigned short) msk); */
      ghw_auto_wr(~msk);
      /*printf(" 0x%04x\n", (unsigned short) (~msk & 0xffff)); */
      msk <<= 1;
      }
   /* printf("\n"); */
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   ghw_auto_rd_start();
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val;
      val = ghw_auto_rd();
      result |= (val ^ msk);
      /*printf("0x%04x ",  (unsigned short) val); */
      val = ghw_auto_rd();
      /*printf(" 0x%04x\n", (unsigned short) val ); */
      result |= (val ^ (~msk));
      msk <<= 1;
      }
   #endif
   return result;  /* 0 = Nul errors */
   #else
   return 0; /* 0 = Nul errors */
   #endif
   }

#endif /* WR_RD_TEST */

/*
   Reset and init contoller registers,
   RAM content remain unchanged.
   The Vbias potmeter is reset set to curent state
*/
static void ghw_reset_init(SGBOOL initram)
   {
   SGUCHAR cmd;
   glcd_err = 0;
   /* Initialize controller according to gdispcfg.h configuration */
   ghw_cmd(GCTRL_RESET );

   /* Set default colors */
   ghw_setcolor( GHW_PALETTE_FOREGROUND, GHW_PALETTE_BACKGROUND );

   #ifndef GHW_NOHDW
   #ifdef OTPVERSION
   if ((sgrdby(GHWSTA) & (GCTRL_OS|GCTRL_WS)) != GCTRL_WS)
      {
      SGUINT timeout;
      /* Allow a delay of more than 150 ms since reset before controller is ready */
      timeout = 60000;
      do
         {
         if ((--timeout == 0) || (glcd_err != 0))
            {
            glcd_err = 1;  /* Controller did not return correct status */
            return;        /* Hardware interface error ? */
            }
         }
      while((sgrdby(GHWSTA) & (GCTRL_OS|GCTRL_WS)) != GCTRL_WS);
      }
   #else
   sgrdby(GHWSTA);
   #endif
   #endif

   ghw_cmd(GCTRL_TC | 0);           /* (0:-0.05, 1:-0.10, 2:-0.15, 3:-0.20) */

   #ifdef GHW_MIRROR_VER

   #ifdef GHW_MIRROR_HOR
   cmd = 0x06;
   #else
   cmd = 0x04;
   #endif

   #else

   #ifdef GHW_MIRROR_HOR
   cmd = 0x02;
   #else
   cmd = 0x00;
   #endif

   #endif

   ghw_cmd(GCTRL_MAP_CON | cmd);    /* set mapping control | (MY,MX,LC0)*/
   ghw_cmd(GCTRL_ADDR_CON | 01);    /* Set RAM address control (direction, inc-order, count wrap around*/
   ghw_cmd(GCTRL_LINE_RATE+2);      /* set line rate (0:10K, 1:12.8K, 2:16K, 3:20K) */
   ghw_cmd(GCTRL_PUMP_CON+3);       /* set pump control (0:Ext., 3:Int. S) */
   ghw_cmd(GCTRL_BIAS+3);           /* set bias ratio (0:5, 1:7, 2:8, 3:9) */
   ghw_cmd(GCTRL_COLOR_MASK | 0);   /* use no masking */
   ghw_cmd(GCTRL_INVERSE | 0);      /* Clear inverse display */
   ghw_cmd(GCTRL_ALL_ON | 0);       /* Clear all on */
   ghw_cmd(GCTRL_COLOR_PATTERN | 1);/* Use RGB color order */

   /* set input color mode */
   #if   (GDISPPIXW <= 8)
   ghw_cmd(GCTRL_COLOR_MODE+0);     /* 8 bit */
   #elif (GDISPPIXW <= 16)
   ghw_cmd(GCTRL_COLOR_MODE+2);     /* 16 bit */
   #else
   ghw_cmd(GCTRL_COLOR_MODE+3);     /* 24 bit */
   #endif

   /*
      Stimuli test loops for initial oscilloscope test of display interface bus signals
      Uncomment to use the test loop for the data bus width.
      It is recommended to check all display bus signals with each of the I/O access
      statements in the loop one by one.
   */
   /*
   #ifdef GHW_BUS8
   for(;;)
      {
      SGUCHAR dat;
      sgwrby(GHWCMD,0xff);
      sgwrby(GHWWR,0x00);
      dat = sgrdby(GHWSTA);
      dat = sgrdby(GHWRD);
      }
   #endif
   */

   #ifdef WR_RD_TEST
   /*
      NOTE:
      The call of ghw_wr_rd_test() should be commented if video buffer read
      is not enabled by the hardware
   */
   if (ghw_wr_rd_test() != ((GCOLOR) 0))
      {
      /* Controller memory write-readback error detected
      (Check the cable or power connections to the display) */
      G_WARNING("Hardware interface error\nCheck display connections\n");  /* Test Warning message output */
      glcd_err = 1;
      return;
      }
   #endif

   if (initram)
      ghw_bufset(GHW_PALETTE_BACKGROUND); /* Clear display RAM to white */

   ghw_setxypos(0,0);             /* Reinit cursor position */
   ghw_cont_set(ghw_contrast);    /* Reinit existing contrast setting */
   #ifdef GBUFFER
   ghw_cmd(GCTRL_ENABLE+7); /* set display enable, use dietering */
   #else
   ghw_cmd(GCTRL_ENABLE+3); /* set display enable, turn off dietering so readback value is not modified */
   #endif

   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif
   }

/*
   Initialize display, clear ram  (low-level)
   Clears glcd_err status before init

   Return 0 if no error,
   Return != 0 if some error
*/

SGBOOL ghw_init(void)
   {
   #ifdef GBUFFER
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;
   ghw_upddelay = 0;
   #endif

   glcd_err = 0;
   ghw_io_init();                     /* Set hardware text font lines */

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL)
      {
      /* Allocate graphic ram buffer */
      if ((gbuf = (GCOLOR *) calloc(ghw_gbufsize(),1)) == NULL)
         glcd_err = 1;
      else
         gbuf_owner = 1;
      }
   #endif

   if (glcd_err)
      return 1;

   /* Set default colors */
   #if (GHW_PALETTE_SIZE > 0)
   ghw_palette_wr(0, sizeof(ghw_palette)/sizeof(ghw_palette[0]), &ghw_palette[0]);
   #endif

   #ifdef GHW_PCSIM
   /* Tell simulator about the visual LCD screen organization before pallette init */
   ghw_init_sim( GDISPW, GDISPH );
   #endif

   /* Set default */
   ghw_contrast = 51;
   ghw_reset_init(1);

   #ifndef GNOCURSOR
   ghw_cursor = GCURSIZE1;          /* Cursor is off initially */
   /* ghw_cursor = GCURSIZE1 | GCURON; */ /* Uncomment to set cursor on initially */
   #endif

   ghw_updatehw();  /* Flush to display hdw or simulator */

   return (glcd_err != 0) ? 1 : 0;
   }

/*
   Return last error state. Called from applications to
   check for LCD HW or internal errors.
   The error state is reset by ghw_init and all high_level
   LCD functions.

   Return == 0 : No errors
   Return != 0 : Some errors
*/
SGUCHAR ghw_err(void)
   {
   #if (defined(_WIN32) && defined( GHW_PCSIM))
   if (GSimError())
      return 1;
   #endif
   return (glcd_err == 0) ? 0 : 1;
   }


/*
   Display a (fatal) error message.
   The LCD display module is always cleared and initialized to
   the system font in advance.
   The error message is automatically centered on the screen
   and any \n characters in the string is processed.

   str = ASCII string to write at display center
*/
void ghw_puterr( PGCSTR str )
   {
   PGCSTR idx=str;
   GXT xp,xcnt;
   GYT yp,h,y, sidx;
   PGSYMBYTE psym;
   SGUCHAR val;
   GCOLOR pval;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   #endif

   if (ghw_init() != 0)  /* (Re-) initialize display */
      return;            /* Some initialization error */

   /* Count number of lines in string */
   if (idx == NULL)
      return;
   xcnt = 1;
   while(*idx)
      {
      if (*(idx++) == '\n')
         xcnt++;
      }

   /* Set start character line */
   h = SYSFONT.symheight;
   yp = (xcnt > ((GDISPH/h)-1)) ? 0 : ((GDISPH/h-1)-xcnt)/2;
   /* Set character height in pixel lines */

   idx=str;
   do
      {
      xcnt=0;  /* Set start x position so line is centered */
      while ((idx[xcnt]!=0) && (idx[xcnt]!='\n') && (xcnt < GDISPW))
         {
         xcnt++;
         }

      /* Calculate start position for centered line */
      xp = (GDISPW-xcnt*SYSFONT.symwidth)/2;

      /* Display text line */
      while (xcnt-- > 0)
         {
         /* Point to graphic content for character symbol */
         psym = &(sysfontsym[(*idx) & 0x7f].b[0]);

         /* Display rows in symbol */
         for (y = 0; y < h; y++)
            {
            /* Get symbol row value */
            val = *psym++;
            /* Initiate LCD controller address pointer */
            #ifdef GBUFFER
            gbufidx = GINDEX(xp, yp*h+y );
            #endif
            ghw_setxypos(xp,yp*h+y);

            /* Display colums in symbol row */
            for (sidx = 0; sidx < SYSFONT.symwidth; sidx++)
               {
               if ((val & sympixmsk[sidx]) != 0)
                  pval = ghw_def_foreground;
               else
                  pval = ghw_def_background;

               /* End of symbol or end of byte reached */
               #ifdef GBUFFER
               gbuf[gbufidx++] = pval;
               #endif
               ghw_auto_wr(pval);
               }
            }

         idx++;
         xp += SYSFONT.symwidth; /* Move to next symbol in line */
         }

      /* Next text line */
      yp++;
      if (*idx == '\n')
         idx++;
      }
   while ((*idx != 0) && (yp < GDISPH/8));

   ghw_updatehw();  /* Flush to display hdw or simulator */
   }

void ghw_exit(void)
   {
   #if defined( GHW_ALLOCATE_BUF)
   if (gbuf != NULL)
      {
      if (gbuf_owner != 0)
         {
         /* Buffer is allocated by ginit, so release graphic buffer here */
         free(gbuf);
         gbuf_owner = 0;
         }
      gbuf = NULL;
      }
   #endif
   ghw_io_exit();      /* Release any LCD hardware resources, if required */
   #ifdef GHW_PCSIM
   ghw_exit_sim();   /* Release simulator resources */
   #endif
   }

/*
   Replace cursor type data (there is no HW cursor support in uc1682)
*/
#ifndef GNOCURSOR
void ghw_setcursor( GCURSOR type)
   {
   ghw_cursor = type;
   #ifdef GHW_ALLOCATE_BUF
   if (gbuf == NULL)
      glcd_err = 1;
   #endif
   }
#endif


/*
   Turn display off
   (Minimize power consumption)
*/
void ghw_dispoff(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif
   ghw_cmd( GCTRL_RESET ); /* Use reset to assure capacitor draining */
   }

/*
   Turn display on
*/
void ghw_dispon(void)
   {
   ghw_reset_init(0);      /* Reinit controller, keep RAM content */
   }

#ifdef GHW_INTERNAL_CONTRAST
/*
   Set contrast (Normalized value range [0 : 99] )
   Returns the old value.
*/
SGUCHAR ghw_cont_set(SGUCHAR contrast)
   {
   SGUCHAR tmp;
   GLIMITU(contrast,99);
   tmp = ghw_contrast;
   ghw_contrast = contrast;

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL) {glcd_err = 1; return contrast;}
   #endif

   ghw_dcmd(GCTRL_GAIN_PM, (SGUCHAR)((((SGUINT) contrast) * 256) / 100));   /* Set contrast level (1-127) */

   return tmp;
   }

/*
   Change contrast (Normalized value range [-99 : +99] )
   Returns the old value.
*/
SGUCHAR ghw_cont_change(SGCHAR contrast_diff)
   {
   SGINT tmp = (SGINT) ghw_contrast;
   tmp += (SGINT) contrast_diff;
   GLIMITU(tmp,99);
   GLIMITD(tmp,0);
   return ghw_cont_set((SGUCHAR)tmp);
   }
#endif /* GHW_INTERNAL_CONTRAST */

#if defined( GHW_ALLOCATE_BUF)
/*
   Size of buffer requied to save the whole screen state
*/
GBUFINT ghw_gbufsize( void )
   {
   return (GBUFINT) GBUFSIZE * sizeof(GCOLOR) + sizeof(GHW_STATE);
   }

#ifdef GSCREENS
/*
   Check if screen buf owns the screen ressources.
*/
SGUCHAR ghw_is_owner( SGUCHAR *buf )
   {
   return ((((GCOLOR *) buf) ==  gbuf) && (gbuf != NULL)) ? 1 : 0;
   }

/*
   Save the current state to the screen buffer
*/
SGUCHAR *ghw_save_state( SGUCHAR *buf )
   {
   GHW_STATE *ps;
   if (!ghw_is_owner(buf))
      return NULL;

   ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);
   ps->upddelay = (ghw_upddelay != 0);
   #ifndef GNOCURSOR
   ps->cursor = ghw_cursor;
   #endif
   ps->foreground = ghw_def_foreground; /* Palette may vary, save it */
   ps->background = ghw_def_background;
   return (SGUCHAR *) gbuf;
   }

/*
   Set state to buf.
   If buffer has not been initiated by to a screen before, only
   the pointer is updated. Otherwise the the buffer
*/
void ghw_set_state(SGUCHAR *buf, SGUCHAR doinit)
   {
   if (gbuf != NULL)
      {
      /* The LCD controller has been initiated before */
      if (gbuf_owner != 0)
         {
         /* Buffer was allocated by ginit, free it so screen can be used instead*/
         free(gbuf);
         gbuf_owner = 0;
         gbuf = NULL;
         }
      }

   if (doinit != 0)
      {
      /* First screen initialization, just set buffer pointer and
         leave rest of initialization to a later call of ghw_init() */
      gbuf = (GCOLOR *) buf;
      gbuf_owner = 0;
      }
   else
      {
      if ((gbuf = ((GCOLOR *) buf)) != NULL)
         {
         GHW_STATE *ps;
         ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);

         #ifndef GNOCURSOR
         ghw_cursor = ps->cursor;
         #endif
         ghw_upddelay = 0;        /* Force update of whole screen */
         iltx = 0;
         ilty = 0;
         irbx = GDISPW-1;
         irby = GDISPH-1;
         ghw_updatehw();

         ghw_upddelay = (ps->upddelay != 0) ? 1 : 0;
         /* Restore drawing color */
         ghw_setcolor(ps->foreground, ps->background);
         }
      }
   }
#endif  /* GSCREENS */
#endif  /* GHW_ALLOCATE_BUF */

#endif /* GBASIC_INIT_ERR */


