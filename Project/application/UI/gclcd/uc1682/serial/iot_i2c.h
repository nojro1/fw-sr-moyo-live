/************************************************************************

   I2C driver prototypes

   Copyright(c) RAMTEX Engineering ApS 2004

************************************************************************/
#ifndef IOT_I2C_H
#define IOT_I2C_H

#ifdef __cplusplus
    extern "C" {
#endif

/* definements i2c states */

#define  I2C_ACCES_SUCCEDED      0
#define  I2C_ACCES_NO_ACK        1

/* Copy of direction port */
extern unsigned long iot_dir;

/* i2c state flag */
extern unsigned char i2c_state;      /* the i2c mode and conditions */

void start_i2c_bus( void );
void stop_i2c_bus( void );
void transmit_i2c_byte( unsigned char outshiftval );
void write_i2c_byte( unsigned char i2c_addr, unsigned char i2cval );
unsigned char receive_i2c_byte( unsigned char no_term_ack );
unsigned char get_i2c_byte( unsigned char i2c_addr );
void i2c_reset(void);

#ifdef __cplusplus
   }
#endif
#endif

