#ifndef BUSSIM_H
#define BUSSIM_H
/***************************** bussim.c ************************************

   Definitions for the OLED bus simulator access functions.

   The definitions below should be modified to reflect the port layout in
   the target system and the given access function implementation.

   The defintions below assume the following hardware bit functionality of
   the "address" byte parameter to the access functions.

      A0 = RS  (Data/command select)

   RAMTEX Engineering Aps 2004

****************************************************************************/

/* The I2C address for UC1682 is 0111aa000b, where aa bits is defined by
   the chip hardware address settings */
/*#define LCD_I2C_ADR  0x70*/
/*#define LCD_I2C_ADR  0x74*/
  #define LCD_I2C_ADR  0x78
/*#define LCD_I2C_ADR  0x7c*/

#define GHWWR  (LCD_I2C_ADR | 0x2)
#define GHWRD  (LCD_I2C_ADR | 0x3)
#define GHWSTA (LCD_I2C_ADR | 0x1)
#define GHWCMD (LCD_I2C_ADR | 0x0)

/*************** Do not modify the definitions below ********************/
#include <gdisphw.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Single chip mode -> access via user supplied access driver functions */

void simwrby( SGUCHAR address, SGUCHAR dat);
SGUCHAR simrdby( SGUCHAR address);
SGUINT simrdbuf(SGUCHAR adr, SGUCHAR *p, SGUINT numbytes);
void sim_reset( void );

#ifdef __cplusplus
}
#endif

#endif /* BUSSIM_H */
