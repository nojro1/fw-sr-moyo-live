/***********************************************************

   I2C driver for IOTESTER.

   Emulates an I2C master device. In this implementation
   the SCLK line is always driven high or low by master.
   The SDA  line must have a pull-up resistor. It is driven
   low and is passive high (pulled up when port is 3 stated)

   Copyright(c) RAMTEX Engineering ApS 2004

************************************************************/
#include <iotester.h>
#include <iot_i2c.h>

/* Define the port bits used for the I2C bus. Here port bit P16, P17 */
#define  SDA 0x10000L
#define  SCL 0x20000L

unsigned long iot_dir;
unsigned char i2c_state;      /* the i2c mode and conditions */


void start_i2c_bus( void )
   {
   iot_wr( IOT_IOSET_REG, SCL );            // SCL = 1
   iot_wr( IOT_IODIR_REG, iot_dir & ~SDA ); // SDA = 1 ( = not driven)
   iot_wr( IOT_IODIR_REG, iot_dir | SDA );  // SDA = 0 (driven)
   iot_wr( IOT_IOCLR_REG, ~SCL );           // SCL = 0
   }

void stop_i2c_bus( void )
   {
   iot_wr( IOT_IODIR_REG, iot_dir | SDA );  // SDA = 0 (driven)
   iot_wr( IOT_IOSET_REG, SCL );            // SCL = 1
   iot_wr( IOT_IODIR_REG, iot_dir & ~SDA ); // SDA = 1 ( = not driven)
   }

unsigned char receive_i2c_byte( unsigned char no_term_ack )
   {
   unsigned char bitcnt;
   unsigned char inshiftval;

   inshiftval = 0;

   for ( bitcnt = 8; bitcnt != 0; bitcnt-- )
      {
      iot_wr( IOT_IOSET_REG, SCL );    // SCL = 1

      if (( iot_rd( IOT_INPUT_REG ) & SDA ) == 0 )
         inshiftval *= 2;                 /* multiply by 2 */
      else
         inshiftval = inshiftval * 2 + 1; /* multiply by 2 and add 1 */

      iot_wr( IOT_IOCLR_REG, ~SCL );    // SCL = 0
      }

   // Send ack or nack
   if (no_term_ack)
      iot_wr( IOT_IODIR_REG, iot_dir & ~SDA ); // SDA = 1 ( = not driven)
   else
      iot_wr( IOT_IODIR_REG, iot_dir | SDA );  // SDA = 0 (driven)

   iot_wr( IOT_IOSET_REG, SCL );    // SCL = 1
   iot_wr( IOT_IOCLR_REG, ~SCL );    // SCL = 0

   if (no_term_ack == 0)
      iot_wr( IOT_IODIR_REG, iot_dir & ~SDA ); /* data channel cannot remain false */

   return inshiftval;
   }

void transmit_i2c_byte( unsigned char outshiftval )
   {
   unsigned char bitcnt;

   for ( bitcnt = 8; bitcnt != 0; bitcnt-- )
      {
      if (( outshiftval & 0x80 ) != 0 )
         iot_wr( IOT_IODIR_REG, iot_dir & ~SDA ); // SDA = 1 ( = not driven)
      else
         iot_wr( IOT_IODIR_REG, iot_dir | SDA );  // SDA = 0 (driven)

      iot_wr( IOT_IOSET_REG, SCL );    // SCL = 1
      outshiftval <<= 1;
      iot_wr( IOT_IOCLR_REG, ~SCL );    // SCL = 0
      }

   /* Prepare for acknowledge */
   iot_wr( IOT_IODIR_REG, iot_dir & ~SDA ); // SDA = 1 ( = not driven)
   iot_wr( IOT_IOSET_REG, SCL );            // SCL = 1

   if (( iot_rd( IOT_INPUT_REG ) & SDA ) != 0 )
     i2c_state = I2C_ACCES_NO_ACK; /* no acknowledge from i2c device */

   iot_wr( IOT_IOCLR_REG, ~SCL );            // SCL = 0
   }


/****************************************************************************

   unsigned char get_i2c_byte( unsigned char i2c_addr )

   This function starts the I2C bus and addresses a slave for reading
   One byte is read.

****************************************************************************/
unsigned char get_i2c_byte( unsigned char i2c_addr )
   {
   unsigned char i2cval;

   i2c_state = I2C_ACCES_SUCCEDED;  /* start without i2c errors */
   start_i2c_bus();

   if ( i2c_state == I2C_ACCES_SUCCEDED )
      transmit_i2c_byte( i2c_addr );

   if ( i2c_state == I2C_ACCES_SUCCEDED )
      i2cval = receive_i2c_byte( 1 );    /* read the only and last byte with false */

   if ( i2c_state == I2C_ACCES_SUCCEDED )
      return i2cval;                /* return proper read value */

   return 0;                        /* return zero when error detected */
   }


/****************************************************************************

   void write_i2c_byte( unsigned char i2c_addr, unsigned char i2cval )

   This function starts the I2C bus and addresses a slave for writing.

****************************************************************************/
void write_i2c_byte( unsigned char i2c_addr, unsigned char i2cval )
   {
   i2c_state = I2C_ACCES_SUCCEDED;  /* start without i2c errors */

   start_i2c_bus();

   if ( i2c_state == I2C_ACCES_SUCCEDED )
      transmit_i2c_byte( i2c_addr );

   if ( i2c_state == I2C_ACCES_SUCCEDED )
      transmit_i2c_byte( i2cval );
   }

void i2c_reset(void)
   {
   i2c_state = 0;
   iot_dir |= SCL;
   iot_dir &= ~SDA;

   // SCL is driven 1, SDA is 3-state high
   iot_wr( IOT_IOSET_REG, SCL );   // SCL = 1
   iot_wr( IOT_IODIR_REG, iot_dir );   // SDA = (1)
   iot_wr( IOT_IODIR_REG, 0 );   // SDA = (1)
   iot_wr( IOT_IOCLR_REG, ~SDA );   // SDA prepared of 0 drive
   }
