/***************************** bussim.c ************************************

   Generic template file for external bus simulator drivers.
   Implemented for serial (SPI) bus simulation using a parallel port.

   The functions below must be implemented as described in the comments.
   Use the proper I/O port instructions given by the actual processor
   architecture, the actual LCD connection method and the I/O access
   capabilities of the target C compiler.

   The portable sgwrby(..) and sgrdby(..) syntax is used for illustration.
   This syntax can be replaced by the compilers native syntax for I/O access.

   The functions in this module is called by the ghwinit.c module when the
   compiler switch GHW_SINGLE_CHIP is defined and GHW_NOHDW is undefined.

   The LCD controller does not support read back of the video memory in
   serial mode. Serial bus mode will therefore require that GBUFFER is
   defined, so the LCD driver functions still can do read-modify-write
   operations on the display content.
   A simrdby() function is not required for the same reason.

   The driver assumes that the address is either 0 or 1.

   Copyright (c) RAMTEX Engineering Aps 2004

****************************************************************************/
#ifndef GHW_NOHDW
#ifdef  GHW_SINGLE_CHIP

#include <bussim.h>
#include <iot_i2c.h>

/*#include < Port declaration file for your compiler > */

SGUCHAR is_addressed;
SGUCHAR curadr;

SGUCHAR simrdby( SGUCHAR adr )
   {
   SGUCHAR dat;
   if (!is_addressed || (is_addressed && (adr != curadr)))
      {
      if (is_addressed)
         stop_i2c_bus();
      i2c_state = I2C_ACCES_SUCCEDED;  /* start without i2c errors */
      start_i2c_bus();
      transmit_i2c_byte(adr);
      curadr = adr;
      }

   if ( i2c_state == I2C_ACCES_SUCCEDED )
      dat = receive_i2c_byte(1);
   stop_i2c_bus();
   is_addressed = 0;
   return dat;
   }

SGUINT simrdbuf(SGUCHAR adr, SGUCHAR *p, SGUINT numbytes)
   {
   SGUINT cnt = 0;
   if ((numbytes == 0) || (p == NULL))
      return 0;

   if (!is_addressed || (is_addressed && (adr != curadr)))
      {
      if (is_addressed)
         stop_i2c_bus();
      i2c_state = I2C_ACCES_SUCCEDED;  /* start without i2c errors */
      start_i2c_bus();
      transmit_i2c_byte(adr);
      curadr = adr;
      }

   // Receive buffer until last byte
   while (( i2c_state == I2C_ACCES_SUCCEDED ) && (++cnt < numbytes))
      {
      *p++ = receive_i2c_byte(0);
      }

   // Last byte
   if ( i2c_state == I2C_ACCES_SUCCEDED )
      *p = receive_i2c_byte(1);
   stop_i2c_bus();
   is_addressed = 0;
   return cnt;  /* Return number of bytes read */
   }

void simwrby(SGUCHAR adr, SGUCHAR dat)
   {
   if (!is_addressed || (is_addressed && (adr != curadr)))
      {
      /* New register or direction */
      if (is_addressed)
         stop_i2c_bus();
      i2c_state = I2C_ACCES_SUCCEDED;  /* start without i2c errors */
      start_i2c_bus();
      transmit_i2c_byte(adr);
      curadr = adr;
      is_addressed = 1;
      }

   if ( i2c_state == I2C_ACCES_SUCCEDED )
      transmit_i2c_byte(dat);

   if (i2c_state != I2C_ACCES_SUCCEDED)
      glcd_err |= 1;
   }

/*
   Initialize and reset LCD display.
   Is called before simwrby() is invoked for the first time

   The LCD reset line is toggled here if it connected to a bus port.
   (it may alternatively be hard-wired to the reset signal to the processors
   in the target system).

   The sim_reset() function is invoked automatically via the ghw_io_init() function.
*/
void sim_reset( void )
   {
   is_addressed = 0;
   curadr = 0;
   i2c_reset();
   }

#endif /* GHW_SINGLE_CHIP */
#endif /* GHW_NOHDW */


