I2C bus driver
--------------

The files in this directory create an access interface to an I2C bus.
The I2C bus is simulated by use of I/O operations.
The I/O operations are done via the RAMTEX IOTester registers using
pin high, pin low and pin 3-state operations.

I/O pin P16, P17 is used for SDA and SCL.
The driver simulates an I2C master so SCL is always driven.

GHW_SINGLE_CHIP must be defined in the compilers preprocessor setting 
to invoke these bussim function.

IOTESTER
--------
IOTESTER must be defined in the PC compiler's preprocessor setting 
to invoke the IOTester functions. 
The IOTester driver files IOTESTER.C and IOTESTER.H must be included
in the PC project.

