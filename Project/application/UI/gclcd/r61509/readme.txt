For R61509 only the low-level intitialization and configuration files 
in this directory and sub-directories are specific for the R61509
display controller family. 

The rest of the low-level drivers are common with the S6D0129 LCD 
controller and should be taken from the \gclcd\s6d0129\ directory
and subdirectories. Typically:
   ghwblkrw.c
   ghwbuf.c
   ghwfill.c
   ghwgscrl.c
   ghwinv.c
   ghwpixel.c
   ghwretgl.c
   ghwsymrd.c
   ghwsymwr.c
   gfgio\ghwioini.c

   gsimintf.c
   sim0129.c

The S6D0129 PC simulator is also used for R61509 simulation.
The SG access files are also the same for R61509
