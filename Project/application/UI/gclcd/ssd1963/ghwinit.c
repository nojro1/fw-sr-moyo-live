/************************** ghwinit.c *****************************

   Low-level driver functions for the SSD1963 LCD display controller
   initialization and error handling.

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is copied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)


   SSD1963+SSD1962+SSD1961 chip and driver support these mode combinations
        8 bit bus, 16 bit color   (With SW coversion to/from 6+6+6)
        8 bit bus, 18 bit color   6+6+6
        8 bit bus, 24 bit color   (With SW coversion to/from 6+6+6)
        16 bit bus, 16 bit color  16
        32 bit bus. 18 bit color  18
        32 bit bus. 24 bit color  24 (only SSD1963)

   Revision date:    21-10-2009
   Revision Purpose: PLL clock settings for display size is now
                     calculated automically based on an input
                     clock setting
   Revision date:    11-11-10
   Revision Purpose: ghw_rgb_to_color(..) updated to use G_RGB_TO_COLOR(r,g,b) macro.
   Revision date:    11-05-11
   Revision Purpose: Special SSD1960 settings added. PLL clock settings adjusted
                     Comments about PLL setup procedure added.
   Revision date:    15-07-12
   Revision Purpose: PLL lock timing adjusted. 
                     ghw_cmd_wait() now using 'volatile' types to prevent delay loop  
                     from being optimized away.

   Version number: 1.3
   Copyright (c) RAMTEX Engineering Aps 2009-2012

*********************************************************************/

#ifdef SGPCMODE
#include "windows.h"  /* Sleep(..) function used by ghw_cmd_wait(..)*/
#endif
#include <gdisphw.h>  /* HW driver prototypes and types */
#include <s6d0129.h>  /* Controller specific definements */
#include <stdio.h>

#define WR_RD_TEST    /* Define to include write-read-back test in ghw_init() */

#if !(defined( GHW_SSD1963 ) || defined( GHW_SSD1960))
  #error Unknown controller, Controller and bustype must be selected in gdispcfg.h
#endif


#ifdef GBASIC_INIT_ERR

/********************* Chip access definitions *********************/

#ifndef GHW_NOHDW
   #if defined( GHW_SINGLE_CHIP )
      /* User defined access types and simulated register address def */
      #include <bussim.h>
      #ifdef GHW_BUS8
        #define  sgwrby(a,d) simwrby((a),(d))
        #define  sgrdby(a)   simrdby((a))
      #elif defined( GHW_BUS32 )
        #define  sgwrdw(a,d) simwrdw((a),(d))
        #define  sgrddw(a)   simrddw((a))
      #else /* bus 16 */
        #define  sgwrwo(a,d) simwrwo((a),(d))
        #define  sgrdwo(a)   simrdwo((a))
      #endif
   #else
      /* Portable I/O functions + hardware port def */
      #include <sgio.h>
   #endif
#else
   #undef GHW_SINGLE_CHIP /* Ignore single chip mode */
#endif

/***********************************************************************/
/** All static LCD driver data is located here in this ghwinit module **/
/***********************************************************************/

/* Active foreground and background color */
GCOLOR ghw_def_foreground;
GCOLOR ghw_def_background;

#if (GHW_PALETTE_SIZE > 0)
/* Default soft palette
   The palette file can be edited directly with the ColorIconEdit program
*/
static GCODE GPALETTE_RGB FCODE ghw_palette[16] =
     #include <gcolor_4.pal>
     ;

/* Operative palette (current palette used for color lookup) */
GCOLOR ghw_palette_opr[16];
#endif

/* Use software font */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR  b[8];           /* Symbol data, fixed size = 8 bytes */
   }
GCODE FCODE sysfontsym[0x80] =
   {
   /* The default font MUST be a monospaced black & white (two-color) font */
   #include <sfs0129.sym> /* System font symbol table */
   };

/* Default system font */
GCODE GFONT FCODE SYSFONT =
   {
   6,      /* width */
   8,      /* height */
   sizeof(sysfontsym[0])-sizeof(GSYMHEAD), /* number of data bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL) sysfontsym,  /* pointer to array of SYMBOLS */
   0x80,   /* num symbols in sysfontsym[] */
   NULL    /* pointer to code page */ /* NULL means code page is not used */
   };

#ifdef GBUFFER
   #ifdef GHW_ALLOCATE_BUF
      /* <stdlib.h> is included via gdisphw.h */
      GCOLOR *gbuf = NULL;           /* Graphic buffer pointer */
      static SGBOOL gbuf_owner = 0;   /* Identify pointer ownership */
   #else
      GCOLOR gbuf[GBUFSIZE];         /* Graphic buffer */
   #endif
   GXT GFAST iltx,irbx;     /* "Dirty area" speed optimizers in buffered mode */
   GYT GFAST ilty,irby;
   SGBOOL  ghw_upddelay;    /* Flag for delayed update */
#else
   GCOLOR ghw_tmpbuf[GDISPW]; /* Row line buffer (for block read-modify-write) */
#endif /* GBUFFER */


#ifdef GHW_INTERNAL_CONTRAST
static SGUCHAR ghw_contrast;/* Current contrast value */
#endif

SGBOOL glcd_err;            /* Internal error */
#ifndef GNOCURSOR
GCURSOR ghw_cursor;         /* Current cursor state */
#endif
/* Bit mask values */
GCODE SGUCHAR FCODE sympixmsk[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

#ifdef GHW_PCSIM
/* PC simulator declaration */
void ghw_init_sim( SGUINT dispw, SGUINT disph );
void ghw_exit_sim(void);
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye);
void ghw_set_xy_sim(GXT xb, GYT yb);
void ghw_autowr_sim( GCOLOR cval );
GCOLOR ghw_autord_sim( void );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
#endif
/****************************************************************/
/** Low level interface functions used only by ghw_xxx modules **/
/****************************************************************/

/* Display control registers for control of primary drawing operations */
#define  GCTRL_H_WIN_ADR      0x2A  /* end,begin */
#define  GCTRL_V_WIN_ADR      0x2B  /* end, begin  */

#define  GCTRL_RAMWR          0x2C  /* Set or get GRAM data */
#define  GCTRL_RAMRD          0x2E  /* Set or get GRAM data */

#define  GDISP_ON             0x29
#define  GDISP_OFF            0x28

/***********  Define Bits to ease setup ******/

#ifdef GHW_LOW_VSYNC
   #define  VSPOL  0x00
#else
   #define  VSPOL  0x01
#endif

#ifdef GHW_LOW_HSYNC
   #define  HSPOL  0x00
#else
   #define  HSPOL  0x02
#endif

#ifdef GHW_LOW_FPSHIFT
   #define  CLKPOL  0x04
#else
   #define  CLKPOL  0x00
#endif

#ifdef GHW_TFT24_LCDBUS
   #define  DB24   0x20
   #define  DBTYPE 0x00
#else
   #define  DB24  0x00
   #ifdef GHW_8BIT_LCDBUS
      #define  DBTYPE 0x40
   #elif defined( GHW_8BIT_LCDBUS_D  )
      #define  DBTYPE 0x60
   #else
      /* Default is TFT18 */
      #define  DBTYPE 0x00
   #endif
#endif

/* Swap RGB,BGR modes */
#ifdef GHW_COLOR_SWAP
   #define  RGB_BIT 0x08
#else
   #define  RGB_BIT 0x00
#endif
/* Mirror horizontal scan order (ram content is the same) */
#ifdef GHW_MIRROR_HOR
   #define  MX_BIT 0x02
#else
   #define  MX_BIT 0x00
#endif
/* Mirror vertical scan order (ram content is the same) */
#ifdef GHW_MIRROR_VER
   #define  MY_BIT 0x01
#else
   #define  MY_BIT 0x00
#endif

/* Rotate adress increment orders, map logical size to physical sizes */
#ifdef GHW_ROTATED
   #define  MV_BIT 0x20
   #define  DISPH  GDISPW
   #define  DISPW  GDISPH
#else
   #define  MV_BIT 0x00
   #define  DISPH  GDISPH
   #define  DISPW  GDISPW
#endif

/****** Validate and limit display panel settings *****/

#if (GHW_HOR_NON_DISPLAY > 0x7ff)
   #undef  GHW_HOR_NON_DISPLAY
   #define GHW_HOR_NON_DISPLAY 0x7ff
#endif
#if (GHW_VER_NON_DISPLAY > 0x7ff)
   #undef  GHW_VER_NON_DISPLAY
   #define GHW_VER_NON_DISPLAY 0x7ff
#endif
#if (GHW_HOR_OFFSET > 0x7ff)
   #undef  GHW_HOR_OFFSET
   #define GHW_HOR_OFFSET 0x7ff
#endif
#if (GHW_VER_OFFSET > 0x7ff)
   #undef  GHW_VER_OFFSET
   #define GHW_VER_OFFSET 0x7ff
#endif
#if (GHW_HOR_PULSEW > 0x7f)
   #undef  GHW_HOR_PULSEW
   #define GHW_HOR_PULSEW 0x7f
#endif
#if (GHW_VER_PULSEW > 0x7f)
   #undef  GHW_VER_PULSEW
   #define GHW_VER_PULSEW 0x7f
#endif

/* Define total settings*/
#define DISPW_TOTAL (DISPW+2*GHW_HOR_NON_DISPLAY+GHW_HOR_OFFSET+GHW_HOR_PULSEW)
#define DISPH_TOTAL (DISPH+2*GHW_VER_NON_DISPLAY+GHW_VER_OFFSET+GHW_VER_PULSEW)

/* Check for total clock overflow */
#if ( DISPW_TOTAL > 0x7ff)
  #error Error horizontal display panel timing settings in gdispcfg.h creates register overflow
#endif

#if ( DISPH_TOTAL > 0x7ff)
  #error Error vertical display panel timing settings in gdispcfg.h creates register overflow
#endif

/*********************************************************************************************
   Adjustment of PLL and pixel clock circuit.

   Two goals must be fullfiled:
   1) The PLL clock multiplier must be set so the internal Voltage Controlled Oscillator (VCO)
      is configured to operate within its valid range, which is
         SSD1960   30MHz  < VCO freq. <  80MHz
         SSD1963  250MHz  < VCO freq. < 800MHz
      where VCO freq. = External Xtal freq * PLL_MULT
   2) The frame clock should be approx 70 Hz (60-90 Hz) to avoid flicking on most
      display modules.
      The required pixel clock (LSHIFT clock) is therefore
         70Hz * total_vertical_clocks * total_horizontal_clocks
      The PLL clock divisor should therefore be adjusted so the correct
      pixel clock is achieved.
      After manually selecting a PLL clock divisor then the macros below automatically
      calculates the shift clock to achieve a close approximation to the correct frame
      frequency.
      This clock configuration goal is therefore most easily achieved by selecing a PLL divisor
      which does not create divisor register overflow, and then verify the clock frequency
      with a scope or frequency counter.

***********************************************************************************************/
/* Input clock and primary PLL settings */
#ifdef GHW_SSD1960
 #define XTAL_FREQ 8000000  /* Clock input to SSD1960 in Hz */
 /* Internal VCO = (PLL_MULT+1) * XTAL_FREQ (must be in the range 30MHz  < VCO < 80MHz) */
 #define PLL_MULT 7         /* (Multiply value used = PLL_MULT+1) (adjust manually) */
 #define PLL_DIV  0         /* (Divisor value used  = PLL_DIV+1)  (adjust manually) */
 /* PLL clock = (XTAL_FREQ * (PLL_MULT+1)/ (PLL_DIV+1) */
#else
 #define XTAL_FREQ 10000000 /* Clock input to SSD196x in Hz (here 10 MHz) */
 /* Internal VCO = (PLL_MULT+1) * XTAL_FREQ (must be in the range 250MHz  < VCO < 800MHz) */
 #define PLL_MULT  45       /* (Multiply value used = PLL_MULT+1) (adjust manually) */
 #define PLL_DIV   3        /* (Divisor value used  = PLL_DIV+1)  (adjust manually) */
#endif

/* Internal PLL frequency used by pixel clock divisor */
#define PLL_FREQ  ((XTAL_FREQ*(PLL_MULT+1))/(PLL_DIV+1)) /* Ex ((10MHz*(44+1))/(2+1)) = 150 MHz */

/* Define pixel clock goal to obtain a 50-70 Hz frame freq */
#define PIXEL_CLK_GOAL ((DISPW_TOTAL*(DISPH_TOTAL+1))*70)

/* Calculate pixel clock divisior value. Result must be between 3 and 0x3ffff (262143) */
#define PLL_LSHIFT ((unsigned long)(((double)PIXEL_CLK_GOAL)*(((double)1048576)/((double)PLL_FREQ)))-1)

/********************************************/

typedef struct
   {
   SGUCHAR index;
   SGUCHAR delay;
   SGUCHAR value;
   } S1D_REGS;

#define CMD 0
#define DAT 1

static GCODE S1D_REGS FCODE as1dregs[] =
   {
   {CMD,100,0x00},    /* Wait for hardware startup completed */
   {CMD,  0,0x01},    /* Software Reset */
   {CMD,  0,0x01},
   {CMD, 10,0x01},

    /*
     Set PLL frequency = (Input clock * Multiplier) / Divisor
     Ex: (10Mhz input clock * 45)/3 = 150 Mhz PLL clock.
     (Check data sheet for VCO and PLL clock range limitations)
   */
   #ifdef GHW_SSD1960
   {CMD,  0,0xdc},
   {DAT,  0,0x03},     /* 5 - 40 MHz reference clock */
   #endif
   {CMD,  0,0xe2},
   #ifdef GHW_SSD1960
   {DAT,  0,(PLL_MULT>>8) & 0xf},  /* PLL Multiplier high (bit 11-8)*/
   {DAT,  0,(PLL_MULT & 0xff )},   /* PLL Multiplier low  (bit  7-0)*/
   {DAT,  0,PLL_DIV},  /* PLL Divisor    {31-0)  */
   #else
   {DAT,  0,PLL_MULT}, /* PLL Multiplier {255-1} (default = 45) */
   {DAT,  0,PLL_DIV }, /* PLL Divisor    {15-1)  (default = 3) */
   #endif
   {DAT,  0,0x04},    /* Effectuate PLL values */

   {CMD,  0,0xe0},
   {DAT,  100,0x01},  /* START PLL + wait */

   {CMD,  0,0xe0},
   {DAT,  0,0x03},    /* LOCK PLL */

   {CMD,  0,0xb0},    /* SET LCD MODESET TFT 18Bits MODE */
   {DAT,  0,DB24|CLKPOL|HSPOL|VSPOL/*|0x08*/},    /* SET TFT MODE & hsync+Vsync+DEN MODE */
   {DAT,  0,0x80|DBTYPE},   /* SET TFT MODE & hsync+Vsync+DEN MODE */
   {DAT,  0,((DISPW-1)>>8)},    /* SET horizontal size=640-1 HightByte */
   {DAT,  0,((DISPW-1)&0xff)},  /* SET horizontal size=640-1 LowByte */
   {DAT,  0,((DISPH-1)>>8)},    /* SET vertical size=480-1 HightByte */
   {DAT,  0,((DISPH-1)&0xff)},  /* SET vertical size=480-1 LowByte */
   {DAT,  0,0x00},    /* SET even/odd line RGB seq.=RGB */

   #ifdef GHW_BUS8
   {CMD,  0,0xf0},
   {DAT,  0,0x00},    /* SET processor bus pixel data I/F format=8bit bus, 3x6 bit data */
   {CMD,  0,0x3a},
   {DAT,  0,0x60},    /*  SET R G B format = 6 6 6 */

   #elif defined( GHW_BUS16 )
   {CMD,  0,0xf0},
   {DAT,  0,0x03},    /* SET processor bus pixel data I/F format=16 bit bus, 16bit data {5:6:5} */
   {CMD,  0,0x3a},
   {DAT,  0,0x50},    /*  SET R G B format = 565 */
   #else
   /* 32 bit bus format */
   #if (GDISPPIXW==18)
   {CMD,  0,0xf0},
   {DAT,  0,0x04},    /* SET 32 bit bus, pixel data = 18bit */
   {CMD,  0,0x3a},
   {DAT,  0,0x60},    /*  SET R G B format = 6 6 6 */

   #else
   {CMD,  0,0xf0},
   {DAT,  0,0x05},    /* SET 32 bit bus, pixel data = 24bit */
   {CMD,  0,0x3a},
   {DAT,  0,0x70},    /*  SET R G B format = 8 8 8 */

   #endif
   #endif

   {CMD,  0,0xb4},    /* SET HBP, */
   {DAT,  0,(DISPW_TOTAL>>8)},            /* SET HSYNC Total=760 */
   {DAT,  0,(DISPW_TOTAL&0xff)},
   {DAT,  0,(GHW_HOR_NON_DISPLAY>>8)},    /* Set horizontal blank period */
   {DAT,  0,(GHW_HOR_NON_DISPLAY & 0xff)},
   {DAT,  0,GHW_HOR_PULSEW},              /*  Hor Pulse width */
   {DAT,  0,(GHW_HOR_OFFSET >> 8)},       /* SET Hsync pulse start position */
   {DAT,  0,(GHW_HOR_OFFSET & 0xff)},
   {DAT,  0,0x00},    /* SET Hsync pulse subpixel start position */

   {CMD,  0,0xb6},    /* SET VBP, */
   {DAT,  0,(DISPH_TOTAL>>8)},            /* SET Vsync total */
   {DAT,  0,(DISPH_TOTAL&0xff)},
   {DAT,  0,(GHW_VER_NON_DISPLAY>>8)},    /* SET VBP=19 */
   {DAT,  0,(GHW_VER_NON_DISPLAY & 0xff)},
   {DAT,  0,GHW_VER_PULSEW},              /*  Vsync Pulse width */
   {DAT,  0,(GHW_VER_OFFSET >> 8)},       /* SET Vsync pulse start position */
   {DAT,  0,(GHW_VER_OFFSET & 0xff)},
   {DAT,  0,0x00}, /* SET Vsync pulse subpixel start position */

   {CMD,  0,0x36},     /* Set video buffer scan modes */
   {DAT,  0,  MV_BIT | RGB_BIT | MX_BIT | MY_BIT },  /* Rotation, x,y mirroring, rgb-bgr modes */
   {DAT,  0,  0 },  /* Rotation, x,y mirroring, rgb-bgr modes */

   {CMD,  0,0xe6},
   {DAT,  0,((PLL_LSHIFT>>16)& 0x0f)},    /*  Pixelclock setting, msb */
   {DAT,  0,((PLL_LSHIFT>>8) & 0xff)},
   {DAT,  0, (PLL_LSHIFT     & 0xff)},    /*  Pixel clock setting, lsb */

/*    {CMD,  0, GDISP_OFF} */
   {CMD,  0, GDISP_ON}  /*  Easier to debug when on initially */
   };

/************************ Use command interface *****************/

/*
   Send a command
*/

#ifndef GHW_NOHDW

 #ifdef GHW_BUS8
  /* Write command byte */
  #define ghw_cmd(cmd)    sgwrby(GHWCMD,(SGUCHAR)(cmd))
  /* Write command data byte */
  #define ghw_cmddat(dat) sgwrby(GHWWR, (SGUCHAR)(dat))
  #define ghw_sta()       ((SGUCHAR) sgrdby(GHWRD))
 #elif defined ( GHW_BUS16 )
  #define ghw_cmd(cmd)    sgwrwo(GHWCMDW,(SGUINT)(cmd))
  #define ghw_cmddat(dat) sgwrwo(GHWWRW, (SGUINT)(dat))
  #define ghw_sta()       ((SGUCHAR) sgrdwo(GHWRDW))
 #else  /* GHW_BUS32 */
  #define ghw_cmd(cmd)    sgwrdw(GHWCMDDW,(SGULONG)(cmd))
  #define ghw_cmddat(dat) sgwrdw(GHWWRDW, (SGULONG)(dat))
  #define ghw_sta()       ((SGUCHAR) sgrddw(GHWRDDW))
 #endif

#else
  #define ghw_cmd(cmd)    { /* Nothing */ }
  #define ghw_cmddat(dat) { /* Nothing */ }
  #define ghw_sta() 0
#endif /* GHW_NOHDW */

/*
   Set the x,y windows range.
   Internal ghw function
   Takes advantages of the index register auto increment feature
*/

void ghw_set_xyrange(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   #ifdef GHW_PCSIM
   ghw_set_xyrange_sim( xb, yb, xe, ye);
   #endif
   #ifdef GHW_ROTATED
   ghw_cmd(GCTRL_H_WIN_ADR);
   ghw_cmddat((SGUCHAR)(yb >> 8));
   ghw_cmddat((SGUCHAR)(yb & 0xff));         /* X start */
   ghw_cmddat((SGUCHAR)(ye >> 8));
   ghw_cmddat((SGUCHAR)(ye & 0xff));         /* X end */
   ghw_cmd(GCTRL_V_WIN_ADR);
   ghw_cmddat((SGUCHAR)(xb >> 8));
   ghw_cmddat((SGUCHAR)(xb & 0xff));         /* Y start */
   ghw_cmddat((SGUCHAR)(xe >> 8));
   ghw_cmddat((SGUCHAR)(xe & 0xff));         /* Y end */
   #else
   ghw_cmd(GCTRL_H_WIN_ADR);
   ghw_cmddat((SGUCHAR)(xb >> 8));
   ghw_cmddat((SGUCHAR)(xb & 0xff));         /* X start */
   ghw_cmddat((SGUCHAR)(xe >> 8));
   ghw_cmddat((SGUCHAR)(xe & 0xff));         /* X end */
   ghw_cmd(GCTRL_V_WIN_ADR);
   ghw_cmddat((SGUCHAR)(yb >> 8));
   ghw_cmddat((SGUCHAR)(yb & 0xff));         /* Y start */
   ghw_cmddat((SGUCHAR)(ye >> 8));
   ghw_cmddat((SGUCHAR)(ye & 0xff));         /* Y end */
   #endif
   /* Index register now points on memory data port register GCTRL_DATAPORT */

   /* Prepare for auto write */
   ghw_cmd(GCTRL_RAMWR);

   }

void ghw_setxypos(GXT xb, GYT yb)
   {
   #ifdef GHW_PCSIM
   ghw_set_xy_sim( xb, yb);
   #endif
   ghw_set_xyrange(xb, yb, GDISPW-1, GDISPH-1);
   }

/*
   Write databyte to controller (at current position) and increment
   internal xadr.

   Internal ghw function
*/
void ghw_auto_wr(GCOLOR dat)
   {
   #ifdef GHW_PCSIM
   ghw_autowr_sim( dat );
   #endif

   #ifndef GHW_NOHDW
   #if ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
      /* 5+6+5 format conversion  */
      sgwrby(GHWWR, (SGUCHAR)(dat>>8)&0xfc);
      sgwrby(GHWWR, (SGUCHAR)(dat>>3));
      sgwrby(GHWWR,((SGUCHAR)(dat))<<3);
   #elif ((GDISPPIXW == 18) && defined( GHW_BUS8 ))
      /* 6+6+6 format conversion  */
      sgwrby(GHWWR, (SGUCHAR)(dat>>10)); /* RRRRRRxx */
      sgwrby(GHWWR, (SGUCHAR)(dat>> 4)); /* GGGGGGxx */
      sgwrby(GHWWR,((SGUCHAR)(dat))<<2); /* BBBBBBxx */
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS8 ))
      /* 8+8+8 + format conversion  */
      sgwrby(GHWWR, (SGUCHAR)(dat>>16)); /* RRRRRRxx */
      sgwrby(GHWWR, (SGUCHAR)(dat>>8));  /* GGGGGGxx */
      sgwrby(GHWWR, (SGUCHAR)(dat));     /* BBBBBBxx */
   #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
      /* 16 bit bus mode, 16 bit color */
      sgwrwo(GHWWRW, (SGUINT) dat);
   #elif (((GDISPPIXW == 18) || (GDISPPIXW == 24)) && defined( GHW_BUS32 ))
      /* 32 bit bus mode, 18 or 24 bit color */
      sgwrdw(GHWWRDW, (SGULONG) dat);
   #else
       #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
   #endif
   #endif /* GHW_NOHDW */
   }


#if (defined(GBUFFER) || !defined( GHW_NO_LCD_READ_SUPPORT ))
/*
   Perform required dummy reads after column position setting
   Note HX8346 only required one dummy read in 8 bit mode
   although the datasheet (ver 2007-4-18) say 3 reads.
*/
void ghw_auto_rd_start(void)
   {
   ghw_cmd(GCTRL_RAMRD);
   }

/*
   Read at address set by previous (auto-increment) operation
*/
GCOLOR ghw_auto_rd(void)
   {
   #ifndef GHW_NOHDW
      #if (defined( GHW_BUS8 ) && (GDISPPIXW == 24))
         GCOLOR ret;
         /* 8 bit bus mode */
         /* 24 bit color mode (3 lsb bytes is r,g,b) */
         /* Left aligned color info */
         ret =  (((GCOLOR) sgrdby(GHWRD)) << 16);  /* MSB (RRRRRR**) */
         ret |= (((GCOLOR) sgrdby(GHWRD)) << 8);   /*     (GGGGGG**) */
         ret |=  ((GCOLOR) sgrdby(GHWRD));         /* LSB (BBBBBB**) */
         return ret;
      #elif (defined( GHW_BUS8 ) && (GDISPPIXW == 18))
         GCOLOR ret;
         /* 8 bit bus mode */
         /* 18 bit color mode  */
         /* Left aligned color info */
         ret =  (((GCOLOR) (sgrdby(GHWRD) & 0xfc)) << 10);  /* MSB (RRRRRR**) */
         ret |= (((GCOLOR) (sgrdby(GHWRD) & 0xfc)) << 4);   /*     (GGGGGG**) */
         ret |= (((GCOLOR) (sgrdby(GHWRD) & 0xfc)) >> 2);   /* LSB (BBBBBB**) */
         return ret;
      #elif (defined( GHW_BUS8 ) && (GDISPPIXW == 16))
         GCOLOR ret;
         /* 8 bit bus mode, controller returns 24 bit rgb */
         /* 16 bit color mode  RGB{5:6:5} */
         ret =  (((GCOLOR) ( sgrdby(GHWRD) & 0xf8)) << 8);  /* (RRRRR*** ********) */
         ret |= (((GCOLOR) ( sgrdby(GHWRD) & 0xfc)) << 3);  /* (*****GGG GGG*****) */
         ret |=  ((GCOLOR) ((sgrdby(GHWRD) & 0xf8)  >> 3)); /* (******** ***BBBBB) */
         return ret;
      #elif (defined( GHW_BUS16 ) && (GDISPPIXW == 16))
         return sgrdwo(GHWRDW);
      #elif (defined( GHW_BUS32 ) && (GDISPPIXW == 18))
         return sgrddw(GHWRDDW) & 0x3ffff;
      #elif (defined( GHW_BUS32 ) && (GDISPPIXW == 24))
         return sgrddw(GHWRDDW) & 0xffffff;
      #else
         #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
      #endif

   #else
     #ifdef GHW_PCSIM
      return ghw_autord_sim();
     #else
      return 0;
     #endif
   #endif /* GHW_NOHDW */
   }

#endif /* GBUFFER || !GHW_NO_LCD_READ_SUPPORT */

/**************************************************************/
/**        Initialization and error handling functions       **/
/**************************************************************/

/*
   Change default (palette) colors
*/
void ghw_setcolor(GCOLOR fore, GCOLOR back)
   {
   /* Update active colors */
   ghw_def_foreground = fore;
   ghw_def_background = back;
   }

/*
   Convert an RGB structure to a color value using the current color mode
*/
GCOLOR ghw_rgb_to_color( GCONSTP GPALETTE_RGB *palette )
   {
   if (palette == NULL)
      return 0;

   return G_RGB_TO_COLOR(palette->r,palette->g,palette->b);
   }

#if (GHW_PALETTE_SIZE > 0)
/*
   Load a new palette or update the existing palette
   (Palette is only used with symbols using 2 or 4 bits pr pixel)
*/
SGBOOL ghw_palette_wr(SGUINT start_index, SGUINT num_elements, GCONSTP GPALETTE_RGB PFCODE *palette)
   {
   if ((num_elements == 0) ||
       ((start_index + num_elements) > 16) ||
       (palette == NULL))
      {
      glcd_err = 1;
      return 1;
      }
   glcd_err = 0;

   /* (Partial) update of operative palette values */
   while(num_elements-- > 0)
      {
      /* Make local palette copy here to be compatible with compilers
         having a non-standard conforming handling of pointer
         (i.e when PFCODE memory qualifer is used) */
      GPALETTE_RGB pal;
      pal.r = palette->r;
      pal.g = palette->g;
      pal.b = palette->b;
      ghw_palette_opr[start_index++] = ghw_rgb_to_color(&pal);
      palette++;

      /* ghw_palette_opr[start_index++] = ghw_rgb_to_color(&palette++); */
      }

   return glcd_err;
   }
#endif


/*
   Fast set or clear of LCD module RAM buffer
   Internal ghw function
*/
static void ghw_bufset(GCOLOR color)
   {
   /* Use hardware accelerator logic */
   GXT x; GYT y;
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);

   for (y=0; y < GDISPH; y++)
      {
      for(x=0; x < GDISPW; x++)
         {
         ghw_auto_wr(color);  /* Set LCD buffer */
         #ifdef GBUFFER
         gbuf[x+y*GDISPW] = color; /* Set ram buffer as well */
         #endif
         }
      }
   }

#ifdef WR_RD_TEST
/*
   Make write-readback test on controller memory.

   This test returns ok (== 0) when the write-readback test succeded. This indicates that
   the processor / display hardware interface / library configuration combination is
   working ok.

   This test will fail if some databus or control signals is not connected correctly.

   This test will fail if 16/8 bit bus mode selection and color resolution settings
   in the library configuration file does not match the actual bus and color resolution
   configuration for the hardware selected via chip pins. (ex display and processor
   16/8 bit bus width, 8080/6800 bus type settings, word / byte address offsets, etc).

   This test may fail if illegal GCTRLW, GCTRLH, GHW_XOFFSET, GHW_YOFFSET
   configuration settings cause overrun of the on-chip video RAM.

   This test can be exectuted correctly with only logic power on the display controller.

   Return 0 if no error,
   Return != 0 if some readback error is detected (the bit pattern may give information
   about connector pins in error)
*/
static GCOLOR ghw_wr_rd_test(void)
   {
   #ifndef GHW_NOHDW
   int i;
   GCOLOR msk,result;
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);

   #if (GDISPPIXW > 16)
   /* 24 (18) bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%06lx ", (unsigned long) msk);*/
      ghw_auto_wr(~msk);
      /*printf(" 0x%06lx\n", (unsigned long) (~msk));*/
      msk <<= 1;
      }
   /*printf("\n");*/

   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   ghw_auto_rd_start();
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val1,val2;
      val1 = ghw_auto_rd();
      val2 = ghw_auto_rd();
      result |= (val1 ^ msk);
      result |= (val2 ^ (~msk));
      /*printf("0x%06lx ",  (unsigned long) val1);*/
      /*printf(" 0x%06lx\n", (unsigned long) val2);*/
      msk <<= 1;
      }
   result &= GHW_COLOR_CMP_MSK;  /* Mask bits unused by controller */

   #else
   /* 16 bit color mode */
   for (i = 0, msk = 1; i < GDISPPIXW; i++)
      {
      ghw_auto_wr(msk);
      /*printf("0x%04x ", (unsigned short) msk);*/
      ghw_auto_wr(~msk);
      /*printf(" 0x%04x\n", (unsigned short) (~msk & 0xffff));*/
      msk <<= 1;
      }
   /* printf("\n");*/
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   ghw_auto_rd_start();
   for (i=0, msk=1, result=0; i < GDISPPIXW; i++)
      {
      GCOLOR val;
      val = ghw_auto_rd();
      result |= (val ^ msk);
      /* printf("0x%04x ",  (unsigned short) val); */
      val = ghw_auto_rd();
      /*printf(" 0x%04x\n", (unsigned short) val ); */
      result |= (val ^ (~msk));
      msk <<= 1;
      }
   #endif
   return result;  /* 0 = Nul errors */
   #else
   return 0;       /* 0 = Nul errors */
   #endif
   }

#endif /* WR_RD_TEST */

/*
   Wait a number of milli seconds
*/
static void ghw_cmd_wait(SGUCHAR ms)
   {
   #ifdef SGPCMODE
   Sleep(ms); /* delay >= ms */
   #else
   volatile SGUINT wait1ms; /* 'volatile' used to prevent compiler optimizers from removing the loop below */
   while (ms-- != 0)
      {
      wait1ms = 2000;       /* Adjust to achieve a 1 ms loop below */
      while( wait1ms != 0)
         wait1ms--;
      }
   #endif
   }

/*
   Initialize display, clear ram  (low-level)
   Clears glcd_err status before init

   Return 0 if no error,
   Return != 0 if some error
*/
SGBOOL ghw_init(void)
   {
   #ifndef GHW_NOHDW
   short i;
   #endif

   #ifdef GBUFFER
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;
   ghw_upddelay = 0;
   #endif

   glcd_err = 0;
   ghw_io_init(); /* Set any hardware interface lines, controller hardware reset */

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL)
      {
      /* Allocate graphic ram buffer */
      if ((gbuf = (GCOLOR *)calloc(ghw_gbufsize(),1)) == NULL)
         glcd_err = 1;
      else
         gbuf_owner = 1;
      }
   #endif

   if (glcd_err != 0)
      return 1;

   #ifdef GHW_PCSIM
   /* Tell simulator about the visual LCD screen organization */
   ghw_init_sim( GDISPW, GDISPH );
   #endif
   /* Set default colors */
   ghw_setcolor( GHW_PALETTE_FOREGROUND, GHW_PALETTE_BACKGROUND );

   #if (GHW_PALETTE_SIZE > 0)
   /* Load palette */
   ghw_palette_wr(0, sizeof(ghw_palette)/sizeof(GPALETTE_RGB), (GCONSTP GPALETTE_RGB PFCODE *)&ghw_palette[0]);
   #endif

   /* Debug printout of result of clock settings */
   /*
   {
   unsigned long tmp;
   printf( "\nHorizontal total setting: (hpw+hfp+width +hbp) = %lu", DISPW_TOTAL);
   printf( "\nVertical total setting:   (vpw+vfp+height+vbp) = %lu", (DISPH_TOTAL+1));
   printf( "\nPixel clock goal = %lu", PIXEL_CLK_GOAL);
   printf( "\nXtal frequency = %lu", XTAL_FREQ);
   printf( "\nVCO multiply   = %lu", (PLL_MULT+1));
   printf( "\nVCO frequency = %lu", (XTAL_FREQ*(PLL_MULT+1)));
   printf( "\nVCO divider   = %lu", (PLL_DIV+1));
   printf( "\nPLL frequency = %lu", PLL_FREQ);
   if (((SGULONG)XTAL_FREQ*(PLL_MULT+1)) < 30000000L)  printf( " too low");
   if (((SGULONG)XTAL_FREQ*(PLL_MULT+1)) > 80000000L)  printf( " too high");
   printf( "\nClock divisor %lu = 0x%lx", (PLL_LSHIFT+1), (PLL_LSHIFT+1));
   if (((SGULONG)(PLL_LSHIFT+1)) > 0xfffff)  printf( " too high");
   tmp = (SGULONG)(((double)PLL_FREQ)*(((double)(PLL_LSHIFT+1))/(double)1048576));
   printf( "\nClock frequency %lu", tmp);
   printf( "\nFrame frequency %lu", (tmp/(DISPH_TOTAL+1))/DISPW_TOTAL);
   }
   */

   #ifndef GHW_NOHDW
   /* Initialize registers */
   for (i=0; i < sizeof(as1dregs)/sizeof(S1D_REGS); i++)
      {
      if (as1dregs[i].index == CMD)
         {
         ghw_cmd(as1dregs[i].value);
         /*printf("\nCMD 0x%02x ", as1dregs[i].value );*/
         }
      else
         {
         ghw_cmddat(as1dregs[i].value);
         /*printf("\n 0x%02x ", as1dregs[i].value );*/
         }
      if (as1dregs[i].delay != 0)
         ghw_cmd_wait( as1dregs[i].delay );
      }
   /*
      Stimuli test loops for initial oscilloscope test of display interface bus signals
      Uncomment to use the test loop for the given data bus width.
      It is recommended to check all display bus signals with each of the I/O access
      statements in the loop one by one.
   */
   /*
   #ifdef GHW_BUS8
   for(;;)
      {
      SGUCHAR dat;
      sgwrby(GHWCMD,0xff);
      sgwrby(GHWWR,0x00);
      dat = sgrdby(GHWSTA);
      dat = sgrdby(GHWRD);
      }
   #endif
   #ifdef GHW_BUS16
   for(;;)
      {
      SGUINT dat;
      sgwrwo(GHWCMDW,0xffff);
      sgwrwo(GHWWRW,0x0000);
      dat = sgrdwo(GHWSTAW);
      dat = sgrdwo(GHWRDW);
      }
   #endif
   */

   #if (defined( WR_RD_TEST ) && !defined(GHW_NO_LCD_READ_SUPPORT))
   /*
      NOTE:
      The call of ghw_wr_rd_test() should be commented out in serial mode.
      In serial mode the display controller  does not provide read-back facility
      and this test will always fail.
   */
   if (ghw_wr_rd_test() != ((GCOLOR) 0))
      {
      /* Controller memory write-readback error detected
      (Check the cable or power connections to the display) */
      G_WARNING("Hardware interface error\nCheck display connections\n");  /* Test Warning message output */
      glcd_err = 1;
      return 1;
      }
   #endif

   #endif  /* !GHW_NOHDW */

   ghw_dispon();  /* Easier to debuf if on before buffer clear */

   /*ghw_bufset( G_RED );*/    /* Screen full of red. For initial debug only */
   /*ghw_bufset( G_GREEN );*/  /* Screen full of green. For initial debug only */
   /*ghw_bufset( G_BLUE );*/   /* Screen full of blue. For initial debug only */
   ghw_bufset( ghw_def_background );

   #ifndef GNOCURSOR
   ghw_cursor = GCURSIZE1;    /* Cursor is off initially */
   /* ghw_cursor = GCURSIZE1 | GCURON; */ /* Uncomment to set cursor on initially */
   #endif

   ghw_dispon();  /* Normal operation is on after initialization */

   ghw_updatehw();  /* Flush to display hdw or simulator */

   return (glcd_err != 0) ? 1 : 0;
   }


/*
   Return last error state. Called from applications to
   check for LCD HW or internal errors.
   The error state is reset by ghw_init and all high_level
   LCD functions.

   Return == 0 : No errors
   Return != 0 : Some errors
*/
SGUCHAR ghw_err(void)
   {
   #if (defined(_WIN32) && defined( GHW_PCSIM))
   if (GSimError())
      return 1;
   #endif
   return (glcd_err == 0) ? 0 : 1;
   }

/*
   Display a (fatal) error message.
   The LCD display module is always cleared and initialized to
   the system font in advance.
   The error message is automatically centered on the screen
   and any \n characters in the string is processed.

   str = ASCII string to write at display center
*/
void ghw_puterr( PGCSTR str )
   {
   PGCSTR idx;
   SGUINT xcnt;
   GXT xp;
   GYT yp,h,y, sidx;
   PGSYMBYTE psym;
   GCOLOR pval;
   SGUCHAR val;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   #endif

   if (ghw_init() != 0)  /* (Re-) initialize display */
      return;            /* Some initialization error */

   /* Count number of lines in string */
   idx=str;
   if (idx == NULL)
      return;
   xcnt = 1;
   while(*idx)
      {
      if (*(idx++) == '\n')
         xcnt++;
      }

   /* Set start character line */
   h = SYSFONT.symheight;
   yp = (xcnt*h > GDISPH) ? 0 : ((GDISPH-1)-xcnt*h)/2;
   /* Set character height in pixel lines */

   idx=str;
   do
      {
      xcnt=0;  /* Set start x position so line is centered */
      while ((idx[xcnt]!=0) && (idx[xcnt]!='\n') && (xcnt < GDISPBW))
         {
         xcnt++;
         }

      /* Calculate start position for centered line */
      xp = (GDISPW-xcnt*SYSFONT.symwidth)/2;

      /* Display text line */
      while (xcnt-- > 0)
         {
         /* Point to graphic content for character symbol */
         psym = &(sysfontsym[(*idx) & 0x7f].b[0]);
         ghw_set_xyrange(xp,yp,xp+SYSFONT.symwidth-1,yp+(h-1));

         /* Display rows in symbol */
         for (y = 0; y < h; y++)
            {
            /* Get symbol row value */
            val = *psym++;
            /* Initiate LCD controller address pointer */
            #ifdef GBUFFER
            gbufidx = GINDEX(xp, (GBUFINT)yp+y );
            #endif

            /* Display colums in symbol row */
            for (sidx = 0; sidx < SYSFONT.symwidth; sidx++)
               {
               if ((val & sympixmsk[sidx]) != 0)
                  pval = ghw_def_foreground;
               else
                  pval = ghw_def_background;

               /* End of symbol or end of byte reached */
               #ifdef GBUFFER
               gbuf[gbufidx++] = pval;
               #endif
               ghw_auto_wr(pval);
               }
            }

         idx++;
         xp += SYSFONT.symwidth; /* Move to next symbol in line */
         }

      /* Next text line */
      yp += h;
      if (*idx == '\n')
         idx++;
      }
   while ((*idx != 0) && (yp < GDISPH));

   ghw_updatehw();  /* Flush to display hdw or simulator */
   }

void ghw_exit(void)
   {
   #if defined( GHW_ALLOCATE_BUF)
   if (gbuf != NULL)
      {
      if (gbuf_owner != 0)
         {
         /* Buffer is allocated by ginit, so release graphic buffer here */
         free(gbuf);
         gbuf_owner = 0;
         }
      gbuf = NULL;
      }
   #endif
   ghw_io_exit();         /* Release any LCD hardware resources, if required */
   #ifdef GHW_PCSIM
   ghw_exit_sim(); /* Release simulator resources */
   #endif
   }

#ifndef GNOCURSOR
/*
   Replace cursor type data (there is no HW cursor support in controller)
*/
void ghw_setcursor( GCURSOR type)
   {
   ghw_cursor = type;
   #ifdef GHW_ALLOCATE_BUF
   if (gbuf == NULL)
      glcd_err = 1;
   #endif
   }
#endif


/*
   Turn display off
*/
void ghw_dispoff(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif

   ghw_cmd(GDISP_OFF);
   ghw_cmd(0x10);     /* Enter sleep mode ( assuming GPIO0 turns power off) */
   ghw_cmd_wait( 5 ); /* No new commands before after 5 ms*/
   }

/*
   Turn display on
*/
void ghw_dispon(void)
   {
   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif

   ghw_cmd(0x11); /* Exit sleep mode ( assuming GPIO0 turns power on ) */
   ghw_cmd_wait( 120 ); /* Wait for power up */
   ghw_cmd(GDISP_ON);
   }

#if defined( GHW_ALLOCATE_BUF)
/*
   Size of buffer requied to save the whole screen state
*/
GBUFINT ghw_gbufsize( void )
   {
   return (GBUFINT) GBUFSIZE * sizeof(GCOLOR) + (GBUFINT) sizeof(GHW_STATE);
   }

#ifdef GSCREENS
/*
   Check if screen buf owns the screen ressources.
*/
SGUCHAR ghw_is_owner( SGUCHAR *buf )
   {
   return (((GCOLOR *)buf == gbuf) && (gbuf != NULL)) ? 1 : 0;
   }

/*
   Save the current state to the screen buffer
*/
SGUCHAR *ghw_save_state( SGUCHAR *buf )
   {
   GHW_STATE *ps;
   if (!ghw_is_owner(buf))
      return NULL;

   ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);
   ps->upddelay = (ghw_upddelay != 0);
   #ifndef GNOCURSOR
   ps->cursor = ghw_cursor;
   #endif
   ps->foreground = ghw_def_foreground; /* Palette may vary, save it */
   ps->background = ghw_def_background;
   return (SGUCHAR *) gbuf;
   }

/*
   Set state to buf.
   If buffer has not been initiated by to a screen before, only
   the pointer is updated. Otherwise the the buffer
*/
void ghw_set_state(SGUCHAR *buf, SGUCHAR doinit)
   {
   if (gbuf != NULL)
      {
      /* The LCD controller has been initiated before */
      if (gbuf_owner != 0)
         {
         /* Buffer was allocated by ginit, free it so screen can be used instead*/
         free(gbuf);
         gbuf_owner = 0;
         gbuf = NULL;
         }
      }

   if (doinit != 0)
      {
      /* First screen initialization, just set buffer pointer and
         leave rest of initialization to a later call of ghw_init() */
      gbuf = (GCOLOR *) buf;
      gbuf_owner = 0;
      }
   else
      {
      if ((gbuf = (GCOLOR *) buf) != NULL)
         {
         GHW_STATE *ps;
         ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);

         #ifndef GNOCURSOR
         ghw_cursor = ps->cursor;
         #endif
         ghw_upddelay = 0;        /* Force update of whole screen */
         iltx = 0;
         ilty = 0;
         irbx = GDISPW-1;
         irby = GDISPH-1;
         ghw_updatehw();
         ghw_upddelay = (ps->upddelay != 0) ? 1 : 0;
         /* Restore drawing color */
         ghw_setcolor(ps->foreground, ps->background);
         }
      }
   }
#endif  /* GSCREENS */
#endif  /* GHW_ALLOCATE_BUF */

#endif /* GBASIC_INIT_ERR */



