/*
   VIRTUAL FONT DRIVER INTERFACE
   -----------------------------
   Internal library low-level drivers for codepage and symbols stored
   in virtual (external) memory.

   External virtual memory is only accessed via getvmem(..) function
   which must be implemented by the library user. The prototype for
   getvmem(..) are located in the getvmem.h header file.

   Local buffering and load position flags are used in this implementation
   to minimize the number of fetches from external memory.

   All knowledge about details for the font symbol data and codepage data storage
   layout is isolated in this module.

   NOTE:
   The data storage format used by this module must correspond with the genvf.exe
   generated formats, so do NOT make any code changes unless you are absolutely shure
   what you are doing.

   Revision date:     17-04-2009
   Revision Purpose:  Virtual font structure split in a RAM dependent and a constant
                      data structure
                      (to handle compilers using non-standard C conformant pointers)

   Revision date:     15-11-12
   Revision Purpose:  Support for dynamic (named) font lookup added (GVIRTUAL_FONTS_DYN parts)

   Revision date:     20-12-12
   Revision Purpose:  Support for virtual "file" storage (GVIRTUAL_FILES parts) added

   Version number: 1.3
   Copyright (c) RAMTEX International ApS 2012
*/
#include <gi_disp.h>
#include <gvfont.h>

#ifdef GVIRTUAL_FONTS
/*****************************************************************/
/**           Font codepage and symbol lookup                   **/
/*****************************************************************/

static GFONTBUFIDX vfsym_base;
/* Vsymbol header, variable (RAM) part */
static GSYMHEADV_V vsymbol_v;
/* Vsymbol header, constant (ROM) part */
static GCODE GSYMHEADV FCODE vsymbol =
   {
   0,0,1, /* Virtual font type identifier */
   (PGSYMHEADV_V) &vsymbol_v /* Pointer to variable part*/
   };

static GCP_RANGE vcp_range;
static GFONTBUFIDX vfcodepage_last = (((GFONTBUFIDX)(-1)) & ~0x1);
static GFONTBUFIDX vfcodepage_base = 0;

/* Data type sizes in virtual memory */
#define GWCHAR_VSIZE    2
#define GXT_VSIZE       2
#define GCP_RANGE_VSIZE (GWCHAR_VSIZE*3)

/*
   Prepare for fast codepage and symbol data lookup (if needed)
   Init and preload a GSYMHEADV structure for the font related parameters
   (Default to first symbol in font)
*/

static void *gi_vfbufowner = NULL;
#ifdef GVIRTUAL_FONTS_DYN
extern GFONTVDYN *gvf_curfont;
#endif

void gi_fontv_open(PGFONTV fp)
   {
   if ((gi_vfbufowner == NULL) || ((void *)(fp) != gi_vfbufowner)) /* Is not loaded already ? */
      { /* New font, prepare for load */
      gi_vfbufowner = ((void *)(fp));;
      #ifdef GVIRTUAL_FONTS_DYN
      if (gisfontv_named(fp))
         { /* Use data from current cashed font */
         vfcodepage_base = gvf_curfont->si_codepage;
         /* Init local vsymbol structure */
         vfsym_base = gvf_curfont->si_symbols;

         vsymbol_v.numbits = gvf_curfont->numbits;
         vsymbol_v.cxpix = gvf_curfont->symwidth;
         vsymbol_v.cypix = gvf_curfont->symheight;
         vsymbol_v.device_id = gvf_curfont->device_id;
         vsymbol_v.symsize = gvf_curfont->symsize+GXT_VSIZE; /* use symbol storage size to */
         }
      else
      #endif
         {
         /* Use data directly from vf (ROM) structure */
         if (fp->pcodepage != NULL)
            vfcodepage_base = fp->pcodepage->si_codepage;
         else
            vfcodepage_base = 0;

         /* Init local vsymbol structure */
         vfsym_base = fp->si_symbols;

         vsymbol_v.numbits = fp->numbits;
         vsymbol_v.cxpix = fp->symwidth;
         vsymbol_v.cypix = fp->symheight;
         vsymbol_v.device_id = fp->device_id;
         vsymbol_v.symsize = fp->symsize+GXT_VSIZE;
         }

      /* Prepare for codepage and symbol reinit */
      vfcodepage_last = (((GFONTBUFIDX)(-1)) & ~0x1);
      vsymbol_v.bidx = vfsym_base;
      vsymbol_v.symbol_font = (void *) fp;
      }
   }

/*
   Return pointer to codepage range for virtual font
   gi_fontv_open(..) must have been called in advance
*/
PGCP_RANGE_V gi_fontv_cp( GWCHAR index )
   {
   GFONTBUFIDX vfcodepage;
   SGUCHAR vfbuf[GCP_RANGE_VSIZE];
   vfcodepage = vfcodepage_base + ((GFONTBUFIDX)index)*GCP_RANGE_VSIZE;
   if (vfcodepage != vfcodepage_last)
      {
      /* New range, load it */
      getvmem(vsymbol_v.device_id, vfbuf, vfcodepage, GCP_RANGE_VSIZE);
      /* Solve any endian differences between platforms */
      vcp_range.min = (GWCHAR)VF16(vfbuf,0);
      vcp_range.max = (GWCHAR)VF16(vfbuf,2);
      vcp_range.idx = (GWCHAR)VF16(vfbuf,4);
      vfcodepage_last = vfcodepage;
      }
   return (PGCP_RANGE_V)(&vcp_range);
   }

/*
   Return pointer to virtual font symbol header for symbol equal to index
   gi_fontv_open(..) must have been called in advance
*/
PGSYMBOL gi_fontv_sym( GWCHAR index )
   {
   GFONTBUFIDX vfsym;
   SGUCHAR vfbuf[GXT_VSIZE];
   vfsym = vfsym_base + ((GFONTBUFIDX)index)*((GFONTBUFIDX)vsymbol_v.symsize);
   if ( (vfsym+GXT_VSIZE) != vsymbol_v.bidx )
      {
      /* Width not loaded, Get active symbol width (assuming a proportional font) */
      getvmem(vsymbol_v.device_id, vfbuf, vfsym, GXT_VSIZE);
      /* Solve any endian differences between platforms */
      vsymbol_v.cxpix = (GXT)VF16(vfbuf,0);
      vsymbol_v.bidx = vfsym+GXT_VSIZE;
      }
   return (PGSYMBOL) (&vsymbol);
   }

#endif /* GVIRTUAL_FONTS */

#if defined( GVIRTUAL_FONTS) || defined( GVIRTUAL_FILES )

/*****************************************************************/
/**     Font data fetch and cashe buffer handling               **/
/*****************************************************************/

/*************** Virtual Font data load functions *****************/

/*
   Define size of temporary load buffer (for optimum font load performance make it large enough
   to hold the symbol data size of a symbol in the most frequently used fonts)
   Minimum allowed value for VFBUFSIZE is 4 bytes
   Optimal minimum when GVIRTUAL_FONTS_DYN is used is 38 bytes.
   Note: Must be an even number to assure word alignement for binary font copy
*/
#define VFBUFSIZE 38  /* >= 38 (fastest named font opening, a complete font structure is loaded in one operation) */

/* Temporary buffer for virtual font handling */
static SGUCHAR vfbuf[VFBUFSIZE];

/***************** Symbol load controls ************/
static GFONTDEVICE vfdevice;   /* Device ID */
static GFONTBUFIDX vfbufstart; /* Current absolute start of cashe buffer */
static GFONTBUFIDX vfbufend;   /* Current absolute end of cashe buffer */
static GFONTBUFIDX vfsymstart; /* Absolute start of vf object */
static GFONTBUFIDX vfsymsize;  /* Total size of vf object */

/********************* Block data fetch ********************/

#ifndef GBUFINT
    /* Map type (enable use of Vitual Files without GUI lib) */
   #define GBUFINT GFONTBUFIDX
#endif


/*
   "Open" a data block and read the first part to cashe buffer.
   The blocklength parameter defines the maximum block index
   The bufoffset parameter defines the offset within the object for first load of data
*/
static void gi_vblock_open( GFONTDEVICE vf_device, GFONTBUFIDX startidx, SGUINT length)
   {
   vfdevice = vf_device;    /* Device ID passed to low level memory device driver */
   vfbufstart = startidx;   /* First buffer load start at object boundary */
   vfsymstart = startidx;   /* Start of object */
   vfsymsize = length;      /* Object length (adjust max cashe load) */
   if (length > VFBUFSIZE)
      length = VFBUFSIZE;   /* Reduce first block load to buffer size */
   vfbufend = startidx + (length-1);
   getvmem(vfdevice, vfbuf, vfbufstart, (SGUINT) length);
   }

/*
   Symbol data fetch
   gi_vblock_open(..) or gi_symv_open(..) must have been called first
   This function is called from low-level drivers.
*/
SGUCHAR gi_symv_by(GBUFINT symdatidx)
   {
   GFONTBUFIDX sidx = vfsymstart+symdatidx;
   if ((sidx < vfbufstart) || (sidx > vfbufend))
      {
      /* Symbol data index is outside temp buffer,
         fetch new data from external memory */
      vfbufstart = sidx;
      vfbufend = vfbufstart + (VFBUFSIZE-1);
      /* Optimize read size */
      if (vfbufend >= vfsymstart+vfsymsize)
         vfbufend = vfsymstart+vfsymsize-1;
      /* Fetch new part of symbol */
      getvmem(vfdevice, vfbuf, vfbufstart, (SGUINT) ((SGULONG)vfbufend-vfbufstart+1));
      }
   return vfbuf[sidx - vfbufstart];
   }

#endif /* GVIRTUAL_FONTS || GVIRTUAL_FILES */

#ifdef GVIRTUAL_FONTS
/*
   Symbol load preset function
   Preset symbol load information and do first symbol load.
   (called before low-level drivers are activated)
*/
void gi_symv_open( PGSYMBOL psymbol, SGUINT bw, GYT offset)
   {
   PGSYMHEADV_V psymv;
   psymv = ((PGSYMHEADV) psymbol)->psymh_v; /* Pointer to variable part */
   gi_vblock_open( psymv->device_id,
       psymv->bidx+(GFONTBUFIDX)bw*offset,         /* Adjust symbol start offset */
       (GFONTBUFIDX)(psymv->cypix - offset)*bw); /* Reduce symbol size */
   }
#endif /* GVIRTUAL_FONTS */

#if defined( GVIRTUAL_FONTS_DYN) || defined( GVIRTUAL_FILES )
/**************************************************************************
**   Functions for dynamic (named) font lookup, load and handling        **
**************************************************************************/


/*
  Fetch 8 bit block via virtual font cashe buffer.
  Returns 0 in case of index is past end of block.
*/
#ifdef GDATACHECK
static SGUCHAR gvfc_rd8(GBUFINT blockidx)
   {
   if (blockidx >= vfsymsize)
      return 0; /* block overflow / illegal index */
   return gi_symv_by(blockidx);
   }
#else
/* Map directly to byte load function */
#define gvfc_rd8(blockidx) gi_symv_by(blockidx)
#endif

/******************************************************************

   DYNAMIC SEEK OF VIRTUAL FONT
   ----------------------------
   Internal library low-level driver for dynamic search of named blocks
   stored in a virtual font image file. Used for dynamic font lookup seek.

*******************************************************************/

/* Get virtual font storage type */

#define VFBLK_HDRSIZE      6
/* Internal block ID types reserved to name search */
#define VFONTDIR           0x0100
#define VFONTDIRLINK       0x0200
#define VF_BINBLK_DIR      0x0500

/*
   Seek for directory block.

   All knowledge about the *.bin block structures are isolated to this function.

      nameindextablesize
      nameindextable[]              // Sorted name list
      {namesize, name, link}

*/
SGUCHAR gi_dirblk_seek(G_SEEK_DIRBLK *dir, GFONTDEVICE device_id, SGUINT dirtype)
   {
   SGUINT  type;
   SGUCHAR buf[8];

   /* Walk bin file blocks to locate directory block(s) */
   for (;;)
      {
      /* Load block header (length plus type) */
      getvmem(device_id,&buf[0],dir->binidx,VFBLK_HDRSIZE);
      if ((dir->binblksize = VF32(buf,0)) < VFBLK_HDRSIZE)
         return 1; /* Not found before end of bin image */
      type = VF32(buf,4);
      if (type == VFONTDIRLINK)
         {
         /* Directory link block found. Load jump index to (next) directory */
         SGULONG dirlink;
         if (dir->binblksize < VFBLK_HDRSIZE+4)
            return 1; /* Illegal link block data size */
         getvmem(device_id,&buf[0],dir->binidx+VFBLK_HDRSIZE,4);
         if ((dirlink = VF32(buf,0)) == 0)
            return 1;           /* No directory in .bin image */
         dir->binidx = dirlink; /* Jump to load directory header next */
         continue;
         }
      if (type == dirtype)
         {
         dir->blkstart = dir->binidx+VFBLK_HDRSIZE;
         getvmem(device_id, &buf[0], dir->blkstart, 2);
         dir->numelem = VF16(buf,0); /* Number of elements in dir name array */
         return 0;                   /* Found */
         }
      dir->binidx += dir->binblksize;  /* Start of next block */
      }
   }

/*
   Seek for named (font) block.

   The name length must be less than 255 bytes
   Returns vf image index to start of block data
   If blocksize != NULL the value is updated with the block data length
   If blocktype != NULL the value is updated with the block type value

   Knowledge about the *.bin directory and font lookup are isolated to this function.
*/
static SGULONG gi_vfont_seek(PGCSTR name, GFONTDEVICE device_id,
                             SGULONG *blocksize, SGUINT *blocktype, SGUINT dirtype)
   {
   G_SEEK_DIRBLK dir;
   SGUINT  half, min, size;
   SGUINT  namesize, i;
   SGUCHAR buf[8];

   /* Get / check size of name ID */
   if (name == NULL) return 0;
   for (namesize = 0; ((name[namesize] != 0) && (namesize < 256)); namesize++);
   if ((namesize == 0) || (namesize > 255)) return 0;

   for(dir.binidx = 0;;)
      {
      if (gi_dirblk_seek(&dir,device_id,dirtype))
         return 0; /* Directory block of specified type not found */

      /* Loop searching for a matching file name (using fast binary search) */
      for(min=0;;)
         {
         SGULONG blkoffset;
         SGINT   result;
         half = dir.numelem>>1;  /* check element at center of (sub) range */
         /* Get offsets and size */
         getvmem(device_id,&buf[0],dir.blkstart+2+((min+half)*8),8);
         blkoffset = VF24(buf,0);   /* name offset is stored as a 3 byte value */
         size = ((SGUINT)buf[3])+1; /* Stored file name length (max is 255) + terminating 0 */
         /* Use cashe to load file name */
         gi_vblock_open(device_id,dir.blkstart+blkoffset,size);
         for (i=0;i<size;i++)   /* Compare names */
            {
            if ((result = (SGINT)((SGUINT)name[i]) - (SGINT)((SGUINT)gvfc_rd8(i))) != 0)
               break; /* Not equal */
            }
         if (result != 0)
            {
            if (half != 0)
               {  /* define next sub range */
               if (result < 0)
                  dir.numelem = half;    /* too high, continue in lower half */
               else
                  {
                  min = min+half;        /* too low, continue in upper half */
                  dir.numelem = dir.numelem - half;
                  }
               if (dir.numelem != 0)     /* subrange exist ? */
                  continue;              /* Check next name */
               }
            return 0; /* Name not found in directory block */
            }

         /* Found */
         dir.binidx = VF32(buf,4); /* start of named block header */
         if ((blocksize != NULL) || (blocktype != NULL))
            {
            /* Fetch block size and type info */
            getvmem(device_id,&buf[0],dir.binidx,VFBLK_HDRSIZE);
            if ((blkoffset = VF32(buf,0)) < VFBLK_HDRSIZE)
               return 0; /* Corrupted file or End of .bin dir */
            if (blocksize != NULL) *blocksize = blkoffset-VFBLK_HDRSIZE; /* Size of block data */
            if (blocktype != NULL) *blocktype = VF16(buf,4);             /* Type of block data */
            }
         /* Return bin index to start of block data */
         return dir.binidx + VFBLK_HDRSIZE;
         }
      }
   }

#endif /* GVIRTUAL_FONTS_DYN || GVIRTUAL_FILES */

#ifdef GVIRTUAL_FONTS_DYN
/*
  Fetch 16 bit block (2 bytes) via virtual font cashe buffer.
  The fetching is in accordance with the byte endian settings used by genvf.exe
  Returns 0 in case of index is past end of block (if GDATACHECK is defined)
*/
static SGUINT gvfc_rd16(GBUFINT blockidx)
   {
   GFONTBUFIDX sidx;
   #ifdef GDATACHECK
   if (( GFONTBUFIDX)(blockidx+1) >= vfsymsize)
      return 0; /* block overflow / illegal index */
   #endif
   sidx = vfsymstart+blockidx;
   #if (VFBUFSIZE < 38)  /* .bin font structure larger than cashe */
   if ((sidx < vfbufstart) || (sidx+2 > vfbufend))
      { /* (Some part) outside cashe buffer, use special handling */
      SGUCHAR buf[2];
      buf[0] = gi_symv_by(blockidx);
      buf[1] = gi_symv_by(blockidx+1);
      return VF16(buf,0);
      }
   else
   #endif
      {
      /* Inside cashe, fetch directly */
      register SGUCHAR *p;
      p = &vfbuf[sidx - vfbufstart];
      return VF16(p,0);
      }
   }

/*
   Fetch 32 bit block (4 bytes) via virtual font cashe buffer.
   The fetching is in accordance with the byte endian settings used by genvf.exe
   Returns 0 in case of index is past end of block (if GDATACHECK is defined)
*/
static SGULONG gvfc_rd32(GBUFINT blockidx)
   {
   GFONTBUFIDX sidx;
   #ifdef GDATACHECK
   if (( GFONTBUFIDX)(blockidx+3) >= vfsymsize)
      return 0; /* block overflow / illegal index */
   #endif
   sidx = vfsymstart+blockidx;
   #if (VFBUFSIZE < 38)  /* .bin font structure larger than cashe */
   if ((sidx < vfbufstart) || (sidx+4 > vfbufend))
      { /* (Some part) outside cashe buffer, use special handling */
      SGUCHAR buf[4];
      buf[0] = gi_symv_by(blockidx);
      buf[1] = gi_symv_by(blockidx+1);
      buf[2] = gi_symv_by(blockidx+2);
      buf[3] = gi_symv_by(blockidx+3);
      return VF32(buf,0);
      }
   else
   #endif
      {
      /* Inside cashe, fetch directly */
      register SGUCHAR *p;
      p = &vfbuf[sidx - vfbufstart];
      return VF32(p,0); /* Little endian */
      }
   }

/*********************************************************************************

   DYNAMIC LOAD OF VIRTUAL FONTS
   -----------------------------
   SGUCHAR gi_vfont_load( GFONTVDYN *pvfont, PGCSTR fontname, GFONTDEVICE vf_device)

   Internal function for dynamic "opening" of a dynamic font in a virtual font image.
   GFONTVDYN structure is initialized from the virtual font image file,

   All font and code page structure information is completely initialized via the
   stored virtual font image.
   This enable runtime maintenance and update of the virtual font image.

   All knowledge about the *.bin vfont structure layout is isolated to this function.

   Return 0 if no errors
   Return != if errors detected (not found, could not open, illegal data)
********************************************************************************/

/* Internal block ID types used by vfonts */
#define VF_FONT_SYM_TYPE   0x0003
#define VF_FONT_SYMCP_TYPE 0x0007

static SGUCHAR gi_vfont_load( GFONTVDYN *pvfont, PGCSTR fontname, GFONTDEVICE vf_device)
   {
   SGULONG binindex;
   #ifdef GDATACHECK
   SGUINT  blktype;
   #endif

   /* Prepare GFONTVDYN type */
   if (pvfont == NULL)
      return 1;

   /* Clear type storage (assure font structure can not be used if font not found) */
   G_POBJ_CLR(pvfont, GFONTVDYN);

   /* Seek font name */
   if (fontname == NULL)
      return 1;

   #ifdef GDATACHECK
   if ((binindex = gi_vfont_seek(fontname, vf_device, NULL, &blktype, VFONTDIR)) == 0)
      {
      G_WARNING("Named font not found");
      return 1; /* No font with that name */
      }
   if ((blktype != VF_FONT_SYM_TYPE) && (blktype != VF_FONT_SYMCP_TYPE))
      return 1; /* Block content type does not match, skip (corrupted *.bin file) */
   #else
   if ((binindex = gi_vfont_seek(fontname, vf_device, NULL, NULL, VFONTDIR)) == 0)
      {
      G_WARNING("Named font not found");
      return 1; /* No font with that name */
      }
   #endif

   /* Open font data block */
   gi_vblock_open(vf_device, binindex, 38);  /* Stored .bin font structure is 38 bytes max */

   /* Font is found. Initialize GVFONT structure (while doing any endian / type conversion) */
   /*pvfont->gfontv_id = 0;*/
   if (gvfc_rd8(0) != 1)
      return 1; /* Not an extended font. Font structure type unsupported by this implementation */

   /* Load all GFONTV data to RAM in storage order */
   pvfont->type_id = 3; /* RAM based extended font */
   pvfont->numbits  = gvfc_rd8(1);
   /*pvfont->reserved = 0; 2 */
   pvfont->symwidth = (GXT) gvfc_rd16(4);
   pvfont->symheight= (GYT) gvfc_rd16(6);
   pvfont->chsp     = gvfc_rd8(8);
   pvfont->lnsp     = gvfc_rd8(9);
   pvfont->numsym   = gvfc_rd16(10);
   pvfont->symsize  = (GBUFINT) gvfc_rd32(12);
   pvfont->si_codepage = (GFONTBUFIDX) gvfc_rd32(16);  /* == 0 if codepage not used */
   /*pvfont->gvf.extention  = NULL; 20 */
   pvfont->si_symbols = (GFONTBUFIDX) gvfc_rd32(24);
   pvfont->device_id  = vf_device;  /* use actual device ID (stored device_id (28) is always a digit) */
   if (pvfont->si_codepage != ((GFONTBUFIDX)0))
      { /* Load code page structure info and reference */
      pvfont->cprnum  = (GWCHAR) gvfc_rd16(32);
      pvfont->def_wch = (GWCHAR) gvfc_rd16(34);
      pvfont->si_codepage += ((GFONTBUFIDX)4); /* Skip CP header, set index to first CP range element */
      }
   return 0;
   }

/*******************************************************************

   DYNAMIC VIRTUAL FONT LOADER INTERFACE
   -------------------------------------
   Internal library low-level drivers dynamic handling of codepage and symbols
   stored in virtual (external) memory.

   These functions manage the font structure (GFONTVDYN) cash buffer used by
   dynamic loaded fonts without a dedicated RAM buffer.

   The gvf_open(..), gvfsym_open(..) functions are called by all font related
   functions. They test if it is a valid font pointer, and if it is a dynamic
   virtual font then the font is made the currently active and gvf_curfont is
   updated.

*****************************************************************/

/*
   Define number of simultaneous opened font structures in font cashe.
   If VFBUF_DYN_CACHE_SIZE is 0 support for cashed named VF fonts are turned off.
*/
//#define VFBUF_DYN_CACHE_SIZE 0 /* No support for cashed virtual font */
#define VFBUF_DYN_CACHE_SIZE 2     /* Define number of simultaneously active (loaded) named fonts */

GFONTVDYN *gvf_curfont;

#if (VFBUF_DYN_CACHE_SIZE > 0)

/* font cashe */
static PGFONT gvf_opened_fonts[ VFBUF_DYN_CACHE_SIZE];
static GFONTVDYN gvf_fonts[VFBUF_DYN_CACHE_SIZE];
/* Reference to current font */
static SGUCHAR gvf_curfont_idx;

/* Open dynamic VF  */
static SGUCHAR _gvf_open( PGFONT pfont )
   {
   SGUCHAR i,j;
   GFONTVDYN *tmpf;
   #if (VFBUF_DYN_CACHE_SIZE > 1)
   /* Check if it is already in font cashe, starting from next element in cashe */
   for (i=0,j=0; i<VFBUF_DYN_CACHE_SIZE-1; i++)
      {
      j=((j+1)%VFBUF_DYN_CACHE_SIZE);
      if (gvf_opened_fonts[j] == pfont)
         { /* Already loaded, just update current pointer */
         gvf_curfont_idx = j;
         gvf_curfont = &gvf_fonts[j];
         return 0;
         }
      }
   #endif

   /* Not in RAM cashe, reload font structure data from virtual font image */
   if (gvf_opened_fonts[j] != NULL)
      {
      j = (gvf_curfont_idx+1) % VFBUF_DYN_CACHE_SIZE; /* Next entry */
      gvf_opened_fonts[j] = NULL; /* Clear new entry reference, just in case */
      }
   tmpf = &gvf_fonts[j];

   /* Seek and open font */
   if (gi_vfont_load(tmpf,((PGFONTV_DYN)pfont)->fontname,((PGFONTV_DYN)pfont)->device_id))
      return 1; /* Seek or open failed */

   /* Update references to current virtual font image */
   gvf_opened_fonts[j] = pfont;
   tmpf->type_id = 3;
   gvf_curfont = tmpf;
   return 0;
   }

#endif

/* Open dynamic VF with dedicated RAM buffer */
static SGUCHAR _gvf_buf_open( PGFONT pfont )
   {
   GFONTVDYN *vfp = ((PGFONTV_DYN_BUF)pfont)->fontbuf;
   if ((vfp->gfontv_id != 0) || (vfp->type_id != 2))
      {
      /* Not loaded. Initialize RAM buffer part (only done once) */
      if (gi_vfont_load(vfp,((PGFONTV_DYN_BUF)pfont)->fontname,((PGFONTV_DYN_BUF)pfont)->device_id))
         return 1; /* Not found */
      vfp->type_id = 2;
      }
   gvf_curfont = vfp;
   return 0;
   }

/*
   General font open function.
   In case of failure the old virtual font settings remain unchanged.

   The font pointer is allowed to point to any of the supported font types, both standard
   compiled-in fonts, and font descriptors for vitual fonts (static and dynamic)
*/
SGUCHAR gvf_open( PGFONT pfont )
   {
   /* Validate font pointer object reference */
   if (pfont == NULL)
      return 1; /* Not a font pointer */
   if (gisfontv(pfont)==0)
      return 0; /* Not a virtual font -> already accesible -> ignore */
   if (((PGFONTV)pfont)->type_id == 1)
      return 0; /* A ROM located virtual font -> already accesible -> ignore */
   if (((PGFONTV)pfont)->type_id == 2)
      return _gvf_buf_open( pfont );

   #if (VFBUF_DYN_CACHE_SIZE > 0)
   if (((PGFONTV)pfont)->type_id == 3)
      {
      /* Font located in cashe buffer */
      if (pfont == gvf_opened_fonts[gvf_curfont_idx])
         {
         gvf_curfont = &gvf_fonts[gvf_curfont_idx];
         return 0; /* Quick check succeded. already current, skip update */
         }
      return _gvf_open( pfont ); /* Make font current */
      }
   #endif
   /* No GFONTV_DYN support */
   #if (VFBUF_DYN_CACHE_SIZE <= 0)
   #ifdef GDATACHECK
   if (((PGFONTV)pfont)->type_id == 3)
      {
      GWARNING("A cashed buffer must be enabled to use GFONTV_DYN fonts");
      }
   #endif
   #endif
   return 1; /* Format not supported or not active */
   }

/*
   Open (new) symbol (a dynamic vitural font with symbol)
   In case of failure the old virtual symbol settings remain unchanged.

   The psym pointer is allowed to any of the supported symbol structure types.
*/
SGUCHAR gvfsym_open( PGSYMBOL psym )
   {
   if (psym == NULL)
      return 1; /* Not active. Prevent NULL comparation */
   if (gissymbolv(psym)==0)
      return 0; /* Not a virtual font symbol -> already accesible -> ignore */
   if (((PGSYMHEADV)psym)->type_id == 1)
      return 0; /* Symbol in a ROM located virtual font -> info already accesible -> ignore */
   if (((PGSYMHEADV)psym)->type_id == 2)
      /* Symbol in named - buffered virtual font -> assure font is first time initialized */
      return _gvf_buf_open((PGFONT)((PGSYMHEADV)psym)->psymh_v->symbol_font);
   #if (VFBUF_DYN_CACHE_SIZE > 0)
   if (((PGSYMHEADV)psym)->type_id == 3)
      {
      /* Is a symbol in a named virtual font, get assosiated font pointer to check if it is loaded */
      PGFONT pfont;
      pfont = (PGFONT)((PGSYMHEADV)psym)->psymh_v->symbol_font;
      if (pfont == gvf_opened_fonts[gvf_curfont_idx])
         return 0; /* Quick check succeded. already current, skip update */
      return _gvf_open( pfont ); /* Make font current */
      }
   #endif
   return 1; /* Format not supported or not active */
   }

/*
   Initialize the dynamic virtual font cashe buffer
   ( Activated via ginit() )
*/
void gvf_init( void )
   {
   #if (VFBUF_DYN_CACHE_SIZE > 0)
   G_OBJ_CLR( gvf_opened_fonts );
   G_OBJ_CLR( gvf_fonts );
   gvf_curfont_idx = 0;
   #endif
   gvf_curfont = NULL;
   }

/*
   Initialize RAM parts of a dynamic virtual font
     For GFONTV_DYN_BUF types clear RAM buffer part
     For GFONTV_DYN types (or a NULL pointer) clear font cashe buffer.
     For all other fonts types just ignore.
*/
void gfontv_reset( PGFONT *pfont)
   {
   if (pfont != NULL)
      {
      if (gisfontv(pfont)==0) return; /* Not a virtual font, ignore */
      if (((PGFONTV)pfont)->type_id == 2)
         { /* Symbol in named - buffered virtual font -> clear font RAM part */
         G_POBJ_CLR(((PGFONTV_DYN_BUF)pfont)->fontbuf,GFONTVDYN);
         return;
         }
      if (((PGFONTV)pfont)->type_id != 3)
         return; /* Not a named non-buffered virtual font, ignore */
      }
   gvf_init(); /* Symbol in a named non-buffered virtual font, or a NULL pointer, clear font cashe */
   }

#endif /* GVIRTUAL_FONTS_DYN */

#ifdef GVIRTUAL_FILES
#include <gvfile.h>  /* GV_FILE structure */

/* block type reserved to user application data */
#define VF_BINBLK_TYPE      0x400

/*
   Open access to a named binary "file" stored in the Virtual Font image
   using dynamic location lookup (= "file" search).
   (Internal function)
*/
SGUCHAR gvfopenrd( GV_FILE *fbin, PGCSTR binname, GFONTDEVICE device_id)
   {
   #ifdef GDATACHECK
   SGULONG blksize;
   SGUINT  blktype;
   #endif
   SGULONG binindex;
   SGULONG datasize;
   SGUCHAR buf[4];
   if (fbin == NULL)
      return 1;

   for(;;)
      {
      if (binname == NULL)
         break;
      #ifdef GDATACHECK
      if ((binindex = gi_vfont_seek(binname, device_id, &blksize, &blktype,VF_BINBLK_DIR)) == 0)
         {
         G_WARNING("Named gvfile not found");
         break; /* No bin block with that name */
         }
      if ((blktype != VF_BINBLK_TYPE) || (blksize < 4))
         {
         G_WARNING("Not a valid file type");
         break; /* Block content type does not match, skip (wrong or corrupted *.bin file ?) */
         }
      #else
      if ((binindex = gi_vfont_seek(binname, device_id, NULL, NULL, VF_BINBLK_DIR)) == 0)
         {
         G_WARNING("Named gvfile not found");
         break; /* No bin block with that name */
         }
      #endif
      /* Get stored data length of "file" */
      getvmem(device_id,&buf[0],binindex, 4);
      datasize = VF32(buf,0);
      #ifdef GDATACHECK
      if (datasize+4 > blksize)
         {
         G_WARNING("Illegal file length");
         break;  /* Corrupted "file" block data */
         }
      #endif
      /* Initialize GV_FILE structure */
      fbin->struct_id = 0;
      fbin->type_id = GV_FILE_ID;
      fbin->device_id = device_id;
      fbin->length = (GFONTBUFIDX) datasize;
      fbin->blockidx = (GFONTBUFIDX) (binindex+4); /* Index for start of "file" data */
      fbin->rdidx = (GFONTBUFIDX)0;
      return 0; /* Open OK, no errors */
      }

   /* Clear type storage (assure structure can not be used) */
   G_POBJ_CLR(fbin, GV_FILE);
   return 1;
   }
#endif /* GVIRTUAL_FILES */


