/*
   VIRTUAL FONT "FILE" INTERFACE

   Set "file" read postion for next stream read operation
   0 = start of file.

   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX International ApS 2013
*/
#include <gvfile.h>

#if defined( GVIRTUAL_FILES ) || defined( GVIRTUAL_FILES_STATIC )
/*
   Set current "file" read position
*/
void gvfseek( GV_FILE *fp, GFONTBUFIDX index)  /* Set current "file" read position */
   {
   if (GVFILE_IS_OPEN(fp))
      fp->rdidx = (index < fp->length) ? index : fp->length;
   }

#endif /* GVIRTUAL_FILES  || GVIRTUAL_FILES_STATIC */


