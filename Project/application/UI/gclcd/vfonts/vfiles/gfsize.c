/*
   VIRTUAL FONT "FILE" INTERFACE

   Return size of the complete "file" object in number of bytes
   = required buffer size of hold the full object.

   Returns 0 if the file has not been opened

   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX International ApS 2013
*/
#include <gvfile.h>

#if defined( GVIRTUAL_FILES ) || defined( GVIRTUAL_FILES_STATIC )
/*
   Return byte size of a "file" object.
   = required buffer size of hold the full object.
*/
GFONTBUFIDX gvfsize( GV_FILE *fp)
   {
   if (GVFILE_IS_OPEN(fp))
      return fp->length;
   return 0;
   }

#endif /* GVIRTUAL_FILES  || GVIRTUAL_FILES_STATIC */


