/*
   VIRTUAL FONT "FILE" INTERFACE

   Close an opened VF "file", make structure unuseable (clear it)

   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX International ApS 2013
*/
#include <gvfile.h>

#if defined( GVIRTUAL_FILES ) || defined( GVIRTUAL_FILES_STATIC )

/*
   Close GV_FILE (make it unuseable)
*/
void gvfclose(GV_FILE *fp)
   {
   if (fp != NULL)
      G_POBJ_CLR(fp,GV_FILE); /* Make structure unuseable */
   }

#endif /* GVIRTUAL_FILES  || GVIRTUAL_FILES_STATIC */

