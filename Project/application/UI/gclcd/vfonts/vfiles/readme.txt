GVFILES
=======
This directory contains the Virtual Font image "file" interface modules.
- The Virtual Files feature enables application-specific data files to be stored 
  as read-only �file� data blocks in the same .bin  image as used by Virtual Fonts. 
- The Virtual Files feature can operate �files� in a Virtual Font .bin  image 
  as if it were a read-only �disk�. 
- The Virtual Files are operated much like standard files. 
- The Virtual Files feature enables �files� to be found, opened, and read using 
  very low run-time overhead.
- This makes it easy, for instance, to use a serial EEPROM or flash memory device 
  as a fast, light-weight, read-only file system, with very low processor RAM and 
  ROM memory consumption.

The GVFILES file interface provide these characteristics:
-  "File" data blocks can be fetch from random file locations and in any order.
-  The GFILE structure is located in the calling function for simplified handling.
   No memory heap support needed.
-  The GFILE system is optimized for use with read-only memory storage.
-  Can be used with small processors system without any heap memory functionality 
   or Operating System interface.

Please read the "Virtual_Font.pdf" document for details, including how to 
place application data files into the Virtual Font image.

