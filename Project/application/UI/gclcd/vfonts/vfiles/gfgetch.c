/*
   VIRTUAL FONT "FILE" INTERFACE

   SGINT gvfgetch( GV_FILE *fp)
      Read a byte from an opened VF "file" at current "file" position.
      Increment "file" position

      Returns byte casted to an SGINT or -1 if EOF is reached or an
      error is detected ("file" not opened)

   void gvfungetch( GV_FILE *fp)
      "Push back" the last character read to stream, i.e. rewind the
      stream read pointer by one byte

   The switch USE_GETCH_CASHE defined below selects between two implementations
    -  direct load (simple, small code, no extra RAM),
    -  load via cashe buffer (Usually faster when multiple sequential reads are used)
       (the speed advantage of using cashed read mode depends on the getvmem(..) implemetation)

   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX International ApS 2013
*/
#include <gvfile.h>

#define USE_GETCH_CASHE  /* Define USE_GETCH_CASHE to read via cashe buffer and block read (often faster with sequential read) */
                         /* Undefine USE_GETCH_CASHE to use byte-wise load from external storage */

#if defined( GVIRTUAL_FILES ) || defined( GVIRTUAL_FILES_STATIC )

#ifdef USE_GETCH_CASHE
  #define  GETCH_CASHE_SIZE 32   /* Number of bytes in cashe buffer */
                                 /* (= default number of bytes loaded during cashe update ) */
                                 /* Can be adjusted for best speed performance / RAM consumption */
                                 /* compromize with the given application */
#endif /* USE_GETCH_CASHE */

/* Get byte, and increment file postion (or return -1 (EOF))  */
SGINT gvfgetch( GV_FILE *fp)
   {
   #ifdef USE_GETCH_CASHE
   /* Local cashe / controls */
   static GFONTBUFIDX cbuf_start = 0; /* Absolute *.bin index for cashe start */
   static GFONTBUFIDX cbuf_end = 0;   /* Absolute *.bin index for last cashe data + 1 */
   static GFONTDEVICE device_id = 0;  /* .bin image ID (needed if more than one .bin image is used) */
   static SGUCHAR cbuf[GETCH_CASHE_SIZE];
   GFONTBUFIDX rdpos;
   #endif
   SGUCHAR val;

   if (!GVFILE_IS_OPEN(fp)) return -1; /* Not opened */
   if (fp->rdidx >=  fp->length)
      {
      if (fp->rdidx == fp->length)
         fp->rdidx++; /* Signals EOF has been returned */
      return -1; /* EOF */
      }

   #ifdef USE_GETCH_CASHE
   rdpos = fp->blockidx+fp->rdidx; /* Absolute .bin read index */
   if ((rdpos >= cbuf_start) && (rdpos < cbuf_end) && (device_id == fp->device_id))
      {
      /* Local cashe hit */
      val = cbuf[rdpos-cbuf_start];
      }
   else
      {  /* Local cashe miss, load from external device */
      GFONTBUFIDX length;
      if ((fp->rdidx + GETCH_CASHE_SIZE) > fp->length)
         length = fp->length-fp->rdidx; /* Do not read past end of "file" block */
      else
         length = GETCH_CASHE_SIZE;
      device_id = fp->device_id;
      getvmem(device_id, &cbuf[0], rdpos, length); /* Fill cashe */
      cbuf_start = rdpos;
      cbuf_end = rdpos+length;
      val = cbuf[0];
      }
   #else
   getvmem(fp->device_id, &val, fp->blockidx+fp->rdidx, 1); /* Read one byte */
   #endif
   fp->rdidx = fp->rdidx+1;
   return (SGINT)((SGUINT)val);
   }

void gvfungetch( GV_FILE *fp )
   {
   if (GVFILE_IS_OPEN(fp))
      {
      /* If not just opened and last char read was not EOF */
      if ((fp->rdidx > 0) && (fp->rdidx <= fp->length))
         fp->rdidx = fp->rdidx-1; /* Unget one byte */
      }
   }
#endif /* GVIRTUAL_FILES  || GVIRTUAL_FILES_STATIC */


