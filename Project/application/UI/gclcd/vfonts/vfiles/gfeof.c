/*
   VIRTUAL FONT "FILE" INTERFACE

   Check an opened VF "file" for End-Of-File reached
   Returns 1 if EOF is reached or the file has not been opened

   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX International ApS 2013
*/
#include <gvfile.h>

#if defined( GVIRTUAL_FILES ) || defined( GVIRTUAL_FILES_STATIC )

/*
   "End of file"
   Previous read reached end of the "file" object
   "End of file" status is automatically updated/cleared if a new
   fetch is done from inside the "file" object.

   Return 1 if read index is at end of "file" object, or there is an error.
   Return 0 if ok and not at end of object
*/
SGUCHAR gvfeof( GV_FILE *fp)
   {
   if (GVFILE_IS_OPEN(fp))
      {
      if (fp->rdidx < fp->length)
         return 0;
      }
   return 1;
   }

#endif /* GVIRTUAL_FILES  || GVIRTUAL_FILES_STATIC */


