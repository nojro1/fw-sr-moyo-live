/*
   VIRTUAL FONT "FILE" INTERFACE

   Returns current read index (== index of next byte in a "stream")
   Returns 0 if the file has not been opened

   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX International ApS 2013
*/
#include <gvfile.h>

#if defined( GVIRTUAL_FILES ) || defined( GVIRTUAL_FILES_STATIC )

/*
   Return current read index (== index of next byte in a "stream")
*/
GFONTBUFIDX gvftell( GV_FILE *fp)
   {
   if (GVFILE_IS_OPEN(fp))
      return fp->rdidx;
   return 0;
   }

#endif /* GVIRTUAL_FILES  || GVIRTUAL_FILES_STATIC */


