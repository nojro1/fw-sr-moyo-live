/*
   VIRTUAL FONT "FILE" INTERFACE

   Read "loadsize" number of bytes from the VF "file", starting from
   "file" position "rdindex"
   Returns number of bytes read ( <= "loadsize")
   Returns 0 if "rdindex" is past end of "file" or file not opened.

   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX International ApS 2013
*/
#include <gvfile.h>

#if defined( GVIRTUAL_FILES ) || defined( GVIRTUAL_FILES_STATIC )

/*
   Read data block from GV_FILE at random location
   Increment "file" "stream pointer" to next byte or EOF
*/
GFONTBUFIDX gvfread( GV_FILE *fp, GFONTBUFIDX rdindex, SGUCHAR *buf, GFONTBUFIDX loadsize)
   {
   if (!GVFILE_IS_OPEN(fp)) return 0; /* Not opened */
   if ((buf == NULL) || (loadsize == 0) || (rdindex >= fp->length))
      return 0;  /* Nothing to read to or EOF */

   /* Prevent reading past end of block */
   if (((SGULONG)rdindex + (SGULONG)loadsize) >  (SGULONG)fp->length)
      loadsize = fp->length-rdindex; /* Reduce load size to remaining buffer */

   getvmem(fp->device_id, buf, fp->blockidx+rdindex, loadsize);
   fp->rdidx = rdindex+loadsize; /* Update read pointer so next operation can be a stream read */
   return loadsize; /* Loaded size */
   }

#endif /* GVIRTUAL_FILES  || GVIRTUAL_FILES_STATIC */

