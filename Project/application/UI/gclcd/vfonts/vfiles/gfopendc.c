/*
   VIRTUAL FONT "FILE" INTERFACE

   Open acess to a "file" via a static PGV_FILE_DESC location describtor
   and initialize the GV_FILE structure

   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX International ApS 2013
*/
#include <gvfile.h>

#if defined( GVIRTUAL_FILES_STATIC )
/*
   Fast access to a static "file" block via a PGV_FILE_DESC descriptor
*/
SGUCHAR gvfopen_desc( GV_FILE *fp, PGV_FILE_DESC filedesc)
   {
   if (fp == NULL)
      return 1;

   for(;;)
      {
      if (filedesc == NULL)
         break;
      if ((filedesc->struct_id != 0) || (filedesc->type_id != GV_FILE_STATIC_ID))
         break; /* Unknown / unsupported descriptor type */

      /* Copy static (fast) lookup descrition data */
      fp->struct_id = 0;
      fp->type_id   = GV_FILE_ID;
      fp->device_id = filedesc->device_id; /* Low-level device VF ID */
      fp->length    = filedesc->length;    /* Byte length of "file" data */
      fp->blockidx  = filedesc->blockidx;  /* Base location of file data block */
      fp->rdidx = 0;
      return 0;  /* Open OK, no errors */
      }

   /* Some error. Clear type storage (assure structure can not be used) */
   G_POBJ_CLR(fp, GV_FILE);
   return 1;
   }

#endif /* GVIRTUAL_FILES_STATIC */

