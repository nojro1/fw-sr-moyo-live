/*
   Interface function for virtual (external) memory

   This module contains access functions for virtual memory.

   The getvmem(..) function must be implemented by the library
   user for the specific type of storage device.

*/
#include <getvmem.h>

/*
   void getvmem(GFONTDEVICE vf_device, SGUCHAR *buf, GFONTBUFIDX index, SGUINT numbytes)

   Read block of data bytes from virtual memory.
   The virtual memory is assumed to be a linear block of bytes where index = 0
   refer to the first byte in the virtual memory image.

   vf_device = Device identifier
               The parameter is normally used when a target driver support multiple storage
               locations. It can be ignored if only one virtual font image and driver is used
               by an application.
               The specific use of this parameter is application specific.
               The value is not used internally by the graphic library. It is just passed
               through from the .vfg file device_id information. 0 is default value.
   buf       = Destination pointer to RAM storage location
   index     = Source index for start of data in virtual font to be loaded to RAM.
               0 mean beginning of virtual font image.
               Note: If any offset has been used during programming (storage) of the virtual
               font image in a memory device, then the same offset must be added to this
               index value during the read back.
   numbytes  = Number of bytes to load from virtual memory to RAM buffer.
               The first byte of this block is located in the virtual memory at byte offset = index.
               The maximum value of this parameter is defined by VFBUFSIZE in gfontv.c
               (Library default is to load max 32 bytes at a time)

   Note: This function just performs a basic hardware device level load.
   (All virtual font image specific knowledge is encapsulated and handled automatically at
    higher abstraction levels.)
*/
void getvmem(GFONTDEVICE vf_device, SGUCHAR *buf, GFONTBUFIDX index, SGUINT numbytes)
   {
   /* Add code here which copy data from external memory (font)
      storage to RAM buffer */

   /* If multiple different storage locations are used then vf_device can be used for
      storage location selection, and if different storage types are used, for
      storage device driver selection */

   /*
     Set buffer array
         from buf[0] to buf[numbytes-1]
     equal to content in virtual storage memory
         from virtual_memory[index] to virtual_memory[index + numbytes-1]
   */
   buf++;    /* dummy operations to silence "not used" compiler warnings, ex when module is used as a test stup. */
   vf_device++;
   index++;
   numbytes++;
   }

/*
   Optinally called by application before first use of virtual font

   vf_device = device identifier (used when target driver support multiple storage locations)

   Return == 0 if no errors
   Return != 0 if some errors

   Leave an empty function if operation is not relevant for memory hardware
*/
int getvmem_open( GFONTDEVICE vf_device )
   {
   vf_device++; /* dummy operation to silence compiler, ex when module is used as a test stup. */
   return 0;
   }

/*
   Optionally called by application after last use of virtual font

   vf_device = device identifier (used when target driver support multiple storage locations)

   Leave an empty function if operation is not relevant for memory hardware
*/
void getvmem_close( GFONTDEVICE vf_device )
   {
   vf_device++; /* dummy operation to silence compiler, ex when module is used as a test stup. */
   }


