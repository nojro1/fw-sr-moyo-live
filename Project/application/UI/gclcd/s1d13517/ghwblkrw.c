/************************** ghwblkrw.c *****************************

   Graphic block copy functions for LCD display

   Read graphic area from the display to a GLCD buffer.
   Write graphic buffer to LCD display.

   Information about the size of the graphic area is stored in the buffer.
   The buffer can be written back to the display with ghw_wrblk(),
   optionally with another start origin.

   All coordinates are absolute pixel coordinate.

   ---------

   The s1d13517 controller is assumed to be used with a LCD module.

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is complied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2010

*********************************************************************/
/* <stdlib.h> is included via gdisphw.h */
#include <gdisphw.h>   /* HW driver prototypes and types */
#include <s1d13517.h>  /* s1d13517 controller specific definements */

#ifdef GBASIC_INIT_ERR
#if defined( GBUFFER )

typedef struct
   {
   GXT lx;
   GYT ly;
   GXT rx;
   GYT ry;
   SGUCHAR dat[1];
   } GHW_BLK_HEADER, *PGHW_BLK_HEADER;

/****************************************************************
 ** block functions
****************************************************************/

/*
   Calculate the needed size for the buffer used by ghw_rdblk()
   Return value can be used as parameter for buffer allocation with
   malloc.
   The coordinates to this function may be absolute or view-port relative
*/
GBUFINT ghw_blksize(GXT ltx, GYT lty, GXT rbx, GYT rby)
   {
   /* Force resonable values (assure that unsigned is poitive) */
   GLIMITD(rby,lty);
   GLIMITD(rbx,ltx);
   return GHW_BLK_SIZE(ltx,lty,rbx,rby);
   }

/*
   Copy a graphic area from the display to a GLCD buffer
   Information about the size of the graphic area is saved in the buffer.
   The buffer can be written back to the display with ghw_wrblk(),
   optionally with another start origin.

   All coordinates are absolute pixel coordinate.

   The first part of the buffer will be a dynamic header defining
   the block rectangle:
      GXT left_top_x,
      GYT left_top_y,
      GXT right_bottom_x,
      GYT right_bottom_y,
     followed by the block data
*/
void ghw_rdblk(GXT ltx, GYT lty, GXT rbx, GYT rby, SGUCHAR *dest, GBUFINT bufsize )
   {
   GXT x;
   PGHW_BLK_HEADER desthdr;
   GCOLOR col;
   GBUFINT gbufidx;
   GBUF_CHECK();

   glcd_err = 0;
   if (dest == NULL)
      return;

   /* Force reasonable values */
   GLIMITU(ltx,GDISPW-1);
   GLIMITU(lty,GDISPH-1);
   GLIMITD(rby,lty);
   GLIMITU(rby,GDISPH-1);
   GLIMITD(rbx,ltx);
   GLIMITU(rbx,GDISPW-1);

   if (ghw_blksize(ltx,lty,rbx,rby) > bufsize)
      {
      G_ERROR( "ghw_rdblk: dest buffer too small" );
      return;
      }

   /* Save header info */
   desthdr = (PGHW_BLK_HEADER) dest;
   dest = &(desthdr->dat[0]);
   desthdr->lx = ltx;
   desthdr->ly = lty;
   desthdr->rx = rbx;
   desthdr->ry = rby;

   /* Convert to byte coordinates */
   for (; lty <= rby; lty++)
      {
      gbufidx = GINDEX(ltx,lty);
      for (x = ltx; x <= rbx; x++)
         {
         /* Read pixel */
         col = gbuf[gbufidx++];
         /* Do a generic pixel storage */
         *dest++ = (SGUCHAR) col;
         #if (GDISPPIXW > 8)
         col >>= 8;
         *dest++ = (SGUCHAR) col;
         #if (GDISPPIXW > 16)
         col >>= 8;
         *dest++ = (SGUCHAR) col;
         #if (GDISPPIXW > 24)
         col >>= 8;
         *dest++ = (SGUCHAR) col;
         #endif
         #endif
         #endif
         }
      }
   }

/*
   Copy a graphic area from a GLCD buffer to the display
   The GLCD buffer must have been read with ghw_rdblk

   If the destination range is larger than the buffered range
   then the destination range is limited to fit the size of
   the buffered range.

   If the destination range is smaller than the buffered range
   then only the upper-left part of the buffer is written to
   the display.
*/
void ghw_wrblk(GXT ltx, GYT lty, GXT rbx, GYT rby, SGUCHAR *src )
   {
   GXT w,we,x,xe;
   GYT h,he;
   PGHW_BLK_HEADER srchdr;
   GBUFINT bw;
   SGUCHAR *nextline;
   GCOLOR col;
   GBUFINT gbufidx;
   GBUF_CHECK();

   glcd_err = 0;
   if (src == NULL)
      return;

   /* Force reasonable values */
   GLIMITU(ltx,GDISPW-1);
   GLIMITU(lty,GDISPH-1);
   GLIMITD(rby,lty);
   GLIMITD(rbx,ltx);
   GLIMITU(rby,GDISPH-1);
   GLIMITU(rbx,GDISPW-1);

   invalrect( ltx, lty );
   invalrect( rbx, rby );

   /* Get header info about stored buffer */
   srchdr = (PGHW_BLK_HEADER) src;
   w = srchdr->lx;
   h = srchdr->ly;
   we = srchdr->rx;
   he = srchdr->ry;
   src = &(srchdr->dat[0]);

   /* Limit destination range against source window size in buffer */
   if (rbx-ltx > (we-w))
      rbx = ltx + (we-w);
   if (rby-lty > (he-h))
      rby = lty + (he-h);

   xe = rbx - ltx;         /* = num horizontal pixels */
   w  = (we - w + 1);      /* Stored line width in pixels */
   bw = (GBUFINT) w * ((GDISPPIXW+7)/8); /* Stored line width in bytes */

   for (; lty <= rby; lty++)
      {
      /* Calculate buffer start / stop indexes for line */
      gbufidx = GINDEX(ltx,lty);
      nextline = &src[bw]; /* Set to next buffer line */

      /* Select patterns and start mask */
      for (x = 0; x <= xe; x++ )
         {
         /* Read pixel data from generic storage */
         col = (GCOLOR) *src;
         src++;
         #if (GDISPPIXW > 8)
         col = (GCOLOR)(col | (((GCOLOR)(*src))<<8));
         src++;
         #if (GDISPPIXW > 16)
         col = (GCOLOR)(col | (((GCOLOR)(*src))<<16));
         src++;
         #if (GDISPPIXW > 24)
         col = (GCOLOR)(col | (((GCOLOR)(*src))<<24));
         src++;
         #endif
         #endif
         #endif

         /* Write pixel */
         gbuf[gbufidx++] = col;
         }
      src = nextline;
      }
   ghw_updatehw();   /* This function may be called directly from the user level so update is needed */
   }

/*
   Retore a block buffer in the same position as it was read
   The position information is taken from the header
*/
void ghw_restoreblk(SGUCHAR *src)
   {
   PGHW_BLK_HEADER srchdr;
   if ((srchdr = (PGHW_BLK_HEADER) src) != NULL)
      ghw_wrblk(srchdr->lx,srchdr->ly, srchdr->rx, srchdr->ry, src );
   }

#endif /* defined( GBUFFER ) */
#endif /* GGRAPHICS */


