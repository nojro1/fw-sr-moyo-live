/******************************************************************

   s1d13517 LCD controller simulator for the RAMTEX LCD driver library

   The s1d13517 is simulated by the use of a buffer array of 16 frame buffers
       controller_video_buffers[]
       controller_video_ram        Pointer to buffer used by display
       controller_wr_video_ram     Pointer to buffer used by write

   The X wr position can only be set in steps of 8 pixels.

   Version number: 1.0
   Copyright (c) RAMTEX Aps 2010

******************************************************************/

#ifndef GHW_PCSIM
#error Only include this file in PC simulation mode
#endif

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <gdisphw.h>    /* swprintf */ /* GHW_FAST_SIM_UPDATE */
#include <s1d13517.h>   /* Controller specific definements */

void ghw_autowr_sim( GCOLOR cval );
static int graph_init = 0; /* don't init graphic twice */

#ifdef _WIN32
   #include <gsimintf.h>
   void simputs( SGINT sgstream, const char *chp )
      {
      sgstream = sgstream; /* remove warning */
      GSimPuts(chp);
      }
   #define far /* nothing */
#else
   #error no simulation for this compiler
#endif

void simprintf( SGINT sgstream, const char far *fmt, ...)
   {
   va_list argptr;
   int form_len;
   static char printf_buf[100];

   va_start(argptr, fmt);
   form_len = vsprintf(printf_buf, fmt, argptr);
   va_end(argptr);

   if (form_len >= 100)
      simputs(-1,"\nERROR: Buffer overrun in simsprintf()");

   simputs( sgstream, printf_buf );
   }

/* Maximum number of video buffers */
#define BUFFER_MAX     16

/* Internal s1d13517 data simulation */
static GSIM_RGB_PARAM *controller_video_buffers[16]; /* S1D13517 simulator module buffer */
static GSIM_RGB_PARAM *controller_wr_video_ram = NULL; /* Current S1D13517 simulator video ram to write to */
static GSIM_RGB_PARAM *controller_video_ram = NULL; /* Current S1D13517 simulator video ram */

/* Copy of user LCD size variables (allows the simulator to be a lib) */
/* Size of video ram in controller */
static SGUINT gdisph;      /* Initiated with user gdisph */
static SGUINT gdispw;      /* Initiated with user gdispw */

/* Viewport area control for physical screen */
static SGUINT gdispph;     /* Initiated with user gdisp_ph */
static SGUINT gdisppw;     /* Initiated with user gdisp_pw */
static SGUINT gdisppx;     /* Initiated with user gdisp_px */
static SGUINT gdisppy;     /* Initiated with user gdisp_py */

/* Auto increment simulation control */
static SGUINT xpos, winxb, winxe;
static SGUINT ypos, winyb, winye;

static SGBOOL onoff;       /* Display on / off */
static SGBOOL winena;      /* Update viewport initialized */

static void clr_screen(void)
   {
   SGUINT x;
   SGUINT y;
   GSIM_RGB_PARAM val;
   if (graph_init == 0) return;   /* Display have not been initiated yet */
   val.par = 0L;
   for (x = 0; x < gdisppw; x++)
      {
      for (y = 0; y < gdispph; y++)
         GSimWrRGBBit(x,y,val);
      }
   GSimFlush();
   }

/* Redraw screen content */
static void redraw_screen(void)
   {
   SGUINT x;
   SGUINT y;
   if (graph_init == 0) return;   /* Display have not been initiated yet */
   for (x = 0; x < gdisppw; x++)
      {
      for (y = 0; y < gdispph; y++)
         GSimWrRGBBit(x,y,controller_video_ram[x+gdisppx + (y+gdisppy)*gdispw]);
      }
   GSimFlush();
   }

/*
   Simulate display on / off
*/
void ghw_dispoff_sim( void )
   {
   /* Off */
   if (onoff)
      clr_screen();
   onoff = 0;
   }

void ghw_dispon_sim( void )
   {
   /* On */
   if (onoff == 0)
      {
      onoff = 1;
      redraw_screen();
      }
   }

/*
   Init cursor and autowrap limits

   Simulates limitation on X position setting
*/
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   GLIMITU(xb,gdispw-1);
   GLIMITU(xe,gdispw-1);
   GLIMITU(yb,gdisph-1);
   GLIMITU(ye,gdisph-1);
   winxb = xb & (~0x7);
   winxe = xe & (~0x7);
   winyb = yb;
   winye = ye;
   xpos = winxb;
   ypos = yb;
   winena = 1;
   }

/*
   Simulate internal pointer handling with auto wrap

   Simulates limitation on X position handling
*/
static void addrinc(void)
   {
   xpos++;
   if (winena)
      {
      if ((xpos & (~0x7)) > (winxe & (~0x7)))
         {
         xpos = winxb;
         if (++ypos > winye)
            ypos = winyb;
         }
      }
   else
      {
      if (xpos >= GDISPW)
         {
         xpos = 0;
         if (++ypos >= GDISPH)
            ypos = 0;
         }
      }
   }


/*
   Write to update PC screen
   Simulate autoincrement
*/
void ghw_autowr_sim( GCOLOR cval )
   {
   GSIM_RGB_PARAM val;
   if (graph_init == 0)
      return;   /* Display have not been initiated yet */

   if (cval != G_TRANSCOLOR)
      {
      // Only transfer data to video memory if not the transperant color

      #if (GDISPPIXW==8)
      val.rgb.r = (SGUCHAR) (cval & 0xe0);
      val.rgb.g = (SGUCHAR)((cval << 3) & 0xe0);
      val.rgb.b = (SGUCHAR)((cval << 6) & 0xc0);
      /* Just expand colors to simulator resolution */
      val.rgb.r |= (SGUCHAR) (val.rgb.r >> 3) | (val.rgb.r >> 6);
      val.rgb.g |= (SGUCHAR) (val.rgb.g >> 3) | (val.rgb.g >> 6);
      val.rgb.b |= (SGUCHAR) (val.rgb.b >> 2) | (val.rgb.b >> 4) | (val.rgb.b >> 6);
      #elif (GDISPPIXW==16)
        /* 16 bit resolution */
        val.rgb.r = (SGUCHAR) ((cval >> 8) & 0xf8);
        val.rgb.g = (SGUCHAR) ((cval >> 3) & 0xfc);
        val.rgb.b = (SGUCHAR) ((cval << 3) & 0xf8);
        /* Just expand colors to simulator resolution */
        val.rgb.r |= (SGUCHAR) (val.rgb.r >> 5);
        val.rgb.g |= (SGUCHAR) (val.rgb.g >> 6);
        val.rgb.b |= (SGUCHAR) (val.rgb.b >> 5);

      #elif (GDISPPIXW==18)
        /* 18 bit resolution */
        val.rgb.r = (SGUCHAR) ((cval >> 10)& 0xfc);
        val.rgb.g = (SGUCHAR) ((cval >> 4) & 0xfc);
        val.rgb.b = (SGUCHAR) ((cval << 2) & 0xfc);

        #if (GHW_COLOR_CMP_MSK == 0x3ffff)
        /* Just expand colors to simulator resolution */
        val.rgb.r |= (SGUCHAR) (val.rgb.r >> 6);
        val.rgb.g |= (SGUCHAR) (val.rgb.g >> 6);
        val.rgb.b |= (SGUCHAR) (val.rgb.b >> 6);
        #else
        /* Set LSB bits if fewer bits are actively used by hardware */
        if (val.rgb.r & 0x80)
           val.rgb.r |= (SGUCHAR) ~((GHW_COLOR_CMP_MSK >> 10) & 0xfc);
        else
           val.rgb.r &= (SGUCHAR)  ((GHW_COLOR_CMP_MSK >> 10) & 0xfc);

        if (val.rgb.g & 0x80)
           val.rgb.g |= (SGUCHAR) ~((GHW_COLOR_CMP_MSK >> 4) & 0xfc);
        else
           val.rgb.g &= (SGUCHAR)  ((GHW_COLOR_CMP_MSK >> 4) & 0xfc);

        if (val.rgb.b & 0x80)
           val.rgb.b |= (SGUCHAR) ~((GHW_COLOR_CMP_MSK << 2) & 0xfc);
        else
           val.rgb.b &= (SGUCHAR)  ((GHW_COLOR_CMP_MSK << 2) & 0xfc);
        #endif

      #elif (GDISPPIXW==24)
        /* 24 bit resolution */
        val.rgb.r = (SGUCHAR) (cval >> 16);
        val.rgb.g = (SGUCHAR) (cval >> 8);
        val.rgb.b = (SGUCHAR) (cval);

        /* Set LSB bits if fewer bits are actively used by hardware */
        #if (GHW_COLOR_CMP_MSK != 0xffffff)
        if (val.rgb.r & 0x80)
           val.rgb.r |= (SGUCHAR) ~(GHW_COLOR_CMP_MSK >> 16);
        else
           val.rgb.r &= (SGUCHAR)  (GHW_COLOR_CMP_MSK >> 16);

        if (val.rgb.g & 0x80)
           val.rgb.g |= (SGUCHAR) ~(GHW_COLOR_CMP_MSK >> 8);
        else
           val.rgb.g &= (SGUCHAR)  (GHW_COLOR_CMP_MSK >> 8);

        if (val.rgb.b & 0x80)
           val.rgb.b |= (SGUCHAR) ~(GHW_COLOR_CMP_MSK);
        else
           val.rgb.b &= (SGUCHAR)  (GHW_COLOR_CMP_MSK);
        #endif
      #else
        #error GDISPPIXW setting illegal for this simulator
      #endif

      controller_wr_video_ram[xpos + ypos * gdispw] = val;

      if (winena)
         {
         if (onoff)
            {
            /* Screen is turned on */
            if (controller_wr_video_ram == controller_video_ram)
               { // Update buffer = visible buffer -> update screen at once
               if ((xpos >= gdisppx) && (ypos >= gdisppy) &&
                   (xpos <  gdisppx+gdisppw) && (ypos < gdisppy+gdispph))
                  /* Pixel inside visible screen, update simulator as well */
                  GSimWrRGBBit( xpos-gdisppx, ypos-gdisppy, val);
               }
            }
         #ifndef GHW_FAST_SIM_UPDATE
         GSimFlush();
         #endif
         }
      }

   /* Increment address, even in transperant color mode */
   addrinc();
   }

/*
   Read back data
   (for test only, not suported by S1D13517 hardware)
*/
GCOLOR ghw_autord_sim( void )
   {
   GPALETTE_RGB palette;
   register GSIM_RGB_PARAM  *cp;
   if (graph_init == 0)
      return 0;   /* Display have not been initiated yet */
   cp = &controller_video_ram[xpos + ypos*gdispw];
   #ifndef GHW_NO_RDINC
   addrinc();
   #endif
   palette.r = cp->rgb.r;
   palette.g = cp->rgb.g;
   palette.b = cp->rgb.b;
   return ghw_rgb_to_color( &palette );
   }

/*
   Select buffer to make visible on screen
*/
void ghw_bufsel_sim(SGUCHAR bufnum)
   {
   if (graph_init == 0)
      return;
   controller_video_ram = controller_video_buffers[bufnum % BUFFER_MAX ];
   redraw_screen();
   }

/*
   Select buffer to write on for update
   (may be different from the visible buffer)
*/
void ghw_wrbufsel_sim(SGUCHAR bufnum)
   {
   if (graph_init == 0)
      return;
   controller_wr_video_ram = controller_video_buffers[bufnum % BUFFER_MAX ];
   }

/*
   Release all buffer ressources
*/
static void ghw_free_buffers(void)
   {
   int i;
   controller_video_ram = NULL;
   controller_wr_video_ram = NULL;
   for (i = 0; i < sizeof(controller_video_buffers)/sizeof(controller_video_buffers[0]); i++)
      {
      if (controller_video_buffers[i] != NULL)
         {
         free( controller_video_buffers[ i ]);
         controller_video_buffers[ i ] = NULL;
         }
      }
   }

/*
   This function is called last in ghw_init after all s1d13517 display
   module parameters has been initiated.

   NOTE: The user code may cause that this function is in-worked more
   than once
*/
/*
   Init simulator so screen may be smaller than the screen buffer
*/
void ghw_init_sim_viewport( SGUINT dispw, SGUINT disph, SGUINT disppw, SGUINT dispph  )
   {
   /* Logical screen data setup(ram buffer sizes) */
   if (dispw < disppw)
      dispw = disppw;
   if (disph < dispph)
      disph = dispph;
   gdisph = disph;
   gdispw = dispw;

   /* Physical screen data setup */
   gdispph = dispph;
   gdisppw = disppw;
   gdisppx = 0;
   gdisppy = 0;

   #ifdef _WIN32
   /* Blank display */
   if( !graph_init )
      {
      unsigned long buffer_reg_size;
      int err,i;

      if (controller_video_ram != NULL)
         ghw_free_buffers();

      buffer_reg_size = dispw*disph*sizeof(GSIM_RGB_PARAM);
      for (i = 0, err = 0; i < BUFFER_MAX; i++)
         {
         if ((controller_video_buffers[i] = calloc(buffer_reg_size,1)) == NULL)
            err = 1;
         }

      if ( err )
         {
         // Some out of memory error, release ressources againg
         ghw_free_buffers();
         graph_init = 0;
         return; /* Simulator init error */
         }

      // Select default buffer
      controller_video_ram = controller_video_buffers[0];
      controller_wr_video_ram = controller_video_buffers[0];

      /* Init connection to LCD simulator (only once to uptimize speed */
      if (!GSimInitC((unsigned short) gdisppw, (unsigned short) gdispph, PALETTE_RGB))
         {
         graph_init = 1;
         }
      }

   clr_screen();
   onoff = 0;
   xpos = 0;
   ypos = 0;
   winena = 0;
   winxb = 0;
   winxe = gdispw-1;
   winyb = 0;
   winye = gdisph-1;
   #else
   #error no simulation for this compiler
   #endif
   }

/*
   Init simulator so "physical" screen and screen buffer has the same size
*/
void ghw_init_sim( SGUINT dispw, SGUINT disph )
   {
   ghw_init_sim_viewport( dispw, disph, dispw, disph );
   }

/*
   This function is activated via ghw_exit() when gexit() is called
*/
void ghw_exit_sim( void )
   {
   graph_init = 0;
   if (controller_video_ram != NULL)
      {
      ghw_free_buffers();
      }

   #if defined( _WIN32 )
   GSimClose();
   #endif
   }

