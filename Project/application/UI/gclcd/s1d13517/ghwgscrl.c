/************************** ghwgscrl.c *****************************

   Scrolls the graphics on LCD x lines up.  The empty area in the
   bottom is cleared with a pattern.

   ---------

   The s1d13517 controller is assumed to be used with a LCD module.

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is copied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)


   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2010

*********************************************************************/
#include <gdisphw.h>    /* HW driver prototypes and types */
#include <s6d0129.h>   /* lcd controller specific definements */

#if defined( GBASIC_TEXT ) || defined(GSOFT_FONTS) || defined(GGRAPHIC)
#if defined( GBUFFER )
/*
   Scrolls the graphics on LCD x lines up.
   The empty area in the bottom is cleared

   lines  =  pixel lines to scroll
*/
void ghw_gscroll(GXT ltx, GYT lty, GXT rbx, GYT rby, GYT lines, SGUINT pattern)
   {
   GYT ylim;
   GXT x;
   GBUFINT gbufidx;
   GBUFINT source;
   GBUF_CHECK();

   glcd_err = 0;

   /* Force resoanable values */
   GLIMITU(ltx,GDISPW-1);
   GLIMITU(lty,GDISPH-1);
   GLIMITD(rby,lty);
   GLIMITU(rby,GDISPH-1);
   GLIMITD(rbx,ltx);
   GLIMITU(rbx,GDISPW-1);

   /* Buffered mode  */
   invalrect( ltx, lty );
   invalrect( rbx, rby );

   if (lines > rby - lty)
      {
      ghw_fill(ltx, lty, rbx, rby, pattern);   /* just clear whole area */
      return;
      }

   ylim = rby - lines;
   for (; lty <= rby; lty++)
      {
      /* Loop rows */
      if (lty >= ylim)
         {
         ghw_fill(ltx, lty, rbx, rby, pattern);   /* clear remaining area */
         return;
         }

      gbufidx = GINDEX(ltx,lty);
      source = gbufidx + (GBUFINT)lines * GDISPW;
      for (x = ltx; x <= rbx; x++)
         {
         /* Loop pixel columns in row */
         gbuf[gbufidx++] = gbuf[source++];
         }
      }

   }

#endif /* defined( GBUFFER ) || !defined(GHW_NO_LCD_READ_SUPPORT) */
#endif /* GBASIC_TEXT */

