/************************** ghwinv.c *****************************

   Invert box area i.e. swap between two colors

   The box area may have any pixel boundary

   ---------

   The s1d13517 controller is assumed to be used with a LCD module.

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is compied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)

   Revision date:
   Revision Purpose:

   Version number: 1.0
   Copyright (c) RAMTEX Engineering Aps 2010

*********************************************************************/
#include <gdisphw.h>   /* HW driver prototypes and types */
#include <s6d0129.h>   /* lcd controller specific definements */

#if (!defined( GNOCURSOR ) && defined (GSOFT_FONTS )) || defined (GGRAPHICS)
#if defined( GBUFFER )

void ghw_invert(GXT ltx, GYT lty, GXT rbx, GYT rby)
   {
   GXT x;
   register GCOLOR color, fore, back;
   GBUFINT gbufidx;
   GBUF_CHECK();

   glcd_err = 0;

   /* Force reasonable values */
   GLIMITU(ltx,GDISPW-1);
   GLIMITU(lty,GDISPH-1);
   GLIMITD(rby,lty);
   GLIMITU(rby,GDISPH-1);
   GLIMITD(rbx,ltx);
   GLIMITU(rbx,GDISPW-1);

   invalrect( ltx, lty );
   invalrect( rbx, rby );
   fore = ghw_def_foreground & GHW_COLOR_CMP_MSK;
   back = ghw_def_background & GHW_COLOR_CMP_MSK;

   for (; lty <= rby; lty++)
      {
      gbufidx = GINDEX(ltx,lty);
      /* loop invert of colors */
      for (x = ltx; x <= rbx; x++)
         {
         color = gbuf[gbufidx];
         /* Swap foreground and background colors */
         if ((color & GHW_COLOR_CMP_MSK) == fore )
            gbuf[gbufidx] = ghw_def_background;
         else
         if ((color & GHW_COLOR_CMP_MSK) == back )
            gbuf[gbufidx] = ghw_def_foreground;
         gbufidx++;
         }
      }
   }

#endif /* defined( GBUFFER )*/
#endif /* GBASIC_TEXT */

