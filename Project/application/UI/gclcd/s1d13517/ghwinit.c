/************************** ghwinit.c *****************************

   Low-level driver functions for the S1D13517 LCD display controller
   initialization and error handling.

   Notes: S1D13517 does not provide video memory read back
          The written pixel format is always internally converted
          to 24 bit.
          The library therefore only support the 16 and 24 bit color modes

   The following LCD module characteristics MUST be correctly
   defined in GDISPCFG.H:

      GDISPW  Display width in pixels
      GDISPH  Display height in pixels
      GBUFFER If defined most of the functions operates on
              a memory buffer instead of the LCD hardware.
              The memory buffer content is copied to the LCD
              display with ghw_updatehw().
              (Equal to an implementation of delayed write)

   S1D13517 chip and driver support these mode combinations
        8 bit bus, 16 bit color   8+8
        8 bit bus, 24 bit color   8+8+8
        16 bit bus, 16 bit color  16
        16 bit bus, 24 bit color  16+8

   Revision date:    11-11-10
   Revision Purpose: ghw_rgb_to_color(..) updated to use G_RGB_TO_COLOR(r,g,b) macro.

   Version number: 1.1
   Copyright (c) RAMTEX Engineering Aps 2010

*********************************************************************/

#ifdef SGPCMODE
#include "windows.h"   /* Sleep(..) function used by ghw_cmd_wait(..)*/
#endif
#include <gdisphw.h>   /* HW driver prototypes and types  */
#include <s1d13517.h>   /* Controller specific definements */

#ifdef GHW_S1D13517
  #if (GDISPW*GDISPH > 960*960)
    #error Screen size too large for display controller (max clk is 45MHz)
  #endif
#else
  #error Unknown controller, Controller must be selected in gdispcfg.h
#endif

#ifdef GBASIC_INIT_ERR

/********************* Chip access definitions *********************/

#ifndef GHW_NOHDW
   #if defined( GHW_SINGLE_CHIP)
      /* User defined access types and simulated register address def */
      #include <bussim.h>
      #ifdef GHW_BUS8
        #define  sgwrby(a,d) simwrby((a),(d))
        #define  sgrdby(a)   simrdby((a))
      #elif defined (GHW_BUS32)
        #define  sgwrdw(a,d) simwrdw((a),(d))
        #define  sgrddw(a)   simrddw((a))
      #else /* bus 16 */
        #define  sgwrwo(a,d) simwrwo((a),(d))
        #define  sgrdwo(a)   simrdwo((a))
      #endif
   #else
      /* Portable I/O functions + hardware port def */
      #include <sgio.h>
   #endif
#else
   #undef GHW_SINGLE_CHIP /* Ignore single chip mode */
#endif

/***********************************************************************/
/** All static LCD driver data is located here in this ghwinit module **/
/***********************************************************************/

/* Active foreground and background color */
GCOLOR ghw_def_foreground;
GCOLOR ghw_def_background;

#if (GHW_PALETTE_SIZE > 0)
/* Default soft palette
   The palette file can be edited directly with the ColorIconEdit program
*/
static GCODE GPALETTE_RGB FCODE ghw_palette[16] =
     #include <gcolor_4.pal>
     ;

/* Operative palette (current palette used for color lookup) */
GCOLOR ghw_palette_opr[16];
#endif

/* Use software font */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR  b[8];           /* Symbol data, fixed size = 8 bytes */
   }
GCODE FCODE sysfontsym[0x80] =
   {
   /* The default font MUST be a monospaced black & white (two-color) font */
   #include <sfs0129.sym> /* System font symbol table */
   };

/* Default system font */
GCODE GFONT FCODE SYSFONT =
   {
   6,      /* width */
   8,      /* height */
   sizeof(sysfontsym[0])-sizeof(GSYMHEAD), /* number of data bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL) sysfontsym,  /* pointer to array of SYMBOLS */
   0x80,   /* num symbols in sysfontsym[] */
   NULL    /* pointer to code page */ /* NULL means code page is not used */
   };

#ifdef GBUFFER
   #ifdef GHW_ALLOCATE_BUF
      /* <stdlib.h> is included via gdisphw.h */
      GCOLOR *gbuf = NULL;           /* Graphic buffer pointer */
      static SGBOOL gbuf_owner = 0;   /* Identify pointer ownership */
   #else
      GCOLOR gbuf[GBUFSIZE];         /* Graphic buffer */
   #endif
   GXT GFAST iltx,irbx;     /* "Dirty area" speed optimizers in buffered mode */
   GYT GFAST ilty,irby;
   SGBOOL  ghw_upddelay;    /* Flag for delayed update */
#else
   GCOLOR ghw_tmpbuf[GDISPW]; /* Row line buffer (for block read-modify-write) */
#endif /* GBUFFER */


#ifdef GHW_INTERNAL_CONTRAST
static SGUCHAR ghw_contrast;/* Current contrast value */
#endif

SGBOOL glcd_err;            /* Internal error */
#ifndef GNOCURSOR
GCURSOR ghw_cursor;         /* Current cursor state */
#endif

#ifdef GHW_PCSIM
/* PC simulator declaration */
void ghw_init_sim( SGUINT dispw, SGUINT disph );
void ghw_exit_sim(void);
void ghw_set_xyrange_sim(GXT xb, GYT yb, GXT xe, GYT ye);
void ghw_autowr_sim( GCOLOR cval );
void ghw_dispon_sim( void );
void ghw_dispoff_sim( void );
void ghw_bufsel_sim(SGUCHAR bufnum);
void ghw_wrbufsel_sim(SGUCHAR bufnum);
#endif
/**********************************************************************/
/** Low level S1D13517 interface functions used only by ghw_xxx modules **/
/**********************************************************************/

/* Bit mask values */
GCODE SGUCHAR FCODE sympixmsk[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};

/********************* Command interface *****************/

/* Commonly used registers */
#define REG00_PROD_CODE        0x00     // Product Code Register [READONLY]
#define REG2A_DSP_MODE         0x2A     // Display Mode Register
#define REG50_DISPLAY_CONTROL  0x50     // Display Control Register [WRITEONLY]
#define REG52_INPUT_MODE       0x52     // Input Mode Register
#define REG5A_WRITE_WIN_X_SP   0x5A     // Write Window X Start Position Register
#define REG66_MEM_DATA_PORT_0  0x66     // Memory Data Port Register base [WRITEONLY]

/* Bit defines to ease setup configuration below */
#ifdef GHW_LOW_VSYNC
  #define VS 0x00
#else
  #define VS 0x80
#endif

#ifdef GHW_LOW_HSYNC
  #define HS 0x00
#else
  #define HS 0x80
#endif

#ifdef GHW_LOW_FPSHIFT
  #define PCK 0x00
#else
  #define PCK 0x80
#endif

#ifdef GHW_TFT_18BIT_LCDBUS
   #define TFT18 1
#else
   #define TFT18 0
#endif

#if (GDISPPIXW == 16)
   /* Input data = RGB 5:6:5 */
   #define  PW 0x4
#else
   /* Input data = RGB 8:8:8 */
   #ifdef GHW_BUS8
     #define  PW 0x0  /* r,g,b */
   #else
     #define  PW 0x2  /* rx,gb */
   #endif
#endif

/* Initialization table */
typedef struct
   {
   SGUCHAR index;
   SGUCHAR value;
   } S1D_REGS;

static GCODE S1D_REGS FCODE as1dregs[] =
   {
   { 0x2A,  0x00 },   /* Display Mode Register (default buffer,display off) */
   { 0x68,  0x00 },   /* Power Save Register (enter power save mode, clocks off)  */
   { 0x04,  0x17 },   /* PLL D-Divider Register                             */
   { 0x06,  0x51 },   /* PLL Setting Register 0                             */
   { 0x08,  0x01 },   /* PLL Setting Register 1                             */
   { 0x0C,  0x4A },   /* PLL N-Divider Register                             */
   { 0x12,  0x01 },   /* Clock Source Select Register                       */
   { 0x04,  0x97 },   /* PLL D-Divider Register                             */
   { 0x0E,  0x3F },   /* SS Control Register 0                              */

   { 0xfc,  0x0A },   /* PLL Register Programming Delay (in us)             */

   { 0x12,  0x82 },   /* Clock Source Select Register (internal PLL, SDCLK) */
   { 0x14,  PW | TFT18 },   /* LCD Panel Type Register (pixel data width, panel bus width) */
   { 0x16, (SGUCHAR)((GDISPW/8)-1) },   /* Horizontal Display Width Register HDISP */
   { 0x18,  GHW_HOR_NON_DISPLAY/2-1},   /* Horizontal Non-Display Period Register (min 4-max 512) */
   { 0x1A, (SGUCHAR)(GDISPH-1)},        /* Vertical Display Height Register 0 (bit 7-0) */
   { 0x1C, (SGUCHAR)((GDISPH-1)>>8) },  /* Vertical Display Height Register 1 (bit 9-8) */
   { 0x1E,  GHW_VER_NON_DISPLAY/2-1 },  /* Vertical Non-Display Period Register (min 4-max 512) */
   { 0x20,  HS | 0x0f },        /* PHS Pulse Width (HSW) Register                     */
   { 0x22,  GHW_HOR_OFFSET },   /* PHS Pulse Start Position (HPS) Register            */
   { 0x24,  VS | 0x02 },        /* PVS Pulse Width (VSW) Register                     */
   { 0x26,  GHW_VER_OFFSET },   /* PVS Pulse Start Position (VPS) Register            */
   { 0x28,  PCK | 0x0 },        /* PCLK Polarity Register                             */
   /* SDRAM setup, here 128 Mbits (1K refresh) */
   { 0x82,  0x03 },   /* SDRAM Control Register 0                           */
   { 0x8C,  0xFF },   /* SDRAM Refresh Counter Register 0                   */
   { 0x8E,  0x03 },   /* SDRAM Refresh Counter Register 1                   */
                      /* SDRAM Write Buffer Memory Size (rounded up to nearest 16K),
                        Note_ Internal memory is always 24 bit (3 bytes) pr pixel */
   { 0x90,  ((GDISPW*GDISPH*3)+((1<<13)-1)) >>13 },
   { 0x68,  0xE8 },   /* Power Save Register (reset internal logic)         */
   { 0x68,  0x00 },   /* Power Save Register (prepare normal operation)     */
   { 0x68,  0x01 },   /* Power Save Register (Start SDCLK,enter normal operation) */
   { 0x84,  0x82 },   /* SDRAM Status Register 0 [WRITEONLY]. Init SDRAM chip and start refresh */

   /* Display orientation (scan direction swapping) */
   #if   ( defined( GHW_MIRROR_HOR ) &&  defined( GHW_MIRROR_VER ))
   { 0x52,  0x08 | 0x1},   /* Input Mode Register (Transperant mode MUST be used) */
   #elif ( defined( GHW_MIRROR_HOR ) && !defined( GHW_MIRROR_VER ))
   { 0x52,  0x08 | 0x2 },   /* Input Mode Register (Transperant mode MUST be used) */
   #elif (!defined( GHW_MIRROR_HOR ) &&  defined( GHW_MIRROR_VER ))
   { 0x52,  0x08 | 0x3 },   /* Input Mode Register (Transperant mode MUST be used) */
   #else
   { 0x52,  0x08 | 0x0 },   /* Input Mode Register (Transperant mode MUST be used) */
   #endif

   /* Set color value used for transperant pixels */
   #if (GDISPPIXW == 16)
   { 0x54,  (SGUCHAR)(G_TRANSCOLOR>>8) }, /* Transperant RED */
   { 0x56,  (SGUCHAR)(G_TRANSCOLOR>>3) }, /* Transperant GREEN */
   { 0x58,  (SGUCHAR)(G_TRANSCOLOR<<3) }, /* Transperant BLUE */
   #else
   { 0x54,  (SGUCHAR)(G_TRANSCOLOR>>16)}, /* Transperant RED */
   { 0x56,  (SGUCHAR)(G_TRANSCOLOR>>8) }, /* Transperant GREEN */
   { 0x58,  (SGUCHAR)(G_TRANSCOLOR)    }, /* Transperant BLUE */
   #endif
   /* Default windows settings, to be overwritten later */
   { 0x5A,  0x00 },   /* Write Window X Start Position Register             */
   { 0x5C,  0x00 },   /* Write Window Start Position Register 0             */
   { 0x5E,  0x00 },   /* Write Window Start Position Register 1             */
   { 0x60,  0xC6 },   /* Write Window X End Position Register               */
   { 0x62,  0x77 },   /* Write Window Y End Position Register 0             */
   { 0x64,  0x03 },   /* Write Window Y End Position Register 1             */
   { 0x2C,  0x00 },   /* PIP1 Display Start Address Register 0              */
   { 0x2E,  0x80 },   /* PIP1 Display Start Address Register 1              */
   { 0x30,  0x1C },   /* PIP1 Display Start Address Register 2              */
   { 0x32,  0x00 },   /* PIP1 Window X Start Position Register              */
   { 0x34,  0x00 },   /* PIP1 Window Y Start Position Register 0            */
   { 0x36,  0x00 },   /* PIP1 Window Y Start Position Register 1            */
   { 0x38,  0xC6 },   /* PIP1 Window X End Position Register                */
   { 0x3A,  0x77 },   /* PIP1 Window Y End Position Register 0              */
   { 0x3C,  0x03 },   /* PIP1 Window Y End Position Register 1              */
   { 0x3E,  0x00 },   /* PIP2 Display Start Address Register 0              */
   { 0x40,  0x40 },   /* PIP2 Display Start Address Register 1              */
   { 0x42,  0x80 },   /* PIP2 Display Start Address Register 2              */
   { 0x44,  0x00 },   /* PIP2 Window X Start Position Register              */
   { 0x46,  0x00 },   /* PIP2 Window Y Start Position Register 0            */
   { 0x48,  0x00 },   /* PIP2 Window Y Start Position Register 1            */
   { 0x4A,  0xC6 },   /* PIP2 Window X End Position Register                */
   { 0x4C,  0x77 },   /* PIP2 Window Y End Position Register 0              */
   { 0x4E,  0x03 },   /* PIP2 Window Y End Position Register 1              */

   { 0x2A,  0x00 },   /* Display Mode Register (display buffer 0, normal buffer handling, display Off)    */
                      /* Updated later by ghw_dispon() */
   { 0x50,  0x80 }    /* Display Control Register [WRITEONLY] Activate new settings */
   };

/*
   Send a command
*/
#ifndef GHW_NOHDW

 #ifdef GHW_BUS8
  /* Write command byte */
  #define ghw_cmd(cmd)        sgwrby(GHWCMD, (SGUCHAR)(cmd))
  #define ghw_cmddat(dat)     sgwrby(GHWWR, (SGUCHAR)(dat))
  #define ghw_sta()          ((SGUCHAR) sgrdby(GHWRD))
 #else  /* GHW_BUS16 */
  #define ghw_cmd(cmd)        sgwrwo(GHWCMDW,(SGUINT)(cmd))
  #define ghw_cmddat(dat)     sgwrwo(GHWWRW, (SGUINT)(dat))
  #define ghw_sta()           ((SGUCHAR) sgrdwo(GHWRDW))
 #endif

#else

  #define ghw_cmd(cmd)    { /* Nothing */ }
  #define ghw_cmddat(dat) { /* Nothing */ }
  #define ghw_sta() 0

#endif /* GHW_NOHDW */

static GXT xbegin,xend;
/*
   Set the x,y windows range.
   Internal ghw function
   Takes advantages of the index register auto increment feature
*/
void ghw_set_xyrange(GXT xb, GYT yb, GXT xe, GYT ye)
   {
   xbegin = xb;
   xend = xe;
   #ifdef GHW_PCSIM
   ghw_set_xyrange_sim( xb, yb, xe, ye);
   #endif
   ghw_cmd(REG5A_WRITE_WIN_X_SP);

   #ifdef GHW_MIRROR_HOR
   ghw_cmddat((SGUCHAR)((((GDISPW-1)-xe) >> 2) & 0xfe));   /* X start mirrored / 4 */
   #else
   ghw_cmddat((SGUCHAR)((xb >> 2) & 0xfe));                /* X start / 4 */
   #endif

   #ifdef GHW_MIRROR_VER
   ghw_cmddat((SGUCHAR)((((GDISPH-1)-ye) >> 2)& 0xff));    /* Y start mirrored */
   ghw_cmddat((SGUCHAR)(((GDISPH-1)-ye) & 0x3));
   #else
   ghw_cmddat((SGUCHAR)((yb >> 2)& 0xff));                 /* Y start */
   ghw_cmddat((SGUCHAR)(yb & 0x3));
   #endif

   #ifdef GHW_MIRROR_HOR
   ghw_cmddat((SGUCHAR)((((GDISPW-1)-xb )>> 2) & 0xfe));   /* X end mirrored / 4 */
   #else
   ghw_cmddat((SGUCHAR)((xe >> 2) & 0xfe));                /* X end / 4 */
   #endif

   #ifdef GHW_MIRROR_VER
   ghw_cmddat((SGUCHAR)((((GDISPH-1)-yb) >> 2)& 0xff));    /* Y end mirrored */
   ghw_cmddat((SGUCHAR)(((GDISPH-1)-yb) & 0x3));
   #else
   ghw_cmddat((SGUCHAR)((ye >> 2) & 0xff));                /* Y end */
   ghw_cmddat((SGUCHAR)(ye & 0x3));
   #endif

   /* Index register now points on memory data port register REG66_MEM_DATA_PORT_0 */
   /* Next operation can be data write */
   }

void ghw_linestart(void)
   {
   GXT x;
   x = (xbegin & 0x7);
   while (x-- != 0)
      {
      ghw_auto_wr(G_TRANSCOLOR);
      }
   }

void ghw_lineend(void)
   {
   GXT x;
   x = (xend & 0x7);
   while (x++ != 0x7)
      {
      ghw_auto_wr(G_TRANSCOLOR);
      }
   }

/*
   Write databyte to controller (at current position) and increment
   internal xadr.

   Internal ghw function
*/
void ghw_auto_wr(GCOLOR dat)
   {
   #ifdef GHW_PCSIM
   ghw_autowr_sim( dat );
   #endif

   #ifndef GHW_NOHDW
   #if ((GDISPPIXW == 16) && defined( GHW_BUS8 ))
      sgwrby(GHWWR, (SGUCHAR )(dat >> 8));
      sgwrby(GHWWR, (SGUCHAR )(dat));
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS8 ))
      /* 8+8+8 + format conversion  */
      sgwrby(GHWWR, (SGUCHAR)(dat>>16)); /* r */
      sgwrby(GHWWR, (SGUCHAR)(dat>>8));  /* g */
      sgwrby(GHWWR, (SGUCHAR)(dat));     /* b */
   #elif ((GDISPPIXW == 16) && defined( GHW_BUS16 ))
      /* 16 bit bus mode, 16 bit color */
      sgwrwo(GHWWRW, (SGUINT) dat);
   #elif ((GDISPPIXW == 24) && defined( GHW_BUS16 ))
      /* 16 bit bus mode 2, 24 bit color */
      sgwrwo(GHWWRW, (SGUINT)(dat>>16)); /* xxxxxxxx,rrrrrrrr */
      sgwrwo(GHWWRW, (SGUINT) dat);      /* gggggggg,bbbbbbbb */
   #else
       #error Illegal GDISPPIXW / GHW_BUSn combination in gdispcfg.h
   #endif
   #endif /* GHW_NOHDW */
   }

/***********************************************************************/
/**        S1D13517 Initialization and error handling functions       **/
/***********************************************************************/

/*
   Change default (palette) colors
*/
void ghw_setcolor(GCOLOR fore, GCOLOR back)
   {
   /* Update active colors */
   GHW_COLORCHECK(fore);
   GHW_COLORCHECK(back);
   ghw_def_foreground = fore;
   ghw_def_background = back;
   }

/*
   Convert an RGB structure to a color value using the current color mode
*/
GCOLOR ghw_rgb_to_color( GCONSTP GPALETTE_RGB *palette )
   {
   if (palette == NULL)
      return 0;

   return G_RGB_TO_COLOR(palette->r,palette->g,palette->b);
   }

#if (GHW_PALETTE_SIZE > 0)
/*
   Load a new palette or update the existing palette
   (Palette is only used with symbols using 2 or 4 bits pr pixel)
*/
SGBOOL ghw_palette_wr(SGUINT start_index, SGUINT num_elements, GCONSTP GPALETTE_RGB PFCODE *palette)
   {
   if ((num_elements == 0) ||
       ((start_index + num_elements) > 16) ||
       (palette == NULL))
      {
      glcd_err = 1;
      return 1;
      }
   glcd_err = 0;

   /* (Partial) update of operative palette values */
   while(num_elements-- > 0)
      {
      /* Make local palette copy here to be compatible with compilers
         having a non-standard conforming handling of pointer
         (i.e when PFCODE memory qualifer is used) */
      GPALETTE_RGB pal;
      GCOLOR color;
      pal.r = palette->r;
      pal.g = palette->g;
      pal.b = palette->b;
      color = ghw_rgb_to_color(&pal);

      GHW_COLORCHECK(color);  // Make sure the palette does not contain the transperant color

      ghw_palette_opr[start_index++] = color;
      palette++;

      /* ghw_palette_opr[start_index++] = ghw_rgb_to_color(&palette++); */
      }

   return glcd_err;
   }
#endif


/*
   Select the frame buffer to show on the screen
*/
void ghw_bufsel(SGUCHAR bufnum)
   {
   SGUCHAR tmp;
   ghw_cmd(REG2A_DSP_MODE);
   tmp = ghw_sta();
   ghw_cmd(REG2A_DSP_MODE);
   ghw_cmddat((tmp & 0xf) | (bufnum << 4));

   // Activate changed screen settings
   ghw_cmd(REG50_DISPLAY_CONTROL);
   ghw_cmddat(0x80);

   // Prepare registers for data write (just in case)
   ghw_cmd(REG66_MEM_DATA_PORT_0);

   #ifdef GHW_PCSIM
   ghw_bufsel_sim(bufnum);
   #endif
   }

/*
   Select the frame buffer to work on and update
   (May be different from the frame buffer shown on screen)
*/
void ghw_wrbufsel(SGUCHAR bufnum)
   {
   SGUCHAR tmp;
   ghw_cmd(REG52_INPUT_MODE);
   tmp = ghw_sta();
   ghw_cmd(REG52_INPUT_MODE);
   ghw_cmddat((tmp & 0xf) | (bufnum << 4));

   // Prepare registers for data write (just in case)
   ghw_cmd(REG66_MEM_DATA_PORT_0);
   #ifdef GHW_PCSIM
   ghw_wrbufsel_sim(bufnum);
   #endif
   }

/*
   Fast set or clear of LCD module RAM buffer
   Internal ghw function
*/
static void ghw_bufset(GCOLOR color)
   {
   /* Use hardware accelerator logic */
   GXT x; GYT y;
   GHW_COLORCHECK(color);
   ghw_set_xyrange(0,0,GDISPW-1,GDISPH-1);
   for (y=0; y < GDISPH; y++)
      {
      for(x=0; x < GDISPW; x++)
         {
         ghw_auto_wr(color);  /* Set LCD buffer */
         #ifdef GBUFFER
         gbuf[x+y*GDISPW] = color; /* Set ram buffer as well */
         #endif
         }
      }
   }

/*
   Initialize display, clear ram  (low-level)
   Clears glcd_err status before init

   Return 0 if no error,
   Return != 0 if some error
*/
SGBOOL ghw_init(void)
   {
   SGUINT i;

   #ifdef GBUFFER
   iltx = 1;
   ilty = 1;
   irbx = 0;
   irby = 0;
   ghw_upddelay = 0;
   #endif

   glcd_err = 0;
   ghw_io_init(); /* Set any hardware interface lines, controller hardware reset */

   #if (defined( GHW_ALLOCATE_BUF) && defined( GBUFFER ))
   if (gbuf == NULL)
      {
      /* Allocate graphic ram buffer */
      if ((gbuf = (GCOLOR *)calloc(ghw_gbufsize(),1)) == NULL)
         glcd_err = 1;
      else
         gbuf_owner = 1;
      }
   #endif

   if (glcd_err != 0)
      return 1;

   #ifdef GHW_PCSIM
   /* Tell simulator about the visual LCD screen organization */
   ghw_init_sim( GDISPW, GDISPH );
   #endif
   /* Set default colors */
   ghw_setcolor( GHW_PALETTE_FOREGROUND, GHW_PALETTE_BACKGROUND );
   #if (GHW_PALETTE_SIZE > 0)
   /* Load palette */
   ghw_palette_wr(0, sizeof(ghw_palette)/sizeof(GPALETTE_RGB), (GCONSTP GPALETTE_RGB PFCODE *)&ghw_palette[0]);
   #endif

   /*
      Stimuli test loops for initial oscilloscope test of display interface bus signals
      Uncomment to use the test loop for the given data bus width.
      It is recommended to check all display bus signals with each of the I/O access
      statements in the loop one by one.
   */
   /*
   #ifdef GHW_BUS8
   for(;;)
      {
      SGUCHAR dat;
      sgwrby(GHWCMD,0xff);
      sgwrby(GHWWR,0x00);
      dat = sgrdby(GHWSTA);
      dat = sgrdby(GHWRD);
      }
   #endif
   #ifdef GHW_BUS16
   for(;;)
      {
      SGUINT dat;
      sgwrwo(GHWCMDW,0xffff);
      sgwrwo(GHWWRW,0x0000);
      dat = sgrdwo(GHWSTAW);
      dat = sgrdwo(GHWRDW);
      }
   #endif
   */

   #ifndef GHW_NOHDW
   /* Check read of chip version register */
   ghw_cmd(REG00_PROD_CODE); /* Product code register */
   i = (SGUINT) ghw_sta();
   if ((i & 0xfc) != 0xB8)
      {
      /* Illegal product ID returned
      (Check the chip, cable or power connections) */
      G_WARNING("Illegal display controller product ID returned");  /* Test Warning message output */
      glcd_err = 1;
      return 1;
      }
   #endif

   /* Initialize controller according to configuration file */
   for (i=0; i < sizeof(as1dregs)/sizeof(S1D_REGS); i++)
      {
      if (as1dregs[i].index > 0xf0)
         {
         SGUINT dly = as1dregs[i].value*10;
         SGUCHAR tmp = 0;
         while(--dly > 0);
            {
            tmp |= ghw_sta(); /* Use status register read as delay element */
            }
         }
      else
         {
         ghw_cmd(as1dregs[i].index);
         ghw_cmddat(as1dregs[i].value);
         }
      }

   ghw_dispon(); /* Turn display on here to ease debug */

   /*ghw_bufset( G_RED ); */
   /*ghw_bufset( G_GREEN ); */
   ghw_bufset( ghw_def_background );

   ghw_dispon();

   #ifndef GNOCURSOR
   ghw_cursor = GCURSIZE1;    /* Cursor is off initially */
   /* ghw_cursor = GCURSIZE1 | GCURON; */ /* Uncomment to set cursor on initially */
   #endif

   ghw_updatehw();  /* Flush to display hdw or simulator */

   return (glcd_err != 0) ? 1 : 0;
   }


/*
   Return last error state. Called from applications to
   check for LCD HW or internal errors.
   The error state is reset by ghw_init and all high_level
   LCD functions.

   Return == 0 : No errors
   Return != 0 : Some errors
*/
SGUCHAR ghw_err(void)
   {
   #if (defined(_WIN32) && defined( GHW_PCSIM))
   if (GSimError())
      return 1;
   #endif
   return (glcd_err == 0) ? 0 : 1;
   }


/*
   Display a (fatal) error message.
   The LCD display module is always cleared and initialized to
   the system font in advance.
   The error message is automatically centered on the screen
   and any \n characters in the string is processed.

   str = ASCII string to write at display center
*/
void ghw_puterr( PGCSTR str )
   {
   PGCSTR idx;
   SGUINT xcnt;
   GXT xp;
   GYT yp,h,y, sidx;
   PGSYMBYTE psym;
   GCOLOR pval;
   SGUCHAR val;
   #ifdef GBUFFER
   GBUFINT gbufidx;
   #endif

   if (ghw_init() != 0)  /* (Re-) initialize display */
      return;            /* Some initialization error */

   /* Count number of lines in string */
   idx=str;
   if (idx == NULL)
      return;
   xcnt = 1;
   while(*idx)
      {
      if (*(idx++) == '\n')
         xcnt++;
      }

   /* Set start character line */
   h = SYSFONT.symheight;
   yp = (xcnt*h > GDISPH) ? 0 : ((GDISPH-1)-xcnt*h)/2;
   /* Set character height in pixel lines */

   idx=str;
   do
      {
      xcnt=0;  /* Set start x position so line is centered */
      while ((idx[xcnt]!=0) && (idx[xcnt]!='\n') && (xcnt < GDISPBW))
         {
         xcnt++;
         }

      /* Calculate start position for centered line */
      xp = (GDISPW-xcnt*SYSFONT.symwidth)/2;

      /* Display text line */
      while (xcnt-- > 0)
         {
         /* Point to graphic content for character symbol */
         psym = &(sysfontsym[(*idx) & 0x7f].b[0]);
         ghw_set_xyrange(xp,yp,xp+SYSFONT.symwidth-1,yp+(h-1));

         /* Display rows in symbol */
         for (y = 0; y < h; y++)
            {
            /* Get symbol row value */
            val = *psym++;
            /* Initiate LCD controller address pointer */
            #ifdef GBUFFER
            gbufidx = GINDEX(xp, (GBUFINT)yp+y );
            #endif
            ghw_linestart();
            /* Display colums in symbol row */
            for (sidx = 0; sidx < SYSFONT.symwidth; sidx++)
               {
               if ((val & sympixmsk[sidx]) != 0)
                  pval = ghw_def_foreground;
               else
                  pval = ghw_def_background;

               /* End of symbol or end of byte reached */
               #ifdef GBUFFER
               gbuf[gbufidx++] = pval;
               #endif
               ghw_auto_wr(pval);
               }
            ghw_lineend();
            }

         idx++;
         xp += SYSFONT.symwidth; /* Move to next symbol in line */
         }

      /* Next text line */
      yp += h;
      if (*idx == '\n')
         idx++;
      }
   while ((*idx != 0) && (yp < GDISPH));

   ghw_updatehw();  /* Flush to display hdw or simulator */
   }

void ghw_exit(void)
   {
   #if defined( GHW_ALLOCATE_BUF)
   if (gbuf != NULL)
      {
      if (gbuf_owner != 0)
         {
         /* Buffer is allocated by ginit, so release graphic buffer here */
         free(gbuf);
         gbuf_owner = 0;
         }
      gbuf = NULL;
      }
   #endif
   ghw_io_exit();         /* Release any LCD hardware resources, if required */
   #ifdef GHW_PCSIM
   ghw_exit_sim(); /* Release simulator resources */
   #endif
   }

#ifndef GNOCURSOR
/*
   Replace cursor type data (there is no HW cursor support in S1D13517)
*/
void ghw_setcursor( GCURSOR type)
   {
   ghw_cursor = type;
   #ifdef GHW_ALLOCATE_BUF
   if (gbuf == NULL)
      glcd_err = 1;
   #endif
   }
#endif


/*
   Turn display off
*/
void ghw_dispoff(void)
   {
   #ifndef GHW_NOHDW
   SGUCHAR tmp;
   #endif
   #ifdef GHW_PCSIM
   ghw_dispoff_sim();
   #endif

   #ifndef GHW_NOHDW
   ghw_cmd(REG2A_DSP_MODE);
   tmp = ghw_sta();
   ghw_cmd(REG2A_DSP_MODE);
   ghw_cmddat(tmp & 0xfe);
   #endif
   }

/*
   Turn display on
*/
void ghw_dispon(void)
   {
   #ifndef GHW_NOHDW
   SGUCHAR tmp;
   #endif
   #ifdef GHW_PCSIM
   ghw_dispon_sim();
   #endif

   #ifndef GHW_NOHDW
   ghw_cmd(REG2A_DSP_MODE);
   tmp = ghw_sta();
   ghw_cmd(REG2A_DSP_MODE);
   ghw_cmddat(tmp | 0x01);
   #endif
   }

#if defined( GHW_ALLOCATE_BUF)
/*
   Size of buffer requied to save the whole screen state
*/
GBUFINT ghw_gbufsize( void )
   {
   return (GBUFINT) GBUFSIZE * sizeof(GCOLOR) + (GBUFINT) sizeof(GHW_STATE);
   }

#ifdef GSCREENS
/*
   Check if screen buf owns the screen ressources.
*/
SGUCHAR ghw_is_owner( SGUCHAR *buf )
   {
   return (((GCOLOR *)buf == gbuf) && (gbuf != NULL)) ? 1 : 0;
   }

/*
   Save the current state to the screen buffer
*/
SGUCHAR *ghw_save_state( SGUCHAR *buf )
   {
   GHW_STATE *ps;
   if (!ghw_is_owner(buf))
      return NULL;

   ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);
   ps->upddelay = (ghw_upddelay != 0);
   #ifndef GNOCURSOR
   ps->cursor = ghw_cursor;
   #endif
   ps->foreground = ghw_def_foreground; /* Palette may vary, save it */
   ps->background = ghw_def_background;
   return (SGUCHAR *) gbuf;
   }

/*
   Set state to buf.
   If buffer has not been initiated by to a screen before, only
   the pointer is updated. Otherwise the the buffer
*/
void ghw_set_state(SGUCHAR *buf, SGUCHAR doinit)
   {
   if (gbuf != NULL)
      {
      /* The LCD controller has been initiated before */
      if (gbuf_owner != 0)
         {
         /* Buffer was allocated by ginit, free it so screen can be used instead*/
         free(gbuf);
         gbuf_owner = 0;
         gbuf = NULL;
         }
      }

   if (doinit != 0)
      {
      /* First screen initialization, just set buffer pointer and
         leave rest of initialization to a later call of ghw_init() */
      gbuf = (GCOLOR *) buf;
      gbuf_owner = 0;
      }
   else
      {
      if ((gbuf = (GCOLOR *) buf) != NULL)
         {
         GHW_STATE *ps;
         ps = (GHW_STATE *)(&gbuf[GBUFSIZE]);

         #ifndef GNOCURSOR
         ghw_cursor = ps->cursor;
         #endif
         ghw_upddelay = 0;        /* Force update of whole screen */
         iltx = 0;
         ilty = 0;
         irbx = GDISPW-1;
         irby = GDISPH-1;
         ghw_updatehw();
         ghw_upddelay = (ps->upddelay != 0) ? 1 : 0;
         /* Restore drawing color */
         ghw_setcolor(ps->foreground, ps->background);
         }
      }
   }
#endif  /* GSCREENS */
#endif  /* GHW_ALLOCATE_BUF */

#endif /* GBASIC_INIT_ERR */



