/***************************** moyo_mono8_8.c ************************

   moyo_mono8_8 font table structure definitions.
   This file has been auto-generated with the IconEdit / FontEdit tool.

   Copyright(c) RAMTEX 1998-2009

*****************************************************************/
#include <gdisphw.h>

/* Symbol table entry with fixed sized symbols */
static struct
   {
   GSYMHEAD sh;        /* Symbol header */
   SGUCHAR b[8];       /* Symbol data, "variable length" */
   }
GCODE FCODE moyo_mono8_8sym[128] =
   {
   #include "moyo_mono8_8.sym" /* Include symbols */
   };

/* Font structure */
GCODE GFONT FCODE moyo_mono8_8 =
   {
   8,       /* averange width */
   8,       /* height */
   sizeof(moyo_mono8_8sym[0]) - sizeof(GSYMHEAD), /* number of bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL)moyo_mono8_8sym, /* pointer to array of SYMBOLS */
   128,      /* num symbols */
   (PGCODEPAGE)(NULL)
   };

