/**************************** narrow10_w.c ***********************

   narrow10_w font table and code page structure definitions.
   This file has been auto-generated with the IconEdit / FontEdit tool.

   Copyright(c) RAMTEX 1998-2009

*****************************************************************/
#include <gdisphw.h>

/* Code page entry (one codepage range element) */
static struct
   {
   GCPHEAD chp;
   GCP_RANGE cpr[1];     /* Adjust this index if more codepage segments are added */
   }
GCODE FCODE narrow10_wcp =
   {
   #include "narrow10_w.cp" /* Codepage table */
   };

/* Symbol table entry with fixed sized symbols */
static struct
   {
   GSYMHEAD sh;         /* Symbol header */
   SGUCHAR b[10];       /* Symbol data, "variable length" */
   }
GCODE FCODE narrow10_wsym[240] =
   {
   #include "narrow10_w.sym" /* Include symbols */
   };

/* Font structure */
GCODE GFONT FCODE narrow10_w =
   {
   6,       /* averange width */
   10,       /* height */
   sizeof(narrow10_wsym[0]) - sizeof(GSYMHEAD), /* number of bytes in a symbol (including any alignment padding)*/
   (PGSYMBOL)narrow10_wsym, /* pointer to array of SYMBOLS */
   240,      /* num symbols */
   (PGCODEPAGE)&narrow10_wcp
   };

