//! \file    cobs_codec.h
//! \author  DRY
//! \date    29.07.2013
//! \brief   Cobs encoder, with internal buffering. Note the implementation should enable you to
//!          avoid extra buffering & unneccessary copying - e.g. the encoder can be used straight
//!          from ISR (but dont' have to).

#ifndef CM3F_COBS_H_
#define CM3F_COBS_H_

#include <cstdint>
//only if you use it
#include <string>

namespace cm3f
{

class cobs_flags
{
  public:
    enum byte_order  { LSB, MSB };
    static byte_order native_byte_order();
};

/* Usage examples :  Please see cobs_codec_test::pass() for an example of use */

class cobs_decoder
{
  public:

    cobs_decoder(unsigned bfsz=63)
      : buff_p_(new std::uint8_t[bfsz+1]), buff_pend_p_(buff_p_+bfsz+1)
      //+1 bs internally we write extra 0
    {  reset_();   }

    ~cobs_decoder() { delete [] buff_p_; }

    //Re-set to initial/unused state
    void reset() { reset_(); }

    //Put next received byte from incoming COBS stream (e.g. serial), excluding pkt separator 0x00.
    //Important: first call /must start with COBS hdr.
    bool put(std::uint8_t b);

    //Current data length, including decoded and partial message
    unsigned data_length() const {
      if (buff_p_ >= curr_data_p_) return 0;
      return curr_data_p_ - buff_p_ - 1;  //-1 remove extra 0x00
    }

    //True when a cobs message has been decoded. Not true while busy decoding one.
    //(so block_ready() if at least one msg been decoded).
    bool block_ready() const  { return blk_ready_; }

    //Raw access to internal buffer, e.g. for copying.(Note: index not checked)
    std::uint8_t operator[](unsigned idx) const  {   return buff_p_[idx];    }

    //Note: the extracting calls do minimal check if data ready, up to user to check messages
    //been decoded / data ready.
    friend cobs_decoder& operator>>(cobs_decoder& cde, std::uint8_t& u8);
    friend cobs_decoder& operator>>(cobs_decoder& cde, std::uint16_t& u16);
    friend cobs_decoder& operator>>(cobs_decoder& cde, std::uint32_t& u32);
    //Change the byte order when deserializing (affects u16,u32). The default after reset is MSB!
    friend cobs_decoder& operator>>(cobs_decoder& cde, cobs_flags::byte_order bo);

    //!!!NOTE for >>(string):
    //  --> Unformatted extraction, till NIL or end of buffer. First and last NIL's will be dropped;
    //  --> Caller must make sure has enough heap, as entire buffer could be copied;
    //[TODO(may be): fixed sized string]
    friend cobs_decoder& operator>>(cobs_decoder& cde, std::string& str);

    //For convenience: allows to get data at a known index without extracting it with >>. (e.g. you
    //un/stuffing a protocol and want to check a field before passing entire message higher up).
    //Note: unlike byte operator[] these check for out-of-bounds index. Note the byte order flag.
    bool at(unsigned idx, std::uint16_t& u16, cobs_flags::byte_order bo=cobs_flags::MSB) const;
    bool at(unsigned idx, std::uint32_t& u32, cobs_flags::byte_order bo=cobs_flags::MSB) const;
    bool at(unsigned idx, std::uint8_t& u8) const; //Safe version of []

    //Set absolure reading position. Returns previous. Invalid pos is ignored.
    unsigned read_pos(unsigned pos);
    //Set the reading position for ops >> relative to current. Returns current/last reading pos.
    //Out of bounds pos will be ignored.
    unsigned read_pos(int pos=0);

  protected:

    void reset_()
    {
      cobs_ = 0x00;
      curr_data_p_ = buff_p_;
      append0_ = false;
      blk_ready_ = 0;
      r_idx_ = 0;
      bo_ = cobs_flags::MSB;
    }

  private:

    std::uint8_t* const buff_p_;
    std::uint8_t* const buff_pend_p_;
    std::uint8_t* curr_data_p_;
    std::uint8_t  cobs_;
    bool append0_;
    bool blk_ready_;
    unsigned r_idx_; //absolute read idx
    cobs_flags::byte_order bo_;
};

class cobs_encoder
{
  public:

    //Calculate compile time your required static buffer size
    template<unsigned bsize>struct calc_cobs_buf
    {
      //1%, rounded up to whole num. Here always +1.
      //(Note: review this if have very large buffers,
      // because COBS needs only ~0.5% overhead)
      enum { SIZE = bsize + bsize/100 + 1 };
    };

    cobs_encoder(unsigned bfsz=calc_cobs_buf<63>::SIZE)
      :buff_p_(new std::uint8_t[bfsz]), buff_pend_p_(buff_p_+bfsz)
    {   reset_();    }

    ~cobs_encoder() { delete [] buff_p_; }

    //Put/add new data. Returns false if can't put more in.
    virtual bool put(std::uint8_t b);

    //Re-set to initial/unused state
    virtual void reset() { reset_(); }

    //Required when you want to finilize/close the current encoded buffer so far.
    //Important: shall ALWAYS be called before any use of encoded buffer.
    void end_block();

    unsigned data_length() const {
      if (curr_data_p_ > buff_pend_p_) //when finish()ed with 0
        return buff_pend_p_ - buff_p_ - 1;
      return curr_data_p_ - buff_p_ - 1;
    }

    //Raw access to internal buffer, e.g. for copying.(Note: index not checked)
    std::uint8_t operator[](unsigned idx) const  {   return buff_p_[idx];    }

    //Convenience for stuffing your basic data in
    friend cobs_encoder& operator<< (cobs_encoder& cbe, std::uint8_t);
    friend cobs_encoder& operator<< (cobs_encoder& cbe, std::uint16_t);
    friend cobs_encoder& operator<< (cobs_encoder& cbe, std::uint32_t);
    //Change the byte order when serializing (affects u16,u32). The default after reset is MSB!
    friend cobs_encoder& operator<<(cobs_encoder& cbe, cobs_flags::byte_order bo);

    //!!!NOTE for >>(char* str):
    //  --> Unformatted stuffing, till NIL in str or end of internal buffer;
    //  --> NIL will be inserted after string end. This helps >>(string) in decoder, but starts new
    //     cobs block and will be inefficient if you << many small strings.
    //(See cobs_streambuf_encoder if you want formatted string as input and output)
    friend cobs_encoder& operator<< (cobs_encoder& cbe, const char* cstr);

  protected:

    void reset_()
    { //reset all as empty buffer
      cobs_p_ = buff_p_;
      curr_data_p_ = buff_p_+1;
      cobs_ = 0x01;
      bo_ = cobs_flags::MSB;
    }
    void finish_block_(std::uint8_t b)
    {
      *cobs_p_ = b;
      cobs_p_ = curr_data_p_++;
      cobs_ = 0x01;
    }
    void set_f_();

  private:

    std::uint8_t* const buff_p_;
    std::uint8_t* const buff_pend_p_;
    std::uint8_t* curr_data_p_;
    std::uint8_t* cobs_p_;
    std::uint8_t  cobs_;
    cobs_flags::byte_order bo_;
};

//This class allows you to wrap it with say ostream or fstream, and thus do formated string output
//to encoder. E.g.:   ostr << "Hello bla bla" << 1234 << 5.457.  Note that all data in buffer will
//be string formatted data (in contrast if you just use  >>,<< directly on enc/dec)
class cobs_streambuf_encoder
  : public std::basic_streambuf<char>
{
  public:

    cobs_streambuf_encoder(cobs_encoder& cbe) : cbe_(cbe){}

  private:

    cobs_encoder& cbe_;

    virtual int sync()
    {
      return 0;
    }
    virtual int_type overflow(int_type c)
    {
      typedef std::basic_streambuf<char>::traits_type traits;
      int_type const eof = traits::eof();
      if (traits::eq_int_type(c, eof))
      {
        return traits::not_eof(c);
      }
      else
      {
        char_type const ch = traits::to_char_type(c);
        bool pok =  cbe_.put(ch);
        return pok ? c : eof;
      }
    }
};

//Use this to check cobs code works
class cobs_codec_test
{
  public:
    cobs_codec_test(){}
    ~cobs_codec_test(){}

    bool pass();
};

} //namespace cm3f

#endif // CM3F_COBS_H_
