//! \file    fixed_strings.h
//! \author  DRY
//! \date    17.07.2013
//! \brief   Nothing but type helpers if you fancy a fixed allocator

#ifndef CM3F_FIXED_STRINGS_H
#define CM3F_FIXED_STRINGS_H

#include "fixed_allocator.h"
#include <string>
#include <sstream>

namespace cm3f
{

template<unsigned BSIZE> class fbca {
  public: typedef fixed_block_allocator<char, BSIZE> type;
};

template<unsigned BSIZE> class string {
  public:  typedef typename fbca<BSIZE>::type fbca_type;
    typedef std::basic_string<char, std::char_traits<char>, fbca_type> type;
};

template<unsigned BSIZE> class stringstream {
    public: typedef typename fbca<BSIZE>::type fbca_type;
      typedef std::basic_stringstream<char, std::char_traits<char>, fbca_type> type;
};

/// --------- Some pre-defined typedef examples, may be of use ---------
typedef string<32>::type string32;
typedef string<64>::type string64;
typedef string<128>::type string128;

typedef stringstream<32>::type stringstream32;
typedef stringstream<64>::type stringstream64;
typedef stringstream<128>::type stringstream128;

} // namespace cm3f

#endif // CM3F_FIXED_STRINGS_H
