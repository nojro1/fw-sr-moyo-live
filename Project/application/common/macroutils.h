//! \file    macroutils.h
//! \author  DRY
//! \date    29.04.2010

//! \brief Collection of useful macros (preprocessor stuff only)

#ifndef  MACRO_UTILS_H_
#define  MACRO_UTILS_H_

#define STRINGIFY(str) #str
#define TOSTRING(str) STRINGIFY(str)

#define FIXBUG_MSG(str) \
  "\n *** FIX_ME *** : " __FILE__ "," TOSTRING(__LINE__)" : \n\t >>>" \
    str " <<<\n"

#define BUG_MSG(str) \
  "\n *** CAUGHT A BUG *** : " __FILE__ "," TOSTRING(__LINE__)" : \n\t >>>" \
    str " <<<\n"

#define TODO_MSG(str) \
  "\n ~~ TODO ~~ : " __FILE__ "," TOSTRING(__LINE__)" : \n\t >" \
    str " <\n"  

#define MODULE_FIX_MSG(str) \
  "\n * Broken modularity * : " __FILE__ "," TOSTRING(__LINE__)" : \n\t >>>" \
    str " <<<\n"

#define PANIC_MSG(str) \
  "\n !!! PANIC !!! : " __FILE__ "," TOSTRING(__LINE__)" : \n\t >>>" \
    str " <<<\n"

#define WARN_MSG(str) \
  "\n ~ WARNING WARNING WARNING ~ : " __FILE__ "," TOSTRING(__LINE__)" : \n\t >>>" \
    str " <<<\n"

#define OBSOLETE_MSG(str) \
  "\n - OBSOLETE CODE - : " __FILE__ "," TOSTRING(__LINE__)" : \n\t >>>" \
    str " <<<\n"

#if defined(__IAR_SYSTEMS_ICC__)
  #define CC_MESSAGE(str) \
    _Pragma ( TOSTRING( message (str)) )
#elif defined(_MSC_VER)
  #define CC_MESSAGE(str) \
    __pragma( message (str) )
#else
  #define CC_MESSAGE(str) 
#endif

#endif // MACRO_UTILS_H_ 
