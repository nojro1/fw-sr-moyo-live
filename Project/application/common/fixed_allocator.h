//! \file    fixed_allocator.h
//! \author  DRY [taken from web & adapted ]
//! \date    17.07.2013
//! \brief   A fixed allocator - can be in particular use when want to use std::string but have
//!          constrained resources. See fixed_block_alocator, and example string typedefs at end.
//!          [Note: only tested with string, see fixed_string.h]

#ifndef CM3F_FIXED_ALLOCATOR_H
#define CM3F_FIXED_ALLOCATOR_H

#include <stdexcept>
#include <algorithm>
#include <functional>
#if __cplusplus >= 201103L //<= 199711L ours
  #include <type_traits>
#endif
#include <string>

namespace cm3f
{

//**********************************************************************
// fixed_allocator
//**********************************************************************
template <typename T, const std::size_t MAX_SIZE>
class fixed_allocator
{
  private:

    static const bool FREE   = false;
    static const bool IN_USE = true;

  public:

    typedef T                  value_type;
    typedef value_type *       pointer;
    typedef const value_type * const_pointer;
    typedef value_type &       reference;
    typedef const value_type &  const_reference;
    typedef std::size_t        size_type;
    typedef std::ptrdiff_t     difference_type;

    //*********************************************************************
    // rebind
    //*********************************************************************
    template<typename U>
    struct rebind
    {
      typedef fixed_allocator<U, MAX_SIZE>
      other;
    };

    //*********************************************************************
    // Constructor
    //*********************************************************************
    fixed_allocator()
      : p_first_free(in_use)
    {
      initialise();
    }

    //*********************************************************************
    // Copy constructor
    //*********************************************************************
    fixed_allocator(const fixed_allocator &rhs)
      : p_first_free(in_use)
    {
      initialise();
    }

    //*********************************************************************
    // Templated copy constructor
    //*********************************************************************
    template<typename U>
    fixed_allocator(const fixed_allocator<U, MAX_SIZE>&rhs)
        : p_first_free(in_use)
    {
      initialise();
    }

    //*********************************************************************
    // Destructor
    //*********************************************************************
    ~fixed_allocator()
    {
    }

    //*********************************************************************
    // address
    //*********************************************************************
    pointer address(reference x) const
    {
      return (&x);
    }

    //*********************************************************************
    // address
    //*********************************************************************
    const_pointer address(const_reference x) const
    {
      return (x);
    }

    //*********************************************************************
    // allocate
    // Allocates from the internal array.
    //*********************************************************************
    pointer allocate(size_type n, const_pointer cp = 0)
    {
      // Pointers to the 'in_use' flags.
      bool *p_first     = p_first_free;
      bool *const p_end = &in_use[MAX_SIZE];

      // 'Find first fit' allocation algorithm, starting from the first free slot.
      // If n == 1 then we already have the free slot address or p_end.
       if (n == 1)
      {
        // No space left?
        if (p_first == p_end)
        {
#if defined(__IAR_SYSTEMS_ICC__)
          _RAISE(std::bad_alloc());
#else
          throw std::bad_alloc();
#endif
        }

        // Mark the slot as 'in use'
        *p_first = IN_USE;
      }
      else
      {
        // Search for a big enough range of free slots.
        p_first = std::search_n(p_first, p_end, static_cast<long>(n), FREE);

        // Not enough space found?
        if (p_first == p_end)
        {
#if defined(__IAR_SYSTEMS_ICC__)
          _RAISE(std::bad_alloc());
#else
          throw std::bad_alloc();
#endif
        }

        // Mark the range as 'in use'
        std::fill(p_first, p_first + n, IN_USE);
      }

      // Update the 'first free' pointer if necessary.
      if (p_first == p_first_free)
      {
        // Find the next free slot or p_end
        p_first_free = std::find(p_first + n, p_end, FREE);
      }

      // Return the memory allocation.
      const std::size_t offset = std::distance(in_use, p_first) * sizeof(value_type);

      return (reinterpret_cast<pointer>(&p_buffer[offset]));
    }

    //*********************************************************************
    // deallocate
    // Clears the 'in_use' flags for the deallocated items.
    //*********************************************************************
    void deallocate(pointer p, size_type n)
    {
      // Find the start of the range.
      std::size_t index = std::distance(p_buffer, reinterpret_cast<char *>(p)) / sizeof(value_type);

      bool *p_first = &in_use[index];

      // Mark the range as 'free'.
      if (n == 1)
      {
        *p_first = FREE;
      }
      else
      {
        std::fill(p_first, p_first + n, FREE);
      }

      // Update the 'first free' pointer if necessary.
      if (p_first < p_first_free)
      {
        p_first_free = p_first;
      }
    }

    //*********************************************************************
    // max_size
    // Returns the maximum size that can be allocated in total.
    //*********************************************************************
    size_type max_size() const
    {
      return (MAX_SIZE);
    }

    //*********************************************************************
    // construct
    // Constructs an item.
    //*********************************************************************
    void construct(pointer p, const value_type &x)
    {
      // Placement 'new'
      new (p)value_type(x);
    }

    //*********************************************************************
    // destroy
    // Destroys an item.
    //*********************************************************************
    void destroy(pointer p)
    {
      // Call the destructor.
      p->~value_type();
    }

  private:

#if __cplusplus >= 201103L
    enum
    {
      ALIGNMENT = std::tr1::alignment_of<T>::value - 1
    };
#else
    enum
    {
      ALIGNMENT = 0
    };
#endif

    //*********************************************************************
    // initialise
    // Initialises the internal allocation buffers.
    //*********************************************************************
    void initialise()
    {
      // Ensure alignment.
      p_buffer = reinterpret_cast<char *>(
                   (reinterpret_cast<std::size_t>(&buffer[0]) + ALIGNMENT) & ~ALIGNMENT );

      // Mark all slots as free.
      std::fill(in_use, in_use + MAX_SIZE, FREE);
    }

    // Disabled operator.
    void operator =(const fixed_allocator &);

    // The allocation buffer. Ensure enough space for correct alignment.
    //char buffer[(MAX_SIZE * sizeof(value_type)) + ALIGNMENT + 1];
    //DRY: I removed above line as didn't check how that ALIGNMENT works and why +1
    char buffer[(MAX_SIZE * sizeof(value_type))];

    // Pointer to the first valid location in the buffer after alignment.
    char *p_buffer;

    // The flags that indicate which slots are in use.
    bool in_use[MAX_SIZE];

    // Pointer to the first free slot.
    bool *p_first_free;
};

//*********************************************************************
// operator ==
// Equality operator.
//*********************************************************************
template<typename T, const std::size_t MAX_SIZE>
inline bool operator ==(const fixed_allocator<T, MAX_SIZE> &,
                        const fixed_allocator<T, MAX_SIZE> &)
{
  return (false);
}

//*********************************************************************
// operator !=
// Inequality operator.
//*********************************************************************
template<typename T, const std::size_t MAX_SIZE>
inline bool operator !=(const fixed_allocator<T, MAX_SIZE> &, 
                        const fixed_allocator<T, MAX_SIZE> &)
{
  return (true);
}


//**********************************************************************
// fixed_block_allocator
//**********************************************************************
template <typename T, const std::size_t MAX_SIZE>
class fixed_block_allocator
{
  public:
    typedef T                  value_type;
    typedef value_type *       pointer;
    typedef const value_type * const_pointer;
    typedef value_type &       reference;
    typedef const value_type & const_reference;
    typedef std::size_t        size_type;
    typedef std::ptrdiff_t     difference_type;

#if __cplusplus >= 201103L
    enum
    {
      // The numbers of buffers to use. Varies according to the type.
      NUMBER_OF_BUFFERS = std::tr1::has_trivial_destructor<T>::value ? 1 : 2
    };
#else
    enum
    {
      NUMBER_OF_BUFFERS  = 1
    };
#endif

    //*********************************************************************
    // rebind
    //*********************************************************************
    template<typename U>
    struct rebind
    {
      typedef fixed_block_allocator<U, MAX_SIZE> other;
    };

    //*********************************************************************
    // Constructor
    //*********************************************************************
    fixed_block_allocator()
        : buffer_id(0)
    {
      initialise();
    }

    //*********************************************************************
    // Copy constructor
    //*********************************************************************
    fixed_block_allocator(const fixed_block_allocator &rhs)
        : buffer_id(0)
    {
      initialise();
    }

    //*********************************************************************
    // Templated copy constructor
    //*********************************************************************
    template<typename U>
    fixed_block_allocator(const fixed_block_allocator<U, MAX_SIZE> &rhs)
        : buffer_id(0)
    {
      initialise();
    }

    //*********************************************************************
    // Destructor
    //*********************************************************************
    ~fixed_block_allocator()
    {
    }

    //*********************************************************************
    // address
    //*********************************************************************
    pointer address(reference x) const
    {
      return (&x);
    }

    //*********************************************************************
    // address
    //*********************************************************************
    const_pointer address(const_reference x) const
    {
      return (x);
    }

    //*********************************************************************
    // allocate
    // Allocates from the internal array.
    // If storage cannot be allocated then std::bad_alloc() is thrown.
    //*********************************************************************
    pointer allocate(size_type     n,
                     const_pointer cp = 0)
    {
      // Just too big?
      if (n > MAX_SIZE)
      {
#if defined(__IAR_SYSTEMS_ICC__)
        _RAISE(std::bad_alloc());
#else
        throw std::bad_alloc();
#endif
      }

      // Get the next buffer.
      ++buffer_id;
      buffer_id %= NUMBER_OF_BUFFERS;

      // Always return the beginning of the buffer.
      return (reinterpret_cast<pointer>(p_buffer[buffer_id]));
    }

  //*********************************************************************
  // deallocate
  // Does nothing.
  //*********************************************************************
  void deallocate(pointer   p,
                  size_type n)
  {
  }

  //*********************************************************************
  // max_size
  // Returns the maximum size that can be allocated in total.
  //*********************************************************************
  size_type max_size() const
  {
    return (MAX_SIZE);
  }

  //*********************************************************************
  // construct
  // Constructs an item.
  //*********************************************************************
  void construct(pointer          p,
                 const value_type &x)
  {
    new (p)value_type(x);
  }

  //*********************************************************************
  // destroy
  // Destroys an item.
  //*********************************************************************
  void destroy(pointer p)
  {
    p->~value_type();
  }

  private:

#if __cplusplus >= 201103L
    enum
    {
      ALIGNMENT = std::tr1::alignment_of<T>::value - 1 // The alignment of the buffers - 1
    };
#else
    enum
    {
      ALIGNMENT = 0
    };
#endif

    //*********************************************************************
    // initialise
    // Initialises the internal allocation buffers.
    //*********************************************************************
    void initialise()
    {
      // Ensure alignment.
      for (int i = 0; i < NUMBER_OF_BUFFERS; ++i)
      {
        p_buffer[i] = reinterpret_cast<char *>(
                        (reinterpret_cast<std::size_t>(&buffer[i][0]) + ALIGNMENT) & ~ALIGNMENT );
      }
    }

    // Disabled operator.
    void operator =(const fixed_block_allocator &);

    // The allocation buffers. Ensure enough space for correct alignment.
    //char buffer[NUMBER_OF_BUFFERS][(MAX_SIZE * sizeof(value_type)) + ALIGNMENT + 1];
    //DRY: Note i disabled above as I didn't check how the ALIGNMENT works and why + 1
    //(e.g. if I want to reservce 32 for string, with the ALIGNMENT = 0 you get buff sz 33 - badone
    char buffer[NUMBER_OF_BUFFERS][(MAX_SIZE * sizeof(value_type))];

    // Pointers to the first valid locations in the buffers after alignment.
    char *p_buffer[NUMBER_OF_BUFFERS];

    // The index of the currently allocated buffer.
    int buffer_id;
};

//*********************************************************************
// operator ==
// Equality operator.
//*********************************************************************
template<typename T, const std::size_t MAX_SIZE>
inline bool operator ==(const fixed_block_allocator<T, MAX_SIZE> &,
                        const fixed_block_allocator<T, MAX_SIZE> &)
{
  return (false);
}

//*********************************************************************
// operator !=
// Inequality operator.
//*********************************************************************
template<typename T, const std::size_t MAX_SIZE>
inline bool operator !=(const fixed_block_allocator<T, MAX_SIZE> &,
                        const fixed_block_allocator<T, MAX_SIZE> &)
{
  return (true);
}

} //namespace cmf3

#endif //CM3F_FIXED_ALLOCATOR_H
