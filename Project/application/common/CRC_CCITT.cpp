#include "CRC_CCITT.h"

//! \brief Helper method for updating the CRC for each byte\n
//! CRC polynom = x^16 + x^12 + x^5 + 1 = 0x1021 (CCITT-CRC16)
void CRC_CCITT::crcUpdate(uint16_t& crc, uint8_t val)
{
    //lint ++fpm
    crc = static_cast<uint8_t>(crc >> 8) | (crc << 8);
    crc ^= val;
    crc ^= static_cast<uint8_t>(crc & 0xff) >> 4;
    crc ^= (crc << 12);         //was "crc ^= (crc << 8) << 4;" in MRX puck, since 8-bit cpu
    crc ^= ((crc & 0xff) << 5); //was "crc ^= ((crc & 0xff) << 4) << 1;" in MRX puck, since 8-bit cpu
    //lint --fpm
}

//! \brief Calculates CRC from the contents of a byte array buffer
//! \param pBuf pointer to buffer
//! \param len length of buffer
//! \return the CRC, 16 bits unsigned
uint16_t CRC_CCITT::calcCrc(uint8_t *pBuf, std::size_t len)
{
    uint16_t crc = CRC_CCITT::CRC_INIT;
    uint8_t *pEnd = pBuf + len;

    while (pBuf < pEnd)
    {
        crcUpdate(crc, *pBuf++);
    }
    return crc;
}


