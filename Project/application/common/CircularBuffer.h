//! \file    CircularBuffer.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    24.05.2011


//! @class CircularBuffer
//! @brief Template class for implementation of a circular buffer (ringbuffer)
//! @brief Based on code from belowmentioned header, fixed to compile on IAR, now with lower case method names
//! @author GIT
//!

/****************************************************************************
*
*
*   DSP Goodies
*   Version 1.1 | Last update Jan 2007 - Alessandro Gallo
*
*   http://ag-works.net/
*
*
*   "CircularBuffer.h"
*   Implementation of a circular buffer
*
*   - <typename T> specifies the samples type (i.e. use <float> for VST)
*
*   You can use or modify without limitation this source code for your projects.
*   This code is provided "as is", I don't offer any warranties, nor I give
*   support for it. Use it at your own risk.
*
*   If you redistribute this file, you must leave this top comment unaltered.
*   If you improve this code, indicate it below, and make me know with an e-mail
*   (http://ag-works.net/contacts.htm).
*
*   Good work!
*   Alessandro Gallo
*
*
****************************************************************************/


#ifndef __CIRCULARBUFFER_H
#define __CIRCULARBUFFER_H

#include <memory>
#include <algorithm>

/*!
 * \brief
 * Implements a circular buffer.
 *
 * \param T
 * Data type for the contained items.
 *
 */
template<typename T> class CircularBuffer
{

    /*!
     * \brief
     * Pointer to the internal memory buffer.
     */
    T* m_pBuffer;

    /*!
     * \brief
     * Allocated size (in items).
     */
    int m_nSize;

    /*!
     * \brief
     * Number of elements stored in the buffer.
     */
    int m_nItems;

    /*!
     * \brief
     * Index of current feed position.
     */
    int m_iWriteNext;

    /*!
     * \brief
     * Index of next read position.
     */
    int m_iReadNext;

    /*!
     * \brief
     * High Watermark.
     */
    int m_iHighWatermark;


public:

    /*!
     * \brief
     * Default constructor.
     *
     * Constructs an empty circular buffer object.
     *
     * \see
     * CircularBuffer(int)
     */
    CircularBuffer()
        : m_pBuffer(0)
        , m_nSize(0)
        , m_nItems(0)
        , m_iWriteNext(0)
        , m_iReadNext(0)
        , m_iHighWatermark(0) {}


    /*!
     * \brief
     * Constructor.
     *
     * Constructs a circular buffer object of the specified size.
     *
     * \param nSize
     * Initial size (in items) of the circular buffer object.
     *
     * \see
     * CircularBuffer()
     */
    CircularBuffer(int nSize)
        : m_nSize(nSize)
        , m_nItems(0)
        , m_iWriteNext(0)
        , m_iReadNext(0)
        , m_iHighWatermark(0)
    {
        m_pBuffer = (T*)new T[nSize];
        std::memset(m_pBuffer, 0, m_nSize * sizeof(T));
    }

    /*!
     * \brief
     * Destructor.
     */
    ~CircularBuffer()
    {
        if(m_pBuffer)
            delete[] m_pBuffer;
    }

    /*!
     * \brief
     * Sets the size for this circular buffer object.
     *
     * \param nSize
     * New size (in mumber of items) for this circular buffer object.
     *
     * \remarks
     * All of the previous content of the circular buffer is lost.
     *
     */
    void create(int nSize)
    {
        if(nSize != m_nSize)
        {
            m_nSize = nSize;
            m_nItems = 0;
            m_iWriteNext = 0;
            m_iReadNext = 0;
            m_iHighWatermark = 0;

            if(m_pBuffer)
                delete[] m_pBuffer;

            m_pBuffer = (T*)new T[m_nSize];
            std::memset(m_pBuffer, 0, m_nSize * sizeof(T));
        }
        else
        {
            reset();
        }
    }

    /*!
     * \brief
     * Deallocates all of the resources taken up by this object.
     *
     * \remarks
     * All of the previous content of the circular buffer is lost.
     */
    void destroy()
    {
        m_nSize = 0;
        m_nItems = 0;
        m_iWriteNext = 0;
        m_iReadNext = 0;
        m_iHighWatermark = 0;

        if(m_pBuffer) {
            delete[] m_pBuffer;
            m_pBuffer = 0;
        }
    }

    /*!
     * \brief
     * Empties this circular buffer object.
     *
     * \remarks
     * The size of this buffer and the allocated memory remain untouched.
     */
    void reset()
    {
        m_nItems = 0;
        m_iWriteNext = 0;
        m_iReadNext = 0;
        m_iHighWatermark = 0;
    }

    /*!
     * \brief
     * Tells the maximum number of items that this circular buffer can store.
     *
     * \returns
     * Maximum number of items that this circular buffer can store.
     */
    inline int getMaximumSize() const {
        return m_nSize;
    }

    /*!
     * \brief
     * Tells the actual number of items stored in this circular buffer.
     *
     * \returns
     * The actual number of items stored in this circular buffer.
     */
    inline int getNumItems() const {
        return m_nItems;
    }


    /*!
     * \brief
     * Tells the number of items which can still be stored in this circular buffer.
     *
     * \returns
     * The number of items which can still be stored in this circular buffer.
     */
    inline int getRoom() const {
        return m_nSize - m_nItems;
    }

    /*!
     * \brief
     * Get the high watermark of this circular buffer.
     *
     * \returns
     * The highest number of items which was ever stored in this circular buffer.
     */
    inline int getHighWatermark() const {
        return m_iHighWatermark;
    }

    /*!
     * \brief
     * Get the high watermark of this circular buffer and reset it after reading.
     *
     * \returns
     * The highest number of items which was ever stored in this circular buffer.
     */
    inline int getAndResetHighWatermark() {
        int wm = m_iHighWatermark;
        m_iHighWatermark = 0;
        return wm;
    }

    /*!
     * \brief
     * Get the high watermark (in percent) of this circular buffer.
     *
     * \returns
     * The highest number of items which was ever stored in this circular buffer.
     */
    inline int getHighWatermarkPercent() const {
        return ( 100*getHighWatermark() ) / getMaximumSize();
    }


    /*!
     * \brief
     * Stores a single item in the circular buffer.
     *
     * \param x
     * Item to be stored.
     *
     * \see
     * Put(const T*, int)
     */
    void put(const T& x)
    {
        // Copy the sample to the first free location
        m_pBuffer[m_iWriteNext] = x;
        ++m_nItems;
      #ifndef NDEBUG
        m_iHighWatermark = std::max(m_nItems, m_iHighWatermark);
      #endif
        // Update write index
        m_iWriteNext++;
        m_iWriteNext %= m_nSize;

        // If we have filled the buffer, move to next read position
        if(m_nItems > m_nSize)
        {
            m_nItems = m_nSize;
            m_iReadNext++;
            m_iReadNext %= m_nSize;
        }
    }

    /*!
     * \brief
     * Retrieves the oldest item in the circular buffer.
     *
     * \param item
     * [out] Takes the value of the retrieved item.
     *
     * \returns
     * true if the item was successfully retrieved, false whether the buffer is empty.
     *
     * \see
     * get(T*, int)
     */
    bool get(T& item)
    {
        if(m_nItems)
        {
            item = m_pBuffer[m_iReadNext++];
            m_iReadNext %= m_nSize;
            --m_nItems;
            return true;
        }
        else
        {
            m_iReadNext = m_iWriteNext;
            return false;
        }
    }

    /*!
     * \brief
     * Retrieves the oldest item in the circular buffer.
     *
     * \returns
     * The value of the retrieved item.
     *
     * \remark
     * Wraparounds to start if end of buffer reached\n
     * This is a special case: Will always return a value, even if no values have been inserted
     */
    T get()
    {
        T item = m_pBuffer[m_iReadNext++];
        m_iReadNext %= m_nSize;
        if (m_nItems)
        {
            --m_nItems;
        }
        return item;
    }

    /*!
     * \brief
     * Stores a set of items in the circular buffer.
     *
     * \param pBlock
     * Pointer to a buffer containing the items to be stored.
     *
     * \param nSize
     * Number of items to which pBlock points to.
     *
     * \see
     * put(const T&)
     */
    void put(const T* pBlock, int nSize)
    {
      #ifndef NDEBUG
        m_iHighWatermark = std::max(m_nItems+nSize, m_iHighWatermark);
      #endif
        // If it doesn't fit, take last m_nSize samples
        if(nSize > m_nSize)
        {
            pBlock = &pBlock[nSize - m_nSize];  // start later, in order to get last m_nSize samples
            nSize = m_nSize;                    // limit the length to the buffer's length
        }

        // Wrap around buffer end
        if(m_iWriteNext + nSize > m_nSize)
        {
            // Fill the end of the buffer
            std::memcpy((m_pBuffer + m_iWriteNext), pBlock, (m_nSize - m_iWriteNext) * sizeof(T));

            // Restart filling from the beginning of the buffer
            std::memcpy(m_pBuffer, (pBlock + m_nSize - m_iWriteNext), (nSize - m_nSize + m_iWriteNext) * sizeof(T));

            // Update feed position
            m_iWriteNext = nSize + m_iWriteNext - m_nSize;
        }
        else
        {
            // Copy the entire pBlock to the first free location
            std::memcpy((m_pBuffer + m_iWriteNext), pBlock, nSize * sizeof(T));

            // Update feed position
            m_iWriteNext = (m_iWriteNext + nSize) % m_nSize;
        }

        // More items in the buffer
        m_nItems += nSize;

        // If we have filled the buffer, move to next read position
        if(m_nItems > m_nSize)
        {
            m_iReadNext += m_nItems - m_nSize;
            m_iReadNext %= m_nSize;
            m_nItems = m_nSize;
        }
    }


    /*!
     * \brief
     * Retrieves the oldest n items in the circular buffer.
     *
     * \param pBlock
     * Pointer to a buffer which receives the wanted items.
     *
     * \param n
     * Number of items to be retrieved.
     *
     * \returns
     * The number of items which were succesfully retrieved.
     * The returned value is always <= n, 0 if the buffer is empty.
     *
     * \see
     * get(T&)
     */
    int get(T* pBlock, int n)
    {
        // Cannot give away more than we have!
        n = (n <= m_nItems) ? n : m_nItems;

        if((m_iReadNext + n) <= m_nSize)
        {
            std::memcpy(pBlock, m_pBuffer + m_iReadNext, n * sizeof(T));
            m_iReadNext = (m_iReadNext + n) % m_nSize;
        }
        else
        {
            int nPart1 = m_nSize - m_iReadNext;
            int nPart2 = n - nPart1;

            std::memcpy(pBlock,          m_pBuffer + m_iReadNext,    nPart1 * sizeof(T));    // copy tail
            std::memcpy(pBlock + nPart1, m_pBuffer,                  nPart2 * sizeof(T));    // copy from beginning

            m_iReadNext = nPart2;
        }

        m_nItems -= n;

        if (0 == m_nItems)
        {
            m_iReadNext = m_iWriteNext;
        }

        return n;
    }

};

#endif // #ifndef __CIRCULARBUFFER_H

//======== End of CircularBuffer.h  ==========

