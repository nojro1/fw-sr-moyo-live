//! \file    CRC-SAE-J1850.h
//! \version 1.0
//! \date 04.08.2010

//! \brief SAE-J1850 Cyclic Redundancy Check byte (CRC)
//!        (developed for network protocol for use in automobiles)

#ifndef CRC_SAE_J1850_H_
#define CRC_SAE_J1850_H_

/*Please create this file which shall include the stdint.h correctly for your platform.*/
#include "stdintinc.h"

#ifndef CRC8_SAE_POLY
  #define CRC8_SAE_POLY  0x1D  /* Poly:  u^8+u^4+u^3+u^2+1 */
  /* Ref Wiki for list of used SAE poly's*/
#endif

#if defined (__cplusplus)
extern "C" {
#endif

uint8_t crc8_sae(const uint8_t *data, int length);

/* NOTE: this version reverses the bits before crc'ing. 
(E.g: see Nanopad Mk1 code, used in of use in EM4134 rfid SPI proto)*/
uint8_t crc8_sae_r(const uint8_t *data, int length);

#if defined(__cplusplus)
}
#endif

#endif /* CRC_SAE_J1850_H_ */
