//! \file    cobs_codec.cpp
//! \author  DRY
//! \date    29.07.2013
//! \brief   Cobs encoder

#include "cobs_codec.h"

#if defined(__IAR_SYSTEMS_ICC__)
  #ifndef BYTE_ORDER_LITTLE_ENDIAN
    #define BYTE_ORDER_LITTLE_ENDIAN  __LITTLE_ENDIAN__
  #endif
#else
  #error You need to define your platform endianess
#endif

namespace cm3f
{

cobs_flags::byte_order
cobs_flags::native_byte_order()
{
#if (1==BYTE_ORDER_LITTLE_ENDIAN)
  return cobs_flags::LSB;
#else
  return cobs_flags::MSB;
#endif
}

bool
cobs_decoder::put(std::uint8_t b)
{
  if (curr_data_p_ < buff_pend_p_)
  {
    if (0x00==cobs_) //b=0x00 or cobs_=0x00 must be same time; so you always start with hdr=length
    {
      if (1==b)
      {
        *curr_data_p_++ = 0x00;
        blk_ready_ = true;
      }
      else
      {
        cobs_ = b-1; // cobs hdr = length
        if (0xff > cobs_)
          append0_=true;
        else
          append0_= false;
        blk_ready_ = false;
      }
    }
    else
    {
     --cobs_;
     *curr_data_p_++ = b;
      if (0x00==cobs_)//pkt end
      {
        blk_ready_= true;
        if (append0_)
          *curr_data_p_++ = 0x00;
      }
    }
    return true;
  }
  else
    return false; // buffer full
}

bool
cobs_encoder::put(std::uint8_t b)
{
  if (curr_data_p_ < buff_pend_p_)
  {
    if (0x00==b)
      finish_block_(cobs_);
    else
    {
      *curr_data_p_++ = b;
      ++cobs_;
      if (0xff ==cobs_)
        finish_block_(cobs_);
    }
    return true;
  }
  else
    return false; //full
}

void
cobs_encoder::end_block()
{
  if (cobs_p_ < buff_pend_p_)
    finish_block_(cobs_);
  //note: can't reset since need final encoded size after the call
}

//Note: currently no error flag is set if any of the << ops fail - you have to check elsewhere
//they've failed (have enogh space for example)
cobs_encoder&
operator << (cobs_encoder& cbe, std::uint8_t u8)
{
  cbe.put(u8);
  return cbe;
}
cobs_encoder&
operator << (cobs_encoder& cbe, std::uint16_t u16)
{
  if (cobs_flags::MSB == cbe.bo_)
    cbe.put( u16 >> 8) && cbe.put(  u16 );
  else
    cbe.put( u16) && cbe.put( u16 >>8);
  return cbe;
}
cobs_encoder&
operator<< (cobs_encoder& cbe, std::uint32_t u32)
{
  if (cobs_flags::MSB == cbe.bo_)
    cbe.put( u32 >> 24) && cbe.put( u32 >> 16) && cbe.put(u32 >>8 ) && cbe.put(u32);
  else
    cbe.put(u32) && cbe.put(u32 >>8 ) && cbe.put( u32 >> 16) && cbe.put( u32 >> 24);
  return cbe;
}
cobs_encoder&
operator << (cobs_encoder& cbe, const char* cstr)
{
  while(*cstr)
    if (!cbe.put(*cstr++))
      break;
  cbe.put('\0'); //thus ends it
  return cbe;
}
cobs_encoder&
operator << (cobs_encoder& cbe, cobs_flags::byte_order bo)
{
  cbe.bo_ = bo;
  return cbe;
}

cobs_decoder&
operator >>(cobs_decoder& cde, std::string& str)
{
  std::uint8_t* dptr = cde.buff_p_ + cde.r_idx_;
  if (0==*dptr) ++dptr; //don't start string with 0
  if (dptr < cde.buff_pend_p_)
  {
    do
    {
      str += *dptr++;
      ++cde.r_idx_;
    }while(*dptr && dptr < cde.buff_pend_p_);
    //Note: now WILL extract \0 if found, this is indended in <<,>> in cobs_en/decoder.
    if (0==*dptr) ++cde.r_idx_;
  }
  return cde;
}
cobs_decoder&
operator >> (cobs_decoder& cde, std::uint16_t& u16)
{
  std::uint8_t* dptr = cde.buff_p_ + cde.r_idx_;
  if (dptr + 1 < cde.buff_pend_p_)
  {
    if (cobs_flags::MSB == cde.bo_)
    {
      u16 = *dptr++ << 8;
      u16 |= *dptr;
    }
    else
    {
      u16 = *dptr++;
      u16 |= *dptr << 8;
    }
    cde.r_idx_+=2;
  }
  return cde;
}
cobs_decoder&
operator >> (cobs_decoder& cde, std::uint32_t& u32)
{
  std::uint8_t* dptr = cde.buff_p_ + cde.r_idx_;
  if (dptr + 3 < cde.buff_pend_p_)
  {
    if (cobs_flags::MSB == cde.bo_)
    {
      u32  = *dptr++ << 24;
      u32 |= *dptr++ << 16;
      u32 |= *dptr++ << 8;
      u32 |= *dptr;
    }
    else
    {
      u32  = *dptr++;
      u32 |= *dptr++ << 8;
      u32 |= *dptr++ << 16;
      u32 |= *dptr   << 24;
    }
    cde.r_idx_+=4;
  }
  return cde;
}
cobs_decoder&
operator >> (cobs_decoder& cde, std::uint8_t& u8)
{
  std::uint8_t* dptr = cde.buff_p_ + cde.r_idx_;
  if (dptr < cde.buff_pend_p_)
  {
    u8 = *dptr;
    ++cde.r_idx_;
  }
  return cde;
}
cobs_decoder&
operator >> (cobs_decoder& cde, cobs_flags::byte_order bo)
{
  cde.bo_ = bo;
  return cde;
}

bool
cobs_decoder::at(unsigned idx, std::uint16_t &u16, cobs_flags::byte_order bo) const
{
  if (buff_p_+idx+1 < buff_pend_p_)
  {
    if (cobs_flags::MSB == bo)
    {
      u16  = buff_p_[idx] << 8;
      u16 |= buff_p_[idx+1];
    }
    else
    {
      u16  = buff_p_[idx];
      u16 |= buff_p_[idx+1] << 8;
    }
    return true;
  }
  return false;
}
bool
cobs_decoder::at(unsigned idx, std::uint32_t &u32, cobs_flags::byte_order bo) const
{
  if (buff_p_+idx+3 < buff_pend_p_)
  {
    if (cobs_flags::MSB == bo)
    {
      u32  = buff_p_[idx]   << 24;
      u32 |= buff_p_[idx+1] << 16;
      u32 |= buff_p_[idx+2] << 8;
      u32 |= buff_p_[idx+3];
    }
    else
    {
      u32  = buff_p_[idx];
      u32 |= buff_p_[idx+1] << 8;
      u32 |= buff_p_[idx+2] << 16;
      u32 |= buff_p_[idx+3] << 24;
    }
    return true;
  }
  return false;
}
bool
cobs_decoder::at(unsigned idx, std::uint8_t &u8) const
{
  if (buff_p_+idx < buff_pend_p_)
  {
    u8 = buff_p_[idx];
    return true;
  }
  return false;
}

unsigned
cobs_decoder::read_pos(unsigned pos)
{
  unsigned prev_rpos = r_idx_;
  if (buff_p_+pos < buff_pend_p_)
    r_idx_ = pos;
  return prev_rpos;
}

unsigned
cobs_decoder::read_pos(int pos)
{
  if (0==pos)
    return r_idx_;

  unsigned curr_rpos = r_idx_;
  if (0>pos)
  {//if backward
    pos *= -1; //unless you want to use std::abs which casts to double first
    if (pos <= r_idx_)
      r_idx_ -= pos;
  }
  else
  {//if forward
    if (buff_p_+r_idx_+pos < buff_pend_p_)
      r_idx_ += pos;
  }
  return curr_rpos;
}

bool
cobs_codec_test::pass()
{
  //Create en/de coders, with default size internal buffers
  cobs_encoder cbe;
  cobs_decoder cde;
  //for deciding pass/fail
  bool t0_ok = false;
  bool t1_ok = false;

  {
    ///Test at least that what we put it is what we get out ...
    const char* HELLOMSG = "Hello, my name is Cobs";
    const char* NUM0MSG = "num0 is : ";
    const char* NUM1MSG = "and num1 is : ";
    const std::uint16_t NUM0 = 1234;
    const std::uint32_t NUM1 = 0xABCD;

    //Shuffle data in
    cbe << HELLOMSG << NUM0MSG << NUM0 << NUM1MSG << cobs_flags::LSB << NUM1;
    //Finalize  - no more data to prepare to send
    cbe.end_block();
    //Then data then sent by wire ...

    //Data is received by recepient, as in e.g. serial ISR in your code
    //while(have data..)
    // if (!cde.put( b )) ....

    for (int i=0; i < cbe.data_length(); ++i)
      if (!cde.put( cbe[i] ))
        return false;

    //Has user/app got a complete message to work with now
    if (!cde.block_ready())
      return false;

    std::string h, n0, n1;
    std::uint16_t rnum0;
    std::uint32_t rnum1;

    //Un-shuffle
    cde >> h >> n0 >> rnum0 >> n1 >> cobs_flags::LSB >> rnum1;
    //Now you could
    //cout << h << n0 << rnum0 << n1 << rnum1;

    if ( HELLOMSG == h && NUM0MSG == n0 && NUM0 == rnum0 && NUM1MSG == n1 && NUM1 == rnum1)
      t0_ok = true;
  }

  cbe.reset();
  cde.reset();

  {
    const int BUFFSZ=32;
    const char *send_buff = new char[BUFFSZ]; //may be some good junk alloc'ed

    for (int i=0; i < BUFFSZ; ++i)
      if (!cbe.put(send_buff[i]))
        break;
    cbe.end_block();
    for (int i=0; i < cbe.data_length(); ++i)
      if (!cde.put( cbe[i]))
        break;
    if (cde.block_ready() && BUFFSZ == cde.data_length() )
    {
      int i;
      for (i=0; i < BUFFSZ; ++i)
        if ( cde[i] != send_buff[i] )
          break;
      if (i == BUFFSZ)
        t1_ok = true;
    }

    cde >> cobs_flags::LSB;
    delete [] send_buff;
  }

  return t0_ok && t1_ok;
}

} //namespace cm3f
