//! \file    CRC-SAE-J1850.c
//! \version 1.0
//! \date 04.08.2010

//! \brief SAE-J1850 Cyclic Redundancy Check byte (CRC)
//!        (developed for network protocol for use in automobiles)

#include "CRC-SAE-J1850.h"

inline static uint8_t
rvb(uint8_t b)
{  
  uint8_t rb=0;
  for (uint8_t i=1, k=0x80; k>0;i<<=1, k>>=1) /* invert bit order */
    if (b&i)
      rb|=k;
  return  rb;
 /*Reverse bits prior to CRC'ing them. 7 operations.
 revb = 0xff & ( ((data[f] * 0x0802LU & 0x22110LU) | (data[f] * 0x8020LU & 0x88440LU)) * 0x10101LU >> 16 );
  */
}

#ifndef CRC8_SAE_POLY
  #define CRC8_SAE_POLY  0x1D  /* Poly:  u^8+u^4+u^3+u^2+1 */
  /* Ref Wiki for list of used SAE poly's*/
#endif

uint8_t
crc8_sae(const uint8_t *data, int length)
{
  /*Do 8-bit CCITT-CRC.*/  
  uint8_t crc = 0;
  int f,bit; 
  
  for (f = 0; f < length; ++f)
  {
    crc ^= data[f];
    for ( bit=0; bit < 8; bit++ ) 
    {
      if ( (crc & 0x80) != 0 )
      {
        crc <<=1;
        crc ^= CRC8_SAE_POLY;
      }
      else
        crc <<= 1;
    }
  }
  /* return (~crc)&0xFF  need this step?*/
  return crc;
}

/* NOTE: this version reverses the bits before crc'ing. 
(E.g: see Nanopad Mk1 code, used in of use in EM4134 rfid SPI proto)*/
uint8_t
crc8_sae_r(const uint8_t *data, int length)
{
  /*Do 8-bit CCITT-CRC.*/  
	uint8_t crc = 0, revb = 0;
	int f,bit; 
	
  for (f = 0; f < length; ++f)
  {
    revb = rvb( data[f] );
    crc ^= revb;
    for ( bit=0; bit < 8; bit++ ) 
    {
      if ( (crc & 0x80) != 0 )
      {
        crc <<=1;
        crc ^= CRC8_SAE_POLY;
      }
      else
        crc <<= 1;
    }
  }

  return rvb(crc);
}

