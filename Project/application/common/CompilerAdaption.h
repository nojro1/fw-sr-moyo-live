//
// Compiler adaption
//

#ifndef _COMPILER_ADAPTION_H_
#define _COMPILER_ADAPTION_H_

#if defined(__cplusplus)
  // platform specific namespace workaround
  #ifdef __embedded_cplusplus
    #define USING_NAMESPACE_STD // empty
  #else
    #define USING_NAMESPACE_STD using namespace std
  #endif //__embedded_cplusplus
#endif // __cplusplus


#ifdef _MSC_VER
  #if (_MSC_VER < 1700) /* Vs 2012 should have round */
    namespace std
    {
        template< typename T>
        T round(T number)
        {
            return number < 0.0 ? static_cast<T>(ceil(number - 0.5)) : static_cast<T>(floor(number + 0.5));
        }
    }
  #endif
#endif

#endif // _COMPILER_ADAPTION_H_