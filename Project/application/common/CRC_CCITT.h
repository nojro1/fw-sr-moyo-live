//! \class CRC_CCITT
//! \brief CRC calculations according to the CRC-16 CCITT standard\n
//! CRC polynom = x^16 + x^12 + x^5 +1 = 0x1021 (CCITT-CRC16)\n
//! Initial CRC value = 0xFFFF\n
// (originated from the LittleSister project)

#ifndef _CRC_CCITT_H_
#define _CRC_CCITT_H_

#ifdef _MSC_VER
  #if (_MSC_VER < 1600) /* Vs 2010 should have cstdint */
    #include "stdint.h"
  #else
    #include <cstdint>
  #endif
#else
  #include <cstdint>
#endif

#include <cstdlib>

#if defined(__cplusplus)
 #ifndef __embedded_cplusplus
  #ifdef _MSC_VER
   #if _MSC_VER > 1400
   using std::uint8_t;
   using std::uint16_t;
   #endif
  #else
   using std::uint8_t;
   using std::uint16_t;
  #endif
 #endif
#endif

class CRC_CCITT
{

public:
    static uint16_t calcCrc(uint8_t *pBuf, std::size_t len);
    static void crcUpdate(uint16_t& crc, uint8_t val);
    static const uint8_t CRC_LEN = sizeof(uint16_t);
    static const uint16_t CRC_INIT = 0xFFFFu; //!< initial CRC value according to CCITT-CRC16

private:

};

#endif //_CRC_CCITT_H_

