//! \file    ExpMovingAverage.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    15.12.2010

#ifndef _EXP_MOVING_AVERAGE_H
#define _EXP_MOVING_AVERAGE_H

#include <cassert>

//! \class ExpMovingAverage
//! \brief Template class for implementation of a exponential weighted moving average \n
//! (that does not need to remember old values)\n
//! Ref. http://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average \n
//! Implements the formula St = alpha*Yt + (1-alpha)*St-1 \n
//! Alpha is here expressed in terms of N time periods (i.e. N+1 samples), alpha thus becoming 2/(N+1) \n
//! For example, N = 19 (20 samples) is equivalent to alpha = 0.1
//! The half-life of the weights (the interval over which the weights decrease by a factor of two) \n
//! is approximately N/2.8854 (within 1% if N > 5)
//! \param T Data type for the moving average items
//!
template<typename T>
class ExpMovingAverage
{

public:


    //! \brief c'tor
    //! \param nSize The number of samples in the moving average window
    //! \param initValue Optional initial value for the MA (default 0)
    ExpMovingAverage(int nSize, const T& initValue=static_cast<T>(0))
    : m_alphaInverted(((nSize-1)+1)/2), m_MA(initValue),
      m_MA_summed(m_MA*m_alphaInverted), m_firstValueReceived( m_MA == 0 ? false : true)
    {
      assert(nSize > 1);
    }

    //! \brief Resets the moving average, and changes the number of samples in the moving average
    //! \param nSize The number of samples in the moving average window to use from now
    //! \param initValue Optional initial value for the MA (default 0)
    void reset(int nSize, const T& initValue=static_cast<T>(0))
    {
      assert(nSize > 1);
      m_alphaInverted = ((nSize-1)+1) / 2;
      m_MA = initValue;
      m_MA_summed = m_MA * m_alphaInverted;      
      m_firstValueReceived =  m_MA == 0 ? false : true;
    }

    //! \brief Resets the moving avarage
    //! \param initValue Optional initial value for the MA (default 0)
    void reset(const T& initValue=static_cast<T>(0))
    {
      m_MA = initValue;
      m_MA_summed = m_MA * m_alphaInverted;
      m_firstValueReceived =  m_MA == 0 ? false : true;
    }

    //! \brief Inserts a new value into the moving average and returns the new moving average
    //! \param value The new value to be inserted into the moving average
    //! \return The updated moving avarage
    const T& operator()(const T& value)
    {
      if (m_firstValueReceived)
      {
        m_MA_summed += value - m_MA;
      }
      else
      {
        m_firstValueReceived = true;
        m_MA_summed = value * m_alphaInverted;
      }
      return (m_MA = m_MA_summed / m_alphaInverted);
    }

    //! \brief Return current MA
    const T& operator()() const {  return m_MA;  }


protected:

private:

    int m_alphaInverted;
    T m_MA;
    T m_MA_summed;
    bool m_firstValueReceived;
};

#endif // #ifndef _EXP_MOVING_AVERAGE_H

//======== End of ExpMovingAverage.h  ==========

