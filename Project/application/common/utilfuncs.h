//
// Common utility functions and macros
//

#ifndef _UTILFUNCS_H_
#define _UTILFUNCS_H_

#ifndef _MSC_VER
  #include "CompilerAdaption.h"
#endif

#if __GNUC__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wuninitialized"
#pragma GCC diagnostic ignored "-Wunused-value"
#if __GNUC__ >= 4 && __GNUC_MINOR__ >=8
    #pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#endif
#endif

#ifdef __cplusplus

// ---------------------------------------------------------------------------------------------------
// Support for compile-time Assertions
// ===================================
// ref. Dr.Dobbs "Compile-Time Assertions & Debugging" December 2005
    template <bool> class OnlyTrue;
    template <> class OnlyTrue<true> {};
    #define CTA(X) sizeof(OnlyTrue<(bool)(X)>)

//  example of usage:
//  void foo() {CTA('enum-value' < ENUM_ABOVE_MAX_VALUE);}
//
// ---------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------
// Compile time Assertion2 -- Boost version
// =========================================
// If the above static assert does JACK with your compiler, try the Boost's one
// copied below.
#ifndef BOOST_STATIC_ASSERT_HPP
  #define BOOST_STATIC_ASSERT_HPP
  namespace boost
  {
    #define BOOST_JOIN( X, Y ) BOOST_DO_JOIN( X, Y )
    #define BOOST_DO_JOIN( X, Y ) BOOST_DO_JOIN2(X,Y)
    #define BOOST_DO_JOIN2( X, Y ) X##Y
  
    template <bool x> struct STATIC_ASSERTION_FAILURE;
  
    template <> struct STATIC_ASSERTION_FAILURE<true>{};
  
    template<int x> struct static_assert_test{};
  };
  
  #define BOOST_STATIC_ASSERT( B ) \
     typedef ::boost::static_assert_test<\
        sizeof(::boost::STATIC_ASSERTION_FAILURE< (bool)( B ) >)>\
           BOOST_JOIN(boost_static_assert_typedef_, __LINE__)
  
  // Usage example:  BOOST_STATIC_ASSERT( false );
  //  should generate 'incomplete type is not allowed' error on my IAR ICC
  // ---------------------------------------------------------------------------------------------------
#endif

// ===================================
// Support for byte swapping
// ===================================

// We define some constant for little, big and host endianess. Here I use
// BOOST_LITTLE_ENDIAN/BOOST_BIG_ENDIAN to check the host indianess. If you
// don't want to use boost you will have to modify this part a bit.

    #define BOOST_LITTLE_ENDIAN

    enum EEndian
    {
      LITTLE_ENDIAN_ORDER,
      BIG_ENDIAN_ORDER,
    #if defined(BOOST_LITTLE_ENDIAN)
      HOST_ENDIAN_ORDER = LITTLE_ENDIAN_ORDER
    #elif defined(BOOST_BIG_ENDIAN)
      HOST_ENDIAN_ORDER = BIG_ENDIAN_ORDER
    #else
      #error "Impossible to determine endianness"
    #endif
    };

    // this function swap the bytes of values given it's size as a template
    // parameter (could sizeof be used?).
    template <class T, unsigned int size>
    inline T SwapBytes(T value)
    {
      union
      {
         T value;
         char bytes[size];
      } in, out;

      in.value = value;

      for (unsigned int i = 0; i < size / 2; ++i)
      {
         out.bytes[i] = in.bytes[size - 1 - i];
         out.bytes[size - 1 - i] = in.bytes[i];
      }

      return out.value;
    }

    // Here is the function you will use. Again there is two compile-time assertion
    // that use the boost libraries. You could probably comment them out, if you
    // are cautious not to use this function for anything else than integers
    // types. This function need to be calles like this :
    //
    //     int x = someValue;
    //     int i = EndianSwapBytes<HOST_ENDIAN_ORDER, BIG_ENDIAN_ORDER>(x);
    //
    template<EEndian from, EEndian to, class T>
    inline T EndianSwapBytes(T value)
    {
      // Only 2, 4 or 8 octets
      BOOST_STATIC_ASSERT(sizeof(T) == 2 || sizeof(T) == 4 || sizeof(T) == 8);

      // Only arithmetic values
      // BOOST_STATIC_ASSERT(boost::is_arithmetic<T>::value);

      // Skip if no conversion needed
      if (from == to)
         return value;

      return SwapBytes<T, sizeof(T)>(value);
    }

// ---------------------------------------------------------------------------------------------------
//
// Safe ARR_ELEMS macro
// ====================
//
// From the Dr. Dobb's article "COUNTING ARRAY ELEMENTS AT COMPILE TIME", April 2007, Ivan J. Johnson
// (will give compile time error if something other than array is used as parameter)
//
// pseudo code:
//
// if x is not an array
//     issue a compile-time error
// else
//     use the traditional ARR_ELEMS expression: sizeof(x)/sizeof(x[0])
//

// public interface
#define ARR_ELEMS(x) (int)(                                            \
    0 * sizeof( reinterpret_cast<const ::Bad_arg_to_ARR_ELEMS*>(x) ) + \
    0 * sizeof( ::Bad_arg_to_ARR_ELEMS::check_type((x), &(x))      ) + \
    sizeof(x) / sizeof((x)[0]) )

// implementation details
class Bad_arg_to_ARR_ELEMS
{
    public:
        class Is_pointer; // intentionally incomplete type
        class Is_array {};
        template <typename T>
        static Is_pointer check_type(const T*, const T* const*);
        static Is_array check_type(const void*, const void*);
};
// ---------------------------------------------------------------------------------------------------

#else

// Macro returning the number of elements in an array
// This is often used to traverse an array in a loop e.g.
//    for (int i = 0; i < ARR_ELEMS(anArray); i++)
//    {
//        do something with the array
//    }
// The pre-processor will expand ARR_ELEMS to a constant number, thus
// making this more effective than using an inline template method
// (we normally do not want to use macros, but this is an exception)

#define ARR_ELEMS(x) (sizeof(x) / sizeof((x)[0]))

#endif //__cplusplus



/*
 * Reading an 8-bit byte in a 16-bit int
 *
 * This method cannot be used to alter a hi/lo byte, this needs pointers (as below)
 *
 * Example C:
 * uint16_t x;
 * uint8_t y;
 * x=0x1234;
 * y=HIBYTE(x);  	   *now y=0x12 - works for variables in any bank 0 to 3
 * y=LOBYTE(x);	       *now y=0x34
 *
 * LOBYTE(x)=0xaa;    *will not work :( - use pointers
 */

#ifndef HIBYTE
  #define HIBYTE(x)		((uint8_t)((x)>>8))
#endif

#ifndef LOBYTE
  #define LOBYTE(x)		((uint8_t)((x) & 0xFF))
#endif

#define HINIBBLE(charValue)	((charValue)>>4)
#define LONIBBLE(charValue)	((charValue)&0xf)

#define LSB_WORD(x)			((x) & 0x00FF)
#define MSB_WORD(x)			(((x) >> 8) & 0x00FF)

/*
 * given variable of any type (char, uchar, int, uint, long) it reads
 * the uint8_t residing at that memory location
 * for ints, byte1 is msb, byte0 is lsb (least significant byte)
 * for longs	byte3 is msb, byte0 is lsb
 *
 * NOTE: requires Little Endian CPU
 *
 * ie: sample C code
 *
 * 	uint16_t myint=0x4321;
 * 	uint32_t mylong=0x87654321;
 *
 *  for myint: 	BYTE1(myint)=0x43; (msb) and BYTE0(myint)=0x21; (lsb)
 *
 *  for mylong:	BYTE3(mylong)=0x87; (msb), BYTE2(mylong)=0x65;
 * 				BYTE2(mylong)=0x43; and BYTE0(mylong)=0x21; (lsb)
 *
 */

#define BYTE0(x)				(uint8_t)(*(((uint8_t *)&x)+0))
#define BYTE1(x)				(uint8_t)(*(((uint8_t *)&x)+1))
#define BYTE2(x)				(uint8_t)(*(((uint8_t *)&x)+2))
#define BYTE3(x)				(uint8_t)(*(((uint8_t *)&x)+3))

/*
 * 	given variable of any type (char, uchar, int, uint, long) it reads
 *	the int residing at that memory location
 *	ie: sample C code
 *
 * 	uint8_t array[4];
 *	uint16_t test;
 *	test=UINT_ATBYTEADDR(&array[0+2])		    //now test=0x8765
 *
 *	note: do NOT use &(array[0]+1) to reference the int stored at array[1] as it will
 *				reference the int after array[0] in pointer arithmetic.  This will
 *				result with the int at array[2].
 *
 *				Instead use &array[0+1] to reference the int at uchar array[1]
 */

#define	UINT_ATBYTEADDR(x)				(unsigned int)(*(((unsigned int *)x)))
#define	ULONG_ATBYTEADDR(x)				(unsigned long)(*(((unsigned long *)x)))
#define	UINT16_ATBYTEADDR(x)			(uint16_t)(*(((uint16_t *)x)))
#define	UINT32_ATBYTEADDR(x)			(uint32_t)(*(((uint32_t *)x)))

/*
 * Turning bits on/off according to mask
 * use ~0 instead of 0xFF, etc, because this ensures machine independence
 * if int changes from 16-bit to 32-bit
 * Example C:
 * x=0b001;
 * BITS_ON(x,0b100)  //now x=0b101
 */

#define BITS_ON(var,mask) ((var) |= (mask))
#define BITS_OFF(var,mask) ((var) &= ~0 ^ (mask))

// other bit functions
#define  TESTBIT(var, bit)   ((var) & (1 <<(bit)))
#define  SETBIT(var, bit)    ((var) |= (1 << (bit)))
#define  CLRBIT(var, bit)    ((var) &= ~(1 << (bit)))


// General macros
#ifndef MIN
  #define MIN(x,y) (((x)<=(y)) ? (x) : (y))
#endif
#ifndef MAX
  #define MAX(x,y) (((x)>=(y)) ? (x) : (y))
#endif

#define SETMAX(max,x) if ( (x)>(max) ) (max)=(x)
#define SETMIN(min,x) if ( (x)<(min) ) (min)=(x)

/*This was only tested on IAR compiler*/
#if !defined(_CSTDINT_) && !defined(_MSC_STDINT_H_) 
  #if defined(__cplusplus)
    #include <cstdint>
  #else
    #include <stdint.h>
  #endif
#endif
/* ---------- */

#if defined(__cplusplus)
 #if !defined(__embedded_cplusplus) && !defined(_MSC_VER)
  using std::uint8_t; using std::uint16_t;
 #endif
#endif

typedef struct
{
	uint8_t bit0	:1;
	uint8_t bit1	:1;
	uint8_t bit2	:1;
	uint8_t bit3	:1;
	uint8_t bit4	:1;
	uint8_t bit5	:1;
	uint8_t bit6	:1;
	uint8_t bit7	:1;
	uint8_t bit8	:1;
	uint8_t bit9	:1;
	uint8_t bit10	:1;
	uint8_t bit11	:1;
	uint8_t bit12	:1;
	uint8_t bit13	:1;
	uint8_t bit14	:1;
	uint8_t bit15	:1;
} SixteenBits_t;

typedef struct
{
	uint8_t bit0	:1;
	uint8_t bit1	:1;
	uint8_t bit2	:1;
	uint8_t bit3	:1;
	uint8_t bit4	:1;
	uint8_t bit5	:1;
	uint8_t bit6	:1;
	uint8_t bit7	:1;
} EightBits_t;

typedef struct
{
	uint8_t low;
	uint8_t high;
} TwoBytes_t;

/*
 * NOTE: it is not recommended that unions are used to reference hi/lo bytes or
 * bits of a variable.  Use >>8 or &FF or pointers instead, as above.  It makes
 * passing variables to a function difficult, as the function must be defined to
 * accept variables of the same union.  Then, the function will no longer accept
 * normally defined variables.
 *
 * these two structures allow access to 2 byte word, high and low bytes of variable
 * declaration:	union wordtype x;
 * usage:	x.word=0xABCD; x.byte.high=0xAB; x.byte.low=0xCD;
 * 				x.part.bit15=1; (msb), x.part.bit0=1; (lsb)
 * declaration:	union chartype x;
 * usage:	x.byte=0xAB;
 * 				x.part.bit7=1; (msb), x.part.bit0=1; (lsb)
 */

typedef union
{
	uint16_t		word;
	TwoBytes_t		byte;
	SixteenBits_t	part;
} WordType_t;

typedef union
{
	uint8_t	byte;
	EightBits_t		part;
} CharType_t;


// Arfinn's bit macros
// -------------------
#define BITBOOL(x) (!(!(x)))
#define bit_set(   arg, posn ) ((arg) |= (1L << (posn)))
#define bit_test(  arg, posn ) BITBOOL((arg) & (1L << (posn)))

// Bit definitions
#define BIT_0   1
#define BIT_1   2
#define BIT_2   4
#define BIT_3   8
#define BIT_4   16
#define BIT_5   32
#define BIT_6   64
#define BIT_7   128
#define BIT_8   256
#define BIT_9   512
#define BIT_10  1024
#define BIT_11  2048
#define BIT_12  4096
#define BIT_13  8192
#define BIT_14  16384
#define BIT_15  32768

#if __GNUC__
#pragma GCC diagnostic pop
#endif

#endif // _UTILFUNCS_H_


