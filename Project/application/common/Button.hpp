//! \file    Button.hpp
//! \author  DRY/nogte1
//! \version 1.1
//! \date    12.03.2015

#ifndef GENERIC_BUTTON_H_
#define GENERIC_BUTTON_H_

/*note: these are from same dir. if put <> they may clash with other stuff.*/
#include "utilfuncs.h"
#include "macroutils.h"
#include <algorithm>

namespace btn_space
{
  /* Remark: avoid Vtables as much as possible? */
  
  //! \brief  Button interface
  class IButton
  {
    public:
    
      enum STATE{}; //!< Implementations define different states.
  
      //!< Return the state of the button.
      const STATE& getState() const  
      { return _State; }
    
      void setState(const STATE& s)
      { _State = s; }
      
    protected:
    
      STATE _State;
  };
  
  
  //! \brief A debouncer for (digital) button port,pin. This works by sampling 
  //! the port, pin and 'filtering' the transitions between OFF and ON (low/high).
  template< 
      int PINOFF, //!< specify the initial / start reading value on the port
      int PINON   //!< specify alternative reading (e.g. when btn is pressed)
     >
  class sampling_debounce
  {
    public:
      
      sampling_debounce(uint8_t pt=0xFC, uint8_t rt=0x3F)
        :  BTN_PRESS_THRESHOLD(pt), BTN_RELEASE_THRESHOLD(rt), _shift_reg(0xFF),
        _pin_state(PINOFF), _debounced(false)
      {
      }
      
      sampling_debounce& operator () ( int pval)
      {
        _shift_reg >>=1;
        if (PINON == pval)
          _shift_reg |= BIT_7;
        if (_pin_state == PINOFF)
        {
          if (_shift_reg >= BTN_PRESS_THRESHOLD)
          {
            _pin_state = PINON;
            _debounced = true;
          }
          else
            _debounced = false;
        }
        else if (_pin_state == PINON)
        {
          if (_shift_reg <= BTN_RELEASE_THRESHOLD)
          {
            _pin_state = PINOFF;
            _debounced = true;
          }
          else
            _debounced = false;
        }
        return *this;
      }
      
      bool debounced() const { return _debounced; }

      bool pressed() const { return _pin_state == PINON; }
      
      operator int () const { return _pin_state; }
      
      // Example debounce usage: 
      //      extern volatile int reading;
      //      sampling_debounce<0,1> bouncer;
      //      while (1 != bouncer(reading) )
      //        ;
      // Also:  
      //      if (bouncer.debounced())
      //        //check what it was
      
    protected:
      
      const uint8_t BTN_RELEASE_THRESHOLD;
      const uint8_t BTN_PRESS_THRESHOLD;
      uint8_t _shift_reg;
      int _pin_state;
      bool _debounced;
        
  };
  
  
  //! \brief Implement reading HW port/pin elsewhere.
  template<int pnum> inline int read_hw_port();  
  
  
  //! \brief This is button with two states - ON, OFF. User holds the button to 
  //! trigger state switch. The button needs to be polled as often as required
  //! by the debounce routine you select to be used.
  template< IButton::STATE BTN_OFF,         //!< Button OFF state (initial)
            IButton::STATE BTN_ON,          //!< Button ON state
            unsigned POLL_PERIOD_TIME_MS,   //!< How often you will be polling (millisecs)
            unsigned OFF_TRANS_TIME_MS,     //!< Time(millisecs) after which if button is ON and debounced, btn state switched to OFF
            unsigned ON_TRANS_TIME_MS,      //!< Time(millisecs) after which if button is OFF and debounced, bnt state switched to ON
            int HW_PIN_NUM,                 //!< To which HW pin/port btn is attached (see above read_hw_port)
            class debouncer                 //!< Specify the debouncing routine btn will use
          >
  class OnOffHoldButton
    : public IButton
  {
    public:
      
      OnOffHoldButton(IButton::STATE s_init=BTN_OFF)
        : _poll_count( (s_init==BTN_OFF ? ON_TRANS_TIME_MS : OFF_TRANS_TIME_MS)
                      / POLL_PERIOD_TIME_MS + 1), 
        _first_on(true),
        //NOTE the importance of setting this to correct pin state (=before any
        //debouncing/even first read took place). You also must have set the
        //correct PINOFF,PINON templ args for your bouncer.
        _last_pin_state_switch( _bouncer )
          
      {
        _State=s_init;
      }
      
      //!< Use poll to keep update the state of the button. It will return 0 
      //!< each time the transition period has expired (thus you need to make
      //!< sure that you constantly poll the button, at least from beginning to 
      //!< end of one transition period). Thus you can say only wait upto trans
      //!< time for a btn switch and then say die / never us it. You can also
      //!< poll the button continuously from start to end and check the state
      //!< for button switches (no poll resets needed).
      unsigned poll(const bool reset=false)
      {
        if (reset || 0 == _poll_count)
        {
          //Reset poll count every time OFF_TRANS_TIME_MS has expired
          if (BTN_OFF == _State)
            _poll_count=ON_TRANS_TIME_MS / POLL_PERIOD_TIME_MS + 1;
          else
            _poll_count=OFF_TRANS_TIME_MS / POLL_PERIOD_TIME_MS + 1;
        }
        
        --_poll_count;

        if ( _bouncer(read_hw_port<HW_PIN_NUM>()).debounced())
        {
          if (_first_on)
          { //The first pin state debounced becomes the switching state for btn.
            //(e.g.: only will switch on button press, ignoring release and vice versa).
            _first_on = false;
            _btn_pin_state_switch = _bouncer;
          }
          _last_dbd_pin_state = _bouncer;

          if (!_bouncer.pressed()) // if released, use the shortest time to prepare for next press
          {
            _poll_count=std::min(ON_TRANS_TIME_MS, OFF_TRANS_TIME_MS) / POLL_PERIOD_TIME_MS + 1;
          }
          else
          {
            //Reset poll count to time hold time from moment pin transition was
            //detected
            if (BTN_OFF == _State)
              _poll_count=ON_TRANS_TIME_MS / POLL_PERIOD_TIME_MS + 1;
            else
              _poll_count=OFF_TRANS_TIME_MS / POLL_PERIOD_TIME_MS + 1;
          }
        }
        
        if (0 == _poll_count)
        {
          //Check if debouncer is still as when it was last debounced.
          if (_last_dbd_pin_state == _bouncer)
          {
            //If this was different transition to last
            if (_last_pin_state_switch != _last_dbd_pin_state)
            {
              //If this was the reading on which we change sw btn state
              if (_btn_pin_state_switch == _last_dbd_pin_state)
              {
                if (BTN_OFF == _State)
                  _State = BTN_ON;
                else
                  _State = BTN_OFF;
              }
              _last_pin_state_switch = _bouncer;
            }
          }
          //invalidate last debounced state we recorded
          _last_dbd_pin_state = ~_last_dbd_pin_state;
        }
        
        return _poll_count * POLL_PERIOD_TIME_MS;
      }
      
      //Set the (debounced reading) state of the pin that will 
      //triger the button state change. Note: normally that will
      //be the first debounced state (see poll above)
      void setPinSwitchingState(int pinstate)
      {
        _first_on=true;
        _btn_pin_state_switch=pinstate;
      }
      
      int getDebouncedPinstate()
      {
        return _bouncer;
      }

      int debounced()
      {
        return _bouncer.debounced();
      }
      
    protected:

      unsigned _poll_count;
      debouncer _bouncer;
      int _last_dbd_pin_state;
      int _last_pin_state_switch;
      int _btn_pin_state_switch;
      bool _first_on;
  };
}


#endif /* GENERIC_BUTTON_H_ */
