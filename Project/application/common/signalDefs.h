//
//  SignalDefs.h
//! Signal definitions
//
#ifndef _SIGNALDEFS_H
#define _SIGNALDEFS_H

#include "sysdefs.h"

#ifndef OS_CUSTOMISED_IPC

//! \struct IPC
//! \brief 'Inter-process' messages/signals between threads
struct IPC {

  IPC() : m_sig(S_OSTIMEOUT), m_data1(0), m_data2(0), m_pMsg(0)
  {
  }

  IPC(THREAD_SIGNAL sig, std::uint32_t d1, std::uint32_t d2, void * pMsg)
    : m_sig(sig), m_data1(d1), m_data2(d2), m_pMsg(pMsg)
  {
  }

  THREAD_SIGNAL    m_sig;        //!< The signal identifier (enum)
  std::uint32_t  m_data1;      //!< User data 1
  std::uint32_t  m_data2;      //!< User data 2
  void*     m_pMsg;       //!< Pointer to user data (for structs etc..)

};

#endif

#endif
