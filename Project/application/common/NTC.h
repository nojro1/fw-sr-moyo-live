//! \file    NTC.h
//! \author  DRY
//! \version 1.0
//! \date    19.06.2013
//! \brief   An NTC calculation wrapper if you fancy one

#ifndef CM3F_NTC_H
#define CM3F_NTC_H

#include <cstdint>
#include <cmath>

//!NOTE: This one is calculations intensive, so you won't use it on an MSP430 say for sure. A light
//way is to use pre-coded table (see NTC_Table.h for that implementation).
//TODO: simplify me ?

class NTC
{
  public:

    NTC(std::uint16_t vref,
        std::uint16_t adc_range,
        std::uint16_t t0_C,
        std::uint16_t r0,
        std::uint16_t rref,
        std::uint16_t B,
        std::double_t a
        )
      :VREF_(vref), ADC_RANGE_(adc_range), T0_(t0_C+273), R0_(r0), RREF_(rref), B_(B), A_(a),
        R_INF_(R0_*std::exp(-1*B_/double(T0_)))
    {
    }

    //Input: measured voltage (Vin); Returns: temperature in degrees C
    int
    operator()(unsigned vin) const
    {
      //calc resistence
      double r = RREF_ * VREF_ / vin - RREF_;
      //calc temp K
      double t = B_ / std::log(r/R_INF_);
      t = std::round(t);
      return static_cast<int>( t - 273);
    }

  private:
    const std::uint16_t VREF_;      //Your voltage reference
    const std::uint16_t RREF_;      //The NTC reference resistor [ will 65K cover it? ]
    const std::uint16_t ADC_RANGE_;
    const std::int16_t  T0_;
    const std::uint16_t R0_;
    const std::uint16_t B_;
    const std::double_t A_;
    const std::double_t R_INF_;
};


//For test and/or debug
class NTC_test
{
  public:
    const NTC T;
    const int ERR;
    NTC_test(int maxerr=0)
      : T(3300, 4096,25,10000,10000,3960,-4.5), ERR(maxerr)
    {
    }
    bool pass()
    {
      bool tests_ok[3]={false};
      if (ERR >= 25 - T(1650))
        tests_ok[0]=true;
      if (ERR >= 1 - T(785))
        tests_ok[1]=true;
      if (ERR >= 100 - T(3086))
        tests_ok[2]=true;
      return tests_ok[0] && tests_ok[1] && tests_ok[2];
    }
};

#endif // CM3F_NTC_H
