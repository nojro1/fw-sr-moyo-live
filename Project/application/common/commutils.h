//! \file    commutils.h
//! \author  DRY
//! \version 1.0
//! \date    25.06.2013
//! \brief   Util funcs / helpers for data exchange.

#ifndef CM3F_COMM_UTILS_H_
#define CM3F_COMM_UTILS_H_

#if defined (__cplusplus)
  extern "C" {
#elif (__STDC_VERSION__ < 199901L)
  #error Your compiler must support C99
#endif

#include <stdint.h>

#if defined(__IAR_SYSTEMS_ICC__) && /*note: IAR's asm inline is prob identical to GCC's*/\
  defined(__ICCARM__) &&/*arm check reduntant but rather keep it*/\
    defined(__ARM_PROFILE_M__)
  /*Note: CM3 supports all of this, and probably all M cores do for byte swaps. I see M0 seems does
   *not have RBIT seems. If you want to use this for all but M3's you need to check the manual.*/
  #define OASMI_SUPPORTED 1 /*aka optimized asm inlines here*/
#endif

#if defined(OASMI_SUPPORTED)

inline uint16_t swap_bytes_2(uint16_t u16){
  __asm("rev16 %0, %1" :  "=r"(u16) : "r" (u16)); return u16;
}
inline uint32_t swap_bytes_4(uint32_t u32) {
  __asm("rev %0, %1" : "=r"(u32) : "r"(u32));  return u32;
}
inline uint32_t reverse_bits_32(uint32_t u32) {
  __asm("rbit %0, %1" : "=r"(u32) : "r"(u32)); return u32;
}

#else

inline uint16_t swap_bytes_2(uint16_t u) { return (u >> 8 | u << 8); }

inline uint32_t swap_bytes_4(uint32_t u)
{
  return (
          (u << 24) | (u << 8 & 0x00ff0000) | (u >> 8 & 0x0000ff00) | (u >> 24)
         );
}

/*This is called reverse bits the obvious way. For others see for e.g. -
http://graphics.stanford.edu/~seander/bithacks.html */
inline unsigned reverse_bits(unsigned u)
{
  unsigned r = u; // r will be reversed bits of u; first get LSB of u
  //You can be very pedantic about 8 bits per char. If the very unlikely (for us @LM) case it is not
  //define CHAR_BIT here & use it.
  int s = sizeof u * 8 - 1; // extra shift needed at end
  for (u >>= 1; u; u >>= 1)
  {
    r <<= 1;
    r |= u & 1;
    s--;
  }
  r <<= s; // shift when v's highest bits are zero
  return r;
}

inline uint16_t reverse_bits_16(uint16_t u) { return reverse_bits((unsigned)u); }
inline uint32_t reverse_bits_32(uint32_t u) { return reverse_bits((unsigned)u); }

#endif

/*TODO: the optimized ARM one..*/
inline uint64_t swap_bytes_8(uint64_t u)
{
  return (
      (u << 56) |
      (u & 0x000000000000ff00UL) << 40 |
      (u & 0x0000000000ff0000UL) << 24 |
      (u & 0x00000000ff000000UL) << 8  |
      (u & 0x000000ff00000000UL) >> 8  |
      (u & 0x0000ff0000000000UL) >> 24 |
      (u & 0x00ff000000000000UL) >> 40 |
      (u >> 56)
     );
}

#if defined (__cplusplus)
  }
#endif

#undef OASMI_SUPPORTED

#endif /* CM3F_COMM_UTILS_H_ */
