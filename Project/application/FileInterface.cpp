//! \file    FileInterface.cpp
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    05.11.2013

//	  PROJECT:    Moyo
//
//	  HISTORY (version, date, signature, note):
//	  05Nov2013, JRo, Original.

#include "FileInterface.h"

static Mutex * fileMutex = 0;

// static member init
LNRM_FS*  LNRM_FS::m_pLNRM_FS = 0;

//
// Construction
// Note: Filesystem must be initialised before constructor is called
//
LNRM_FS::LNRM_FS()
{
    if (m_pLNRM_FS == 0) // prevent multiple construct
    {
        m_pLNRM_FS = this;
        
        m_massStorageActive = false;
    
        FS_FAT_SupportLFN();
    
    }
}

//
// Destruction
//
LNRM_FS::~LNRM_FS()
{
}

int LNRM_FS::FS__GetVolumeStatus(const char  * sVolume)
{
    Lock();
    int r = FS_GetVolumeStatus(sVolume); 
    Unlock();
    return r;
}

U32 LNRM_FS::FS__GetVolumeSize(const char * sVolume)
{
    Lock();
    U32 r = FS_GetVolumeSize(sVolume);
    Unlock();
    return r; 
}

U32 LNRM_FS::FS__GetVolumeFreeSpace(const char * sVolume) 
{
    Lock();
    U32 r = FS_GetVolumeFreeSpace(sVolume);
    Unlock();
    return r;
}

FS_FILE * LNRM_FS::FS__FOpen(const char * pFileName, const char * pMode) 
{
    Lock();
    FS_FILE *r = FS_FOpen(pFileName, pMode);
    Unlock();
    return r;
}

void LNRM_FS::FS__SetFileWriteMode(FS_WRITEMODE WriteMode)
{
    Lock();
    FS_SetFileWriteMode(WriteMode);
    Unlock();
}

/*
I32 LNRM_FS::FS__GetFilePos(FS_FILE * pFile)
{
    Lock();
    I32 r = FS_GetFilePos(pFile);
    Unlock();
    return r;
}


int LNRM_FS::FS__SetFilePos(FS_FILE * pFile, I32 Offset, int Origin)
{
    Lock();
    int r = FS_SetFilePos(pFile, Offset, Origin);
    Unlock();
    return r;
}
*/

int LNRM_FS::FS__FClose(FS_FILE *pFile) 
{
    Lock();
    int r = FS_FClose(pFile); 
    Unlock();
    return r;
}

U32 LNRM_FS::FS__Write(FS_FILE *pFile, const void *pData, U32 NumBytes) 
{
    Lock();
    U32 r = FS_Write(pFile, pData, NumBytes); 
    Unlock();
    return r;
}

U32 LNRM_FS::FS__Read(FS_FILE *pFile, void *pData, U32 NumBytes) 
{
    Lock();
    U32 r = FS_Read(pFile, pData, NumBytes);
    Unlock();
    return r;
}

int LNRM_FS::FS__MkDir(const char * pDirName) 
{
    Lock();
    int r = FS_MkDir(pDirName);
    Unlock();
    return r;
}

void LNRM_FS::FS__ClearErr(FS_FILE * pFile) 
{
    Lock();
    FS_ClearErr(pFile);
    Unlock();
}

char LNRM_FS::FS__FindFirstFile(FS_FIND_DATA * pfd, const char * sPath, char * sFilename, int sizeofFilename) 
{
    Lock();
    char r = FS_FindFirstFile(pfd, sPath, sFilename, sizeofFilename);
    Unlock();
    return r;
}

int LNRM_FS::FS__Rename(const char * sOldName, const char * sNewName) 
{
    Lock();
    int r = FS_Rename(sOldName, sNewName);
    Unlock();
    return r;
}

char LNRM_FS::FS__FindNextFile(FS_FIND_DATA * pfd) 
{
    Lock();
    char r = FS_FindNextFile(pfd);
    Unlock();
    return r;
}

void LNRM_FS::FS__FindClose(FS_FIND_DATA * pfd) 
{
    Lock();
    FS_FindClose(pfd);
    Unlock();
}

I16 LNRM_FS::FS__FError(FS_FILE * pFile) 
{
    Lock();
    I16 r = FS_FError(pFile);
    Unlock();
    return r;
}
    
int LNRM_FS::FS__Remove(const char *sFileName) 
{
    Lock();
    int r = FS_Remove(sFileName);
    Unlock();
    return r;
}

int LNRM_FS::FS__RmDir(const char * pDirName) 
{
    Lock();
    int r = FS_RmDir(pDirName);
    Unlock();
    return r;
}

void LNRM_FS::FS__FileTimeToTimeStamp(const FS_FILETIME * pFileTime, U32 * pTimeStamp) 
{
    Lock();
    FS_FileTimeToTimeStamp(pFileTime, pTimeStamp);
    Unlock();
}

int LNRM_FS::FS__SetFileTimeEx(const char * pName, U32 TimeStamp, int Index) 
{
    Lock();
    int r = FS_SetFileTimeEx(pName, TimeStamp, Index);
    Unlock();
    return r;
}

/*
void LNRM_FS::Mass__Storage_Init()
{
    Lock();
    Mass_Storage_Init();
    m_massStorageActive = true; 
    //No unlock. We need to protect the mass storage from beeing interruped from File handler
}

void LNRM_FS::Mass__Storage_Stop()
{
    if(m_massStorageActive)
    {
        Mass_Storage_Stop();
        m_massStorageActive = false;
        Unlock(); //Only unlock. Was locked upon Mass__Storage_Init()
    }
}

bool LNRM_FS::Mass__Storage_Start()
{
    return Mass_Storage_Start();
}

bool LNRM_FS::Mass__StorageIsActive()
{
    return m_massStorageActive;
}
*/

bool LNRM_FS::Lock()
{
    if (0==fileMutex)
    {
        fileMutex = new Mutex(); // create when needed
    }
    if (fileMutex)
    {
        fileMutex->lock();
        return true;
    }
    return false;
    
}
bool LNRM_FS::Unlock()
{
    if (fileMutex)
    {
        fileMutex->release();
        return true;
    }
    return false;
}