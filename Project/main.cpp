/******************************************************************

    NAME: main

    FILE: main.cpp

    AUTHOR:  Jan Arild R�yneberg

    DESCRIPTION: The main entry point for Haydom mainboard STM32 controller

    PROJECT: Haydom

    DEVELOPED BY:

    (C)Laerdal Medical A/S, Stavanger, Norway, 2012

    All rights reserved. Reproduction, use, copy, distribution or
    disclosure to third parties without expressed authority is forbidden.
    If such is authorised, both the copyright note above, and this text,
    must appear in all copies and supporting documentation.

    Express authority is also needed to change this software. Modified
    software is not allowed to be distributed.

    Laerdal Medical A/S
    Research & Development
    P.O. Box 377
    N-4001 Stavanger
    Norway

    HISTORY (version, date, signature, note):
    02dec2013, JRo, Original.
    
    \brief The main entry point for Moyo mainboard STM32 controller
*******************************************************************/
#include <new>

#include "includes.h"
#include "versioninfo.h"
#include "cmsis_os.h"
#include "RTOS.h"
#include "th1super.h"
#include "consolethread.h"
#include "filethread.h"
#include "uithread.h"
#include "adthread.h"
#include "FHRM_hw_api.h"
#include "TimerThread.h"

////////////////////////////////////////////////////////////////////////////////
// All static objects here
//

static void noMoreMemory(void);

USING_NAMESPACE_STD;
#ifndef DEBUG
#ifdef VCOM_ID_VID_LAERDAL
#warning("VCOM_ID_VID_LAERDAL defined")
#endif

#ifdef NO_IMPEDANCE_MEASUREMENT
#warning("NO_IMPEDANCE_MEASUREMENT defined")
#endif

#ifdef NOPOWER_OFF_AT_USB_DISCONN
#warning("NOPOWER_OFF_AT_USB_DISCONN defined")
#endif

#ifdef TEST_ECG_TO_FILE
#warning("TEST_ECG_TO_FILE defined")
#endif

#ifdef TEST_ACC_TO_FILE
#warning("TEST_ACC_TO_FILE defined")
#endif

#ifdef NO_FUEL_GAUGE
#warning("NO_FUEL_GAUGE defined")
#endif

#ifdef NO_LED_BLINK
#warning("NO_LED_BLINK defined")
#endif

#ifdef MEASURE_SOUND_PROC_TIME
#warning("MEASURE_SOUND_PROC_TIME defined")
#endif

#ifdef TEST_SOUND_TO_FILE
#warning("TEST_SOUND_TO_FILE defined")
#endif

#ifdef MEASURE_PROBE_ISR_TIME
#warning("MEASURE_PROBE_ISR_TIME defined")
#endif

#ifdef NO_PROBE
#warning("NO_PROBE defined")
#endif

#ifdef NO_POST
#warning("NO_POST defined")
#endif

#ifdef NO_CRC_CHECK
#warning("NO_CRC_CHECK defined")
#endif

#ifdef DEBUG_EPISODE_HANDLER
#warning("DEBUG_EPISODE_HANDLER defined")
#endif

#ifdef DEBUG_ALARM_HANDLER
#warning("DEBUG_ALARM_HANDLER defined")
#endif

#ifdef USE_GAINED_SOUND_DATA
#warning("USE_GAINED_SOUND_DATA defined")
#endif

#ifdef USE_VOLTAGE_AS_SHUTDOWN_CRITERIA
#warning("USE_VOLTAGE_AS_SHUTDOWN_CRITERIA defined")
#endif

#ifdef REPORT_BATTERY_STATE
#warning("REPORT_BATTERY_STATE defined")
#endif
#endif //DEBUG

int main(void)
{
    set_new_handler(noMoreMemory); // handle out of heap memory conditions

    OS_IncDI();         //Initially disable interrupts
    OS_InitKern();      //initialize OS
    OS_INIT_SYS_LOCKS(); // Make system libraries thread-safe

    HwAbsl* pHw = new HwAbsl;   // Create the hw wrapper object. Constructor does HW init
    //OS_InitHW();                //NOTE! HW init already done. This call will just set some
                                //clock values for internal OS utility functions.

    CONSOLE_PRINT("\r\nFetal Heart Rate Monitor - Ver.%d.%d.%d Build %d\r\n", VER_MAJ, VER_MIN, VER_MAINTENANCE, VER_BUILD);

    //The config data in Emulated EEprom
    //Used in several classes, but created here
    Config* pConfig = new Config();
    
    //Create the event handler here. Used by several classes
    Events * m_pEvents = new Events();
    

    Th1Super* pSuper = new Th1Super;                        // Create thread1 object.
    ConsoleThread* pConTh = new ConsoleThread;              // Create thread2 object.
    FileThread* pFileTh = new FileThread;
    UIThread* pUiTh = new UIThread;
    ADThread* pAdTh = new ADThread;
    
    /* Init scheduler */
    osKernelInitialize();
    
	// Call the init function in the thread base class.
	// At least one thread must be started before OS_Start() is called
	// Enumeration of process ID's must start at 1.
	// Priority range 1..255 where 255=highest priority
    pSuper->start       ("Th1Super",   P_SUPER,   10,  256,  5, pHw);  // uses mailbox size = 5 messages
   	pConTh->start       ("Th2Console", P_CONS,     5, 1024,  0, pHw);
    pFileTh->start      ("FileThread", P_FILE,     5, 1024,  5, pHw);
    pUiTh->start        ("UIThread",   P_UI,      20,  300,  5, pHw);
    pAdTh->start        ("ADThread",   P_ADC,     40,  200,  5, pHw);
    
    
    __enable_irq();
    
    /* Start scheduler */
    osKernelStart();

    return 0;       //Should never arrive here!
}

static void noMoreMemory(void)
{
    OS_EnterRegion();     // Avoid further task switches
    
    //DEBUG_TRACE("ERROR: Out of heap memory\n");
    while (1)
    {
    }
}

// IAR Technical Note 26060
// In EC++/EEC++, save RAM by deleting "at exit function calls" (EWARM 5.4x.x)
// IMPORTANT: can only be used if main() never exits !
#ifdef __cplusplus
extern "C"
{
#endif
int dummy__cxa_atexit(void (*destroyer)(void*), void* object, void* dso_handle)
{
  return 0;  // 0 indicates "ok"
}

#ifndef OS_LIBMODE_XR
// to make RTOS link when using IAR 8.x
#if (__VER__ >= 800)
void* __iar_dlib_perthread_allocate(void) {return 0;}
void __iar_dlib_perthread_deallocate(void) {};
#endif
#endif

#ifdef __cplusplus
}
#endif

#ifdef  USE_FULL_ASSERT
//
// \brief Reports the name of the source file and the source line number
// \brief where the assert_param error has occurred.
// \param - file: pointer to the source file name
// \param - line: assert_param error line source number
// \return : None
//
void assert_failed(uint8_t* file, uint32_t line)
{
    //User can add his own implementation to report the file name and line number,
    //ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    printf("Wrong parameters value: file %s on line %d\r\n", file, line);

    while (1)
    {
    }
}
#endif
