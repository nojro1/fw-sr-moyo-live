#if OS_USE_VARINTTABLE==0
//Prototypes for interrupt service routines
extern "C"
{
}
#endif

#include "hwabsl.h"
#include <board_cfg.h>
#include "timerperiphcfg.h"
#include <isrpriocfg.h>
#include <sysdefs.h>
#include <utilfuncs.h>
#include <driverutils.h>
#include <serialport.h>
#include <VirtualComPort.h>
#include <SoftwareUART.h>
#include <adc.h>
#include <dac.h>
#include <PwmGen2.h>
#include <SPImaster.h>
#include <I2Cbus.h>
#include <QuadEncoder.h>
#include <Watchdog.h>
#include <FlowMeter.h>
#include <PressureFlowMeter.h>
#include <RFIDReader.h>
#include <BFDdriver.h>
#include <PCA9554.h>
#include <BlsAudio.h>
#include <BlsECG.h>
#include <Button.hpp>
#include <GpioLED.h>
#include <TimerThread.h>
#include <LMBluetooth.h>
#include <AEDLinkDriver.h>
#include <DS2482.h>
#include <DS2781.h>

#include <Thread.h>
#include <Guard.h>
#include <cmath>
#include <cassert>

using namespace btn_space;

// Define the reading of hw port for nON_KEY button
template<> inline int btn_space::read_hw_port<CFG_nON_KEY_Pin>()
{
  return GPIO_ReadInputDataBit(CFG_nON_KEY_PORT, CFG_nON_KEY_Pin);
}

enum BTN_PIN_STATE
{
  //The button pin reading when released, pressed
  BTN_PIN_RELEASED = 1,
  BTN_PIN_PRESSED  = 0
};

enum ONOFF_BTN_STATE
{
  //Note: these are button 'virtual' state - i.e state after pin state has been de-bounced.
  BTN_OFF = 0,
  BTN_ON  = 1
};

static OnOffHoldButton<
    IButton::STATE(BTN_OFF),
    IButton::STATE(BTN_ON),
    10,  // poll every 10 ms
    400, // time in ms for ON to OFF Transition
    100, // time in ms for OFF to ON Transition
    CFG_nON_KEY_Pin,
    sampling_debounce<BTN_PIN_RELEASED, BTN_PIN_PRESSED> >
_OnOffBtn;

// Example debounce usage:
//      extern volatile int reading;
//      sampling_debounce<0,1> bouncer;
//      while (1 != bouncer(reading) )
//        ;
// Also:
//      if (bouncer.debounced())
//        //check what it was

static sampling_debounce<BTN_PIN_RELEASED, BTN_PIN_PRESSED> _audioIncBouncer;
static sampling_debounce<BTN_PIN_RELEASED, BTN_PIN_PRESSED> _audioDecBouncer;
static sampling_debounce<BTN_PIN_RELEASED, BTN_PIN_PRESSED> _wirelessModeBouncer;
static sampling_debounce<0,1> _modeKeyBouncer;


// static member init
HwAbsl*  HwAbsl::pHw = 0;

//Prototypes for library support routines
//
extern "C"
{
    int MyLowLevelPutchar(int x);
    int MyLowLevelGetchar(); //lint !e1717
#if OS_USE_VARINTTABLE==0
    void PVD_IRQHandler(void);
#endif
}

const uint8_t HwAbsl::ADC_CH_DIFF_PRESSURE       =  CFG_ADC_CH_DIFF_PRESSURE;
const uint8_t HwAbsl::ADC_CH_VBAT                =  CFG_ADC_CH_VBAT;
const uint8_t HwAbsl::ADC_CH_HW_VERSION          =  CFG_ADC_CH_HW_VERSION;
const uint8_t HwAbsl::ADC_CH_USB_PWR             =  CFG_ADC_CH_USB_PWR;
const uint8_t HwAbsl::ADC_CH_TEMP                =  CFG_ADC_CH_TEMP;
const uint8_t HwAbsl::ADC_CH_VCC                 =  CFG_ADC_CH_VCC;

//
//! Construction
//! All basic hardware setup done here
//
HwAbsl::HwAbsl() : m_runningOnExternalPower(false), m_loadboxConnected(false), m_isFirstVent(true),
                   m_clientCommMethod(bls_2010::CommonDefs::CLIENT_COMM_WIRED), m_displayedClientCommMethod(bls_2010::CommonDefs::CLIENT_COMM_WIRED),
                   m_wlanMode(WiFiConfigDefs::WLAN_STATE_UNKNOWN),
                   m_pSerialConsole(0), m_pSerialConsoleCurrent(0),
                   m_pSerialHostComm(0), m_pSerialHostCommWired(0), m_pSerialHostCommWiFi(0), m_pSerialHostCommBluetooth(0),
                   m_pSerialAEDlink(0),
                   m_pAdc(0), m_pDac(0), m_pPwmECG(0), m_pSPI(0), m_pI2C(0),
                   m_pFlowEncoder(0), m_pDepthEncoder(0), m_pFlowmeter(0), m_pWatchdog(0),
                   m_pRFIDreader(0),
                   m_pInternalEEProm(0), m_pExtIO(0),
                   m_pOnewireBridge(0), m_pFuelGauge(0),
                   m_pPrimaryTypeBattery(0),
                   m_pBFD(0), m_pAudio(0),
                   m_pECG(0),
                   m_pWifiModule(0), m_pBluetoothModule(0),
                   m_pAEDLink(0),
                   m_pOnIndicatorLED(0), m_pErrorIndicatorLED(0), m_pWiFiLED(0), m_pBluetoothLED(0),
                   m_pTimerThread(0),
                   m_serialFlashDevId(0),
                   m_wifiModuleStatus(WIFI_MODULE_NOT_YET_READ),
                   m_bluetoothModuleStatus(BT_MODULE_NOT_YET_CONFIGURED),
                   m_hardwareVersion(HW_VER_UNKNOWN),
                   m_hardwareVersionVoltage(0),
                   m_pBatteryMutex(0)
{
    bootloader_activation = eBL_Inactive;

    m_batteryStatus.probeResult.voltage_mV = 0;
    m_batteryStatus.probeResult.current_uA = 0;
    m_batteryStatus.probeResult.averageCurrent_uA = 0;
    m_batteryStatus.probeResult.temperature_mC = 0;
    m_batteryStatus.probeResult.remainingCapacityPercent = -1;
    m_batteryStatus.probeResult.charging = false;

    m_batteryStatus.status = bls_2010::CommonDefs::eBatteryUnknown;
    m_batteryStatus.statusChanged = false;

    m_pBatteryMutex = new rtos_support::Mutex();

    if (pHw == 0) // prevent multiple construct
    {
        __disable_irq();

#ifdef USE_DFU_BOOTLOADER
        // Set the Vector Table base location at APP_START_OFFSET
        NVIC_SetVectorTable(NVIC_VectTab_FLASH, APP_START_OFFSET);
#endif

        NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4); // IMPORTANT! must be done BEFORE setting ISR priorities

        sysTick_init();

#if OS_USE_VARINTTABLE
        OS_ARM_InstallISRHandler(SysTick_IRQn, (OS_ISR_HANDLER*)OS_Systick);
#endif
    }

}

//
// Destruction
//
HwAbsl::~HwAbsl()
{
    OS_IncDI();

    // Devices using encoder
    delete m_pFlowmeter;

    // Devices using ISerial
    delete m_pAEDLink;
    delete m_pWifiModule;
    delete m_pBluetoothModule;
    delete m_pRFIDreader;

    // Devices using Dac
    delete m_pAudio;

    // Devices using Pwm
    delete m_pECG;

    // Devices using m_pTimerThread
    delete m_pOnIndicatorLED;
    delete m_pErrorIndicatorLED;
    delete m_pWiFiLED;
    delete m_pBluetoothLED;
    delete m_pTimerThread;

    // I2C devices
    delete m_pBFD;
    delete m_pExtIO;
    delete m_pFuelGauge;
    delete m_pPrimaryTypeBattery;
    delete m_pOnewireBridge;

    // Lowest level devices
    delete m_pI2C;
    delete m_pSPI;
    delete m_pAdc;
    delete m_pDac;
    delete m_pPwmECG;
    delete m_pFlowEncoder;
    delete m_pDepthEncoder;
    delete m_pWatchdog;
    delete m_pInternalEEProm;
    delete m_pSerialConsole;
    delete m_pSerialHostCommWired;
    delete m_pSerialHostCommWiFi;
    delete m_pSerialHostCommBluetooth;
    delete m_pSerialAEDlink;

	pHw = 0;

    OS_DecRI();
}

//
//! Init the GPIO pins
//
void HwAbsl::gpio_init()
{
#if CFG_DISABLE_JTAG == 1
        // Disable JTAG pins so that PB3, PB4 and PA15 can be used for GPIO
        // Note: This disables TRACESWO !
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
        GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
#else
        // Enable JTAG and fill SWO except NJTRST (PB4 can be used for GPIO)
        // Note: This enables TRACESWO !
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
        GPIO_PinRemapConfig(GPIO_Remap_SWJ_NoJTRST, ENABLE);
#endif


    // Enable GPIO clock and release reset
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_GPIOA, DISABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_GPIOB, DISABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_GPIOC, DISABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_GPIOD, DISABLE);

    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

    // ---------------
    // Prepare inputs
    // ---------------

#ifdef CFG_nVALID_VENT_Pin
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = CFG_nVALID_VENT_Pin;
    GPIO_Init(CFG_nVALID_VENT_PORT, &GPIO_InitStructure);
#endif

#ifdef CFG_DEFIB_DETECT_Pin
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
    GPIO_InitStructure.GPIO_Pin = CFG_DEFIB_DETECT_Pin;
    GPIO_Init(CFG_DEFIB_DETECT_PORT, &GPIO_InitStructure);
#endif

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = CFG_nON_KEY_Pin;
    GPIO_Init(CFG_nON_KEY_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = CFG_VOLUME_UP_Pin;
    GPIO_Init(CFG_VOLUME_UP_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = CFG_VOLUME_DOWN_Pin;
    GPIO_Init(CFG_VOLUME_DOWN_PORT, &GPIO_InitStructure);

#ifdef CFG_WIRELESS_MODE_Pin
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Pin = CFG_WIRELESS_MODE_Pin;
    GPIO_Init(CFG_WIRELESS_MODE_PORT, &GPIO_InitStructure);
#endif

#ifdef USB_PWR_PIN
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_InitStructure.GPIO_Pin = USB_PWR_PIN;
    GPIO_Init(USB_PWR, &GPIO_InitStructure);
#endif

    GPIO_InitStructure.GPIO_Mode = CFG_XIPHOID_Mode;
    GPIO_InitStructure.GPIO_Pin = CFG_XIPHOID_Pin;
    GPIO_Init(CFG_XIPHOID_PORT, &GPIO_InitStructure);

#ifdef CFG_FINGERPOS_Pin
    GPIO_InitStructure.GPIO_Mode = CFG_FINGERPOS_Mode;
    GPIO_InitStructure.GPIO_Pin = CFG_FINGERPOS_Pin;
    GPIO_Init(CFG_FINGERPOS_PORT, &GPIO_InitStructure);
#endif

    // ---------------
    // Prepare outputs
    // ---------------
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;

    if ( !isExternalPowerPresent() )
    {
        externalPowerOff();
    }
    else
    {
        externalPowerOn();
    }

#ifdef CFG_SHUTDOWN_USB_Pin
    GPIO_InitStructure.GPIO_Pin = CFG_SHUTDOWN_USB_Pin;
    GPIO_Init(CFG_SHUTDOWN_USB_PORT, &GPIO_InitStructure);
#endif

    GPIO_WriteBit(CFG_VBAT_ON_PORT, CFG_VBAT_ON_Pin, Bit_RESET); // VBAT_ON=OFF -> use power from USB or power off
    GPIO_InitStructure.GPIO_Pin = CFG_VBAT_ON_Pin;
    GPIO_Init(CFG_VBAT_ON_PORT, &GPIO_InitStructure);

    // LEDS
    if (0==m_pTimerThread && 0==m_pOnIndicatorLED && 0==m_pBluetoothLED &&
        0==m_pErrorIndicatorLED && 0==m_pWiFiLED)
    {
        m_pTimerThread = new TimerThread();

        GPIO_InitTypeDef GPIO_LEDInitStructure;

        GPIO_LEDInitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_LEDInitStructure.GPIO_Speed = GPIO_Speed_2MHz;

        GPIO_LEDInitStructure.GPIO_Pin = CFG_ON_LED_Pin;
        m_pOnIndicatorLED = new GpioLED(CFG_ON_LED_PORT, GPIO_LEDInitStructure, Bit_SET, m_pTimerThread);

        GPIO_LEDInitStructure.GPIO_Pin = CFG_ERR_LED_Pin;
        m_pErrorIndicatorLED = new GpioLED(CFG_ERR_LED_PORT, GPIO_LEDInitStructure, Bit_SET, m_pTimerThread);

#ifdef CFG_WIFI_LED_Pin
        GPIO_LEDInitStructure.GPIO_Pin = CFG_WIFI_LED_Pin;
        m_pWiFiLED = new GpioLED(CFG_WIFI_LED_PORT, GPIO_LEDInitStructure, Bit_SET, m_pTimerThread);
#endif

#ifdef CFG_BT_LED_Pin
        GPIO_LEDInitStructure.GPIO_Pin = CFG_BT_LED_Pin;
        m_pBluetoothLED = new GpioLED(CFG_BT_LED_PORT, GPIO_LEDInitStructure, Bit_SET, m_pTimerThread);
#endif

    showDeviceOnStatus();

    // Audio_off line
    audioOff(); // Audio initially off
    GPIO_InitStructure.GPIO_Pin = CFG_AUDIO_EN_Pin;
    GPIO_Init(CFG_AUDIO_EN_PORT, &GPIO_InitStructure);

#ifdef CFG_BLUETOOTH_ENABLE_Pin
    // BT
    GPIO_WriteBit(CFG_BLUETOOTH_ENABLE_PORT, CFG_BLUETOOTH_ENABLE_Pin, Bit_RESET);
    GPIO_InitStructure.GPIO_Pin = CFG_BLUETOOTH_ENABLE_Pin;
    GPIO_Init(CFG_BLUETOOTH_ENABLE_PORT, &GPIO_InitStructure);
#endif

#ifdef CFG_WIFI_RESET_Pin
    // WiFi
    GPIO_WriteBit(CFG_WIFI_RESET_PORT, CFG_WIFI_RESET_Pin, Bit_RESET);
    GPIO_InitStructure.GPIO_Pin = CFG_WIFI_RESET_Pin;
    GPIO_Init(CFG_WIFI_RESET_PORT, &GPIO_InitStructure);
#endif

#ifdef CFG_WIFI_GPIO9_Pin
    GPIO_WriteBit(CFG_WIFI_GPIO9_PORT, CFG_WIFI_GPIO9_Pin, Bit_RESET);
    GPIO_InitStructure.GPIO_Pin = CFG_WIFI_GPIO9_Pin;
    GPIO_Init(CFG_WIFI_GPIO9_PORT, &GPIO_InitStructure);
#endif


    }

    // Now initialize all unused port pins to input + pull-down
    // (except for those allocated at reset to SWD/JTAG)
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;

#if (CFG_GPIO_A_UNUSED != 0)
    GPIO_InitStructure.GPIO_Pin = CFG_GPIO_A_UNUSED;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
#endif

#if (CFG_GPIO_B_UNUSED != 0)
    GPIO_InitStructure.GPIO_Pin = CFG_GPIO_B_UNUSED;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
#endif

#if (CFG_GPIO_C_UNUSED != 0)
    GPIO_InitStructure.GPIO_Pin = CFG_GPIO_C_UNUSED;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
#endif

#if (CFG_GPIO_D_UNUSED != 0)
    GPIO_InitStructure.GPIO_Pin = CFG_GPIO_D_UNUSED;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
#endif

}

void HwAbsl::deviceReset()
{
    getpHw()->resetRFIDReader();
    NVIC_SystemReset();   // will never return
}

void HwAbsl::enterDfuBootloader()
{
  #if USE_DFU_BOOTLOADER
    bootloader_activation = eBL_Activate; // Signal to BL to stay in boot mode at next reset

    NVIC_SystemReset();   // will never return
  #endif
}

//
//! Init the various peripherals
//
void HwAbsl::perip_init()
{
    watchdog_init();

    detectHardwareVersionViaGPIO(); // in case of rev B boards

    adc_init();
    delayUs(10000); // let ADC sampling stabilize before using results

    detectHardwareVersionViaADC(); // for Rev C boards and higher

    gpio_init();

    if ( !isRunningOnExternalPower() )
    {

      for( ;; )
      {
        delayUs(10000);
        if (0 == _OnOffBtn.poll()) //wait util 1 transition period expired
        {
          if (_OnOffBtn.getState() == IButton::STATE(BTN_ON))
          {
            batteryPowerOn();
            break; //Button ON -> system running
          }
          else
          {
            batteryPowerOff();
          }
        }
      }
    }

    _OnOffBtn.setState(IButton::STATE( BTN_ON )); // ARM the ON/OFF button


    checkForLoadboxConnected();

    if (isLoadboxConnected())
    {
        pwm_init();
        ecg_init();
    }

    dac_init();
    audio_init();
    //spi_init();
    i2c_init();

    // start watchdog before we start I2C devices
    m_pWatchdog->start();
    m_pWatchdog->kick();

    extIO_init();
    resetWiFi();

    disableBluetooth();

    showDeviceOnStatus();

    indicateWifiReservedStatus(false);

    onewireBridge_init();
    fuelGauge_init();

    primary_battery_init();

    bfd_init();
    encoder_init();
    flowmeter_init();
    internal_eeprom_init();
    usarts_init();
    rfid_reader_init();

    pvd_init();

    //NOTE!  When adding new peripherals, the corresponding driver source must
    //       be added to project folder "StdPeriph_Driver" in addition to
    //       enabling the corresponding defines in "stm32f10x_conf.h"
}

//
//! Init the system tick timer for N ms interrupts according to SYSTICK_TIME
//! Used for PowerPack RTOS
//
void HwAbsl::sysTick_init() const
{
    uint32_t clk = getSysClk(); // get current system clock freq.

    #if (SYSTICK_TIME < 1)         // mS
        uint32_t relVal = (clk / 1000);
    #else
        uint32_t relVal = (clk / 1000) * SYSTICK_TIME;
    #endif

    SysTick_Config(relVal);
}

//
//! Init Internal watchdog
//
void HwAbsl::watchdog_init()
{
    if (0 == m_pWatchdog)
    {
        m_pWatchdog = new Watchdog();
    }
}

void HwAbsl::watchdog_kick()
{
    if (m_pWatchdog != 0)
    {
        m_pWatchdog->kick();
    }
}

// Programmeble Voltage detector (PVD) initialization
void HwAbsl::pvd_init()
{
    // Enable PWR clock
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

    OS_ARM_InstallISRHandler(PVD_IRQn, (OS_ISR_HANDLER*)&pvd_isr);

    EXTI_InitTypeDef EXTI_InitStructure;

    //Configure EXTI Line16(PVD Output) to generate an interrupt on rising edges
    EXTI_ClearITPendingBit(EXTI_Line16);
    EXTI_InitStructure.EXTI_Line = EXTI_Line16;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    NVIC_InitTypeDef NVIC_InitStructure;

    // Enable the PVD Interrupt
    NVIC_InitStructure.NVIC_IRQChannel = PVD_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = ISR_PRIO_PVD_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);


    // Configure the PVD Level to 2.7V
    PWR_PVDLevelConfig(PWR_PVDLevel_2V7);

    // Enable the PVD Output
    PWR_PVDCmd(ENABLE);
}

//
//! Init USARTs
//
void HwAbsl::usarts_init()
{
    if (0 == m_pSerialHostCommWired)
    {
#if CONSOLE_USART != 0
        delayMs(500);
        m_pSerialHostCommWired = new VirtualComPort(HOSTCOMM_RXBUFSZ, HOSTCOMM_TXBUFSZ);
#else
        m_pSerialHostCommWired = new SerialPort(SerialPort::COM5, HOSTCOMM_BAUD, HOSTCOMM_RXBUFSZ, HOSTCOMM_TXBUFSZ);
#endif
        m_pSerialHostComm = m_pSerialHostCommWired;
    }

#if MANIKIN_HAS_WIRELESS
    if (0 == m_pSerialHostCommWiFi)
    {
        m_pSerialHostCommWiFi = new SerialPort(SerialPort::COM3, HOSTCOMM_BAUD, HOSTCOMM_RXBUFSZ, HOSTCOMM_WLAN_TXBUFSZ, USART_Parity_No, USART_HardwareFlowControl_CTS);
        m_pSerialHostCommBluetooth = new SerialPort(SerialPort::COM2, HOSTCOMM_BAUD, HOSTCOMM_RXBUFSZ, HOSTCOMM_TXBUFSZ);

        if (0==m_pWifiModule)
        {
            m_pWifiModule = new WiFly(m_pSerialHostCommWiFi);
        }
        if (0==m_pBluetoothModule)
        {
            m_pBluetoothModule = new LMBluetooth(m_pSerialHostCommBluetooth);
        }

    }
#endif

    if (0 == m_pSerialConsole)
    {
#if CONSOLE_USART != 0
        m_pSerialConsole = new SerialPort(SerialPort::COM5, CONSOLE_BAUD, CONSOLE_RXBUFSZ, CONSOLE_TXBUFSZ);
#else
        m_pSerialConsole = new VirtualComPort(CONSOLE_RXBUFSZ, CONSOLE_TXBUFSZ);
#endif
        m_pSerialConsoleCurrent = m_pSerialConsole;
    }

}

//
//! Init ADC
//
void HwAbsl::adc_init()
{
    if (0==m_pAdc)
    {
        if (getHardwareVersion() == HW_VER_UNKNOWN)
        {
            m_pAdc = new Adc(CFG_ADC_CH_ALL_USED);
        }
        else
        {
            m_pAdc = new Adc(CFG_ADC_CH_ALL_USED_NO_HW_VERSION);
        }
    }
}

//
//! Init DAC
//
void HwAbsl::dac_init()
{
#if defined(STM32F10X_HD) || defined(STM32F10X_HD_VL) || defined(STM32F10X_XL) || defined(STM32F10X_CL) || defined(STM32L1XX_MD)
    if (0==m_pDac)
    {
        Dac::DacInitStruct_t initStruct = {TIM6};
        m_pDac = new Dac(0x01, initStruct);
    }
#endif
}

//
//! Init SPI
//
void HwAbsl::spi_init()
{
#ifdef CFG_SERIAL_FLASH_CS_Pin
  if (0==m_pSPI)
  {
    m_pSPI = new SPImaster(SPImaster::SPIPORT_1, SPImaster::SPIMODE_0_0, SPImaster::SPI_WORDSIZE_8, 24000000);

    m_serialFlashDevId = m_pSPI->registerDevice(CFG_SERIAL_FLASH_CS_PORT, CFG_SERIAL_FLASH_CS_Pin, Bit_RESET, SPImaster::SPI_WORDSIZE_8);

    //Note: if alloc fails system won't run (see noMoreMemory()) so we don't
    //check return from new.
  }
#endif
}

//
//! Init I2C
//
void HwAbsl::i2c_init()
{
    if (0==m_pI2C)
    {
        m_pI2C = new I2Cbus(I2Cbus::I2CPORT_1, 200000, 0x0A);
    }
}


//
//! Init Quadrature encoder
//
void HwAbsl::encoder_init()
{
    if (0==m_pDepthEncoder)
    {
        m_pDepthEncoder = new QuadEncoder(4); // uses TIM4 channels 1 and 2
    }
}

//
//! Init FlowMeter
//
void HwAbsl::flowmeter_init()
{
    if (m_pAdc && 0==m_pFlowmeter)
    {
        m_pFlowmeter = new PressureFlowMeter(m_pAdc, ADC_CH_DIFF_PRESSURE);
    }
}

//
//! Init Internal (emulated) EEPROM
//
void HwAbsl::internal_eeprom_init()
{
    if (0==m_pInternalEEProm)
    {
        m_pInternalEEProm = new EmulatedEEprom();
    }
}

void HwAbsl::extIO_init()
{
    if (m_pI2C && 0==m_pExtIO)
    {
        m_pExtIO = new PCA9554(m_pI2C);
        setExtIoToSafeMode();
    }
}

void HwAbsl::setExtIoToSafeMode()
{
    if (m_pExtIO)
    {
        m_pExtIO->configureAsOutput(EXTIO_CFG_INIT_SAFE);
    }
}

void HwAbsl::setExtIoToWifiMode()
{
    if (m_pExtIO)
    {
        m_pExtIO->configureAsOutput(EXTIO_CFG_INIT_WIFI);
    }
}

void HwAbsl::onewireBridge_init()
{
    if (m_pI2C && 0==m_pOnewireBridge)
    {
        m_pOnewireBridge = new DS2482(m_pI2C);
    }
}

void HwAbsl::fuelGauge_init()
{
    if (m_pOnewireBridge && 0==m_pFuelGauge)
    {
        m_pFuelGauge = new DS2781(m_pOnewireBridge);
    }
}

void HwAbsl::bfd_init()
{
    if (m_pI2C)
    {
        m_pBFD = new BFDdriver(m_pI2C);
    }
}

void HwAbsl::primary_battery_init()
{
    if (0 == m_pPrimaryTypeBattery)
    {
        m_pPrimaryTypeBattery = new bls_2010::Battery(); // battery monitoring
    }
}

const HwAbsl::DetailedBatteryStatus& HwAbsl::probeBattery()
{
    DetailedBatteryStatus s;

    {
        Guard g(*m_pBatteryMutex); // reentrancy protection
        s = m_batteryStatus;

        if (m_pPrimaryTypeBattery)
        {
            if ( !isRunningOnExternalPower() )
            {
                if (m_pPrimaryTypeBattery->statusChanged())
                {
                    s.status = m_pPrimaryTypeBattery->getStatus();
                }
                s.probeResult.voltage_mV = m_pPrimaryTypeBattery->getLastReadVoltage();
            }
        }
    }


    if (m_pFuelGauge)
    {
        if (m_pFuelGauge->probe(s.probeResult))
        {
            Guard g(*m_pBatteryMutex); // reentrancy protection
            if (m_pPrimaryTypeBattery)
            {
                // we are using LiIon battery, so stop monitoring primary battery
                delete m_pPrimaryTypeBattery;
                m_pPrimaryTypeBattery = 0;
            }

            if (s.probeResult.charging)
            {
                s.status = bls_2010::CommonDefs::eBatteryCharging;
            }
            else
            {
                if (s.probeResult.remainingCapacityPercent <= 5)
                {
                    s.status = bls_2010::CommonDefs::eBatteryEmpty;
                }
                else if (s.probeResult.remainingCapacityPercent <= 15)
                {
                    s.status = bls_2010::CommonDefs::eBatteryLow;
                }
                else if (s.probeResult.remainingCapacityPercent <= 50)
                {
                    s.status = bls_2010::CommonDefs::eBatteryHalfFull;
                }
                else
                {
                    s.status = bls_2010::CommonDefs::eBatteryFull;
                }
            }
        }
    }

    Guard g(*m_pBatteryMutex); // reentrancy protection
    s.statusChanged = s.status != m_batteryStatus.status;
    m_batteryStatus = s;

    return m_batteryStatus;
}

const HwAbsl::DetailedBatteryStatus& HwAbsl::getLastBatteryStatus() const
{
    Guard g(*m_pBatteryMutex); // reentrancy protection
    return m_batteryStatus;
}

bool HwAbsl::showDeviceOnStatus()
{
    m_pErrorIndicatorLED->off();
    m_pOnIndicatorLED->on();

    if (m_pExtIO)
    {
        m_pExtIO->setLow(EXTIO_CH_WIFI_BATT_LOW);
    }

    return true;
}

bool HwAbsl::showDeviceBatteryLowStatus()
{
    m_pErrorIndicatorLED->on();
    m_pOnIndicatorLED->on();

    if (m_pExtIO)
    {
        m_pExtIO->setHigh(EXTIO_CH_WIFI_BATT_LOW);
    }

    return true;
}

bool HwAbsl::showDeviceBatteryChargingStatus()
{
    m_pErrorIndicatorLED->blink(2000, 1000, 0);
    m_pOnIndicatorLED->blink(2000, 1000, 0);

    if (m_pExtIO)
    {
        m_pExtIO->setLow(EXTIO_CH_WIFI_BATT_LOW);
    }

    return true;
}

bool HwAbsl::showDeviceErrorStatus()
{
    m_pErrorIndicatorLED->on();
    m_pOnIndicatorLED->off();
    return true;
}

bool HwAbsl::hideDeviceStatus()
{
    m_pErrorIndicatorLED->off();
    m_pOnIndicatorLED->off();
    return true;
}

void HwAbsl::showWifiIndicatorAsSlowBlink()
{
    if (m_pWiFiLED)
    {
        m_pWiFiLED->on();
        m_pWiFiLED->blink(2000, 1000, 0);
    }
}

void HwAbsl::showWifiIndicatorAsFastBlink()
{
    if (m_pWiFiLED)
        m_pWiFiLED->blink(1000, 500, 0);
}

void HwAbsl::showWifiIndicatorAsSteadyOn()
{
    if (m_pWiFiLED)
        m_pWiFiLED->on();
}


void HwAbsl::hideWifiIndicator()
{
    if (m_pWiFiLED)
        m_pWiFiLED->off();
}

void HwAbsl::showBluetoothIndicatorAsWaiting()
{
    if (m_pBluetoothLED)
    {
        m_pBluetoothLED->on();
        m_pBluetoothLED->blink(1000, 500, 0);
    }
}

void HwAbsl::showBluetoothIndicatorAsConnected()
{
    if (m_pBluetoothLED)
        m_pBluetoothLED->on();
}

void HwAbsl::hideBluetoothIndicator()
{
    if (m_pBluetoothLED)
        m_pBluetoothLED->off();
}

bool HwAbsl::indicateWifiReservedStatus(bool state)
{
    if (m_pExtIO)
    {
        if (state)
        {
            return m_pExtIO->setHigh(EXTIO_CH_WIFI_RESERVED);
        }
        else
        {
            return m_pExtIO->setLow(EXTIO_CH_WIFI_RESERVED);
        }
    }
    return false;
}

bool HwAbsl::markWifiSSIDAsReserved(bool state)
{
    bool retval = false;
    if (m_pWifiModule)
    {
        OS_EnterRegion();
        retval = m_pWifiModule->mark_SoftAP_SSID_asReserved(state);
        OS_LeaveRegion();
    }
    return retval;
}

void HwAbsl::refreshWirelessControlLines() const
{
    if (m_pExtIO)
    {
        m_pExtIO->refresh();
    }
}

void HwAbsl::disableManikinStiffnessSensor()
{
}

void HwAbsl::enableManikinStiffnessSensor()
{
}

void HwAbsl::audioOff()
{
    GPIO_WriteBit(CFG_AUDIO_EN_PORT, CFG_AUDIO_EN_Pin, Bit_SET);
}

void HwAbsl::audioOn()
{
    GPIO_WriteBit(CFG_AUDIO_EN_PORT, CFG_AUDIO_EN_Pin, Bit_RESET);
}

bool HwAbsl::isAudioOn()
{
    return GPIO_ReadOutputDataBit(CFG_AUDIO_EN_PORT, CFG_AUDIO_EN_Pin) == Bit_RESET;
}

void HwAbsl::audio_init()
{
    if (0==m_pAudio && m_pDac)
    {
        m_pAudio = new BlsAudio(m_pDac);
    }
}

void HwAbsl::detectHardwareVersionViaGPIO()
{
#ifdef CFG_HW_VER_Pin
    if (getHardwareVersion() == HW_VER_UNKNOWN)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
        RCC_APB2PeriphResetCmd(RCC_APB2Periph_GPIOC, DISABLE);

        GPIO_InitTypeDef GPIO_InitStructure;

        GPIO_InitStructure.GPIO_Pin = CFG_HW_VER_Pin;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;

        GPIO_Init(CFG_HW_VER_PORT, &GPIO_InitStructure);

        if (GPIO_ReadInputDataBit(CFG_HW_VER_PORT, CFG_HW_VER_Pin) == Bit_SET)
        {
            m_hardwareVersion = HW_VER_B;
        }
    }
#endif
}

void HwAbsl::detectHardwareVersionViaADC()
{
    if (getHardwareVersion() == HW_VER_UNKNOWN && m_pAdc)
    {
        uint16_t adcVal = m_pAdc->readAsMillivolts(ADC_CH_HW_VERSION);
        m_hardwareVersionVoltage = adcVal;
        if (adcVal < 1164)
        {
            if (adcVal >= 25)
            {
                m_hardwareVersion = HW_VER_E;
            }
            else
            {
                m_hardwareVersion = HW_VER_D;
            }
        }
        else
        {
            m_hardwareVersion = HW_VER_B;
        }
    }
}

uint16_t HwAbsl::getBatteryVoltage()
{
    if (m_pAdc)
    {
        return (m_pAdc->readAsMillivolts(ADC_CH_VBAT)*(100000uL+30100uL))/30100uL;
    }
    return 0;
}

void HwAbsl::batteryPowerOff()
{
    if ( !isRunningOnExternalPower() || !isExternalPowerPresent() )
    {
        if (m_pBFD)
        {
            m_pBFD->hideAll();
        }
        hideDeviceStatus();
        __disable_irq();
        GPIO_WriteBit(CFG_VBAT_ON_PORT, CFG_VBAT_ON_Pin, Bit_RESET); // VBAT_ON=OFF -> use power from USB or power off
        while(1)
        {
            // noop
        }
    }
    else
    {
        GPIO_WriteBit(CFG_VBAT_ON_PORT, CFG_VBAT_ON_Pin, Bit_RESET); // VBAT_ON=OFF -> use power from USB or power off

        Guard g(*m_pBatteryMutex); // reentrancy protection
        if (m_pPrimaryTypeBattery)
        {
            m_pPrimaryTypeBattery->reset();
        }
    }
    _OnOffBtn.setState(IButton::STATE( BTN_ON ));

}

void HwAbsl::batteryPowerOn()
{
    GPIO_WriteBit(CFG_VBAT_ON_PORT, CFG_VBAT_ON_Pin, Bit_SET);  // VBAT_ON=ON -> use battery
}

bool HwAbsl::isExternalPowerPresent()
{
  #ifdef USB_PWR_PIN
    return GPIO_ReadInputDataBit(USB_PWR, USB_PWR_PIN) == USB_PWR_ACTIVE_STATE;
  #else
    return getUSBPowerVoltage() > 3000; // > 3V -> we are powered by USB
  #endif
}

HwAbsl::ExternalPowerType_t HwAbsl::getExternalPowerSourceType() const
{
    if (m_pSerialHostCommWired)
    {
        return m_pSerialHostCommWired->hasUSBHostConnection() ?  EXT_PWR_HOST : EXT_PWR_WALL;
    }
    return EXT_PWR_UNKNOWN;
}

// for use when we don't want to draw power from a USB host that is running on batteries (SimPad)
void HwAbsl::externalPowerOff()
{
#ifdef CFG_SHUTDOWN_USB_Pin
    GPIO_WriteBit(CFG_SHUTDOWN_USB_PORT, CFG_SHUTDOWN_USB_Pin, Bit_SET);
#endif
    m_runningOnExternalPower = false;
}

void HwAbsl::externalPowerOn()
{
#ifdef CFG_SHUTDOWN_USB_Pin
    GPIO_WriteBit(CFG_SHUTDOWN_USB_PORT, CFG_SHUTDOWN_USB_Pin, Bit_RESET);
#endif
    m_runningOnExternalPower = true;
}

uint16_t HwAbsl::getUSBPowerVoltage()
{
    if (m_pAdc)
    {
        return (m_pAdc->readAsMillivolts(ADC_CH_USB_PWR)*(100000uL+100000uL))/100000uL;
    }
    return 0;
}


bool HwAbsl::isOffKeyPressed()
{
    _OnOffBtn.poll();
    return (IButton::STATE(BTN_OFF) == _OnOffBtn.getState() ? true : false);
}

bool HwAbsl::isModeKeyPressed()
{
    if (m_pBFD)
    {
        bool pinState = false;
        if ( m_pBFD->getButtonState(pinState) )
        {
            return _modeKeyBouncer(pinState ? 1 : 0) == 1;
        }
    }
    return false;
}

bool HwAbsl::isAudioVolumeIncreaseKeyPressed()
{
    bool pinState = GPIO_ReadInputDataBit(CFG_VOLUME_UP_PORT, CFG_VOLUME_UP_Pin) == Bit_SET;
    return _audioIncBouncer(pinState ? BTN_PIN_RELEASED : BTN_PIN_PRESSED) == BTN_PIN_PRESSED;
}

bool HwAbsl::isAudioVolumeDecreaseKeyPressed()
{
    bool pinState = GPIO_ReadInputDataBit(CFG_VOLUME_DOWN_PORT, CFG_VOLUME_DOWN_Pin) == Bit_SET;
    return _audioDecBouncer(pinState ? BTN_PIN_RELEASED : BTN_PIN_PRESSED) == BTN_PIN_PRESSED;
}

bool HwAbsl::isWirelessModeKeyPressed()
{
#ifdef CFG_WIRELESS_MODE_Pin
    bool pinState = GPIO_ReadInputDataBit(CFG_WIRELESS_MODE_PORT, CFG_WIRELESS_MODE_Pin) == Bit_SET;
    return _wirelessModeBouncer(pinState ? BTN_PIN_RELEASED : BTN_PIN_PRESSED) == BTN_PIN_PRESSED;
#else
    return false;
#endif
}

void HwAbsl::indicateSelectedCommunicationMethod(bls_2010::CommonDefs::ClientCommSetting_t commMethod)
{
    switch (commMethod)
    {
        case bls_2010::CommonDefs::CLIENT_COMM_WIRED:
            hideBluetoothIndicator();
            hideWifiIndicator();
            m_displayedClientCommMethod = bls_2010::CommonDefs::CLIENT_COMM_WIRED;
            break;

        case bls_2010::CommonDefs::CLIENT_COMM_WIFI:
            hideBluetoothIndicator();
            showWifiIndicatorAsSlowBlink();
            m_displayedClientCommMethod = bls_2010::CommonDefs::CLIENT_COMM_WIFI;
            break;

        case bls_2010::CommonDefs::CLIENT_COMM_BLUETOOTH:
            hideWifiIndicator();
            showBluetoothIndicatorAsWaiting();
            m_displayedClientCommMethod = bls_2010::CommonDefs::CLIENT_COMM_BLUETOOTH;
            break;

        default:
            break;
    }
}


void HwAbsl::setHostCommMethodWired()
{
    OS_EnterRegion();
    m_pSerialHostCommWired->enable();
    if (m_pSerialHostComm != m_pSerialHostCommWired)
    {
        m_pSerialHostComm->flush();
        m_pSerialHostComm = m_pSerialHostCommWired;
        m_pSerialHostComm->flush();
    }
    OS_LeaveRegion();

    hideWifiIndicator();
    hideBluetoothIndicator();
    disableBluetooth();
    disableWiFi();

    OS_EnterRegion();
    m_pSerialConsoleCurrent = m_pSerialConsole;
    m_clientCommMethod = bls_2010::CommonDefs::CLIENT_COMM_WIRED;
    OS_LeaveRegion();
}

void HwAbsl::setHostCommMethodWiFi()
{
    OS_EnterRegion();
    if (isAudioVolumeDecreaseKeyPressed()) // allow console on USB if volume down key presses
    {
        m_pSerialConsoleCurrent = m_pSerialHostCommWired;
    }
    bls_2010::CommonDefs::ClientCommSetting_t prevCommMethod = m_clientCommMethod;
    ISerial * tmpPort = m_pSerialHostComm;
    m_pSerialHostComm = 0; // disable normal communication until reconfigured
    OS_LeaveRegion();

    disableBluetooth();

    if ( enableWiFi() )
    {
        OS_EnterRegion();
        if (tmpPort != 0)
        {
            tmpPort->flush();
        }
        m_pSerialHostComm = m_pSerialHostCommWiFi;
        m_pSerialHostComm->flush();
        m_clientCommMethod = bls_2010::CommonDefs::CLIENT_COMM_WIFI;
        OS_LeaveRegion();
    }
    else
    {
        // revert back to what is was
        OS_EnterRegion();
        m_pSerialHostComm = tmpPort;
        if (m_pSerialHostComm != 0)
        {
            m_pSerialHostComm->flush();
        }
        OS_LeaveRegion();

        indicateSelectedCommunicationMethod(prevCommMethod);
        setClientCommMethod(prevCommMethod);
    }
}

void HwAbsl::setHostCommMethodBluetooth()
{
    OS_EnterRegion();
    bls_2010::CommonDefs::ClientCommSetting_t prevCommMethod = m_clientCommMethod;
    ISerial * tmpPort = m_pSerialHostComm;
    m_pSerialHostComm = 0; // disable normal communication until reconfigured
    OS_LeaveRegion();

    if (enableBluetooth())
    {
        OS_EnterRegion();
        if (tmpPort != 0)
        {
            tmpPort->flush();
        }
        m_pSerialHostComm = m_pSerialHostCommBluetooth;
        m_pSerialHostComm->flush();
        m_clientCommMethod = bls_2010::CommonDefs::CLIENT_COMM_BLUETOOTH;
        OS_LeaveRegion();

        disableWiFi();
    }
    else
    {
        // revert back to what is was
        OS_EnterRegion();
        m_pSerialHostComm = tmpPort;
        if (m_pSerialHostComm != 0)
        {
            m_pSerialHostComm->flush();
        }
        OS_LeaveRegion();

        indicateSelectedCommunicationMethod(prevCommMethod);
        setClientCommMethod(prevCommMethod);
    }
}



void HwAbsl::setClientCommMethod(bls_2010::CommonDefs::ClientCommSetting_t commMethod)
{
  #if MANIKIN_HAS_WIRELESS

    if (m_pSerialHostCommWired && m_pSerialHostCommWiFi && m_pSerialHostCommBluetooth)
    {
        switch (commMethod)
        {
            case bls_2010::CommonDefs::CLIENT_COMM_WIRED:
                setHostCommMethodWired();
                break;

            case bls_2010::CommonDefs::CLIENT_COMM_WIFI:
                setHostCommMethodWiFi();
                break;

            case bls_2010::CommonDefs::CLIENT_COMM_BLUETOOTH:
                setHostCommMethodBluetooth();
                break;

            default:
                break;
        }
    }
  #endif

}

WiFiConfigDefs::WlanAttachMode_t HwAbsl::clientConfigureWiFi(const WiFiConfigDefs::WIFI_Conf & conf)
{
    if (m_pWifiModule)
    {
        OS_EnterRegion();
        ISerial* curPort = m_pSerialHostComm;
        if (curPort == m_pSerialHostCommWiFi)
        {
            m_pSerialHostComm = 0; // disable normal communication if we are on WiFi
        }
        OS_LeaveRegion();
        if (curPort && curPort==m_pSerialHostCommWiFi)
        {
            rtos_support::Thread::sleep(300);
            curPort->flush();
        }

        setExtIoToSafeMode();

        WiFiConfigDefs::WlanAttachMode_t mode = m_pWifiModule->ConfigureWLAN(conf);
        if (WiFiConfigDefs::WLAN_STATE_UNKNOWN == mode)
        {
            CONSOLE_PRINT("Failed to configure WiFi from client\r\n");
        }
        else
        {
            setExtIoToWifiMode();
        }
        if (curPort && curPort==m_pSerialHostCommWiFi)
        {
            curPort->flush();
        }

        if (getClientCommMethod() != bls_2010::CommonDefs::CLIENT_COMM_WIFI)
        {
            disableWiFi();
        }

        OS_EnterRegion();
        m_pSerialHostComm = curPort; // re-enable normal communication
        m_wlanMode = mode;
        OS_LeaveRegion();
        return m_wlanMode;
    }
    return WiFiConfigDefs::WLAN_STATE_UNKNOWN;
}

HwAbsl::WiFiModuleStatus_t HwAbsl::detectWiFiModuleStatus()
{
    if (WIFI_MODULE_NOT_YET_READ == m_wifiModuleStatus || WIFI_MODULE_FAILED_TO_DETECT == m_wifiModuleStatus)
    {
        WiFiModuleStatus_t retVal = WIFI_MODULE_FAILED_TO_DETECT;
        if (m_pWifiModule && m_pWatchdog)
        {
            OS_EnterRegion();
            ISerial* curPort = m_pSerialHostComm;
            if (curPort == m_pSerialHostCommWiFi)
            {
                m_pSerialHostComm = 0; // disable normal communication if we are on WiFi
            }
            OS_LeaveRegion();

            int tries = 2;
            while (tries-- > 0)
            {
                m_pWatchdog->kick();

                if (curPort && curPort==m_pSerialHostCommWiFi)
                {
                    rtos_support::Thread::sleep(300);
                    curPort->flush();
                }

                m_pWatchdog->kick();

                if (m_pSerialHostCommWiFi->getBaud() != HOSTCOMM_BAUD)
                {
                    m_pSerialHostCommWiFi->setBaud(HOSTCOMM_BAUD);
                    m_pSerialHostCommWiFi->flush();
                }

                if (m_pWifiModule->enterCommandMode())
                {
                    m_pWifiModule->exitCommandMode();
                    retVal = WIFI_MODULE_CONFIGURED;
                }
                else
                {
                    m_pWatchdog->kick();
                    m_pSerialHostCommWiFi->setBaud(WiFly::DEFAULT_BAUD);
                    m_pSerialHostCommWiFi->flush();
                    if ( m_pWifiModule->enterCommandMode() )
                    {
                        m_pWifiModule->exitCommandMode();
                        retVal = WIFI_MODULE_NOT_CONFIGURED;
                    }
                }

                if (WIFI_MODULE_FAILED_TO_DETECT == retVal && tries > 0)
                {
                    hwResetWifi();
                }

            }


            if (curPort && curPort==m_pSerialHostCommWiFi)
            {
                curPort->flush();
            }
            OS_EnterRegion();
            m_pSerialHostComm = curPort; // re-enable normal communication
            OS_LeaveRegion();

            m_pWatchdog->kick();

        }

        m_wifiModuleStatus = retVal;

    }

    return m_wifiModuleStatus;

}

bool HwAbsl::factoryResetWifi()
{
    bool retVal = false;

    if (m_pWifiModule)
    {
        OS_EnterRegion();
        ISerial* curPort = m_pSerialHostComm;
        if (curPort == m_pSerialHostCommWiFi)
        {
            m_pSerialHostComm = 0; // disable normal communication if we are on WiFi
        }
        OS_LeaveRegion();
        if (curPort && curPort==m_pSerialHostCommWiFi)
        {
            rtos_support::Thread::sleep(300);
            curPort->flush();
        }

        setExtIoToSafeMode();

        int tries = 2;
        while (tries-- > 0 && !retVal)
        {
            DEBUG_TRACE("WiFi module factory reset start\r\n");
            m_wlanMode = WiFiConfigDefs::WLAN_STATE_UNKNOWN;

            if (!m_pWifiModule->enterCommandMode())
            {
                if (m_pSerialHostCommWiFi->getBaud() == HOSTCOMM_BAUD)
                {
                    m_pSerialHostCommWiFi->setBaud(WiFly::DEFAULT_BAUD);
                }
                else
                {
                    m_pSerialHostCommWiFi->setBaud(HOSTCOMM_BAUD);
                }
                m_pSerialHostCommWiFi->flush();
            }
            else
            {
                m_pWifiModule->exitCommandMode();
            }

            retVal = m_pWifiModule->factoryResetWIFI(m_deviceId);
            if (retVal)
            {
                CONSOLE_PRINT("WiFi module factory reset OK\r\n");
                if (m_pSerialHostCommWiFi->getBaud() != HOSTCOMM_BAUD)
                {
                    m_pSerialHostCommWiFi->setBaud(HOSTCOMM_BAUD);
                    m_pSerialHostCommWiFi->flush();
                }

                setExtIoToWifiMode();

                if (getClientCommMethod() == bls_2010::CommonDefs::CLIENT_COMM_WIFI)
                {
                    retVal = enableWiFi();
                }
            }
            else
            {
                CONSOLE_PRINT("WiFi module factory reset failed\r\n");
            }
        }

        if (getClientCommMethod() != bls_2010::CommonDefs::CLIENT_COMM_WIFI)
        {
            disableWiFi();
        }

        if (curPort && curPort==m_pSerialHostCommWiFi)
        {
            curPort->flush();
        }
        OS_EnterRegion();
        m_pSerialHostComm = curPort; // re-enable normal communication
        OS_LeaveRegion();
    }

    return retVal;
}


void HwAbsl::hwResetWifi()
{
    if (getHardwareVersion() == HW_VER_E)
    {
#if defined(CFG_WIFI_GPIO9_Pin) && defined(CFG_WIFI_RESET_Pin)

        DEBUG_TRACE("WiFi module HW GPIO9 reset\r\n");

        m_pWatchdog->kick();

        GPIO_WriteBit(CFG_WIFI_GPIO9_PORT, CFG_WIFI_GPIO9_Pin, Bit_SET);

        if (getHardwareVersion() == HW_VER_B)
            m_pExtIO->setLow(EXTIO_CH_WIFI_RESET);
        else
            GPIO_WriteBit(CFG_WIFI_RESET_PORT, CFG_WIFI_RESET_Pin, Bit_RESET);

        rtos_support::Thread::sleep(WiFly::REBOOT_TIME);

        if (getHardwareVersion() == HW_VER_B)
            m_pExtIO->setHigh(EXTIO_CH_WIFI_RESET);
        else
            GPIO_WriteBit(CFG_WIFI_RESET_PORT, CFG_WIFI_RESET_Pin, Bit_SET);

        rtos_support::Thread::sleep(2*WiFly::REBOOT_TIME);

        m_pWatchdog->kick();

        for (int i=0; i<4; ++i)
        {
            GPIO_WriteBit(CFG_WIFI_GPIO9_PORT, CFG_WIFI_GPIO9_Pin, Bit_RESET);
            rtos_support::Thread::sleep(WiFly::GPIO9_TOGGLE_TIME);
            GPIO_WriteBit(CFG_WIFI_GPIO9_PORT, CFG_WIFI_GPIO9_Pin, Bit_SET);
            rtos_support::Thread::sleep(WiFly::GPIO9_TOGGLE_TIME);
            m_pWatchdog->kick();
        }

        GPIO_WriteBit(CFG_WIFI_GPIO9_PORT, CFG_WIFI_GPIO9_Pin, Bit_RESET);
        rtos_support::Thread::sleep(WiFly::GPIO9_TOGGLE_TIME);
        m_pWatchdog->kick();

        if (getHardwareVersion() == HW_VER_B)
            m_pExtIO->setLow(EXTIO_CH_WIFI_RESET);
        else
            GPIO_WriteBit(CFG_WIFI_RESET_PORT, CFG_WIFI_RESET_Pin, Bit_RESET);

        rtos_support::Thread::sleep(WiFly::REBOOT_TIME);

        if (getHardwareVersion() == HW_VER_B)
            m_pExtIO->setHigh(EXTIO_CH_WIFI_RESET);
        else
            GPIO_WriteBit(CFG_WIFI_RESET_PORT, CFG_WIFI_RESET_Pin, Bit_SET);

        rtos_support::Thread::sleep(2*WiFly::REBOOT_TIME);

        m_pWatchdog->kick();
#endif
    }
}

bool HwAbsl::setWifiDeviceId(const std::string & id)
{
    bool retVal = false;

    if (m_pWifiModule)
    {
        OS_EnterRegion();
        ISerial* curPort = m_pSerialHostComm;
        if (curPort == m_pSerialHostCommWiFi)
        {
            m_pSerialHostComm = 0; // disable normal communication if we are on WiFi
        }
        OS_LeaveRegion();
        rtos_support::Thread::sleep(300);
        if (curPort && curPort==m_pSerialHostCommWiFi)
        {
            curPort->flush();
        }

        retVal = m_pWifiModule->configureDeviceId(id);

        if (retVal)
        {
            m_deviceId = id;
            if (getClientCommMethod() == bls_2010::CommonDefs::CLIENT_COMM_WIFI &&
                getWlanMode() == WiFiConfigDefs::WLAN_STATE_APMODE)
            {
                retVal = enableWiFi(); // restart softAP
            }
        }

        if (getClientCommMethod() != bls_2010::CommonDefs::CLIENT_COMM_WIFI)
        {
            rtos_support::Thread::sleep(300);
            disableWiFi();
        }

        if (curPort && curPort==m_pSerialHostCommWiFi)
        {
            curPort->flush();
        }
        OS_EnterRegion();
        m_pSerialHostComm = curPort; // re-enable normal communication
        OS_LeaveRegion();
    }

    return retVal;
}

void HwAbsl::initDeviceId(const std::string & id)
{
    m_deviceId = id;
}

void HwAbsl::setEnableBluetoothControlLineActive(bool state)
{
    if (m_pExtIO)
    {
        if (state)
        {
            if (getHardwareVersion() == HW_VER_B)
            {
                m_pExtIO->setHigh(EXTIO_CH_BLUETOOTH_EN);
            }
            else
            {
              #ifdef CFG_BLUETOOTH_ENABLE_Pin
                GPIO_WriteBit(CFG_BLUETOOTH_ENABLE_PORT, CFG_BLUETOOTH_ENABLE_Pin, Bit_SET);
              #endif
            }
        }
        else
        {
            if (getHardwareVersion() == HW_VER_B)
            {
                m_pExtIO->setLow(EXTIO_CH_BLUETOOTH_EN);
            }
            else
            {
              #ifdef CFG_BLUETOOTH_ENABLE_Pin
                GPIO_WriteBit(CFG_BLUETOOTH_ENABLE_PORT, CFG_BLUETOOTH_ENABLE_Pin, Bit_RESET);
              #endif
            }
        }
    }

}

void HwAbsl::disableBluetooth()
{
    setEnableBluetoothControlLineActive(false);
}

bool HwAbsl::enableBluetooth()
{
    bool success = false;
    m_bluetoothModuleStatus = BT_MODULE_NOT_YET_CONFIGURED;

    if (m_pBluetoothModule)
    {
        setEnableBluetoothControlLineActive(true);

        rtos_support::Thread::sleep(LMBluetooth::BOOT_TIME);
        success = m_pBluetoothModule->configure(m_deviceId);
        if (!success)
        {
            CONSOLE_PRINT("Configuring BT module failed. Try again\r\n");

            setEnableBluetoothControlLineActive(false);
            rtos_support::Thread::sleep(LMBluetooth::RESET_TIME);
            setEnableBluetoothControlLineActive(true);
            rtos_support::Thread::sleep(2*LMBluetooth::BOOT_TIME);

            success = m_pBluetoothModule->configure(m_deviceId);

            if (!success)
            {
                CONSOLE_PRINT("Configuring BT module failed after retry\r\n");
            }
        }
    }
    m_bluetoothModuleStatus = success ? BT_MODULE_CONFIGURED : BT_MODULE_FAILED_TO_CONFIGURE;
    return success;
}

HwAbsl::BluetoothConnectionStatus_t HwAbsl::getBluetoothClientConnectionStatus()
{
    HwAbsl::BluetoothConnectionStatus_t status = BT_CONN_STATUS_FAILED_TO_READ;
    if (m_pExtIO)
    {
        bool clientConnected = false;
        if ( m_pExtIO->read(EXTIO_CH_BLUETOOTH_STATUS, clientConnected) )
        {
            status = clientConnected ? BT_CONN_STATUS_CONNECTED : BT_CONN_STATUS_NOT_CONNECTED;
        }
    }
    return status;
}

void HwAbsl::disableWiFi()
{
    if (m_pWifiModule)
    {
        // Put WiFi module to sleep
        if (m_pWifiModule->enterCommandMode())
        {
            if (m_pWifiModule->setToSleep())
            {
                DEBUG_TRACE("WiFi module put to sleep\r\n");
            }
        }
    }
}

bool HwAbsl::enableWiFi()
{
    if (m_pWifiModule)
    {
        setExtIoToSafeMode();

        m_wlanMode = m_pWifiModule->AutoAttachWLAN();

        if (m_wlanMode != WiFiConfigDefs::WLAN_STATE_UNKNOWN)
        {
            setExtIoToWifiMode();

          #ifndef _NDEBUG
            std::string s;
            m_pWifiModule->enterCommandMode();
            if (m_pWifiModule->getDeviceId( s ))
            {
                DEBUG_TRACE("WiFi module ID: %s\r\n", s.c_str());
            }
            DEBUG_TRACE("WiFi module SSID: %s\r\n", m_pWifiModule->getActiveSSID().c_str());
            m_pWifiModule->exitCommandMode();
          #endif
            return true;
        }
        else
        {
            CONSOLE_PRINT("Failed to configure WiFi module\r\n");
            resetWiFi();
        }
    }
    return false;
}

void HwAbsl::resetWiFi()
{
    if (getHardwareVersion() == HW_VER_B)
        m_pExtIO->setLow(EXTIO_CH_WIFI_RESET);
  #ifdef CFG_WIFI_RESET_Pin
    else
        GPIO_WriteBit(CFG_WIFI_RESET_PORT, CFG_WIFI_RESET_Pin, Bit_RESET);
  #endif

    delayUs(1000);

    if (getHardwareVersion() == HW_VER_B)
        m_pExtIO->setHigh(EXTIO_CH_WIFI_RESET);
  #ifdef CFG_WIFI_RESET_Pin
    else
        GPIO_WriteBit(CFG_WIFI_RESET_PORT, CFG_WIFI_RESET_Pin, Bit_SET);
  #endif

    delayUs(1000);
}


HwAbsl::WiFiAssociationStatus_t HwAbsl::getWiFiAssociationStatus()
{
    HwAbsl::WiFiAssociationStatus_t status = WIFI_ASSOC_STATUS_FAILED_TO_READ;
    if (m_pExtIO)
    {
        bool associated = false;
        if ( m_pExtIO->read(EXTIO_CH_WIFI_ASSOC_STATUS, associated) )
        {
            status = associated ? WIFI_ASSOC_STATUS_ASSOCIATED : WIFI_ASSOC_STATUS_NOT_ASSOCIATED;
        }
    }
    return status;
}

HwAbsl::WiFiConnectionStatus_t HwAbsl::getWiFiClientConnectionStatus()
{
    HwAbsl::WiFiConnectionStatus_t status = WIFI_CONN_STATUS_FAILED_TO_READ;
    if (m_pExtIO)
    {
        bool clientConnected = false;
        if ( m_pExtIO->read(EXTIO_CH_WIFI_CONN_STATUS, clientConnected) )
        {
            status = clientConnected ? WIFI_CONN_STATUS_CONNECTED : WIFI_CONN_STATUS_NOT_CONNECTED;
        }
    }
    return status;
}


bool HwAbsl::isHandposXiphoidActive()
{
    return GPIO_ReadInputDataBit(CFG_XIPHOID_PORT, CFG_XIPHOID_Pin) == Bit_RESET;
}

bool HwAbsl::getRawDepth(int16_t & rawDepth)
{
    if (m_pDepthEncoder)
    {
        rawDepth = static_cast<int16_t>(m_pDepthEncoder->read());
        return true;
    }
    return false;
}

int16_t HwAbsl::convertRawDepthToZeroPointZeroOneMillimeterUnits(const int16_t rawDepth)
{
    if (rawDepth >= 0)
    {
        if (rawDepth < LEN_DEPTH_CONV_TABLE)
        {
            return DEPTH_CONVERSION_TABLE[rawDepth];
        }
        return DEPTH_CONVERSION_TABLE[LEN_DEPTH_CONV_TABLE-1];
    }
    return 0;
}

bool HwAbsl::resetRawDepth()
{
    if (m_pDepthEncoder)
    {
        m_pDepthEncoder->reset();
        return true;
    }
    return false;
}

bool HwAbsl::swapConsolePort()
{
   #if 0
    if (m_pSerialHostCommWired != 0 && m_pSerialConsole != 0)
    {
        OS_EnterRegion();
        ISerial* tmp = m_pSerialHostCommWired;
        m_pSerialHostCommWired = m_pSerialConsole;
        m_pSerialConsole = tmp;
        m_pSerialHostCommWired->flush();
        m_pSerialConsole->flush();
        OS_LeaveRegion();
        return true;
    }
   #endif
    return false;
}

void HwAbsl::pvd_isr()
{
    if (EXTI_GetITStatus(EXTI_Line16) != RESET)
    {
        HwAbsl* pHw = HwAbsl::getpHw();
        if (pHw)
        {
            pHw->batteryPowerOn();
            pHw->externalPowerOff();
        }
        // Clear the EXTI line pending bit
        EXTI_ClearITPendingBit(EXTI_Line16);
    }
}

////////////////////////////////////////////////////////////////////////////////
// Global Routines Section /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//
// Entry point for library __write for
// STDOUT and STDERR
// Implemented for using printf()
//
int MyLowLevelPutchar(int x)
{
    HwAbsl* pHw = HwAbsl::getpHw();
    assert(pHw!=0);
    ISerial * pPort = pHw->getpSerialConsole();
    if (pPort)
    {
      if (pPort != pHw->getpSerialHostCommWired())
      {
        while ( !pPort->hasTxBufferSpaceAvailable() )
        {
            __WFI();
        }
      }
      pPort->write(x);
      return 1;
    }
    return -1;
}

//
// Entry point for library __read for
// STDIN
//
int MyLowLevelGetchar()
{
    ISerial * pPort = HwAbsl::getpHw()->getpSerialConsole();
    if (pPort)
    {
        return pPort->read();
    }
    return -1;
}

#if OS_USE_VARINTTABLE==0

////////////////////////////////////////////////////////////////////////////////
// Interrupt Service Routines Section //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// NOTE! PowerPack RTOS handlers OS_Systick and OS_Exception are inserted
//       directly into the vector table! (See stm32f10_vector.c).

/**
  * @brief  This function handles the PVD Output interrupt request.
  * @param  None
  * @retval None
  */
void PVD_IRQHandler(void)
{
    HwAbsl* pHw = HwAbsl::getpHw();
    if (pHw)
    {
        pHw->pvd_isr();
    }
}



#endif


