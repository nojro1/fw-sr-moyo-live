//! \file    board_cfg.h
//! \author  Jan Arild R�yneberg
//! \version 1.0
//! \date    06.05.2020

#ifndef _BOARD_CFG_H
#define _BOARD_CFG_H

//Definitions for pins and ports used
#define AN_ambLight_Pin GPIO_PIN_0
#define AN_ambLight_GPIO_Port GPIOC
#define AN_impedance_Pin GPIO_PIN_1
#define AN_impedance_GPIO_Port GPIOC
#define AN_ECG_Pin GPIO_PIN_2
#define AN_ECG_GPIO_Port GPIOC
#define AN_5V_Pin GPIO_PIN_3
#define AN_5V_GPIO_Port GPIOC
#define Audio_P_Pin GPIO_PIN_4
#define Audio_P_GPIO_Port GPIOA
#define BAT_CE_Pin GPIO_PIN_4
#define BAT_CE_GPIO_Port GPIOE
#define BAT_GD_Pin GPIO_PIN_7
#define BAT_GD_GPIO_Port GPIOF
#define BAT_nPG_Pin GPIO_PIN_9
#define BAT_nPG_GPIO_Port GPIOG
#define BAT_nTE_Pin GPIO_PIN_3
#define BAT_nTE_GPIO_Port GPIOE
#define BAT_Prog2_Pin GPIO_PIN_2
#define BAT_Prog2_GPIO_Port GPIOE
#define BAT_Sel_Pin GPIO_PIN_1
#define BAT_Sel_GPIO_Port GPIOE
#define BAT_Stat0_Pin GPIO_PIN_14
#define BAT_Stat0_GPIO_Port GPIOG
#define BAT_Stat1_Pin GPIO_PIN_15
#define BAT_Stat1_GPIO_Port GPIOG
#define EN_5V_Pin GPIO_PIN_3
#define EN_5V_GPIO_Port GPIOH
#define EN_audio_Pin GPIO_PIN_10
#define EN_audio_GPIO_Port GPIOF
#define EN_LCD_Pin GPIO_PIN_6
#define EN_LCD_GPIO_Port GPIOD
#define EN_Transducer_Pin GPIO_PIN_10
#define EN_Transducer_GPIO_Port GPIOG
#define EN_Phototran_Pin GPIO_PIN_3
#define EN_Phototran_GPIO_Port GPIOD
#define IR_IN_Pin GPIO_PIN_6
#define IR_IN_GPIO_Port GPIOF
#define LCD_nRST_Pin GPIO_PIN_8
#define LCD_nRST_GPIO_Port GPIOG
#define nAudio_SHDN_Pin GPIO_PIN_0
#define nAudio_SHDN_GPIO_Port GPIOB
#define nKey_pressed_Pin GPIO_PIN_1
#define nKey_pressed_GPIO_Port GPIOB
#define nSYSTEM_SHDN_Pin GPIO_PIN_0
#define nSYSTEM_SHDN_GPIO_Port GPIOE
#define nAudio_SHDN_Pin GPIO_PIN_0
#define nAudio_SHDN_GPIO_Port GPIOB
#define SD_detect_Pin GPIO_PIN_9
#define SD_detect_GPIO_Port GPIOF
#define SD_Power_Pin GPIO_PIN_12
#define SD_Power_GPIO_Port GPIOG
#define Test_EN_Pin GPIO_PIN_13
#define Test_EN_GPIO_Port GPIOG
#define toggle_SW_Pin GPIO_PIN_11
#define toggle_SW_GPIO_Port GPIOF
#define USB_Det_Pin GPIO_PIN_8
#define USB_Det_GPIO_Port GPIOF
#define USART3_Tx_pin GPIO_PIN_10
#define USART3_Rx_pin GPIO_PIN_11
#define USART3_GPIO_Port GPIOB
#define volume_SW_Pin GPIO_PIN_2
#define volume_SW_GPIO_Port GPIOB

//! \brief Configuration file for Moyo device drivers\n
//! Please modify to fit your board

//! \brief Configuration for the SerialPort class\n
//! Please modify CFG_COMx_REMAP to fit your board
#define CFG_COM1_REMAP 0 //!< 0 (PA9, PA10), 1 (PB6, PB7) or 2 (PG9, PG10)
#define CFG_COM2_REMAP 0 //!< 0 (PA2, PA3) or 1 (PD5, PD6)
#define CFG_COM3_REMAP 0 //!< 0 (PB10, PB11), 1 (PC4, PC5), 2 (PC10, PC11) or 3 (PD8, PD9)

/* With a timer 16 bits, maximum frequency will be 32000 times this value.  */
#define TIMER_FREQUENCY_RANGE_MIN      ((uint32_t)    1)
/* Timer prescaler maximum value (0xFFFF for a timer 16 bits)               */
#define TIMER_PRESCALER_MAX_VALUE      ((uint32_t)0xFFFF-1)

//! \brief Configuration for classes that uses TIM1/2/3/4 for peripheral I/O\n
//! Ref. class PwmGen and/or CapCom
//! Please modify CFG_TIMx_REMAP to fit your board
#define CFG_TIM1_REMAP   0 //!< 0, 1 or 3
#define CFG_TIM2_REMAP   3 //!< 0, 1, 2 or 3
#define CFG_TIM3_REMAP   3 //!< 0, 2 or 3
#define CFG_TIM4_REMAP   0 //!< 0 or 1

//! Decide if PwmGen GPIO mode is open drain or push-pull (alternate function, AF)

#define CFG_TIM1_CH_1_4_GPIO_Mode_Out    GPIO_MODE_AF_PP, GPIO_MODE_AF_PP, GPIO_MODE_AF_PP, GPIO_MODE_AF_PP
#define CFG_TIM1_CH_1N_3N_GPIO_Mode_Out  GPIO_MODE_AF_PP, GPIO_MODE_AF_PP, GPIO_MODE_AF_PP, GPIO_MODE_AF_PP
#define CFG_TIM2_CH_1_4_GPIO_Mode_Out    GPIO_MODE_AF_PP, GPIO_MODE_AF_PP, GPIO_MODE_AF_PP, GPIO_MODE_AF_PP
#define CFG_TIM3_CH_1_4_GPIO_Mode_Out    GPIO_MODE_AF_PP, GPIO_MODE_AF_PP, GPIO_MODE_AF_PP, GPIO_MODE_AF_PP
#define CFG_TIM4_CH_1_4_GPIO_Mode_Out    GPIO_MODE_AF_PP, GPIO_MODE_AF_PP, GPIO_MODE_AF_PP, GPIO_MODE_AF_PP

//! Decide if CapCom GPIO mode is floating input or with pull-up/pull-down resistors
#define CFG_TIM1_CH_1_4_GPIO_Mode_In    GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING
#define CFG_TIM2_CH_1_4_GPIO_Mode_In    GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING
#define CFG_TIM3_CH_1_4_GPIO_Mode_In    GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING
#define CFG_TIM4_CH_1_4_GPIO_Mode_In    GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING, GPIO_Mode_IN_FLOATING

//! \name Configuration for class PwmGen
//! Ref. class PwmGen\n
//! Please modify to fit your board
#define CFG_PWM_RESOLUTION_DECIMALS    1   //!< 1 or 2 decimals in Duty-cycle resolution, here 0.1 %

enum DC_LEVEL
{
  DC_0_PERC = 0,
#if CFG_PWM_RESOLUTION_DECIMALS == 1
  DC_100_PERC = 1000,
#elif CFG_PWM_RESOLUTION_DECIMALS == 2
  DC_100_PERC = 10000
#else
  #error "CFG_PWM_RESOLUTION_DECIMALS not defined correctly"
#endif
};

#define PWM_BACKLIGHT_STEP 20

//! \name Configuration for class EmulatedEEprom
//! Ref. class EmulatedEEprom\n
//! Please modify to fit your board
#define CFG_EMULATED_EEPROM_START_ADDRESS   0x0807f000    //!< EEPROM emulation start address. Last two pages in a 512K device
#define CFG_EMULATED_EEPROM_START_PAGE      126           //!< EEPROM emulation start page. Last two pages in a 512K device
#define CFG_EMULATED_EEPROM_START_BANK      2             //!< EEPROM emulation bank. Upper bank
#define CFG_EMULATED_EEPROM_SIZE            256           //!< 32, 64, 128, 256 locations
#define CFG_BOOT_SETTINGS_BANK_NUMBER       2             //!< Bank2
#define CFG_BOOT_SETTINGS_PAGE              (CFG_EMULATED_EEPROM_START_PAGE - 1) //!< Page before EEPROM. Address defined in linker config file

//! \name Configuration for class Watchdog
//! Ref. class Watchdog\n
//! Please modify to fit your board
#define CFG_WATCHDOG_TIMEOUT 20000                       //!< 20000 milliseconds watchdog timeout

//! \name Configuration for class VirtualComPort / USB
//! Ref. class VirtualComPort\n
//! Please modify to fit your board
#define USB_DISCONNECT                      GPIOA
#define USB_DISCONNECT_PIN                  GPIO_Pin_8
#define USB_DISCONNECT_PIN_Mode_Out_Type    GPIO_Mode_Out_PP // GPIO_Mode_Out_PP or GPIO_Mode_Out_OD
#define RCC_APB2Periph_GPIO_DISCONNECT      RCC_APB2Periph_GPIOA
#define USB_DISCONNECT_ACTIVE_STATE         Bit_RESET        // Active state for USB disconnect
#define USB_DISCONNECT_INACTIVE_STATE       Bit_SET          // Inactive state for USB disconnect
#define USB_BUS_POWERED                                      // Device use power from USB bus

//! \name Configuration for class I2Cbus
//! Ref. class I2Cbus\n
//#define USE_TIMER_FOR_DELAY_uS
//#define CFG_DELAY_uS_TIMx                   TIM14
//#define CFG_DELAY_uS_RCC_APB1Periph_TIMx    RCC_APB1Periph_TIM14
#define CFG_I2C_BUS_ERR_TIMEOUT             50
#define CFG_I2C1_REMAP                      0
#define CFG_I2C_TX_BUF_SIZE                 40

#define ADC_ECG_SAMPLE_TIMx                 TIM10 
#define RCC_APBPeriph_TIMx                  RCC_APB2Periph_TIM10
#define TIM_UP_TIMx_IRQn                    TIM1_UP_TIM10_IRQn

//ADC channels
#define AD_5V           ADC_CHANNEL_0
#define AD_AMB_LIGHT    ADC_CHANNEL_1
#define AD_IMPEDANCE    ADC_CHANNEL_2
#define AD_ECG          ADC_CHANNEL_3



//PWM channels
#define PWM_RED_LED     10
#define PWM_GREEN_LED   11
#define PWM_LCD_LED     12
#define PWM_IMP_MEAS    13

#endif // _BOARD_CFG_H

