//
//FHRM_hw_api.h
//The HW abstraction class for Cortex STM32
//
#ifndef _FHRM_BOARD_HW_API_H
#define _FHRM_BOARD_HW_API_H

#include "commondefs.h"
#include "stm32l5xx_hal.h"
#include "stm32l5xx_ll_system.h"
#include "stm32l5xx_ll_pwr.h"
#include "stm32l5xx_ll_utils.h"
#include "stm32l5xx_ll_rcc.h"
#include "stm32l562_moyo_sd.h"

// forward declarations
class Mutex;
class SerialPort;
class DacDma;
class FHRMAudio;
class FHRMps;
class Adc;
class Rtc;
class I2Cbus;
class Bq27510;
class ISerial;
class VirtualComPort;
class PwmGen;
class EmulatedEEprom;
class Watchdog;


//
//! \brief The Cortex STM32 hardware wrapper class
//
class HwAbsl
{
public:
    
    HwAbsl();
    ~HwAbsl();
    
    enum OnOff_t{eOFF = 0, eON = 1};
    enum USBpowerMode_t{eUSB_PM_100mA_noCharge = 0, eUSB_PL_500mA_Charge, eUSB_1000mA_Charge, eUSB_PL_500mA_noCharge};
    enum StartupMode_t{eStartupModeNormal = 0, eStartupModeUSBpowered, eStartupModeUSBvirtCOM};
    enum FHRledColor_t{eFhrLEdColorOff = 0, eFhrLEdColorGreen, eFhrLEdColorYellow, eFhrLEdColorRed};
    enum FHRblinkMode_t{eFhrBMBlinkOnce = 0, eFhrBMBlinkContinous};
    enum ChargerStatus_t{eCSTfault = 0x00, eCSTchargeComplete = 0x01, eCSTcharging = 0x02, eCSTshutdown};
    
    typedef struct
    {
        FHRblinkMode_t blinkMode;
        FHRledColor_t color;
        OnOff_t ledState;
        uint16_t onTime;
        uint16_t offTime;
    } FHRLedCtrl;
    
    
    //! \brief Get the system clock frequency after setup
    //! \return : uint32_t System clock in Hz
    uint32_t getSysClk() { return SystemCoreClock; }
    static HwAbsl* getpHw() { return pHw; }
    SerialPort* getpSerialConsole() const {return m_pSerialConsole; }
    FHRMAudio* getpAudio() const {return m_pAudio; }
    DacDma* getpDac() const {return m_pDac; }
    FHRMps* getProbeSerial() const {return m_pFHRMps; }
    Adc* getpADC() const {return m_pAdc; }
    PwmGen* getpPWM() const {return m_pPwm; }
    Rtc* getpRtc() const {return m_pRtc; }
    I2Cbus* getpI2C() const {return m_pI2C; }
    Bq27510* getpBq27510() const {return m_pBq27510; }
    VirtualComPort* getpVirtCOM() const {return m_pVirtCOM; }
    EmulatedEEprom* getpEEprom() const {return m_pEEprom; }
    Watchdog* getpWatchdog() const {return m_pWatchdog; }

    void audioOn();
    void audioOff();
    bool isAudioOn();
    void RedLED(GPIO_PinState state);
    void GreenLED(GPIO_PinState state);
    void systemShutDown(void);
    void enable5V(bool enable);
    void enableSDpower(bool enable);
    void updateFhrBlinkRate(uint16_t a_bpm, FHRledColor_t a_color);
    uint16_t getFhrBlinkPer(void);
    FHRledColor_t getFhrBlinkColor(void);
    void enableLCDreset(bool enable);
    void enableLCD(bool enable);
    void enableAmbLightSensor(bool enable);
    void setUSBcurrentMode(USBpowerMode_t limit);
    USBpowerMode_t getUSBcurrentMode(void);
    void transducerPower(bool state);
    bool isOffKeyPressed();
    bool isDisplayModeKeyPressed();
    bool isAudioVolumeKeyPressed();
    bool isOnKeyPressed();
    bool isTestmodeEnabled(void);
    StartupMode_t getStartupMode();
    void setStartupMode(StartupMode_t a_startupMode);
    bool isUSBpowered();
    void setDutycycleBackLight(uint16_t dc);
    void setImpedanceFreq(bool enable);
    void setRedLED(bool enable);
    void setRedLED(uint16_t dc);
    void setGreenLED(bool enable);
    void setGreenLED(uint16_t dc);
    void FHRledBlinkStop(void);
    void FHRledBlinkContinous(FHRledColor_t a_color, uint16_t a_onTimeMs, uint16_t a_cycleTimeMs);
    POST_ctrl_t getPostResult(void);
    void clearPostResult(void);
    ChargerStatus_t getChargeStatus(void);
    void initVirtualCom(void);
    void SDcardInit(void);
    

protected:
    

private:

    void sysTick_init();
    void perip_init();
    void gpio_init();
    void usarts_init();
    void audio_init();
    void FHRM_probeComm_init();
    void RTCinit();
    void I2Cinit();
    void VirtCOMinit();
    int32_t FSMCinit(void);
    void ADCinit();
    void PWMinit(void);
    void EEpromInit();
    void WDinit();
    void initFHRledTimer(void);
    static void TMR_FHRled_ISR(void);
    void MspInit(void);
    //void ADC_MspInit(ADC_HandleTypeDef* hadc);
    void ADC_MspDeInit(ADC_HandleTypeDef* hadc);
    void TIM_Base_MspInit(TIM_HandleTypeDef* htim_base);
    void TIM_Base_MspDeInit(TIM_HandleTypeDef* htim_base);
    void SystemClock_Config(void);
    //void initFmcMsp(void);
    void FMC_MspInit(SRAM_HandleTypeDef *hSram);
    HAL_StatusTypeDef MX_FMC_BANK4_Init(SRAM_HandleTypeDef *hSram);
    void PeriphCommonClock_Config(void);
    
    //friend void HAL_FMC_MspInit(SRAM_HandleTypeDef *hSram);
    //friend void HAL_SRAM_MspInit(SRAM_HandleTypeDef* hsram);
    friend void TIM15_IRQHandler(void);
    
    bool checkSum();
    StartupMode_t m_startupMode;
    
    uint16_t m_fhrBlinkPer;
    uint16_t m_currentFhrBlinkPer;
    FHRledColor_t m_fhrLedColor;
    POST_ctrl_t m_POSTstatus;
    USBpowerMode_t m_UsbPowerMode;
    uint32_t m_FmcInitialized;
    SRAM_HandleTypeDef m_FmcSram;
    TIM_HandleTypeDef m_FhrLedTim;
    


    SerialPort          * m_pSerialConsole;
    static HwAbsl       * pHw;
    DacDma              * m_pDac;
    FHRMAudio           * m_pAudio;
    FHRMps              * m_pFHRMps;
    Adc                 * m_pAdc;
    PwmGen              * m_pPwm;
    Rtc                 * m_pRtc;
    I2Cbus              * m_pI2C;
    Bq27510             * m_pBq27510;
    VirtualComPort      * m_pVirtCOM;
    EmulatedEEprom      * m_pEEprom;
    Watchdog            * m_pWatchdog;
    
    FHRLedCtrl          m_FhrLedCtrl;

};
#endif

