//! \file    adccfg.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    08.03.2010

#ifndef _ADC_CFG_H
#define _ADC_CFG_H

// header includes

// forward declarations
// ...

#define ADC_VREF 3300uL //! the ADC's voltage reference

//!< Comment out the next line if you need individual sampling times for each channel
#define CFG_USE_COMMON_SAMPLETIME_FOR_ALL_STD_CHANNELS 

//!< Please refer to formula in device datasheet for R_ain
//!< Adjust accordingly to your analog source impedance
#define CFG_SAMPLETIME_DEFAULT_ALL_STD_CH   ADC_SampleTime_13Cycles5 //!< max R_ain = 11.4 kOhm @ 14 MHz ADC clock

#if defined (STM32F10X_LD_VL) || defined (STM32F10X_MD_VL)
  // ADCCLK = PCLK2/4
  #define CFG_ADCCLK RCC_PCLK2_Div4 // 6MHz @ 24MHz pclk, note: max allowed = 12 MHz
#else
  // ADCCLK = PCLK2/8
  #define CFG_ADCCLK RCC_PCLK2_Div8 // 9MHz @ 72MHz pclk, note: max allowed = 14 MHz
#endif


//!< Please refer to formula in device datasheet for R_ain
//!< Adjust accordingly to your analog source impedance
#define CFG_ADC_CH0_SMPTIM  ADC_SampleTime_13Cycles5   // max R_ain = 11.4 kOhm @ 14 MHz ADC clock
#define CFG_ADC_CH1_SMPTIM  ADC_SampleTime_13Cycles5
#define CFG_ADC_CH2_SMPTIM  ADC_SampleTime_13Cycles5
#define CFG_ADC_CH3_SMPTIM  ADC_SampleTime_13Cycles5
#define CFG_ADC_CH4_SMPTIM  ADC_SampleTime_13Cycles5
#define CFG_ADC_CH5_SMPTIM  ADC_SampleTime_13Cycles5
#define CFG_ADC_CH6_SMPTIM  ADC_SampleTime_13Cycles5
#define CFG_ADC_CH7_SMPTIM  ADC_SampleTime_13Cycles5
#define CFG_ADC_CH8_SMPTIM  ADC_SampleTime_13Cycles5
#define CFG_ADC_CH9_SMPTIM  ADC_SampleTime_13Cycles5
#define CFG_ADC_CH10_SMPTIM ADC_SampleTime_13Cycles5
#define CFG_ADC_CH11_SMPTIM ADC_SampleTime_13Cycles5
#define CFG_ADC_CH12_SMPTIM ADC_SampleTime_13Cycles5
#define CFG_ADC_CH13_SMPTIM ADC_SampleTime_13Cycles5
#define CFG_ADC_CH14_SMPTIM ADC_SampleTime_13Cycles5
#define CFG_ADC_CH15_SMPTIM ADC_SampleTime_13Cycles5

#define CFG_ADC_CH16_SMPTIM ADC_SampleTime_239Cycles5 // temperature channel, required 17.1 uS @ 14 MHz ADC clock
#define CFG_ADC_CH17_SMPTIM ADC_SampleTime_239Cycles5 // Vref channel, required 17.1 uS @ 14 MHz ADC clock

#endif   // _ADC_CFG_H

//======== End of adccfg.h ==========

