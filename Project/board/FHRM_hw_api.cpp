/******************************************************************

    NAME: HW Abstraction layer

    FILE: FHRM_hw_api.cpp

    AUTHOR:  Jan A. R�yneberg

    DESCRIPTION: The HW abstraction class for Cortex STM32F103VF

    PROJECT:    Moyo

    DEVELOPED BY:

    (C)Laerdal Medical A/S, Stavanger, Norway, 2014

    All rights reserved. Reproduction, use, copy, distribution or
    disclosure to third parties without expressed authority is forbidden.
    If such is authorised, both the copyright note above, and this text,
    must appear in all copies and supporting documentation.

    Express authority is also needed to change this software. Modified
    software is not allowed to be distributed.

    Laerdal Medical A/S
    Research & Development
    P.O. Box 377
    N-4001 Stavanger
    Norway


    HISTORY (version, date, signature, note):
    12feb2014, JRo, Original.
    
*******************************************************************/

#include "FHRM_hw_api.h"
#include "isrpriocfg.h"
#include "includes.h"
#include "board_cfg.h"
#include "guard.h"
#include "mutex.h"
#include "serialport.h"
//#include "driverutils.h"
#include "dacdma.h"
#include "FHRMAudio.h"
#include "fhrmprobeserial.h"
#include "rtc.h"
#include "virtualcomport.h"
//#include "GpioLED.h"
#include "TimerThread.h"
#include "thread.h"
#include "Button.hpp"
#include "adc.h"
#include "pwmgen.h"
#include "EmulatedEEprom.h"
#include "i2cbus.h"
#include "bq27510.h"
#include "watchdog.h"
#include "dwt_stm32_delay.h"

using namespace btn_space;

// Define the reading of hw port for nON_KEY button
template<> inline int btn_space::read_hw_port<nKey_pressed_Pin>()
{
  return HAL_GPIO_ReadPin(nKey_pressed_GPIO_Port, nKey_pressed_Pin);
}

// Define the reading of hw port for VolumeUp button
template<> inline int btn_space::read_hw_port<volume_SW_Pin>()
{
  return HAL_GPIO_ReadPin(volume_SW_GPIO_Port, volume_SW_Pin);
}

// Define the reading of hw port for display mode button
template<> inline int btn_space::read_hw_port<toggle_SW_Pin>()
{
  return HAL_GPIO_ReadPin(toggle_SW_GPIO_Port, toggle_SW_Pin);
}

enum BTN_PIN_STATE
{
  //The button pin reading when released, pressed  
  BTN_PIN_RELEASED = 1,
  BTN_PIN_PRESSED  = 0
};

enum ONOFF_BTN_STATE
{
  //Note: these are button 'virtual' state - i.e state after pin state has been de-bounced.
  BTN_OFF = 0,
  BTN_ON  = 1
};

//! \brief Setup On/Off button
static OnOffHoldButton<
    IButton::STATE(BTN_OFF),
    IButton::STATE(BTN_ON),
    10,  // poll every x ms
    1500, // time in ms for ON to OFF Transition
    10, // time in ms for OFF to ON Transition
    nKey_pressed_Pin,
    sampling_debounce<BTN_PIN_RELEASED, BTN_PIN_PRESSED> >
_OnOffBtn;

//! \brief Setup display mode button
static OnOffHoldButton<
    IButton::STATE(BTN_OFF),
    IButton::STATE(BTN_ON),
    10,  // poll every x ms
    10, // time in ms for ON to OFF Transition
    10, // time in ms for OFF to ON Transition
    toggle_SW_Pin,
    sampling_debounce<BTN_PIN_RELEASED, BTN_PIN_PRESSED> >
_DisplayModeBtn;


static sampling_debounce<BTN_PIN_RELEASED, BTN_PIN_PRESSED> _displayModeBouncer;
static sampling_debounce<BTN_PIN_RELEASED, BTN_PIN_PRESSED> _audioVolBouncer;
static sampling_debounce<BTN_PIN_RELEASED, BTN_PIN_PRESSED> _onButtonBouncer;

// static member init
HwAbsl*  HwAbsl::pHw = 0;


//Prototypes for library support routines
//
extern "C"
{
    int MyLowLevelPutchar(int x);
    int MyLowLevelGetchar();
}


//
//! Construction
//! All basic hardware setup done here
//

HwAbsl::HwAbsl() : m_pSerialConsole(0), m_pDac(0), m_pAudio(0), m_pFHRMps(0), m_pRtc(0), m_pVirtCOM(0), m_pAdc(0), m_pPwm(0), m_pEEprom(0),
                   m_pI2C(0), m_pBq27510(0), m_fhrBlinkPer(1000), m_currentFhrBlinkPer(1000), m_fhrLedColor(eFhrLEdColorOff), m_pWatchdog(0)
{
    if (pHw == 0) // prevent multiple construct
    {
        pHw = this;
        
        //__disable_irq();
        
#ifdef USE_DFU_BOOTLOADER
        // Set the Vector Table base location at APP_START_OFFSET
        //NVIC_SetVectorTable(NVIC_VectTab_FLASH, APP_START_OFFSET); TODO
#endif

        m_startupMode = HwAbsl::eStartupModeNormal;
        m_POSTstatus.finished = false;
        m_POSTstatus.post.postWord = 0;
        m_UsbPowerMode = eUSB_PM_100mA_noCharge;
        m_FmcInitialized = 0;
        
        HAL_Init();
        
        /** Disable the internal Pull-Up in Dead Battery pins of UCPD peripheral 
        */
        LL_PWR_DisableUCPDDeadBattery();
        
        SystemClock_Config();
        
        PeriphCommonClock_Config();
        
        __enable_irq();

        perip_init();
        
        initFHRledTimer();

        

    }

}



//
// Destruction
//
HwAbsl::~HwAbsl()
{
	pHw = 0;
}

//
//! Init the various peripherals
//
void HwAbsl::perip_init()
{
    
    PWMinit();
    gpio_init();
    WDinit(); 
    DWT_Delay_Init();
    bool displayModeBtnPressed = false;
    
    //Check startup reason
    //If power button not pressed, startup cause must be USB connection
    uint16_t cnt = 20;
    
    while(isOnKeyPressed() == false && cnt > 0)
    {
        cnt--;
        DWT_Delay_us(10000);
    }
#ifdef SIM_POWER_ON_BTN_PRESSED
    cnt = 1;
#endif    
    
    if(cnt == 0)
    {
        //On button not pressed. Must be started with USB power
        //Wait for power good before goto full speed
        cnt = 100;
        while(isUSBpowered() == false && cnt > 0)
        {
            DWT_Delay_us(10000);
            cnt--;
            if(cnt == 0)
            {
                //Power good was never set. Shutoff
                systemShutDown();
            }
        }
    }
    else
    {
        //Check if On button is hold for at least 300mS
#ifdef SIM_POWER_ON_BTN_PRESSED
        m_startupMode = HwAbsl::eStartupModeNormal;
#else
        cnt = 0;
        while(isOnKeyPressed())
        {
            DWT_Delay_us(1000);
            cnt++;
            if(cnt >= 300)
            {
                m_startupMode = HwAbsl::eStartupModeNormal;
                
                break;
            }
        }
        if(cnt < 300)
        {
            //Shut off power. On button is not hold for 500ms
            systemShutDown();
        }
#endif   
        //Turn on green LED
        setGreenLED(true);

    }
    
    /*
    for( ;; )
    {
        if (0 == _DisplayModeBtn.poll()) //wait util 1 transition period expired
        {
            if (_DisplayModeBtn.getState() == IButton::STATE(BTN_ON))
            {
                //Button ON -> Enable USB virtual COM console port
                displayModeBtnPressed = true;  
                break;
            }
            else
            {
                //Button OFF -> Enable UART console
                displayModeBtnPressed = false;
                break;
            }
        }
    }
    */
    
    //Test flash memory checksum
    bool crcOK = checkSum();
    if(!crcOK)
    {
        m_POSTstatus.post.postBits.programCheckSum = true;
    }
    m_POSTstatus.finished = true;
        
    if(!isUSBpowered())
    {
        if(!crcOK)
        {
            //Wrong flash checksum
            //Enable red LED. We will try to run until POST test (UI)
            setRedLED(true);
            setGreenLED(false);
        }
        
    }
    else
    {
        if(displayModeBtnPressed)
        {
            m_startupMode = HwAbsl::eStartupModeUSBvirtCOM;
        }
        else
        {
            m_startupMode = HwAbsl::eStartupModeUSBpowered;
        }
    }
    
    _OnOffBtn.setState(IButton::STATE( BTN_ON )); // ARM the ON/OFF button
    
    SDcardInit();
    ADCinit();
    RTCinit();
    I2Cinit();
    usarts_init();
    EEpromInit();
    FSMCinit();
    FHRM_probeComm_init();
    audio_init();
    
    
}

void HwAbsl::SDcardInit()
{
    BSP_SD_Init(0);
    
}
//
//! Init watchdog timer
//
void HwAbsl::WDinit()
{
    if (0 == m_pWatchdog)
    {
        m_pWatchdog = new Watchdog();
        
#ifdef NDEBUG //Only start watchdog in release config
        m_pWatchdog->start();
#endif
    }
}

//
//! Init Real time clock
//
void HwAbsl::RTCinit()
{
    if (0 == m_pRtc)
    {
        m_pRtc = new Rtc();
    }   
}

//
//! Init I2C
//
void HwAbsl::I2Cinit()
{
    if (0 == m_pI2C)
    {

#ifndef NO_FUEL_GAUGE
        m_pI2C = new I2Cbus(I2Cbus::I2CPORT_1, 100000, 0);
        m_pBq27510 = new Bq27510(m_pI2C);
#endif

    }   
}

//
//! Init ADC
//
void HwAbsl::ADCinit()
{
    if (0 == m_pAdc)
    {
        m_pAdc = new Adc();
    }
}

//
//! Init the GPIO pins
//
void HwAbsl::gpio_init()
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    // Enable GPIO clock
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_GPIOF_CLK_ENABLE();
    __HAL_RCC_GPIOG_CLK_ENABLE();
    

    // Set PA8 (USB enable) to output push pull.
    //GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    //GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
    //GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    // Set nSYSTEM_SHDN to output push pull and deactivate.
    HAL_GPIO_WritePin(nSYSTEM_SHDN_GPIO_Port, nSYSTEM_SHDN_Pin, GPIO_PIN_SET);
    GPIO_InitStruct.Pin = nSYSTEM_SHDN_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(nSYSTEM_SHDN_GPIO_Port, &GPIO_InitStruct);
    
    
    // Set EN_5V to output push pull and deactivate.
    HAL_GPIO_WritePin(EN_5V_GPIO_Port, EN_5V_Pin, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = EN_5V_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(EN_5V_GPIO_Port, &GPIO_InitStruct);
    
    // Set EN_LCD to output push pull and deactivate.
    HAL_GPIO_WritePin(EN_LCD_GPIO_Port, EN_LCD_Pin, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = EN_LCD_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(EN_LCD_GPIO_Port, &GPIO_InitStruct);
    
    // Set nAudio_SHDN to output push pull and deactivate.
    HAL_GPIO_WritePin(nAudio_SHDN_GPIO_Port, nAudio_SHDN_Pin, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = nAudio_SHDN_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(nAudio_SHDN_GPIO_Port, &GPIO_InitStruct);
    
    //Set USB charge current to minimum current (maximum of what a PC can deliver without negotioation).
    setUSBcurrentMode(eUSB_PL_500mA_noCharge);    
    
    // Set BAT_Sel to output push pull
    GPIO_InitStruct.Pin = BAT_Sel_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BAT_Sel_GPIO_Port, &GPIO_InitStruct);
    
    // Set BAT_Prog2 to output push pull
    GPIO_InitStruct.Pin = BAT_Prog2_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BAT_Prog2_GPIO_Port, &GPIO_InitStruct);
    
    // Set BAT_nTE to output push pull
    GPIO_InitStruct.Pin = BAT_nTE_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BAT_nTE_GPIO_Port, &GPIO_InitStruct);
    
    // Set BAT_CE to output push pull
    GPIO_InitStruct.Pin = BAT_CE_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BAT_CE_GPIO_Port, &GPIO_InitStruct);
    
    // Set EN_Transducer to output open drain and activate.
    // Note: If enabling EN_Transducer after 5V, 5V regulator can become unstable
    // We therefore keep EN_Transducer always avtive and turn transducer on and off with EN_5V
    HAL_GPIO_WritePin(EN_Transducer_GPIO_Port, EN_Transducer_Pin, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = EN_Transducer_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(EN_Transducer_GPIO_Port, &GPIO_InitStruct);

    // Set BAT_nPG to input
    GPIO_InitStruct.Pin = BAT_nPG_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BAT_nPG_GPIO_Port, &GPIO_InitStruct);
    
    
    // Set EN_Phototran to output push pull and deactivate.
    HAL_GPIO_WritePin(EN_Phototran_GPIO_Port, EN_Phototran_Pin, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = EN_Phototran_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(EN_Phototran_GPIO_Port, &GPIO_InitStruct);
    
    // Set SD_Power to output push pull and deactivate.
    HAL_GPIO_WritePin(SD_Power_GPIO_Port, SD_Power_Pin, GPIO_PIN_RESET);
    GPIO_InitStruct.Pin = SD_Power_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SD_Power_GPIO_Port, &GPIO_InitStruct);
    
    // Set Test_EN to input
    GPIO_InitStruct.Pin = Test_EN_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(Test_EN_GPIO_Port, &GPIO_InitStruct);
    
    
    // Set BAT_Stat0 to input
    GPIO_InitStruct.Pin = BAT_Stat0_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BAT_Stat0_GPIO_Port, &GPIO_InitStruct);
    
    // Set BAT_Stat1 to input
    GPIO_InitStruct.Pin = BAT_Stat1_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BAT_Stat1_GPIO_Port, &GPIO_InitStruct);
    
    // Set n_KEY_PRESSED to input and enable weak pullup
    GPIO_InitStruct.Pin = nKey_pressed_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(nKey_pressed_GPIO_Port, &GPIO_InitStruct);
    
    // Set toggle_SW (display mode) to input and enable weak pullup
    GPIO_InitStruct.Pin = toggle_SW_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(toggle_SW_GPIO_Port, &GPIO_InitStruct);
    
    // Set volume_SW to input and enable weak pullup
    GPIO_InitStruct.Pin = volume_SW_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(volume_SW_GPIO_Port, &GPIO_InitStruct);
    
    
    
    //Display
    // Set LCD_nRST to output push pull and deactivate.
    HAL_GPIO_WritePin(LCD_nRST_GPIO_Port, LCD_nRST_Pin, GPIO_PIN_SET);
    GPIO_InitStruct.Pin = LCD_nRST_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LCD_nRST_GPIO_Port, &GPIO_InitStruct);
    
}

//
//! Init audio
//
void HwAbsl::audio_init()
{
    //
    //! Init DAC
    //
    if (0==m_pDac)
    {
        m_pDac = new DacDma();
    }

        
    if (0==m_pAudio && m_pDac)
    {
        m_pAudio = new FHRMAudio(m_pDac);
    }
}

//
//! Init USARTs
//
void HwAbsl::usarts_init()
{

#ifndef NDEBUG
    if (0 == m_pSerialConsole)
    {
        if(m_startupMode != HwAbsl::eStartupModeUSBvirtCOM)
        {
            m_pSerialConsole = new SerialPort(SerialPort::COM3, USART3_BAUD, USART3_RXBUFSZ, USART3_TXBUFSZ);
        }
    }
#endif
}

//
//! Init Serial comm/DMA for doppler probe
//
void HwAbsl::FHRM_probeComm_init()
{
    if (0 == m_pFHRMps)
    {
        m_pFHRMps = new FHRMps();
    }
    
}

//
//! Init PWM
//
void HwAbsl::PWMinit(void)
{
    //PWM on ch 10 and 11, using TIM2-CH3 and TIM2-CH4 with remap (CFG_TIM2_REMAP = 3)
    //PWM on ch 12 and 13, using TIM3-CH1 and TIM3-CH2 with remap (CFG_TIM3_REMAP = 3)
    m_pPwm = new PwmGen(100000, 0); 
    
    //Default off
    setDutycycleBackLight(0); 
    setImpedanceFreq(false);
    setRedLED(false);
    setGreenLED(false);
}


//
//! Init the FSMC that is used for display
//
int32_t HwAbsl::FSMCinit(void)
{
    int32_t status = BSP_ERROR_NONE;
  
    memset(&m_FmcSram, 0, sizeof(m_FmcSram));
    
  m_FmcSram.Instance    = FMC_NORSRAM_DEVICE;
  m_FmcSram.Extended    = FMC_NORSRAM_EXTENDED_DEVICE;
  m_FmcSram.Init.NSBank = FMC_NORSRAM_BANK4;

  if (HAL_SRAM_GetState(&m_FmcSram) == HAL_SRAM_STATE_RESET)
  {
#if (USE_HAL_SRAM_REGISTER_CALLBACKS == 0)
    /* Init the FMC Msp */
    FMC_MspInit(&m_FmcSram);

    if (MX_FMC_BANK4_Init(&m_FmcSram) != HAL_OK)
    {
      status = BSP_ERROR_BUS_FAILURE;
    }
#else
    if (Lcd_IsSramMspCbValid[0] == 0U)
    {
      if (BSP_LCD_RegisterDefaultMspCallbacks(0) != BSP_ERROR_NONE)
      {
        status = BSP_ERROR_MSP_FAILURE;
      }
    }

    if (status == BSP_ERROR_NONE)
    {
      if (MX_FMC_BANK4_Init(&m_FmcSram) != HAL_OK)
      {
        status = BSP_ERROR_BUS_FAILURE;
      }
    }
#endif
  }
  return status;
}

/**
  * @brief  Initializes FMC MSP.
  * @param  hSram : SRAM handler
  * @retval None
  */
void HwAbsl::FMC_MspInit(SRAM_HandleTypeDef *hSram)
{
  GPIO_InitTypeDef  gpio_init_structure;

  /* Prevent unused argument(s) compilation warning */
  UNUSED(hSram);

  /*** Configure the GPIOs ***/

  /* Enable VddIO2 for GPIOG */
  __HAL_RCC_PWR_CLK_ENABLE();
  HAL_PWREx_EnableVddIO2();

  /* Enable FMC clock */
  __HAL_RCC_FMC_CLK_ENABLE();

  /* Enable GPIOs clock */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /* Common GPIO configuration */
  gpio_init_structure.Mode      = GPIO_MODE_AF_PP;
  gpio_init_structure.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
  gpio_init_structure.Alternate = GPIO_AF12_FMC;
  gpio_init_structure.Pull      = GPIO_PULLDOWN;

  //TODO: Define pins and ports in boar_cfg.h
  /*## NE configuration #######*/
  /* NE4 : LCD */
  gpio_init_structure.Pin = GPIO_PIN_12;
  HAL_GPIO_Init(GPIOG, &gpio_init_structure);

  /*## NOE and NWE configuration #######*/
  gpio_init_structure.Pin = GPIO_PIN_4 | GPIO_PIN_5;
  HAL_GPIO_Init(GPIOD, &gpio_init_structure);

  /*## RS configuration #######*/
  gpio_init_structure.Pin = GPIO_PIN_6;
  HAL_GPIO_Init(GPIOE, &gpio_init_structure);

  /*## Data Bus #######*/
  gpio_init_structure.Pull      = GPIO_NOPULL;
  /* GPIOD configuration */
  gpio_init_structure.Pin = GPIO_PIN_0  | GPIO_PIN_1  | GPIO_PIN_8 | GPIO_PIN_9 |\
                            GPIO_PIN_10 | GPIO_PIN_14 | GPIO_PIN_15;
  HAL_GPIO_Init(GPIOD, &gpio_init_structure);

  /* GPIOE configuration */
  gpio_init_structure.Pin = GPIO_PIN_7  | GPIO_PIN_8  | GPIO_PIN_9  | GPIO_PIN_10 |\
                            GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 |\
                            GPIO_PIN_15;
  HAL_GPIO_Init(GPIOE, &gpio_init_structure);
}

/**
  * @brief  MX FMC BANK4 initialization.
  * @param  hSram SRAM handle.
  * @retval HAL status.
  */
HAL_StatusTypeDef HwAbsl::MX_FMC_BANK4_Init(SRAM_HandleTypeDef *hSram)
{
  static FMC_NORSRAM_TimingTypeDef  SramTiming = {0};
  static FMC_NORSRAM_TimingTypeDef  ExtendedTiming = {0};

  /* SRAM device configuration */
  hSram->Init.DataAddressMux         = FMC_DATA_ADDRESS_MUX_DISABLE;
  hSram->Init.MemoryType             = FMC_MEMORY_TYPE_SRAM;
  hSram->Init.MemoryDataWidth        = FMC_NORSRAM_MEM_BUS_WIDTH_16;
  hSram->Init.BurstAccessMode        = FMC_BURST_ACCESS_MODE_DISABLE;
  hSram->Init.WaitSignalPolarity     = FMC_WAIT_SIGNAL_POLARITY_LOW;
  hSram->Init.WaitSignalActive       = FMC_WAIT_TIMING_BEFORE_WS;
  hSram->Init.WriteOperation         = FMC_WRITE_OPERATION_ENABLE;
  hSram->Init.WaitSignal             = FMC_WAIT_SIGNAL_DISABLE;
  hSram->Init.ExtendedMode           = FMC_EXTENDED_MODE_ENABLE;
  hSram->Init.AsynchronousWait       = FMC_ASYNCHRONOUS_WAIT_DISABLE;
  hSram->Init.WriteBurst             = FMC_WRITE_BURST_DISABLE;
  hSram->Init.ContinuousClock        = FMC_CONTINUOUS_CLOCK_SYNC_ASYNC;
  hSram->Init.WriteFifo              = FMC_WRITE_FIFO_DISABLE;
  hSram->Init.PageSize               = FMC_PAGE_SIZE_NONE;
  hSram->Init.NBLSetupTime           = FMC_NBL_SETUPTIME_0;
  hSram->Init.MaxChipSelectPulse     = DISABLE;
  hSram->Init.MaxChipSelectPulseTime = 1;

  /* Following timings have been computed according HX8347I datasheet and with a clock of 110Mhz (clock cycle of 9ns) */
  /* Read timings */
  SramTiming.AddressSetupTime      = 4;//2; /* Minimum 10ns, 18ns computed */
  SramTiming.AddressHoldTime       = 4;//2; /* Minimum 10ns, 18ns computed */
  SramTiming.DataSetupTime         = 12;//6; /* Minimum 45ns, 54ns computed */
  SramTiming.DataHoldTime          = 6;//3; /* Minimum 20ns, 27ns computed */
  SramTiming.BusTurnAroundDuration = 1; /* Minimum 0ns, 9ns computed */
  SramTiming.CLKDivision           = 2; /* Minimum value, not used in mode D */
  SramTiming.DataLatency           = 2; /* Minimum value, not used in mode D */
  SramTiming.AccessMode            = FMC_ACCESS_MODE_D;
  /* Write timings */
  ExtendedTiming.AddressSetupTime      = 4;//2; /* Minimum 10ns, 18ns computed */
  ExtendedTiming.AddressHoldTime       = 4;//2; /* Minimum 10ns, 18ns computed */
  ExtendedTiming.DataSetupTime         = 4;//2; /* Minimum 15ns, 18ns computed */
  ExtendedTiming.DataHoldTime          = 2;//1; /* Minimum 10ns, 18ns computed */
  ExtendedTiming.BusTurnAroundDuration = 1; /* Minimum 0ns, 9ns computed */
  ExtendedTiming.CLKDivision           = SramTiming.CLKDivision; /* Not used in mode D */
  ExtendedTiming.DataLatency           = SramTiming.DataLatency; /* Not used in mode D */
  ExtendedTiming.AccessMode            = SramTiming.AccessMode;

  return HAL_SRAM_Init(hSram, &SramTiming, &ExtendedTiming);
}

//
//! Init emulated EEProm
//
void HwAbsl::EEpromInit()
{
    if (0 == m_pEEprom)
    {
        m_pEEprom = new EmulatedEEprom();
    }
}


//
//! Turn on or off doppler probe power
//
void HwAbsl::transducerPower(bool enable)
{

    //Ensure transducer power transistor is settled before enable 5V
    //If not so, 5V regulator can become unstable
    //delayUs(1000); 
    
    enable5V(enable);
}

//
//! Turn off the audio amplifier
//
void HwAbsl::audioOff()
{
    HAL_GPIO_WritePin(nAudio_SHDN_GPIO_Port, nAudio_SHDN_Pin, GPIO_PIN_RESET);
}

//
//! Turn on the audio amplifier
//
void HwAbsl::audioOn()
{
    HAL_GPIO_WritePin(nAudio_SHDN_GPIO_Port, nAudio_SHDN_Pin, GPIO_PIN_SET);
}

//
//! Get whether the audio amplifier is on or off 
//
bool HwAbsl::isAudioOn()
{
    return (HAL_GPIO_ReadPin(nAudio_SHDN_GPIO_Port, nAudio_SHDN_Pin) == GPIO_PIN_SET);
}

//
//! Turn off power
//
void HwAbsl::systemShutDown(void)
{
    __disable_irq();
    HAL_GPIO_WritePin(nSYSTEM_SHDN_GPIO_Port, nSYSTEM_SHDN_Pin, GPIO_PIN_RESET);
    while(1)
    {
        // noop
    }
    
}

//
//! Turn on or off power for SD card
//
void HwAbsl::enableSDpower(bool enable)
{
    if(enable)
    {
        HAL_GPIO_WritePin(SD_Power_GPIO_Port, SD_Power_Pin, GPIO_PIN_SET);
    }
    else
    {
        HAL_GPIO_WritePin(SD_Power_GPIO_Port, SD_Power_Pin, GPIO_PIN_RESET);
    }
 }


//
//! Turn on or off 5V
//
void HwAbsl::enable5V(bool enable)
{
    if(enable)
    {
        HAL_GPIO_WritePin(EN_5V_GPIO_Port, EN_5V_Pin, GPIO_PIN_SET);
    }
    else
    {
        HAL_GPIO_WritePin(EN_5V_GPIO_Port, EN_5V_Pin, GPIO_PIN_SET);
    }    
}

//
//! Get status for USB charger controller
//
HwAbsl::ChargerStatus_t HwAbsl::getChargeStatus(void)
{
    //TODO
    return (ChargerStatus_t)(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_5) | (HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_6) << 1));
}

//
//! Set current output for the USB power regulator
//
void HwAbsl::setUSBcurrentMode(USBpowerMode_t limit)
{
    //GPIO_Pin_1 = BAT_Sel
    //GPIO_Pin_2 = BAT_Prog2
    //GPIO_Pin_3 = BAT_nTE
    //GPIO_Pin_4 = BAT_CE

    switch(limit)
    {
        case eUSB_PM_100mA_noCharge:
            HAL_GPIO_WritePin(BAT_Sel_GPIO_Port, BAT_Sel_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(BAT_Prog2_GPIO_Port, BAT_Prog2_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(BAT_nTE_GPIO_Port, BAT_nTE_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(BAT_CE_GPIO_Port, BAT_CE_Pin, GPIO_PIN_RESET);
            break;
        case eUSB_PL_500mA_Charge:
            HAL_GPIO_WritePin(BAT_Sel_GPIO_Port, BAT_Sel_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(BAT_Prog2_GPIO_Port, BAT_Prog2_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(BAT_nTE_GPIO_Port, BAT_nTE_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(BAT_CE_GPIO_Port, BAT_CE_Pin, GPIO_PIN_SET);
            break;
        case eUSB_1000mA_Charge:
            HAL_GPIO_WritePin(BAT_Sel_GPIO_Port, BAT_Sel_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(BAT_Prog2_GPIO_Port, BAT_Prog2_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(BAT_nTE_GPIO_Port, BAT_nTE_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(BAT_CE_GPIO_Port, BAT_CE_Pin, GPIO_PIN_SET);
            break;
        case eUSB_PL_500mA_noCharge:
            HAL_GPIO_WritePin(BAT_Sel_GPIO_Port, BAT_Sel_Pin, GPIO_PIN_RESET);
            HAL_GPIO_WritePin(BAT_Prog2_GPIO_Port, BAT_Prog2_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(BAT_nTE_GPIO_Port, BAT_nTE_Pin, GPIO_PIN_SET);
            HAL_GPIO_WritePin(BAT_CE_GPIO_Port, BAT_CE_Pin, GPIO_PIN_RESET);
            break;
        default:
            break;
    }
    
    m_UsbPowerMode = limit;
}

//
//! Get current output for the USB power regulator
//
HwAbsl::USBpowerMode_t HwAbsl::getUSBcurrentMode(void)
{
    return m_UsbPowerMode;
}

//
//! Init Virtual COM port
//
void HwAbsl::initVirtualCom(void)
{
    if (0 == m_pSerialConsole)
    {
        //m_pSerialConsole = new VirtualComPort(HOSTCOMM_RXBUFSZ, HOSTCOMM_TXBUFSZ);
    }
}

//
//! Set backlight strength
//
void HwAbsl::setDutycycleBackLight(uint16_t dc)
{
    if(dc <= DC_100_PERC)
    {
        m_pPwm->setDuty(PWM_LCD_LED, dc);
    }    
}

//
//! Turn on or off measurement frequency (100MHz) for impedance measurement
//
void HwAbsl::setImpedanceFreq(bool enable)
{
    if(enable)
    {
        m_pPwm->setDuty(PWM_IMP_MEAS, DC_100_PERC/2); //50%
    }
    else
    {
        m_pPwm->setDuty(PWM_IMP_MEAS, DC_0_PERC); //Off
    }
        
}

//
//! Turn on or off red LED
//
void HwAbsl::setRedLED(bool enable)
{
    if(enable)
    {
        m_pPwm->setDuty(PWM_RED_LED, RED_LED_FULL_PWM);
    }
    else
    {
        m_pPwm->setDuty(PWM_RED_LED, DC_0_PERC); //Off
    }
        
}

//
//! Set strength for red LED
//
void HwAbsl::setRedLED(uint16_t dc)
{
    if(dc <= DC_100_PERC)
    {
        m_pPwm->setDuty(PWM_RED_LED, dc);
    }    
}

//
//! Turn on or off green LED
//
void HwAbsl::setGreenLED(bool enable)
{
    if(enable)
    {
        m_pPwm->setDuty(PWM_GREEN_LED, GREEN_LED_FULL_PWM);
    }
    else
    {
        m_pPwm->setDuty(PWM_GREEN_LED, DC_0_PERC); //Off
    }
        
}

//
//! Set strength for green LED
//
void HwAbsl::setGreenLED(uint16_t dc)
{
    if(dc <= DC_100_PERC)
    {
        m_pPwm->setDuty(PWM_GREEN_LED, dc);
    }    
}

//
//! Set blink rate and color for FHR status LED 
//
void HwAbsl::updateFhrBlinkRate(uint16_t a_bpm, FHRledColor_t a_color)
{

    m_fhrBlinkPer = (uint16_t)(60.0/(float)a_bpm*1000);
    if(m_fhrBlinkPer > 3000)
    {
        m_fhrBlinkPer = 3000;
    }
    
    m_fhrLedColor = a_color;
    
    m_FhrLedCtrl.color = a_color;
    m_FhrLedCtrl.offTime = (m_fhrBlinkPer * 10) - m_FhrLedCtrl.onTime;
    
}

//
//! Get current blink periode for FHR status LED
//
uint16_t HwAbsl::getFhrBlinkPer(void)
{
    return m_fhrBlinkPer;
}

//
//! Get current color for FHR status LED
//
HwAbsl::FHRledColor_t HwAbsl::getFhrBlinkColor(void)
{
    return m_fhrLedColor;
}

//
//! Set or clear the display reset lime
//
void HwAbsl::enableLCDreset(bool enable)
{
    if(enable)
    {
        HAL_GPIO_WritePin(LCD_nRST_GPIO_Port, LCD_nRST_Pin, GPIO_PIN_RESET);
    }
    else
    {
        HAL_GPIO_WritePin(LCD_nRST_GPIO_Port, LCD_nRST_Pin, GPIO_PIN_SET);
    }    
}

//
//! Turn on or off power for ambient light sensor
//
void HwAbsl::enableAmbLightSensor(bool enable)
{
    if(enable)
    {
        HAL_GPIO_WritePin(EN_Phototran_GPIO_Port, EN_Phototran_Pin, GPIO_PIN_SET);
    }
    else
    {
        HAL_GPIO_WritePin(EN_Phototran_GPIO_Port, EN_Phototran_Pin, GPIO_PIN_RESET);
    }    
}


//
//! Get whether the off key is pressed
//! Note: This function has to be frequently called to make the debouncer working
//
bool HwAbsl::isOffKeyPressed()
{
    _OnOffBtn.poll();
    return (IButton::STATE(BTN_OFF) == _OnOffBtn.getState() ? true : false);
}

//
//! Get whether the displau mode key is pressed
//! Note: This function has to be frequently called to make the debouncer working
//
bool HwAbsl::isDisplayModeKeyPressed()
{
    bool pinState = HAL_GPIO_ReadPin(toggle_SW_GPIO_Port, toggle_SW_Pin) == GPIO_PIN_SET;
    return _displayModeBouncer(pinState ? BTN_PIN_RELEASED : BTN_PIN_PRESSED) == BTN_PIN_PRESSED;
    
}

//
//! Get whether the volume key is pressed
//! Note: This function has to be frequently called to make the debouncer working
//
bool HwAbsl::isAudioVolumeKeyPressed()
{
    bool pinState = HAL_GPIO_ReadPin(volume_SW_GPIO_Port, volume_SW_Pin) == GPIO_PIN_SET;
    return _audioVolBouncer(pinState ? BTN_PIN_RELEASED : BTN_PIN_PRESSED) == BTN_PIN_PRESSED;
}

//
//! Get whether the on key is pressed
//! Note: This function has to be frequently called to make the debouncer working
//
bool HwAbsl::isOnKeyPressed()
{
    bool pinState = HAL_GPIO_ReadPin(nKey_pressed_GPIO_Port, nKey_pressed_Pin) == GPIO_PIN_SET;
    return _onButtonBouncer(pinState ? BTN_PIN_RELEASED : BTN_PIN_PRESSED) == BTN_PIN_PRESSED;
}

//
//! Get whether startup was normal (with power button), USB connected (no buttons pressed and USB connected),
//! or USB viritual COM (display mode button pressed and USB connection)
//
HwAbsl::StartupMode_t HwAbsl::getStartupMode()
{
    return m_startupMode;
}

//
//! Get whether the test mode is enabled (J200 pin 8 high)
//
bool HwAbsl::isTestmodeEnabled(void)
{
    //Check if test pin is active
    bool inTestMode = true;
    for(uint8_t i = 0; i < 100; i++)
    {
        if(HAL_GPIO_ReadPin(Test_EN_GPIO_Port, Test_EN_Pin) == GPIO_PIN_RESET)
        {
            inTestMode = false;
            break;
        }
        DWT_Delay_us(1000);
    }
    
    return inTestMode;
}

//
//! Override detected startup mode
//
void HwAbsl::setStartupMode(HwAbsl::StartupMode_t a_startupMode)
{
    m_startupMode = a_startupMode;
}


//
//! Get if USB powered based on Power Good line from USB power controller
//
bool HwAbsl::isUSBpowered()
{
    bool pinState = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4) == GPIO_PIN_RESET;
    return false;
    return pinState;
}

//
//! Init the interrupt timer used for FHR status LED
//
void HwAbsl::initFHRledTimer(void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig = {0};
    TIM_MasterConfigTypeDef sMasterConfig = {0};
    TIM_OC_InitTypeDef sConfigOC = {0};
    TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};
  
    //OS_EnterRegion();
    
    // Timer15 Init
    // Set timer period 1 sec intitially
    m_FhrLedTim.Instance = TIM15;
    m_FhrLedTim.Init.Prescaler = 11000;  // 100us resolution;
    m_FhrLedTim.Init.CounterMode = TIM_COUNTERMODE_UP;
    m_FhrLedTim.Init.Period = 10000;  // 1sec
    m_FhrLedTim.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    m_FhrLedTim.Init.RepetitionCounter = 0;
    m_FhrLedTim.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&m_FhrLedTim) != HAL_OK)
    {
        //Error_Handler();
    }
    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&m_FhrLedTim, &sClockSourceConfig) != HAL_OK)
    {
        //Error_Handler();
    }
    if (HAL_TIM_OC_Init(&m_FhrLedTim) != HAL_OK)
    {
        //Error_Handler();
    }
    sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&m_FhrLedTim, &sMasterConfig) != HAL_OK)
    {
        //Error_Handler();
    }
    sConfigOC.OCMode = TIM_OCMODE_TIMING;
    sConfigOC.Pulse = 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
    sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
    if (HAL_TIM_OC_ConfigChannel(&m_FhrLedTim, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
    {
        //Error_Handler();
    }
    sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
    sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
    sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
    sBreakDeadTimeConfig.DeadTime = 0;
    sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
    sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
    sBreakDeadTimeConfig.BreakFilter = 0;
    sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
    if (HAL_TIMEx_ConfigBreakDeadTime(&m_FhrLedTim, &sBreakDeadTimeConfig) != HAL_OK)
    {
        //Error_Handler();
    }
    
    //OS_LeaveRegion();
}

//
//! Stop blinking and turn off FHR status LED
//
void HwAbsl::FHRledBlinkStop(void)
{
    __HAL_TIM_DISABLE(&m_FhrLedTim);
    HwAbsl::getpHw()->setRedLED(false);
    HwAbsl::getpHw()->setGreenLED(false);
    
}

//
//! Set FHR status LED to blink continuously in choosen color
//
void HwAbsl::FHRledBlinkContinous(FHRledColor_t a_color, uint16_t a_onTimeMs, uint16_t a_cycleTimeMs)
{
    __HAL_TIM_DISABLE(&m_FhrLedTim);
    
    m_FhrLedCtrl.blinkMode = eFhrBMBlinkContinous;
    m_FhrLedCtrl.color = a_color;
    m_FhrLedCtrl.ledState = eOFF;
    
    if(a_onTimeMs > 6500)
    {
        //Prevent rollover
        m_FhrLedCtrl.onTime = 65000;
    }
    else
    {
        m_FhrLedCtrl.onTime = a_onTimeMs * 10;
    }
    
    if(a_cycleTimeMs > 6500)
    {
        //Prevent rollover
        m_FhrLedCtrl.offTime = 65000 - (a_onTimeMs * 10);
    }
    else
    {
        m_FhrLedCtrl.offTime = (a_cycleTimeMs * 10) - m_FhrLedCtrl.onTime;
    }
    
    
    if(a_color == eFhrLEdColorGreen)
    {
        getpHw()->setGreenLED(true);
    }
    else if(a_color == eFhrLEdColorYellow)
    {
        getpHw()->setGreenLED(GREEN_LED_REDUCED_PWM);
        getpHw()->setRedLED(RED_LED_REDUCED_PWM);
    }
    else if(a_color == eFhrLEdColorRed)
    {
        getpHw()->setRedLED(true);
    }
    
    //Make fire now
    m_FhrLedTim.Instance->ARR = 1;
    __HAL_TIM_ENABLE(&m_FhrLedTim);
}

//
//! ISR handler for FHR status LED timer
//
void HwAbsl::TMR_FHRled_ISR(void)
{
    if (__HAL_TIM_GET_FLAG(&getpHw()->m_FhrLedTim, TIM_FLAG_UPDATE) != RESET)
    {
        if (__HAL_TIM_GET_IT_SOURCE(&getpHw()->m_FhrLedTim, TIM_IT_UPDATE) != RESET)
        {
            /* TIMx interrupt interrupt pending bit */
            __HAL_TIM_CLEAR_IT(&getpHw()->m_FhrLedTim, TIM_IT_UPDATE);
        
            switch(getpHw()->m_FhrLedCtrl.blinkMode)
            {
                case eFhrBMBlinkOnce:
                    if(getpHw()->m_FhrLedCtrl.ledState == eON)
                    {
                        getpHw()->setGreenLED(false);
                        getpHw()->setRedLED(false);
                        __HAL_TIM_DISABLE(&getpHw()->m_FhrLedTim);
                    }
                    break;
                case eFhrBMBlinkContinous:
                    if(getpHw()->m_FhrLedCtrl.ledState == eON)
                    {
                        getpHw()->m_FhrLedTim.Instance->ARR = getpHw()->m_FhrLedCtrl.offTime;
                        getpHw()->setGreenLED(false);
                        getpHw()->setRedLED(false);
                        getpHw()->m_FhrLedCtrl.ledState = eOFF;
                    }
                    else
                    {
                        getpHw()->m_FhrLedTim.Instance->ARR = getpHw()->m_FhrLedCtrl.onTime;
                        if(getpHw()->m_FhrLedCtrl.color == eFhrLEdColorGreen)
                        {
                            getpHw()->setGreenLED(GREEN_LED_FULL_PWM);
                        }
                        else if(getpHw()->m_FhrLedCtrl.color == eFhrLEdColorYellow)
                        {
                            getpHw()->setGreenLED(GREEN_LED_REDUCED_PWM);
                            getpHw()->setRedLED(RED_LED_REDUCED_PWM);
                        }
                        else if(getpHw()->m_FhrLedCtrl.color == eFhrLEdColorRed)
                        {
                            getpHw()->setRedLED(RED_LED_FULL_PWM);
                        }
                        getpHw()->m_FhrLedCtrl.ledState = eON;
                    }
                    break;
                default:
                    break;
            }
        }        
        
    }
    
    
}

//
//! Get post result for HW
//
POST_ctrl_t HwAbsl::getPostResult(void)
{
    return m_POSTstatus;
}

//
//! Clear post result for HW
//
void HwAbsl::clearPostResult(void)
{
    m_POSTstatus.finished = false;
}

//
//! Return if checksum match for program memory
//
bool HwAbsl::checkSum()
{
    
#ifndef NO_CRC_CHECK
    uint32_t CRCValue = 0;
    extern const uint32_t __checksum;
    
    
    //Last address - 4 (4 byte checksum), - start address, + 1 for total bytes num.
    const uint32_t FLASH_SIZE = 0x080BF7FF - 4 - 0x08002800  + 1;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);

    CRC_ResetDR();

    CRCValue = CRC_CalcBlockCRC( (uint32_t *)0x08002800,  FLASH_SIZE / 4 );

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, DISABLE);

    return (__checksum == CRCValue);
#else
    return 1;
#endif
}

/**
  * Initializes the Global MSP.
  */
void HwAbsl::MspInit(void)
{
  __HAL_RCC_SYSCFG_CLK_ENABLE();
  __HAL_RCC_PWR_CLK_ENABLE();

  /* System interrupt init*/

  /** Disable the internal Pull-Up in Dead Battery pins of UCPD peripheral 
  */
  HAL_PWREx_DisableUCPDDeadBattery();
}


/**
* @brief ADC MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param hadc: ADC handle pointer
* @retval None
*/
void HwAbsl::ADC_MspDeInit(ADC_HandleTypeDef* hadc)
{
  if(hadc->Instance==ADC1)
  {
    /* Peripheral clock disable */
    __HAL_RCC_ADC_CLK_DISABLE();
  
    /**ADC1 GPIO Configuration    
    PC0     ------> ADC1_IN1
    PC1     ------> ADC1_IN2
    PC2     ------> ADC1_IN3 
    */
    //TODO
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2);

    /* ADC1 DMA DeInit */
    HAL_DMA_DeInit(hadc->DMA_Handle);
  }

}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void HwAbsl::SystemClock_Config(void)
{
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_5);

  if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_5)
  {
      //TODO
    //Error_Handler();  
  };

  LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE0);
  LL_RCC_LSI_Enable();

   /* Wait till LSI is ready */
  while(LL_RCC_LSI_IsReady() != 1)
  {
  };

  LL_RCC_MSI_Enable();

   /* Wait till MSI is ready */
  while(LL_RCC_MSI_IsReady() != 1)
  {
  };

  LL_RCC_MSI_EnableRangeSelection();
  LL_RCC_MSI_SetRange(LL_RCC_MSIRANGE_6);
  LL_RCC_MSI_SetCalibTrimming(0);
  LL_PWR_EnableBkUpAccess();
  LL_RCC_ForceBackupDomainReset();
  LL_RCC_ReleaseBackupDomainReset();
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_MSI, LL_RCC_PLLM_DIV_1, 55, LL_RCC_PLLR_DIV_2);
  LL_RCC_PLL_EnableDomain_SYS();
  LL_RCC_PLL_Enable();

   /* Wait till PLL is ready */
  while(LL_RCC_PLL_IsReady() != 1)
  {
  };

   /* Intermediate AHB prescaler 2 when target frequency clock is higher than 80 MHz */
   LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_2);
  
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

   /* Wait till System clock is ready */
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
  {
  };

  /* Insure 1�s transition state at intermediate medium speed clock based on DWT*/
  CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
  

  DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
  DWT->CYCCNT = 0;
  while(DWT->CYCCNT < 100);
  
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
  LL_SetSystemCoreClock(110000000);

   /* Update the time base */
  if (HAL_InitTick (TICK_INT_PRIORITY) != HAL_OK)
  {
      //TODO
    //Error_Handler();  
  };
  
  LL_InitTick(HAL_RCC_GetHCLKFreq(), 1000);
  //LL_InitTick(RCC_GetHCLKClockFreq(RCC_GetSystemClockFreq()), 1000);
}

void HwAbsl::PeriphCommonClock_Config(void)
{
  LL_RCC_PLLSAI1_ConfigDomain_ADC(LL_RCC_PLLSAI1SOURCE_MSI, LL_RCC_PLLSAI1M_DIV_1, 16, LL_RCC_PLLSAI1R_DIV_2);
  LL_RCC_PLLSAI1_EnableDomain_ADC();
}

#if(0)
void HwAbsl::initFmcMsp(SRAM_HandleTypeDef *hSram)
{
    GPIO_InitTypeDef GPIO_InitStruct ={0};
    if (m_FmcInitialized)
    {
        return;
    }
  
    m_FmcInitialized = 1;

    /* Peripheral clock enable */
    __HAL_RCC_FMC_CLK_ENABLE();

    /** FMC GPIO Configuration  
    PF0   ------> FMC_A0
    PF1   ------> FMC_A1
    PF2   ------> FMC_A2
    PF3   ------> FMC_A3
    PF4   ------> FMC_A4
    PF5   ------> FMC_A5
    PF12   ------> FMC_A6
    PF13   ------> FMC_A7
    PF14   ------> FMC_A8
    PF15   ------> FMC_A9
    PG0   ------> FMC_A10
    PG1   ------> FMC_A11
    PE7   ------> FMC_D4
    PE8   ------> FMC_D5
    PE9   ------> FMC_D6
    PE10   ------> FMC_D7
    PE11   ------> FMC_D8
    PE12   ------> FMC_D9
    PE13   ------> FMC_D10
    PE14   ------> FMC_D11
    PE15   ------> FMC_D12
    PD8   ------> FMC_D13
    PD9   ------> FMC_D14
    PD10   ------> FMC_D15
    PD11   ------> FMC_A16
    PD14   ------> FMC_D0
    PD15   ------> FMC_D1
    PG2   ------> FMC_A12
    PG3   ------> FMC_A13
    PG4   ------> FMC_A14
    PG5   ------> FMC_A15
    PD0   ------> FMC_D2
    PD1   ------> FMC_D3
    PD4   ------> FMC_NOE
    PD5   ------> FMC_NWE
    PD7   ------> FMC_NE1
    */
    //TODO
    GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3 
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_12|GPIO_PIN_13 
                          |GPIO_PIN_14|GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF12_FMC;
    HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3 
                          |GPIO_PIN_4|GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF12_FMC;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10 
                          |GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14 
                          |GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF12_FMC;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
                          |GPIO_PIN_14|GPIO_PIN_15|GPIO_PIN_0|GPIO_PIN_1 
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF12_FMC;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}
#endif
/*
void HAL_FMC_MspInit(void)
{
    HwAbsl::getpHw()->initFmcMsp();
}

void HAL_SRAM_MspInit(SRAM_HandleTypeDef* hsram)
{
    HwAbsl::getpHw()->initFmcMsp();
}
*/

void TIM15_IRQHandler(void)
{
    HwAbsl::getpHw()->TMR_FHRled_ISR();
}

////////////////////////////////////////////////////////////////////////////////
// Global Routines Section /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//
// Entry point for library __write for
// STDOUT and STDERR
// Implemented for using printf()
//
int MyLowLevelPutchar(int x)
{
#if CONSOLE_USART<=3
    SerialPort * pPort = HwAbsl::getpHw()->getpSerialConsole();
    if (pPort)
    {
        while ( !pPort->hasTxBufferSpaceAvailable() )
        {
            __WFI();
        }
        pPort->write(x);
        return 1;
    }
#else
    //#error ("Console USART not selected")
#endif
    return -1;
}

//
// Entry point for library __read for
// STDIN
//
int MyLowLevelGetchar()
{
#if CONSOLE_USART <= 3
    SerialPort * pPort = HwAbsl::getpHw()->getpSerialConsole();
    if (pPort)
    {
        return pPort->read();
    }
#else
    //#error ("Console USART not selected")
#endif
    return -1;
}




