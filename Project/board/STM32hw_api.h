//
//STM32_HW_API.H
//The HW abstraction class for Cortex STM32
//
#ifndef _HW_ABSL_H
#define _HW_ABSL_H

// forward declarations
class Mutex;
class SerialPort;
class Dac1;
class FHRMAudio;
class FHRMps;

//
//! \brief The Cortex STM32 hardware wrapper class
//
class HwAbsl
{
public:
    
    HwAbsl();
    ~HwAbsl();
    
    enum OnOff_t{eOFF = 0, eON = 1};
    
    //! \brief Get the system clock frequency after setup
    //! \return : uint32_t System clock in Hz
    uint32_t getSysClk() { return SystemCoreClock; }
    static HwAbsl* getpHw() { return pHw; }
    SerialPort* getpSerialConsole() const {return m_pSerialConsole; }
    FHRMAudio* getpAudio() const {return m_pAudio; }
    FHRMps* getProbeSerial() const {return m_pFHRMps; }

    //For sound test using chest board HW
    void batteryPowerOn();
    void audioOn();
    void audioOff();
    bool isAudioOn();

        
protected:


private:

    void sysTick_init();
    void perip_init();
    void gpio_init();
    void usarts_init();
    void dac_init();
    void audio_init();
    void FHRM_probeComm_init();
    //void USART2_Configuration(void);
    //void USART2_DMA_Configuration(void);
    //void USART2_NVIC_Configuration();
    //void USART2_gpio_init();
    
    //static void DMA1_Channel6_ISR(void);
    
    bool checkSum();

#if OS_USE_VARINTTABLE==0

#endif // #if OS_USE_VARINTTABLE==0

    SerialPort          * m_pSerialConsole;
    static HwAbsl       * pHw;
    Dac1                * m_pDac;
    FHRMAudio           * m_pAudio;
    FHRMps              * m_pFHRMps;
    

};
#endif

