//! \file    isrpriocfg.h
//! \author  Geir Inge tellnes
//! \version 1.0
//! \date    25.02.2010

#ifndef _ISRPRIOCFG_H
#define _ISRPRIOCFG_H

// header includes

// forward declarations
// ...


//! \brief Configuration file for interrupt priorities (lower number=higher priority)\n
//! Please modify to fit your board

//! For NORMAL interrupts that use RTOS resources\n
//! Use ISR priorities >= 128 and <= 255\n
//! Note: these are the only interrupts that PowerPac will enable/disable\n
//!
//! IMPORTANT: These IAR PowerPac RTOS interrupt handlers have to start with OS_EnterInterrupt() or\n
//! OS_EnterNestableInterrupt() and must end with OS_LeaveInterrupt() or\n
//! OS_LeaveNestableInterrupt() if they re-enable interrupts or call RTOS functions\n
//!
//! Ref. PowerPac_RTOS_ARMSuppl.ENU.pdf\n
//! example: ISR_PRIO_SYSTICK = 0x0F, left shift 4 gives 0xF0 = 240\n\n
// ---------------------------------------------------------------------------------------
#define ISR_PRIO_SYSTICK         0x0F   //!< actual priority = 240, implicitly defined by peripheral library, see SysTick_Config and its call to NVIC_SetPriority

#define ISR_PRIO_USART1_IRQn     0x0E   //!< actual priority = 224
#define ISR_PRIO_USART2_IRQn     0x0E   //!< actual priority = 224
#define ISR_PRIO_USART3_IRQn     0x0E   //!< actual priority = 224
#define ISR_PRIO_UART4_IRQn      0x0E   //!< actual priority = 224
#define ISR_PRIO_UART5_IRQn      0x0E   //!< actual priority = 224

#define ISR_PRIO_RTC_IRQn        0x0E   //!< actual priority = 224

#define ISR_PRIO_NORMAL_HIGHEST  0x08   //!< actual priority = 128, the last priority that the RTOS can disable


//! For FAST interrupts that do NOT use RTOS resources
//! Use ISR priorities < 128
//! Ref. PowerPac_RTOS_ARMSuppl.ENU.pdf
//! example: ISR_PRIO_TIM1_UP_IRQn = 0x07, left shift 4 gives 0x70 = 112
// ---------------------------------------------------------------------------------------
#define ISR_PRIO_FAST_LOWEST     0x07   //!< actual priority = 112, the first priority that the RTOS will never disable

#define ISR_PRIO_TIM1_UP_IRQn    0x07   //!< actual priority = 112
#define ISR_PRIO_TIMx_IRQn       0x07   //!< actual priority = 112
//#define ISR_PRIO_CAPCOM1_IRQn    0x06   //!< actual priority = 96
//#define ISR_PRIO_CAPCOM2_IRQn    0x05   //!< actual priority = 80

#define ISR_PRIO_CAPCOM1_IRQn    0x06   //!< actual priority = 96
#define ISR_PRIO_CAPCOM2_IRQn    0x06   //!< actual priority = 96
#define ISR_PRIO_CAPCOM3_IRQn    0x06   //!< actual priority = 96
#define ISR_PRIO_CAPCOM4_IRQn    0x06   //!< actual priority = 96

#define ISR_PRIO_EXTI_IRQn       0x07   //!< actual priority = 112

#define ISR_PRIO_I2C1_EV_IRQn    0x07   //!< actual priority = 112
#define ISR_PRIO_I2C2_EV_IRQn    0x07   //!< actual priority = 112
#define ISR_PRIO_I2C1_ER_IRQn    0x06   //!< actual priority = 96
#define ISR_PRIO_I2C2_ER_IRQn    0x06   //!< actual priority = 96

#define ISR_PRIO_USB_LP_CAN1_RX0_IRQn 0x0C   //!< actual priority = 192
#define ISR_PRIO_OTG_FS_IRQn          0x0C   //!< actual priority = 192
#define ISR_PRIO_USB_PWR_IRQn    	  0x0C   //!< actual priority = 192


#endif   // _ISRPRIOCFG_H

//======== End of isrpriocfg.h ==========

