/******************************************************************

    NAME: HW Abstraction layer

    FILE: STM32_HW_API.CPP

    AUTHOR:  Arnfinn Edvardsen

    DESCRIPTION: The HW abstraction class for Cortex STM32F103RB

    PROJECT:    Haydom

    DEVELOPED BY:

    (C)Laerdal Medical A/S, Stavanger, Norway, 2010

    All rights reserved. Reproduction, use, copy, distribution or
    disclosure to third parties without expressed authority is forbidden.
    If such is authorised, both the copyright note above, and this text,
    must appear in all copies and supporting documentation.

    Express authority is also needed to change this software. Modified
    software is not allowed to be distributed.

    Laerdal Medical A/S
    Research & Development
    P.O. Box 377
    N-4001 Stavanger
    Norway


    HISTORY (version, date, signature, note):
    26jan2010, AEd, Original.
    10Feb2010, GIT, Adapted to Peripheral LIB 3.1.2

*******************************************************************/
#if OS_USE_VARINTTABLE==0
//Prototypes for interrupt service routines
extern "C"
{
    
}
#endif

#include "hwabsl.h"
#include "isrpriocfg.h"
#include "includes.h"
#include "guard.h"
#include "mutex.h"
#include "serialport.h"
#include "driverutils.h"
#include "dac1.h"
#include "FHRMAudio.h"
#include "fhrmprobeserial.h"

// static member init
HwAbsl*  HwAbsl::pHw = 0;

//Prototypes for library support routines
//
extern "C"
{
    int MyLowLevelPutchar(int x);
    int MyLowLevelGetchar();
}


//
//! Construction
//! All basic hardware setup done here
//
HwAbsl::HwAbsl() : m_pSerialConsole(0), m_pDac(0), m_pAudio(0), m_pFHRMps(0)
{
    if (pHw == 0) // prevent multiple construct
    {
        pHw = this;
        
        __disable_irq();

        NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4); // IMPORTANT! must be done BEFORE setting ISR priorities

        sysTick_init();

        perip_init();

      #if OS_USE_VARINTTABLE
        OS_ARM_InstallISRHandler(SysTick_IRQn, (OS_ISR_HANDLER*)OS_Systick);
      #endif
    }

}



//
// Destruction
//
HwAbsl::~HwAbsl()
{
	pHw = 0;
}

//
//! Init the various peripherals
//
void HwAbsl::perip_init()
{
    //timer1_init();
    gpio_init();
    //USART2_gpio_init();
    //USART2_Configuration();
    //USART2_NVIC_Configuration();
    //USART2_DMA_Configuration();
    FHRM_probeComm_init();
    usarts_init();
    dac_init();
    audio_init();
}


    //TODO - More Inits

    //NOTE!  When adding new peripherals, the corresponding driver source must
    //       be added to project folder "StdPeriph_Driver" in addition to
    //       enabling the corresponding defines in "stm32f10x_conf.h"

//
//! Init the system tick timer for 1 ms interrupts
//! Used for PowerPack RTOS
//
void HwAbsl::sysTick_init()
{
    uint32_t relVal;
    uint32_t clock = getSysClk(); // get current system clock freq.

    if(SYSTICK_TIME < 1)         // mS
    {
        relVal = (clock / 1000);
    }
    else
    {
        relVal = (clock / 1000) * SYSTICK_TIME;
    }

    SysTick_Config(relVal);
}


//
//! Init the GPIO pins
//
void HwAbsl::gpio_init()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    

    // Enable GPIO clock and release reset
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_GPIOA, DISABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_GPIOB, DISABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    RCC_APB2PeriphResetCmd(RCC_APB2Periph_GPIOC, DISABLE);

    GPIO_StructInit(&GPIO_InitStructure);
    
    //For sound test using chest board HW
    //Power hold circuit
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    batteryPowerOn();
    
    //Green LED on
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_WriteBit(GPIOC, GPIO_Pin_1, Bit_SET);
    
    //Red LED Init
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_WriteBit(GPIOC, GPIO_Pin_0, Bit_RESET); 
    
    //Audio pwr
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    //
    
}

//
//! Init DAC
//
void HwAbsl::dac_init()
{
#if defined(STM32F10X_HD) || defined(STM32F10X_HD_VL) || defined(STM32F10X_XL) || defined(STM32F10X_CL) || defined(STM32L1XX_MD)
    if (0==m_pDac)
    {
        Dac1::DacInitStruct_t initStruct = {TIM6};
        m_pDac = new Dac1(0x01, initStruct);
    }
#endif
}

void HwAbsl::audio_init()
{
    if (0==m_pAudio && m_pDac)
    {
        m_pAudio = new FHRMAudio(m_pDac);
    }
}

//
//! Init USARTs
//
void HwAbsl::usarts_init()
{
    if (0 == m_pSerialConsole)
    {
        m_pSerialConsole = new SerialPort(SerialPort::COM1, USART1_BAUD, USART1_RXBUFSZ, USART1_TXBUFSZ);
    }
}

void HwAbsl::FHRM_probeComm_init()
{
    if (0 == m_pFHRMps)
    {
        m_pFHRMps = new FHRMps();
    }
    
}

/*
void HwAbsl::USART2_Configuration(void)
{
    USART_InitTypeDef USART_InitStructure;
 
    // USARTx configuration ------------------------------------------------------
    // USARTx configured as follow:
    //    - BaudRate = 115200 baud
    //    - Word Length = 8 Bits
    //    - One Stop Bit
    //    - No parity
    //    - Receive and transmit enabled
    
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
 
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
 
    USART_Init(USART2, &USART_InitStructure);
 
    USART_Cmd(USART2, ENABLE);
}
*/

/*
//FHRM probe, USART2
void HwAbsl::USART2_DMA_Configuration(void)
{
    m_soundBuffer = new uint8_t[SOUNDBUFFER_SIZE];
    
    DMA_InitTypeDef  DMA_InitStructure;

    DMA_DeInit(DMA1_Channel6);

    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)m_soundBuffer;
    DMA_InitStructure.DMA_BufferSize = (uint16_t)SOUNDBUFFER_SIZE - 1;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USART2->DR;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    
    
    DMA_Init(DMA1_Channel6, &DMA_InitStructure);

    // Enable the USART Rx DMA request 
    USART_DMACmd(USART2, USART_DMAReq_Rx, ENABLE);

    //Install ISR handler
    OS_ARM_InstallISRHandler(DMA1_Channel6_IRQn, (OS_ISR_HANDLER*)&DMA1_Channel6_ISR);
    
    // Enable DMA Stream Transfer Complete interrupt 
    DMA_ITConfig(DMA1_Channel6, DMA_IT_TC, ENABLE);

    // Enable the DMA RX Stream 
    DMA_Cmd(DMA1_Channel6, ENABLE);
    
    // Enable DMA Stream Half Transfer and Transfer Complete interrupt 
    //DMA_ITConfig(DMA1_Stream1, DMA_IT_TC, ENABLE);
    //DMA_ITConfig(DMA1_Stream1, DMA_IT_HT, ENABLE);
  

}
*/
/*
//FHRM probe, USART2
void HwAbsl::USART2_NVIC_Configuration()
{
    NVIC_InitTypeDef NVIC_InitStructure;
  
    // Configure the Priority Group to 2 bits 
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
  
    // Enable the UART2 RX DMA Interrupt 
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel6_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}
*/

//For sound test using chest board HW
void HwAbsl::batteryPowerOn()
{
    GPIO_WriteBit(GPIOB, GPIO_Pin_12, Bit_SET);  // VBAT_ON=ON -> use battery
}

void HwAbsl::audioOff()
{
    GPIO_WriteBit(GPIOC, GPIO_Pin_14, Bit_SET);
}

void HwAbsl::audioOn()
{
    GPIO_WriteBit(GPIOC, GPIO_Pin_14, Bit_RESET);
}

bool HwAbsl::isAudioOn()
{
    return GPIO_ReadOutputDataBit(GPIOC, GPIO_Pin_14) == Bit_RESET;
}

/*
void HwAbsl::DMA1_Channel6_ISR(void)
{
    // Test on DMA Stream Transfer Complete interrupt 
    if (DMA_GetITStatus(DMA1_IT_TC6))
    {
        // Clear DMA Stream Transfer Complete interrupt pending bit 
        DMA_ClearITPendingBit(DMA1_IT_TC6);
    }
}
*/

////////////////////////////////////////////////////////////////////////////////
// Global Routines Section /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//
// Entry point for library __write for
// STDOUT and STDERR
// Implemented for using printf()
//
int MyLowLevelPutchar(int x)
{
#if CONSOLE_USART==1
    SerialPort * pPort = HwAbsl::getpHw()->getpSerialConsole();
    if (pPort)
    {
        while ( !pPort->hasTxBufferSpaceAvailable() )
        {
            __WFI();
        }
        pPort->write(x);
        return 1;
    }
#else
    #error ("Console USART not selected")
#endif
    return -1;
}

//
// Entry point for library __read for
// STDIN
//
int MyLowLevelGetchar()
{
#if CONSOLE_USART==1
    SerialPort * pPort = HwAbsl::getpHw()->getpSerialConsole();
    if (pPort)
    {
        return pPort->read();
    }
#else
    #error ("Console USART not selected")
#endif
    return -1;
}


#if OS_USE_VARINTTABLE==0

////////////////////////////////////////////////////////////////////////////////
// Interrupt Service Routines Section //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// NOTE! PowerPack RTOS handlers OS_Systick and OS_Exception are inserted
//       directly into the vector table! (See stm32f10_vector.c).


#endif


//TODO - More handlers
