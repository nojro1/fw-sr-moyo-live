/*********************************************************************
*                SEGGER MICROCONTROLLER GmbH & Co. KG                *
*        Solutions for real time microcontroller applications        *
**********************************************************************
*                                                                    *
*        (c) 2003-2011     SEGGER Microcontroller GmbH & Co KG       *
*                                                                    *
*        Internet: www.segger.com    Support:  support@segger.com    *
*                                                                    *
**********************************************************************

**** emFile file system for embedded applications ****
emFile is protected by international copyright laws. Knowledge of the
source code may not be used to write a similar product. This file may
only be used in accordance with a license and should not be re-
distributed in any way. We appreciate your understanding and fairness.
----------------------------------------------------------------------
----------------------------------------------------------------------
Original File        : MMC_CM_HW_STM32F103ZET6_STM_MB672_RevD.c
Purpose              : Hardware layer for MMC/SD driver in CardMode, for the ST MB672 Eval board Rev. D (STM3210E-EVAL)

NOJRO1 29.11.2012    : Adapted for use in Haydom
---------------------------END-OF-HEADER------------------------------
*/

/*********************************************************************
*
*             #include section
*
**********************************************************************
*/
#include "FS_Int.h"
#include "MMC_SD_CardMode_X_HW.h"
#include "stm32l5xx.h" //TODO
#include "RTOS.h"

/*********************************************************************
*
*       Defines non-configurable
*
**********************************************************************
*/
#define MMC_CLK           72000L  // Clock of MMC module in kHz
#define MAX_BLOCK_SIZE    512     // Maximun number of bytes in a transfered data block

/*********************************************************************
*
*       Register macros & defines for register addresses
*
**********************************************************************
*/
#define _GPIO_PB0_BASE_ADDR         (0x40010C00UL)
#define _GPIO_PC0_BASE_ADDR         (0x40011000UL)
#define _GPIO_PD0_BASE_ADDR         (0x40011400UL)
#define _RCC_BASE                   (0x40021000UL)
#define _SDIO_BASE                  (0x40018000UL)
#define _DMA2_BASE                  (0x40020400UL)

#define _SYSCTRL_CP_PORT_BIT        (1UL << 3)  //Clock reset (RCC_APB2RSTR) and enable (RCC_APB2ENR) for port B is bit3
#define _SDIO_CP_BIT                (1UL << 15) //SD_Detect = PB15
#define _SDIO_CP_CR_SHIFT           (28)        //Mode15 and CNF15 in port configuration register
#define _SDIO_CP_CR                 *(volatile unsigned int*)(_GPIO_PB0_BASE_ADDR + 0x04)
#define _SDIO_CP_IDR                *(volatile unsigned int*)(_GPIO_PB0_BASE_ADDR + 0x08)
#define _SDIO_CP_ODR                *(volatile unsigned int*)(_GPIO_PB0_BASE_ADDR + 0x0C)
#define _SYSCTRL_PC_PORT_BIT        (1UL << 4) //Clock reset (RCC_APB2RSTR) and enable (RCC_APB2ENR) for port C is bit4
#define _SYSCTRL_PD_PORT_BIT        (1UL << 5) //Clock reset (RCC_APB2RSTR) and enable (RCC_APB2ENR) for port D is bit5

#define _RCC_APB2RSTR               *(volatile unsigned *)(_RCC_BASE + 0x0C)
#define _RCC_AHBENR                 *(volatile unsigned *)(_RCC_BASE + 0x14)
#define _RCC_APB2ENR                *(volatile unsigned *)(_RCC_BASE + 0x18)

#define _RCC_AHBENR_DMA2EN          (1UL <<  1)
#define _RCC_AHBENR_SDIOEN          (1UL << 10)

#define _SDIO_POWER                 *(volatile unsigned *)(_SDIO_BASE + 0x00)
#define _SDIO_CLKCR                 *(volatile unsigned *)(_SDIO_BASE + 0x04)
#define _SDIO_ARG                   *(volatile unsigned *)(_SDIO_BASE + 0x08)
#define _SDIO_CMD                   *(volatile unsigned *)(_SDIO_BASE + 0x0C)
#define _SDIO_RESPCMD               *(volatile unsigned *)(_SDIO_BASE + 0x10)
#define _SDIO_RESP1                 *(volatile unsigned *)(_SDIO_BASE + 0x14)
#define _SDIO_RESP2                 *(volatile unsigned *)(_SDIO_BASE + 0x18)
#define _SDIO_RESP3                 *(volatile unsigned *)(_SDIO_BASE + 0x1C)
#define _SDIO_RESP4                 *(volatile unsigned *)(_SDIO_BASE + 0x20)
#define _SDIO_DTIMER                *(volatile unsigned *)(_SDIO_BASE + 0x24)
#define _SDIO_DLEN                  *(volatile unsigned *)(_SDIO_BASE + 0x28)
#define _SDIO_DCTRL                 *(volatile unsigned *)(_SDIO_BASE + 0x2C)
#define _SDIO_DCOUNT                *(volatile unsigned *)(_SDIO_BASE + 0x30)
#define _SDIO_STA                   *(volatile unsigned *)(_SDIO_BASE + 0x34)
#define _SDIO_ICR                   *(volatile unsigned *)(_SDIO_BASE + 0x38)
#define _SDIO_MASK                  *(volatile unsigned *)(_SDIO_BASE + 0x3C)
#define _SDIO_FIFOCNT               *(volatile unsigned *)(_SDIO_BASE + 0x48)
#define _SDIO_FIFO                  *(volatile unsigned *)(_SDIO_BASE + 0x80)

#define _SDIO_CLKCR_WIDBUS_MASK           (0x3UL << 11)
#define _SDIO_CLKCR_WIDBUS_4B             (0x01L << 11)
#define _SDIO_CLKCR_CLKEN                 (1UL   <<  8)
#define _SDIO_CLKCR_HWFC_EN               (1UL   << 14)

#define _SDIO_CMD_WAITPEND                (1UL << 9)
#define _SDIO_CMD_CPSMEN                  (1UL << 10)
#define _SDIO_CMD_CMD_MASK                (0x3FUL)
#define _SDIO_CMD_RESPONSE_FORMAT_MASK    (3UL << 6)
#define _SDIO_CMD_RESPONSE_FORMAT_SHORT   (1UL << 6)
#define _SDIO_CMD_RESPONSE_FORMAT_LONG    (3UL << 6)

#define _SDIO_STA_CCRCFAIL                (1UL << 0)
#define _SDIO_STA_DCRCFAIL                (1UL << 1)
#define _SDIO_STA_CTIMEOUT                (1UL << 2)
#define _SDIO_STA_DTIMEOUT                (1UL << 3)
#define _SDIO_STA_TXUNDERR                (1UL << 4)
#define _SDIO_STA_RXOVERR                 (1UL << 5)
#define _SDIO_STA_CMDREND                 (1UL << 6)
#define _SDIO_STA_CMDSENT                 (1UL << 7)
#define _SDIO_STA_DATAEND                 (1UL << 8)
#define _SDIO_STA_STBITERR                (1UL << 9)
#define _SDIO_STA_DBCKEND                 (1UL <<10)
#define _SDIO_STA_CMDACT                  (1UL <<11)
#define _SDIO_STA_TXACT                   (1UL <<12)
#define _SDIO_STA_RXACT                   (1UL <<13)
#define _SDIO_STA_TXFIFOHE                (1UL <<14)
#define _SDIO_STA_RXFIFOHF                (1UL <<15)
#define _SDIO_STA_TXFIFOF                 (1UL <<16)
#define _SDIO_STA_RXFIFOF                 (1UL <<17)
#define _SDIO_STA_TXFIFOE                 (1UL <<18)
#define _SDIO_STA_RXFIFOE                 (1UL <<19)
#define _SDIO_STA_TXDAVL                  (1UL <<20)
#define _SDIO_STA_RXDAVL                  (1UL <<21)
#define _SDIO_STA_SDIOIT                  (1UL <<22)
#define _SDIO_STA_CEATAEND                (1UL <<23)

#define _SDIO_ICR_CCRCFAIL                (1UL << 0)
#define _SDIO_ICR_DCRCFAIL                (1UL << 1)
#define _SDIO_ICR_CTIMEOUT                (1UL << 2)
#define _SDIO_ICR_DTIMEOUT                (1UL << 3)
#define _SDIO_ICR_TXUNDERR                (1UL << 4)
#define _SDIO_ICR_RXOVERR                 (1UL << 5)
#define _SDIO_ICR_CMDREND                 (1UL << 6)
#define _SDIO_ICR_CMDSENT                 (1UL << 7)
#define _SDIO_ICR_DATAEND                 (1UL << 8)
#define _SDIO_ICR_STBITERR                (1UL << 9)
#define _SDIO_ICR_DBCKEND                 (1UL <<10)
#define _SDIO_ICR_SDIOIT                  (1UL <<22)
#define _SDIO_ICR_CEATAEND                (1UL <<23)

#define _SDIO_DCTRL_DTEN                  (1UL << 0)
#define _SDIO_DCTRL_DTDIR                 (1UL << 1)
#define _SDIO_DCTRL_DTMODE                (1UL << 2)
#define _SDIO_DCTRL_DMAEN                 (1UL << 3)
#define _SDIO_DCTRL_DBLOCKSIZE_SHIFT      (4)
#define _SDIO_DCTRL_DBLOCKSIZE_MASK       (0xFUL << _SDIO_DCTRL_DBLOCKSIZE_SHIFT)
#define _SDIO_DCTRL_RWSTART               (1UL << 8)
#define _SDIO_DCTRL_RWSTOP                (1UL << 9)
#define _SDIO_DCTRL_RWMOD                 (1UL <<10)
#define _SDIO_DCTRL_SDIOEN                (1UL <<11)

#define _SDIO_DLEN_DATALENGTH_MASK        (0x03FFFFFFuL)

#define _DMA_ISR                    *(volatile unsigned *)(_DMA2_BASE + 0x00)
#define _DMA_IFCR                   *(volatile unsigned *)(_DMA2_BASE + 0x04)
#define _DMA_CCR1                   *(volatile unsigned *)(_DMA2_BASE + 0x08)
#define _DMA_CNDTR1                 *(volatile unsigned *)(_DMA2_BASE + 0x0C)
#define _DMA_CPAR1                  *(volatile unsigned *)(_DMA2_BASE + 0x10)
#define _DMA_CMAR1                  *(volatile unsigned *)(_DMA2_BASE + 0x14)
#define _DMA_CCR2                   *(volatile unsigned *)(_DMA2_BASE + 0x1C)
#define _DMA_CNDTR2                 *(volatile unsigned *)(_DMA2_BASE + 0x20)
#define _DMA_CPAR2                  *(volatile unsigned *)(_DMA2_BASE + 0x24)
#define _DMA_CMAR2                  *(volatile unsigned *)(_DMA2_BASE + 0x28)
#define _DMA_CCR3                   *(volatile unsigned *)(_DMA2_BASE + 0x30)
#define _DMA_CNDTR3                 *(volatile unsigned *)(_DMA2_BASE + 0x34)
#define _DMA_CPAR3                  *(volatile unsigned *)(_DMA2_BASE + 0x38)
#define _DMA_CMAR3                  *(volatile unsigned *)(_DMA2_BASE + 0x3C)
#define _DMA_CCR4                   *(volatile unsigned *)(_DMA2_BASE + 0x44)
#define _DMA_CNDTR4                 *(volatile unsigned *)(_DMA2_BASE + 0x48)
#define _DMA_CPAR4                  *(volatile unsigned *)(_DMA2_BASE + 0x4C)
#define _DMA_CMAR4                  *(volatile unsigned *)(_DMA2_BASE + 0x50)
#define _DMA_CCR5                   *(volatile unsigned *)(_DMA2_BASE + 0x58)
#define _DMA_CNDTR5                 *(volatile unsigned *)(_DMA2_BASE + 0x5C)
#define _DMA_CPAR5                  *(volatile unsigned *)(_DMA2_BASE + 0x60)
#define _DMA_CMAR5                  *(volatile unsigned *)(_DMA2_BASE + 0x64)
#define _DMA_CCR6                   *(volatile unsigned *)(_DMA2_BASE + 0x6C)
#define _DMA_CNDTR6                 *(volatile unsigned *)(_DMA2_BASE + 0x70)
#define _DMA_CPAR6                  *(volatile unsigned *)(_DMA2_BASE + 0x74)
#define _DMA_CMAR6                  *(volatile unsigned *)(_DMA2_BASE + 0x78)
#define _DMA_CCR7                   *(volatile unsigned *)(_DMA2_BASE + 0x80)
#define _DMA_CNDTR7                 *(volatile unsigned *)(_DMA2_BASE + 0x84)
#define _DMA_CPAR7                  *(volatile unsigned *)(_DMA2_BASE + 0x88)
#define _DMA_CMAR7                  *(volatile unsigned *)(_DMA2_BASE + 0x8C)

#define _DMA_ISR_GIF4               (1UL  << 12)
#define _DMA_ISR_TCIF4              (1UL  << 13)
#define _DMA_ISR_HTIF4              (1UL  << 14)
#define _DMA_ISR_TEIF4              (1UL  << 15)

#define _DMA_IFCR_CGIF4             (1UL  << 12)
#define _DMA_IFCR_CTCIF4            (1UL  << 13)
#define _DMA_IFCR_CHTIF4            (1UL  << 14)
#define _DMA_IFCR_CTEIF4            (1UL  << 15)

#define _DMA_CCR_EN                 (1UL  <<   0)
#define _DMA_CCR_TCIE               (1UL  <<   1)
#define _DMA_CCR_HTIE               (1UL  <<   2)
#define _DMA_CCR_TEIE               (1UL  <<   3)
#define _DMA_CCR_DIR                (1UL  <<   4)
#define _DMA_CCR_CIRC               (1UL  <<   5)
#define _DMA_CCR_PINC               (1UL  <<   6)
#define _DMA_CCR_MINC               (1UL  <<   7)
#define _DMA_CCR_PSIZE_MASK         (3UL  <<   8)
#define _DMA_CCR_PSIZE_SHIFT        (8)
#define _DMA_CCR_MSIZE_MASK         (3UL  <<  10)
#define _DMA_CCR_MSIZE_SHIFT        (10)
#define _DMA_CCR_PL_MASK            (3UL  <<  12)
#define _DMA_CCR_PL_SHIFT           (12)
#define _DMA_CCR_MEM2MEM            (1UL  <<  14)

#define _SDIO_PORT_CR               *(volatile unsigned int*)(_GPIO_PC0_BASE_ADDR + 0x04)
#define _SDIO_CMD_PIN_PORT_CR       *(volatile unsigned int*)(_GPIO_PD0_BASE_ADDR + 0x00)

#define PERIPHERAL_TO_MEMORY        0
#define MEMORY_TO_PERIPHERAL        1

#define MAX_NUM_BLOCKS              (_SDIO_DLEN_DATALENGTH_MASK / MAX_BLOCK_SIZE)   // Number of blocks to transfer at once

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/
static U8     _IgnoreCRC;
static U16    _BlockSize;
static U16    _NumBlocks;
static U32    _DataBlockSize;
static void * _pBuffer;

/*********************************************************************
*
*       Static code
*
**********************************************************************
*/
/*********************************************************************
*
*       _ld
*/
static U16 _ld(U32 Value) {
  U16 i;
  for (i = 0; i < 16; i++) {
    if ((1UL << i) == Value) {
      break;
    }
  }
  return i;
}

/*********************************************************************
*
*       _DMAStart
*/
static void _DMAStart(U32 * pMemory, U32 * pPripheral, U32 NumBytes, U8 Direction)
{
  _DMA_CCR4 &= ~_DMA_CCR_EN;      // disable channel
  _DMA_CPAR4 = (U32)pPripheral;   // Periphery data register address
  _DMA_CMAR4 = (U32)pMemory;      // Memory buffer address
  _DMA_CNDTR4 = NumBytes / sizeof(U32);
  _DMA_IFCR  = _DMA_IFCR_CGIF4;   // Clear pending interrupt flags
  if(MEMORY_TO_PERIPHERAL == Direction) {
    _DMA_CCR4 |= _DMA_CCR_DIR;
  } else {
    _DMA_CCR4 &=~_DMA_CCR_DIR;
  }
  _DMA_CCR4 |= _DMA_CCR_EN;      // enable channel
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWNumBlocks
*
*  Function description
*    Sets the number of block (sectors) to be transferred.
*
*/
void FS_MMC_HW_X_SetHWNumBlocks(U8 Unit, U16 NumBlocks) {
  _NumBlocks = NumBlocks;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetHWBlockLen
*
*  Function description
*    Sets the block size (sector size) that shall be transferred.
*/
void FS_MMC_HW_X_SetHWBlockLen(U8 Unit, U16 BlockSize) {
  _BlockSize     = BlockSize;
  _DataBlockSize = _ld(BlockSize) << _SDIO_DCTRL_DBLOCKSIZE_SHIFT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetMaxSpeed
*
*  Function description
*    Sets the frequency of the MMC/SD card controller.
*    The frequency is given in kHz.
*    It is called 2 times:
*     1. During card initialization
*        Initialize the frequency to not more than 400kHz.
*
*     2. After card initialization
*        The CSD register of card is read and the max frequency
*        the card can operate is determined.
*        [In most cases: MMC cards 20MHz, SD cards 25MHz]
*
*/
U16 FS_MMC_HW_X_SetMaxSpeed(U8 Unit, U16 Freq) {
  U32      Fact;
  unsigned Div;

  _SDIO_CLKCR &=  ~_SDIO_CLKCR_CLKEN;
  Fact         = 2;
  Div          = 0;
  while ((Freq * Fact) < MMC_CLK) {
    ++Fact;
    if (0xFF == ++Div) {
      break;
    }
  }
  _SDIO_CLKCR &= ~0xFF;
  _SDIO_CLKCR |=  Div | _SDIO_CLKCR_CLKEN;
  return MMC_CLK / Div;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsPresent
*
*    Returns the state of the media. If you do not know the state, return
*    FS_MEDIA_STATE_UNKNOWN and the higher layer will try to figure out if
*    a media is present.
*
*  Parameters:
*    Unit                 - Device Index
*
*  Return value:
*    FS_MEDIA_STATE_UNKNOWN      - the state of the media is unkown
*    FS_MEDIA_NOT_PRESENT        - no card is present
*    FS_MEDIA_IS_PRESENT         - a card is present
*/
int FS_MMC_HW_X_IsPresent(U8 Unit) {
    return (_SDIO_CP_IDR & _SDIO_CP_BIT) ? FS_MEDIA_NOT_PRESENT : FS_MEDIA_IS_PRESENT;
}

/*********************************************************************
*
*       FS_MMC_HW_X_IsWriteProtected
*
*  Function description
*    Returns whether card is write protected or not.
*/
int FS_MMC_HW_X_IsWriteProtected  (U8 Unit) {
  return 0;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetResponseTimeOut
*
*  Function description
*    Sets the response time out value given in MMC/SD card cycles.
*
*/
void FS_MMC_HW_X_SetResponseTimeOut(U8 Unit, U32 Value) {
  //
  // The response timeout is fixed in hardware
  //
  ;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetReadDataTimeOut
*
*  Function description
*    Sets the read data time out value given in MMC/SD card cycles.
*/
void FS_MMC_HW_X_SetReadDataTimeOut(U8 Unit, U32 Value) {
  _SDIO_DTIMER = Value;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SendCmd
*
*  Function description
*    Sends a command to the MMC/SD card.
*/
void FS_MMC_HW_X_SendCmd(U8 Unit, unsigned Cmd, unsigned CmdFlags, unsigned ResponseType, U32 Arg) {
  U32 CmdCfg;
  U8  Direction;
  U32 NumBytes;

  CmdCfg = _SDIO_CMD_CPSMEN;
  _IgnoreCRC = 0;
  switch(ResponseType)
  {
  case FS_MMC_RESPONSE_FORMAT_R3:
    _IgnoreCRC = 1;
  case FS_MMC_RESPONSE_FORMAT_R1:
    CmdCfg |= _SDIO_CMD_RESPONSE_FORMAT_SHORT;
    break;
  case FS_MMC_RESPONSE_FORMAT_R2:
    CmdCfg |= _SDIO_CMD_RESPONSE_FORMAT_LONG;
    break;
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_USE_SD4MODE) {   // 4 bit mode?
    _SDIO_CLKCR |= _SDIO_CLKCR_WIDBUS_4B;
  } else {
    _SDIO_CLKCR &=~_SDIO_CLKCR_WIDBUS_MASK;
  }
  if (CmdFlags & FS_MMC_CMD_FLAG_DATATRANSFER) {
    _SDIO_ICR = _SDIO_ICR_DCRCFAIL
              | _SDIO_ICR_DTIMEOUT
              | _SDIO_ICR_TXUNDERR
              | _SDIO_ICR_DATAEND
              | _SDIO_ICR_DBCKEND
              | _SDIO_ICR_STBITERR
              | _SDIO_ICR_RXOVERR
              ;
    NumBytes = _BlockSize * _NumBlocks;
    _SDIO_DLEN = NumBytes;
    if (CmdFlags & FS_MMC_CMD_FLAG_WRITETRANSFER) {
      Direction = MEMORY_TO_PERIPHERAL;
    } else {
      Direction = PERIPHERAL_TO_MEMORY;
    }
    _DMAStart((U32 *)_pBuffer, (void *)&_SDIO_FIFO, NumBytes, Direction);
  }
  //
  // Clear pending status flags
  //
  _SDIO_ICR = _SDIO_ICR_CCRCFAIL
            | _SDIO_ICR_CTIMEOUT
            | _SDIO_ICR_CMDREND
            | _SDIO_ICR_CMDSENT
            ;
  _SDIO_ARG = Arg;
  _SDIO_CMD = CmdCfg | (Cmd & _SDIO_CMD_CMD_MASK);
  if (CmdFlags & FS_MMC_CMD_FLAG_INITIALIZE)  {
    while (!(( _SDIO_STA_CMDSENT  |
               _SDIO_STA_CMDREND  |
               _SDIO_STA_CCRCFAIL |
               _SDIO_STA_CTIMEOUT) & _SDIO_STA));
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetResponse
*
*  Function description
*    Receives the responses that was sent by the card after
*    a command was sent to the card.
*/
int FS_MMC_HW_X_GetResponse(U8 Unit, void *pBuffer, U32 Size) {
  U8           * p;
  int            NumBytes;
  volatile U32 * pReg;
  U32            Data32;

  p        = pBuffer;
  NumBytes = Size;
  do {
    Data32 = _SDIO_STA;
  } while (!((_SDIO_STA_CMDSENT  |
              _SDIO_STA_CMDREND  |
              _SDIO_STA_CCRCFAIL |
              _SDIO_STA_CTIMEOUT) & Data32));
  if (_SDIO_STA_CTIMEOUT & Data32) {
    return FS_MMC_CARD_RESPONSE_TIMEOUT;
  }
  if ((_SDIO_STA_CCRCFAIL & Data32) && !_IgnoreCRC) {
    return FS_MMC_CARD_READ_CRC_ERROR;
  }
  if ((_SDIO_STA_CMDREND & Data32) || _IgnoreCRC) {
    *p++ = _SDIO_RESPCMD;
    NumBytes--;
    pReg = (volatile U32 *)(&_SDIO_RESP1);
    do {
      Data32 = FS_LoadU32BE((const U8 *)pReg);
      FS_StoreU32LE(p, Data32);
      NumBytes -= 4;
      pReg++;
      p += 4;
    } while (NumBytes >= 4);
    _SDIO_ICR = _SDIO_STA_CMDREND;
  }
  return FS_MMC_CARD_NO_ERROR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_ReadData
*
*  Function description
*    Reads data from MMC/SD card try the MMC/SD card controller.
*
*/
int FS_MMC_HW_X_ReadData(U8 Unit, void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  U32 Status;

  FS_USE_PARA(Unit);
  FS_USE_PARA(pBuffer);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  //
  // Data transfer starts when the data control register is updated
  //
  _SDIO_DCTRL = _DataBlockSize
              | _SDIO_DCTRL_DTDIR
              | _SDIO_DCTRL_DMAEN
              | _SDIO_DCTRL_DTEN
              ;
  do {
    if (_DMA_ISR & _DMA_ISR_TEIF4) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    Status = _SDIO_STA;
    if (Status & _SDIO_STA_DCRCFAIL) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (Status & _SDIO_STA_DTIMEOUT) {
      return FS_MMC_CARD_READ_TIMEOUT;
    }
    if (Status & _SDIO_STA_RXOVERR) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    if (Status & _SDIO_STA_STBITERR) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
  } while (!(Status & _SDIO_STA_DATAEND));
  return FS_MMC_CARD_NO_ERROR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_WriteData
*
*  Function description
*    Writes the data to MMC/SD card try the MMC/SD card controller.
*
*/
int FS_MMC_HW_X_WriteData(U8 Unit, const void * pBuffer, unsigned NumBytes, unsigned NumBlocks) {
  U32 Status;

  FS_USE_PARA(Unit);
  FS_USE_PARA(pBuffer);
  FS_USE_PARA(NumBytes);
  FS_USE_PARA(NumBlocks);
  //
  // Data transfer starts when the data control register is updated
  //
  _SDIO_DCTRL = _DataBlockSize
              | _SDIO_DCTRL_DTEN
              | _SDIO_DCTRL_DMAEN
              ;
  do {
    if (_DMA_ISR & _DMA_ISR_TEIF4) {
      return FS_MMC_CARD_READ_CRC_ERROR;
    }
    Status = _SDIO_STA;
    if (Status & _SDIO_STA_DCRCFAIL) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;
    }
    if (Status & _SDIO_STA_DTIMEOUT) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;
    }
    if (Status & _SDIO_STA_TXUNDERR) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;
    }
    if (Status & _SDIO_STA_STBITERR) {
      return FS_MMC_CARD_WRITE_CRC_ERROR;
    }
  } while (!(Status & _SDIO_STA_DATAEND));
  return FS_MMC_CARD_NO_ERROR;
}

/*********************************************************************
*
*       FS_MMC_HW_X_Delay
*
*  Function description
*    Waits for a certain time given by the parameter.
*
*/
void FS_MMC_HW_X_Delay(int ms) {
  while (ms--) {
    for(volatile U32 i = 4500; i; i--) {
      ;
    }
  }
}

/*********************************************************************
*
*       FS_MMC_HW_X_InitHW
*
*  Function description
*    Initialize the MMC/SD card controller.
*/
void FS_MMC_HW_X_InitHW(U8 Unit) {
  FS_USE_PARA(Unit);
  //
  // GPIO enable clock and release Reset
  //
  _RCC_APB2RSTR &= ~(  _SYSCTRL_CP_PORT_BIT
                     | _SYSCTRL_PC_PORT_BIT
                     | _SYSCTRL_PD_PORT_BIT
                     );
  _RCC_APB2ENR  |=     _SYSCTRL_CP_PORT_BIT
                     | _SYSCTRL_PC_PORT_BIT
                     | _SYSCTRL_PD_PORT_BIT    
                     ;

  //
  // DMA2 and SDIO clk enable
  //
  _RCC_AHBENR   |= _RCC_AHBENR_DMA2EN | _RCC_AHBENR_SDIOEN;
  //
  // Init CP pin
  // Port in input with pull-up
  //
  _SDIO_CP_CR  &= ~(7UL <<  _SDIO_CP_CR_SHIFT);
  _SDIO_CP_CR  |=  (8UL <<  _SDIO_CP_CR_SHIFT);
  _SDIO_CP_ODR |=  _SDIO_CP_BIT;
  //
  // Configure D0, D1, D2, D3, CLK lines
  //
  _SDIO_PORT_CR &= ~(  (4UL <<  0)
                     | (4UL <<  4)
                     | (4UL <<  8)
                     | (4UL << 12)
                     | (4UL << 16)
                     );
  _SDIO_PORT_CR |=     (11UL <<  0)
                     | (11UL <<  4)
                     | (11UL <<  8)
                     | (11UL << 12)
                     | (11UL << 16)
                    ;
  //
  // Configure CMD line
  //
  _SDIO_CMD_PIN_PORT_CR &= ~(4UL <<  8);
  _SDIO_CMD_PIN_PORT_CR |=  (11UL <<  8);
  //
  // Init DMA2 ch 4
  //
  _DMA_IFCR  = _DMA_IFCR_CGIF4;
  _DMA_CCR4  = (2UL << _DMA_CCR_PSIZE_SHIFT)    // Periphery bus 32 bits
             | (2UL << _DMA_CCR_MSIZE_SHIFT)    // Memory bus 32 bits
             | _DMA_CCR_MINC                    // Momory increment enable
             ;
  //
  // Reset SDIO
  //
  _SDIO_CLKCR    =  (1UL << 9); // Power saving enable
  _SDIO_POWER    = 0x00000000;  // Power off
  _SDIO_ARG      = 0x00000000;
  _SDIO_CMD      = 0x00000000;
  _SDIO_DTIMER   = 0x00000000;
  _SDIO_DLEN     = 0x00000000;
  _SDIO_DCTRL    = 0x00000000;
  _SDIO_ICR      = 0x00C007FF;
  _SDIO_MASK     = 0x00000000;
  _SDIO_POWER    = 0x00000003;  // Power On
  FS_MMC_HW_X_Delay(100);
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxReadBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be read at once with a single
*    READ_MULTIPLE_SECTORS.
*
*/
U16 FS_MMC_HW_X_GetMaxReadBurst (U8 Unit) {
  FS_USE_PARA(Unit);
  return (U16)MAX_NUM_BLOCKS;
}

/*********************************************************************
*
*       FS_MMC_HW_X_GetMaxWriteBurst
*
*  Function description
*    Returns the number of block (sectors)
*    that can be written at once with a single
*    WRITE_MULTIPLE_SECTORS.
*/
U16 FS_MMC_HW_X_GetMaxWriteBurst (U8 Unit) {
  FS_USE_PARA(Unit);
  return (U16)MAX_NUM_BLOCKS;
}

/*********************************************************************
*
*       FS_MMC_HW_X_SetDataPointer
*
*  Function description
*    Tells the hardware layer where to read data from
*    or write data to. This may be necessary for some controller,
*    before sending the command to the card, eg. programming the DMA.
*    In most cases this function can be left empty.
*
*  Parameters:
*    Unit          - SD card controller no, in case there are more than one.
*
*/
void FS_MMC_HW_X_SetDataPointer(U8 Unit, const void * p) {
  FS_USE_PARA(Unit);
  _pBuffer = (void *)p; // cast const away as this buffer is used also for storing the data from card
}


/*************************** End of file ****************************/

