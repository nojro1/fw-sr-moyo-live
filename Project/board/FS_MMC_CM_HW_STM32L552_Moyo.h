/*********************************************************************
*                     SEGGER Microcontroller GmbH                    *
*                        The Embedded Experts                        *
**********************************************************************
*                                                                    *
*       (c) 2003 - 2019  SEGGER Microcontroller GmbH                 *
*                                                                    *
*       www.segger.com     Support: support@segger.com               *
*                                                                    *
**********************************************************************
----------------------------------------------------------------------
File        : FS_MMC_CM_HW_STM32F746_ST_STM32F746G-DISCO.h
Purpose     : Low-level SD card driver for ST STM32F746G-DISCO eval board.
------------  END-OF-HEADER  -----------------------------------------
*/

#ifndef FS_MMC_CM_HW_STM32L552_MOYO_H     // Avoid recursive and multiple inclusion
#define FS_MMC_CM_HW_STM32L552_MOYO_H

#include "FS.h"

/*********************************************************************
*
*       Public data
*
**********************************************************************
*/
//extern const FS_MMC_HW_TYPE_CM FS_MMC_HW_CM_STM32L552_MOYO;

#endif  // FS_MMC_CM_HW_STM32L552_MOYO_H

/*************************** End of file ****************************/
