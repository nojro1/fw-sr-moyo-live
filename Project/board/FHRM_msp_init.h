//! @author Jan Arild R�yneberg
//! @date   01.04.2020
//!

#ifndef _FHRM_MSP_INIT_H_
#define _FHRM_MSP_INIT_H_

#include "stm32l5xx_hal.h"

void HAL_MspInit(void);
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base);

#endif
